﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CrystalViewer.aspx.cs" Inherits="IGFoodProject.CrystalViewer.CrystalViewer" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>

    <asp:Label runat="server"  Text="" ID="testLebel"></asp:Label>
    <form id="form1" runat="server">
        <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
        <div>
            <CR:crystalreportviewer id="rptViewer" runat="server" autodatabind="true" enabledatabaselogonprompt="False" enableparameterprompt="False" toolpanelview="None" ondisposed="rptViewerBasic_Disposed" onunload="rptViewerBasic_Unload" />
           
        </div>
    </form>
</body>
</html>
