﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using IGFoodProject.Models.ReportModel;
using IGFoodProject.Utility;
using IGFoodProject.DAL;
using IGFoodProject.DAL.Accounts;
using IGFoodProject.DAL.ReportDAL;
using IGFoodProject.Models;
using IGFoodProject.Models.Accounts;
using IGFoodProject.ViewModel;
using System.Drawing.Printing;
using System.IO;
using System.Net;
using IGFoodProject.DAL.Barcode;

namespace IGFoodProject.CrystalViewer
{
    public partial class CrystalViewer : System.Web.UI.Page
    {
        private DataSet ds;
        ReportDocument rptdoc = new ReportDocument();
        private string[] reportParameters;
        private UtilityDAL aUtilityDAL = new UtilityDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session[SessionCollection.UserName] == null)
            {
                Response.Redirect("~/Home/Index");
            }
            if (Session[SessionCollection.ReportValue] != null)
            {
                BasicInfoReportsParameter aBasicInfoReportsParameter =(BasicInfoReportsParameter)Session[SessionCollection.ReportValue];
                GenerateReport(aBasicInfoReportsParameter);
            } 
        }


        private void GenerateReport(BasicInfoReportsParameter aBasicInfoReportsParameter)
        {

            string reportValue = aBasicInfoReportsParameter.ReportValue;

            switch (reportValue)
            {
                case "SalesOrder":
                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];

                        SalesOrderReportsDAL _aDal = new SalesOrderReportsDAL();
                        UtilityDAL aUtilityDAL = new UtilityDAL();

                        ds = new DataSet();
                        DataTable dtSalesOrder = _aDal.GetOrderDetailsForReport(aRelatedReport.SalesOrderProcessedMasterId,aRelatedReport.IsTrade).Copy();
                        dtSalesOrder.TableName = "dtDailyForSingleShed";
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        dtReportHeader.TableName = "dtReportHeader";

                        ds.DataSetName = "SalesOrder";
                        ds.Tables.Add(dtSalesOrder);
                        ds.Tables.Add(dtReportHeader);
                        var d = ds;
                        if (aRelatedReport.IsTrade)
                        {

                            ShowReport(ds, "rpt_TradeSalesOrder.rpt");
                        }
                        else
                        {

                            ShowReport(ds, "rpt_SalesOrder.rpt");
                        }
                        
                        
                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;

                case "FalseSalesOrder":
                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];

                        SalesOrderReportsDAL _aDal = new SalesOrderReportsDAL();
                        UtilityDAL aUtilityDAL = new UtilityDAL();

                        ds = new DataSet();
                        DataTable dtSalesOrderFalse = _aDal.GetOrderDetailsForReport_False(aRelatedReport.FalseSalesOrderProcessedMasterId ).Copy();
                        dtSalesOrderFalse.TableName = "dtSalesOrderFalse";
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        dtReportHeader.TableName = "dtReportHeader";

                        ds.DataSetName = "dsSalesOrderFalse";
                        ds.Tables.Add(dtSalesOrderFalse);
                        ds.Tables.Add(dtReportHeader);

                        // WriteXSD(ds, "SalesOrderFalse.xsd");
                        ShowReport(ds, "rpt_SalesOrder_False.rpt");


                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;
                case "SeePaymentDetails":
                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];

                        SalesOrderReportsDAL _aDal = new SalesOrderReportsDAL();
                        UtilityDAL aUtilityDAL = new UtilityDAL();

                        ds = new DataSet();
                        DataTable dtSalesOrderFalse = _aDal.GetCustomerPayment(Convert.ToInt32(aRelatedReport.SalesOrderProcessedMasterId)).Copy();
                        dtSalesOrderFalse.TableName = "dtCustomerPayment";
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        dtReportHeader.TableName = "dtReportHeader";

                        ds.DataSetName = "dsCustomerPayment";
                        ds.Tables.Add(dtSalesOrderFalse);
                        ds.Tables.Add(dtReportHeader);

                        // WriteXSD(ds, "CustomerPayment.xsd");
                        ShowReport(ds, "rpt_CustomerPayment.rpt");


                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;
                case "MoneyReceipt":
                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];

                        SalesOrderReportsDAL _aDal = new SalesOrderReportsDAL();
                        UtilityDAL aUtilityDAL = new UtilityDAL();

                        ds = new DataSet();
                        DataTable dtSalesOrderFalse = _aDal.GetMoneyReceipt(aRelatedReport.SalesOrderProcessedMasterId).Copy();
                        dtSalesOrderFalse.TableName = "dtCustomerMonley";
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        dtReportHeader.TableName = "dtReportHeader";

                        ds.DataSetName = "dsCustomerMonley";
                        ds.Tables.Add(dtSalesOrderFalse);
                        ds.Tables.Add(dtReportHeader);

                        // WriteXSD(ds, "CustomerMoneyRcept.xsd");
                        ShowReport(ds, "MoneyReceipt.rpt");


                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;
                case "DailySalesOrderSummary":
                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];

                        SalesOrderReportsDAL _aDal = new SalesOrderReportsDAL();

                        ds = new DataSet();
                        DataTable dtSalesOrder = _aDal.GetAllOrderSummaryForReport(aRelatedReport.FromDate,aRelatedReport.ToDate,aRelatedReport.GroupId).Copy();
                        dtSalesOrder.TableName = "DailySalesOrderSummary";

                        ds.DataSetName = "SalesOrder";
                        ds.Tables.Add(dtSalesOrder);
                        var d = ds;

                       // WriteXSD(ds, "DailySalesOrderSummary.xsd");

                        ShowReport(ds, "DailySalesOrderSummary.rpt");
                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;

                case "DailyDeliverySummary":
                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];

                        SalesOrderReportsDAL _aDal = new SalesOrderReportsDAL();
                        CrystalDecisions.Shared.ParameterField parameter_PF = new CrystalDecisions.Shared.ParameterField();
                        CrystalDecisions.Shared.ParameterDiscreteValue parameter_PV = new CrystalDecisions.Shared.ParameterDiscreteValue();
                        CrystalDecisions.Shared.ParameterFields parameter_PAF = new CrystalDecisions.Shared.ParameterFields();

                        ds = new DataSet();
                        DataTable dtSalesOrder = _aDal.GetAllDeliverySummaryForReport(aRelatedReport.FromDate, aRelatedReport.ToDate, aRelatedReport.GroupId, aRelatedReport.CustomerId,aRelatedReport.IsEcommerce).Copy();
                        dtSalesOrder.TableName = "dtDailyChallanSummary";

                        ds.DataSetName = "dsDailyChallanSummary";
                        ds.Tables.Add(dtSalesOrder);
                        var d = ds;

                        parameter_PF.ParameterFieldName = "reportName";
                        parameter_PV.Value = "Delivery Wise Invoice";        // THE VALUE WHICH IS TO BE SHOWN.
                        parameter_PF.CurrentValues.Add(parameter_PV);
                        parameter_PAF.Add(parameter_PF);                     // ADD ALL THE FIELDS.

                         //WriteXSD(ds, "DailyChallanSummary.xsd");

                        if (aRelatedReport.IsEcommerce)
                        {
                            ShowReport(ds, "rptDailyDeliverySummary_Ecommerce.rpt", 0, parameter_PAF);

                        }
                        else
                        {
                            ShowReport(ds, "rptDailyDeliverySummary.rpt", 0, parameter_PAF);

                        }

                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;

                case "DailySalesOrderSummaryProductWise":
                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];

                        SalesOrderReportsDAL _aDal = new SalesOrderReportsDAL();

                        ds = new DataSet();
                        DataTable dtSalesOrder = _aDal.GetAllOrderSummaryForReport(aRelatedReport.FromDate,aRelatedReport.ToDate,aRelatedReport.GroupId).Copy();
                        dtSalesOrder.TableName = "DailySalesOrderSummary";

                        ds.DataSetName = "SalesOrder";
                        ds.Tables.Add(dtSalesOrder);
                        var d = ds;

                        // WriteXSD(ds, "DailySalesOrderSummary.xsd");

                        ShowReport(ds, "DailySalesOrderSummaryProductWise.rpt");
                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;

                case "DateToDatePaymentCollection":
                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];

                        SalesOrderReportsDAL _aDal = new SalesOrderReportsDAL();

                        ds = new DataSet();
                        DataTable dtSalesOrder = _aDal.GetAllPaymentCollectionReport(aRelatedReport.FromDate,aRelatedReport.ToDate,aRelatedReport.GroupId).Copy();
                        dtSalesOrder.TableName = "DateToDatePaymentCollection";

                        ds.DataSetName = "PaymentCollection";
                        ds.Tables.Add(dtSalesOrder);
                        var d = ds;

                        // WriteXSD(ds, "DateToDatePaymentCollection.xsd");

                       ShowReport(ds, "DateToDatePaymentCollection.rpt");
                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;

                case "CustomerWisePaymentCollection":
                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];

                        SalesOrderReportsDAL _aDal = new SalesOrderReportsDAL();

                        ds = new DataSet();
                        DataTable dtSalesOrder = _aDal.GetAllPaymentCollectionReport(aRelatedReport.CustomerId, aRelatedReport.FromDate, aRelatedReport.ToDate,aRelatedReport.GroupId).Copy();
                        dtSalesOrder.TableName = "DateToDatePaymentCollection";

                        ds.DataSetName = "PaymentCollection";
                        ds.Tables.Add(dtSalesOrder);
                        var d = ds;

                        // WriteXSD(ds, "DateToDatePaymentCollection.xsd");

                        ShowReport(ds, "CustomerWiseDateToDatePaymentCollection.rpt");
                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;

                case "DailyStockSummery":
                    
                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];

                        SalesOrderReportsDAL _aDal = new SalesOrderReportsDAL();

                        ds = new DataSet();
                        DataTable dtStock = _aDal.GetProductStock(aRelatedReport.WareHouseId, aRelatedReport.FromDate,aRelatedReport.GroupId, aRelatedReport.ProductId).Copy();
                        dtStock.TableName = "DateWiseStock";
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        dtReportHeader.TableName = "dtReportHeader";
                        ds.DataSetName = "DailyStockLedger";
                        ds.Tables.Add(dtStock);
                        ds.Tables.Add(dtReportHeader);
                        var d = ds;

                      //  WriteXSD(ds, "DateWiseStock.xsd");

                     ShowReport(ds, "DateWiseStockReport.rpt");
                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;

                case "DailyTradeStockSummery":

                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];

                        SalesOrderReportsDAL _aDal = new SalesOrderReportsDAL();

                        ds = new DataSet();
                        DataTable dtStock = _aDal.GetTradeProductStock(aRelatedReport.WareHouseId, aRelatedReport.FromDate, aRelatedReport.GroupId, aRelatedReport.ProductId).Copy();
                        dtStock.TableName = "DateWiseStock";
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        dtReportHeader.TableName = "dtReportHeader";
                        ds.DataSetName = "DailyStockLedger";
                        ds.Tables.Add(dtStock);
                        ds.Tables.Add(dtReportHeader);
                        var d = ds;

                      //  WriteXSD(ds, "DateWiseStock.xsd");

                        ShowReport(ds, "DateWiseTradeStockReport.rpt");
                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;
                case "MonthlyStockSummery":

                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];

                        SalesOrderReportsDAL _aDal = new SalesOrderReportsDAL();

                        ds = new DataSet();
                        DataTable dtStock = _aDal.GetProductStockMonthWise(aRelatedReport.WareHouseId, aRelatedReport.StockMonth,aRelatedReport.GroupId , aRelatedReport.ProductId).Copy();
                        dtStock.TableName = "MonthWiseStock";
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        ds.DataSetName = "StockCollection";
                        ds.Tables.Add(dtStock);
                        ds.Tables.Add(dtReportHeader);


                        // WriteXSD(ds, "MonthWiseStock.xsd");

                         ShowReport(ds, "MonthWiseStockReportNoGroup.rpt");
                       
                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;
                case "MonthlyTradeStockSummery":

                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];

                        SalesOrderReportsDAL _aDal = new SalesOrderReportsDAL();

                        ds = new DataSet();
                        DataTable dtStock = _aDal.GetTradeProductStockMonthWise(aRelatedReport.WareHouseId, aRelatedReport.StockMonth, aRelatedReport.GroupId, aRelatedReport.ProductId).Copy();
                        dtStock.TableName = "MonthWiseStock";
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        ds.DataSetName = "StockCollection";
                        ds.Tables.Add(dtStock);
                        ds.Tables.Add(dtReportHeader);


                        // WriteXSD(ds, "MonthWiseStock.xsd");

                        ShowReport(ds, "MonthWiseTradeStockReportNoGroup.rpt");

                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;

                case "PurchaseOrderInvoice":
                    try
                    {
                        PurchaseOrderReportParameter aRelatedReport = (PurchaseOrderReportParameter)Session[SessionCollection.ReportParam];
                        PurchaseOrderDAL aOrderDal = new PurchaseOrderDAL();
                        DataTable inv = aOrderDal.GetPurchaseOrderInvoice(aRelatedReport.PurchaseOrderID).Copy();
                        inv.TableName = "INVOICE";

                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        dtReportHeader.TableName = "RptHeader";
                        ds = new DataSet();
                        ds.Tables.Add(inv);
                        ds.Tables.Add(dtReportHeader);
                        ds.DataSetName = "PurchaseInvoice";
                        //WriteXSD(ds, "PurchaseInvoice.xsd");
                        ShowReport(ds, "PurchaseInvoice.rpt");
                    }
                    catch (Exception exception)
                    {
                        
                        throw exception;
                    }
                    break;


                case "PurchaseOrderReport":
                    try
                    {
                        PurchaseOrderReportParameter aRelatedReport = (PurchaseOrderReportParameter)Session[SessionCollection.ReportParam];
                        PurchaseOrderDAL aOrderDal = new PurchaseOrderDAL();

                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        DataTable purchaseOrder = aOrderDal.PurchaseOrderReport(aRelatedReport.CompanyId,
                            aRelatedReport.LocationId, aRelatedReport.WarehouseId, aRelatedReport.SupplierId,
                            aRelatedReport.FromDate, aRelatedReport.ToDate, aRelatedReport.RStatus).Copy();
                        dtReportHeader.TableName = "RptHeader";
                        purchaseOrder.TableName = "PurchaseOrder";
                        ds = new DataSet();
                        ds.Tables.Add(purchaseOrder);
                        ds.Tables.Add(dtReportHeader);
                        ds.DataSetName = "PurchaseOrderReport";
                       // WriteXSD(ds, "PurchaseOrderReport.xsd");
                        ShowReport(ds, "rpt_PurchaseOrderReport.rpt");
                    }
                    catch (Exception ex)
                    {
                        
                        throw ex;
                    }
                    break;

                case "RequisitionReport":
                    try
                    {
                        RequisitionReport aRelatedReport = (RequisitionReport)Session[SessionCollection.ReportParam];

                        RequisitionReportDAL _aDal = new RequisitionReportDAL();
                        UtilityDAL aUtilityDAL = new UtilityDAL();

                        ds = new DataSet();
                        DataTable dtRequisition = _aDal.GetRequisitionReport(aRelatedReport.CompanyId, aRelatedReport.LocationId, aRelatedReport.WarehouseId, aRelatedReport.FromDate, aRelatedReport.ToDate, aRelatedReport.RStatus).Copy();
                        dtRequisition.TableName = "dtRequisition";
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        dtReportHeader.TableName = "dtReportHeader";

                        ds.DataSetName = "dsRequisition";
                        ds.Tables.Add(dtRequisition);
                        ds.Tables.Add(dtReportHeader);
                        // WriteXSD(ds, "RequisitionListReport.xsd");
                        ShowReport(ds, "rpt_RequisitionListReport.rpt");



                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;
                case "Challan":

                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];

                        ChallanProcessedDAL _aDal = new ChallanProcessedDAL();
                        UtilityDAL aUtilityDAL = new UtilityDAL();
                        ds = new DataSet();
                        DataTable dtSalesOrder = _aDal.GetGeneratedChallanDetailsForPrint(aRelatedReport.SalesOrderChallanMasterId,aRelatedReport.IsTrade).Copy();
                        dtSalesOrder.TableName = "challanDetails";
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        dtReportHeader.TableName = "dtReportHeader";

                        ds.DataSetName = "challan";
                        ds.Tables.Add(dtSalesOrder);
                        ds.Tables.Add(dtReportHeader);
                        var d = ds;
                        // var groupId = aRelatedReport.GroupId;
                        
                        //WriteXSD(ds, "ChallanDetailsData.xsd");

                        ShowReport(ds, "rpt_challan_Processed.rpt");
                      
                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;


                case "StockTransferIssueChallan": 

                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];

                        StockTransferDAL _aDal = new StockTransferDAL(); 
                        UtilityDAL aUtilityDAL = new UtilityDAL();
                        ds = new DataSet();
                        DataTable dtSalesOrder = _aDal.GetStockTransferIssueChallanForPrint(aRelatedReport.StockTreansferIssueId).Copy();  
                        dtSalesOrder.TableName = "challanDetails";
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        dtReportHeader.TableName = "dtReportHeader";

                        ds.DataSetName = "challan";
                        ds.Tables.Add(dtSalesOrder);
                        ds.Tables.Add(dtReportHeader);

                       // WriteXSD(ds, "StockTransferIssueChallan.xsd");

                        ShowReport(ds, "StockTransferIssueChallan.rpt");

                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;




                case "SRPaySlip":

                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];

                        SalesRepresentativeDAL _aDal = new SalesRepresentativeDAL();
                        UtilityDAL aUtilityDAL = new UtilityDAL();
                        ds = new DataSet();
                        DataTable dtSRPayslip = _aDal.GetSRPaySlipData(aRelatedReport.CustomerId, aRelatedReport.FromDate, aRelatedReport.ToDate, aRelatedReport.SRId).Copy();
                        dtSRPayslip.TableName = "dtSRPayslip";
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        dtReportHeader.TableName = "dtReportHeader";

                        ds.DataSetName = "SRPayslip";
                        ds.Tables.Add(dtSRPayslip);
                        ds.Tables.Add(dtReportHeader);
                      

                        //WriteXSD(ds, "DSSrPayslip.xsd");

                        ShowReport(ds, "rpt_SrPayslip.rpt");

                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;


                case "DeliveryTopSheet":

                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];

                        TopSheetDAL _aDal = new TopSheetDAL();
                        UtilityDAL aUtilityDAL = new UtilityDAL();
                        ds = new DataSet();

                        DataTable dt1 = _aDal.GenerateTopSheet(aRelatedReport.SalesOrderChallanMasterId,
                            aRelatedReport.StockTreansferIssueId).Copy();

                        
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        dt1.TableName = "TopSheet";
                        dtReportHeader.TableName = "Header";

                        ds.DataSetName = "TopSheet";
                        
                        ds.Tables.Add(dt1);
                        ds.Tables.Add(dtReportHeader);
                        //var d = ds;
                        // var groupId = aRelatedReport.GroupId;

                        //WriteXSD(ds, "TopSheet.xsd");

                       ShowReport(ds, "rpt_DeliveryTopSheet.rpt");

                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;

                case "Mrr":

                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];

                        MrRDAL _aDal = new MrRDAL();
                        UtilityDAL aUtilityDAL = new UtilityDAL();
                        ds = new DataSet();
                        DataTable dtMrrDetails = _aDal.GetMrrDetailsForPrint(aRelatedReport.MrrMasterId).Copy();
                        dtMrrDetails.TableName = "mrrDetails";
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        dtReportHeader.TableName = "dtReportHeader";
                        ds.DataSetName = "Mrr";
                        ds.Tables.Add(dtMrrDetails);
                        ds.Tables.Add(dtReportHeader);
                        var d = ds;
                        //WriteXSD(ds, "MrrDetailsData.xsd");
                        
                        ShowReport(ds, "rpt_Mrr.rpt");
                        

                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;

                case "BirdRequisitionInvoice":
                    try
                    {
                        RequisitionReport aRelatedReport = (RequisitionReport)Session[SessionCollection.ReportParam];

                        RequisitionReportDAL _aDal = new RequisitionReportDAL();
                        UtilityDAL aUtilityDAL = new UtilityDAL();
                        ds = new DataSet();
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        DataTable dt2 = _aDal.GetRequisitionInvoiceData(aRelatedReport.RequisitionID).Copy();

                        dtReportHeader.TableName = "dtReportHeader";
                        dt2.TableName = "ReqTable";
                        ds.Tables.Add(dt2);
                        ds.Tables.Add(dtReportHeader);
                        ds.DataSetName = "RequisitionInv";
                        //WriteXSD(ds, "RequisitionInv.xsd");
                        ShowReport(ds, "rpt_RequisitionInvoice.rpt");
                    }
                    catch (Exception ex)
                    {
                            
                        throw ex;
                    }
                    break;
                case "LiveBirdDressInvoice":
                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];

                        LiveBirdDressDAL _aDal = new LiveBirdDressDAL();
                        UtilityDAL aUtilityDAL = new UtilityDAL();
                        ds = new DataSet();
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        DataTable dt2 = _aDal.GetLiveBirdDressReportInvoice(aRelatedReport.DressBirdNo).Copy();

                        dtReportHeader.TableName = "dtReportHeader";
                        dt2.TableName = "LiveBirdDress";
                        ds.Tables.Add(dt2);
                        ds.Tables.Add(dtReportHeader);
                        ds.DataSetName = "LiveBirdDress";
                        //WriteXSD(ds, "LiveBirdDress.xsd");
                        ShowReport(ds, "rpt_LiveBirdDress.rpt");
                    }
                    catch (Exception ex)
                    {

                        throw ex;
                    }
                    break;
                case "PickingRequestSummary":
                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];
                       var empId= Session["loginuserempid"].ToString();
                        PickingProcessDAL _aDal = new PickingProcessDAL();
                        UtilityDAL aUtilityDAL = new UtilityDAL();
                        ds = new DataSet();
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        DataTable dt2 = _aDal.GetPickingSummary(aRelatedReport.SalesOrderChallanMasterId,aRelatedReport.StockTreansferIssueId, empId).Copy();

                        dtReportHeader.TableName = "dtReportHeader";
                        dt2.TableName = "PickingSummary";
                        ds.Tables.Add(dt2);
                        ds.Tables.Add(dtReportHeader);
                        ds.DataSetName = "PickingSummary";
                        //WriteXSD(ds, "PickingSummary.xsd");
                        ShowReport(ds, "rpt_PickingRequestSummary.rpt");
                    }
                    catch (Exception ex)
                    {

                        throw ex;
                    }
                    break;

                case "PickingRequestDetails":
                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];
                        var empId = Session["loginuserempid"].ToString();
                        PickingProcessDAL _aDal = new PickingProcessDAL();
                        UtilityDAL aUtilityDAL = new UtilityDAL();
                        ds = new DataSet();
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        DataTable dt2 = _aDal.GetPickingDetails(aRelatedReport.SalesOrderChallanMasterId, aRelatedReport.StockTreansferIssueId, empId).Copy();

                        dtReportHeader.TableName = "dtReportHeader";
                        dt2.TableName = "PickingDetails";
                        ds.Tables.Add(dt2);
                        ds.Tables.Add(dtReportHeader);
                        ds.DataSetName = "PickingDetails";
                        //WriteXSD(ds, "PickingDetails.xsd");
                        ShowReport(ds, "rpt_PickingRequestDetails.rpt");
                    }
                    catch (Exception ex)
                    {

                        throw ex;
                    }
                    break;
                case "DailyChallanSummary":
                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];

                        SalesOrderReportsDAL _aDal = new SalesOrderReportsDAL();
                        CrystalDecisions.Shared.ParameterField parameter_PF = new CrystalDecisions.Shared.ParameterField();
                        CrystalDecisions.Shared.ParameterDiscreteValue parameter_PV = new CrystalDecisions.Shared.ParameterDiscreteValue();
                        CrystalDecisions.Shared.ParameterFields parameter_PAF = new CrystalDecisions.Shared.ParameterFields();

                        ds = new DataSet();
                        DataTable dtSalesOrder = _aDal.GetAllChallanSummaryForReport(aRelatedReport.FromDate, aRelatedReport.ToDate, aRelatedReport.GroupId, aRelatedReport.CustomerId,aRelatedReport.IsEcommerce).Copy();
                        dtSalesOrder.TableName = "dtDailyChallanSummary";

                        ds.DataSetName = "dsDailyChallanSummary";
                        ds.Tables.Add(dtSalesOrder);
                        var d = ds;
                        parameter_PF.ParameterFieldName = "reportName";
                        parameter_PV.Value = "Challan Wise Invoice";        // THE VALUE WHICH IS TO BE SHOWN.
                        parameter_PF.CurrentValues.Add(parameter_PV);
                        parameter_PAF.Add(parameter_PF);                     // ADD ALL THE FIELDS.
                        //WriteXSD(ds, "DailyChallanSummary.xsd");

                        if (aRelatedReport.IsEcommerce)
                        {
                            ShowReport(ds, "rptDailyChallanSummary_Ecommerce.rpt", 0, parameter_PAF);

                        }
                        else
                        {
                            ShowReport(ds, "rptDailyChallanSummary.rpt",0, parameter_PAF);

                        }


                        
                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;


                case "DeliveryReport_ChallanPrint":
                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];

                        SalesOrderReportsDAL _aDal = new SalesOrderReportsDAL();
                        CrystalDecisions.Shared.ParameterField parameter_PF = new CrystalDecisions.Shared.ParameterField();
                        CrystalDecisions.Shared.ParameterDiscreteValue parameter_PV = new CrystalDecisions.Shared.ParameterDiscreteValue();
                        CrystalDecisions.Shared.ParameterFields parameter_PAF = new CrystalDecisions.Shared.ParameterFields();

                        ds = new DataSet();
                        DataTable dtSalesOrder = _aDal.GetDeliveryReport_ChallanPrint(aRelatedReport.ChallanMasterId).Copy(); 
                        dtSalesOrder.TableName = "dtDailyChallanSummary";

                        ds.DataSetName = "dsDailyChallanSummary";
                        ds.Tables.Add(dtSalesOrder);
                        var d = ds;
                        parameter_PF.ParameterFieldName = "reportName";
                        parameter_PV.Value = "Challan Wise Invoice";        // THE VALUE WHICH IS TO BE SHOWN.
                        parameter_PF.CurrentValues.Add(parameter_PV);
                        parameter_PAF.Add(parameter_PF);                     // ADD ALL THE FIELDS.
                        //WriteXSD(ds, "DailyChallanSummary.xsd");rptDailyChallanSummary_Ecommerce.rpt

                        ShowReport(ds, "rptDailyChallanSummary.rpt", 0, parameter_PAF);

                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;

                case "ChallanWiseDelivery":
                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];

                        ChallanProcessedDAL _aDal = new ChallanProcessedDAL();

                        ds = new DataSet();
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();

                        System.Data.DataColumn newColumn = new System.Data.DataColumn("LoggedInUser", typeof(System.String));
                        newColumn.DefaultValue = Session[SessionCollection.UserInfo].ToString();
                        dtReportHeader.Columns.Add(newColumn);

                        DataTable challanWiseDeliveryDt = _aDal.ChallanWiseDeliverySummary(aRelatedReport.CustomerId,aRelatedReport.FromDate,aRelatedReport.ToDate).Copy();
                        
                        challanWiseDeliveryDt.TableName = "challanWiseDeliveryDt";
                        dtReportHeader.TableName = "dtReportHeader";

                        ds.DataSetName = "challanWiseDeliveryDs";
                        ds.Tables.Add(challanWiseDeliveryDt);
                        ds.Tables.Add(dtReportHeader);

                       // WriteXSD(ds, "ChallanWiseDelivery.xsd");

                          ShowReport(ds, "rptChallanWiseDelivery.rpt");


                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;

                case "DeliveryConfirmationSummary":
                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];

                        ChallanProcessedDAL _aDal = new ChallanProcessedDAL();

                        ds = new DataSet();

                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        System.Data.DataColumn newColumn = new System.Data.DataColumn("LoggedInUser", typeof(System.String));
                        newColumn.DefaultValue = Session[SessionCollection.UserInfo].ToString();
                        dtReportHeader.Columns.Add(newColumn);

                       
                        DataTable challanWiseDeliveryDt = _aDal.CustomerWiseDeliveryConfirmationSummary(aRelatedReport.CustomerId, aRelatedReport.FromDate, aRelatedReport.ToDate).Copy();

                        challanWiseDeliveryDt.TableName = "challanWiseDeliveryDt";
                        dtReportHeader.TableName = "dtReportHeader";

                        ds.DataSetName = "challanWiseDeliveryDs";
                        ds.Tables.Add(challanWiseDeliveryDt);
                        ds.Tables.Add(dtReportHeader);

                        //WriteXSD(ds, "ChallanWiseDelivery.xsd");

                        ShowReport(ds, "rptDeliveryConfirmationSummary.rpt");


                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;

                case "TradeChallanInvoice":
                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];

                        SalesOrderReportsDAL _aDal = new SalesOrderReportsDAL();
                        CrystalDecisions.Shared.ParameterField parameter_PF =new CrystalDecisions.Shared.ParameterField();
                        CrystalDecisions.Shared.ParameterDiscreteValue parameter_PV = new CrystalDecisions.Shared.ParameterDiscreteValue();
                        CrystalDecisions.Shared.ParameterFields parameter_PAF = new CrystalDecisions.Shared.ParameterFields();

                        ds = new DataSet();
                        DataTable dtSalesOrder = _aDal.GetTradeChallanInvoice(aRelatedReport.FromDate, aRelatedReport.ToDate, aRelatedReport.GroupId, aRelatedReport.CustomerId, aRelatedReport.IsEcommerce).Copy();
                        dtSalesOrder.TableName = "dtDailyChallanSummary";

                        ds.DataSetName = "dsDailyChallanSummary";
                        ds.Tables.Add(dtSalesOrder);
                        var d = ds;
                        parameter_PF.ParameterFieldName = "reportName";
                        parameter_PV.Value = "Trade Challan Wise Invoice";        // THE VALUE WHICH IS TO BE SHOWN.
                        parameter_PF.CurrentValues.Add(parameter_PV);
                        parameter_PAF.Add(parameter_PF);                     // ADD ALL THE FIELDS.

                        //WriteXSD(ds, "DailyChallanSummary.xsd");rptDailyChallanSummary_Ecommerce.rpt

                        if (aRelatedReport.IsEcommerce)
                        {
                            ShowReport(ds, "rptDailyChallanSummary_Ecommerce.rpt", 0, parameter_PAF);

                        }
                        else
                        {
                            ShowReport(ds, "rptDailyChallanSummary.rpt",0, parameter_PAF);

                        }



                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;

                case "TradeDeliveryInvoice":
                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];

                        SalesOrderReportsDAL _aDal = new SalesOrderReportsDAL();
                        CrystalDecisions.Shared.ParameterField parameter_PF = new CrystalDecisions.Shared.ParameterField();
                        CrystalDecisions.Shared.ParameterDiscreteValue parameter_PV = new CrystalDecisions.Shared.ParameterDiscreteValue();
                        CrystalDecisions.Shared.ParameterFields parameter_PAF = new CrystalDecisions.Shared.ParameterFields();

                        ds = new DataSet();
                        DataTable dtSalesOrder = _aDal.GetTradeDeliveryInvoice(aRelatedReport.FromDate, aRelatedReport.ToDate, aRelatedReport.GroupId, aRelatedReport.CustomerId, aRelatedReport.IsEcommerce).Copy();
                        dtSalesOrder.TableName = "dtDailyChallanSummary";

                        ds.DataSetName = "dsDailyChallanSummary";
                        ds.Tables.Add(dtSalesOrder);
                        var d = ds;
                        parameter_PF.ParameterFieldName = "reportName";
                        parameter_PV.Value = "Trade Delivery Wise Invoice";        // THE VALUE WHICH IS TO BE SHOWN.
                        parameter_PF.CurrentValues.Add(parameter_PV);
                        parameter_PAF.Add(parameter_PF);                     // ADD ALL THE FIELDS.

                        //WriteXSD(ds, "DailyChallanSummary.xsd");rptDailyChallanSummary_Ecommerce.rpt

                        if (aRelatedReport.IsEcommerce)
                        {
                            ShowReport(ds, "rptDailyDeliverySummary_Ecommerce.rpt", 0, parameter_PAF);

                        }
                        else
                        {
                            ShowReport(ds, "rptDailyDeliverySummary.rpt", 0, parameter_PAF);

                        }

                       

                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;
                case "RetailOrder":

                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];

                        RetailSalesDAL _aDal = new RetailSalesDAL();
                        UtilityDAL aUtilityDAL = new UtilityDAL();
                        ds = new DataSet();
                        DataTable dtSalesOrder = _aDal.GetRetailOrderForPrint(aRelatedReport.soId).Copy();
                        dtSalesOrder.TableName = "RetailOrderDetails";
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        dtReportHeader.TableName = "dtReportHeader";

                        ds.DataSetName = "RetailOrder";
                        ds.Tables.Add(dtSalesOrder);
                        ds.Tables.Add(dtReportHeader);
                        var d = ds;
                        // var groupId = aRelatedReport.GroupId;

                        //WriteXSD(ds, "RetailOrderDetailsData.xsd");

                        ShowReportPDF(ds, "ReportOrder.rpt");

                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;
                case "RestaturantOrder":

                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];

                        RestaturantSalesDAL _aDal = new RestaturantSalesDAL();
                        UtilityDAL aUtilityDAL = new UtilityDAL();
                        ds = new DataSet();
                        DataTable dtSalesOrder = _aDal.GetRestaturantOrderForPrint(aRelatedReport.soId).Copy();
                        dtSalesOrder.TableName = "RestaturantOrderDetails";
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        dtReportHeader.TableName = "dtReportHeader";

                        ds.DataSetName = "RestaturantOrder";
                        ds.Tables.Add(dtSalesOrder);
                        ds.Tables.Add(dtReportHeader);
                        var d = ds;
                        // var groupId = aRelatedReport.GroupId;

                       // WriteXSD(ds, "RestaturantOrderDetailsData.xsd");

                        ShowReportPDF(ds, "RestaturantOrder.rpt");

                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;
                case "OwnConsumption":
                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];

                        OwnConsumptionRequisitionDAL _aDal = new OwnConsumptionRequisitionDAL();
                        UtilityDAL aUtilityDAL = new UtilityDAL();

                        ds = new DataSet();
                        DataTable dtSalesOrder = _aDal.GetOwnConsumptionSalesOrderInvoice(aRelatedReport.SalesOrderProcessedMasterId, aRelatedReport.IsTrade).Copy();
                        dtSalesOrder.TableName = "dtOwnConsumption";
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        dtReportHeader.TableName = "dtReportHeader";

                        ds.DataSetName = "OwnConsumption";
                        ds.Tables.Add(dtSalesOrder);
                        ds.Tables.Add(dtReportHeader);
                        var d = ds;
                        //WriteXSD(ds, "OwnConsumptionData.xsd");
                        ShowReport(ds, "rptOwnConsumptionSalesInvoice.rpt");

                        //if (aRelatedReport.IsTrade)
                        //{

                        //    ShowReport(ds, "rpt_TradeSalesOrder.rpt");
                        //}
                        //else
                        //{

                        //    ShowReport(ds, "rpt_SalesOrder.rpt");
                        //}


                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;
                case "VoucherPrint":
                    try
                    {
                        VoucherPrintReport aRelatedReport = (VoucherPrintReport)Session[SessionCollection.ReportParam];

                        JournalVoucherDal _aDal = new JournalVoucherDal();
                        UtilityDAL aUtilityDAL = new UtilityDAL();

                        ds = new DataSet();
                        DataTable dtVoucherPrint = _aDal.GetVoucherPrintReport(aRelatedReport.VoucherMasterId).Copy();
                        dtVoucherPrint.TableName = "dtVoucherPrint";
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        dtReportHeader.TableName = "dtReportHeader";

                        ds.DataSetName = "VoucherPrint";
                        ds.Tables.Add(dtVoucherPrint);
                        ds.Tables.Add(dtReportHeader);
                        var d = ds;
                        //WriteXSD(ds, "VoucherPrint.xsd");
                        ShowReport(ds, "rptVoucherPrint.rpt");
                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;

                case "DeliveryReport_ReturnChallan":
                    try
                    {
                        VoucherPrintReport aRelatedReport = (VoucherPrintReport)Session[SessionCollection.ReportParam];

                        JournalVoucherDal _aDal = new JournalVoucherDal();
                        UtilityDAL aUtilityDAL = new UtilityDAL();

                        ds = new DataSet();
                        DataTable dtVoucherPrint = _aDal.GetDeliveryReport_ReturnChallan(aRelatedReport.SalesOrderChallanMasterId, aRelatedReport.Category).Copy();
                        dtVoucherPrint.TableName = "dtVoucherPrint";
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        dtReportHeader.TableName = "dtReportHeader";

                        ds.DataSetName = "VoucherPrint";
                        ds.Tables.Add(dtVoucherPrint);
                        ds.Tables.Add(dtReportHeader);
                        var d = ds;
                        //WriteXSD(ds, "VoucherPrint.xsd");
                        ShowReport(ds, "rptVoucherPrint.rpt");
                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;

                case "PurchaseOrderOther":
                    try
                    {
                        PurchaseOrderOtherReport aRelatedReport = (PurchaseOrderOtherReport)Session[SessionCollection.ReportParam];

                        PurchaseOrderOtherDAL _aDal = new PurchaseOrderOtherDAL();
                        UtilityDAL aUtilityDAL = new UtilityDAL();

                        ds = new DataSet();
                        DataTable dtPOOPrint = _aDal.GetPurchaseOrderOtherReport(aRelatedReport.POMasterId).Copy();
                        dtPOOPrint.TableName = "dtPOOPrint";
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        dtReportHeader.TableName = "dtReportHeader";

                        ds.DataSetName = "POOPrint";
                        ds.Tables.Add(dtPOOPrint);
                        ds.Tables.Add(dtReportHeader);
                        var d = ds;
                        //WriteXSD(ds, "POOPrint.xsd");
                        ShowReport(ds, "rpt_PurchaseOrderOtherReport.rpt");
                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;

                case "PurchaseOrderOtherMultiple":
                    try
                    {
                        PurchaseOrderOtherReport aRelatedReport = (PurchaseOrderOtherReport)Session[SessionCollection.ReportParam];

                        PurchaseOrderOtherDAL _aDal = new PurchaseOrderOtherDAL();
                        UtilityDAL aUtilityDAL = new UtilityDAL();

                        ds = new DataSet();
                        DataTable dtPOOPrint = _aDal.GetPurchaseOrderOtherMultipleReport(aRelatedReport.PurchaseOrderMasterId).Copy();
                        dtPOOPrint.TableName = "dtPOOMultiplePrint";
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        dtReportHeader.TableName = "dtReportHeader";

                        ds.DataSetName = "POOMultiplePrint";
                        ds.Tables.Add(dtPOOPrint);
                        ds.Tables.Add(dtReportHeader);
                        var d = ds;
                         //WriteXSD(ds, "POOMultiplePrint.xsd");
                        ShowReport(ds, "rpt_PurchaseOrderOtherMultiplerint.rpt");
                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;
                case "MrrPrint":
                    try
                    {
                        ViewOthersMrr aRelatedReport = (ViewOthersMrr)Session[SessionCollection.ReportParam];

                        OthersMrrDal _aDal = new OthersMrrDal();
                        UtilityDAL aUtilityDAL = new UtilityDAL();

                        ds = new DataSet();
                        DataTable dtMrrPrint = _aDal.GetMrrPrintReport(aRelatedReport.MRRrOtherID).Copy();
                        dtMrrPrint.TableName = "dtMrrPrint";
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        dtReportHeader.TableName = "dtReportHeader";

                        ds.DataSetName = "MrrPrint";
                        ds.Tables.Add(dtMrrPrint);
                        ds.Tables.Add(dtReportHeader);
                        var d = ds;
                         //WriteXSD(ds, "MrrPrint.xsd");
                        ShowReport(ds, "rptMrrPrint.rpt");




                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;
                case "TrailBalance":
                    try
                    {
                        TrailBalanceReport aRelatedReport = (TrailBalanceReport)Session[SessionCollection.ReportParam];

                        JournalVoucherDal _aDal = new JournalVoucherDal();
                        UtilityDAL aUtilityDAL = new UtilityDAL();

                        ds = new DataSet();
                        DataTable dtTrailBalancePrint = _aDal.GetTrailBalancePrintReport(aRelatedReport.FromDate, aRelatedReport.ToDate, aRelatedReport.all, aRelatedReport.tran).Copy();
                        dtTrailBalancePrint.TableName = "dtTrailBalancePrint";
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        dtReportHeader.TableName = "dtReportHeader";

                        ds.DataSetName = "TrailBalancePrint";
                        ds.Tables.Add(dtTrailBalancePrint);
                        ds.Tables.Add(dtReportHeader);
                        var d = ds;
                        //WriteXSD(ds, "TrailBalancePrint.xsd");
                        ShowReport(ds, "rptTrailBalancePrint.rpt");




                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;
                case "PrintMultipleMRRReport":
                    try
                    {
                        DirrectMrr aRelatedReport = (DirrectMrr)Session[SessionCollection.ReportParam];

                        DirrectMrrDAL _aDal = new DirrectMrrDAL();
                        UtilityDAL aUtilityDAL = new UtilityDAL();

                        ds = new DataSet();
                        DataTable dtMrrPrint = _aDal.GetDirectMrrPrintReport(aRelatedReport.Ids).Copy();
                        dtMrrPrint.TableName = "dtDirectMrrPrint";
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        dtReportHeader.TableName = "dtReportHeader";

                        ds.DataSetName = "DirectMrrPrint";
                        ds.Tables.Add(dtMrrPrint);
                        ds.Tables.Add(dtReportHeader);
                        //WriteXSD(ds, "DirectMrrPrint.xsd");
                        ShowReport(ds, "rptDirectMrrPrint.rpt");

                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;

                case "PrintIndivualInvoice":
                    try
                    {
                        ViewInvoiceMaster aRelatedReport = (ViewInvoiceMaster)Session[SessionCollection.ReportParam];

                        InvoiceDAL _aDal = new InvoiceDAL();
                        UtilityDAL aUtilityDAL = new UtilityDAL();

                        ds = new DataSet();
                        DataTable dtMrrPrint = _aDal.GetIndivualInvoicePrintReport(aRelatedReport.InvoiceMasterID).Copy();
                        dtMrrPrint.TableName = "dtIndivualInvoice";
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        dtReportHeader.TableName = "dtReportHeader";

                        ds.DataSetName = "IndivualInvoice";
                        ds.Tables.Add(dtMrrPrint);
                        ds.Tables.Add(dtReportHeader);
                        // WriteXSD(ds, "IndivualInvoicePrint.xsd");
                        ShowReport(ds, "rptIndivualInvoicePrint.rpt");

                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;

                case "MultipleInvoiceReport":
                    try
                    {
                        ViewInvoiceMaster aRelatedReport = (ViewInvoiceMaster)Session[SessionCollection.ReportParam];

                        InvoiceDAL _aDal = new InvoiceDAL();
                        UtilityDAL aUtilityDAL = new UtilityDAL();

                        ds = new DataSet();
                        DataTable dtMrrPrint = _aDal.GetMultipleInvoiceReport(aRelatedReport.InvoiceNo).Copy();
                        dtMrrPrint.TableName = "dtMultipleInvoice";
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        dtReportHeader.TableName = "dtReportHeader";

                        ds.DataSetName = "MultipleInvoice";
                        ds.Tables.Add(dtMrrPrint);
                        ds.Tables.Add(dtReportHeader);
                       // WriteXSD(ds, "MultipleInvoicePrint.xsd");
                        ShowReport(ds, "rptMultipleInvoicePrint.rpt");

                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;
                case "DailyItemStockSummery":

                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];

                        ItemStockDAL _aDal = new ItemStockDAL();

                        ds = new DataSet();
                        DataTable dtStock = _aDal.GetItemStock(aRelatedReport.WareHouseId, aRelatedReport.FromDate, aRelatedReport.ToDate, aRelatedReport.GroupId, aRelatedReport.ProductId).Copy();
                        dtStock.TableName = "DateWiseStock";
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        dtReportHeader.TableName = "dtReportHeader";
                        ds.DataSetName = "DailyStockLedger";
                        ds.Tables.Add(dtStock);
                        ds.Tables.Add(dtReportHeader);
                        var d = ds;

                      // WriteXSD(ds, "DailyItemStockSummery.xsd");

                        ShowReport(ds, "DailyItemStockSummery.rpt");
                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;
                case "PurchaseRequisitionReport":
                    try
                    {
                        OtherPoRequisitionMaster aRelatedReport = (OtherPoRequisitionMaster)Session[SessionCollection.ReportParam];

                        OtherPoRequisitionDal _aDal = new OtherPoRequisitionDal();
                        UtilityDAL aUtilityDAL = new UtilityDAL();

                        ds = new DataSet();
                        DataTable dtRequisition = _aDal.GetRequisitionReport(aRelatedReport.LocationId, aRelatedReport.WarehouseId, aRelatedReport.FromDate, aRelatedReport.ToDate, aRelatedReport.RStatus).Copy();
                        dtRequisition.TableName = "dtRequisition";
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        dtReportHeader.TableName = "dtReportHeader";

                        ds.DataSetName = "dsRequisition";
                        ds.Tables.Add(dtRequisition);
                        ds.Tables.Add(dtReportHeader);
                        //  WriteXSD(ds, "PoRequisitionListReport.xsd");
                        ShowReport(ds, "rpt_PoRequisitionListReport.rpt");



                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;
                case "PoRequisitionSlip":
                    try
                    {
                        RequisitionReport aRelatedReport = (RequisitionReport)Session[SessionCollection.ReportParam];

                        OtherPoRequisitionDal _aDal = new OtherPoRequisitionDal();
                        UtilityDAL aUtilityDAL = new UtilityDAL();
                        ds = new DataSet();
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        DataTable dt2 = _aDal.GetRequisitionInvoiceData(aRelatedReport.RequisitionID).Copy();

                        dtReportHeader.TableName = "dtReportHeader";
                        dt2.TableName = "dtPoRequisition";
                        ds.DataSetName = "PoRequisitionSlip";
                        ds.Tables.Add(dt2);
                        ds.Tables.Add(dtReportHeader);
                       
                        //WriteXSD(ds, "PoRequisitionSlipNew.xsd");
                        ShowReport(ds, "rpt_PoRequisitionSlipNew.rpt");
                    }
                    catch (Exception ex)
                    {

                        throw ex;
                    }
                    break;
                case "IssueRequisitionReport":
                    try
                    {
                        IssueRequisition aRelatedReport = (IssueRequisition)Session[SessionCollection.ReportParam];

                        StockOutDal _aDal = new StockOutDal();
                        UtilityDAL aUtilityDAL = new UtilityDAL();

                        ds = new DataSet();
                        DataTable dtRequisition = _aDal.GetRequisitionReport(aRelatedReport.LocationId, aRelatedReport.WarehouseId, aRelatedReport.FromDate, aRelatedReport.ToDate, aRelatedReport.RStatus).Copy();
                        dtRequisition.TableName = "dtRequisition1";
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        dtReportHeader.TableName = "dtReportHeader";

                        ds.DataSetName = "dsRequisition1";
                        ds.Tables.Add(dtRequisition);
                        ds.Tables.Add(dtReportHeader);
                        //WriteXSD(ds, "IssueRequisitionListReport.xsd");
                        ShowReport(ds, "rpt_IssueRequisitionListReport.rpt");



                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;
                case "IssueRequisitionSlip":
                    try
                    {
                        RequisitionReport aRelatedReport = (RequisitionReport)Session[SessionCollection.ReportParam];

                        StockOutDal _aDal = new StockOutDal();
                        UtilityDAL aUtilityDAL = new UtilityDAL();
                        ds = new DataSet();
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        DataTable dt2 = _aDal.GetRequisitionInvoiceData(aRelatedReport.RequisitionID).Copy();

                        dtReportHeader.TableName = "dtReportHeader";
                        dt2.TableName = "ReqTable";
                        ds.Tables.Add(dt2);
                        ds.Tables.Add(dtReportHeader);
                        ds.DataSetName = "RequisitionInvPo";
                        // WriteXSD(ds, "PoRequisitionSlip.xsd");
                        ShowReport(ds, "rpt_IssueRequisitionSlip.rpt");
                    }
                    catch (Exception ex)
                    {

                        throw ex;
                    }
                    break;

                case "MultipleBirdInvoiceReport":
                    try
                    {
                        ViewInvoiceMaster aRelatedReport = (ViewInvoiceMaster)Session[SessionCollection.ReportParam];

                        InvoiceBirdPurchaseDal _aDal = new InvoiceBirdPurchaseDal();
                        UtilityDAL aUtilityDAL = new UtilityDAL();

                        ds = new DataSet();
                        DataTable dtMrrPrint = _aDal.GetMultipleInvoiceReport(aRelatedReport.InvoiceNo).Copy();
                        dtMrrPrint.TableName = "dtMultipleInvoice";
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        dtReportHeader.TableName = "dtReportHeader";

                        ds.DataSetName = "MultipleInvoice";
                        ds.Tables.Add(dtMrrPrint);
                        ds.Tables.Add(dtReportHeader);
                        //WriteXSD(ds, "MultipleInvoicePrint.xsd");
                        ShowReport(ds, "rptMultiBirdInvoicePrint.rpt");

                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;
                case "TradeMultipleInvoiceReport":
                    try
                    {
                        ViewTradeInvoiceMaster aRelatedReport = (ViewTradeInvoiceMaster)Session[SessionCollection.ReportParam];

                        TradeInvoiceDAL _aDal = new TradeInvoiceDAL();
                        UtilityDAL aUtilityDAL = new UtilityDAL();

                        ds = new DataSet();
                        DataTable dtMrrPrint = _aDal.GetMultipleTradeInvoiceReport(aRelatedReport.TradeInvoiceNo).Copy();
                        dtMrrPrint.TableName = "dtTradeMultipleInvoice";
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        dtReportHeader.TableName = "dtReportHeader";
                        ds.DataSetName = "TradeMultipleInvoice";
                        ds.Tables.Add(dtMrrPrint);
                        ds.Tables.Add(dtReportHeader);
                        //WriteXSD(ds, "TradeMultipleInvoicePrint.xsd");
                        ShowReport(ds, "MultipleTradeInvoiceReport.rpt");

                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;
                case "IssuePrint":
                    try
                    {
                        RequisitionReport aRelatedReport = (RequisitionReport)Session[SessionCollection.ReportParam];

                        StockOutDal _aDal = new StockOutDal();
                        UtilityDAL aUtilityDAL = new UtilityDAL();
                        ds = new DataSet();
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        DataTable dt2 = _aDal.IssuePrintData(aRelatedReport.RequisitionID).Copy();

                        dtReportHeader.TableName = "dtReportHeader";
                        dt2.TableName = "ReqTable";
                        ds.Tables.Add(dt2);
                        ds.Tables.Add(dtReportHeader);
                        ds.DataSetName = "RequisitionInvPo";
                        //WriteXSD(ds, "IssueSlipPrintFinal.xsd");
                        ShowReport(ds, "rpt_IssueSlipPrint.rpt");
                    }
                    catch (Exception ex)
                    {

                        throw ex;
                    }
                    break;

                case "BarcodePdf":
                    try
                    {

                        BarcodeDAL dal = new BarcodeDAL();
                        UtilityDAL aUtilityDAL = new UtilityDAL();
                        ds = new DataSet();
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        DataTable dtbar = dal.GetProductWithBarcode().Copy(); 

                        dtReportHeader.TableName = "dtReportHeader";
                        dtbar.TableName = "dtbar";
                        ds.Tables.Add(dtbar);
                        ds.Tables.Add(dtReportHeader);
                        ds.DataSetName = "dsProductBarcode";
                        //WriteXSD(ds, "ProductBarcode.xsd");
                        ShowReport(ds, "ProductBarcode.rpt");
                    }
                    catch (Exception ex)
                    {

                        throw ex;
                    }
                    break;
                case "PrintMultipleTradeMRR":
                    try
                    {
                        DirrectMrr aRelatedReport = (DirrectMrr)Session[SessionCollection.ReportParam];

                        TradeMrrDAL _aDal = new TradeMrrDAL();
                        UtilityDAL aUtilityDAL = new UtilityDAL();

                        ds = new DataSet();
                        DataTable dtMrrPrint = _aDal.PrintMultipleTradeMRR(aRelatedReport.Ids).Copy();
                        dtMrrPrint.TableName = "dtDirectMrrPrint";
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        dtReportHeader.TableName = "dtReportHeader";

                        ds.DataSetName = "DirectMrrPrint";
                        ds.Tables.Add(dtMrrPrint);
                        ds.Tables.Add(dtReportHeader);
                        //WriteXSD(ds, "DirectMrrPrint.xsd");
                        ShowReport(ds, "PrintMultipleTradeMRR.rpt");

                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;
                case "DailyTransferSummary":
                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];
                        StockTransferDAL _aDal = new StockTransferDAL();
                        UtilityDAL aUtilityDAL = new UtilityDAL();
                        ds = new DataSet();
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        DataTable dt2 = _aDal.GetTransferWiseInvoice(aRelatedReport.FromDate, aRelatedReport.ToDate, aRelatedReport.GroupId, aRelatedReport.LocationId, aRelatedReport.WareHouseId).Copy();

                        dtReportHeader.TableName = "dtReportHeader";
                        dt2.TableName = "TransferIssueSummary";
                        ds.Tables.Add(dt2);
                        ds.Tables.Add(dtReportHeader);
                        ds.DataSetName = "TransferIssueSummary";
                       // WriteXSD(ds, "TransferIssueSummary.xsd");
                        ShowReport(ds, "rptDailyTransferIssueSummary.rpt");
                    }
                    catch (Exception ex)
                    {

                        throw ex;
                    }
                    break;

                case "DailyTransferSummaryWithValue":
                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];
                        StockTransferDAL _aDal = new StockTransferDAL();
                        UtilityDAL aUtilityDAL = new UtilityDAL();
                        ds = new DataSet();
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        DataTable dt2 = _aDal.GetTransferWiseInvoiceWithValue(aRelatedReport.FromDate, aRelatedReport.ToDate, aRelatedReport.GroupId, aRelatedReport.LocationId, aRelatedReport.WareHouseId).Copy();

                        dtReportHeader.TableName = "dtReportHeader";
                        dt2.TableName = "TransferIssueSummaryWithValue";
                        ds.Tables.Add(dt2);
                        ds.Tables.Add(dtReportHeader);
                        ds.DataSetName = "TransferIssueSummaryWithValue";
                       // WriteXSD(ds, "TransferIssueSummaryWithValue.xsd");
                        ShowReport(ds, "rptDailyTransferIssueSummaryWithValue.rpt");
                    }
                    catch (Exception ex)
                    {

                        throw ex;
                    }
                    break;
                case "OwnConsumptionChallanSummary":
                    try
                    {
                        SalesOrderReport aRelatedReport = (SalesOrderReport)Session[SessionCollection.ReportParam];

                        SalesOrderReportsDAL _aDal = new SalesOrderReportsDAL();
                        UtilityDAL aUtilityDAL = new UtilityDAL();
                        ds = new DataSet();
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        DataTable dt2 = _aDal.GetOwnConsupmtionChallanPrint(aRelatedReport.soId).Copy();

                        dtReportHeader.TableName = "dtReportHeader";
                        dt2.TableName = "ReqTable";
                        ds.Tables.Add(dt2);
                        ds.Tables.Add(dtReportHeader);
                        ds.DataSetName = "OwnConsumptionChallan";
                      //   WriteXSD(ds, "OwnConsumptionChallan.xsd");
                       ShowReport(ds, "rptOwnConsumptionChallan.rpt");
                    }
                    catch (Exception ex)
                    {

                        throw ex;
                    }
                    break;
                case "mrrReturnList":
                    try
                    {
                        ViewTradeMrrReturnMaster aRelatedReport = (ViewTradeMrrReturnMaster)Session[SessionCollection.ReportParam];

                        OthersMrrDal _aDal = new OthersMrrDal();
                        UtilityDAL aUtilityDAL = new UtilityDAL();

                        ds = new DataSet();
                        DataTable dtRequisition = _aDal.GetReturnReport(aRelatedReport.SupplierID, aRelatedReport.DelWareHouseID, aRelatedReport.FromDate, aRelatedReport.ToDate).Copy();
                        dtRequisition.TableName = "dtReturnM";
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        dtReportHeader.TableName = "dtReportHeader";

                        ds.DataSetName = "dsReturnM";
                        ds.Tables.Add(dtRequisition);
                        ds.Tables.Add(dtReportHeader);
                        //  WriteXSD(ds, "mrrReturnedListReport.xsd");
                        ShowReport(ds, "rpt_MrrReturnListReport.rpt");
                        // ShowReport(ds, "rpt_tradeMrrReturnListReport.rpt");




                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;
                case "TradeReturnList":
                    try
                    {
                        ViewTradeMrrReturnMaster aRelatedReport = (ViewTradeMrrReturnMaster)Session[SessionCollection.ReportParam];

                        TradeMrrDAL _aDal = new TradeMrrDAL();
                        UtilityDAL aUtilityDAL = new UtilityDAL();

                        ds = new DataSet();
                        DataTable dtRequisition = _aDal.GetReturnReport(aRelatedReport.SupplierID, aRelatedReport.DelWareHouseID, aRelatedReport.FromDate, aRelatedReport.ToDate).Copy();
                        dtRequisition.TableName = "dtReturn";
                        DataTable dtReportHeader = aUtilityDAL.GetReportHeader(7).Copy();
                        dtReportHeader.TableName = "dtReportHeader";

                        ds.DataSetName = "dsReturn";
                        ds.Tables.Add(dtRequisition);
                        ds.Tables.Add(dtReportHeader);
                        //  WriteXSD(ds, "PoRequisitionListReport.xsd");
                        ShowReport(ds, "rpt_tradeMrrReturnListReport.rpt");
                        //  ShowReport(ds, "rpt_PoRequisitionListReport.rpt");



                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    break;
            }
        }

        private void WriteXSD(DataSet dsDataSet, string xsdName)
        {
            string path = @"~\\Report\\DataSets\\" + xsdName;
            dsDataSet.WriteXmlSchema(MapPath(path));
        }
        private string ReportPath(string rptName)
        {
            var path = Convert.ToString(Server.MapPath(@"~\\Report\\CrystalReports\\" + rptName));
            return Convert.ToString(Server.MapPath(@"~\\Report\\CrystalReports\\" + rptName));
        }
        protected void rptViewerBasic_Unload(object sender, EventArgs e)
        {
            if (this.rptdoc != null)
            {
                rptdoc.Close();
                rptdoc.Dispose();
                rptViewer.Dispose();
            }
        }

        protected void rptViewerBasic_Disposed(object sender, EventArgs e)
        {
            if (this.rptdoc != null)
            {
                rptdoc.Close();
                rptdoc.Dispose();
                rptViewer.Dispose();
            }
        }


        public void ExcelExport(DataTable excelData)
        {
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Customers.xls"));
            Response.ContentType = "application/ms-excel";
            DataTable dt = excelData;
            string str = string.Empty;
            foreach (DataColumn dtcol in dt.Columns)
            {
                Response.Write(str + dtcol.ColumnName);
                str = "\t";
            }
            Response.Write("\n");
            foreach (DataRow dr in dt.Rows)
            {
                str = "";
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    Response.Write(str + Convert.ToString(dr[j]));
                    str = "\t";
                }
                Response.Write("\n");
            }
            Response.End();


        }
        private void ShowReport(DataSet dsDataSet, string reportName, int checkIndex = 0, CrystalDecisions.Shared.ParameterFields paramFields=null)
        {
            if (dsDataSet.Tables[checkIndex].Rows.Count > 0)
            {
                var path = ReportPath(reportName);
                rptdoc.Load(ReportPath(reportName));

                rptdoc.SetDataSource(dsDataSet);
                rptViewer.ReportSource = rptdoc;
                rptViewer.ParameterFieldInfo = paramFields;
                rptViewer.DataBind();

            }
            else
            {
                lblMsg.Text = "No Data Found!!!!";
            }

        }

        private void ShowReportPDF(DataSet dsDataSet, string reportName, int checkIndex = 0, CrystalDecisions.Shared.ParameterFields paramFields = null)
        {
            if (dsDataSet.Tables[checkIndex].Rows.Count > 0)
            {
                var path = ReportPath(reportName);
                rptdoc.Load(ReportPath(reportName));

                rptdoc.SetDataSource(dsDataSet);

                // rptdoc.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, "ExportedReport");
                //Stream stream= rptdoc.ExportToStream(ExportFormatType.PortableDocFormat);
                Response.Buffer = true;
                //Response.BufferOutput
                rptdoc.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, false, "ExportedReport");
               
                Response.End();
                // rptdoc.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response,true,);
                //PrinterSettings settings = new PrinterSettings();

                //PrintDialog pdialog = new PrintDialog();
                //if (pdialog.ShowDialog() == DialogResult.OK)
                //{
                //    settings = pdialog.PrinterSettings;
                //}

                //rptdoc.PrintToPrinter(settings, new PageSettings() { }, false);
                //string FilePath = Server.MapPath("javascript1-sample.pdf");
                //WebClient User = new WebClient();
                //Byte[] FileBuffer = User.DownloadData(FilePath);
                //if (FileBuffer != null)
                //{
                //    Response.ContentType = "application/pdf";
                //    Response.AddHeader("content-length", FileBuffer.Length.ToString());
                //   // Response.OutputStream()
                //    Response.BinaryWrite(FileBuffer);
                //}
               // rptdoc.PrintToPrinter(1, true, 0, 0);

                //rptViewer.ReportSource = rptdoc;
                //rptViewer.ParameterFieldInfo = paramFields;
                //rptViewer.DataBind();

            }
            else
            {
                lblMsg.Text = "No Data Found!!!!";
            }

        }
    }
}