﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IGFoodProject.DataManager;
using IGFoodProject.Models;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;
using System.Data;

namespace IGFoodProject.CrystalViewer
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        DataAccessManager _accessManager = new DataAccessManager();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private byte[] ConvertImageToByteArray(System.Drawing.Image imageToConvert,
                                       System.Drawing.Imaging.ImageFormat formatOfImage)
        {
            byte[] Ret;
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    imageToConvert.Save(ms, formatOfImage);
                    Ret = ms.ToArray();
                }
            }
            catch (Exception) { throw; }
            return Ret;
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            System.Drawing.Image imag = System.Drawing.Image.FromStream(
        FileUpload1.PostedFile.InputStream);
            System.Data.SqlClient.SqlConnection conn = null;
            try
            {
                try
                {
                    conn = new System.Data.SqlClient.SqlConnection(
                        ConfigurationManager.ConnectionStrings["CodeDbSet"].ConnectionString);
                    conn.Open();
                    System.Data.SqlClient.SqlCommand insertCommand =
                        new System.Data.SqlClient.SqlCommand(
                        "update tbl_ReportHeader set  [Logo]=@Pic where CompanyId=7", conn);
                    insertCommand.Parameters.Add("Pic", SqlDbType.Image, 0).Value =
                        ConvertImageToByteArray(imag, System.Drawing.Imaging.ImageFormat.Jpeg);
                    int queryResult = insertCommand.ExecuteNonQuery();
                    if (queryResult == 1)
                        lblRes.Text = "msg record Created Successfully";
                }
                catch (Exception ex)
                {
                    lblRes.Text = "Error: " + ex.Message;
                }
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
            
        }
    }
}