﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IGFoodProject.Utility;
using IGFoodProject.DAL;


namespace IGFoodProject.Utility
{
    public class AccessAuthoraization
    {

        public static bool CheckAccess(string controllerName, string actionName)
        {

            try
            {
                if (HttpContext.Current.Session[SessionCollection.UserName] == null)
                {
                    return false;
                }
                if (HttpContext.Current.Session[SessionCollection.UserName].ToString().ToLower() == "admin")
                {
                    return true;
                }
                else
                {
                    UtilityDAL aUtilityDal = new UtilityDAL();
                    if (aUtilityDal.CheckMenuAccess(controllerName, actionName, HttpContext.Current.Session[SessionCollection.UserName].ToString().ToLower()))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

            }
            catch (Exception)
            {

                return false;
            }
        }

    }
}