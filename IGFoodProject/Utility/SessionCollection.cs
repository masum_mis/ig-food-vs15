﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Utility
{
    public static class SessionCollection
    {
        public static string UserName = "username";
        public static string LoginUserId = "loginuserid";
        public static string LoginUserType = "loginusertype";
        public static string LoginUserEmpId = "loginuserempid";
        public static string ModuleId = "moduleid";
        public static string ReportValue = "reportvalue";
        public static string ReportParam = "reportparam";
        public static string UserInfo = "UserInfo"; 
    }
}