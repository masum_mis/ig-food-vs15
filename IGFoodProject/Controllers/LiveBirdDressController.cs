﻿using IGFoodProject.Utility;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IGFoodProject.Controllers
{
    public class LiveBirdDressController : Controller
    {
        private LiveBirdDressDAL _liveBirdDressDAL = new LiveBirdDressDAL();
        public ActionResult Index()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;

            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public dynamic GetAddRow(int tr)
        {
            TempData["trSl"] = tr;
            return PartialView("_PartialLiveBirdDress");
        }

        public dynamic LoadBirdStock(int itemId, int wareHouseId, DateTime receiveDate)
        {
            return Json(_liveBirdDressDAL.ItemStock(itemId, wareHouseId, receiveDate), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveDressBird(DressedBirdMaster entity)
        {
            ResultResponse aResponse = new ResultResponse();
            entity.CreateBy = Session[SessionCollection.LoginUserEmpId].ToString() ;
            entity.CreatDate = DateTime.Now;
            entity.DressedStatus = 2;
            entity.StockInStatus = 3;
            aResponse = _liveBirdDressDAL.SaveDressBird(entity);
            return Json(aResponse, JsonRequestBehavior.AllowGet);
            
        }

        public ActionResult LiveBirdDressList()
        {
            if (Session[SessionCollection.LoginUserEmpId].ToString() != null)
            {
                List<DressedBirdMaster> dressedBirdMasterList = new List<DressedBirdMaster>();
                dressedBirdMasterList = _liveBirdDressDAL.LiveBirdDressList();
                return View(dressedBirdMasterList);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
        }

        public PartialViewResult ViewLiveBirdDressed(int id = 0)
        {
            DressedBirdMaster dressedBirdMaster = new DressedBirdMaster();
            dressedBirdMaster = _liveBirdDressDAL.GetLiveBirdDressById(id);
            return PartialView("_PartialViewLiveBirdDress", dressedBirdMaster);
            
        }

        public ActionResult LoadAllWareHouse()
        {
            List<WareHouse> wareHouses = _liveBirdDressDAL.LoadAllWareHouse();
            return Json(wareHouses.FindAll(x=> x.WarehouseTypeId!=1), JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadReceiveDate()
        {
            List<MrRMaster> wareHouses = _liveBirdDressDAL.LoadReceiveDate();
            return Json(wareHouses, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LiveBirdDressApprove(int id=0)
        {
            DressedBirdMaster aMaster = _liveBirdDressDAL.GetLiveBirdDressForApprove(id);
            return View( aMaster);
        }

        public dynamic GetAddRowForApprove(int tr)
        {
            TempData["trSl"] = tr;
            return PartialView("_PartialLiveBirdDressApprove");
        }

        public ActionResult ApproveDressBird(DressedBirdMaster entity)
                                                                                                                                                                                                                           {
            ResultResponse aResponse = new ResultResponse();
            entity.ApprovedBy = Session[SessionCollection.LoginUserEmpId].ToString();
            entity.ApprovedDate = DateTime.Now;
            entity.DressedStatus = 2;
            entity.StockInStatus = 3;
            aResponse = _liveBirdDressDAL.ApproveDressBird(entity);
            return Json(aResponse, JsonRequestBehavior.AllowGet);

        }

        public ActionResult LiveBirdDressReport()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;

            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }

        public ActionResult GetLiveBirdDressList(DateTime fromDate, DateTime toDate, int itemId = 0)
        {
            DataTable dt = _liveBirdDressDAL.GetLiveBirdDressReport(fromDate, toDate, itemId);
            return PartialView("_LiveBirdDressReport",dt);
        }
        public ActionResult LoadDressBirdNo( DateTime dressDate)
        {
            List<DressedBirdMaster> aMaster = _liveBirdDressDAL.LiveBirdDressNoList(dressDate);
            return Json(aMaster, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetLiveBirdDressListInvoice(int dressedBirdMasterId)
        {
            DataTable dt = _liveBirdDressDAL.GetLiveBirdDressReportInvoice(dressedBirdMasterId);
            return Json(dt, JsonRequestBehavior.AllowGet);
        }
    }
}
