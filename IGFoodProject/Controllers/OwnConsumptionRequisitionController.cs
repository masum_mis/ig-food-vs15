﻿using IGFoodProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;
using IGFoodProject.Models.ReportModel;

namespace IGFoodProject.Controllers
{
    public class OwnConsumptionRequisitionController : Controller
    {
        // GET: OwnConsumptionRequisition
        OwnConsumptionRequisitionDAL aDal = new OwnConsumptionRequisitionDAL();
        public ActionResult Index(int SalesOrderId = 0, string eflag = "ed")
        {
            ViewBag.SalesOrderId = SalesOrderId;
            ViewBag.eflag = eflag;
            return View();
        }
        public ActionResult GetCustomerType()
        {
           
            List<CustomerType> aList = aDal.GetCustomerTypeList();
            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddRowForOrder(int tr)
        {
            TempData["trSl"] = tr;

            return PartialView("_addNewOrderRow");
        }

        public ActionResult GetProductDetails_own(int productId, DateTime orderdate, int customertype)
        {
            ViewProductDetailsSales aDetailsSales = aDal.GetProductDetails_Own(productId, orderdate, customertype);
            return Json(aDetailsSales, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveOrderPrecessed(SalesOrderProcessedMaster aMaster)
        {

            ResultResponse aResponse = aDal.SaveSalesOrderProcessed(aMaster,
                Session[SessionCollection.UserName].ToString());
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetCustomer()
        {
            List<ViewCustomerInfo> aList = aDal.GetCustomerList();
            return Json(aList, JsonRequestBehavior.AllowGet);
        }
        



        public ActionResult RequisitionList(DateTime? fromdate, DateTime? todate, string salespersonid, int customerid = 0)
        {
            List<ViewSalesOrderProcessedMaster> aList = aDal.GetSalesOrderProcessedMaster(fromdate, todate, salespersonid, customerid);


            return View(aList);
        }
        public ActionResult RequisitionList_Search(DateTime? fromdate, DateTime? todate, string salespersonid, int customerid = 0)
        {
            List<ViewSalesOrderProcessedMaster> aList = aDal.GetSalesOrderProcessedMaster(fromdate, todate, salespersonid, customerid);


            return Json(aList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ViewOrder(int id)
        {
            return PartialView("_processedOrder");
        }

        public ActionResult RequisitionApproveList(DateTime? fromdate, DateTime? todate, string salespersonid, int customerid = 0)
        {
            IEnumerable<ViewSalesOrderProcessedMaster> aList = aDal.GetRequisitionMaster(fromdate, todate, salespersonid, customerid);


            return View(aList);
        }

        public ActionResult RequisitionApproveList_Search(DateTime? fromdate, DateTime? todate, string salespersonid, int customerid = 0)
        {
            IEnumerable<ViewSalesOrderProcessedMaster> aList = aDal.GetRequisitionMaster(fromdate, todate, salespersonid, customerid);


            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ApproveOwnRequision(int id)
        {

            ViewBag.SalesOrder = id;
            return View();


        }


        public ActionResult RuquisitionListForIssue()
        {

            List<ViewSalesOrderProcessedMaster> aList = aDal.GetRequisitionForIssue();
            return View(aList);
        }

        public ActionResult ProcessedIssue(int id)
        {
            ViewBag.SalesOrder = id;
            


            return View();
        }
        public ActionResult GetRequisitionMaster_Issue(int id)
        {
            SalesOrderProcessedMaster aMaster = aDal.GetSalesOrderProcessedMaster(id);
            aMaster.SalesDetails = aDal.GetRequisitionDetailsByMaster_Issue(id);
            return Json(aMaster, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddRowForOrder_Issue(int tr)
        {
            TempData["trSl"] = tr;
            return PartialView("_addNewOrderRow_Isuue");
        }
        public ActionResult LoadProductDetails(int warehouse, DateTime? orderdate, int productId)
        {
            ViewProductDetailsSales aDetailsSales = aDal.GetProductDetails(productId, orderdate,
                warehouse);
            return Json(aDetailsSales, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetProductDetails(int productId, int wareHouseId, int customerType, DateTime orderDate,
            DateTime challanDate)
        {
            ViewProductDetailsSales aDetails = aDal.GetProductDetailsForChallan(productId, wareHouseId,
                customerType, orderDate, challanDate);
            return Json(aDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveProcessedIssue(SalesOrderChallanMaster aMaster)
        {
            ResultResponse aResponse = aDal.SaveIssueProcessedChallan(aMaster,
                Session[SessionCollection.UserName].ToString());

            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ConfirmedIssueList()
        {
            List<ViewGeneratedChallan> aList = aDal.GetGeneratedIssue();
            return View(aList);
        }

        public ActionResult ViewIssue(int id)
        {

            ViewGeneratedChallan aChallan = aDal.GetGeneratedIssue(id)[0];
            aChallan.Details = aDal.GetGeneratedIssueDetails(id).ToList();
            return View(aChallan);
        }

        public ActionResult SalesOrderReports(SalesOrderReport aSalesOrderReport)
        {
            ResultResponse aResponse = new ResultResponse();
            BasicInfoReportsParameter aBasicInfoReportsParameter = new BasicInfoReportsParameter();
            aBasicInfoReportsParameter.ReportValue = aSalesOrderReport.ReportValue;
            Session[SessionCollection.ReportValue] = aBasicInfoReportsParameter;
            Session[SessionCollection.ReportParam] = aSalesOrderReport;
            aResponse.isSuccess = true;
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
    }
}