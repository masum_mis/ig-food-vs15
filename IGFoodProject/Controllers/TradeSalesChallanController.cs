﻿using System;
using System.Collections.Generic;
using System.Data;
using System.EnterpriseServices;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;

namespace IGFoodProject.Controllers
{
    public class TradeSalesChallanController : Controller
    {
        private  TradeSalesOrderDAL tradeSalesDal= new TradeSalesOrderDAL();
        private  TradeSalesChallanDAL tradeChallanDal= new TradeSalesChallanDAL();
        // GET: TradeSalesChallan
        public ActionResult Index()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                List<SalesOrderMasterTrade> aList = tradeChallanDal.GetSalesOrderProcessedForChallan();
                return View(aList);
            }
        }
        public ActionResult TradeChallan(int soId = 0)

        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                ViewBag.SalesOrderId = soId;
                return View();
            }
        }
        public ActionResult GetSalesOrderMasterFChallan(int id)
        {
            
            SalesOrderMasterTrade aMaster = tradeSalesDal.GetSalesOrderProcessedMaster(id);
            aMaster.SalesDetailsTrade = tradeSalesDal.GetSalesOderProcessedDetailsByMaster(id);
            return Json(aMaster, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetProductDetails(int productId, int wareHouseId,DateTime challanDate)
        {
            ViewProductDetailsSales aDetails = tradeChallanDal.GetProductDetailsForChallan(productId, wareHouseId,challanDate);
            return Json(aDetails, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddRowForOrder(int tr)
        {
            TempData["trSl"] = tr;
            return PartialView("_addNewRow");
        }
        public ActionResult SaveTradedChallan(TradeSalesChallanMaster aMaster)
        {
            ResultResponse aResponse = tradeChallanDal.SaveSalesOrderProcessedChallan(aMaster,
                Session[SessionCollection.UserName].ToString());

            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ChallanListForDeliveryConfirm_Processed()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                List<ChallanListForDelivery> aList = tradeChallanDal.GetChallanListForDeliveryConfirm();
                return View("ChallanListForDeliveryConfirm", aList);
            }
        }
        public ActionResult TradeDeliveryConfirm(int id)
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                ViewBag.challanno = id;
                return View();
            }
        }

        public ActionResult GetTradeChallanMaster(int id)
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                ViewChallanDetailForDelivery aMaster = tradeChallanDal.GetTradeChallanProcessedMaster(id);
                aMaster.SalesDetails = tradeChallanDal.GetTradeChallanDetailsByMaster(id);
                return Json(aMaster, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AddRowForDelivery(int tr)
        {
            TempData["trSl"] = tr;
            return PartialView("_addNewOrderRow_Tradedelivery");
        }

        public ActionResult SavePrecessedDelivery(SalesOrderChallanMaster aMaster)
        {
            ResultResponse aResponse = tradeChallanDal.SaveTradeChallanDelivery(aMaster,
                Session[SessionCollection.UserName].ToString());

            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }


        public ActionResult TradeSalesChallanReport()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
              return  View();
            }
        }

        public ActionResult GetTradeChallaneport( DateTime? orderFromDate, DateTime? orderToDate, DateTime? challanFromDate, DateTime? challanToDate, int locationId = 0, int wareHouseId = 0, string salesperson = "")
        {
            String[] data;
            data = salesperson.Split(':');
            TradeSalesChallanDAL aDal = new TradeSalesChallanDAL();
            DataTable dt = aDal.TradeChallanReportReport(locationId,wareHouseId,data[0],orderFromDate,orderToDate,challanFromDate,challanToDate);
            return PartialView("_PartialChallanReport", dt);
        }

        public ActionResult TradeSalesChallanReturn()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }

        public ActionResult GetTradeChallanReturn(DateTime fromDate, DateTime toDate)
        {
            TradeSalesChallanDAL aDal = new TradeSalesChallanDAL();
            DataTable dt = aDal.TradeChallanReturn(fromDate,toDate);
            return PartialView("_PartialTradeChallanFReturn", dt);
        }

        public ActionResult TradeChallanReturn(int challanId = 0)

        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                TradeSalesChallanDAL aDal = new TradeSalesChallanDAL();
                DataTable dt = aDal.TradeChallanReturnFChallanId(challanId);
                return View(dt);
            }
        }

        public ActionResult SaveTradeReturn(TradeReturnLog aMaster)
        {
            ResultResponse aResponse = tradeChallanDal.SaveTradeReturn(aMaster,
                Session[SessionCollection.UserName].ToString());

            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TradeChallanReturnQcOperation()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }

        public ActionResult GetChallan(DateTime challanDate)
        {
            TradeSalesChallanDAL aDal = new TradeSalesChallanDAL();
            List<SalesOrderChallanMaster> aList = aDal.GetChallanList(challanDate);
            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TradeQcOperationProductList( int challanId)
        {
            TradeSalesChallanDAL aDal = new TradeSalesChallanDAL();

            DataTable aList = aDal.GetRemainingtradeQcRequestedProductList(challanId);
            return PartialView("_PartialTradeQCOperationProductList", aList);
        }

        public ActionResult SaveQcOperation(QcOperationMaster entity)
        {
            TradeSalesChallanDAL aDal = new TradeSalesChallanDAL();
            entity.CreateDate = DateTime.Now;
            ResultResponse aResponse = new ResultResponse();
            aResponse = aDal.SaveTradeQcOperation(entity);
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetQcProductHistoryOfQty(int challanId, int productId,int challanDetails)
        {
            TradeSalesChallanDAL aDal = new TradeSalesChallanDAL();
            DataTable aList = aDal.GetTradeQcProductHistoryOfQty(challanId, productId, challanDetails);
            return PartialView("_PartialTradeProductHistoryQty", aList);
        }

        public ActionResult GetQcProductHistoryOfKg(int challanId, int productId, int challanDetails)
        {
            TradeSalesChallanDAL aDal = new TradeSalesChallanDAL();
            DataTable aList = aDal.GetTradeQcProductHistoryOfKg(challanId, productId, challanDetails);
            return PartialView("_PartialTradeProductHistoryKg", aList);
        }

        public ActionResult ProductUnitCost(int productId, DateTime challanDate)
        {
            TradeSalesChallanDAL aDal = new TradeSalesChallanDAL();
            var unitCost = aDal.GetProdutCostPrice(productId, challanDate);
            return Json(unitCost, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PreviousReturn(int challanDetailsId)
        {
            TradeSalesChallanDAL aDal = new TradeSalesChallanDAL();
            var data = aDal.GetProdutPreviousReturn(challanDetailsId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TradeConfirmedChallan(DateTime? fromDate,DateTime? toDate)
        {
            TradeSalesChallanDAL aDal = new TradeSalesChallanDAL();
            var data = aDal.TradeConfirmedChallan(fromDate, toDate);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}