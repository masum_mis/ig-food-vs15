﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.Utility;
using IGFoodProject.DAL;
using IGFoodProject.Models;

namespace IGFoodProject.Controllers
{
    public class ProductPackSizeController : Controller
    {
        // GET: ProductPackSize
        public ActionResult ProductPackSizeList()
        {
            ProductPackSizeDAL aDal = new ProductPackSizeDAL();
            List<tbl_ProductPackSize> alist = aDal.GetPackSizeList();
            return View(alist);
        }
        public ActionResult AddPackSize(int id = 0)
        {
            ViewBag.packsizeid = id;
            return View();
        }
        public JsonResult CheckPackSize(string group)
        {

            ProductPackSizeDAL aDAL = new ProductPackSizeDAL();

            int desig = aDAL.CheckPackSize(group);

            return Json(desig , JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SavePackSize(tbl_ProductPackSize aGroup)
        {
            ResultResponse aResponse = new ResultResponse();
            ProductPackSizeDAL aDAL = new ProductPackSizeDAL();

            string entryBy = Session[SessionCollection.UserName].ToString();
            aGroup.CreateBy = entryBy;
            aGroup.UpdateBy = entryBy;
            aResponse.isSuccess = aDAL.SavePackSize(aGroup);

            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetPackSizeByID(int id)
        {
            ProductPackSizeDAL aDal = new ProductPackSizeDAL();
            return Json(aDal.GetPackSizeForEdit(id),JsonRequestBehavior.AllowGet);

        }
        public JsonResult DeletePackSize(int id)
        {
            ResultResponse aResponse = new ResultResponse();
            ProductPackSizeDAL aDAL = new ProductPackSizeDAL();


            aResponse.isSuccess = aDAL.DeletePacksize(id);

            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
    }
}