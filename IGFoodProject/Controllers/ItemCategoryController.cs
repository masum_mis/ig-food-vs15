﻿using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IGFoodProject.Controllers
{
    public class ItemCategoryController : Controller
    {
        ItemCategoryDAL aDal = new ItemCategoryDAL();

        // GET: ItemCategory
        public ActionResult AddItemCategory(int id = 0)
        {
            ViewBag.ItemCategoryId = id;
            return View();
        }
        public ActionResult ItemCategoryList()
        {
            List<ItemCategory> alist = aDal.GetItemCategoryList();
            return View(alist);
        }
        public JsonResult LoadItemType()
        {
            return Json(aDal.GetItemTypeList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckItemCategory(string ptype)
        {
            int type = aDal.CheckItemCategory(ptype);
            return Json(type, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveItemCategory(ItemCategory aType)
        {
            ResultResponse aResponse = new ResultResponse();
            string entryBy = Session[SessionCollection.UserName].ToString();
            aType.CreateBy = entryBy;
            aType.UpdateBy = entryBy;
            aResponse.isSuccess = aDal.SaveItemCategory(aType);
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetItemCategoryByID(int id)
        {
            return Json(aDal.GetItemCategoryByID(id), JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteItemCategory(int id)
        {
            ResultResponse aResponse = new ResultResponse();
            aResponse.isSuccess = aDal.DeleteItemCategory(id);
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

    }
}