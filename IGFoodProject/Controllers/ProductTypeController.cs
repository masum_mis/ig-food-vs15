﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.Utility;
using IGFoodProject.DAL;
using IGFoodProject.Models;

namespace IGFoodProject.Controllers
{
    public class ProductTypeController : Controller
    {
        // GET: ProductType
        public ActionResult ProductTypeList()
        {
            ProductDAL aDal = new ProductDAL();
            List<tbl_ProductType> alist = aDal.GetTypeList();
            return View(alist);
        }

        public ActionResult AddType(int id = 0)
        {
            ViewBag.typeid = id;
            return View();
        }
        public JsonResult LoadGroup()
        {
            ProductDAL aDal = new ProductDAL();
            return Json(aDal.GetGroupList() , JsonRequestBehavior.AllowGet);
        }
        public JsonResult CheckType(string ptype)
        {

            ProductDAL aDAL = new ProductDAL();

            int desig = aDAL.CheckType(ptype);

            return Json(desig, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveType(tbl_ProductType aType)
        {
            ResultResponse aResponse = new ResultResponse();
            ProductDAL aDAL = new ProductDAL();

            string entryBy = Session[SessionCollection.UserName].ToString();
            aType.CreateBy = entryBy;
            aType.UpdateBy = entryBy;
            aResponse.isSuccess = aDAL.SaveType(aType);

            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTypeByID(int id)
        {
            ProductDAL aDal = new ProductDAL();
            return Json(aDal.GetTypeForEdit(id), JsonRequestBehavior.AllowGet);

        }

        public JsonResult DeleteType(int id)
        {
            ResultResponse aResponse = new ResultResponse();
            ProductDAL aDAL = new ProductDAL();


            aResponse.isSuccess = aDAL.DeleteType(id);

            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
    }
}