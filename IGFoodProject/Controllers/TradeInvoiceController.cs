﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;
using IGFoodProject.Models.ReportModel;

namespace IGFoodProject.Controllers
{
    public class TradeInvoiceController : Controller
    {
        // GET: TradeInvoice
        TradeInvoiceDAL _aDal = new TradeInvoiceDAL();

        // GET: Invoice
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult IndexMultiplePrint()
        {
            return View();
        }
        public ActionResult InvoiceEntry()
        {
            return View();
        }
        public ActionResult LoadMrrNoBySupplier(int id)
        {
            List<ViewOthersMrr> aPo = _aDal.LoadMrrNoBySupplier(id);
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadTradeMrrInfo(int supplierId, DateTime? mrrDate = null)
        {
            List<ViewTradeInvoiceMaster> aPo = _aDal.LoadTradeMrrInfo(supplierId, mrrDate);
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveTradeInvoice(ViewTradeInvoiceMaster entity)
        {
            try
            {
                string entryBy = Session[SessionCollection.UserName].ToString();
                ResultResponse aResponse = _aDal.SaveTradeInvoice(entity, entryBy);
                return Json(aResponse, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }



        public ActionResult GetTradeInvoices(int supplierId = 0, int tradeInvoiceId = 0, DateTime? fromDate = null, DateTime? toDate = null)
        {
            List<ViewTradeInvoiceMaster> aPo = _aDal.GetTradeInvoices(supplierId, tradeInvoiceId, fromDate, toDate);
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }
        public ActionResult TradeInvoiceDetailsById(int id)
        {
            List<ViewTradeInvoiceDetails> aPo = _aDal.TradeInvoiceDetailsById(id);
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LoadTradeInvoiceNo()
        {
            List<ViewTradeInvoiceMaster> aPo = _aDal.LoadTradeInvoiceNo();
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PrintIndivualInvoice(ViewInvoiceMaster aDirrectMrr)
        {
            ResultResponse aResponse = new ResultResponse();
            BasicInfoReportsParameter aBasicInfoReportsParameter = new BasicInfoReportsParameter();
            aBasicInfoReportsParameter.ReportValue = aDirrectMrr.ReportValue;
            Session[SessionCollection.ReportValue] = aBasicInfoReportsParameter;
            Session[SessionCollection.ReportParam] = aDirrectMrr;
            aResponse.isSuccess = true;
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MultipleInvoiceReport(ViewTradeInvoiceMaster aDirrectMrr)
        {
            ResultResponse aResponse = new ResultResponse();
            BasicInfoReportsParameter aBasicInfoReportsParameter = new BasicInfoReportsParameter();
            aBasicInfoReportsParameter.ReportValue = aDirrectMrr.ReportValue;
            Session[SessionCollection.ReportValue] = aBasicInfoReportsParameter;
            Session[SessionCollection.ReportParam] = aDirrectMrr;
            aResponse.isSuccess = true;
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
    }
}