﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;

namespace IGFoodProject.Controllers
{
    public class TerritoryController : Controller
    {
        private TerritoryDAL TerriDal=new TerritoryDAL() ;
        // GET: Territory
        public ActionResult Index(int id=0)
        {
            UtilityDAL dal= new UtilityDAL();
            List<Area> areaList = dal.GetArea();
            Territory aTerritory= new Territory();
             aTerritory = TerriDal.GetTerritory(id);
            var tuple = new Tuple<IEnumerable<Area>, Territory>(areaList, aTerritory);
            return View(tuple);
        }

        public ActionResult SaveTerritory(Territory entity)
        {
            ResultResponse aResponse = new ResultResponse();
            entity.CreateBy = Session[SessionCollection.LoginUserEmpId].ToString();
            entity.CreateDate = DateTime.Now;
            aResponse = TerriDal.SaveTerritory(entity);
            return Json(aResponse, JsonRequestBehavior.AllowGet);

        }

        public ActionResult TerritoryList()
        {
           List<Territory> territoryList= new List<Territory>();
            territoryList = TerriDal.GetAllTerritory();
            return View(territoryList);
        }
    }
}