﻿using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IGFoodProject.Controllers
{
    public class DirrectMrrController : Controller
    {
         DirrectMrrDAL aDal = new DirrectMrrDAL();
        // GET: DirrectMrr
        public ActionResult Create(int MRRrOtherID = 0)
        {
            ViewBag.MRRrOtherID = MRRrOtherID;
            return View();
        }

        public JsonResult LoadReceiveWarehouse()
        {
            List<DirrectMrr> bu = aDal.LoadReceiveWarehouse();
            return Json(bu, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadSupplier()
        {
            List<DirrectMrr> sup = aDal.LoadSupplier(); 
            return Json(sup, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddRowForItem(int tr)  
        {
            TempData["trSl"] = tr;

            return PartialView("_addNewGLRow");
        }

        public ActionResult GetItemDetails(int itemId)
        {
            DirrectMrr aDetails = aDal.GetItemDetails(itemId); 
            return Json(aDetails, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetItemList()
        {
            List<DirrectMrr> sup = aDal.GetItemList();  
            return Json(sup, JsonRequestBehavior.AllowGet);
        }
      

        public ActionResult SaveDirrectMrr(DirrectMrr aMaster)
        {
            try
            {
                string entryBy = Session[SessionCollection.UserName].ToString();
                ResultResponse aResponse = aDal.SaveDirrectMrr(aMaster, entryBy);
                return Json(aResponse, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


    }
}