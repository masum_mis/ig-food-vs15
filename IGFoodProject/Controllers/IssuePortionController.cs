﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;

namespace IGFoodProject.Controllers
{
    public class IssuePortionController : Controller
    {
        // private bool status = false;
        private IssuePortionDAL _dal = new IssuePortionDAL();

        //public IssuePortionController()
        //{
        //   if (SessionCollection.LoginUserId== "loginuserid")
        //   {
        //       status = true;

        //   }


        //}

        //~IssuePortionController()
        //{
        //    RedirectToAction("LogIn", "Home");
        //}
        // GET: IssuePortion
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAllProtionReqList()
        {
            return Json(_dal.GetDBPortionReqNOByWareHouse(), JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetDbPortionReqInfoById(int dbToPortionReqId)
        {
            return Json(_dal.GetDbPortionReqViewInfoById(dbToPortionReqId), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDbPortionReqDetailsById(int dBToPortionReqId)
        {
            return PartialView("_PartialIssuePortion", _dal.GetDbPortionReqDetailsById(dBToPortionReqId));
        }

        public ActionResult GetDbPortionReqDetails2ById(int dBToPortionReqId)
        {
            return PartialView("_partialIssuePortionDetails", _dal.GetDbPortionReqDetails2ById(dBToPortionReqId));
        }

        public ActionResult SaveIssueProduct(DBIssuePortionMaster entity)
        {
            if (Session[SessionCollection.UserName].ToString() != "")
            {
                ResultResponse aResponse = new ResultResponse();
                entity.IssueDate = DateTime.Now;
                entity.CreateBy = Session[SessionCollection.UserName].ToString();
                entity.CreateDate = DateTime.Now;
                aResponse = _dal.DbToPortionIssue(entity);
                return Json(aResponse, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult PortionIssueList()
        {
            return View(_dal.GetDbPortionIssueList());

        }






        public ActionResult AddPortionFromIssue()
        {
            return View();
        }

        public ActionResult GetIssueForStockIn(int warehouseId)
        {
            List< DBIssuePortionMaster > aList = _dal.GetIssueForStockIn(warehouseId);

            return Json(aList, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetIssueDetails(int id)
        {
            List<ViewDBPortionIssueDetail> aDetails = _dal.GetIssueDetailsByIssueId(id);
            return PartialView("_issue_details" , aDetails);
        }


        public ActionResult SavePortionStockTemp(PortionStockInMaster_Log aInMasterLog)
        {
            ResultResponse aResponse = _dal.SaveStockTemp(aInMasterLog, Session[SessionCollection.UserName].ToString());
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult IssueApproveList()
        {
            return View();
        }




        public ActionResult GetApproveList(DateTime fromdate)
        {
            List<ViewPortionStockTemp> aList = _dal.GetTempStockForApprove(fromdate);

            return PartialView("_approve_issue", aList);
        }

        public ActionResult ApproveStockIn(int id = 0)
        {

            ViewBag.stockinid = id;

            return View();

        }

        public ActionResult LoadLogMasterByID(int id)
        {
            PortionStockInMaster_Log aInMasterLog = _dal.GetIssueLogMasterById(id);
            return Json(aInMasterLog, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadLogDetailsById(int id)
        {
             //

            List<ViewPortionIssusDetailsLog> aList = _dal.GetDetailsByMasterId(id);
            return PartialView("_approveDetails", aList);
        }


        public ActionResult ApproveStockSave(PortionStockInMaster_Log aInMasterLog)
        {
            ResultResponse aResponse = _dal.SaveApproveDBPortion(aInMasterLog);
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DirectStockPortion()
        {
            return View();
        }
        public ActionResult GetProductStock(int productId, int wareHouseId, int groupId, DateTime stockDate)
        {
            return Json(_dal.GetProductStock(productId, wareHouseId, groupId, stockDate), JsonRequestBehavior.AllowGet);
        }
    }
}