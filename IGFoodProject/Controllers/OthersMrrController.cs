﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Models.Accounts;
using IGFoodProject.Models.ReportModel;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;
using System.Data;
using Newtonsoft.Json;

namespace IGFoodProject.Controllers
{
    public class OthersMrrController : Controller
    {
        OthersMrrDal _aDal = new OthersMrrDal();

        // GET: OthersMrr
        public ActionResult Index(DateTime? fromdate, DateTime? todate, int poNo = 0, int supplierId = 0, int itemId = 0)
        {
            List<ViewOthersMrr> aList = _aDal.PoList(fromdate, todate, poNo, supplierId, itemId);
            return View(aList);
        }
        public ActionResult SearchMrrList(DateTime? fromdate, DateTime? todate, int poNo = 0, int supplierId = 0, int itemId = 0)
        {
            List<ViewOthersMrr> aList = _aDal.PoList(fromdate, todate, poNo, supplierId, itemId);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }

        public JsonResult LoadPONoForMRR()
        {
            List<ViewOthersMrr> aPo = _aDal.LoadPONoForMRR();
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetPo_forModalPopUpDetailsById(int id)
        {
            List<ViewOthersMrrDetails> aPo = _aDal.PoDetailsForMrr(id);
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReceiveMrr(int id)
        {

            ViewBag.MasterId = id;

            return View();
        }
        public JsonResult GetDetailsForMrrGenerate(int id)
        {
            ViewOthersMrr aMaster = _aDal.GetPoInfoByMasterById(id);
            aMaster.ViewOthersMrrDetailsList = _aDal.PoDetailsForMrr(id);
            return Json(aMaster, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LoadPackSize()
        {
            List<tbl_ProductPackSize> aPo = _aDal.LoadPackSize();
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveOtherMrr(ViewOthersMrr entity)
        {
            try
            {
                string entryBy = Session[SessionCollection.UserName].ToString();
                ResultResponse aResponse = _aDal.SaveOthersMrr(entity, entryBy);
               return Json(aResponse, JsonRequestBehavior.AllowGet);
               

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public ActionResult MRRList(DateTime? fromdate, DateTime? todate, int mrrNo = 0, int supplierId = 0, int itemId = 0)
        {
            List<ViewOthersMrr> aList = _aDal.MrrListForPrint(fromdate, todate, mrrNo, supplierId, itemId);
            return View(aList);
        }
        public JsonResult LoadMrrNo()
        {
            List<ViewOthersMrr> aPo = _aDal.LoadMrrNo();
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SearchGeneratedMrrList(DateTime? fromdate, DateTime? todate, int mrrNo = 0, int supplierId = 0, int itemId = 0)
        {
            List<ViewOthersMrr> aList = _aDal.MrrList(fromdate, todate, mrrNo, supplierId, itemId);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }
        public ActionResult SearchMrrPrintList(DateTime? fromdate, DateTime? todate, int mrrNo = 0, int supplierId = 0, int itemId = 0)
        {
            List<ViewOthersMrr> aList = _aDal.MrrListForPrint(fromdate, todate, mrrNo, supplierId, itemId);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetMRR_forModalPopUpDetailsById(int id)
        {
            List<ViewOthersMrrDetails> aPo = _aDal.MRRDetailsById(id);
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }
        public ActionResult MRRListindex(DateTime? fromdate, DateTime? todate, int mrrNo = 0, int supplierId = 0, int itemId = 0)
        {
            List<ViewOthersMrr> aList = _aDal.MrrList(fromdate, todate, mrrNo, supplierId, itemId);
            return View(aList);
        }
        #region Edit mrr

        public ActionResult EditMrr(int id=0, int PoMasterId=0, int IsApprove=0)
        {

            ViewBag.MasterId = id;
            ViewBag.PoMasterId = PoMasterId;
            ViewBag.IsApprove = IsApprove;
            return View();
        }
        public JsonResult GetMrrDetails(int id)
        {
            ViewOthersMrr aMaster = _aDal.GetMrrInfoByMasterById(id);
            aMaster.ViewOthersMrrDetailsList = _aDal.MRRDetailsByIdForEdit(id);
            return Json(aMaster, JsonRequestBehavior.AllowGet);
        }


        public ActionResult MRRApprovalList(DateTime? fromdate, DateTime? todate, int mrrNo = 0, int supplierId = 0, int productId = 0)
        {
            List<ViewOthersMrr> aList = _aDal.MrrApprovalList(fromdate, todate, mrrNo, supplierId, productId);
            return View(aList);
        }

        public ActionResult SearchMrrApprovalList(DateTime? fromdate, DateTime? todate, int mrrNo = 0, int supplierId = 0, int productId = 0)
        {
            List<ViewOthersMrr> aList = _aDal.MrrApprovalList(fromdate, todate, mrrNo, supplierId, productId);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }
        #endregion









        #region ReturnMrr

        public ActionResult ListForMrrReturn(DateTime? fromdate, DateTime? todate, int mrrNO = 0, int supplierId = 0, int itemId = 0)
        {
            List<ViewOthersMrr> aList = _aDal.ListForReturn(fromdate, todate, mrrNO, supplierId, itemId);
            return View(aList);
        }

        public ActionResult SearchListForMrrReturn(DateTime? fromdate, DateTime? todate, int mrrNO = 0, int supplierId = 0, int itemId = 0)
        {
            List<ViewOthersMrr> aList = _aDal.ListForReturn(fromdate, todate, mrrNO, supplierId, itemId);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }
        public ActionResult ReturnMrr(int id)
        {

            ViewBag.MasterId = id;

            return View();
        }
        public JsonResult GetDetailsForMrrReturn(int id)
        {
            ViewOthersMrr aMaster = _aDal.GetMrrInfoByMasterById(id);
            aMaster.ViewOthersMrrDetailsList = _aDal.MrrDetailsforReturnById(id);
            return Json(aMaster, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveMrrReturn(ViewMrrReturnMaster entity)
        {
            try
            {
                string entryBy = Session[SessionCollection.UserName].ToString();
                ResultResponse aResponse = _aDal.SaveMrrReturn(entity, entryBy);
                return Json(aResponse, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public ActionResult ReturnedMrrList(DateTime? fromdate, DateTime? todate, int supplierId = 0, int itemId = 0)
        {
            List<ViewMrrReturnMaster> aList = _aDal.ReturnedMrrList(fromdate, todate, supplierId, itemId);
            return View(aList);
        }
        public ActionResult SearchForReturned(DateTime? fromdate, DateTime? todate, int supplierId = 0, int itemId = 0)
        {
            List<ViewMrrReturnMaster> aList = _aDal.ReturnedMrrList(fromdate, todate, supplierId, itemId);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetReturnedDetailsById(int id)
        {
            List<ViewMrrReturnDetails> aPo = _aDal.MrrReturnDetailsById(id);
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetMRRReturn_forModalPopUpDetailsById(int id)
        {
            List<ViewOthersMrrDetails> aPo = _aDal.MrrDetailsforReturnById(id)
;
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult MrrReturnListReport()
        {

            return View();
        }
        public ActionResult MrrReturnReport(int supplierId, int warehouse, DateTime fromdate, DateTime todate)
        {
            ViewTradeMrrReturnMaster aList = new ViewTradeMrrReturnMaster();


            aList.SupplierID = supplierId;
            aList.DelWareHouseID = warehouse;
            aList.FromDate = fromdate;
            aList.ToDate = todate;


            ResultResponse aResponse = new ResultResponse();
            BasicInfoReportsParameter aBasicInfoReportsParameter = new BasicInfoReportsParameter();
            aBasicInfoReportsParameter.ReportValue = "mrrReturnList";
            Session[SessionCollection.ReportValue] = aBasicInfoReportsParameter;
            Session[SessionCollection.ReportParam] = aList;
            aResponse.isSuccess = true;
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MRRDetailsReport()
        {
            return View();
        }
        public ActionResult GetSupplierMrrDetails(DateTime fromDate, DateTime toDate, int supplierId)
        {
            DataTable aObj = _aDal.GetSupplierMrrDetails(fromDate, toDate, supplierId);
            string JSONresult;
            JSONresult = JsonConvert.SerializeObject(aObj);
            return Json(JSONresult, JsonRequestBehavior.AllowGet);

        }
    }
}