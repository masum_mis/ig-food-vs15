﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;

namespace IGFoodProject.Controllers
{
    public class ChallanByProductController : Controller
    {
        ChallanByProductDAL _challanByProductDal = new ChallanByProductDAL();
        // GET: ChallanProcessed
        public ActionResult Index()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;

            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {

                List<ViewSalesOrderProcessedMaster> aList = _challanByProductDal.GetSalesOrderProcessedForChallan();
                return View(aList);
            }
        }


        public ActionResult GenerateChallan(int id=0)
        {
            string loginUserEmpId = Session["loginuserempid"] as string;

            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                ViewBag.SalesOrder = id;
                return View();
            }
        }

        public ActionResult SavePrecessedChallan(SalesOrderChallanMaster aMaster)
        {
            ResultResponse aResponse = _challanByProductDal.SaveSalesOrderProcessedChallan(aMaster,
                Session[SessionCollection.UserName].ToString());

            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddRowForOrder(int tr)
        {
            TempData["trSl"] = tr;
            return PartialView("_addNewOrderRow");
        }

        public ActionResult GetPreviousChallan(int orderId, int productId)
        {
            ViewPreviousIssue issue = _challanByProductDal.GetPreviousIssue(orderId, productId);
            return Json(issue, JsonRequestBehavior.AllowGet);
        }
    }
}