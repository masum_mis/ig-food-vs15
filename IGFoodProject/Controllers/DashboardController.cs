﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.Utility;
using IGFoodProject.DAL;
using IGFoodProject.Models;

namespace IGFoodProject.Controllers
{
    public class DashboardController : Controller
    {
        // GET: Dashboard
        public ActionResult Index()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;

            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                string userName = Session[SessionCollection.UserName].ToString();
                DashboardDAL aDal = new DashboardDAL();
                List<ApprovalMenuList> aList = aDal.GetApprovalMenuData(userName);
                return View("Index", aList);
            }
           
        }

        public ActionResult MenuGeneration()
        {
            try
            {
                DashboardDAL aDashBoardDal = new DashboardDAL();
                return PartialView("~/Views/Shared/_Partial_Menu_Leyout.cshtml",
                           (List<MenuStepOne>)Session["MenuList"]);
                //return PartialView("~/Views/Shared/_Partial_Menu_Leyout.cshtml",
                //    aDashBoardDal.GetMenuStepOne(Session[SessionCollection.LoginUserId].ToString()));
            }
            catch (Exception exception)
            {
                string message = exception.ToString();

                return RedirectToAction("Index", "Home");
            }
        }
        public ActionResult ApprovalDashBoard()
        {
            //if (AccessAuthoraization.CheckAccess(
            //     Request.RequestContext.RouteData.Values["Controller"].ToString(),
            //     Request.RequestContext.RouteData.Values["Action"].ToString()) == false || Session[SessionCollection.UserName] == null)


            //{
            //    return RedirectToAction("Login", "Home");
            //}

            string userName = Session[SessionCollection.UserName].ToString();
            DashboardDAL aDal = new DashboardDAL();
            List<ApprovalMenuList> aList = aDal.GetApprovalMenuData(userName);
            return View("ApprovalDashBoard", aList);
        }

        public ActionResult VoucherApprovalDashBoard()
        {
            string userName = Session[SessionCollection.UserName].ToString();
            DashboardDAL aDal = new DashboardDAL();
            List<ApprovalMenuList> aList = aDal.GetVoucherApprovalDashboard(userName);
            return View("VoucherApprovalDashBoard", aList);
        }
    }
}