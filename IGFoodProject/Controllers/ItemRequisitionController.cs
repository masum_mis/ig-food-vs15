﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.Utility;
using IGFoodProject.DAL;
using IGFoodProject.Models;

namespace IGFoodProject.Controllers
{
    public class ItemRequisitionController : Controller
    {
        // GET: ItemRequisition
        private ItemRequisitionDal _dal = new ItemRequisitionDal();
        public ActionResult Index()
        {
            return View();
        }
        public async Task<JsonResult> LoadLocation()
        {
            List<Location> locationList = await _dal.LocationInformation();
            return Json(locationList ,JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadWareHouse(int location,int warehouseTypeId)
        {
            List<WareHouse> WareHouseList = _dal.WareHouseInformation(location,warehouseTypeId);
            return Json(WareHouseList, JsonRequestBehavior.AllowGet);
        }

        public dynamic GetAddRow(int tr)
        {
            TempData["trSl"] = tr;
            return PartialView("_partialItemRequisition");
        }

        [HttpPost]
        public ActionResult SaveRequisition(RequisitionMaster aRequisition)
        {

            ResultResponse aResponse = new ResultResponse();
            String createBy = Session[SessionCollection.UserName].ToString();
            DateTime createDate = DateTime.Now;
            aRequisition.CreateBy = createBy;
            aRequisition.CreateDate = createDate;
            aRequisition.UpdateBy = createBy;
            aRequisition.UpdateDate = createDate;
            //aRequisition.RequisitionBy = createBy;
            aRequisition.IsApprove = 0;
            aResponse = _dal.SaveRequisition(aRequisition);
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RequisitionList()
        {
            if (Session[SessionCollection.UserName].ToString() != "")
            {
                List<RequisitionMaster> requisitionMasterList = new List<RequisitionMaster>();
                requisitionMasterList = _dal.RequisitionList();
                return View(requisitionMasterList);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
        }

        public ActionResult EditRequisition(int id = 0)
        {
            if (Session[SessionCollection.UserName].ToString() !="")
            {
                RequisitionMaster requisitionMaster = new RequisitionMaster();
                requisitionMaster = _dal.GetRequisitionById(id);
                ViewBag.Remarks = requisitionMaster.Remarks;
                return View(requisitionMaster);
            }
            else
            {

                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
        }
        public dynamic GetAddRowFEdit(RequisitionDetails requisitionDetails, int tr)
        {
            TempData["trSl"] = tr;
            return PartialView("_partialItemRequisition", requisitionDetails);
        }
        public ActionResult RequisitionApproved(int id = 0)
        {
            if (Session[SessionCollection.UserName].ToString() != "")
            {
                RequisitionMaster requisitionMaster = new RequisitionMaster();
                requisitionMaster = _dal.GetRequisitionById(id);

                return View(requisitionMaster);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
        }
        public ActionResult RequisitionApprovedList()
        {
            if (Session[SessionCollection.UserName].ToString() != "")
            {
                List<RequisitionMaster> requisitionMasterList = new List<RequisitionMaster>();
                requisitionMasterList = _dal.ApprovedRequisitionList();
                return View(requisitionMasterList);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
        }
        public ActionResult ApprovedRequisition(RequisitionMaster aRequisition)
        {

            ResultResponse aResponse = new ResultResponse();
            String createBy = Session[SessionCollection.UserName].ToString();
            DateTime createDate = DateTime.Now;
            aRequisition.ApproveBy = createBy;
            aRequisition.ApproveDate = createDate;
            //aRequisition.RequisitionBy = createBy;
            aRequisition.IsApprove = 1;
            aResponse = _dal.ApprovedRequisition(aRequisition);
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
        public ActionResult RejectRequisition(RequisitionMaster aRequisition)
        {

            ResultResponse aResponse = new ResultResponse();
            String createBy = Session[SessionCollection.UserName].ToString();
            DateTime createDate = DateTime.Now;
            aRequisition.ApproveBy = createBy;
            aRequisition.ApproveDate = createDate;
            aRequisition.IsApprove = 2;
            aResponse = _dal.RejectRequisition(aRequisition);
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LastPriceOfItem(int itemId)
        {
            double unitPrice = 0.00;
            unitPrice = _dal.LastPriceOfItem(itemId);
            return Json(unitPrice,JsonRequestBehavior.AllowGet);
        }

    }
}