﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.Utility;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.ViewModel;

namespace IGFoodProject.Controllers
{
    public class PurchaseOrderController : Controller
    {
        // GET: PurchaseOrder
        private PurchaseOrderDAL _dal = new PurchaseOrderDAL();
        public ActionResult PurchaseOrderList()
        {
            //if (Session[SessionCollection.UserName].ToString() != "")
            //{
            //    List<tbl_PurchaseOrderMaster> requisitionMasterList = new List<tbl_PurchaseOrderMaster>();
            //    requisitionMasterList = _dal.PurchaseOrderList();
            //    return View(requisitionMasterList);
            //}
            //else
            //{
            // return RedirectToAction(nameof(HomeController.Login), "Home");
            //}
            return View();

        }
        public ActionResult PurchaseOrderCloseList()
        {
            
            return View();

        }

        public ActionResult GetPurchaseOrderListByDate(DateTime? fromdate, DateTime? todate, string supplierName = "")
        {
            List<tbl_PurchaseOrderMaster> aList = _dal.GetPurchaseOrderListByDate(fromdate, todate, supplierName);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetPurchaseOrderCloseListByDate(DateTime? fromdate, DateTime? todate, string supplierName = "")
        {
            List<tbl_PurchaseOrderMaster> aList = _dal.GetPurchaseOrderCloseListByDate(fromdate, todate, supplierName);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }
        public ActionResult PurchaseOrderApproveList()
        {
            if (Session[SessionCollection.UserName].ToString() != "")
            {
                return View();
                //List<tbl_PurchaseOrderMaster> requisitionMasterList = new List<tbl_PurchaseOrderMaster>();
                //requisitionMasterList = _dal.PurchaseOrderList();
                //return View(requisitionMasterList);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }

        }

        public ActionResult AddPurchaseOrder(int id = 0, string flag = "ne")
        {

            string loginUserEmpId = Session["loginuserempid"] as string;

            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                ViewBag.orderid = id;
                ViewBag.flagid = flag;
                return View();
            }
        }

        public JsonResult LoadRequisition(int companyId, int warehouseId, DateTime exPurchaseDate)
        {
            List<RequisitionMaster> alist = _dal.RequisitionInformation(companyId, warehouseId, exPurchaseDate);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LoadSupplier()
        {
            List<tbl_SupplierInfo> alist = _dal.SupplierInformation();
            return Json(alist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadSupplierByID(int supplierId)
        {
            tbl_SupplierInfo alist = _dal.SupplierInformationById(supplierId);
            return Json(alist);
        } 

        public JsonResult LoadRequisitionDetailByID(int reqId)
        {
            List<RequisitionDetails> alist = _dal.GetRequisitionDetailByID(reqId);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SavePurchaseOrder(tbl_PurchaseOrderMaster aorder)
        {
            ResultResponse aResponse = new ResultResponse();


            string entryBy = Session[SessionCollection.UserName].ToString();
            aorder.CreateBy = entryBy;
            aorder.UpdateBy = entryBy;
            aorder.ApproveBy = entryBy;
            aResponse.isSuccess = _dal.SavePurchaseOrder(aorder);

            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPurchaseOrderByID(int id)
        {

            return Json(_dal.GetPurchaseOrderForEdit(id), JsonRequestBehavior.AllowGet);

        }

        public JsonResult LoadRequisitionForEdit(int id)
        {
            List<RequisitionMaster> alist = _dal.RequisitionInformationForEdit(id);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadPurchaseDetailForEdit(int id)
        {
            List<tbl_PurchaseOrderDetail> alist = _dal.GetPurchaseDetailByID(id);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }

        #region Purchase Order Report
    
        public ActionResult POReport()
        {
            return View();
        }

        public PartialViewResult LoadPoMasterReport(int company, int location, int wareHouse, string fromDate, string toDate, int status)
        {

            List<IGFoodProject.ViewModel.ViewPurchaseOrderReportMaster> aList = _dal.GetPurchaseOrderMasterReport(company, location, wareHouse, fromDate, toDate, status);



            return PartialView("_poRepoortMaster", aList);
        }
        #endregion

        public ActionResult GetLastFivePriceOfSuplier(int suplierId, int itemId)
        {
            List<tbl_PurchaseOrderDetail> aList = _dal.GetLastFivePriceOfSuplier(suplierId, itemId);
            return PartialView("_PartialSuplierStatus", aList);
        }
        public ActionResult DeletePurchaseOrder(int id)
        {
            ResultResponse aResponse = new ResultResponse();

            tbl_PurchaseOrderMaster aorder = new tbl_PurchaseOrderMaster();

            string entryBy = Session[SessionCollection.UserName].ToString();
            aorder.PurchaseOrderID = id;
            aorder.DeleteBy = entryBy;
            aorder.DeleteDate = DateTime.Now;
            aResponse.isSuccess = _dal.DeletePurchaseOrder(aorder);

            return RedirectToAction(nameof(PurchaseOrderController.PurchaseOrderApproveList), "PurchaseOrder"); ;
        }
        public ActionResult ClosePurchaseOrder(int id)
        {
            ResultResponse aResponse = new ResultResponse();

            tbl_PurchaseOrderMaster aorder = new tbl_PurchaseOrderMaster();

            string entryBy = Session[SessionCollection.UserName].ToString();
            aorder.PurchaseOrderID = id;
            aorder.ClosedBy = entryBy;
            aorder.ClosedDate = DateTime.Now;
            aResponse.isSuccess = _dal.ClosePurchaseOrder(aorder);

            return RedirectToAction(nameof(PurchaseOrderController.PurchaseOrderList), "PurchaseOrder"); ;
        }






        public ActionResult PurchaseOrderInvoiceList()
        {
            //
            //return View(aList);
            return View();
        }

        public ActionResult ShowPurchaseApprovedList(DateTime fromDate, DateTime toDate , int warehouse, int supplier)
        {
            List<ViewPurchaseOrderMaster> aList = _dal.GetPurchaseOrderViewApproved(fromDate , toDate , warehouse , supplier);
            return PartialView("_purchaseInvoiceList", aList);
        }
    }
}