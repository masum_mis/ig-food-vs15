﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using Microsoft.Ajax.Utilities;

namespace IGFoodProject.Controllers
{
    
    public class TradeProcessedCostPriceConfigsController : Controller
    {
        private CodeDbSet db = new CodeDbSet();
         ProductDAL aDal = new ProductDAL();

        // GET: TradeProcessedCostPriceConfigs
        public ActionResult Index()
        {
            return View(db.TradeProcessedCostPriceConfigs.Include(t => t.Product));
        }

        // GET: TradeProcessedCostPriceConfigs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TradeProcessedCostPriceConfig tradeProcessedCostPriceConfig = db.TradeProcessedCostPriceConfigs.Find(id);
            if (tradeProcessedCostPriceConfig == null)
            {
                return HttpNotFound();
            }
            return View(tradeProcessedCostPriceConfig);
        }

        // GET: TradeProcessedCostPriceConfigs/Create
        public ActionResult Create()
        {
            List<Product> products = aDal.GetProductList(0);
            var idList = new[] { 8, 9, 10};
            List<Product> a = products.Where(x => x.IsActive == true && idList.Contains(x.GroupId)).ToList();
            int aa = a.Count;
            ViewBag.ProductId = new SelectList(a.Where(x => x.IsActive == true).ToList(), "ProductId", "ProductName");
            return View();
        }

        // POST: TradeProcessedCostPriceConfigs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TradeProcessedCostPriceId,ProductId,PurchasePrice,CostPrice,IsActive,FromDate,ToDate")] TradeProcessedCostPriceConfig tradeProcessedCostPriceConfig)
        {
            if (ModelState.IsValid)
            {
                db.TradeProcessedCostPriceConfigs.Add(tradeProcessedCostPriceConfig);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ProductId = new SelectList(db.Product, "ProductId", "ProductNo", tradeProcessedCostPriceConfig.ProductId);
            return View(tradeProcessedCostPriceConfig);
        }

        // GET: TradeProcessedCostPriceConfigs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TradeProcessedCostPriceConfig tradeProcessedCostPriceConfig = db.TradeProcessedCostPriceConfigs.Find(id);
            if (tradeProcessedCostPriceConfig == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProductId = new SelectList(db.Product, "ProductId", "ProductName", tradeProcessedCostPriceConfig.ProductId);
            return View(tradeProcessedCostPriceConfig);
        }

        // POST: TradeProcessedCostPriceConfigs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TradeProcessedCostPriceId,ProductId,PurchasePrice,CostPrice,IsActive,FromDate,ToDate")] TradeProcessedCostPriceConfig tradeProcessedCostPriceConfig)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tradeProcessedCostPriceConfig).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ProductId = new SelectList(db.Product, "ProductId", "ProductNo", tradeProcessedCostPriceConfig.ProductId);
            return View(tradeProcessedCostPriceConfig);
        }

        // GET: TradeProcessedCostPriceConfigs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TradeProcessedCostPriceConfig tradeProcessedCostPriceConfig = db.TradeProcessedCostPriceConfigs.Find(id);
            if (tradeProcessedCostPriceConfig == null)
            {
                return HttpNotFound();
            }
            return View(tradeProcessedCostPriceConfig);
        }

        // POST: TradeProcessedCostPriceConfigs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TradeProcessedCostPriceConfig tradeProcessedCostPriceConfig = db.TradeProcessedCostPriceConfigs.Find(id);
            db.TradeProcessedCostPriceConfigs.Remove(tradeProcessedCostPriceConfig);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
