﻿using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Models.ReportModel;
using IGFoodProject.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IGFoodProject.Controllers
{
    public class ItemStockController : Controller
    {
        ItemStockDAL aDal = new ItemStockDAL();
        // GET: ItemStock
        public ActionResult StockReport()
        {
            return View();
        }

        public JsonResult LoadGroup()
        {      
            return Json(aDal.GetGroupList(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetProductByGroup2(int groupId = 0)
        {
            List<Product> aProducts = aDal.GetProductByGroupId2(groupId);
            return Json(aProducts, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ItemStockReports(SalesOrderReport aSalesOrderReport) 
        {
            ResultResponse aResponse = new ResultResponse();
            BasicInfoReportsParameter aBasicInfoReportsParameter = new BasicInfoReportsParameter();
            aBasicInfoReportsParameter.ReportValue = aSalesOrderReport.ReportValue;
            Session[SessionCollection.ReportValue] = aBasicInfoReportsParameter;
            Session[SessionCollection.ReportParam] = aSalesOrderReport;
            aResponse.isSuccess = true;
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }


    }
}