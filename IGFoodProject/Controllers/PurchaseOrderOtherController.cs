﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.Models;
using IGFoodProject.DAL;
using IGFoodProject.Utility;

namespace IGFoodProject.Controllers
{
    public class PurchaseOrderOtherController : Controller
    {
        PurchaseOrderOtherDAL pooDal = new PurchaseOrderOtherDAL();
        private CustomerDAL aDAL = new CustomerDAL();
        private OtherPoRequisitionDal _aDal = new OtherPoRequisitionDal();
        public ActionResult PendingPoRequisition()
        {
            ViewBag.UserId = Session[SessionCollection.UserName].ToString();
            return View();
        }
        public ActionResult GetApprovedRequisition(DateTime? fromdate, DateTime? todate, int warehouse = 0, string reqById = null, string UserId = null)
        {
            List<OtherPoRequisitionMaster> aList = _aDal.ApprovedRequisitionList(fromdate, todate, warehouse, reqById, UserId);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }
        public ActionResult AddRowForPurchase(int tr)
        {
            TempData["trSl"] = tr;

            return PartialView("_addNewGLRow");
        }
        public ActionResult AddRowForPurchaseView(int tr)
        {
            TempData["trSl"] = tr;

            return PartialView("_addNewRowView");
        }
        public ActionResult Create(int PurchaseId = 0, string eflag = "ed",int requisitionId=0)
        {
            ViewBag.PurchaseId = PurchaseId;
            ViewBag.eflag = eflag;
            ViewBag.RequisitionId = requisitionId;
            ViewBag.UserId = Session[SessionCollection.UserName].ToString();
            return View();
        }
        public ActionResult LoadUOM()
        {
            List<tbl_UOM> uom = aDAL.GetUOMList();
            return Json(uom, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LoadSupplier()
        {
            List<tbl_SupplierInfo> alist = pooDal.SupplierInformation();
            return Json(alist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadSupplierByType(string type)
        {
            List<tbl_SupplierInfo> alist = pooDal.SupplierInformationByType(type);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetItem()
        {
            List<ItemDescription> cu = pooDal.GetItem();
            return Json(cu, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetItemDetails(int itemId)
        {
            ItemDescription aDetails = pooDal.GetItemDetails(itemId);
            return Json(aDetails, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SavePurchaseOrderOther(PurchaseOrderOtherMaster aMaster)
        {
            ResultResponse aResponse = pooDal.SavePurchaseOrderOther(aMaster, Session[SessionCollection.UserName].ToString());
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetPurchaseOrderOtherByMasterId(int id)
        {
            PurchaseOrderOtherMaster aMaster = pooDal.GetPurchaseOrderOtherMasterById(id);
            aMaster.POOtherDetails = pooDal.GetPurchaseOrderOtherDetailsById(id);
            aMaster.TermsAndconditions = pooDal.GetTermsAndconditionById(id);
            return Json(aMaster, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Index(DateTime? fromdate, DateTime? todate, int SupplierId = 0, int DelWareHouseId = 0)
        {
            List<PurchaseOrderOtherMaster> aList = pooDal.GetPurchaseOrderOtherMaster(fromdate, todate, SupplierId, DelWareHouseId);
            return View(aList);
        }
        public ActionResult PurchaseOrderListData(DateTime? fromdate, DateTime? todate, int SupplierId = 0, int DelWareHouseId = 0)
        {
            List<PurchaseOrderOtherMaster> aList = pooDal.GetPurchaseOrderOtherMaster(fromdate, todate, SupplierId, DelWareHouseId);
            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetTermsAndConditionDefault()
        {
            TermsAndcondition tmaster = pooDal.GetTermsAndConditionDefault();
            return Json(tmaster, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PurchaseOrderOtherApprovalList(DateTime? fromdate, DateTime? todate, int SupplierId = 0, int DelWareHouseId = 0)
        {
            List<PurchaseOrderOtherMaster> aList = pooDal.GetPurchaseOrderOtherMasterForApproval(fromdate, todate, SupplierId, DelWareHouseId);
            return View(aList);
        }
        public ActionResult PurchaseOrderListForApprovalData(DateTime? fromdate, DateTime? todate, int SupplierId = 0, int DelWareHouseId = 0)
        {
            List<PurchaseOrderOtherMaster> aList = pooDal.GetPurchaseOrderOtherMasterForApproval(fromdate, todate, SupplierId, DelWareHouseId);
            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PurchaseOrderOtherMultiplePrint(DateTime? fromdate, DateTime? todate, int SupplierId = 0, int DelWareHouseId = 0)
        {
            List<PurchaseOrderOtherMaster> aList = pooDal.GetPurchaseOrderOtherMasterApprove(fromdate, todate, SupplierId, DelWareHouseId);
            return View(aList);
        }
        public ActionResult PurchaseOrderListForApproveData(DateTime? fromdate, DateTime? todate, int SupplierId = 0, int DelWareHouseId = 0)
        {
            List<PurchaseOrderOtherMaster> aList = pooDal.GetPurchaseOrderOtherMasterApprove(fromdate, todate, SupplierId, DelWareHouseId);
            return Json(aList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ViewPurchaseOrder(int id)
        {
            return PartialView("PurchaseOrderView");
        }
    }
}