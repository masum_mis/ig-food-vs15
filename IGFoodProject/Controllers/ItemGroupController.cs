﻿using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IGFoodProject.Controllers
{
    public class ItemGroupController : Controller
    {
        ItemGroupDAL aDAL = new ItemGroupDAL();
        // GET: ItemGroup
        public ActionResult AddItemGroup(int id = 0) 
        {
            ViewBag.ItemGroupId = id;
            return View();
        }

        public ActionResult ItemGroupList()
        {
            List<ItemGroup> alist = aDAL.GetItemGroupList();
            return View(alist);
        }
        public JsonResult CheckItemGroup(string group)
        {    
            int desig = aDAL.CheckItemGroup(group);
            return Json(desig, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveItemGroup(ItemGroup aGroup)
        {
            ResultResponse aResponse = new ResultResponse();         

            string entryBy = Session[SessionCollection.UserName].ToString();
            aGroup.CreateBy = entryBy;
            aGroup.UpdateBy = entryBy;
            aResponse.isSuccess = aDAL.SaveItemGroup(aGroup);
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteItemGroup(int id)
        {
            ResultResponse aResponse = new ResultResponse();          
            aResponse.isSuccess = aDAL.DeleteItemGroup(id); 
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetItemGroupByID(int id) 
        {
            return Json(aDAL.GetItemGroupByID(id), JsonRequestBehavior.AllowGet);

        }

    }
}