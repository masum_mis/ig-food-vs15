﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Models.ReportModel;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;

namespace IGFoodProject.Controllers
{
    public class TopSheetChallanController : Controller
    {
        // GET: TopSheetChallan

            TopSheetDAL _aDal = new TopSheetDAL();
        
        public ActionResult Index()
        {
            return View();
        }




        public ActionResult SearchToSheet(int com, int wh, int vh, DateTime delDate)
        {
            List<ViewChallanTopSheet> aList = _aDal.GetTopSheet(com, wh, vh, delDate);
            return PartialView("_searchResultTopSheet" , aList);
        }

        public ActionResult GenerateTopSheet(SalesOrderReport aSalesOrderReport)
        {
            ResultResponse aResponse = new ResultResponse();
            BasicInfoReportsParameter aBasicInfoReportsParameter = new BasicInfoReportsParameter();
            aBasicInfoReportsParameter.ReportValue = aSalesOrderReport.ReportValue;
            Session[SessionCollection.ReportValue] = aBasicInfoReportsParameter;
            Session[SessionCollection.ReportParam] = aSalesOrderReport;
            aResponse.isSuccess = true;
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

    }



}