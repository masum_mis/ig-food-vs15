﻿using IGFoodProject.DAL;
using IGFoodProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.Utility;

namespace IGFoodProject.Controllers
{
    public class CreditController : Controller
    {
        CreditDAL aDal = new CreditDAL();
        // GET: Credit
        public ActionResult CreditEntry()
        {
            if (Session[SessionCollection.LoginUserEmpId] != null)
            {
                return View();
            }

            return RedirectToAction(nameof(HomeController.Login), "Home");
        }

        public JsonResult LoadCustomerType()
        {
            List<CustomerCredit> bu = aDal.LoadCustomerType();
            return Json(bu, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadCustomerByType(int customerTypeId)
        {
            List<CustomerCredit> bu = aDal.LoadCustomerByType(customerTypeId, 0);
            return Json(bu, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadCustomer(int CustomerTypeID, int CustomerNameId)
        {
            List<CustomerCredit> _customerCredits = aDal.LoadCustomerByType(CustomerTypeID, CustomerNameId);
            return PartialView("_PartialLoadCustomer", _customerCredits);
        }

        public ActionResult SaveMonthlyCredit(List<MonthlyCreditM> _monthlyCredit)
        {
            string usrName = Session[SessionCollection.UserName].ToString();
            ResultResponse aResponse = new ResultResponse();
            aResponse.isSuccess = aDal.SaveMonthlyCredit(_monthlyCredit, usrName);
            return Json(aResponse, JsonRequestBehavior.AllowGet);

        }


    }
}