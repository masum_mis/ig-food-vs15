﻿using IGFoodProject.DAL;
using IGFoodProject.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.ViewModel;
using IGFoodProject.Utility;

namespace IGFoodProject.Controllers
{
    public class PortionStockInController : Controller
    {
        private PortionStockInDAL _dal = new PortionStockInDAL();
        // GET: PortionStockIn
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AddPortionStockIn()
        {
            
            return View();
        }
        public JsonResult LoadIssue(int warehouseid)
        {
            List<DBIssuePortionMaster> alist = _dal.IssueInformation(warehouseid);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadIssueDetail(int DBIssueMasterID)
        {
            List<ViewTargetPortionInfo> alist = _dal.IssueDetail(DBIssueMasterID);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult LoadProduct()
        {
            List<ViewProductDescription> alist = _dal.LoadProduct();
            return Json(alist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LoadAllProduct()
        {
            List<ViewProductDescription> alist = _dal.LoadAllProduct();
            return Json(alist, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SavePortionStock(tbl_StockInMaster aorder)
        {
            ResultResponse aResponse = new ResultResponse();
           
            string entryBy = Session[SessionCollection.UserName].ToString();
            aorder.CreateBy = entryBy;
           
            aResponse.isSuccess = _dal.SavePortionStock(aorder);

            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAddRow(int tr)
        {
            TempData["trSl"] = tr;
            return PartialView("_partialPortionStockInDetail");

        }

        public ActionResult PortionStockReport()
        {
            return View();
        }

        public ActionResult GetPortionStockList( tbl_StockInMaster aMaster)
        {
            DataTable dt = _dal.LoadPortionStockList(aMaster);

            return PartialView("_PartialPortionStockReport", dt);
        }
    }
}