﻿using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IGFoodProject.Controllers
{
    public class ByProductStockInController : Controller
    {
        private ByProductStockInDAL _dal = new ByProductStockInDAL();
        // GET: ByProductStockIn
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AddByProductStockIn()
        {

            return View();
        }
        public JsonResult LoadIssue(int WarehouseId)
        {
            List<DBIssuePortionMaster> alist = _dal.IssueInformation(WarehouseId);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadDressMaster(int WarehouseId)
        {
            List<DressedBirdMaster> alist = _dal.DressbirdInformation(WarehouseId);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadIssueDetail(int DBIssueMasterID)
        {
            List<ViewTargetPortionInfo> alist = _dal.IssueDetail(DBIssueMasterID);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LoadProduct()
        {
            List<ViewProductDescription> alist = _dal.LoadProduct();
            return Json(alist, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveByProductStock(tbl_StockInMaster aorder)
        {
            ResultResponse aResponse = new ResultResponse();

            string entryBy = Session[SessionCollection.UserName].ToString();
            aorder.CreateBy = entryBy;

            aResponse.isSuccess = _dal.SaveByProductStock(aorder);

            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAddRow(int tr)
        {
            TempData["trSl"] = tr;
            return PartialView("_partialByProductStockInDetail");

        }
    }
}