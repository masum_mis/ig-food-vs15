﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;
using Newtonsoft.Json;
using System.Web.Configuration;

namespace IGFoodProject.Controllers
{
    public class ChallanProcessedController : Controller
    {

        ChallanProcessedDAL _challanProcessedDal = new ChallanProcessedDAL();
        SalesOrderProcessedDAL _sakeOrderProcessedDal = new SalesOrderProcessedDAL();
        // GET: ChallanProcessed
        public ActionResult Index()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                List<ViewSalesOrderProcessedMaster> aList = _challanProcessedDal.GetSalesOrderProcessedForChallan(loginUserEmpId);
                return View(aList);
            }
        }


        //public ActionResult GenerateChallan(int id)D:\ig-food-vs15\IGFoodProject\Controllers\ChallanProcessedController.cs
        //{
        //    ViewBag.SalesOrder = id;


        //    return  View();
        //}
        public ActionResult ProcessedChallan(int id, int pickId)
        {
            ViewBag.SalesOrder = id;
            ViewBag.PickMaster = pickId;


            return View();
        }

        public ActionResult GetPickigMasterById(int id)
        {
            PickingProcessDAL aDal = new PickingProcessDAL();
            PickingRequestMaster dt = aDal.GetPickingInformation(id);
            return Json(dt, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPickigDetails(int id, int soId)
        {
            PickingProcessDAL aDal = new PickingProcessDAL();
            DataTable dt = aDal.GetPickingDetailsById(id, soId);

            string JSONString = JsonConvert.SerializeObject(dt);
            return Json(JSONString, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SavePrecessedChallan(SalesOrderChallanMaster aMaster)
        {
            ResultResponse aResponse = _challanProcessedDal.SaveSalesOrderProcessedChallan(aMaster,
                Session[SessionCollection.UserName].ToString());

            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetProductDetails(int productId, int wareHouseId, int customerType, DateTime orderDate,
            DateTime challanDate)
        {
            ViewProductDetailsSales aDetails = _challanProcessedDal.GetProductDetailsForChallan(productId, wareHouseId,
                customerType, orderDate, challanDate);
            return Json(aDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddRowForOrder(int tr)
        {
            TempData["trSl"] = tr;
            return PartialView("_addNewOrderRow");
        }

        public ActionResult GetPreviousChallan(int orderId, int productId)
        {
            ViewPreviousIssue issue = _challanProcessedDal.GetPreviousIssue(orderId, productId);
            return Json(issue, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GeneratedChallan()
        {
            List<ViewGeneratedChallan> aList = _challanProcessedDal.GetGeneratedChallan();
            return View(aList);
        }

        public ActionResult ViewChallan(int id)
        {

            ViewGeneratedChallan aChallan = _challanProcessedDal.GetGeneratedChallan(id)[0];
            aChallan.Details = _challanProcessedDal.GetGeneratedChallanDetails(id).ToList();
            return View(aChallan);
        }


        public ActionResult LoadProductDetails(int warehouse, DateTime? orderdate, int productId)
        {
            ViewProductDetailsSales aDetailsSales = _sakeOrderProcessedDal.GetProductDetails(productId, orderdate,
                warehouse);
            return Json(aDetailsSales, JsonRequestBehavior.AllowGet);
        }


        public ActionResult AddChallanProductRow(int sl)
        {
            TempData["trSl"] = sl;
            return PartialView("_addNewChallanRow");
        }

        public ActionResult challanchaeck(int so_id, int pick_id)
        {

            string isso =
                _challanProcessedDal.GetSOCheck(so_id, pick_id);
            return Json(isso, JsonRequestBehavior.AllowGet);
        }


        #region  Delivery Confirm

        public ActionResult ChallanListForDeliveryConfirm_Processed()
        {

            List<ChallanListForDelivery> aList = _challanProcessedDal.GetChallanListForDeliveryConfirm();
            return View("ChallanListForDeliveryConfirm", aList);
        }

        public ActionResult DeliveryConfirm(int id)
        {
            ViewBag.challanno = id;

            DateTime challandate =
                _challanProcessedDal.GetChallanDate(id);
            if (challandate > Convert.ToDateTime("2020-11-01") )
            {
                return View("DeliveryConfirmNew");
            }
            else
            {
                return View("DeliveryConfirm");
            }

           
        }

        public ActionResult GetChallanMaster(int id)
        {
            ViewChallanDetailForDelivery aMaster = _challanProcessedDal.GetChallanProcessedMaster(id);
            aMaster.SalesDetails = _challanProcessedDal.GetChallanDetailsByMaster(id);
            return Json(aMaster, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetChallanMaster_New(int id)
        {
            ViewChallanDetailForDelivery aMaster = _challanProcessedDal.GetChallanProcessedMaster(id);
            aMaster.SalesDetails = _challanProcessedDal.GetChallanDetailsByMaster_New(id);
            return Json(aMaster, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddRowForDelivery(int tr)
        {
            TempData["trSl"] = tr;
            return PartialView("_addNewOrderRow_delivery");
        }

        public ActionResult SavePrecessedDelivery(SalesOrderChallanMaster aMaster)
        {
            ResultResponse aResponse = _challanProcessedDal.SaveChallanDelivery(aMaster,
                Session[SessionCollection.UserName].ToString());

            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SavePrecessedDelivery_New(SalesOrderChallanMaster aMaster)
        {
            ResultResponse aResponse = _challanProcessedDal.SaveChallanDelivery_New(aMaster,
                Session[SessionCollection.UserName].ToString());

            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        #endregion

        public ActionResult DeliveryRoleBack()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {

                return View();
            }
        }

        public ActionResult GetAllPickingDetailsList(DateTime fromDate, DateTime toDate, int flag)
        {
            DataTable dt = _challanProcessedDal.DeliveryListForRoleBack(fromDate, toDate, flag);

            return PartialView("_PartialDeliveryRoleBack", dt);
        }

        public ActionResult RoleBack(string id, int type)
        {
            bool aResponse = _challanProcessedDal.UpdateChallanMasterRoleBack(id, type,
                Session[SessionCollection.UserName].ToString());

            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetRoleBackDetails(int id,int type)
        {
           DataTable dt = _challanProcessedDal.GetChallanSummery(id,type);


            return PartialView("_PartialRoleBackSummery", dt);

        }

        public ActionResult ConfirmedChallan()
        {
            List<ViewGeneratedChallan> aList = _challanProcessedDal.GetConfirmedChallan();
            return View(aList);
        }

        public ActionResult ViewConfirmedChallan(int id)
        {

            ViewGeneratedChallan aChallan = _challanProcessedDal.GetConfirmedChallan(id)[0];
            aChallan.Details = _challanProcessedDal.GetConfirmedChallanDetails(id).ToList();
            return View(aChallan);
        }

        public ActionResult SalesReturn()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult LoadChallanNoByCustomer(int customerId)
        {
            List<SalesOrderChallanMaster> aPo = _challanProcessedDal.LoadChallanNoByCustomer(customerId);
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetChallanDetailsById(int customerId, int challanMasterId)
        {

            DataTable aObj = _challanProcessedDal.GetChallanDetailsById(customerId, challanMasterId);
            string jSONresult = JsonConvert.SerializeObject(aObj);


            return Json(jSONresult, JsonRequestBehavior.AllowGet);

        }
        public ActionResult SavePrecessedReturn(List<ViewGeneratedChallanDetails> challanDetails, ViewGeneratedChallanDetails ObjData)
        {
            ObjData.FileFolderLocation = WebConfigurationManager.AppSettings["FilePath"];
            ResultResponse aResponse = _challanProcessedDal.SavePrecessedReturn(challanDetails, ObjData,Session[SessionCollection.UserName].ToString());
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public JsonResult uploadFile()
        {
            // check if the user selected a file to upload
            var PK = Request.Form["PK"];
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;
                    HttpPostedFileBase file = files[0];
                    string fileName = file.FileName;
                    
                    // create the uploads folder if it doesn't exist
                    string grandParent = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName;

                    //Directory.CreateDirectory(Server.MapPath("~/uploads/"));
                    string path = Path.Combine(grandParent + "\\SalesReturnFile", PK+"-"+ fileName);
                  
                    file.SaveAs(path );
                    ResultResponse aResponse = _challanProcessedDal.UpdateFilePath(path, PK);
                    return Json("File uploaded successfully");
                }

                catch (Exception e)
                {
                    return Json("error" + e.Message);
                }
            }

            return Json("no files were selected !");
        }


        //public ActionResult uploadFile()
        //{
        //    //FileUploadDAL fud = new FileUploadDAL();
        //    // Checking no of files injected in Request object  
        //    if (Request.Files.Count > 0)
        //    {

        //        try
        //        {
        //            var tempFileOldName = Request.Form["FileOldName"];
        //            var newFileName = Request.Form["FileNewNames"];

        //            HttpFileCollectionBase files = Request.Files;


        //            for (int i = 0; i < files.Count; i++)
        //            {
        //                HttpPostedFileBase file = files[i];

        //                string fname = file.FileName;


        //                string extension;
        //                //int primaryKey = 0;


        //                string temp = fname;



        //                //Getting Extension of the file

        //                extension = Path.GetExtension(fname);

        //                int positonOfextension = temp.LastIndexOf(".");
        //                if (positonOfextension > 0)
        //                {
        //                    temp = temp.Substring(0, positonOfextension);
        //                }


        //                var fileOldName = tempFileOldName[i];


        //                ////Saving info to db 
        //                //if (fname == fileOldName)
        //                //{
        //                //    primaryKey = fud.SaveFileInfo(temp, extension, FileNewNames[i], FileDetails[i], "Shaon");
        //                //}






        //                fname = fname.Replace(fname, newFileName + extension);






        //                string grandParent = new DirectoryInfo(Server.MapPath("~/")).Parent.FullName;
        //                //var parentDir = Directory.GetParent(Server.MapPath("~/Uploads/"));
        //                fname = Path.Combine(grandParent + "\\EmployeeEducationFile", fname);



        //                file.SaveAs(fname);
        //            }
        //            // Returns message that successfully uploaded  
        //            return Json("1");
        //        }
        //        catch (Exception ex)
        //        {
        //            return Json("0");
        //        }
        //    }
        //    else
        //    {
        //        return Json("2");
        //    }




        //}

    }

}