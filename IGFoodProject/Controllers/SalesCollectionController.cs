﻿using System.Collections.Generic;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;
using System.Data;
using System;
using System.Linq;
using IGFoodProject.Models.Accounts;
using Newtonsoft.Json;

namespace IGFoodProject.Controllers
{
    public class SalesCollectionController : Controller
    {
        SalesCollectionDAL _salesCollectionDAL = new SalesCollectionDAL();
        // GET
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SalesCollectionEntry()
        {
            ViewBag.UserId = Session[SessionCollection.UserName].ToString();
            return View();
        }
        public ActionResult SalesOrderListForPaymentCollection(int customerId, string reportType, DateTime paymentDate)
        {
            List<SalesOrderChallanMaster> aList = _salesCollectionDAL.ChallanListForPaymentCollection(customerId, paymentDate);
            //List<SalesOrderChallanMaster> aList1 = reportType == "due" ? aList.Where(l => l.restamount > 1).ToList() : aList;
            List<SalesOrderChallanMaster> aList1 = reportType == "due" ? aList.Where(l => l.restamount > 0).ToList() : aList;


            return PartialView("_salesOrderPaymentCollection", aList1);
        }

        public ActionResult SalesOrderLocationListForPaymentCollection(int customerId, string reportType, DateTime paymentDate, int locationId)
        {
            List<SalesOrderChallanMaster> aList = _salesCollectionDAL.ChallanLocationListForPaymentCollection(customerId, paymentDate, locationId);
            List<SalesOrderChallanMaster> aList1 = reportType == "due" ? aList.Where(l => l.restamount > 1).ToList() : aList;


            return PartialView("_salesOrderPaymentCollection", aList1);
        }

        public ActionResult SaveSalesCollection(SalesCollectionMaster salesCollectionMaster)
        {
            ResultResponse aResponse = new ResultResponse();
            SalesCollectionDAL aDal = new SalesCollectionDAL();
            salesCollectionMaster.CreateBy = Session[SessionCollection.UserName].ToString();
            aResponse.isSuccess = aDal.SaveSalesCollectionDetails(salesCollectionMaster);
            return Json(aResponse, JsonRequestBehavior.AllowGet);
           
        }

        public ActionResult SavePreviousSalesCollection(SalesCollectionMaster salesCollectionMaster)
        {
            ResultResponse aResponse = new ResultResponse();
            SalesCollectionDAL aDal = new SalesCollectionDAL();
            salesCollectionMaster.CreateBy = Session[SessionCollection.UserName].ToString();
            aResponse.isSuccess = aDal.SavePreviousSalesCollection(salesCollectionMaster);
            return Json(aResponse, JsonRequestBehavior.AllowGet);

        }

        public ActionResult SaveAdvancedCollection(SalesCollectionMaster salesCollectionMaster)
        {
            ResultResponse aResponse = new ResultResponse();
            SalesCollectionDAL aDal = new SalesCollectionDAL();
            salesCollectionMaster.CreateBy = Session[SessionCollection.UserName].ToString();
            aResponse.isSuccess = aDal.SaveAdvancedCollection(salesCollectionMaster);
            return Json(aResponse, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetPreviousSalesCollection(int challanMasterId)
        {
           


            List<SalesCollectionDetail> aList =
                _salesCollectionDAL.GetPreviousCollectionDetails(challanMasterId);
            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        

        public ActionResult AllCustomerLedger()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }

        public ActionResult GetAllCustomerLedger(DateTime fromDate, DateTime toDate, string customerTypeId)
        {

            DataTable aList = _salesCollectionDAL.GetAllCustomerLedger(fromDate, toDate, customerTypeId);
            return PartialView("_PartialAllCustomerLedger", aList);
        }
        public ActionResult CustomerLedger()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult GetCustomerLedger(int customerId, DateTime fromDate, DateTime toDate)
        {

            DataTable aList = _salesCollectionDAL.GetCustomerLedger(customerId, fromDate, toDate);
            return PartialView("_PartialCustomerLedger", aList);
        }

        public ActionResult PrintMoneyReceipt()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult GetCustomerPaymentList(int rpttype, DateTime? collfromDate, DateTime? colltoDate, DateTime? createfromDate,DateTime? createtoDate,int? customerId, string salesperson="")
        {

            DataTable aList = _salesCollectionDAL.GetCustomerPaymentList(rpttype,collfromDate, colltoDate,createfromDate,createtoDate,customerId,salesperson);
            return PartialView("_PartialCustomerInvoice", aList);
        }

        public ActionResult SrSalary(int delarId)
        {
            DataTable aList = _salesCollectionDAL.GetSdrSalary(delarId);


            return PartialView("_PartialSrSalary", aList);
        }
        public ActionResult RetaintionAdjust(int delarId)
        {
            DataTable aList = _salesCollectionDAL.GetRetaintionAdjust(delarId);
            return PartialView("_PartialRetaintionAdjust", aList);
        }
        public ActionResult DamageAdjust(int delarId)
        {
            DataTable aList = _salesCollectionDAL.GetDamageAdjust(delarId);
            return PartialView("_PartialDamageAdjust", aList);
        }

        public ActionResult PromotionAdjust(int delarId)
        {
           
            return PartialView("_PartialPromotionAdjust");
        }
        public ActionResult FreezerRentAdjust(int delarId)
        {
           
            return PartialView("_PartialRentAdjust");
        }

        public ActionResult ChaldalAdjust(int delarId)
        {

            return PartialView("_PartialChaldalAdjust");
        }
        public ActionResult SalesReturnAdvAdjust(int delarId)
        {
            DataTable aList = _salesCollectionDAL.GetSalesReturnAdvAdjust(delarId);
            return PartialView("_PartialSalesReturnAdvAdjust", aList);
        }

        public ActionResult CustomerAcLedger()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }

        public ActionResult GetCustomerACLedger(int customerId, DateTime fromDate, DateTime toDate)
        {
            DataTable aList = _salesCollectionDAL.GetCustomerAcLedger(customerId,fromDate,toDate);
            return PartialView("_PartialAcLedger", aList);
        }
        public ActionResult SalesCollectionDayBook()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult GetDayBookReport(int companyId,  DateTime fromDate)
        {
           
            DataTable dt = _salesCollectionDAL.GetDayBookReport(companyId,fromDate);

            return PartialView("_daybookreport", dt);
        }

        public ActionResult SalesPersonWiseCustomerList()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }

        public ActionResult GetSalesPersonwisecustomerReport(int companyId, DateTime fromDate, DateTime toDate, string salesperson)
        {

            DataTable dt = _salesCollectionDAL.GetSalesPersonWiseCustomerList(companyId, fromDate, toDate, salesperson);

            return PartialView("GetSalesPersonwisecustomerReport", dt);
        }

        public ActionResult CollectionVoid()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult GetCollectioninfoForVoid(int customerId, DateTime paymentDate)
        {

            DataTable aObj = _salesCollectionDAL.GetCollectioninfoForVoid(customerId, paymentDate);
            string jSONresult = JsonConvert.SerializeObject(aObj);


            return Json(jSONresult, JsonRequestBehavior.AllowGet);

        }

        public ActionResult SaveCollectionVoid(SalesCollectionMaster entity)
        {
            try
            {
                string entryBy = Session[SessionCollection.UserName].ToString();
                ResultResponse aResponse = _salesCollectionDAL.SaveCollectionVoid(entity, entryBy);
                if (aResponse.isSuccess == true)
                {
                    aResponse.pk = 1;
                }
                return Json(aResponse, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult CustomerVoucherLedger()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult GetCustomerVoucherLedger(DateTime fromDate, DateTime toDate,int glId,int refDetailsId=0)
        {
            DataTable aList = _salesCollectionDAL.GetCustomerVoucherLedger(fromDate, toDate, glId, refDetailsId);
            return PartialView("_PartialCustomerVoucherLedger", aList);
        }

        public ActionResult CustomerTdsVoucherLedger()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult GetCustomerTdsVoucherLedger(DateTime fromDate, DateTime toDate, int glId, int refDetailsId = 0, int sourchId = 0)
        {
            SupplierPaymentOtherDAL dal = new SupplierPaymentOtherDAL();
            DataTable aList = dal.GetSupplierTdsVoucherLedger(fromDate, toDate, glId, refDetailsId, sourchId);
            return PartialView("_PartialCustomerTdsVoucherLedger", aList);
        }


        public ActionResult SubLedgerTrialBalance() 
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }

        public ActionResult GetGLByGlType()
        {
            List<ChartOfAccLayerSeven> cu = _salesCollectionDAL.GetGLByGlType();
            return Json(cu, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetSubLedgerReport(DateTime fromDate, DateTime toDate, int glId)   
        {
            DataTable aList = _salesCollectionDAL.GetSubLedgerReport(fromDate, toDate, glId); 
            return PartialView("_PartialSubLedgerTrialBalance", aList);
        }


    }
}