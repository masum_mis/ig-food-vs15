﻿using IGFoodProject.DAL.Barcode;
using IGFoodProject.Models;
using IGFoodProject.Models.Barcode;
using IGFoodProject.Models.ReportModel;
using IGFoodProject.Utility;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net.Http;
using System.Web.Mvc;
using System.Windows;
using ZXing;
using ZXing.Common;


namespace IGFoodProject.Controllers
{
    public class BarCodeController : Controller
    {
        // GET: BarCode

        BarcodeDAL dal = new BarcodeDAL();



        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]


        public JsonResult CreateProductBarCode() 
        {
            ResultResponse aResponse = new ResultResponse();

            List<BarcodeProduct> productList = dal.GetProductList();

            foreach (var product in productList)
            {
                BarcodeProduct objprod = new BarcodeProduct();

                objprod.ProductId = product.ProductId;
                objprod.Barcode = product.ProductId.ToString()+ product.GroupId.ToString() + product.TypeId.ToString() + product.CategoryId.ToString() + product.PackSizeId.ToString() + product.UOMId.ToString();
               
                
                // Generate barcode
                var writer = new BarcodeWriter
                {
                    Format = BarcodeFormat.CODE_128,
                    Options = new EncodingOptions
                    {
                        Height = 120,
                        Width = 300,
                        Margin = 10
                    }
                };

                //using (var bitmap = writer.Write(text))
                //using (var stream = new MemoryStream())
                //{
                //    bitmap.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                //    objprod.BarcodeImage = stream.ToArray();
                //}

                using (var barcodeBitmap = writer.Write(objprod.Barcode))
                {
                    int topMargin = 20; // Set your desired top margin here

                    // Create a new bitmap with the additional top margin
                    using (var finalBitmap = new Bitmap(barcodeBitmap.Width, barcodeBitmap.Height + topMargin))
                    {
                        using (var graphics = Graphics.FromImage(finalBitmap))
                        {
                            graphics.FillRectangle(Brushes.White, 0, 0, finalBitmap.Width, finalBitmap.Height); // Optional: Fill the background with white
                            graphics.DrawImage(barcodeBitmap, 0, topMargin);
                        }

                        // Save the final bitmap to a memory stream
                        using (var stream = new MemoryStream())
                        {
                            finalBitmap.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                            objprod.BarcodeImage = stream.ToArray();
                        }
                    }
                }


                // Save the barcode image to the database
                aResponse.isSuccess = dal.SaveBarcode(objprod);
            }

            aResponse.isSuccess = true;

            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
        
        
        public ActionResult ProductList()
        {
            List<BarcodeProduct> alist = dal.GetProductListWithBarcode();
            return View(alist);
        }



        public ActionResult DownloadExcel()
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            var productList = dal.GetProductListWithBarcode();

            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Product Barcode List");
                worksheet.Cells[1, 1].Value = "Product Name";
                worksheet.Cells[1, 2].Value = "Product No";
                worksheet.Cells[1, 3].Value = "Barcode";

                // Set the column width and row height
                worksheet.Column(3).Width = 15; // Adjust the column width as needed

                int row = 2;
                int imageIndex = 1;
                foreach (var product in productList)
                {
                    worksheet.Cells[row, 1].Value = product.Name;
                    worksheet.Cells[row, 2].Value = product.Barcode;

                    if (!string.IsNullOrEmpty(product.ImageUrl))
                    {
                        var base64Data = product.ImageUrl.Substring(product.ImageUrl.IndexOf(',') + 1);
                        var imageBytes = Convert.FromBase64String(base64Data);

                        using (var streams = new MemoryStream(imageBytes))
                        {
                            var picture = worksheet.Drawings.AddPicture($"Image_{imageIndex}", streams);
                            picture.SetPosition(row - 1, 0, 2, 0);
                            picture.SetSize(150, 60); // Set the size of the image to passport size (e.g., 100x150 pixels)
                            imageIndex++;
                        }
                    }
                    // Adjust the row height to fit the image
                    worksheet.Row(row).Height = 100; // Adjust the row height as needed
                    row++;
                }

                var stream = new MemoryStream();
                package.SaveAs(stream);
                stream.Position = 0;

                var fileName = "ProductBarcodeList.xlsx";
                var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

                return File(stream, contentType, fileName);
            }
        }


        public ActionResult DownloadPDF()
        {
            ResultResponse aResponse = new ResultResponse();
            BasicInfoReportsParameter aBasicInfoReportsParameter = new BasicInfoReportsParameter();
            aBasicInfoReportsParameter.ReportValue = "BarcodePdf";
            Session[SessionCollection.ReportValue] = aBasicInfoReportsParameter;
            aResponse.isSuccess = true;
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }



    }
}