﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Models.ReportModel;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;
namespace IGFoodProject.Controllers
{
    public class InvoiceBirdPurchaseController : Controller
    {
        InvoiceBirdPurchaseDal _aDal = new InvoiceBirdPurchaseDal();
        // GET: InvoiceBirdPurchase
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult InvoiceEntry()
        {
            return View();
        }
        public ActionResult MrrList()
        {
            return View();
        }
        public ActionResult SearchMrrList(DateTime? fromdate, DateTime? todate, int warehouseId = 0)
        {
            List<ViewMrrMaster> aList = _aDal.GetMrrList(fromdate, todate, warehouseId);
            return Json(aList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadMrrInfo(int supplierId)
        {
            List<ViewInvoiceMaster> aPo = _aDal.LoadMrrInfo(supplierId);
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveBill(ViewInvoiceMaster entity)
        {
            try
            {
                string entryBy = Session[SessionCollection.UserName].ToString();
                ResultResponse aResponse = _aDal.SaveBill(entity, entryBy);
                return Json(aResponse, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult GetMRRDetailsById(int id)
        {
            List<ViewMrrDetails> aPo = _aDal.GetMrrDetailsForEdit(id)
;
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LoadInvoiceNo()
        {
            List<ViewInvoiceMaster> aPo = _aDal.LoadInvoiceNo();
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetGeneratedInvoices(int supplierId = 0, int invoiceId = 0, DateTime? fromDate = null, DateTime? toDate = null)
        {
            List<ViewInvoiceMaster> aPo = _aDal.GetGeneratedInvoices(supplierId, invoiceId, fromDate, toDate);
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }
        public ActionResult InvoiceDetailsById(int id)
        {
            List<ViewInvoiceDetails> aPo = _aDal.InvoiceDetailsById(id);
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }
        public ActionResult MultipleInvoiceReport(ViewInvoiceMaster aDirrectMrr)
        {
            ResultResponse aResponse = new ResultResponse();
            BasicInfoReportsParameter aBasicInfoReportsParameter = new BasicInfoReportsParameter();
            aBasicInfoReportsParameter.ReportValue = aDirrectMrr.ReportValue;
            Session[SessionCollection.ReportValue] = aBasicInfoReportsParameter;
            Session[SessionCollection.ReportParam] = aDirrectMrr;
            aResponse.isSuccess = true;
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
        public ActionResult IndexMultiplePrint()
        {
            return View();
        }
    }
}