﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.Models;

namespace IGFoodProject.Controllers
{
    public class FurtherProcessedCostPriceConfigsController : Controller
    {
        private CodeDbSet db = new CodeDbSet();

        // GET: FurtherProcessedCostPriceConfigs
        public ActionResult Index()
        {
            var furtherProcessedCostPriceConfigs = db.FurtherProcessedCostPriceConfigs.Include(f => f.Product);
            return View(furtherProcessedCostPriceConfigs.ToList());
        }

        // GET: FurtherProcessedCostPriceConfigs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FurtherProcessedCostPriceConfig furtherProcessedCostPriceConfig = db.FurtherProcessedCostPriceConfigs.Find(id);
            if (furtherProcessedCostPriceConfig == null)
            {
                return HttpNotFound();
            }
            return View(furtherProcessedCostPriceConfig);
        }

        // GET: FurtherProcessedCostPriceConfigs/Create
        public ActionResult Create()
        {
            ViewBag.ProductId = new SelectList(db.Product.Where(p=> p.GroupId ==6 &&  p.GroupId==12), "ProductId", "ProductName");
            return View();
        }

        // POST: FurtherProcessedCostPriceConfigs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "FurtherProcessedCostPriceId,ProductId,CostPrice,IsActive,FromDate,ToDate")] FurtherProcessedCostPriceConfig furtherProcessedCostPriceConfig)
        {
            if (ModelState.IsValid)
            {
                db.FurtherProcessedCostPriceConfigs.Add(furtherProcessedCostPriceConfig);
                db.SaveChanges();
               
                return RedirectToAction("Index");
            }

            ViewBag.ProductId = new SelectList(db.Product, "ProductId", "ProductName", furtherProcessedCostPriceConfig.ProductId);
            return View(furtherProcessedCostPriceConfig);
        }

        // GET: FurtherProcessedCostPriceConfigs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FurtherProcessedCostPriceConfig furtherProcessedCostPriceConfig = db.FurtherProcessedCostPriceConfigs.Find(id);
            if (furtherProcessedCostPriceConfig == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProductId = new SelectList(db.Product, "ProductId", "ProductName", furtherProcessedCostPriceConfig.ProductId);
            return View(furtherProcessedCostPriceConfig);
        }

        // POST: FurtherProcessedCostPriceConfigs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "FurtherProcessedCostPriceId,ProductId,CostPrice,IsActive,FromDate,ToDate")] FurtherProcessedCostPriceConfig furtherProcessedCostPriceConfig)
        {
            if (ModelState.IsValid)
            {
                db.Entry(furtherProcessedCostPriceConfig).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ProductId = new SelectList(db.Product, "ProductId", "ProductName", furtherProcessedCostPriceConfig.ProductId);
            return View(furtherProcessedCostPriceConfig);
        }

        // GET: FurtherProcessedCostPriceConfigs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FurtherProcessedCostPriceConfig furtherProcessedCostPriceConfig = db.FurtherProcessedCostPriceConfigs.Find(id);
            if (furtherProcessedCostPriceConfig == null)
            {
                return HttpNotFound();
            }
            return View(furtherProcessedCostPriceConfig);
        }

        // POST: FurtherProcessedCostPriceConfigs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            FurtherProcessedCostPriceConfig furtherProcessedCostPriceConfig = db.FurtherProcessedCostPriceConfigs.Find(id);
            db.FurtherProcessedCostPriceConfigs.Remove(furtherProcessedCostPriceConfig);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
