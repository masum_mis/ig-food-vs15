﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;

namespace IGFoodProject.Controllers
{
    public class ChallanFurtherProcessedController : Controller
    {
        ChallanFurtherProcessedDAL _challanFurtherProcessedDal = new ChallanFurtherProcessedDAL();
        // GET: ChallanProcessed
        public ActionResult Index()
        {

            List<ViewSalesOrderProcessedMaster> aList = _challanFurtherProcessedDal.GetSalesOrderProcessedForChallan();
            return View(aList);
        }


        public ActionResult GenerateChallan(int id)
        {
            ViewBag.SalesOrder = id;


            return View();
        }

        public ActionResult SavePrecessedChallan(SalesOrderChallanMaster aMaster)
        {
            ResultResponse aResponse = _challanFurtherProcessedDal.SaveSalesOrderProcessedChallan(aMaster,
                Session[SessionCollection.UserName].ToString());

            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddRowForOrder(int tr)
        {
            TempData["trSl"] = tr;
            return PartialView("_addNewOrderRow");
        }

        public ActionResult GetPreviousChallan(int orderId, int productId)
        {
            ViewPreviousIssue issue = _challanFurtherProcessedDal.GetPreviousIssue(orderId, productId);
            return Json(issue, JsonRequestBehavior.AllowGet);
        }
    }
}