﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;

namespace IGFoodProject.Controllers
{
    public class TradeProcessedSalePriceConfigsController : Controller
    {
        private CodeDbSet db = new CodeDbSet();
        ProductDAL aDal = new ProductDAL();

        // GET: TradeProcessedSalePriceConfigs
        public ActionResult Index()
        {
            var tradeProcessedSalePriceConfigs = db.TradeProcessedSalePriceConfigs.Include(t => t.Product);
            return View(tradeProcessedSalePriceConfigs.ToList());
        }

        // GET: TradeProcessedSalePriceConfigs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TradeProcessedSalePriceConfig tradeProcessedSalePriceConfig = db.TradeProcessedSalePriceConfigs.Find(id);
            if (tradeProcessedSalePriceConfig == null)
            {
                return HttpNotFound();
            }
            return View(tradeProcessedSalePriceConfig);
        }

        // GET: TradeProcessedSalePriceConfigs/Create
        public ActionResult Create()
        {
            List<Product> products = aDal.GetProductList(0);
            var idList = new[] { 8, 9, 10 };
            List<Product> a = products.Where(x => x.IsActive == true && idList.Contains(x.GroupId)).ToList();
            int aa = a.Count;
            ViewBag.ProductId = new SelectList(a.Where(x => x.IsActive == true).ToList(), "ProductId", "ProductName");
            return View();
        }

        // POST: TradeProcessedSalePriceConfigs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TradeProcessedSalePriceSetupId,ProductId,PurchasePrice,SalePrice,IsActive,FromDate,ToDate")] TradeProcessedSalePriceConfig tradeProcessedSalePriceConfig)
        {
            if (ModelState.IsValid)
            {
                db.TradeProcessedSalePriceConfigs.Add(tradeProcessedSalePriceConfig);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ProductId = new SelectList(db.Product, "ProductId", "ProductName", tradeProcessedSalePriceConfig.ProductId);
            return View(tradeProcessedSalePriceConfig);
        }

        // GET: TradeProcessedSalePriceConfigs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TradeProcessedSalePriceConfig tradeProcessedSalePriceConfig = db.TradeProcessedSalePriceConfigs.Find(id);
            if (tradeProcessedSalePriceConfig == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProductId = new SelectList(db.Product, "ProductId", "ProductName", tradeProcessedSalePriceConfig.ProductId);
            return View(tradeProcessedSalePriceConfig);
        }

        // POST: TradeProcessedSalePriceConfigs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TradeProcessedSalePriceSetupId,ProductId,PurchasePrice,SalePrice,IsActive,FromDate,ToDate")] TradeProcessedSalePriceConfig tradeProcessedSalePriceConfig)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tradeProcessedSalePriceConfig).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ProductId = new SelectList(db.Product, "ProductId", "ProductName", tradeProcessedSalePriceConfig.ProductId);
            return View(tradeProcessedSalePriceConfig);
        }

        // GET: TradeProcessedSalePriceConfigs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TradeProcessedSalePriceConfig tradeProcessedSalePriceConfig = db.TradeProcessedSalePriceConfigs.Find(id);
            if (tradeProcessedSalePriceConfig == null)
            {
                return HttpNotFound();
            }
            return View(tradeProcessedSalePriceConfig);
        }

        // POST: TradeProcessedSalePriceConfigs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TradeProcessedSalePriceConfig tradeProcessedSalePriceConfig = db.TradeProcessedSalePriceConfigs.Find(id);
            db.TradeProcessedSalePriceConfigs.Remove(tradeProcessedSalePriceConfig);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
