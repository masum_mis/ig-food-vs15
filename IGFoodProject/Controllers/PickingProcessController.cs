﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;

namespace IGFoodProject.Controllers
{
    public class PickingProcessController : Controller
    {

        private PickingProcessDAL _aDal = new PickingProcessDAL();
        // GET: PickingProcess
        public ActionResult PickingRequestCreate(int id = 0)
        {
            PickingRequestMaster pickingRequestMaster = new PickingRequestMaster();
            if (id > 0)
            {
                pickingRequestMaster = _aDal.GetPickingInformation(id);
            }


            return View(pickingRequestMaster);
        }
        public ActionResult GetSalesOrdersForPicking(PickingSearchParam pickingSearchParam)
        {

            List<ViewSalesOrderMasterForPicking> salesOrderMasterForPickingList = _aDal.GetSalesOrdersForPicking(pickingSearchParam);

            return PartialView("_SalesOrderListForPicking", salesOrderMasterForPickingList);

        }

        //public ActionResult PickingRequestEdit(int pickingMasterId= 0)
        //{
        //    ViewSalesOrderMasterForPicking viewSalesOrderMasterForPicking = new ViewSalesOrderMasterForPicking();
        //    return View();
        //}

        public ActionResult SavePickingRequest(ViewSalesOrderMasterForPicking salesOrderMasterForPicking)
        {
            string UserID = string.Empty;
            ResultResponse result = new ResultResponse();
            if (Session[SessionCollection.LoginUserEmpId] != null)
            {
                UserID = Session[SessionCollection.LoginUserEmpId].ToString();
                result.isSuccess = _aDal.SavePickingRequest(salesOrderMasterForPicking, UserID);
            }
            else
            {
                result.isSuccess = false;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PickingDetailsReport()
        {
            return View();
        }

        public ActionResult GetAllPickingDetailsList(DateTime? deliveryDate = null, int companyId = 0, int locationId = 0, int warehouseId = 0, int vehicleId = 0, int reqType = 1)
        {
            DataTable dt = _aDal.GetAllPickingDetailsList(deliveryDate, companyId, locationId, warehouseId, vehicleId, reqType);

            return PartialView("_PartialPickingDetailsReport", dt);
        }

        public ActionResult GetPickingDetailsById(int id)
        {
            DataTable dt = _aDal.GetPickingDetailsById(id);

            return PartialView("_PartialPickingDetailsById", dt);
        }

        public ActionResult PickingList()
        {
            DataTable dt = _aDal.GetPickingList();
            return View(dt);
        }

        public ActionResult LetsPicked(int id, string type)                                                                                              
        {
            var result = _aDal.LetsPick(id, type);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetWarehouseDetails(int deliveryWarehouseId)
        {
            WareHouse aList = _aDal.GetWarehouseDetails(deliveryWarehouseId);

            return Json(aList, JsonRequestBehavior.AllowGet);

        }


    }
}