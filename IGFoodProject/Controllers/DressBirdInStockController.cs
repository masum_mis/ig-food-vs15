﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.Utility;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.ViewModel;
using Microsoft.Ajax.Utilities;

namespace IGFoodProject.Controllers
{
    public class DressBirdInStockController : Controller
    {
        // GET: DressBirdInStock
        private DressBirdInStockDAL _dal = new DressBirdInStockDAL();
        public ActionResult StockInList()
        {
            return View();
        }
        public ActionResult AddStockIn(int id = 0)
        {

            ViewBag.stockinid = id;

            return View();

        }
        public JsonResult LoadDressedBird(int wareid)
        {
            List<ViewDressedBirdMaster> alist = _dal.DressBirdInformation(wareid);
            return Json(alist ,JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadDressBirdDetailByID(int reqId)
        {
            List<ViewDressedDirdDetail> alist = _dal.GetDressedBirdDetailByID(reqId);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadProduct()
        {
            List<ViewProductDescription> alist = _dal.LoadProduct();
            return Json(alist, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAddRow(int reqId)
        {
            List<ViewDressedDirdDetail> alist = _dal.GetDressedBirdDetailByID(reqId);

            return PartialView("_partialDressStockDetail_New", alist);
        }
        public ActionResult GetAddRowOpening(int tr)
        {
            TempData["trSl"] = tr;
            return PartialView("_partialDressStockDetail_opening");
            
        }
       
        [HttpPost]
        public ActionResult SaveStockin(tbl_StockInMaster aorder)
        {
            ResultResponse aResponse = new ResultResponse();


            string entryBy = Session[SessionCollection.UserName].ToString();
            aorder.CreateBy = entryBy;

            aResponse.isSuccess = _dal.SaveDressBird(aorder);

            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadSalablePriceByID(int productid, DateTime sdate)
        {
            decimal alist = _dal.GetSalablePriceByID(productid, sdate);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveStockin_Log(tbl_DressBirdStockInMaster_Log aorder)
        {
            ResultResponse aResponse = new ResultResponse();


            string entryBy = Session[SessionCollection.UserName].ToString();
            aorder.CreateBy = entryBy;

            aResponse.isSuccess = _dal.SaveDressBird_Log(aorder);

            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult StockInListForApprove(string fromdate)
        {
            IEnumerable<DressBirdStockInListForApproval> aList = Enumerable.Empty<DressBirdStockInListForApproval>();

            aList = _dal.GetDressBirdStockInListForApproval(fromdate);

            return PartialView("_stoclinListForApprove", aList);
        }

        public ActionResult ApproveStockIn(int id = 0)
        {

            ViewBag.stockinid = id;

            return View();

        }
        public JsonResult LoadLogMasterByID(int id)
        {
            ViewDressBirdMasterLog alist = _dal.LoadLogMaster(id);
           
            return Json(alist, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAddRow_app(int id)
        {
            List<tbl_DressBirdStockInDetail_Log> alist = _dal.GetDressedBirdDetailLogByID(id);
            //List<tbl_DressBirdStockInDetail_Log> list = alist.DistinctBy(z=> z.DressedBirdDetailID).ToList();
            //List<tbl_DressBirdStockInDetail_Log> aList2 = alist.GroupBy(a => a.ItemID).Select(item => new tbl_DressBirdStockInDetail_Log( ) {itemName =item.i }).ToList();
            //var al = from p in alist select new tbl_DressBirdStockInDetail_Log() {ItemID = p.ItemID,itemName = p.itemName, DressedBirdDetailID = p.DressedBirdDetailID};
            return PartialView("_partialDressStockDetail_Log_New", alist);
        }


        public ActionResult AddDetailsRow(int detailTableId , int rowCount)
        {
            TempData["detailsId"] = detailTableId;
            TempData["rowCount"] = rowCount;
            return PartialView("_detailTableDetailRow");
        }
    }
}