﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Models.ReportModel;
using IGFoodProject.Utility;

namespace IGFoodProject.Controllers
{
    public class OtherPoRequisitionController : Controller
    {
        private OtherPoRequisitionDal _aDal=new OtherPoRequisitionDal();
        // GET: OtherPoRequisition
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult NewRequisition(int RequisitionId = 0, string eflag = "ed")
        {
            ViewBag.RequisitionId = RequisitionId;
            ViewBag.eflag = eflag;
            return View();
        }
        public ActionResult AddRowForRequisition(int tr, string eflag = "ed")
        {
            TempData["trSl"] = tr;
            ViewBag.eflag = eflag;
            return PartialView("_addNewRow");
        }
        public ActionResult SavePoRequisition(OtherPoRequisitionMaster aMaster)
        {
            aMaster.CreateBy = Session[SessionCollection.UserName].ToString();
            ResultResponse aResponse = _aDal.SaveRequisition(aMaster);
           return Json(aResponse, JsonRequestBehavior.AllowGet);
          
        }
        public ActionResult GetRequisitionList(DateTime? fromdate, DateTime? todate, int warehouse = 0, string reqById = null)
        {
            List<OtherPoRequisitionMaster> aList = _aDal.RequisitionList(fromdate, todate, warehouse, reqById);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }
        public JsonResult RequisitionDetailsById(int id)
        {
            List<OtherPoRequisitionDetails> aPo = _aDal.RequisitionDetailsById(id);
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetRequisitionByMasterId(int id)
        {
            OtherPoRequisitionMaster aMaster = _aDal.GetRequisitionMasterById(id);
            aMaster.PoRequisitionDetailses = _aDal.RequisitionDetailsById(id);

            return Json(aMaster, JsonRequestBehavior.AllowGet);
        }
        public ActionResult RequisitionApprovalList()
        {

            return View();
        }
        public ActionResult GetRequisitionApprovalList(DateTime? fromdate, DateTime? todate, int warehouse = 0, string reqById = null)
        {
            List<OtherPoRequisitionMaster> aList = _aDal.RequisitionApproveList(fromdate, todate, warehouse, reqById);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }

        public ActionResult RequisitionReport()
        {
            return View();
        }
        public ActionResult GetRequisitionReport( int location, int warehouse, DateTime fromdate, DateTime todate, int rstatus)
        {
            OtherPoRequisitionMaster aList = new OtherPoRequisitionMaster();

         
            aList.LocationId = location;
            aList.WarehouseId = warehouse;
            aList.FromDate = fromdate;
            aList.ToDate = todate;
            aList.RStatus = rstatus;

            ResultResponse aResponse = new ResultResponse();
            BasicInfoReportsParameter aBasicInfoReportsParameter = new BasicInfoReportsParameter();
            aBasicInfoReportsParameter.ReportValue = "PurchaseRequisitionReport";
            Session[SessionCollection.ReportValue] = aBasicInfoReportsParameter;
            Session[SessionCollection.ReportParam] = aList;
            aResponse.isSuccess = true;
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ItemRequisitionInvoiceList()
        {
           
            return View();
        }
        public ActionResult ShowRequisitionApprovedList(DateTime fromDate, DateTime toDate, int warehouse, string reqById)
        {
            List<OtherPoRequisitionMaster> aList = _aDal.PurchaseRequisitionPrintList(fromDate, toDate,   warehouse,reqById);
            return PartialView("_requisitionInvoiceList", aList);
        }
        public ActionResult RequisitionInvoiceReport(RequisitionReport aSalesOrderReport)
        {
            ResultResponse aResponse = new ResultResponse();
            BasicInfoReportsParameter aBasicInfoReportsParameter = new BasicInfoReportsParameter();
            aBasicInfoReportsParameter.ReportValue = "PoRequisitionSlip";
            Session[SessionCollection.ReportValue] = aBasicInfoReportsParameter;
            Session[SessionCollection.ReportParam] = aSalesOrderReport;
            aResponse.isSuccess = true;
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ViewRequisitionOrder(int id)
        {
            return PartialView("_RequisitionOrderView");
        }
        public ActionResult Reject(int id)
        {
           
            var aPo = _aDal.Reject(id, Session[SessionCollection.UserName].ToString());
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }
    }
}