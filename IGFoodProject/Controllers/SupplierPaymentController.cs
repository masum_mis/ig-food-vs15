﻿using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IGFoodProject.Controllers
{
    public class SupplierPaymentController : Controller
    {
        SupplierPaymentDAL _dal = new SupplierPaymentDAL();
        // GET: SupplierPayment
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult PurchaseOrderListForPaymentCollection(int supplierId)
        {
            List<ViewPurchaseOrderMaster> aList = _dal.PurchaseOrderListForPaymentCollection(supplierId);
            return PartialView("_purchaseOrderPaymentCollection", aList);
        }
        public ActionResult GetPreviousPaymentCollection(int DODetailsID, string ptype)
        {

            List<SupplierPaymentDetail> aList =
                _dal.GetPreviousCollectionDetails(DODetailsID, ptype);
            return Json(aList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveSupplierPayment(SupplierPaymentMaster salesCollectionMaster)
        {
            ResultResponse aResponse = new ResultResponse();
           
            salesCollectionMaster.CreateBy = Session[SessionCollection.UserName].ToString();
            aResponse.isSuccess = _dal.SaveSupplierPaymentDetails(salesCollectionMaster);
            return Json(aResponse, JsonRequestBehavior.AllowGet);

        }
        public ActionResult SaveSupplierOldPayment(SupplierPaymentMaster salesCollectionMaster)
        {
            ResultResponse aResponse = new ResultResponse();

            salesCollectionMaster.CreateBy = Session[SessionCollection.UserName].ToString();
            salesCollectionMaster.CreateDate=DateTime.Now;
            aResponse.isSuccess = _dal.SaveOldSupplierPaymentMaster(salesCollectionMaster);
            return Json(aResponse, JsonRequestBehavior.AllowGet);

        }

        public ActionResult SupplierLedger()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult GetSupplierLedger(int supplierId,DateTime fromDate,DateTime toDate)
        {
            DataTable aTable = _dal.SupplierLedger(supplierId,fromDate,toDate);
            return PartialView("_PartialSupplierLedger", aTable);
        }
        public ActionResult AllSupplierLedger()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult GetAllSupplierLedger(DateTime fromDate, DateTime toDate)
        {
            DataTable aTable = _dal.AllSupplierLedger(fromDate, toDate);
            return PartialView("_PartialAllSupplierLedger", aTable);
        }
    }
}