﻿using IGFoodProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.Models.ReportModel;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;
using IGFoodProject.DAL;

namespace IGFoodProject.Controllers
{
    public class BirdRequisitionReportController : Controller
    {
        private BirdRequisitionReportDAL _dal = new BirdRequisitionReportDAL();
        // GET: BirdRequisitionReport
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RequisitionReport()
        {
            return View();
        }
        public ActionResult GetRequisitionReport(int company, int location, int warehouse, DateTime fromdate, DateTime todate, int rstatus)
        {
            RequisitionReport aList = new RequisitionReport();

            aList.CompanyId = company;
            aList.LocationId = location;
            aList.WarehouseId = warehouse;
            aList.FromDate = fromdate;
            aList.ToDate = todate;
            aList.RStatus = rstatus;

            ResultResponse aResponse = new ResultResponse();
            BasicInfoReportsParameter aBasicInfoReportsParameter = new BasicInfoReportsParameter();
            aBasicInfoReportsParameter.ReportValue = "RequisitionReport";
            Session[SessionCollection.ReportValue] = aBasicInfoReportsParameter;
            Session[SessionCollection.ReportParam] = aList;
            aResponse.isSuccess = true;
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ItemRequisitionInvoiceList()
        {
            //
            //return View(aList);
            return View();
        }
        public ActionResult ShowRequisitionApprovedList(DateTime fromDate, DateTime toDate, int company, int location, int warehouse)
        {
            List<RequisitionMaster> aList = _dal.GetRequisitionApproved(fromDate, toDate, company, location, warehouse);
            return PartialView("_requisitionInvoiceList", aList);
        }
        public ActionResult RequisitionInvoiceReport(RequisitionReport aSalesOrderReport)
        {
            ResultResponse aResponse = new ResultResponse();
            BasicInfoReportsParameter aBasicInfoReportsParameter = new BasicInfoReportsParameter();
            aBasicInfoReportsParameter.ReportValue = "BirdRequisitionInvoice";
            Session[SessionCollection.ReportValue] = aBasicInfoReportsParameter;
            Session[SessionCollection.ReportParam] = aSalesOrderReport;
            aResponse.isSuccess = true;
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
    }
}