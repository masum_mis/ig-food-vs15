﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.Models;

namespace IGFoodProject.Controllers
{
    public class ProcessedCostPriceConfigsController : Controller
    {
        private CodeDbSet db = new CodeDbSet();

        // GET: ProcessedCostPriceConfigs
        public ActionResult Index()
        {
            var processedCostPriceConfig = db.ProcessedCostPriceConfig.Include(p => p.Product);
            return View(processedCostPriceConfig.ToList());
        }

        // GET: ProcessedCostPriceConfigs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProcessedCostPriceConfig processedCostPriceConfig = db.ProcessedCostPriceConfig.Find(id);
            if (processedCostPriceConfig == null)
            {
                return HttpNotFound();
            }
            return View(processedCostPriceConfig);
        }

        // GET: ProcessedCostPriceConfigs/Create
        public ActionResult Create()
        {
            //ViewBag.ProductId = new SelectList(db.Product, "ProductId", "ProductName");
            ViewBag.ProductId = new SelectList(db.Product.Where(x => x.IsActive == true).ToList(), "ProductId", "ProductName");
            return View();
        }

        // POST: ProcessedCostPriceConfigs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProcessedCostPriceId,ProductId,PurchasePrice,CostPrice,IsActive,FromDate,ToDate")] ProcessedCostPriceConfig processedCostPriceConfig)
        {
            if (ModelState.IsValid)
            {
                db.ProcessedCostPriceConfig.Add(processedCostPriceConfig);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ProductId = new SelectList(db.Product, "ProductId", "ProductName", processedCostPriceConfig.ProductId);
            return View(processedCostPriceConfig);
        }

        // GET: ProcessedCostPriceConfigs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProcessedCostPriceConfig processedCostPriceConfig = db.ProcessedCostPriceConfig.Find(id);
            if (processedCostPriceConfig == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProductId = new SelectList(db.Product, "ProductId", "ProductName", processedCostPriceConfig.ProductId);
            return View(processedCostPriceConfig);
        }

        // POST: ProcessedCostPriceConfigs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ProcessedCostPriceId,ProductId,PurchasePrice,CostPrice,IsActive,FromDate,ToDate")] ProcessedCostPriceConfig processedCostPriceConfig)
        {
            if (ModelState.IsValid)
            {
                db.Entry(processedCostPriceConfig).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ProductId = new SelectList(db.Product, "ProductId", "ProductName", processedCostPriceConfig.ProductId);
            return View(processedCostPriceConfig);
        }

        // GET: ProcessedCostPriceConfigs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProcessedCostPriceConfig processedCostPriceConfig = db.ProcessedCostPriceConfig.Find(id);
            if (processedCostPriceConfig == null)
            {
                return HttpNotFound();
            }
            return View(processedCostPriceConfig);
        }

        // POST: ProcessedCostPriceConfigs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ProcessedCostPriceConfig processedCostPriceConfig = db.ProcessedCostPriceConfig.Find(id);
            db.ProcessedCostPriceConfig.Remove(processedCostPriceConfig);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
