﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using System.Data;
using Newtonsoft.Json;
using IGFoodProject.Models;
using IGFoodProject.ViewModel;
using IGFoodProject.Utility;

namespace IGFoodProject.Controllers
{
    public class TargetSetupController : Controller
    {
        TargetSetupDAL aDAL = new TargetSetupDAL();
        // GET: TargetSetup

        public ActionResult TargetSetupEntry()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult GetTargetSetup(DateTime fromDate, DateTime toDate)
        {
            DataSet aObj = aDAL.GetTargetSetup(fromDate, toDate);

            DataTable dataTable = aObj.Tables["Table"];
            DataTable dataTable1 = aObj.Tables["Table1"];
            DataTable dataTable2 = aObj.Tables["Table2"];

            string JSONresult = JsonConvert.SerializeObject(dataTable);
            string JSONresult1 = JsonConvert.SerializeObject(dataTable1);
            string JSONresult2 = JsonConvert.SerializeObject(dataTable2);

            return Json(new { JSONresult, JSONresult1, JSONresult2 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveTargetSetup(TargetSetupMaster entity)
        {
            try
            {
                string entryBy = Session[SessionCollection.UserName].ToString();
                ResultResponse aResponse = aDAL.SaveTargetSetup(entity, entryBy);
                return Json(aResponse, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public ActionResult Index(DateTime? fromdate, DateTime? todate)
        {
            List<TargetSetupMaster> aList = aDAL.TargetSetupList(fromdate, todate);
            return View(aList);
        }

        public ActionResult SearchTargetSetupList(DateTime? fromdate, DateTime? todate)
        {
            List<TargetSetupMaster> aList = aDAL.TargetSetupList(fromdate, todate);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }

        public ActionResult TargetVsAchivementReport()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult TargetVsAchievementReport(DateTime fromDate, DateTime toDate, int monthly = 0, int groupId=0,int categoryId=0,int productId=0)
        {
            DataTable dt = new DataTable();

            dt = aDAL.TargetVsAchievementReport(fromDate, toDate, monthly, groupId, categoryId, productId);

            return PartialView("_PartialTargetVsAchivementReport", dt);

        }
        
        public ActionResult EditTargetSetup(int id)
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                ViewBag.TargetSetupId = id;
                return View();
            }
        }
        public ActionResult GetTargetMappingDataById(int targetSetupId)
        {
            DataSet aObj = aDAL.GetTargetMappingDataById(targetSetupId);

            DataTable dataTable = aObj.Tables["Table"];
            DataTable dataTable1 = aObj.Tables["Table1"];
            DataTable dataTable2 = aObj.Tables["Table2"];
            DataTable dataTable3 = aObj.Tables["Table3"];
            DataTable dataTable4 = aObj.Tables["Table4"];

            string JSONresult = JsonConvert.SerializeObject(dataTable);
            string JSONresult1 = JsonConvert.SerializeObject(dataTable1);
            string JSONresult2 = JsonConvert.SerializeObject(dataTable2);
            //string JSONresult3 = JsonConvert.SerializeObject(dataTable3);
            string JSONresult3 = JsonConvert.SerializeObject(dataTable3);
            string JSONresult4 = JsonConvert.SerializeObject(dataTable4);

            return Json(new { JSONresult, JSONresult1, JSONresult2, JSONresult3, JSONresult4 }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult TargetSetupExist(DateTime? fromDate, DateTime? toDate)
        {
            var value = aDAL.TargetSetupExist(fromDate, toDate);
            return Json(value, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CategorySalesRate(int categoryId)
        {
            var salseRate = aDAL.CategorySalesRate(categoryId);
            return Json(salseRate, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CustomerTargetSetup(int id=0)
        {
            List<TargetSetupDetails> aList = new List<TargetSetupDetails>();
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                //if(id>0)
                //{
                //    aList= aDAL.TargetSetupDetailsById(id);
                //}
                //else
                //{
                //    aList = aDAL.GetProductCategory();
                //}
                ViewBag.CustomerTargetSetupId = id;
                return View(aList);
            }
        }
        public ActionResult GetProductCategory(DateTime fromDate,DateTime toDate,int salesOfficerId,int areaId)
        {
            List<TargetSetupDetails> aList = aDAL.GetProductCategory(fromDate, toDate, salesOfficerId, areaId);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetArea()
        {
            List<Area> aList = aDAL.GetArea();
            return Json(aList, JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetSalesOfficerByArea(int areaId)
        {
            List<View_EmployeeInfo> aList = aDAL.GetSalesOfficerByArea(areaId);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetTargetSetupDetails(int id)
        {
            TargetSetupMaster aMaster = aDAL.GetTargetSetupMasterById(id);
            aMaster.TargetSetupDetails = aDAL.TargetSetupDetailsById(id);
            return Json(aMaster, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveCustomerTargetSetup(TargetSetupMaster entity)
        {
            try
            {
                //entity.FromDate = entity.FromDate;
                //string x = entity.FromDate.ToString("yyyy-MM-dd")
                //var Date = DateTime.ParseExact( entity.FromDate, "dd/MM/yyyy", null).ToString("MM/dd/yyyy");
                //DateTime.Parse.ToString()
                string entryBy = Session[SessionCollection.UserName].ToString();
                ResultResponse aResponse = aDAL.SaveCustomerTargetSetup(entity, entryBy);
                return Json(aResponse, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public ActionResult ExistingTargetKg(DateTime fromDate,DateTime toDate, int categoryId,int salesOfficerId,int areaId)
        {
            var targetKg = aDAL.ExistingTargetKg(fromDate, toDate, categoryId, salesOfficerId, areaId);
            return Json(targetKg, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult ExistingTargetKgSearch(int fromDate, int toDate, int categoryId, int salesOfficerId, int areaId)
        //{
        //    var targetKg = aDAL.ExistingTargetKgSearch(YearId, MonthId, categoryId, salesOfficerId, areaId);
        //    return Json(targetKg, JsonRequestBehavior.AllowGet);
        //}

        public ActionResult CustomerIndex(DateTime? fromdate, DateTime? todate)
        {
            List<TargetSetupMaster> aList = aDAL.CustomerTargetSetupList(fromdate, todate);
            return View(aList);
        }

        public ActionResult SearchCustomerTargetSetupList(DateTime? fromdate, DateTime? todate)
        {
            List<TargetSetupMaster> aList = aDAL.CustomerTargetSetupList(fromdate, todate);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }

        public ActionResult CustomerTargetSetupExist(DateTime? fromDate, DateTime? toDate,int customerId)
        {
            var value = aDAL.CustomerTargetSetupExist(fromDate, toDate, customerId);
            return Json(value, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CustomerTargetSetupExistAndNextDate(DateTime? fromDate, DateTime? toDate, int customerId)
        {
            TargetSetupMaster value = aDAL.CustomerTargetSetupExistAndNextDate(fromDate, toDate, customerId);
            return Json(value, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CustomerTargetSetupExistForEdit(DateTime? fromDate, DateTime? toDate, int customerId,int customerTargetSetupId)
        {
            TargetSetupMaster value = aDAL.CustomerTargetSetupExistForEdit(fromDate, toDate, customerId, customerTargetSetupId);
            return Json(value, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CustomerTargetVsAchivementReport()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult CustomerTargetVsAchievementReport(DateTime fromDate, DateTime toDate, int monthly,int CustomerTypeId=0, int CustomerId = 0, int AreaId = 0, int SalesOfficerId = 0)
        {
            DataTable dt = new DataTable();

            dt = aDAL.CustomerTargetVsAchievementReport(fromDate, toDate, monthly, CustomerTypeId, CustomerId, AreaId, SalesOfficerId);

            return PartialView("_PartialCustomerTargetVsAchivementReport", dt);

        }
        public ActionResult GetCustomerType()
        {
            List<CustomerType> aList = aDAL.GetCustomerType();
            return Json(aList, JsonRequestBehavior.AllowGet);

        }
        public ActionResult CustomerMasterTargetSetup(int id = 0)
        {
            //List<CustomerType> ctList = new List<CustomerType>(); 
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                ViewBag.CustomerTypeMasterTargetSetupId = id;
                //if(id==0)
                //{
                //    ctList = aDAL.GetCustomerType();
                //}
               
                return View();
            }
        }

        public ActionResult GetCustomerTypeTargetSetup(DateTime fromDate, DateTime toDate)
        {
            DataSet aObj = aDAL.GetCustomerTypeTargetSetup(fromDate, toDate);

            DataTable dataTable = aObj.Tables["Table"];
            DataTable dataTable1 = aObj.Tables["Table1"];

            string JSONresult = JsonConvert.SerializeObject(dataTable);
            string JSONresult1 = JsonConvert.SerializeObject(dataTable1);

            return Json(new { JSONresult, JSONresult1 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CustomerTypeTargetSetupExist(DateTime? fromDate, DateTime? toDate, int areaId = 0, int salesOfficerId = 0)
        {
            var value = aDAL.CustomerTypeTargetSetupExist(fromDate, toDate, areaId, salesOfficerId);
            return Json(value, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveCustomerTypeTargetSetup(CustomerTypeTargetSetupMaster entity)
        {
            try
            {
                string entryBy = Session[SessionCollection.UserName].ToString();
                ResultResponse aResponse = aDAL.SaveCustomerTypeTargetSetup(entity, entryBy);
                return Json(aResponse, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public ActionResult CustomerTypeIndex(DateTime? fromdate, DateTime? todate, int areaId = 0, int salesOfficerId = 0)
        {
            List<CustomerTypeTargetSetupMaster> aList = aDAL.CustomerTypeTargetSetupList(fromdate, todate, areaId, salesOfficerId);
            return View(aList);
        }

        public ActionResult SearchCustomerTypeTargetSetupList(DateTime? fromdate, DateTime? todate,int areaId=0,int salesOfficerId=0)
        {
            List<CustomerTypeTargetSetupMaster> aList = aDAL.CustomerTypeTargetSetupList(fromdate, todate, areaId, salesOfficerId);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetCustomerTypeTargetMappingDataById(int customerTypeTargetSetupId)
        {
            DataSet aObj = aDAL.GetCustomerTypeTargetMappingDataById(customerTypeTargetSetupId);

            DataTable dataTable = aObj.Tables["Table"];
            DataTable dataTable1 = aObj.Tables["Table1"];
            DataTable dataTable2 = aObj.Tables["Table2"];
            DataTable dataTable3 = aObj.Tables["Table3"];

            string JSONresult = JsonConvert.SerializeObject(dataTable);
            string JSONresult1 = JsonConvert.SerializeObject(dataTable1);
            string JSONresult2 = JsonConvert.SerializeObject(dataTable2);
            string JSONresult3 = JsonConvert.SerializeObject(dataTable3);

            return Json(new { JSONresult, JSONresult1, JSONresult2, JSONresult3 }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAvailableTarget(DateTime FromDate, DateTime ToDate, int CategoryId, int AreaId, int SalesOfficerId)
        {
            TargetSetupDetails aMaster = aDAL.GetAvailableTarget(FromDate, ToDate, CategoryId, AreaId, SalesOfficerId);
            return Json(aMaster, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCustomer(int typeid,int salesOfficerId)
        {
            List<ViewCustomerInfo> aList = aDAL.GetCustomerList(typeid, salesOfficerId);
            return Json(aList, JsonRequestBehavior.AllowGet);
        }
    }
}