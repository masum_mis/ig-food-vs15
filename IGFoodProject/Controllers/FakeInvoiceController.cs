﻿using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IGFoodProject.Controllers
{
    public class FakeInvoiceController : Controller
    {
        FakeInvoiceDAL adal = new FakeInvoiceDAL();
        // GET: FakeInvoice
        public ActionResult Index(int id=0)
        {
            ViewBag.invoiceid = id;
            return View();
        }
        public ActionResult GetMainInvoice(int customerid, string orderdate)
        {
            List<ViewSaleOrder> aList =
                adal.GetMainInvoice(customerid, orderdate);
            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetOrderInfo(int orderid)
        {
            ViewSaleOrder aList =
                adal.GetOrderInfo(orderid);
            return Json(aList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddRowForOrder(int tr)
        {
            TempData["trSl"] = tr;
            return PartialView("_addNewOrderRow");
        }
        [HttpPost]
        public ActionResult SaveOrderPrecessed(SalesOrderProcessedMaster_FalseInvoice aMaster)
        {

            ResultResponse aResponse = adal.SaveSalesOrderProcessed(aMaster,
                Session[SessionCollection.UserName].ToString());
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FalseSalesOrderList(DateTime? invoicedate, int customerid = 0)
        {
            List<ViewSalesOrderProcessedMaster_False> aList = adal.GetSalesOrderProcessedMaster(invoicedate, customerid);


            return View(aList);
        }

        public ActionResult FalseSalesOrderList_Search(DateTime? invoicedate, int customerid = 0)
        {
            List<ViewSalesOrderProcessedMaster_False> aList = adal.GetSalesOrderProcessedMaster(invoicedate, customerid);


            return Json(aList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetSalesOrderMaster_False(int id)
        {
            SalesOrderProcessedMaster_FalseInvoice aMaster = adal.GetSalesOrderProcessedMaster_False(id);
            aMaster.SalesDetails = adal.GetSalesOderProcessedDetailsByMaster_False(id);
            return Json(aMaster, JsonRequestBehavior.AllowGet);
        }
    }
}