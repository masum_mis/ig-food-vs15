﻿using IGFoodProject.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.Models;
using System.Data;

namespace IGFoodProject.Controllers
{
    public class ProductInOutReportController : Controller
    {
        // GET: ProductInOutReport
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ProductInOut()
        {
            return View();
        }
        public JsonResult LoadProduct(int groupid = 0, int typeid=0, int categoryid=0)
        {
            ProductInOutReportDAL aDal = new ProductInOutReportDAL();
            List<Product> plist = aDal.GetProductList(groupid,typeid,categoryid);
            return Json(plist, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ProductInOutReport( DateTime fromDate, DateTime toDate, int Company=0, int Location = 0, int Warehouse=0, int Group=0, int Type=0,int Category=0, int ProductId=0)
        {
            ProductInOutReportDAL aDal = new ProductInOutReportDAL();
            DataTable dt = new DataTable();
            
                dt = aDal.ProductInOutReport( fromDate,  toDate,  Company,  Location,  Warehouse ,  Group ,  Type,  Category ,  ProductId);
            
                return PartialView("_PartialProductInOutReport", dt);
           
        }
    }
}