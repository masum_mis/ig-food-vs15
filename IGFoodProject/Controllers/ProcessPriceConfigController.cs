﻿using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IGFoodProject.Controllers
{
    public class ProcessPriceConfigController : Controller
    {
        private ProcessPriceConfigDAL aDAL = new ProcessPriceConfigDAL();
        ResultResponse aResponse = new ResultResponse();
        // GET: ProcessPriceConfig
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PriceProcess()
        {
            return View();
        }
        public ActionResult GetPurchasePrice(DateTime orderdate)
        {

            List<ProceesPriceConfig> alist = aDAL.GetPurchasePrice(orderdate);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetPurchasePrice_Egg(DateTime orderdate)
        {

            List<ProceesPriceConfig> alist = aDAL.GetPurchasePrice_Egg(orderdate);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveprocessPrice(List<ProceesPriceConfig> aMaster)
        {

             aResponse.isSuccess = aDAL.SaveSalesPriceProcessed(aMaster);
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
    }
}