﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;

namespace IGFoodProject.Controllers
{
    public class SupplierController : Controller
    {
        // GET: Supplier

            SupplierDAL _aDal = new SupplierDAL();
        public ActionResult Index(int id=0)
        {

            ViewBag.SupplierId = id;
            return View();
        }


        public ActionResult SaveSupplier(SupplierInfo aInfo)
        {
            string user = Session[SessionCollection.UserName].ToString();
            ResultResponse aResponse = _aDal.SaveSupplier(aInfo, user);
            return Json(aResponse, JsonRequestBehavior.AllowGet);

        }

        public ActionResult SupplierList()
        {
            List<SupplierInfo> aList = _aDal.GetSupplierList();
            return View(aList);
        }

        public ActionResult GetSuplierType()
        {
            List<SupplierInfo> aList = _aDal.GetSuplierType();
            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSuppliernfo(int supplierId)
        {
            SupplierInfo aInfo = _aDal.GetSupplierList(supplierId)[0];
            return Json(aInfo, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SupplierDuplicateCheck(int supplierId, string supplierName)
        {
            List<SupplierInfo> aList =  _aDal.GetSupplierList()
                .Where(
                sup=>sup.SupplierName == supplierName.Trim() && sup.SupplierId !=supplierId
                ).ToList();
            bool isExist = aList.Count > 0 ? true : false;
            return Json(isExist, JsonRequestBehavior.AllowGet);

        }
    }
}