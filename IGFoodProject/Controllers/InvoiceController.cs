﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;
using IGFoodProject.Models.ReportModel;
namespace IGFoodProject.Controllers
{
    public class InvoiceController : Controller
    {
        InvoiceDAL _aDal = new InvoiceDAL();
        // GET: Invoice
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult IndexMultiplePrint()
        {
            return View();
        }
        public ActionResult InvoiceEntry()
        {
            return View();
        }
        public ActionResult LoadMrrNoBySupplier( int id)
        {
            List<ViewOthersMrr> aPo = _aDal.LoadMrrNoBySupplier(id);
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadMrrInfo(int supplierId,int mrrId=0, DateTime? mrrDate = null)
        {
            List<ViewInvoiceMaster> aPo = _aDal.LoadMrrInfo(supplierId,mrrId,mrrDate);
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveBill(ViewInvoiceMaster entity)
        {
            try
            {
                string entryBy = Session[SessionCollection.UserName].ToString();
                ResultResponse aResponse = _aDal.SaveBill(entity, entryBy);
                return Json(aResponse, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ActionResult GetGeneratedInvoices(int supplierId=0, int invoiceId = 0, DateTime? fromDate = null, DateTime? toDate = null)
        {
            List<ViewInvoiceMaster> aPo = _aDal.GetGeneratedInvoices(supplierId, invoiceId, fromDate,toDate);
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }
        public ActionResult InvoiceDetailsById(int id)
        {
            List<ViewInvoiceDetails> aPo = _aDal.InvoiceDetailsById(id);
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LoadInvoiceNo()
        {
            List<ViewInvoiceMaster> aPo = _aDal.LoadInvoiceNo();
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }
        public ActionResult PrintIndivualInvoice(ViewInvoiceMaster aDirrectMrr)
        {
            ResultResponse aResponse = new ResultResponse();
            BasicInfoReportsParameter aBasicInfoReportsParameter = new BasicInfoReportsParameter();
            aBasicInfoReportsParameter.ReportValue = aDirrectMrr.ReportValue;
            Session[SessionCollection.ReportValue] = aBasicInfoReportsParameter;
            Session[SessionCollection.ReportParam] = aDirrectMrr;
            aResponse.isSuccess = true;
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
        public ActionResult MultipleInvoiceReport(ViewInvoiceMaster aDirrectMrr)
        {
            ResultResponse aResponse = new ResultResponse();
            BasicInfoReportsParameter aBasicInfoReportsParameter = new BasicInfoReportsParameter();
            aBasicInfoReportsParameter.ReportValue = aDirrectMrr.ReportValue;
            Session[SessionCollection.ReportValue] = aBasicInfoReportsParameter;
            Session[SessionCollection.ReportParam] = aDirrectMrr;
            aResponse.isSuccess = true;
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
    }
}