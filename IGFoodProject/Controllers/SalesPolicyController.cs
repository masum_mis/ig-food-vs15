﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IGFoodProject.Controllers
{
    public class SalesPolicyController : Controller
    {
        // GET: SalesPolicy
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AddRowForSalesPolicy(int tr, string eflag = "ed")
        {
            TempData["trSl"] = tr;
            ViewBag.eflag = eflag;
            return PartialView("_addNewRow");
        }
    }
}