﻿using IGFoodProject.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;

namespace IGFoodProject.Controllers
{
    public class LiveBirdMortalityController : Controller
    {
        LiveBirdMortalityDAL mortalityDAL = new LiveBirdMortalityDAL();
        MrRDAL mrRDAL = new MrRDAL();
        PurchaseOrderDAL _dal = new PurchaseOrderDAL();
        // GET: LiveBirdMortality
        public ActionResult Index()
        {
            
            List<tbl_SupplierInfo> supplierList = _dal.SupplierInformation();
            return View(supplierList);
           
        }

        public ActionResult GetMrrList(int supplierId, DateTime fromDate, DateTime toDate)
        {
            List<ViewMrrMaster> mrrList = mortalityDAL.GetMrrList(supplierId, fromDate, toDate);
            return PartialView("_mrrListForMortality", mrrList);
        }

        public ActionResult MortalityEntry(int id)
        {
            ViewMrrMaster aMaster = mrRDAL.GetMrRMasterView(id);
          //  List<ViewMrrDetails> aList = mrRDAL.GetMrrDetailsForEdit(id);
            aMaster.MrrDetailsList= mrRDAL.GetMrrDetailsForEdit(id);
            return View(aMaster);
        }

        public ActionResult SaveLiveBirdMortality(LiveBirdMortalityMaster liveBirdMortalityMaster)
        {
            string UserID = string.Empty;
            ResultResponse result = new ResultResponse();
            if (Session[SessionCollection.LoginUserEmpId] != null)
            {
                liveBirdMortalityMaster.EntryBy= Session[SessionCollection.LoginUserEmpId].ToString();
                result = mortalityDAL.SaveMortalityMaster(liveBirdMortalityMaster);
            }
           
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult MortalityIndex()
        {

            List<tbl_SupplierInfo> supplierList = _dal.SupplierInformation();
            return View(supplierList);

        }
        public ActionResult GetMortalityList( DateTime fromDate, DateTime toDate, int? supplierId,int? warehouseId)
        {
            DataTable moList = mortalityDAL.GetMortalityList(fromDate, toDate, supplierId, warehouseId);
            return PartialView("_mortalityList", moList);
        }
    }
}