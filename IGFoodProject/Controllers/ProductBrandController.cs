﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.Utility;
using IGFoodProject.DAL;
using IGFoodProject.Models;

namespace IGFoodProject.Controllers
{
    public class ProductBrandController : Controller
    {
        // GET: ProductBrand
        public ActionResult ProductBrandList()
        {
            ProductBrandDAL aDal = new ProductBrandDAL();
            List<tbl_ProductBrand> alist = aDal.GetBrandList();
            return View(alist);
        }
        public ActionResult AddBrand(int id = 0)
        {
            ViewBag.brandid = id;
            return View();
        }
        public JsonResult CheckBrand(string group)
        {

            ProductBrandDAL aDAL = new ProductBrandDAL();

            int desig = aDAL.CheckBrand(group);

            return Json(desig, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveBrand(tbl_ProductBrand aGroup)
        {
            ResultResponse aResponse = new ResultResponse();
            ProductBrandDAL aDAL = new ProductBrandDAL();

            string entryBy = Session[SessionCollection.UserName].ToString();
            aGroup.CreateBy = entryBy;
            aGroup.UpdateBy = entryBy;
            aResponse.isSuccess = aDAL.SaveBrand(aGroup);

            return Json(aResponse,JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetBrandByID(int id)
        {
            ProductBrandDAL aDal = new ProductBrandDAL();
            return Json(aDal.GetBrandForEdit(id), JsonRequestBehavior.AllowGet);

        }
        public JsonResult DeleteBrand(int id)
        {
            ResultResponse aResponse = new ResultResponse();
            ProductBrandDAL aDAL = new ProductBrandDAL();


            aResponse.isSuccess = aDAL.DeleteBrand(id);

            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
    }
}