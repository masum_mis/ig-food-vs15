﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;
using System.Data;

namespace IGFoodProject.Controllers
{
    public class ProductionReportController : Controller
    {
        ProductionReportDAL _ProductionReport = new ProductionReportDAL();
        // GET: ProductionReport
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult DailyBroilerPurchaseSummary(DateTime productionDate)
        {
            DataTable dt = _ProductionReport.DailyBroilerPurchaseSummary(productionDate);

            return PartialView("_PartialPurchaseSummary", dt);
        }

        public ActionResult DailyBroilerInputDressBirdSummary(DateTime productionDate)
        {
            DataTable dt = _ProductionReport.DailyBroilerInputDressBirdSummary(productionDate);

            return PartialView("_partialBroilerInputDressBirdSummary", dt);
        }
        public ActionResult DailyBroilerInputDressBirdByProductProductionPercentage(DateTime productionDate)
        {
            DataTable dt = _ProductionReport.DailyBroilerInputDressBirdByProductProductionPercentage(productionDate);

            return PartialView("_partialBroilerInputDressBirdByProductProductionPercentage", dt);
        }
        public ActionResult DailyPortionProduction(DateTime productionDate)
        {
            DataTable dt = _ProductionReport.DailyPortionProduction(productionDate);

            return PartialView("_partialPortionProduction", dt);
        }
        public ActionResult DailyPortionProductionLossPercentage(DateTime productionDate)
        {
            DataTable dt = _ProductionReport.DailyPortionProductionLossPercentage(productionDate);

            return PartialView("_partialPortionProductionLoss", dt);
        }
        public ActionResult DailyNormalPortion(DateTime productionDate)
        {
            DataTable dt = _ProductionReport.DailyNormalPortion(productionDate);

            return PartialView("_partialNormalPortionProduction", dt);
        }
        public ActionResult DailyDressingPointByProductProductionPercentage(DateTime productionDate)
        {
            DataTable dt = _ProductionReport.DailyDressingPointByProductProductionPercentage(productionDate);

            return PartialView("_partialDressingPointByProductProduction", dt);
        }
        public ActionResult MonthlyIndex()
        {
            return View();
        }
        public ActionResult MonthlyBroilerPurchaseSummary(string productionMonth)
        {
            DataTable dt = _ProductionReport.MonthlyBroilerPurchaseSummary(productionMonth);

            return PartialView("_PartialPurchaseSummary", dt);
        }

        public ActionResult MonthlyBroilerInputDressBirdSummary(string productionMonth)
        {
            DataTable dt = _ProductionReport.MonthlyBroilerInputDressBirdSummary(productionMonth);

            return PartialView("_partialBroilerInputDressBirdSummary", dt);
        }
        public ActionResult MonthlyBroilerInputDressBirdByProductProductionPercentage(string productionMonth)
        {
            DataTable dt = _ProductionReport.MonthlyBroilerInputDressBirdByProductProductionPercentage(productionMonth);

            return PartialView("_partialBroilerInputDressBirdByProductProductionPercentage", dt);
        }
        public ActionResult MonthlyPortionProduction(string productionMonth)
        {
            DataTable dt = _ProductionReport.MonthlyPortionProduction(productionMonth);

            return PartialView("_partialPortionProduction", dt);
        }
        public ActionResult MonthlyPortionProductionLossPercentage(string productionMonth)
        {
            DataTable dt = _ProductionReport.MonthlyPortionProductionLossPercentage(productionMonth);

            return PartialView("_partialPortionProductionLoss", dt);
        }
        public ActionResult MonthlyNormalPortion(string productionMonth)
        {
            DataTable dt = _ProductionReport.MonthlyNormalPortion(productionMonth);

            return PartialView("_partialNormalPortionProduction", dt);
        }
        public ActionResult MonthlyDressingPointByProductProductionPercentage(string productionMonth)
        {
            DataTable dt = _ProductionReport.MonthlyDressingPointByProductProductionPercentage(productionMonth);

            return PartialView("_partialDressingPointByProductProduction", dt);
        }
    }
}