﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using IGFoodProject.Models;
using IGFoodProject.Utility;

namespace IGFoodProject.Controllers
{
    public class tbl_WareHouseInfoController : Controller
    {
        private IGFOODDBEntities db = new IGFOODDBEntities();
     

        // GET: tbl_WareHouseInfo
        public ActionResult Index()
        {
            var tbl_WareHouseInfo = db.tbl_WareHouseInfo.Include(t => t.tbl_LocationIG).Include(t => t.tbl_WareHouseType);
            return View(tbl_WareHouseInfo.ToList());
        }

        // GET: tbl_WareHouseInfo/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_WareHouseInfo tbl_WareHouseInfo = db.tbl_WareHouseInfo.Find(id);
            if (tbl_WareHouseInfo == null)
            {
                return HttpNotFound();
            }
            return View(tbl_WareHouseInfo);
        }

        // GET: tbl_WareHouseInfo/Create
        public ActionResult Create()
        {
             var LocationId = new SelectList(db.tbl_LocationIG, "LocationId", "LocationName");
           
            ViewBag.LocationId = LocationId;
            ViewBag.WarehouseTypeId = new SelectList(db.tbl_WareHouseType, "WarehouseTypeId", "WarehouseType");
            return View();
        }

        // POST: tbl_WareHouseInfo/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "WareHouseId,WareHouseName,LocationId,IsActive,CreateBy,CreateDate,UpdateBy,UpdateDate,WarehouseTypeId")] tbl_WareHouseInfo tbl_WareHouseInfo)
        {
            if (ModelState.IsValid)
            {
                var alist = db.tbl_WareHouseInfo.Where(x => x.WareHouseName == tbl_WareHouseInfo.WareHouseName).ToList();
                if (!alist.Any())
                {
                    tbl_WareHouseInfo.CreateBy = Session[SessionCollection.UserName].ToString();
                    tbl_WareHouseInfo.CreateDate = DateTime.Now;
                    db.tbl_WareHouseInfo.Add(tbl_WareHouseInfo);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    var msg = "Duplicate Warehouse Name Can Not Insert";
                    ViewBag.Message = msg;
                    //return RedirectToAction("Index");
                }
               
            }

            ViewBag.LocationId = new SelectList(db.tbl_LocationIG, "LocationId", "LocationName", tbl_WareHouseInfo.LocationId);
            ViewBag.WarehouseTypeId = new SelectList(db.tbl_WareHouseType, "WarehouseTypeId", "WarehouseType", tbl_WareHouseInfo.WarehouseTypeId);
            return View(tbl_WareHouseInfo);
        }

        // GET: tbl_WareHouseInfo/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_WareHouseInfo tbl_WareHouseInfo = db.tbl_WareHouseInfo.Find(id);
            if (tbl_WareHouseInfo == null)
            {
                return HttpNotFound();
            }
            ViewBag.LocationId = new SelectList(db.tbl_LocationIG, "LocationId", "LocationName", tbl_WareHouseInfo.LocationId);
            ViewBag.WarehouseTypeId = new SelectList(db.tbl_WareHouseType, "WarehouseTypeId", "WarehouseType", tbl_WareHouseInfo.WarehouseTypeId);
            return View(tbl_WareHouseInfo);
        }

        // POST: tbl_WareHouseInfo/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "WareHouseId,WareHouseName,LocationId,IsActive,CreateBy,CreateDate,UpdateBy,UpdateDate,WarehouseTypeId")] tbl_WareHouseInfo tbl_WareHouseInfo)
        {
            if (ModelState.IsValid)
            {
                tbl_WareHouseInfo.UpdateBy= Session[SessionCollection.UserName].ToString();
                tbl_WareHouseInfo.UpdateDate=DateTime.Now;
                db.Entry(tbl_WareHouseInfo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.LocationId = new SelectList(db.tbl_LocationIG, "LocationId", "LocationName", tbl_WareHouseInfo.LocationId);
            ViewBag.WarehouseTypeId = new SelectList(db.tbl_WareHouseType, "WarehouseTypeId", "WarehouseType", tbl_WareHouseInfo.WarehouseTypeId);
            return View(tbl_WareHouseInfo);
        }

        // GET: tbl_WareHouseInfo/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_WareHouseInfo tbl_WareHouseInfo = db.tbl_WareHouseInfo.Find(id);
            if (tbl_WareHouseInfo == null)
            {
                return HttpNotFound();
            }
            return View(tbl_WareHouseInfo);
        }

        // POST: tbl_WareHouseInfo/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_WareHouseInfo tbl_WareHouseInfo = db.tbl_WareHouseInfo.Find(id);
            db.tbl_WareHouseInfo.Remove(tbl_WareHouseInfo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
