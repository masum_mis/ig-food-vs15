﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;
 namespace IGFoodProject.Controllers
{
    public class FurtherProcessRequisitionController : Controller
    {
        private FaurtherRequisitionDal _dal = new FaurtherRequisitionDal();
        // GET: FurtherProcessRequisition
        public ActionResult FurtherRequisition()
        {
            return View();
        }
         //public JsonResult LoadRequisitionDetailByID(int wareHouse)
        //{
        //    List<ViewDBRequsitionDetail> alist = _dal.GetRequisitionDetailByID(wareHouse);
        //    return Json(alist, JsonRequestBehavior.AllowGet);
        //}
        public dynamic GetAddRow(int tr)
        {
            TempData["trSl"] = tr;
            return PartialView("_PartialFurtherProcess");
        }
        public JsonResult LoadProduct(int wareHouseId)
        {
            List<ViewProductDescription> alist = _dal.LoadProduct(wareHouseId);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveRequisition(FurtherProcessRequisitionMaster aorder)
        {
            ResultResponse aResponse = new ResultResponse();
              string entryBy = Session[SessionCollection.UserName].ToString();
            aorder.CreateBy = entryBy;
            aorder.UpdateBy = entryBy;
            aorder.ApproveBy = entryBy;
            aorder.ApproveDate=DateTime.Now;
            aorder.CreateDate=DateTime.Now;
            aorder.UpdateDate=DateTime.Now;
            aResponse.isSuccess = _dal.SaveRequisition(aorder);
             return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
         public JsonResult LoadStockForFurtherProcess(int productId, int fromWareHouseId)
        {
            tbl_StockInDetail alist = _dal.LoadProductStockForProtionIssue(productId, fromWareHouseId);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FurtherProcessReqisitionList()
        {
            DataTable dt = _dal.GetFurtherProcessRequisitionList();
            return  View(dt);
        }
        public ActionResult FurtherProcessIssue(int id, int warehouseId)
        {
            FurtherProcessRequisitionMaster aMaster = _dal.GetFurtherProcessRequisition(id,warehouseId);
            return  View(aMaster);

        }


        public ActionResult SaveFurtherProcessIssue(FurtherProcessIssueMaster alist)
        {
            ResultResponse aResponse = new ResultResponse();
            string entryBy = Session[SessionCollection.UserName].ToString();
            alist.IssueBy = entryBy;
            alist.IssueDate = DateTime.Now;
            aResponse.isSuccess = _dal.SaveFurtherIssue(alist);
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
    }
}