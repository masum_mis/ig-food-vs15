﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;

namespace IGFoodProject.Controllers
{
    public class FurtherProcessedController : Controller
    {
        // GET: FurtherProcessed
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult FurtherStockInList()
        {
            return View();
        }

        public ActionResult FurtherProcessedStockIn()
        {
            return View();
        }
        public JsonResult LoadIssue(int WarehouseId)
        {
            FurtherProcessedDAL _dal = new FurtherProcessedDAL();
            List<DBIssuePortionMaster> alist = _dal.IssueInformation(WarehouseId);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetProductsForFurtherProcessed()
        {
            ProductDAL aDal = new ProductDAL();
            List<Product> productList = aDal.GetProductList(6);
            return Json(productList.Where(x => x.IsActive == true), JsonRequestBehavior.AllowGet);
            //return PartialView("_productStockInTable", productList);
        }

        public ActionResult SaveStockInForFurtherProcessed(tbl_StockInMaster stockInMaster)
        {
            ResultResponse aResponse = new ResultResponse();
            FurtherProcessedDAL aDal = new FurtherProcessedDAL();
            stockInMaster.CreateBy = Session[SessionCollection.UserName].ToString();
            aResponse.isSuccess= aDal.SaveFurtherProcessedStockDetails(stockInMaster);
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAddRow(int tr)
        {
            TempData["trSl"] = tr;
            return PartialView("_partialFurtherProcessStockInDetail");

        }
        public JsonResult LoadSalablePriceByID(int productid,DateTime pdate)
        {
            FurtherProcessedDAL aDal = new FurtherProcessedDAL();
            decimal alist = aDal.GetSalablePriceByID(productid, pdate);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }



        public ActionResult SaveStockInForFurtherProcessed_Log(tbl_FurtherProcessStockInMaster_Log stockInMaster)
        {
            ResultResponse aResponse = new ResultResponse();
            FurtherProcessedDAL aDal = new FurtherProcessedDAL();
            stockInMaster.CreateBy = Session[SessionCollection.UserName].ToString();
            aResponse.isSuccess = aDal.SaveFurtherProcessedStockDetails_Log(stockInMaster);
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult StockInListForApprove(string fromdate)
        {
            IEnumerable<ViewFurtherStockInListForApproval> aList = Enumerable.Empty<ViewFurtherStockInListForApproval>();
            FurtherProcessedDAL aDal = new FurtherProcessedDAL();
            aList = aDal.GetFurtherProcessStockInListForApproval(fromdate);

            return PartialView("_stockFurtherListForApprove", aList);
        }

        public ActionResult ApproveStockIn(int id = 0)
        {

            ViewBag.stockinid = id;

            return View();

        }

        public JsonResult LoadLogMasterByID(int id)
        {
            FurtherProcessedDAL aDal = new FurtherProcessedDAL();
            ViewDressBirdMasterLog alist = aDal.LoadLogMaster(id);

            return Json(alist, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAddRow_app(int id)
        {
            FurtherProcessedDAL aDal = new FurtherProcessedDAL();
            List<tbl_DressBirdStockInDetail_Log> alist = aDal.GetFurtherProcessDetailLogByID(id);

            return PartialView("_partialFurtherProccessStockDetail_Log", alist);
        }

        public ActionResult GetIssueDetails(int id)
        {
            FurtherProcessedDAL aDal = new FurtherProcessedDAL();
            List<FurtherProcessIssueDetail> aDetails = aDal.GetFurtherIssueDetailsByIssueId(id);
            return PartialView("_issue_details", aDetails);
        }



    }
}