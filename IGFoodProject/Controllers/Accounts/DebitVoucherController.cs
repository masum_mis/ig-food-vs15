﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.Models.Accounts;
using IGFoodProject.DAL.Accounts;
using IGFoodProject.Models;
using IGFoodProject.Utility;

namespace IGFoodProject.Controllers.Accounts
{
    public class DebitVoucherController : Controller
    {
        DebitVoucherDAL dbDal = new DebitVoucherDAL();
        // GET: DebitVoucher
        //public ActionResult Index()
        //{
        //    return View();
        //}
        public ActionResult AddRowForVoucher(int tr)
        {
            TempData["trSl"] = tr;

            return PartialView("_addNewGLRow");
        }
        public ActionResult Create(int VoucherId = 0, string eflag = "ed", DateTime? fromdate = null, DateTime? todate = null)
        {
            ViewBag.VoucherId = VoucherId;
            ViewBag.eflag = eflag;
            ViewBag.fromdate = fromdate;
            ViewBag.todate = todate;
            return View();
        }
        public ActionResult GetGLDetails(int glId)
        {
            ChartOfAccLayerSeven aDetails = dbDal.GetGLDetails(glId);
            return Json(aDetails, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadCostCenterByGlId(int CALayerSevenId)
        {
            List<CostCenter> cu = dbDal.GetCostCenterByGlId(CALayerSevenId);
            return Json(cu, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadReferenceTypeByGlId(int CALayerSevenId)
        {
            List<ReferanceTypeDetails> cu = dbDal.GetReferenceTypeByGlId(CALayerSevenId);
            return Json(cu, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadReferenceTypeByGlIdSupllierId(int CALayerSevenId, int supplierId)
        {
            List<ReferanceTypeDetails> cu = dbDal.LoadReferenceTypeByGlIdSupllierId(CALayerSevenId, supplierId);
            return Json(cu, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadRefDetailsByRefTypeId(int RefTypeId)
        {
            List<ReferanceTypeDetails> cu = dbDal.GetRefDetailsByRefTypeId(RefTypeId);
            return Json(cu, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetBankBranch(int CALayerSevenId)
        {
            BankAccount aDetails = dbDal.GetBankBranch(CALayerSevenId);
            return Json(aDetails, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveVoucher(VoucherMaster aMaster)
        {

            ResultResponse aResponse = dbDal.SaveVoucher(aMaster, Session[SessionCollection.UserName].ToString());
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Index(DateTime? fromdate, DateTime? todate, int CompanyId = 0, int BUId = 0)
        {
            List<VoucherMasterView> aList = dbDal.GetDebitVoucherMaster(fromdate, todate, CompanyId, BUId);
            return View(aList);
        }
        public ActionResult GetVoucherByMasterId(int id)
        {
            VoucherMaster aMaster = dbDal.GetVoucherMasterById(id);
            aMaster.VoucherDetails = dbDal.GetVoucherDetailsById(id);
            return Json(aMaster, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DebitVoucherListData(DateTime? fromdate, DateTime? todate, int CompanyId = 0, int BUId = 0)
        {
            List<VoucherMasterView> aList = dbDal.GetDebitVoucherMaster(fromdate, todate, CompanyId, BUId);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }

        // Debit Voucher Approval
        public ActionResult DebitVoucherApproval(DateTime? fromdate, DateTime? todate, int CompanyId = 0, int BUId = 0)
        {
            ViewBag.fromdate = fromdate;
            ViewBag.todate = todate;
            List<VoucherMasterView> aList = dbDal.GetDebitVoucherMasterForApproval(fromdate, todate, CompanyId, BUId);
            return View(aList);
        }
        public ActionResult DebitVoucherApprovalListData(DateTime? fromdate, DateTime? todate, int CompanyId = 0, int BUId = 0, int statusType = 100)
        {
            List<VoucherMasterView> aList = dbDal.GetDebitVoucherMasterForApproval(fromdate, todate, CompanyId, BUId, statusType);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }
        public ActionResult Reject(int id)
        {

            var aPo = dbDal.Reject(id, Session[SessionCollection.UserName].ToString());
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Review(int id, string ReviewReasion)
        {

            var aPo = dbDal.Review(id, Session[SessionCollection.UserName].ToString(), ReviewReasion);
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }
    }
}