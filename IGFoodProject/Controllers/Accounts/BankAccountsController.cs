﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using IGAccounts.Models;
using IGAccounts.Repository;
using IGFoodProject.Models;
using IGFoodProject.Models.Accounts;
using IGFoodProject.Utility;

namespace IGFoodProject.Controllers.Accounts
{
    public class BankAccountsController : Controller
    {
        private CodeDbSetAccounts db = new CodeDbSetAccounts();
        private CodeDbSetHR dbHr= new CodeDbSetHR();

        // GET: BankAccounts
        public ActionResult Index()
        {
            var company = dbHr.Company.ToList();
            List<CompanyInformation> coms = company;
            List<BankAccount> list = (from a in db.BankAccount.AsEnumerable()
                                      join Cu in db.Currency.AsEnumerable() on a.CurrencyId equals Cu.CurrencyId
                join s in db.ChartOfAccLayerSix.AsEnumerable() on a.BranchId equals s.CALayerSixId
                join f in db.ChartOfAccLayerFive.AsEnumerable() on s.CALayerFiveId equals f.CALayerFiveId
                select new BankAccount()
                {
                    CurrencyName = Cu.CurrencyName,
                    AccountNo = a.AccountNo,
                    Bank = f.CALayerFiveName,
                    Branch = s.CALayerSixName,
                    BankAccountId = a.BankAccountId,
                    CompanyId = a.CompanyId,
                    Description = a.Description
                    
                }
                ).ToList();
            var alist = (from a in list
                join com in coms on a.CompanyId equals com.CompanyId

                select new BankAccount()
                {
                    CurrencyName = a.CurrencyName,
                    AccountNo = a.AccountNo,
                    Bank = a.Bank,
                    Branch = a.Branch,
                    BankAccountId = a.BankAccountId,
                    CompanyName = com.CompanyName,
                    Description = a.Description
                }).ToList();
            return View(alist);
        }

        // GET: BankAccounts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BankAccount bankAccount = db.BankAccount.Find(id);
            if (bankAccount == null)
            {
                return HttpNotFound();
            }
            return View(bankAccount);
        }

        // GET: BankAccounts/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BankAccounts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "BankAccountId,CompanyId,BankId,BranchId,AccountName,Description,SubsidiryLId,AccountNo,CALayerSevenId,BankAccountCode,CurrencyId")] BankAccount bankAccount)
        {
            if (ModelState.IsValid)
            {
                db.BankAccount.Add(bankAccount);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bankAccount);
        }

        // GET: BankAccounts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.id = id;
            return View();
        }

        public ActionResult EditBankAccount(int id)
        {
            var iid = db.BankAccount.Where(x => x.BankAccountId == id).Select(a => a.BranchId).First();
            var bankId = db.ChartOfAccLayerSix.Where(f => f.CALayerSixId == iid).Select(b => b.CALayerFiveId).First();
            var layerFourId = db.ChartOfAccLayerFive.Where(e => e.CALayerFiveId == bankId).Select(c => c.CALayerFourId).First();
            BankAccount bankAccount = db.BankAccount.Find(id);
            var layerSevenCode = db.ChartOfAccLayerSeven.Where(e => e.CALayerSevenId == bankAccount.CALayerSevenId).Select(a => a.CALayerSevenCode).First();
            bankAccount.LayerSevenCode = layerSevenCode;
            bankAccount.BankId = bankId;
            bankAccount.LayerFourId = layerFourId;
            if (bankAccount == null)
            {
                return HttpNotFound();
            }
            return Json(bankAccount, JsonRequestBehavior.AllowGet);
        }

        // POST: BankAccounts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "BankAccountId,CompanyId,BankId,BranchId,AccountName,Description,SubsidiryLId,AccountNo,CALayerSevenId,BankAccountCode,CurrencyId")] BankAccount bankAccount)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bankAccount).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bankAccount);
        }

        // GET: BankAccounts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BankAccount bankAccount = db.BankAccount.Find(id);
            if (bankAccount == null)
            {
                return HttpNotFound();
            }
            return View(bankAccount);
        }

        // POST: BankAccounts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BankAccount bankAccount = db.BankAccount.Find(id);
            db.BankAccount.Remove(bankAccount);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        [HttpGet]
        public JsonResult GetLayerFour()
        {
            IList< ChartOfAccLayerFour > list  = db.ChartOfAccLayerFour.Where(x => x.IsActive == true).ToList();
            return   Json(list, JsonRequestBehavior.AllowGet); ;
        }

        [HttpGet]
        public JsonResult GetLayerFive(int layerFourId)
        {
            IList<ChartOfAccLayerFive> list = db.ChartOfAccLayerFive.Where(x => x.IsActive == true && x.CALayerFourId==layerFourId).ToList();
            return Json(list, JsonRequestBehavior.AllowGet); ;
        }[HttpGet]
        public JsonResult GetLayerF()
        {
            IList<ChartOfAccLayerFive> list = db.ChartOfAccLayerFive.Where(x => x.IsActive == true).ToList();
            return Json(list, JsonRequestBehavior.AllowGet); ;
        }[HttpGet]
        public JsonResult GetLayerS()
        {
            IList<ChartOfAccLayerSix> list = db.ChartOfAccLayerSix.Where(x => x.IsActive == true).ToList();
            return Json(list, JsonRequestBehavior.AllowGet); ;
        }

        [HttpGet]
        public JsonResult GetLayerSix(int layerFiveId)
        {
            IList<ChartOfAccLayerSix> list = db.ChartOfAccLayerSix.Where(x => x.IsActive == true && x.CALayerFiveId == layerFiveId).ToList();
            return Json(list, JsonRequestBehavior.AllowGet); ;
        }
        [HttpGet]
        public JsonResult GetCurrency()
        {
            IList<Currency> list = db.Currency.ToList();
            return Json(list, JsonRequestBehavior.AllowGet); ;
        }

        [HttpGet]
        public JsonResult GetLayerTree()
        {
            IList<ChartOfAccLayerThree> list = db.ChartOfAccLayerThree.Where(x => x.IsActive == true).ToList();
            return Json(list, JsonRequestBehavior.AllowGet); ;
        }

        public ActionResult SaveLayer(BankAccount entity)
        {
            //using (var dbtransac = db.Database.BeginTransaction())
            //{
                try
                {
                    ResultResponse aResponse = new ResultResponse();

                    
                    var list = db.ChartOfAccLayerSeven.Where(x => x.CALayerSevenName == entity.AccountNo).ToList();
                    if (list.Any() && entity.BankAccountId == 0)
                    {
                        aResponse.msg = "duplicate Entry Not Possible";
                        return Json(aResponse);
                    }
                    else
                    {
                        ChartOfAccLayerSeven aEntity = new ChartOfAccLayerSeven();
                        aEntity.CALayerSevenId = entity.CALayerSevenId;
                        if (entity.BankAccountId == 0)
                        {
                            ////aEntity.CALayerSevenCode = db.ChartOfAccLayerSeven.Max(x => x.CALayerSevenCode) == null
                            ////    ? 1001
                            ////    : db.ChartOfAccLayerSeven.Max(x => x.CALayerSevenCode) + 1;

                        }
                        else
                        {
                            aEntity.CALayerSevenCode = entity.LayerSevenCode;
                        }

                        aEntity.CALayerSevenName = entity.AccountNo;

                        aEntity.IsActive = true;
                        aEntity.CALayerSixId = entity.BranchId;
                        aEntity.ActiveDate = DateTime.Now;
                        aEntity.EntryBy = Session[SessionCollection.UserName].ToString();
                        aEntity.EntryDate = DateTime.Now;
                        GenericRepository<ChartOfAccLayerSeven> data = new GenericRepository<ChartOfAccLayerSeven>(db);
                        aResponse.isSuccess = aEntity.CALayerSevenId == 0? data.Insert(aEntity): data.Update(aEntity, aEntity.CALayerSevenId);
                        if (aResponse.isSuccess)
                        {
                            entity.CALayerSevenId = aEntity.CALayerSevenId;
                            GenericRepository<BankAccount> bankData = new GenericRepository<BankAccount>(db);

                            aResponse.isSuccess = entity.BankAccountId==0? bankData.Insert(entity): bankData.Update(entity, entity.BankAccountId);
                    }
                        //else
                        //{
                        //    dbtransac.Rollback();
                        //}
                    }
                    return Json(aResponse);
                }
                catch (Exception ex)
                {
                    //dbtransac.Rollback();
                    throw ex;
                }
            //}
            

        }
    }
}
