﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using IGAccounts.Models;
using IGAccounts.Repository;
using IGFoodProject.Models;
using IGFoodProject.Models.Accounts;
using IGFoodProject.Utility;

namespace IGFoodProject.Controllers.Accounts
{
    public class CashAccountsController : Controller
    {
        private CodeDbSetAccounts db = new CodeDbSetAccounts();
        private CodeDbSetHR dbHr = new CodeDbSetHR();

        // GET: CashAccounts
        public ActionResult Index()
        {
            var cashAccount = db.CashAccount.Include(c => c.ChartOfAccLayerSeven);

            var company = dbHr.Company.ToList();
            List<CompanyInformation> coms = company;

            var a = db.CashAccount.ToList();
            var ab = (from c in a
                      join com in coms on c.CompanyId equals com.CompanyId
                      select new CashAccount()
                      {
                          CashAccountId = c.CashAccountId,
                          AccountName = c.AccountName,
                          Description = c.Description,
                          CompanyName = com.CompanyName


                      }

                ).ToList();
            return View(ab);
        }

        // GET: CashAccounts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CashAccount cashAccount = db.CashAccount.Find(id);
            if (cashAccount == null)
            {
                return HttpNotFound();
            }
            return View(cashAccount);
        }

        // GET: CashAccounts/Create
        public ActionResult Create()
        {
            ViewBag.CALayerSevenId = new SelectList(db.ChartOfAccLayerSeven, "CALayerSevenId", "CALayerSevenName");
            return View();
        }

        // POST: CashAccounts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CashAccountId,AccountName,Description,SubsidiryLId,CompanyId,CALayerSevenId")] CashAccount cashAccount)
        {
            if (ModelState.IsValid)
            {
                db.CashAccount.Add(cashAccount);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CALayerSevenId = new SelectList(db.ChartOfAccLayerSeven, "CALayerSevenId", "CALayerSevenName", cashAccount.CALayerSevenId);
            return View(cashAccount);
        }

        // GET: CashAccounts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CashAccount cashAccount = db.CashAccount.Find(id);
            if (cashAccount == null)
            {
                return HttpNotFound();
            }
            //ViewBag.CALayerSevenId = new SelectList(db.ChartOfAccLayerSeven, "CALayerSevenId", "CALayerSevenName", cashAccount.CALayerSevenId);
            ViewBag.id = id;
            return View();
        }


        public ActionResult EditCashAccount(int id)
        {
           
            CashAccount cashAccount= db.CashAccount.Find(id);
            var layerSevenCode = db.ChartOfAccLayerSeven.Where(e => e.CALayerSevenId == cashAccount.CALayerSevenId).Select(a => a.CALayerSevenCode).First();
            var iid = db.ChartOfAccLayerSeven.Where(x => x.CALayerSevenId == cashAccount.CALayerSevenId).Select(a => a.CALayerSixId).First();
            var bankId = db.ChartOfAccLayerSix.Where(f => f.CALayerSixId == iid).Select(b => b.CALayerFiveId).First();
            var layerFourId = db.ChartOfAccLayerFive.Where(e => e.CALayerFiveId == bankId).Select(c => c.CALayerFourId).First();
            cashAccount.LayerSevenCode = layerSevenCode;
            cashAccount.CALayerSixId = iid;
            cashAccount.LayerFiveId = bankId;
            cashAccount.LayerFourId = layerFourId;
            if (cashAccount == null)
            {
                return HttpNotFound();
            }
            return Json(cashAccount, JsonRequestBehavior.AllowGet);
        }


        // POST: CashAccounts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CashAccountId,AccountName,Description,SubsidiryLId,CompanyId,CALayerSevenId")] CashAccount cashAccount)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cashAccount).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CALayerSevenId = new SelectList(db.ChartOfAccLayerSeven, "CALayerSevenId", "CALayerSevenName", cashAccount.CALayerSevenId);
            return View(cashAccount);
        }

        // GET: CashAccounts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CashAccount cashAccount = db.CashAccount.Find(id);
            if (cashAccount == null)
            {
                return HttpNotFound();
            }
            return View(cashAccount);
        }

        // POST: CashAccounts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CashAccount cashAccount = db.CashAccount.Find(id);
            db.CashAccount.Remove(cashAccount);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult SaveLayer(CashAccount entity)
        {
            //using (var dbtransac = db.Database.BeginTransaction())
            //{
            try
            {
                ResultResponse aResponse = new ResultResponse();


                var list = db.ChartOfAccLayerSeven.Where(x => x.CALayerSevenName == entity.AccountName).ToList();
                if (list.Any() && entity.CashAccountId == 0)
                {
                    aResponse.msg = "duplicate Entry Not Possible";
                    return Json(aResponse);
                }
                else
                {
                    ChartOfAccLayerSeven aEntity = new ChartOfAccLayerSeven();
                    aEntity.CALayerSevenId = entity.CALayerSevenId;
                    if (entity.CashAccountId == 0)
                    {
                        //aEntity.CALayerSevenCode = db.ChartOfAccLayerSeven.Max(x => x.CALayerSevenCode) == null
                        //    ? 1001
                        //    : db.ChartOfAccLayerSeven.Max(x => x.CALayerSevenCode) + 1;

                    }
                    else
                    {
                        aEntity.CALayerSevenCode = entity.LayerSevenCode;
                    }

                    aEntity.CALayerSevenName = entity.AccountName;

                    aEntity.IsActive = true;
                    aEntity.CALayerSixId = entity.CALayerSixId;
                    aEntity.ActiveDate = DateTime.Now;
                    aEntity.EntryBy = Session[SessionCollection.UserName].ToString();
                    aEntity.EntryDate = DateTime.Now;
                    GenericRepository<ChartOfAccLayerSeven> data = new GenericRepository<ChartOfAccLayerSeven>(db);
                    aResponse.isSuccess = aEntity.CALayerSevenId == 0 ? data.Insert(aEntity) : data.Update(aEntity, aEntity.CALayerSevenId);
                    if (aResponse.isSuccess)
                    {
                        entity.CALayerSevenId = aEntity.CALayerSevenId;
                        GenericRepository<CashAccount> cashData = new GenericRepository<CashAccount>(db);

                        aResponse.isSuccess = entity.CashAccountId == 0 ? cashData.Insert(entity) : cashData.Update(entity, entity.CashAccountId);
                    }
                    //else
                    //{
                    //    dbtransac.Rollback();
                    //}
                }
                return Json(aResponse);
            }
            catch (Exception ex)
            {
                //dbtransac.Rollback();
                throw ex;
            }
            //}


        }
    }
}
