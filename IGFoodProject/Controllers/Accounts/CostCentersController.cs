﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using IGAccounts.Models;
using IGAccounts.Repository;
using IGFoodProject.Models;
using IGFoodProject.Models.Accounts;
using IGFoodProject.Utility;

namespace IGAccounts.Controllers
{
    public class CostCentersController : Controller
    {
        private CodeDbSetAccounts db = new CodeDbSetAccounts();
      private CodeDbSetHR dbHr = new CodeDbSetHR();

        // GET: CostCenters
        public ActionResult Index()
        {
            var company = dbHr.Company.ToList();
            List<CompanyInformation> coms = company;
            var a = db.CostCenters.ToList();
            var b = db.CostCenterLayers.ToList();
            var d = db.CostCenterTypes.ToList();
            var ab = (from c in a
                      join com in coms on c.CompanyId equals com.CompanyId
                join layer in b on c.CostCenterLayerId equals layer.CostCenterLayerId
                join type in d on c.CostCenterTypeId equals type.CostCenterTypeId
            
            
            
            
                      select new CostCenter()
                      {
                          CostCenterId = c.CostCenterId,
                          CostCenterName = c.CostCenterName,
                          IsActive = c.IsActive,
                          ActiveDate = c.ActiveDate,
                          InActiveDate = c.InActiveDate,
                          Description = c.Description,
                          CompanyName = com.CompanyName,
                          CostCenterTypeName = type.CostCenterTypeName,
                          LayerName = layer.CostCenterLayerName,
                          IsBudgetCenter = c.IsBudgetCenter,
                          IsProfitCenter = c.IsProfitCenter

                      }

                ).ToList();
            return View(ab);
        }

        // GET: CostCenters/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CostCenter costCenter = db.CostCenters.Find(id);
            if (costCenter == null)
            {
                return HttpNotFound();
            }
            return View(costCenter);
        }

        // GET: CostCenters/Create
        public ActionResult Create()
        {
            ViewBag.CompanyId = new SelectList(dbHr.Company, "CompanyId", "CompanyName");
            ViewBag.CostCenterLayerId = new SelectList(db.CostCenterLayers, "CostCenterLayerId", "CostCenterLayerName");
            ViewBag.CostCenterTypeId = new SelectList(db.CostCenterTypes, "CostCenterTypeId", "CostCenterTypeName");
            return View();
        }

        // POST: CostCenters/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CostCenterId,CompanyId,CostCenterTypeId,CostCenterLayerId,ParentId,CostCenterName,Description,IsBudgetCenter,IsProfitCenter,IsActive,ActiveDate,InActiveDate,CreateBy,CreateDate")] CostCenter costCenter)
        {
            if (ModelState.IsValid)
            {
                db.CostCenters.Add(costCenter);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CompanyId = new SelectList(dbHr.Company, "CompanyId", "CompanyName", costCenter.CompanyId);
            ViewBag.CostCenterLayerId = new SelectList(db.CostCenterLayers, "CostCenterLayerId", "CostCenterLayerName", costCenter.CostCenterLayerId);
            ViewBag.CostCenterTypeId = new SelectList(db.CostCenterTypes, "CostCenterTypeId", "CostCenterTypeName", costCenter.CostCenterTypeId);
            return View(costCenter);
        }

        // GET: CostCenters/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CostCenter costCenter = db.CostCenters.Find(id);
            if (costCenter == null)
            {
                return HttpNotFound();
            }
            ViewBag.CostCenterLayerId = new SelectList(db.CostCenterLayers, "CostCenterLayerId", "CostCenterLayerName", costCenter.CostCenterLayerId);
            ViewBag.CostCenterTypeId = new SelectList(db.CostCenterTypes, "CostCenterTypeId", "CostCenterTypeName", costCenter.CostCenterTypeId);
            ViewBag.CostCenterId = id;
            return View(costCenter);
        }

        // POST: CostCenters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CostCenterId,CompanyId,CostCenterTypeId,CostCenterLayerId,ParentId,CostCenterName,Description,IsBudgetCenter,IsProfitCenter,IsActive,ActiveDate,InActiveDate,CreateBy,CreateDate")] CostCenter costCenter)
        {
            if (ModelState.IsValid)
            {
                db.Entry(costCenter).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CompanyId = new SelectList(dbHr.Company, "CompanyId", "CompanyName", costCenter.CompanyId);
            ViewBag.CostCenterLayerId = new SelectList(db.CostCenterLayers, "CostCenterLayerId", "CostCenterLayerName", costCenter.CostCenterLayerId);
            ViewBag.CostCenterTypeId = new SelectList(db.CostCenterTypes, "CostCenterTypeId", "CostCenterTypeName", costCenter.CostCenterTypeId);
            return View(costCenter);
        }

        // GET: CostCenters/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CostCenter costCenter = db.CostCenters.Find(id);
            if (costCenter == null)
            {
                return HttpNotFound();
            }
            return View(costCenter);
        }

        // POST: CostCenters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CostCenter costCenter = db.CostCenters.Find(id);
            db.CostCenters.Remove(costCenter);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpGet]
        public JsonResult GetParent(int id)
        {
            IList<CostCenter> list= new List<CostCenter>() ;
            if (id != 1)
            {
                 list = db.CostCenters.Where(c=> c.CostCenterLayerId== (id-1)).ToList();
           
            }
             return Json(list, JsonRequestBehavior.AllowGet); ;
        }

        public ActionResult SaveCostCenter(CostCenter entity)
        {
            //using (var dbtransac = db.Database.BeginTransaction())
            //{
            try
            {
                ResultResponse aResponse = new ResultResponse();


                var list = db.CostCenters.Where(x => x.CostCenterName == entity.CostCenterName).ToList();
                if (list.Any() && entity.CostCenterId == 0)
                {
                    aResponse.msg = "duplicate Entry Not Possible";
                    return Json(aResponse);
                }
                else
                {

                    entity.CreateBy = Session[SessionCollection.UserName].ToString();
                    entity.CreateDate = DateTime.Now;
                    GenericRepository<CostCenter> saveData = new GenericRepository<CostCenter>(db);
                    aResponse.isSuccess = entity.CostCenterId == 0
                            ? saveData.Insert(entity)
                            : saveData.Update(entity, entity.CostCenterId);
                }
                return Json(aResponse);
            }
            catch (Exception ex)
            {
                //dbtransac.Rollback();
                throw ex;
            }
            //}


        }
        public ActionResult GetDataFEdit(int id)
        {
            var result = db.CostCenters.Find(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
