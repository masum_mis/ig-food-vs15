﻿using IGAccounts.Models;
using IGAccounts.DAL;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGAccounts.Repository;
using IGFoodProject.Models;
using IGFoodProject.Controllers;
using IGFoodProject.Models.Accounts;
using IGFoodProject.Utility;

namespace IGAccounts.Controllers
{
    public class COAHeadConfigureController : Controller
    {
        //private CodeDbSet db = new CodeDbSet();
        private CodeDbSetAccounts db = new CodeDbSetAccounts();
        // GET: COAHeadConfigure
        public ActionResult Index()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                var result = db.COAHeadConfigures.Include(p => p.AnOperationMaster).Include(p => p.AnOperationDetails).Include(p=>p.ChartOfAccLayerSeven).ToList();
                return View(result);
            }
            // return View();
        }

        public ActionResult LoadOperation()
        {
            return Json(db.AnOperationMasters.ToList(), JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadCOASavenLayer()
        {
            return Json(db.ChartOfAccLayerSeven.ToList(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetOperationDetails(int operationMasterId, int coaHeadConfiId = 0)
        {
            List<COAHeadConfigureViewModel> cList = new List<COAHeadConfigureViewModel>();
            if (coaHeadConfiId == 0 && operationMasterId != 0)
            {
                List<AnOperationDetails> aList = db.AnOperationDetails.Where(p => p.OperationMasterId == operationMasterId).ToList();
                foreach (var item in aList)
                {
                    COAHeadConfigureViewModel ci = new COAHeadConfigureViewModel();
                    ci.OperationDetailsId = item.OperationDetailsId;
                    ci.InputTypeName = item.InputTypeName;
                    ci.InputTypeValue = item.InputTypeValue;
                    cList.Add(ci);
                }
            }
            else
            {
                var chc = db.COAHeadConfigures.Include(p => p.AnOperationDetails).Where(p => p.COAHeadConfigureId == coaHeadConfiId).FirstOrDefault();

                cList.Add(new COAHeadConfigureViewModel()
                {
                    COAHeadConfigureId = chc.COAHeadConfigureId,
                    OperationMasterId = chc.OperationMasterId,
                    OperationDetailsId = chc.OperationDetailsId,
                    CALayerSevenId = chc.CALayerSevenId,
                    InputTypeName = chc.AnOperationDetails.InputTypeName,
                    InputTypeValue = chc.AnOperationDetails.InputTypeValue,
                    CrOrDr = chc.CrOrDr,
                    IsCrIncrease = chc.IsCrIncrease
                });
            }

            return PartialView("_PartialOperationDetails", cList);
        }
        //public ActionResult EditCoaHeadConfigure(int COAHeadConfigureId)
        //{
        //    var chc = db.COAHeadConfigures.Include(p => p.AnOperationDetails).Where(p => p.COAHeadConfigureId == COAHeadConfigureId).FirstOrDefault();
        //    List<COAHeadConfigureViewModel> cList = new List<COAHeadConfigureViewModel>();
        //    var cc= cList.Select(c => { c.COAHeadConfigureId = chc.COAHeadConfigureId; chc.OperationMasterId = chc.OperationMasterId; return c; }).ToList();

        //    cList.Add(new COAHeadConfigureViewModel() { COAHeadConfigureId = chc.COAHeadConfigureId,
        //    OperationMasterId = chc.OperationMasterId,
        //    OperationDetailsId = chc.OperationDetailsId,
        //    CALayerSevenId = chc.CALayerSevenId,
        //    InputTypeName = chc.AnOperationDetails.InputTypeName,
        //    InputTypeValue = chc.AnOperationDetails.InputTypeValue,
        //    CrOrDr = chc.CrOrDr,
        //    IsCrIncrease = chc.IsCrIncrease });
        //    //return Json(chc, JsonRequestBehavior.AllowGet);
        //    return PartialView("_PartialOperationDetails", cList);
        //}
        public ActionResult SaveCoaHeadConfigure(List<COAHeadConfigure> coaheadcongObj)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                foreach (var headconfi in coaheadcongObj)
                {
                    COAHeadConfigure aEntity = new COAHeadConfigure();

                    aEntity = headconfi;
                    if (headconfi.COAHeadConfigureId == 0)
                    {
                        aEntity.CreateBy = Session[SessionCollection.UserName].ToString();
                        aEntity.CreateDate = DateTime.Now;
                    }
                    else
                    {
                        aEntity.UpdateBy = Session[SessionCollection.UserName].ToString();
                        aEntity.UpdateDate = DateTime.Now;
                    }

                    GenericRepository<COAHeadConfigure> data = new GenericRepository<COAHeadConfigure>(db);
                    aResponse.isSuccess = headconfi.COAHeadConfigureId == 0 ? data.Insert(aEntity) : data.Update(aEntity, headconfi.COAHeadConfigureId);
                }
                return Json(aResponse);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}