﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using IGAccounts.Models;
using IGAccounts.Repository;
using IGFoodProject.Models;
using IGFoodProject.Models.Accounts;
using IGFoodProject.Utility;

namespace IGAccounts.Controllers
{
    public class CostCenterTypesController : Controller
    {
        //protected override void OnActionExecuting(ActionExecutingContext filterContext)
        //{
        //    string loginUserEmpId = Session["loginuserempid"] as string;
        //    if (string.IsNullOrEmpty(loginUserEmpId))
        //    {
        //         RedirectToAction(nameof(HomeController.Login), "Home");
        //    }
        //}

      
        private CodeDbSetAccounts db = new CodeDbSetAccounts();
        private CodeDbSetHR dbHr = new CodeDbSetHR();
        //private  CodeDbSetHR dbHr= new CodeDbSetHR();
        // GET: CostCenterTypes
        public ActionResult Index()
        {
            var company = dbHr.Company.ToList();
            List<CompanyInformation> coms = company;
            var a = db.CostCenterTypes.ToList();
            var ab = (from c in a
                join com in coms on c.CompanyId equals com.CompanyId
                select new CostCenterType()
                {
                     CostCenterTypeId = c.CostCenterTypeId,
                    CostCenterTypeName = c.CostCenterTypeName,
                    IsActive = c.IsActive,
                    ActiveDate = c.ActiveDate,
                    InActiveDate = c.InActiveDate,
                    Description = c.Description,
                    CompanyName = com.CompanyName


                }

                );
            //var costCenterTypes = db.CostCenterTypes.Include(c=> c.Company);
            return View(ab);
        }

        // GET: CostCenterTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CostCenterType costCenterType = db.CostCenterTypes.Find(id);
            if (costCenterType == null)
            {
                return HttpNotFound();
            }
            return View(costCenterType);
        }

        // GET: CostCenterTypes/Create
        public ActionResult Create()
        {
            ViewBag.CompanyId = new SelectList(dbHr.Company, "CompanyId", "CompanyName");
            return View();
        }

        // POST: CostCenterTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CostCenterTypeId,CostCenterTypeName,IsActive,ActiveDate,InActiveDate,Description,CompanyId")] CostCenterType costCenterType)
        {
            if (ModelState.IsValid)
            {
                db.CostCenterTypes.Add(costCenterType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CompanyId = new SelectList(dbHr.Company, "CompanyId", "CompanyName", costCenterType.CompanyId);
            return View(costCenterType);
        }

        // GET: CostCenterTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ////CostCenterType costCenterType = db.CostCenterTypes.Find(id);
            //if (costCenterType == null)
            //{
            //    return HttpNotFound();
            //}
            //ViewBag.CompanyId = new SelectList(db.Companies, "CompanyId", "CompanyName", costCenterType.CompanyId);
            ViewBag.id = id;
            return View(/*costCenterType*/);
        }

        // POST: CostCenterTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CostCenterTypeId,CostCenterTypeName,IsActive,ActiveDate,InActiveDate,Description,CompanyId")] CostCenterType costCenterType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(costCenterType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CompanyId = new SelectList(dbHr.Company, "CompanyId", "CompanyName", costCenterType.CompanyId);
            return View(costCenterType);
        }

        // GET: CostCenterTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CostCenterType costCenterType = db.CostCenterTypes.Find(id);
            if (costCenterType == null)
            {
                return HttpNotFound();
            }
            return View(costCenterType);
        }

        // POST: CostCenterTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CostCenterType costCenterType = db.CostCenterTypes.Find(id);
            db.CostCenterTypes.Remove(costCenterType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        public ActionResult SaveCostCenter(CostCenterType entity)
        {
            //using (var dbtransac = db.Database.BeginTransaction())
            //{
            try
            {
                ResultResponse aResponse = new ResultResponse();


                var list = db.CostCenterTypes.Where(x => x.CostCenterTypeName == entity.CostCenterTypeName).ToList();
                if (list.Any() && entity.CostCenterTypeId == 0)
                {
                    aResponse.msg = "duplicate Entry Not Possible";
                    return Json(aResponse);
                }
                else
                {
                    
                        entity.CreateBy= Session[SessionCollection.UserName].ToString();
                    entity.CreateDate=DateTime.Now;
                    GenericRepository<CostCenterType> saveData = new GenericRepository<CostCenterType>(db);
                    aResponse.isSuccess = entity.CostCenterTypeId == 0
                            ? saveData.Insert(entity)
                            : saveData.Update(entity, entity.CostCenterTypeId);
                }
                return Json(aResponse);
            }
            catch (Exception ex)
            {
                //dbtransac.Rollback();
                throw ex;
            }
            //}


        }

        public ActionResult GetDataFEdit(int id)
        {
            var result = db.CostCenterTypes.Find(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
