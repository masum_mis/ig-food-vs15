﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.Models.Accounts;
using IGFoodProject.DAL.Accounts;
using System.Data;
using IGFoodProject.Models;
using IGFoodProject.Utility;

namespace IGFoodProject.Controllers.Accounts
{
    public class OpeningBalanceController : Controller
    {
        OpeningBalanceDAL opdal = new OpeningBalanceDAL();
        // GET: OpeningBalance
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAllGL(int id)
        {
            List<ChartOfAccLayerSeven> cu = opdal.GetAllGL(id);
            return Json(cu, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetOpeningValance(DateTime date, int LG = 0, int LevelTwo = 0, int LevelThree = 0, int LevelFour = 0, int LevelFive = 0, int LevelSix = 0, int GL = 0)
        {
            List<OpeningBalanceView> op = opdal.GetOpeningValance(date, LG, LevelTwo, LevelThree, LevelFour, LevelFive, LevelSix, GL);
            return PartialView("_PartialOpeningBalance", op);
        }

        public ActionResult SaveOpeningBalance(List<OpeningBalance> openingBalance)
        {
            ResultResponse aResponse = new ResultResponse();
            aResponse.isSuccess = opdal.SaveOpeningBalance(openingBalance, Session[SessionCollection.UserName].ToString());
            return Json(aResponse, JsonRequestBehavior.AllowGet);

        }
    }
}