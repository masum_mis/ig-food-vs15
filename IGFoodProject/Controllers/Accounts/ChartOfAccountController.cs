﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using IGAccounts.DAL;
using IGAccounts.Repository;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Models.Accounts;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;

namespace IGFoodProject.Controllers.Accounts
{
    public class ChartOfAccountController : Controller
    {
        private CodeDbSetAccounts db = new CodeDbSetAccounts();
        ChartOfAccountDAL _aDal = new ChartOfAccountDAL();
        // GET: ChartOfAccount
        public ActionResult Index()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                var result = _aDal.GetSectosAndDetails();
              //  var a = db.LedgerGroup.Include(p => p.ChartOfAccLayerTwoList).ToList();
                return View(result);
            }
        }

        public ActionResult GetCode(int layerId, int prantId)
        {
            var fresult = "";
            if (layerId == 2)
            {
                var max = db.ChartOfAccLayerTwo.Max(x => x.CALayerTwoId) + 1;
                var result = db.LedgerGroup.Where(x => x.LedgerGroupId == prantId).Select(x => x.LedgerGroupCode).FirstOrDefault();
                fresult = result + "-" + max;
            }
            if (layerId == 3)
            {
                var max = db.ChartOfAccLayerThree.Max(x => x.CALayerThreeId) + 1;
                var result = (from a in db.ChartOfAccLayerTwo
                              join b in db.LedgerGroup on a.LedgerGroupId equals b.LedgerGroupId
                              where a.CALayerTwoId == prantId
                              select new
                              {
                                  CALayerTwoCode =  a.CALayerTwoCode
                              }).FirstOrDefault();
                var res = result.CALayerTwoCode;
                string[] tokens = res.Split('-');
                fresult = tokens[0] + "-" + max+ tokens[1];

            }
            if (layerId == 4)
            {

                var max =  db.ChartOfAccLayerFour.Max(x => x.CALayerFourId) + 1;
                var result = (from e in db.ChartOfAccLayerThree
                              where e.CALayerThreeId == prantId
                              select new
                              {
                                  CALayerTwoCode = e.CALayerThreeCode
                              }).FirstOrDefault();
                var res = result.CALayerTwoCode;
                string[] tokens = res.Split('-');
                fresult = tokens[0] + "-" + max + tokens[1];

            }
            if (layerId == 5)
            {

                var max =  db.ChartOfAccLayerFive.Max(x => x.CALayerFiveId) + 1;
                var result = (from d in db.ChartOfAccLayerFour
                              where d.CALayerFourId == prantId
                              select new
                              {
                                  CALayerTwoCode = d.CALayerFourCode
                              }).FirstOrDefault();
                var res = result.CALayerTwoCode;
                string[] tokens = res.Split('-');
                fresult = tokens[0] + "-" + max + tokens[1];

            }
            if (layerId == 6)
            {
                var max = db.ChartOfAccLayerSix.Max(x => x.CALayerSixId) + 1;
                var result = (from c in db.ChartOfAccLayerFive
                              where c.CALayerFiveId == prantId
                              select new
                              {
                                  CALayerTwoCode =  c.CALayerFiveCode
                              }).FirstOrDefault();
                var res = result.CALayerTwoCode;
                string[] tokens = res.Split('-');
                fresult = tokens[0] + "-" + max + tokens[1];

            }
            if (layerId == 7)
            {
               
                var max =  db.ChartOfAccLayerSeven.Max(x => x.CALayerSevenId) + 1;
                var result = (from b in db.ChartOfAccLayerSix
                             
                              where b.CALayerSixId == prantId
                              select new
                              {
                                  CALayerTwoCode = b.CALayerSixCode
                              }).FirstOrDefault();
                var res = result.CALayerTwoCode;
                string[] tokens = res.Split('-');
                fresult = tokens[0] + "-" + max + tokens[1];


            }
            return Json(fresult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveLayer(LedgerGroup entity)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                if (entity.LayerId == 2)
                {
                    var list = db.ChartOfAccLayerTwo.Where(x => x.CALayerTwoName == entity.LedgerGroupName).ToList();
                    if (list.Any() && entity.LedgerGroupId == 0)
                    {
                        aResponse.msg = "duplicate Entry Not Possible";
                        return Json(aResponse);
                    }
                    else
                    {
                        ChartOfAccLayerTwo aEntity = new ChartOfAccLayerTwo();
                        aEntity.CALayerTwoId = entity.LedgerGroupId;
                        if (entity.LedgerGroupId == 0)
                        {
                            //aEntity.CALayerTwoCode = db.ChartOfAccLayerTwo.Max(x => x.CALayerTwoId) == null
                            //    ? 10
                            //    : db.ChartOfAccLayerTwo.Max(x => x.CALayerTwoId) + 1;
                        }
                        else
                        {
                            aEntity.CALayerTwoCode = entity.Code;
                            aEntity.UpdateBy = Session[SessionCollection.UserName].ToString();
                            aEntity.UpdateDate = DateTime.Now;
                        }

                        aEntity.CALayerTwoName = entity.LedgerGroupName;
                        aEntity.LedgerGroupId = entity.ParentId;
                        aEntity.IsActive = entity.IsActive;
                        aEntity.ActiveDate = DateTime.Now;
                        aEntity.EntryBy = Session[SessionCollection.UserName].ToString();
                        aEntity.EntryDate = DateTime.Now;
                        aEntity.Description = entity.Description;
                        GenericRepository<ChartOfAccLayerTwo> data = new GenericRepository<ChartOfAccLayerTwo>(db);
                        aResponse.isSuccess = aEntity.CALayerTwoId == 0 ? data.Insert(aEntity) : data.Update(aEntity, aEntity.CALayerTwoId);
                    }
                }
                if (entity.LayerId == 3)
                {
                    var list = db.ChartOfAccLayerThree.Where(x => x.CALayerThreeName == entity.LedgerGroupName).ToList();
                    if (list.Any() && entity.LedgerGroupId == 0)
                    {
                        aResponse.msg = "duplicate Entry Not Possible";
                        return Json(aResponse);
                    }
                    else
                    {
                        ChartOfAccLayerThree aEntity = new ChartOfAccLayerThree();
                        aEntity.CALayerThreeId = entity.LedgerGroupId;
                        if (entity.LedgerGroupId == 0)
                        {
                            //aEntity.CALayerThreeCode = db.ChartOfAccLayerThree.Max(x => x.CALayerThreeCode) == null
                            //    ? 10
                            //    : db.ChartOfAccLayerThree.Max(x => x.CALayerThreeCode) + 1;

                        }
                        else
                        {
                            aEntity.CALayerThreeCode = entity.Code;
                            aEntity.UpdateBy = Session[SessionCollection.UserName].ToString();
                            aEntity.UpdateDate = DateTime.Now;
                        }

                        aEntity.CALayerThreeName = entity.LedgerGroupName;
                        aEntity.Description = entity.Description;
                        aEntity.CALayerTwoId = entity.ParentId;
                        aEntity.IsActive = entity.IsActive;
                        aEntity.ActiveDate = DateTime.Now;
                        aEntity.EntryBy = Session[SessionCollection.UserName].ToString();
                        aEntity.EntryDate = DateTime.Now;
                        GenericRepository<ChartOfAccLayerThree> data = new GenericRepository<ChartOfAccLayerThree>(db);
                        aResponse.isSuccess = aEntity.CALayerThreeId == 0
                            ? data.Insert(aEntity)
                            : data.Update(aEntity, aEntity.CALayerThreeId);
                    }
                }
                if (entity.LayerId == 4)
                {
                    var list = db.ChartOfAccLayerFour.Where(x => x.CALayerFourName == entity.LedgerGroupName).ToList();
                    if (list.Any() && entity.LedgerGroupId == 0)
                    {
                        aResponse.msg = "duplicate Entry Not Possible";
                        return Json(aResponse);
                    }
                    else
                    {
                        ChartOfAccLayerFour aEntity = new ChartOfAccLayerFour();
                        aEntity.CALayerFourId = entity.LedgerGroupId;
                        if (entity.LedgerGroupId == 0)
                        {
                            //aEntity.CALayerFourCode = db.ChartOfAccLayerFour.Max(x => x.CALayerFourCode) == null
                            //    ? 10
                            //    : db.ChartOfAccLayerFour.Max(x => x.CALayerFourCode) + 1;

                        }
                        else
                        {
                            aEntity.CALayerFourCode = entity.Code;
                            aEntity.UpdateBy = Session[SessionCollection.UserName].ToString();
                            aEntity.UpdateDate = DateTime.Now;
                        }
                        aEntity.CALayerFourName = entity.LedgerGroupName;
                        aEntity.Description = entity.Description;
                        aEntity.CALayerThreeId = entity.ParentId;
                        aEntity.IsActive = entity.IsActive;
                        aEntity.ActiveDate = DateTime.Now;
                        aEntity.EntryBy = Session[SessionCollection.UserName].ToString();
                        aEntity.EntryDate = DateTime.Now;
                        GenericRepository<ChartOfAccLayerFour> data = new GenericRepository<ChartOfAccLayerFour>(db);
                        aResponse.isSuccess = aEntity.CALayerFourId == 0
                            ? data.Insert(aEntity)
                            : data.Update(aEntity, aEntity.CALayerFourId);
                        aResponse.pk = aEntity.CALayerFourId;
                    }
                }
                if (entity.LayerId == 5)
                {
                    var list = db.ChartOfAccLayerFive.Where(x => x.CALayerFiveName == entity.LedgerGroupName).ToList();
                    if (list.Any() && entity.LedgerGroupId == 0)
                    {
                        aResponse.msg = "duplicate Entry Not Possible";
                        return Json(aResponse);
                    }
                    else
                    {
                        ChartOfAccLayerFive aEntity = new ChartOfAccLayerFive();
                        aEntity.CALayerFiveId = entity.LedgerGroupId;
                        if (entity.LedgerGroupId == 0)
                        {
                            //aEntity.CALayerFiveCode = db.ChartOfAccLayerFive.Max(x => x.CALayerFiveCode) == null
                            //    ? 10
                            //    : db.ChartOfAccLayerFive.Max(x => x.CALayerFiveCode) + 1;

                        }
                        else
                        {
                            aEntity.CALayerFiveCode = entity.Code;
                            aEntity.UpdateBy = Session[SessionCollection.UserName].ToString();
                            aEntity.UpdateDate = DateTime.Now;
                        }
                        aEntity.CALayerFiveName = entity.LedgerGroupName;
                        aEntity.Description = entity.Description;
                        aEntity.IsActive = entity.IsActive;
                        aEntity.CALayerFourId = entity.ParentId;

                        aEntity.ActiveDate = DateTime.Now;
                        aEntity.EntryBy = Session[SessionCollection.UserName].ToString();
                        aEntity.EntryDate = DateTime.Now;
                        GenericRepository<ChartOfAccLayerFive> data = new GenericRepository<ChartOfAccLayerFive>(db);
                        aResponse.isSuccess = aEntity.CALayerFiveId == 0
                            ? data.Insert(aEntity)
                            : data.Update(aEntity, aEntity.CALayerFiveId);

                        aResponse.pk = aEntity.CALayerFiveId;
                    }

                }
                if (entity.LayerId == 6)
                {
                    var list = db.ChartOfAccLayerSix.Where(x => x.CALayerSixName == entity.LedgerGroupName).ToList();
                    if (list.Any() && entity.LedgerGroupId == 0)
                    {
                        aResponse.msg = "duplicate Entry Not Possible";
                        return Json(aResponse);
                    }
                    else
                    {
                        ChartOfAccLayerSix aEntity = new ChartOfAccLayerSix();
                        aEntity.CALayerSixId = entity.LedgerGroupId;
                        if (entity.LedgerGroupId == 0)
                        {
                            //aEntity.CALayerSixCode = db.ChartOfAccLayerSix.Max(x => x.CALayerSixCode) == null
                            //    ? 10
                            //    : db.ChartOfAccLayerSix.Max(x => x.CALayerSixCode) + 1;

                        }
                        else
                        {
                            aEntity.CALayerSixCode = entity.Code;
                            aEntity.UpdateBy = Session[SessionCollection.UserName].ToString();
                            aEntity.UpdateDate = DateTime.Now;
                        }
                        aEntity.CALayerSixName = entity.LedgerGroupName;
                        aEntity.Description = entity.Description;
                        aEntity.IsActive = entity.IsActive;
                        aEntity.CALayerFiveId = entity.ParentId;
                        aEntity.IsActive = entity.IsActive;

                        aEntity.ActiveDate = DateTime.Now;
                        aEntity.EntryBy = Session[SessionCollection.UserName].ToString();
                        aEntity.EntryDate = DateTime.Now;
                        GenericRepository<ChartOfAccLayerSix> data = new GenericRepository<ChartOfAccLayerSix>(db);
                        aResponse.isSuccess = aEntity.CALayerSixId == 0
                            ? data.Insert(aEntity)
                            : data.Update(aEntity, aEntity.CALayerSixId);
                        aResponse.pk = aEntity.CALayerSixId;
                    }

                }
                if (entity.LayerId == 7)
                {
                    var list = db.ChartOfAccLayerSeven.Where(x => x.CALayerSevenName == entity.LedgerGroupName).ToList();
                    if (list.Any() && entity.LedgerGroupId == 0)
                    {
                        aResponse.msg = "duplicate Entry Not Possible";
                        return Json(aResponse);
                    }
                    else
                    {
                        ChartOfAccLayerSeven aEntity = new ChartOfAccLayerSeven();
                        aEntity.CALayerSevenId = entity.LedgerGroupId;
                        if (entity.LedgerGroupId == 0)
                        {
                            //aEntity.CALayerSevenCode = db.ChartOfAccLayerSeven.Max(x => x.CALayerSevenCode) == null
                            //    ? 1001
                            //    : db.ChartOfAccLayerSeven.Max(x => x.CALayerSevenCode) + 1;

                        }
                        else
                        {
                            aEntity.CALayerSevenCode = entity.Code;
                            aEntity.UpdateBy = Session[SessionCollection.UserName].ToString();
                            aEntity.UpdateDate = DateTime.Now;
                        }
                        aEntity.CALayerSevenName = entity.LedgerGroupName;
                        aEntity.Description = entity.Description;
                        aEntity.IsActive = entity.IsActive;
                        aEntity.CALayerSixId = entity.ParentId;
                        aEntity.IsActive = entity.IsActive;

                        aEntity.ActiveDate = DateTime.Now;
                        aEntity.EntryBy = Session[SessionCollection.UserName].ToString();
                        aEntity.EntryDate = DateTime.Now;
                        GenericRepository<ChartOfAccLayerSeven> data = new GenericRepository<ChartOfAccLayerSeven>(db);
                        aResponse.isSuccess = aEntity.CALayerSevenId == 0
                            ? data.Insert(aEntity)
                            : data.Update(aEntity, aEntity.CALayerSevenId);
                        aResponse.pk = aEntity.CALayerSixId;
                    }
                }
                return Json(aResponse);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }


#region GL Setup

        public ActionResult GLSetup(int glid=0)
        {
            ViewBag.glid = glid;
            return View();
        }
        public ActionResult GetAllCoaLevelSix( int id)
        {
            List<ChartOfAccLayerSix> coa = _aDal.GetCoaAllLayerSix(id);
            return Json(coa, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAllCostCenter(int glid)
        {
            List<CostCenter> cc = _aDal.GetAllCostCenter(glid);
            return Json(cc, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadCostCenterInPartialView(int glid)
        {
            List<CostCenter> cc = _aDal.GetAllCostCenter(glid);
            return PartialView("_CostCenterPartial", cc);
        }

        public ActionResult LoadReferanceInPartialView(int glid)
        {
            List<ReferanceType> cc = _aDal.GetAllReferanceType(glid);
            return PartialView("_RefPartial", cc);
        }
        public ActionResult GenerateGLCode(int parentId)
        {
            GLSetup gl = _aDal.GenerateGLCode(parentId);
            return Json(gl, JsonRequestBehavior.AllowGet);
        }


        public ActionResult SaveGL(LedgerGroup entity)
        {
            try
            {
              
                string entryBy = Session[SessionCollection.UserName].ToString();
                ResultResponse aResponse =  _aDal.SaveNewGL(entity, entryBy);

                return Json(aResponse, JsonRequestBehavior.AllowGet);

              
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public ActionResult GLIndex()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                DataTable result = _aDal.GetAllGL();
                return View(result);
            }
        }

        public ActionResult GetAllLedgerGroup()
        {
            List<LedgerGroup> coa = _aDal.GetAllLedgerGroup();
            return Json(coa, JsonRequestBehavior.AllowGet);
        }
      
        public ActionResult GetAllCOATwo(int id)
        {
            List<ChartOfAccLayerTwo> coa = _aDal.GetAllLayerTwoByLayerOne(id);
            return Json(coa, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAllCOAThree(int id)
        {
            List<ChartOfAccLayerThree> coa = _aDal.GetAllLayerThreeByLayerTwo(id);
            return Json(coa, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAllCOAFour(int id)
        {
            List<ChartOfAccLayerFour> coa = _aDal.GetAllLayerFourByLayerThree(id);
            return Json(coa, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAllCOAFive(int id)
        {
              List<ChartOfAccLayerFive> coa = _aDal.GetAllLayerFiveByLayerFour(id);
            return Json(coa, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RefreanceTypeDetails()
        {

            return View();

        }
        public ActionResult RefreanceTypeList(int glid=0)
        {
            List<ReferanceType> cc = _aDal.GetAllReferanceType(glid);
            return Json(cc, JsonRequestBehavior.AllowGet);


        }
        public ActionResult LoadCustomerPartialView(int refId)
        {
           
            List<ViewCustomerInfo> aList = _aDal.GetCustomerListForReferanceDetails(refId);
           
            return PartialView("_CustomerPartial", aList);
        }
       public ActionResult LoadSupplierPartialView(int refId)
        {
         
       
            List<tbl_SupplierInfo> alist = _aDal.GetSupplierListForReferanceDetails(refId);

            return PartialView("_SupplierPartial", alist);
        }
        public ActionResult LoadEeployeePartialView(int refId)
        {
          
       
            List<View_EmployeeInfo> alist = _aDal.GetEmployeeListForReferanceDetails(refId);

            return PartialView("_EmployeePartial", alist);
        }
        public ActionResult LoadModulePartialView(int refId)
        {
          
       
            List<Modules> alist = _aDal.GetModuleForReferanceDetails(refId);

            return PartialView("_ModulePartial", alist);
        }

        public ActionResult SaveReferanceDetails(List<RefarenceTypeDetails> entity)
        {
            try
            {

                string entryBy = Session[SessionCollection.UserName].ToString();
                var aResponse= _aDal.SaveRefarenceDetails(entity, entryBy);

                return Json(aResponse, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        #endregion

        public ActionResult Design()
        {
            
                return View();
            
        }
        #region gl edit
        public ActionResult GetGlMasterById(int glid)
        {
            try
            {

               
               GLViewModel list = _aDal.GetGLDetailsById(glid);

                return Json(list, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {

                throw ex;
            }

        }


        public ActionResult GetBankAccountByGlId(int glid)
        {
            try
            {


                GLViewModel list = _aDal.GetBankAccountByGlId(glid);

                return Json(list, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
     
        #endregion




    }
}