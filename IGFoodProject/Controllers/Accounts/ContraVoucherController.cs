﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL.Accounts;
using IGFoodProject.Models;
using IGFoodProject.Models.Accounts;
using IGFoodProject.Utility;

namespace IGFoodProject.Controllers.Accounts
{
    public class ContraVoucherController : Controller
    {
        ContraVoucherDal dbDal = new ContraVoucherDal();
        // GET: ContraVoucher
        public ActionResult Index(DateTime? fromdate, DateTime? todate, int CompanyId = 0, int BUId = 0)
        {
            List<VoucherMasterView> aList = dbDal.ContraList(fromdate, todate, CompanyId, BUId);
            return View(aList);
        }
        public ActionResult NewContra(int VoucherId = 0, string eflag = "ed", DateTime? fromdate=null, DateTime? todate=null)
        {
            ViewBag.VoucherId = VoucherId;
            ViewBag.eflag = eflag;
            ViewBag.fromdate = fromdate;
            ViewBag.todate = todate;
            return View();
        }
        public ActionResult AddRowForContra(int tr)
        {
            TempData["trSl"] = tr;
            return PartialView("_addNewContraGLRow");
        }
        public ActionResult AddRowForContraCr(int tr)
        {
            TempData["trSl"] = tr;
            return PartialView("_addNewContraGLRowCr");
        }


        public ActionResult SaveContra(VoucherMaster entity)
        {
            try
            {
                string entryBy = Session[SessionCollection.UserName].ToString();
            ResultResponse aResponse = dbDal.SaveContra(entity, entryBy);
                return Json(aResponse, JsonRequestBehavior.AllowGet);
               // return Json( JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public ActionResult ContraListData(DateTime? fromdate, DateTime? todate, int CompanyId = 0, int BUId = 0)
        {
            List<VoucherMasterView> aList = dbDal.ContraList(fromdate, todate, CompanyId, BUId);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }

        //------------Contra Approval-----------
        public ActionResult ContraVoucherApproval(DateTime? fromdate, DateTime? todate, int CompanyId = 0, int BUId = 0)
        {
            List<VoucherMasterView> aList = dbDal.ContraListForApproval(fromdate, todate, CompanyId, BUId);
            if (fromdate == null)
            {
                fromdate = System.DateTime.Now.AddDays(-1);

            }
            if (todate == null)
            {
                todate = System.DateTime.Now;

            }
            ViewBag.fromdate2 = fromdate;
            ViewBag.todate2 = todate;
            return View(aList);
        }
        public ActionResult ContraVoucherApprovalListData(DateTime? fromdate, DateTime? todate, int CompanyId = 0, int BUId = 0, int statusType=100)
        {
            List<VoucherMasterView> aList = dbDal.ContraListForApproval(fromdate, todate, CompanyId, BUId, statusType);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }

    }
}