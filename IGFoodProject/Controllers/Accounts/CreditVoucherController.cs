﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.Models.Accounts;
using IGFoodProject.DAL.Accounts;
using IGFoodProject.Models;
using IGFoodProject.Utility;

namespace IGFoodProject.Controllers.Accounts
{
    public class CreditVoucherController : Controller
    {
        CreditVoucherDAL cbDal = new CreditVoucherDAL();
        // GET: DebitVoucher
        //public ActionResult Index()
        //{
        //    return View();
        //}
        public ActionResult AddRowForVoucher(int tr)
        {
            TempData["trSl"] = tr;

            return PartialView("_addNewGLRow");
        }
        public ActionResult Create(int VoucherId = 0, string eflag = "ed", DateTime? fromdate = null, DateTime? todate = null)
        {
            ViewBag.VoucherId = VoucherId;
            ViewBag.eflag = eflag;
            ViewBag.fromdate = fromdate;
            ViewBag.todate = todate;
            return View();
        }
        public ActionResult GetCreditVoucherGL()
        {
            List<ChartOfAccLayerSeven> cu = cbDal.GetCreditVoucherGL();
            return Json(cu, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetGLDetails(int glId)
        {
            ChartOfAccLayerSeven aDetails = cbDal.GetGLDetails(glId);
            return Json(aDetails, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadCostCenterByGlId(int CALayerSevenId)
        {
            List<CostCenter> cu = cbDal.GetCostCenterByGlId(CALayerSevenId);
            return Json(cu, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadReferenceTypeByGlId(int CALayerSevenId)
        {
            List<ReferanceTypeDetails> cu = cbDal.GetReferenceTypeByGlId(CALayerSevenId);
            return Json(cu, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadRefDetailsByRefTypeId(int RefTypeId)
        {
            List<ReferanceTypeDetails> cu = cbDal.GetRefDetailsByRefTypeId(RefTypeId);
            return Json(cu, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetBankBranch(int CALayerSevenId)
        {
            BankAccount aDetails = cbDal.GetBankBranch(CALayerSevenId);
            return Json(aDetails, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveVoucher(VoucherMaster aMaster)
        {

            ResultResponse aResponse = cbDal.SaveVoucher(aMaster, Session[SessionCollection.UserName].ToString());
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Index(DateTime? fromdate, DateTime? todate, int CompanyId = 0, int BUId = 0)
        {
            List<VoucherMasterView> aList = cbDal.GetCreditVoucherMaster(fromdate, todate, CompanyId, BUId);
            return View(aList);
        }
        public ActionResult CreditVoucherListData(DateTime? fromdate, DateTime? todate, int CompanyId = 0, int BUId = 0)
        {
            List<VoucherMasterView> aList = cbDal.GetCreditVoucherMaster(fromdate, todate, CompanyId, BUId);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetVoucherByMasterId(int id)
        {
            VoucherMaster aMaster = cbDal.GetVoucherMasterById(id);
            aMaster.VoucherDetails = cbDal.GetVoucherDetailsById(id);
            return Json(aMaster, JsonRequestBehavior.AllowGet);
        }
        //----Credit Voucher Approval-------
        public ActionResult CreditVoucherApproval(DateTime? fromdate, DateTime? todate, int CompanyId = 0, int BUId = 0)
        {
            ViewBag.fromdate = fromdate;
            ViewBag.todate = todate;
            List<VoucherMasterView> aList = cbDal.GetCreditVoucherMasterForApproval(fromdate, todate, CompanyId, BUId);
            return View(aList);
        }
        public ActionResult CreditVoucherApprovalListData(DateTime? fromdate, DateTime? todate, int CompanyId = 0, int BUId = 0,int statusType = 100)
        {
            List<VoucherMasterView> aList = cbDal.GetCreditVoucherMasterForApproval(fromdate, todate, CompanyId, BUId, statusType);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }
    }
}