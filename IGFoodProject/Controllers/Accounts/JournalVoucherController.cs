﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL.Accounts;
using IGFoodProject.Models;
using IGFoodProject.Models.Accounts;
using IGFoodProject.Utility;

namespace IGFoodProject.Controllers.Accounts
{
    public class JournalVoucherController : Controller
    {
        JournalVoucherDal dbDal = new JournalVoucherDal();
        // GET: JournalVoucher
        public ActionResult Index(DateTime? fromdate, DateTime? todate, int CompanyId = 0, int BUId = 0)
        {
            List<VoucherMasterView> aList = dbDal.JournalList(fromdate, todate, CompanyId, BUId);
            return View(aList);
        }

        public ActionResult NewJournal(DateTime? fromdate, DateTime? todate, int VoucherId = 0, string eflag = "ed")
        {
            ViewBag.VoucherId = VoucherId;
            ViewBag.eflag = eflag;
            ViewBag.fromdate = fromdate;
            ViewBag.todate = todate;
            return View();
        }
        public ActionResult AddRowForJournal(int tr)
        {
            TempData["trSl"] = tr;

            return PartialView("_addNewJVGLRow");
        }
        public ActionResult AddRowForJournalCr(int tr)
        {
            TempData["trSl"] = tr;

            return PartialView("_addNewJVGLRowCr");
        }
        public ActionResult TrailBalance()
        {

            return View();
        }
        public ActionResult SaveJournal(VoucherMaster entity)
        {
            try
            {
                string entryBy = Session[SessionCollection.UserName].ToString();
                ResultResponse aResponse = dbDal.SaveJournal(entity, entryBy);
                return Json(aResponse, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public ActionResult JournalListData(DateTime? fromdate, DateTime? todate, int CompanyId = 0, int BUId = 0)
        {
            List<VoucherMasterView> aList = dbDal.JournalList(fromdate, todate, CompanyId, BUId);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GLInquiry(DateTime? fromdate, DateTime? todate, int glId = 0, int refdetailid = 0)
        {
            //  List<VoucherMasterView> aList = dbDal.JournalList(fromdate, todate, CompanyId, BUId);
            return View();
        }

        public ActionResult LoadReferenceTypeByGlId(int glId)
        {
            List<ReferanceTypeDetails> cu = dbDal.GetReferenceTypeByGlId(glId);
            return Json(cu, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetRefDetailsDuDate(int GlId, int RefDetailsId)
        {
            OpeningBalance cu = dbDal.GetRefDetailsDuDate(GlId, RefDetailsId);
            return Json(cu, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadDataGLInquiry(DateTime? fromdate, DateTime? todate, int glId = 0, string refdetailid = null)
        {
            List<GLInquiry> cu = dbDal.LoadDataGLInquiry(fromdate, todate, glId, refdetailid);
            return Json(cu, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetClosingBalanceOfGL( int glId = 0, string refdetailid = null)
        {
           GLInquiry cu = dbDal.GetClosingBalanceOfGL( glId, refdetailid);
            return Json(cu, JsonRequestBehavior.AllowGet);
        }

        //------------Journal Approval-----------
        public ActionResult JournalVoucherApproval(DateTime? fromdate, DateTime? todate, int CompanyId = 0, int BUId = 0)
        {
            ViewBag.fromdate = fromdate;
            ViewBag.todate = todate;
            List<VoucherMasterView> aList = dbDal.JournalListForApproval(fromdate, todate, CompanyId, BUId);
            return View(aList);
        }
        public ActionResult JournalVoucherApprovalListData(DateTime? fromdate, DateTime? todate, int CompanyId = 0, int BUId = 0, int statusType = 100)
        {
            List<VoucherMasterView> aList = dbDal.JournalListForApproval(fromdate, todate, CompanyId, BUId, statusType);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }
        public ActionResult CustomerGLInquiry(DateTime? fromdate, DateTime? todate, int glId = 0, int refdetailid = 0)
        {
            //  List<VoucherMasterView> aList = dbDal.JournalList(fromdate, todate, CompanyId, BUId);
            return View();
        }


        public ActionResult GetFiscalyearByVoucherDate(DateTime voucherDate) 
        {
            VoucherMaster vim = dbDal.GetFiscalyearByVoucherDate(voucherDate);
            return Json(vim, JsonRequestBehavior.AllowGet);
        }

    }
}