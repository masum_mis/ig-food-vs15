﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.Models.Accounts;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using IGAccounts.Repository;
using IGAccounts.Models;

namespace IGFoodProject.Controllers.Accounts
{
    public class ReferanceTypesController : Controller
    {
        private CodeDbSetAccounts db = new CodeDbSetAccounts();
        private CodeDbSetHR dbHr = new CodeDbSetHR();

        // GET: ReferanceTypes
        public ActionResult Index()
        {
            var company = dbHr.Company.ToList();
            List<CompanyInformation> coms = company;
            
            var r = db.ReferanceTypes.ToList();
            var ab = (from c in r
                      join com in coms on c.CompanyId equals com.CompanyId
                      join b in db.BusinessUnits on c.BUId equals b.BUId
                      select new ReferanceType()
                      {
                          ReferanceTypeId = c.ReferanceTypeId,
                          ReferanceTypeName = c.ReferanceTypeName,
                          IsActive = c.IsActive,
                          ActiveDate = c.ActiveDate,
                          InactiveDate = c.InactiveDate,
                          Description = c.Description,
                          CompanyName = com.CompanyName,
                          BusinessUnitName=b.Name

                      }

                );
            //var costCenterTypes = db.CostCenterTypes.Include(c=> c.Company);
            return View(ab);
        }

        // GET: ReferanceTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReferanceType referanceType = db.ReferanceTypes.Find(id);
            if (referanceType == null)
            {
                return HttpNotFound();
            }
            return View(referanceType);
        }

        // GET: ReferanceTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        public ActionResult SaveReferenceType(ReferanceType entity)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                var list = db.ReferanceTypes.Where(x => x.ReferanceTypeName == entity.ReferanceTypeName).ToList();
                if (list.Any() && entity.ReferanceTypeId == 0)
                {
                    aResponse.msg = "duplicate Entry Not Possible";
                    return Json(aResponse);
                }
                else
                {

                    entity.EntryBy = Session[SessionCollection.UserName].ToString();
                    entity.EntryDate = DateTime.Now;
                    GenericRepository<ReferanceType> saveData = new GenericRepository<ReferanceType>(db);
                    aResponse.isSuccess = entity.ReferanceTypeId == 0
                            ? saveData.Insert(entity)
                            : saveData.Update(entity, entity.ReferanceTypeId);
                }
                return Json(aResponse);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ActionResult GetDataFEdit(int id)
        {
            var result = db.ReferanceTypes.Find(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        // POST: ReferanceTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ReferanceTypeId,ReferanceTypeName,Description,IsActive,ActiveDate,InactiveDate,CompanyId,CreateBy,CreateDate,UpdateBy,UpdateDate,BUId")] ReferanceType referanceType)
        {
            if (ModelState.IsValid)
            {
                db.ReferanceTypes.Add(referanceType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(referanceType);
        }

        // GET: ReferanceTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ////CostCenterType costCenterType = db.CostCenterTypes.Find(id);
            //if (costCenterType == null)
            //{
            //    return HttpNotFound();
            //}
            //ViewBag.CompanyId = new SelectList(db.Companies, "CompanyId", "CompanyName", costCenterType.CompanyId);
            ViewBag.id = id;
            return View(/*costCenterType*/);
        }

        // POST: ReferanceTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ReferanceTypeId,ReferanceTypeName,Description,IsActive,ActiveDate,InactiveDate,CompanyId,CreateBy,CreateDate,UpdateBy,UpdateDate,BUId")] ReferanceType referanceType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(referanceType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(referanceType);
        }

        // GET: ReferanceTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReferanceType referanceType = db.ReferanceTypes.Find(id);
            if (referanceType == null)
            {
                return HttpNotFound();
            }
            return View(referanceType);
        }

        // POST: ReferanceTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ReferanceType referanceType = db.ReferanceTypes.Find(id);
            db.ReferanceTypes.Remove(referanceType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
