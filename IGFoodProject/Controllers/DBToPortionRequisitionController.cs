﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using System.Web.Mvc;
using IGFoodProject.ViewModel;
using IGFoodProject.Utility;
using IGFoodProject.Utility;

namespace IGFoodProject.Controllers
{
    public class DBToPortionRequisitionController : Controller
    {
        private DBToPortionRequisitionDAL _dal = new DBToPortionRequisitionDAL();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddDBRequisition(int id = 0)

        {
            string loginUserEmpId = Session["loginuserempid"] as string;

            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                ViewBag.reqid = id;
                return View();
            }
        }
        public JsonResult LoadCompany()
        {
            List<CompanyInformation> alist = _dal.CompanyInformation();
            return Json(alist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LoadLocation(int comid)
        {
            List<Location> alist = _dal.LocationInformation(comid);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LoadWareHouse(int locid, int wareHouseTypeId)
        {
            int loginId = Convert.ToInt32(Session[SessionCollection.LoginUserId]);
            List<WareHouse> alist = _dal.WareHouseInformation(loginId,locid, wareHouseTypeId);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LoadRequisitionDetailByID(int wareHouse)
        {
            List<ViewDBRequsitionDetail> alist = _dal.GetRequisitionDetailByID(wareHouse);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }
        public dynamic GetAddRow(int tr)
        {
            TempData["trSl"] = tr;
            return PartialView("_partialTargetPortion");
        }
        public JsonResult LoadProduct(int wareHouseId)
        {
            List<ViewProductDescription> alist = _dal.LoadProduct(wareHouseId);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveRequisition(tbl_DBToPortionRequisitionMaster aorder)
        {
            ResultResponse aResponse = new ResultResponse();


            string entryBy = Session[SessionCollection.UserName].ToString();
            aorder.CreateBy = entryBy;
            aorder.UpdateBy = entryBy;
            aorder.ApproveBy = entryBy;
            aResponse.isSuccess = _dal.SaveRequisition(aorder);
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadStockForProtionIssue(int productId, int fromWareHouseId, DateTime reqDate)
        {
            tbl_StockInDetail alist = _dal.LoadProductStockForProtionIssue(productId, fromWareHouseId, reqDate);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DbToPortionReqReport()
        {
            return View();
        }

        public ActionResult DbToPortionReqList(tbl_DBToPortionRequisitionMaster aMaster)
        {
            DataTable dt = _dal.GetDbToPorReqList(aMaster);

            return PartialView("_PartialProtionRequisitionReqort", dt);
        }
    }
}