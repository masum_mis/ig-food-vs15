﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.Utility;
using IGFoodProject.DAL;
using IGFoodProject.Models;

namespace IGFoodProject.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        public ActionResult ProductList()
        {
            ProductDAL aDal = new ProductDAL();
            List<Product> alist = aDal.GetProductList();
            return View(alist);
        }
        public ActionResult AddProduct(int id = 0)
        {
            ViewBag.productid = id;
            return View();
        }
        public JsonResult CheckProduct(string group)
        {

            ProductDAL aDAL = new ProductDAL();

            int desig = aDAL.CheckProduct(group);

            return Json(desig,JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveProduct(Product aGroup)
        {
            ResultResponse aResponse = new ResultResponse();
            ProductDAL aDAL = new ProductDAL();

            string entryBy = Session[SessionCollection.UserName].ToString();
            aGroup.CreateBy = entryBy;
            aGroup.UpdateBy = entryBy;
            aResponse.pk = aDAL.SaveProduct(aGroup);

            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetProductByID(int id)
        {
            ProductDAL aDal = new ProductDAL();
            return Json(aDal.GetProductForEdit(id), JsonRequestBehavior.AllowGet);

        }
        public JsonResult DeleteBrand(int id)
        {
            ResultResponse aResponse = new ResultResponse();
            ProductBrandDAL aDAL = new ProductBrandDAL();


            aResponse.isSuccess = aDAL.DeleteBrand(id);

            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CategoryListByTypeID(int typeid=0)
        {
            ProductDAL aDal = new ProductDAL();
            List<tbl_ProductCategory> alist = aDal.GetCategoryListByType(typeid);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBrand()
        {
            ProductDAL aDal = new ProductDAL();
            List<tbl_ProductBrand> alist = aDal.GetBrandList();
            return Json(alist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPackSize()
        {
            ProductDAL aDal = new ProductDAL();
            List<tbl_ProductPackSize> alist = aDal.GetPackSizeList();
            return Json(alist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetUOM()
        {
            ProductDAL aDal = new ProductDAL();
            List<tbl_UOM> alist = aDal.GetUOMList();
            return Json(alist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetProductNo(int proid)
        {
            ProductDAL aDal = new ProductDAL();
            return Json(aDal.GetProductNo(proid), JsonRequestBehavior.AllowGet);

        }

        public JsonResult CheckProductSerial(int pserial, int? productId)
        {

            ProductDAL aDAL = new ProductDAL();

            int desig = aDAL.CheckProductSerial(pserial, productId);

            return Json(desig, JsonRequestBehavior.AllowGet);
        }

        public JsonResult MaxSerialId()
        {
            CodeDbSet aSet= new CodeDbSet();
            var max = aSet.Product.Max(x => x.ProductSerialId)+1;
            return Json(max, JsonRequestBehavior.AllowGet);
        }
    }
}