﻿using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IGFoodProject.Controllers
{
    public class ItemController : Controller
    {
        ItemDAL aDal = new ItemDAL();
        // GET: Item
        public ActionResult AddItem(int id = 0) 
        {
            ViewBag.ItemId = id;
            return View();
        }
        public ActionResult ItemList()
        {
            List<Item> alist = aDal.GetItemList();
            return View(alist);
        }
        public JsonResult LoadType(int groupid = 0)
        {
            return Json(aDal.GetTypeList(groupid), JsonRequestBehavior.AllowGet);
        }
        public JsonResult CategoryListByTypeID(int typeid = 0)
        {
            List<ItemCategory> alist = aDal.GetCategoryListByType(typeid);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetItemColor()
        {
            List<ItemColor> alist = aDal.GetItemColor();
            return Json(alist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckItem(string group) 
        {          

            int desig = aDal.CheckItem(group);

            return Json(desig, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveItem(Item aGroup) 
        {
            ResultResponse aResponse = new ResultResponse();
            string entryBy = Session[SessionCollection.UserName].ToString();
            aGroup.CreateBy = entryBy;
            aGroup.UpdateBy = entryBy;
            aResponse.isSuccess = aDal.SaveItem(aGroup);
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetItemByID(int id) 
        {
            return Json(aDal.GetItemByIDForEdit(id), JsonRequestBehavior.AllowGet); 

        }

        public JsonResult DeleteItem(int id)
        {
            ResultResponse aResponse = new ResultResponse();

            aResponse.isSuccess = aDal.DeleteItem(id);

            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

    }
}