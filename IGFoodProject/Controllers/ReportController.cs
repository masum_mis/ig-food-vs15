﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL.ReportDAL;
using IGFoodProject.Models;
using IGFoodProject.Models.ReportModel;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;
using IGFoodProject.DAL;
using System.Data;
using IGFoodProject.Models.Accounts;
using Newtonsoft.Json;

namespace IGFoodProject.Controllers
{
    public class ReportController : Controller
    {
        // GET: Report
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SalesOrderReport()
        {
            return View();
        }

        public ActionResult SalesOrderReports(SalesOrderReport aSalesOrderReport)
        {
            ResultResponse aResponse = new ResultResponse();
            BasicInfoReportsParameter aBasicInfoReportsParameter = new BasicInfoReportsParameter();
            aBasicInfoReportsParameter.ReportValue = aSalesOrderReport.ReportValue;
            Session[SessionCollection.ReportValue] = aBasicInfoReportsParameter;
            Session[SessionCollection.ReportParam] = aSalesOrderReport;
            aResponse.isSuccess = true;
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PurchaseOrderReport(PurchaseOrderReportParameter aSalesOrderReport)
        {
            ResultResponse aResponse = new ResultResponse();
            BasicInfoReportsParameter aBasicInfoReportsParameter = new BasicInfoReportsParameter();
            aBasicInfoReportsParameter.ReportValue = aSalesOrderReport.ReportValue;
            Session[SessionCollection.ReportValue] = aBasicInfoReportsParameter;
            Session[SessionCollection.ReportParam] = aSalesOrderReport;
            aResponse.isSuccess = true;
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllOrderNo(DateTime fromDate, DateTime todate)
        {
            DAL.ReportDAL.SalesOrderReportsDAL _aDal = new SalesOrderReportsDAL();

            List<SalesOrder> salesOrderList = _aDal.GetDBPortionReqNO(fromDate, todate);
            return Json(salesOrderList, JsonRequestBehavior.AllowGet);

        }
        public ActionResult TrailBalancePrintReports(TrailBalanceReport aSalesOrderReport)
        {
            ResultResponse aResponse = new ResultResponse();
            BasicInfoReportsParameter aBasicInfoReportsParameter = new BasicInfoReportsParameter();
            aBasicInfoReportsParameter.ReportValue = aSalesOrderReport.ReportValue;
            Session[SessionCollection.ReportValue] = aBasicInfoReportsParameter;
            Session[SessionCollection.ReportParam] = aSalesOrderReport;
            aResponse.isSuccess = true;
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
        public ActionResult StockReport()
        {
            return View();
        }


       
        public ActionResult reportValueInsert(SalesOrderReport aSalesOrderReport)
        {
            ResultResponse aResponse = new ResultResponse();
            BasicInfoReportsParameter aBasicInfoReportsParameter = new BasicInfoReportsParameter();
            aBasicInfoReportsParameter.ReportValue = aSalesOrderReport.ReportValue;
            Session[SessionCollection.ReportValue] = aBasicInfoReportsParameter;
            Session[SessionCollection.ReportParam] = aSalesOrderReport;
            aResponse.isSuccess = true;
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ChallanPrint()
        {
            // TaxConfig aTaxConfig = _utilityDal.GetTaxInfo(productGroupId, amount);
            // return Json(aTaxConfig, JsonRequestBehavior.AllowGet);
            return View();
        }

        public ActionResult ChallanListForPrint(DateTime fromDate , DateTime toDate , int? customerType, int? customer, int? location,int? warehouse)
        {
           ChallanProcessedDAL _challanDAl = new ChallanProcessedDAL();
           List<ViewGeneratedChallan> aList = _challanDAl.GetGeneratedChallanPrintList(fromDate , toDate , customerType , customer , location , warehouse);

            return PartialView("_challanListForPrint", aList);
        }
        public ActionResult TradeChallanPrint()
        {
            // TaxConfig aTaxConfig = _utilityDal.GetTaxInfo(productGroupId, amount);
            // return Json(aTaxConfig, JsonRequestBehavior.AllowGet);
            return View();
        }
        public ActionResult TradeChallanListForPrint(DateTime fromDate, DateTime toDate, int? customerType, int? customer, int? location, int? warehouse)
        {
            TradeSalesChallanDAL _challanDAl = new TradeSalesChallanDAL();
            List<ViewGeneratedChallan> aList = _challanDAl.GetGeneratedChallanPrintList(fromDate, toDate, customerType, customer, location, warehouse);

            return PartialView("_TradeChallanListForPrint", aList);
        }
        public ActionResult LiveBirdStockReport()
        {
            return View();
        }

        public ActionResult GetLiveBirdStockInLedger(DateTime fromDate, DateTime toDate, int wareHouseId)
        {
            StockReportDAL reportDAL = new StockReportDAL();
            DataTable dt = reportDAL.GetLiveBirdStockInLedger(fromDate, toDate, wareHouseId);

            return PartialView("_liveBirdStockReport", dt);
        }

        public ActionResult GetLiveBirdMonthlyStockInLedger(String reportMonth, int wareHouseId)
        {
            // DateTime fromDate = "2019-12-11";
            // DateTime toDate = "2019-12-11";
            //  int wareHouseId = 1;
            DateTime date = Convert.ToDateTime(reportMonth).Date;
            var fromDate = new DateTime(date.Year, date.Month, 1);
            var toDate = fromDate.AddMonths(1).AddDays(-1);
            StockReportDAL reportDAL = new StockReportDAL();
            DataTable dt = reportDAL.GetLiveBirdStockInLedger(fromDate, toDate, wareHouseId);

            return PartialView("_liveBirdStockReport", dt);
            //return null;
        }

        public ActionResult LiveBirdDressInvoiceReport()
        {
          
            return View();
        }


        public ActionResult LiveBirdDressReports(SalesOrderReport aSalesOrderReport)
        {
            ResultResponse aResponse = new ResultResponse();
            BasicInfoReportsParameter aBasicInfoReportsParameter = new BasicInfoReportsParameter();
            aBasicInfoReportsParameter.ReportValue = aSalesOrderReport.ReportValue;
            Session[SessionCollection.ReportValue] = aBasicInfoReportsParameter;
            Session[SessionCollection.ReportParam] = aSalesOrderReport;
            aResponse.isSuccess = true;
            return Json(aResponse, JsonRequestBehavior.AllowGet);


        }

        public ActionResult PickingRequestReports(SalesOrderReport aSalesOrderReport)
        {
            ResultResponse aResponse = new ResultResponse();
            BasicInfoReportsParameter aBasicInfoReportsParameter = new BasicInfoReportsParameter();
            aBasicInfoReportsParameter.ReportValue = aSalesOrderReport.ReportValue;
            Session[SessionCollection.ReportValue] = aBasicInfoReportsParameter;
            Session[SessionCollection.ReportParam] = aSalesOrderReport;
            aResponse.isSuccess = true;
            return Json(aResponse, JsonRequestBehavior.AllowGet);


        }

        public ActionResult ProductMovement()
        {
            
            return View();
        }

        public ActionResult GetProductMovement(DateTime fromDate, DateTime toDate, int wareHouseId, int productId)
        {
            StockReportDAL _aDal= new StockReportDAL();
            DataTable a = _aDal.GetProductMovement(fromDate,toDate,wareHouseId,productId);
            return PartialView("_PartialProductMovement", a);
        }

        public ActionResult ProductWiseStockVsChallan()
        {
          

            return View();
        }

        public ActionResult GetProductWiseStockVsChallan(DateTime fromDate, DateTime toDate, int wareHouseId, int productId)
        {
            StockReportDAL reportDAL = new StockReportDAL();
            DataTable dt = reportDAL.GetProductWiseStockVsChallan(fromDate, toDate, wareHouseId,productId);

            return PartialView("_productWiseStockVsChallan", dt);
        }

        public ActionResult ReceiveReport()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult GetProductReceive(int companyId, int locationId,  DateTime fromDate, DateTime todDate, string fromTime, string toTime, int warehouseId=0)
        {
            StockReportDAL reportDAL = new StockReportDAL();
            DataTable dt = reportDAL.GetProductReceived(companyId, locationId, fromDate, todDate, fromTime, toTime, warehouseId);

            return PartialView("_PartialReceive", dt);
        }
        public ActionResult ProductIsssueReport()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }

        public ActionResult GetProductIssue( DateTime fromDate, DateTime todDate, string fromTime, string toTime, int warehouseId=0)
        {
            StockReportDAL reportDAL = new StockReportDAL();
            DataTable dt = reportDAL.GetProductIsssue(warehouseId, fromDate, todDate, fromTime, toTime);

            return PartialView("_PartialProductIssue", dt);
        }
        public ActionResult UndeliverReport()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult GetUndeliverProduct(string soDate = "", string deliveryDate = "", int warehouseId = 0, int customerId = 0)
        {
            StockReportDAL reportDAL = new StockReportDAL();
            DataTable dt = reportDAL.GetUndeliverProduct(soDate, deliveryDate, warehouseId, customerId);

            return PartialView("_PartialUndeliverReport", dt);
        }

        public ActionResult AgingReport()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult GetAgingReport(int companyId, int locationId, DateTime fromDate, int groupid,  int warehouseId = 0)
        {
            StockReportDAL reportDAL = new StockReportDAL();
            DataTable dt = reportDAL.GetAgingReport( companyId,  locationId,  fromDate,  groupid,  warehouseId = 0);

            return PartialView("_agingreport", dt);
        }


        public ActionResult CustomerProductGroupReport()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult GetCustomerProductSummary(DateTime fromDate, DateTime toDate, int groupid, int customertype, int customer, int reportType)
        {
            StockReportDAL reportDAL = new StockReportDAL();
            //List<OwnBreederHistoricalSummary> aObj = aDal.GetBreederOwnHistoricalSummary(breeder, house, flock);
            DataTable aObj = reportDAL.GetCustomerProductSummary( fromDate,  toDate,  groupid,  customertype,  customer,  reportType);
            string JSONresult;
            JSONresult = JsonConvert.SerializeObject(aObj);


            return Json(JSONresult, JsonRequestBehavior.AllowGet);

        }
        public ActionResult VehicleWiseChallan()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult GetVehicleWiseChallan(DateTime fromDate, DateTime toDate, int? vehicleId)
        {
            ChallanProcessedDAL reportDAL= new ChallanProcessedDAL();
            DataTable dt = reportDAL.GetVehicleWiseChallan(fromDate, toDate, vehicleId);

            return PartialView("_PartialVehicleWiseChallan", dt);
        }
        public ActionResult VehicleWiseVatChallan()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult GetVehicleWiseVatChallan(DateTime fromDate, DateTime toDate, int? vehicleId)
        {
            ChallanProcessedDAL reportDAL = new ChallanProcessedDAL();
            List<ViewVatChallan> dt = reportDAL.GetVehicleWiseVatChallan(fromDate, toDate, vehicleId);

            return PartialView("_PartialVehicleWiseVatChallan", dt);
        }
        public ActionResult VehicleWisePickingSummary()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult GetVehicleWisePickingSummary(DateTime fromDate, DateTime toDate, int? vehicleId)
        {
            ChallanProcessedDAL reportDAL = new ChallanProcessedDAL();
            List<ViewVatChallan> dt = reportDAL.GetVehicleWisePickingSummary(fromDate, toDate, vehicleId);

            return PartialView("_PartialVehicleWisePickingSummary", dt);
        }
        public ActionResult ProductIsssueReceiveReport()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }

        public ActionResult GetProductIssueReceive(DateTime fromDate, DateTime todDate, int warehouseId = 0,int toWarehouseId = 0,bool isInterSTO=false)
        {
            StockTransferDAL reportDAL = new StockTransferDAL();
            DataTable dt = reportDAL.GetProductIsssueReceive(fromDate, todDate,warehouseId, toWarehouseId, isInterSTO);

            return PartialView("_PartialProductIssueReceive", dt);
        }

        #region accounts

        public ActionResult PrintVoucher()
        {
            return View();
        }
        public ActionResult VoucherPrintReports(VoucherPrintReport aVoucherPrintReport)
        {
            ResultResponse aResponse = new ResultResponse();
            BasicInfoReportsParameter aBasicInfoReportsParameter = new BasicInfoReportsParameter();
            aBasicInfoReportsParameter.ReportValue = aVoucherPrintReport.ReportValue;
            Session[SessionCollection.ReportValue] = aBasicInfoReportsParameter;
            Session[SessionCollection.ReportParam] = aVoucherPrintReport;
            aResponse.isSuccess = true;
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult OwnConsumptionChallanPrint(SalesOrderReport aVoucherPrintReport)
        {
            ResultResponse aResponse = new ResultResponse();
            BasicInfoReportsParameter aBasicInfoReportsParameter = new BasicInfoReportsParameter();
            aBasicInfoReportsParameter.ReportValue = aVoucherPrintReport.ReportValue;
            Session[SessionCollection.ReportValue] = aBasicInfoReportsParameter;
            Session[SessionCollection.ReportParam] = aVoucherPrintReport;
            aResponse.isSuccess = true;
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PurchaseOrderOtherReports(PurchaseOrderOtherReport poReport)
        {
            ResultResponse aResponse = new ResultResponse();
            BasicInfoReportsParameter aBasicInfoReportsParameter = new BasicInfoReportsParameter();
            aBasicInfoReportsParameter.ReportValue = poReport.ReportValue;
            Session[SessionCollection.ReportValue] = aBasicInfoReportsParameter;
            Session[SessionCollection.ReportParam] = poReport;
            aResponse.isSuccess = true;
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MrrPrintReports(ViewOthersMrr aOthersMrr)
        {
            ResultResponse aResponse = new ResultResponse();
            BasicInfoReportsParameter aBasicInfoReportsParameter = new BasicInfoReportsParameter();
            aBasicInfoReportsParameter.ReportValue = aOthersMrr.ReportValue;
            Session[SessionCollection.ReportValue] = aBasicInfoReportsParameter;
            Session[SessionCollection.ReportParam] = aOthersMrr;
            aResponse.isSuccess = true;
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        #endregion

      public ActionResult MultipleMRRReport(DirrectMrr aDirrectMrr)
        {
            ResultResponse aResponse = new ResultResponse();
            BasicInfoReportsParameter aBasicInfoReportsParameter = new BasicInfoReportsParameter();
            aBasicInfoReportsParameter.ReportValue = aDirrectMrr.ReportValue;
            Session[SessionCollection.ReportValue] = aBasicInfoReportsParameter;
            Session[SessionCollection.ReportParam] = aDirrectMrr;
            aResponse.isSuccess = true;
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult YearlySalesReport()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult YearlySalesReportDAta( int yearId,int productType)
        {
            SalesOrderReportsDAL reportDAL = new SalesOrderReportsDAL();
           List<ViewYearlySalesReport> aObj = reportDAL.YearlySalesReport(yearId, productType);
           
            //string JSONresult;
            //JSONresult = JsonConvert.SerializeObject(aObj);


            return Json(aObj, JsonRequestBehavior.AllowGet);

        }

        public ActionResult BreakdownOfRevenue()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult GetBreakdownofRevenue(int yearId)
        {
            SalesOrderProcessedDAL reportDAL = new SalesOrderProcessedDAL();
            DataTable dt = reportDAL.GetBreakdownofRevenue(yearId);

            return PartialView("_PartialBreakdownOfRevenue", dt);
        }

    }
}