﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;
using IGFoodProject.Models.ReportModel;
using System.Data;

namespace IGFoodProject.Controllers
{
    public class RetailSalesController : Controller
    {
        RetailSalesDAL _retailSalesDal = new RetailSalesDAL();
        // GET: RetailSales
        public ActionResult Index(int soId = 0, string eflag = "ed")
        {
            string UserID = string.Empty;
            string IssDss = "";
            string empName = "";
            var sdd = Session[SessionCollection.LoginUserEmpId];
            if (Session[SessionCollection.LoginUserEmpId] != null)
            {

                //UserID = Session[SessionCollection.LoginUserEmpId].ToString() + " : " + Session[SessionCollection.UserEmpName].ToString() + " : " + Session[SessionCollection.Designation].ToString();
            }
            else
            {
                HttpCookie cookie = Request.Cookies["LoginUserIdCookie"];
                if (cookie != null)
                {
                    IssDss = cookie.Values["EmpId"];
                    empName = cookie.Values["EmpName"];
                    return RedirectToAction("LockScreen", "Home", new { EmpId = IssDss, EmpName = empName });
                }
                else
                {
                    return RedirectToAction(nameof(HomeController.Login), "Home");
                }

            }

            ViewBag.SalesOrderId = soId;
            ViewBag.eflag = eflag;
            return View();
        }
        public ActionResult GetCustomerType()
        {
            List<CustomerType> aList = _retailSalesDal.GetCustomerTypeList();
            return Json(aList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetCustomer(int typeid)
        {
            List<ViewCustomerInfo> aList = _retailSalesDal.GetCustomerList(typeid, Convert.ToInt32(Session[SessionCollection.LoginUserId]));
            return Json(aList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetDeliveryWarehouse(int warehouseTypeid,int customerId)
        {
            List<WareHouse> aList = _retailSalesDal.GetWareHousesByType(warehouseTypeid,Convert.ToInt32(Session[SessionCollection.LoginUserId]), customerId);
            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddRowForOrder(int tr)
        {
            TempData["trSl"] = tr;

            return PartialView("_addNewOrderRow");
        }
        public ActionResult GetRetailCustomerDetails(string phoneNo)
        {
            tbl_RetailCustomerInfo retailCustDetails = _retailSalesDal.GetRetailCustomerDetails(phoneNo);
            return Json(retailCustDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCouponDetails(int customerId, string couponText, DateTime orderDate)
        {
            CouponDetails couponDetails = _retailSalesDal.GetCouponDetails(customerId, couponText, orderDate);
            return Json(couponDetails, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetProductDetails(int productId, DateTime orderdate, int customertype, int wareHouseId)
        {
            ViewProductDetailsSales aDetailsSales = _retailSalesDal.GetProductDetails(productId, orderdate, customertype, wareHouseId);
            return Json(aDetailsSales, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveOrderPrecessed(SalesOrderProcessedMaster aMaster)
        {
            ResultResponse aResponse =new ResultResponse();
            if (!string.IsNullOrEmpty(aMaster.CouponCode))
            {
                aResponse = _retailSalesDal.SaveSalesOrderProcessedWithCoupon(aMaster,
                Session[SessionCollection.UserName].ToString());
            }
            else
            {
                 aResponse = _retailSalesDal.SaveSalesOrderProcessed(aMaster,
                Session[SessionCollection.UserName].ToString());
            }
            
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
        public ActionResult RetailSalesList(DateTime? fromdate, DateTime? todate, string salespersonid, int retailCustomerid = 0, int deliveryWarehouseId = 0)
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                List<ViewSalesOrderProcessedMaster> aList = _retailSalesDal.GetSalesOrderProcessedMasterSearch(fromdate, todate, salespersonid, loginUserEmpId, retailCustomerid, deliveryWarehouseId);
                return View(aList);
            }

        }
        public ActionResult RetailSalesList_Search(DateTime? fromdate, DateTime? todate, string salespersonid, int retailCustomerid = 0, int deliveryWarehouseId = 0)
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                List<ViewSalesOrderProcessedMaster> aList = _retailSalesDal.GetSalesOrderProcessedMasterSearch(fromdate, todate, salespersonid, loginUserEmpId, retailCustomerid, deliveryWarehouseId);
                return Json(aList, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult RetailSalesListCouponApprove(DateTime? fromdate, DateTime? todate, string salespersonid, int retailCustomerid = 0, int deliveryWarehouseId = 0)
        {
            //string loginUserEmpId = Session["loginuserempid"] as string;
            //if (string.IsNullOrEmpty(loginUserEmpId))
            //{
            //    return RedirectToAction(nameof(HomeController.Login), "Home");
            //}
            //else
            //{
                List<ViewSalesOrderProcessedMaster> aList = _retailSalesDal.GetSalesOrderProcessedMasterSearch_Coupon(fromdate, todate, salespersonid,  retailCustomerid, deliveryWarehouseId);
                return View(aList);
            //}

        }

        public ActionResult RetailSalesList_Search_Coupon(DateTime? fromdate, DateTime? todate, string salespersonid, int retailCustomerid = 0, int deliveryWarehouseId = 0)
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                List<ViewSalesOrderProcessedMaster> aList = _retailSalesDal.GetSalesOrderProcessedMasterSearch_Coupon(fromdate, todate, salespersonid,  retailCustomerid, deliveryWarehouseId);
                return Json(aList, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult RetailSalesListForVoid(DateTime? fromdate, DateTime? todate, string salespersonid, int retailCustomerid = 0, int deliveryWarehouseId = 0)
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                List<ViewSalesOrderProcessedMaster> aList = _retailSalesDal.GetSalesOrderProcessedMasterSearchVoid(fromdate, todate, salespersonid, retailCustomerid, deliveryWarehouseId);
                return View(aList);
            }

        }
        public ActionResult RetailSalesListForVoid_Search(DateTime? fromdate, DateTime? todate, string salespersonid, int retailCustomerid = 0, int deliveryWarehouseId = 0)
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                List<ViewSalesOrderProcessedMaster> aList = _retailSalesDal.GetSalesOrderProcessedMasterSearchVoid(fromdate, todate, salespersonid, retailCustomerid, deliveryWarehouseId);
                return Json(aList, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SODelete(int soId)
        {
            ResultResponse aResponse = _retailSalesDal.SODelete(soId);
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
        
       public ActionResult SoCancelVoid(int soId)
        {
            ResultResponse aResponse = _retailSalesDal.SoCancelVoid(soId);
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult SOVoid(int soId)
        {
            ResultResponse aResponse = _retailSalesDal.SOVoid(soId);
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CouponApprove(int soId)
        {
            ResultResponse aResponse = _retailSalesDal.CouponApprove(soId);
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetRetailCustomer()
        {
            List<tbl_RetailCustomerInfo> aList = _retailSalesDal.GetRetailCustomerList();
            return Json(aList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ViewOrder(int id)
        {
            return PartialView("_RetailSales");
        }
        public ActionResult GetSalesOrderMaster(int id)
        {
            SalesOrderProcessedMaster aMaster = _retailSalesDal.GetSalesOrderProcessedMaster(id);
            aMaster.SalesDetails = _retailSalesDal.GetSalesOderProcessedDetailsByMaster(id);
            return Json(aMaster, JsonRequestBehavior.AllowGet);
        }
        public ActionResult RetailSalesOrderReports(SalesOrderReport aSalesOrderReport)
        {
            ResultResponse aResponse = new ResultResponse();
            BasicInfoReportsParameter aBasicInfoReportsParameter = new BasicInfoReportsParameter();
            aBasicInfoReportsParameter.ReportValue = aSalesOrderReport.ReportValue;
            Session[SessionCollection.ReportValue] = aBasicInfoReportsParameter;
            Session[SessionCollection.ReportParam] = aSalesOrderReport;
            aResponse.isSuccess = true;
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SalesPersonSalesReportRetail()
        {
            return View();
        }

        public ActionResult SalesOrderWiseSalesPersonSalesReportRetail(DateTime fromDate, DateTime toDate, int reportType, int summary = 0, string salesPerson = "", string customerTypeId = "", int divisionId = 0, int areaId = 0, int territoryId = 0, int marketId = 0,int warehouseId=0)
        {
            DataTable dt = new DataTable();
            if (reportType == 1)
            {
                dt = _retailSalesDal.SalesOrderWiseSalesPersonSalesReport(fromDate, toDate, summary, salesPerson, customerTypeId, divisionId, areaId, territoryId, marketId,warehouseId);
            }
            if (reportType == 2)
            {
                dt = _retailSalesDal.ChallanWiseSalesPersonSalesReport(fromDate, toDate, summary, salesPerson, customerTypeId, divisionId, areaId, territoryId, marketId,warehouseId);
            }
            if (summary == 0)
            {
                return PartialView("_PartialSalesPersonSalesReportRetail", dt);
            }
            //if (summary == 1)
            //{
            //    return PartialView("_PartialSalesPersonSalesSummary", dt);
            //}
            return null;
        }

        public ActionResult SalesPersonSalesDetailsReportRetail()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult SalesOrderWiseSalesPersonSalesDetailsReportRetail(DateTime fromDate, DateTime toDate, int reportType, int summary = 0, string salesPerson = "", string customerTypeId = "", int divisionId = 0, int areaId = 0, int territoryId = 0, int marketId = 0,int warehouseId=0)
        {
            DataTable dt = new DataTable();
            if (reportType == 1)
            {
                dt = _retailSalesDal.SalesOrderWiseSalesPersonSalesDetailsReport(fromDate, toDate, summary, salesPerson, customerTypeId, divisionId, areaId, territoryId, marketId,warehouseId);
            }
            if (reportType == 2)
            {
                dt = _retailSalesDal.ChallanWiseSalesPersonSalesDetailsReport(fromDate, toDate, summary, salesPerson, customerTypeId, divisionId, areaId, territoryId, marketId,warehouseId);
            }
            if (summary == 0)
            {
                return PartialView("_PartialSalesPersonSalesDetailsReportRetail", dt);
            }
            //if (summary == 1)
            //{
            //    return PartialView("_PartialSalesPersonSalesSummary", dt);
            //}
            return null;
        }

        public JsonResult GetCouponType() 
        {
            List<SalesOrderProcessedMaster> alist = _retailSalesDal.GetCouponType();
            return Json(alist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCoupon(int type) 
        {
            return Json(_retailSalesDal.GetCoupon(type), JsonRequestBehavior.AllowGet);
        }


    }
}