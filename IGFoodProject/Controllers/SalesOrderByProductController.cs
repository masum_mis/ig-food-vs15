﻿using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IGFoodProject.Controllers
{
    public class SalesOrderByProductController : Controller
    {
        // GET: SalesOrderByProduct
        CustomerDAL _customerDal = new CustomerDAL();
        UtilityDAL _utilityDal= new UtilityDAL();
        SalesOrderByProductDAL _salesProcess = new SalesOrderByProductDAL();
        public ActionResult Index(int salesOrderProcessMasterId=0, int approveFlag=0)
        {
            string loginUserEmpId = Session["loginuserempid"] as string;

            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                ViewBag.salesOrderProcessMasterId = salesOrderProcessMasterId;
                ViewBag.ApproveFlag = approveFlag;
                return View();
            }
        }
        public ActionResult GetCustomer(int typeid=0)
        {
            List<ViewCustomerInfo> aList = _customerDal.GetCustomerList(typeid);
            return Json(aList, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetCustomerDetails(int id)
        {

            ViewCustomerInfo ci = _customerDal.GetCustomerViewById(id);
            return PartialView("_customerDetailsView", ci);
        }

        public ActionResult GetSalesPhoneNo(int empNo)
        {
            string PhoneNo = _salesProcess.GetSalesPersonPhone(empNo);

            return Json(PhoneNo, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddRowForOrder(int tr)
        {
            TempData["trSl"] = tr;
            return PartialView("_addNewOrderRow");
        }

        public ActionResult GetProductByGroup(int groupId)
        {

            List<Product> aProducts = _utilityDal.GetProductByGroupId(groupId).FindAll(x=>x.TypeId==30 || x.TypeId==39);
            return Json(aProducts, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetProductDetails(int productId)
        {
            ViewProductDetailsSales aDetailsSales = _salesProcess.GetProductDetails(productId);
            return Json(aDetailsSales, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveOrderPrecessed(SalesOrderProcessedMaster aMaster)
        {

            ResultResponse aResponse = _salesProcess.SaveSalesOrderFurtherProcessed(aMaster,
                Session[SessionCollection.UserName].ToString());
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ByProductSalesOrderList()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;

            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                List<ViewSalesOrderProcessedMaster> aList = _salesProcess.GetSalesOrderByProductMaster();

                return View(aList);
            }
        }

        public ActionResult ViewOrder()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;

            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return PartialView("_partialByproductOrder");
            }
        }
        public ActionResult ByProductSalesOrderApproveList()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;

            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                List<ViewSalesOrderProcessedMaster> aList = _salesProcess.GetSalesOrderByProductMaster();

                return View(aList);
            }
        }
        public ActionResult ApproveSalesProcessed(int id)
        {
            string loginUserEmpId = Session["loginuserempid"] as string;

            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {

                ViewBag.SalesOrder = id;
                return View();
            }


        }
        public ActionResult GetSalesOrderMaster(int id)
        {
            SalesOrderProcessedMaster aMaster = _salesProcess.GetSalesOrderProcessedMaster(id);
            aMaster.SalesDetails = _salesProcess.GetSalesOderProcessedDetailsByMaster(id);
            return Json(aMaster, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetLocationIdByWarehouse(int id)
        {
            int location = _salesProcess.GetLocationIdByWareHouse(id);
            return Json(location, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ApproveSalesProcesed(int id, bool status)
        {
            ResultResponse aResponse = _salesProcess.ApproveSalesOrder(id, status,
                Session[SessionCollection.UserName].ToString());
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetProductDetails_edit(int productId, int groupid, int masterid)
        {
            ViewProductDetailsSales aDetailsSales = _salesProcess.GetProductDetails_Edit(productId, groupid, masterid);
            return Json(aDetailsSales, JsonRequestBehavior.AllowGet);
        }



        
    }
}