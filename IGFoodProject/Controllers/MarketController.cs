﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;

namespace IGFoodProject.Controllers
{
    public class MarketController : Controller
    {
        UtilityDAL utilityDal = new UtilityDAL();
        MarketDAL marketDal = new MarketDAL();
        // GET: Market
        public ActionResult Index()
        {
            List<Territory> territories = utilityDal.GetTerritory();
            return View(territories);
        }
        public ActionResult GetMarketByTerritoryId(int territoryId)
        {
            List<Market> markets = marketDal.GetMarketByTerritoryId(territoryId);
            return PartialView("_marketList",markets);
        }
        public ActionResult Insert(int id=0)
        {
            List<Territory> territories = utilityDal.GetTerritory();
            Market aMarket = marketDal.GetMarketByMarketId(id);
            var tuple = new Tuple<IEnumerable<Territory>, Market>(territories, aMarket);
            return View(tuple);
        }
        public ActionResult SaveMarketInfo(Market marketInfo)
        {
            ResultResponse aResponse=new ResultResponse();
            marketInfo.CreateBy = Convert.ToString(Session[SessionCollection.LoginUserEmpId]);
            aResponse.isSuccess = marketDal.SaveMarketInfo(marketInfo);
            return Json(aResponse,JsonRequestBehavior.AllowGet);
        }
    }
}