﻿using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IGFoodProject.Controllers
{
    public class SalesOrderFurtherProcessController : Controller
    {
        // GET: SalesOrderFurtherProcess
        CustomerDAL _customerDal = new CustomerDAL();
        SalesOrderFurtherProcessDAL _salesProcess = new SalesOrderFurtherProcessDAL();
        public ActionResult Index(int soId = 0)
        {
            ViewBag.SalesOrderId = soId;
            return View();
        }
        public ActionResult GetCustomerType()
        {
            List<CustomerType> aList = _customerDal.GetCustomerTypeList();
            return Json(aList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetCustomer(int typeid)
        {
            List<ViewCustomerInfo> aList = _customerDal.GetCustomerList(typeid);
            return Json(aList, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetCustomerDetails(int id)
        {

            ViewCustomerInfo ci = _customerDal.GetCustomerViewById(id);
            return PartialView("_customerDetailsView", ci);
        }

        public ActionResult GetSalesPhoneNo(int empNo)
        {
            string PhoneNo = _salesProcess.GetSalesPersonPhone(empNo);

            return Json(PhoneNo, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddRowForOrder(int tr)
        {
            TempData["trSl"] = tr;
            return PartialView("_addNewOrderRow");
        }

        public ActionResult GetProductDetails(int productId, int groupid)
        {
            ViewProductDetailsSales aDetailsSales = _salesProcess.GetProductDetails(productId, groupid);
            return Json(aDetailsSales, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveOrderPrecessed(SalesOrderProcessedMaster aMaster)
        {

            ResultResponse aResponse = _salesProcess.SaveSalesOrderFurtherProcessed(aMaster,
                Session[SessionCollection.UserName].ToString());
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FurtherSalesOrderList()
        {
            List<ViewSalesOrderProcessedMaster> aList = _salesProcess.GetSalesOrderFurtherProcessedMaster();


            return View(aList);
        }

        //

        public ActionResult ViewOrder()
        {
            return PartialView("_partialApproveSalesProcessed");
        }
        public ActionResult FurtherSalesOrderApproveList()
        {
            List<ViewSalesOrderProcessedMaster> aList = _salesProcess.GetSalesOrderFurtherProcessedMaster();


            return View(aList);
        }
        public ActionResult ApproveSalesProcessed(int id)
        {

            ViewBag.SalesOrder = id;
            return View();


        }
        public ActionResult GetSalesOrderMaster(int id)
        {
            SalesOrderProcessedMaster aMaster = _salesProcess.GetSalesOrderProcessedMaster(id);
            aMaster.SalesDetails = _salesProcess.GetSalesOderProcessedDetailsByMaster(id);
            return Json(aMaster, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetLocationIdByWarehouse(int id)
        {
            int location = _salesProcess.GetLocationIdByWareHouse(id);
            return Json(location, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ApproveSalesProcesed(int id, bool status)
        {
            ResultResponse aResponse = _salesProcess.ApproveSalesOrder(id, status,
                Session[SessionCollection.UserName].ToString());
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
    }
}