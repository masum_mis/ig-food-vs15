﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;

namespace IGFoodProject.Controllers
{
    public class ProductDamageController : Controller
    {
        private  ProductDamageDAL _aDal= new ProductDamageDAL();
        private  ResultResponse aResponse = new ResultResponse();
        // GET: ProductDamage
        public ActionResult Index()
        {
            DataTable dt = _aDal.GetDamageList();
            return View(dt);
        }

        public ActionResult ProductDamageCreate()
        {
            return View();
        }

        public dynamic GetAddRow(int tr)
        {
            TempData["trSl"] = tr;
            return PartialView("_PartialDamageProduct");
        }


        public ActionResult SaveData(ProductDamageMaster aMaster)
        {
            aMaster.CreateDate = DateTime.Now;
            aMaster.CreateBy= Session[SessionCollection.UserName].ToString();
            aResponse = _aDal.SaveProductDamage(aMaster);
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DumpingStockClearing()
        {
            return View();
        }

        public ActionResult GetDumpingStockForClearing(int warehouseId, DateTime fromDate, DateTime toDate)
        {
            DataTable dList = _aDal.GetDumpingStockForClearing(warehouseId, fromDate, toDate);
            return PartialView("_PartialDumpingStockClearing", dList);
        }

        public ActionResult SaveDumping(DumpingMaster aMaster)
        {
            ResultResponse aResponse = _aDal.SaveDumpingProcessed(aMaster,
                Session[SessionCollection.UserName].ToString());

            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DumpingIndex()
        {
            DataTable dmList = _aDal.GetDumpingList();
            return View(dmList);
        }

        public ActionResult ViewDumping(int id)
        {
            DataTable dinfo = _aDal.GetDumpingInfoById(id);
            return View(dinfo);
        }
    }
}