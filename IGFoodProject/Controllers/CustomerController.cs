﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.Utility;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.ViewModel;

namespace IGFoodProject.Controllers
{
    public class CustomerController : Controller
    {
        // GET: Customer

       private CustomerDAL aDAL = new CustomerDAL();
        public ActionResult CustomerList()
        {
            return View();
        }

        public ActionResult GetCustomerByType(int typeid = 0)
        {
            List<ViewCustomerInfo> alist = aDAL.GetCustomerList(typeid);
            return PartialView("_customerList" , alist);
        }

        public ActionResult CheckCustomer(int id , string name)
        {

            List<ViewCustomerInfo> alist = aDAL.GetCustomerList(0).Where(a=>a.CustomerName== name && a.CustomerID!=id).ToList();
            return Json(alist , JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddCustomer(int id = 0)
        {
             
            ViewBag.customerid = id;
            if (id == 0)
            {
                tbl_CustomerInformation customerInfo = new tbl_CustomerInformation();
                return View(customerInfo);
            }
            else
            {
                tbl_CustomerInformation customerInfo1 = aDAL.GetCustomerById(id);
                return View(customerInfo1);
            }

        }

        [HttpPost]
        public ActionResult SaveCustomer(tbl_CustomerInformation acustomer)
        {
            ResultResponse aResponse = new ResultResponse();
             
            string entryBy = Session[SessionCollection.UserName].ToString();
            acustomer.CreateBy = entryBy;
            acustomer.UpdateBy = entryBy;
            aResponse = aDAL.SaveCustomer(acustomer);
            return Json(aResponse , JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetProductByID(int id)
        {
            ProductDAL aDal = new ProductDAL();
            return Json(aDal.GetProductForEdit(id));

        }
        public ActionResult GetCustomerType()
        {
            ProductDAL aDal = new ProductDAL();
            List<CustomerType> aList = aDal.GetCustomerType();
            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public async Task<dynamic> DeleteCustomer(int id)
        {
            ResultResponse aResponse = new ResultResponse();
             
            aResponse.isSuccess = aDAL.DeleteCustomer(id);
            return Json(aResponse);
        }

        public ActionResult LoadOtherCustomer()
        {
            List<tbl_CustomerInformation> aList = aDAL.GetDealer();

            return PartialView("_dealerView", aList);
        }

        public ActionResult GetCustomerInfo( int customerId )
        {
            tbl_CustomerInformation aInformation = aDAL.GetCustomerById(customerId);
            return Json(aInformation, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetFilteredCustomerList(int divisionId, int areaId, int territoryId,int customerTypeId)
        {
            List<tbl_CustomerInformation> customerInfoList = aDAL.GetFilteredCustomerList( divisionId,  areaId,  territoryId, customerTypeId);
            return Json(customerInfoList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCustomerNameEmpId(int empNo)
        {
            ViewCustomerInfo CustomerInfo = aDAL.GetCustomerNameEmpId(empNo);

            return Json(CustomerInfo, JsonRequestBehavior.AllowGet);
        }




    }

}