﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.Utility;
using IGFoodProject.DAL;
using IGFoodProject.Models;

namespace IGFoodProject.Controllers
{
    public class HomeController : Controller
    {
        DashboardDAL aDashBoardDal = new DashboardDAL();
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Logout()
        {

            if (ViewData["LoginMsg"] == null)
            {
                ViewData["LoginMsg"] = "Please Input Correct User Name Password!!";
            }
            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();
            return View("Login");
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        public ActionResult Login()
        {
            //if (Request.Cookies["LoginUserIdCookie"] != null)
            //{

            //    var c = new HttpCookie("LoginUserIdCookie");
            //    c.Expires = DateTime.Now.AddDays(-1);
            //    Response.Cookies.Add(c);
            //}

            //HttpContext.Session.Clear();

            return View("Login");


        }

        public ActionResult LoginAction(LoginUser login)
        {
            if (CheckLogin(login.userName, login.password))
            {
                ViewData["LoginMsg"] = "Home";
                //return RedirectToAction("Index", "DashBoard");
                return RedirectToAction("Index", "DashBoard");
            }
            else
            {
                //TempData["msg"] = "Please Enter Valid Credentials";
                ViewData["LoginMsg"] = "Invalid Credentials";

                return RedirectToAction("Login", "Home");

            }
        }
        public ActionResult LockScreen(string EmpId, string EmpName)
        {
            ViewBag.EmpName = EmpName;
            ViewBag.EmpId = EmpId;
            return View("LockScreen");
        }
        public bool CheckLogin(string userName, string pass)
        {
            try
            {
                HomeDAL aHomeDal = new HomeDAL();

                DataTable dt = aHomeDal.CheckLoginDAL(userName, pass);

                if (dt.Rows.Count > 0)
                {
                    Session[SessionCollection.UserName] = dt.Rows[0]["LoginUserName"].ToString();
                    Session[SessionCollection.LoginUserId] = dt.Rows[0]["LoginUserId"].ToString();
                    Session[SessionCollection.LoginUserType] = dt.Rows[0]["LoginUserType"].ToString();
                    Session[SessionCollection.LoginUserEmpId] = dt.Rows[0]["LoginUserEmpId"].ToString();
                    Session[SessionCollection.ModuleId] = "13";
                    Session["MenuList"] = aDashBoardDal.GetMenuStepOne(Session[SessionCollection.LoginUserId].ToString());

                    UtilityDAL aDal = new UtilityDAL();
                    string userInfo = aDal.GetUserInfoStrig(dt.Rows[0]["LoginUserName"].ToString());
                    Session[SessionCollection.UserInfo] = userInfo;

                    HttpCookie cookie = Request.Cookies["LoginUserIdCookie"];
                    if (cookie != null)
                    {
                        //cookie.Value = dt.Rows[0]["LoginUserName"].ToString();
                        cookie.Values["EmpId"] = dt.Rows[0]["LoginUserName"].ToString();
                        cookie.Values["EmpName"] = dt.Rows[0]["EmpName"].ToString();
                        Response.SetCookie(cookie);

                    }
                    else
                    {
                        cookie = new HttpCookie("LoginUserIdCookie");

                        cookie.Values["EmpId"] = dt.Rows[0]["LoginUserName"].ToString();
                        cookie.Values["EmpName"] = dt.Rows[0]["EmpName"].ToString();
                        this.ControllerContext.HttpContext.Response.Cookies.Add(cookie);
                    }

                    return true;
                }
                else
                {
                    Session.Clear();
                    Session.Abandon();
                    Session.RemoveAll();
                    return false;
                }
            }
            catch (Exception ex)
            {
                string exception = ex.ToString();
                return false;
            }
            finally
            {

            }
        }

    }
}