﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.ViewModel;

namespace IGFoodProject.Controllers
{
    public class ProcessedSalePriceConfigsController : Controller
    {
        private CodeDbSet db = new CodeDbSet();
        ProductDAL aDal = new ProductDAL();
        // GET: ProcessedSalePriceConfigs
        public ActionResult Index()
        {
            
            return View(db.ProcessedSalePriceConfig.Include(p=>p.Product).ToList());
        }

        // GET: ProcessedSalePriceConfigs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProcessedSalePriceConfig processedSalePriceConfig = db.ProcessedSalePriceConfig.Find(id);
            if (processedSalePriceConfig == null)
            {
                return HttpNotFound();
            }
            return View(processedSalePriceConfig);
        }

        // GET: ProcessedSalePriceConfigs/Create
        public ActionResult Create()
        {
            List<Product> products = aDal.GetProductList(5);
            List<Product> a =products.Where(x => x.IsActive == true).ToList();
            int aa = a.Count;
            ViewBag.ProductId = new SelectList(products.Where(x => x.IsActive == true).ToList(), "ProductId", "ProductName");
            return View();
        }

        // POST: ProcessedSalePriceConfigs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProcessedSalePriceConfigId,ProductId,PurchasePrice,SalePrice,IsActive,FromDate,ToDate")] ProcessedSalePriceConfig processedSalePriceConfig)
        {
            if (ModelState.IsValid)
            {
                db.ProcessedSalePriceConfig.Add(processedSalePriceConfig);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(processedSalePriceConfig);
        }

        // GET: ProcessedSalePriceConfigs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProcessedSalePriceConfig processedSalePriceConfig = db.ProcessedSalePriceConfig.Find(id);
            if (processedSalePriceConfig == null)
            {
                return HttpNotFound();
            }
            ProductDAL aDal = new ProductDAL();
            List<Product> products = aDal.GetProductList(5);
            ViewBag.ProductId = new SelectList(products.Where(x => x.IsActive == true), "ProductId", "ProductName", processedSalePriceConfig.ProductId);
            return View(processedSalePriceConfig);
        }

        // POST: ProcessedSalePriceConfigs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ProcessedSalePriceConfigId,ProductId,PurchasePrice,SalePrice,IsActive,FromDate,ToDate")] ProcessedSalePriceConfig processedSalePriceConfig)
        {
            if (ModelState.IsValid)
            {
                db.Entry(processedSalePriceConfig).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(processedSalePriceConfig);
        }

        // GET: ProcessedSalePriceConfigs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProcessedSalePriceConfig processedSalePriceConfig = db.ProcessedSalePriceConfig.Find(id);
            if (processedSalePriceConfig == null)
            {
                return HttpNotFound();
            }
            return View(processedSalePriceConfig);
        }

        // POST: ProcessedSalePriceConfigs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ProcessedSalePriceConfig processedSalePriceConfig = db.ProcessedSalePriceConfig.Find(id);
            db.ProcessedSalePriceConfig.Remove(processedSalePriceConfig);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
