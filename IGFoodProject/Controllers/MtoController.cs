﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;

namespace IGFoodProject.Controllers
{
    public class MtoController : Controller
    {
        MtoDal _aDal=new MtoDal();
        CustomerDAL _customerDal = new CustomerDAL();
        //SalesOrderFurtherProcessDAL _salesFurtherProcess = new SalesOrderFurtherProcessDAL();
        //SalesOrderByProductDAL _salesByProduct = new SalesOrderByProductDAL();

        public ActionResult Index(int soId = 0, string eflag = "ed")
        {
            ViewBag.SalesOrderId = soId;
            ViewBag.eflag = eflag;
            return View();
        }
        public ActionResult GetCustomer(int typeid)
        {
            List<ViewCustomerInfo> aList = _customerDal.GetCustomerList(typeid);
            return Json(aList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetCustomerType()
        {
            List<CustomerType> aList = _aDal.GetCustomerTypeList();
            return Json(aList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetCustomerDetails(int id)
        {

            ViewCustomerInfo ci = _customerDal.GetCustomerViewById(id);
            return PartialView("_customerDetailsView", ci);
        }
        public JsonResult LoadWareHouseByMtoCustomer(int customerId)
        {
           
            List<MtoLocationWarehouse> alist = _aDal.LoadWareHouseByMtoCustomer(customerId);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadLocationByMtoCustomer(int customerId)
        {

            List<MtoLocationWarehouse> alist = _aDal.LoadLocationByMtoCustomer(customerId);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadLocation(int id)
        {
            List<MtoLocationWarehouse> locationList =  _aDal.GetLocationIdByWareHouse(id);
            return Json(locationList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetSalesPhoneNo(int empNo)
        {
            string PhoneNo = _aDal.GetSalesPersonPhone(empNo);

            return Json(PhoneNo, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddRowForOrder(int tr, int customertype)
        {
            TempData["trSl"] = tr;
            ViewBag.customertype = customertype;
            return PartialView("_addNewOrderRow");
        }
        public ActionResult GetSpecialOffers(int customertype, DateTime orderDate, int location)
        {
            SpecialOffers alist = _aDal.GetSpecialOffers(customertype, orderDate, location);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetProductDetails(int productId, DateTime orderdate, int customertype, int wareHouseId)
        {
            ViewProductDetailsSales aDetailsSales = _aDal.GetProductDetails(productId, orderdate, customertype, wareHouseId);
            return Json(aDetailsSales, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetProductDetailsChallan(int productId, DateTime orderdate, int wareHouseId)
        {
            //ViewProductDetailsSales aDetailsSales = _salesProcess.GetProductDetails(productId, orderdate);
            ViewProductDetailsSales aDetailsSales = new ViewModel.ViewProductDetailsSales();
            return Json(aDetailsSales, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetProcessedSalePrice(int productId, decimal price)
        {
            decimal salePrice = _aDal.GetProductSalePrice(productId, price);
            return Json(salePrice, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SalesOrderList(DateTime? fromdate, DateTime? todate, string salespersonid, int customerid = 0, int deliveryWarehouseId = 0)
        {
            List<ViewSalesOrderProcessedMaster> aList = _aDal.GetSalesOrderProcessedMasterSearch(fromdate, todate, salespersonid, customerid, deliveryWarehouseId);


            return View(aList);
        }
        public ActionResult SalesOrderList_Search(DateTime? fromdate, DateTime? todate, string salespersonid, int customerid = 0, int deliveryWarehouseId = 0)
        {
            List<ViewSalesOrderProcessedMaster> aList = _aDal.GetSalesOrderProcessedMasterSearch(fromdate, todate, salespersonid, customerid, deliveryWarehouseId);
            
            return Json(aList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ViewOrder(int id)
        {
            return PartialView("_processedOrder");
        }
        public ActionResult SalesOrderApproveList(DateTime? fromdate, DateTime? todate, string salespersonid, int customerid = 0, int deliveryWarehouseId = 0)
        {
            IEnumerable<ViewSalesOrderProcessedMaster> aList = _aDal.GetSalesOrderProcessedMasterSearch(fromdate, todate, salespersonid, customerid, deliveryWarehouseId);


            return View(aList);
        }
        public ActionResult SalesOrderApproveList_Search(DateTime? fromdate, DateTime? todate, string salespersonid, int customerid = 0, int deliveryWarehouseId = 0)
        {
            IEnumerable<ViewSalesOrderProcessedMaster> aList = _aDal.GetSalesOrderProcessedMasterSearch(fromdate, todate, salespersonid, customerid, deliveryWarehouseId);


            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SalesOrderApproveListForCancel(DateTime? fromdate, DateTime? todate, string salespersonid, int customerid = 0)
        {
            IEnumerable<ViewSalesOrderProcessedMaster> aList = _aDal.GetSalesOrderProcessedMasterForCancel(fromdate, todate, salespersonid, customerid);


            return View(aList);
        }

        public ActionResult SalesOrderApproveListForCancel_Search(DateTime? fromdate, DateTime? todate, string salespersonid, int customerid = 0)
        {
            IEnumerable<ViewSalesOrderProcessedMaster> aList = _aDal.GetSalesOrderProcessedMasterForCancel(fromdate, todate, salespersonid, customerid);


            return Json(aList, JsonRequestBehavior.AllowGet);
        }


        public ActionResult ApproveSalesProcessed(int id)
        {

            ViewBag.SalesOrder = id;
            return View();


        }


        [HttpPost]
        public ActionResult SaveOrderPrecessed(SalesOrderProcessedMaster aMaster)
        {

            ResultResponse aResponse = _aDal.SaveSalesOrderProcessed(aMaster,
                Session[SessionCollection.UserName].ToString());
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveOrderPrecessedApprove(SalesOrderProcessedMaster aMaster)
        {

            ResultResponse aResponse = _aDal.SaveSalesOrderProcessedApprove(aMaster,
                Session[SessionCollection.UserName].ToString());
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetSalesOrderMaster(int id)
        {
            SalesOrderProcessedMaster aMaster = _aDal.GetSalesOrderProcessedMaster(id);
            aMaster.SalesDetails = _aDal.GetSalesOderProcessedDetailsByMaster(id);
            return Json(aMaster, JsonRequestBehavior.AllowGet);
        }


    }
}