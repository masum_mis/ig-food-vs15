﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.Models;
using Newtonsoft.Json;
using IGFoodProject.DAL;
using IGFoodProject.Models.ReportModel;
using IGFoodProject.ViewModel;
using IGFoodProject.Utility;

namespace IGFoodProject.Controllers
{
    public class StockTransferController : Controller
    {
        StockTransferDAL stockTransferDAL = new StockTransferDAL();
        // GET: StockTransfer
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult StockTransferEntry(int id=0)
        {
            UtilityDAL utilityDAL = new UtilityDAL();
            ProductDAL productDAL = new ProductDAL();
            List<CompanyInformation> companyInformation = utilityDAL.CompanyInformation();
            List<Location> locations = utilityDAL.LocationInformation();
            List<tbl_ProductGroup> productGroups = productDAL.GetGroupList();
            var dataForView = new Tuple<IEnumerable<CompanyInformation>, IEnumerable<Location>, IEnumerable<tbl_ProductGroup>>(companyInformation, locations, productGroups);
            ViewBag.id = id;
            return View(dataForView);
        }

        public ActionResult InternalStockTransferEntry(int id = 0)
        {
            UtilityDAL utilityDAL = new UtilityDAL();
            ProductDAL productDAL = new ProductDAL();
            List<CompanyInformation> companyInformation = utilityDAL.CompanyInformation();
            List<Location> locations = utilityDAL.LocationInformation();
            List<InternalTransferComments> comment = utilityDAL.InternalStoComments();
            List<tbl_ProductGroup> productGroups = productDAL.GetGroupList();
            var dataForView = new Tuple<IEnumerable<CompanyInformation>, IEnumerable<Location>, IEnumerable<InternalTransferComments>, IEnumerable<tbl_ProductGroup>>(companyInformation, locations, comment, productGroups);
            ViewBag.id = id;
            return View(dataForView);
        }

        public ActionResult SaveStockTransferRequest(StockTransferReqMaster aMaster)
        {
            aMaster.CreateBy = Session[SessionCollection.UserName].ToString();
            ResultResponse aResponse = stockTransferDAL.SaveStockTransferRequisitionMaster(aMaster);
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveInternalStockTransfer(StockTransferReqMaster aMaster)
        {
            aMaster.CreateBy = Session[SessionCollection.UserName].ToString();
            ResultResponse aResponse = stockTransferDAL.SaveInternalStockTransferMaster(aMaster);
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetStockedProductByWarehouseAndGroup(int warehouseId, int groupId)
        {
           
            List<ViewProductByGroupAndWareHouse> productList = stockTransferDAL.GetStockedProductByWarehouseAndGroup(warehouseId, groupId);
            return Json(productList, JsonRequestBehavior.AllowGet);
        }
        public dynamic GetAddRow(int tr)
        {
            TempData["trSl"] = tr;
            return PartialView("_partialProductList");
        }

        public ActionResult StockTransferIssue()
        {
            return View();
        }

        public ActionResult GetStockTransferRequestByDate(DateTime reqDate)
        {
            List<StockTransferReqMaster> stockTransferReqMasters = new List<StockTransferReqMaster>();
            stockTransferReqMasters = stockTransferDAL.GetStockTransferRequestByDate(reqDate);
            return Json(stockTransferReqMasters, JsonRequestBehavior.AllowGet);
        }
        public ActionResult StockTransferList(DateTime? requisitionDate)
        {
            List<StockTransferReqMaster> aList = stockTransferDAL.StockTransferList(requisitionDate);
            return View(aList);
        }
        public ActionResult StockTransferList_Search(DateTime? requisitionDate)
        {
            List<StockTransferReqMaster> aList = stockTransferDAL.StockTransferList(requisitionDate);


            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InternalStockTransferList(DateTime? fromDate, DateTime? toDate)
        {
            List<StockTransferReqMaster> aList = stockTransferDAL.InternalStockTransferList(fromDate, toDate);
            return View(aList);
        }
        public ActionResult InternalStockTransferList_Search(DateTime? fromDate, DateTime? toDate)
        {
            List<StockTransferReqMaster> aList = stockTransferDAL.InternalStockTransferList(fromDate, toDate);


            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetStockTransferMaster(int id)
        {
            StockTransferReqMaster aMaster = stockTransferDAL.GetStockTransferMaster(id);
            aMaster.StockTransferReqDetails = stockTransferDAL.GetStockTransferDetailsByMaster(id);
            return Json(aMaster, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetStockTransferDetailsbyMasterId(int id)
        {
            StockTransferReqMaster aMaster = stockTransferDAL.GetStockTransferMaster(id);
            aMaster.StockTransferReqDetails= stockTransferDAL.GetStockTransferDetailsByMaster(id);
            //List<StockTransferReqDetails> StockTransferReqDetails = stockTransferDAL.GetStockTransferDetailsByMaster(id);
            return PartialView("_stockTransferReqDetails", aMaster);
           // return Json(StockTransferReqDetails, JsonRequestBehavior.AllowGet);
        }

        public ActionResult StockTransferListForIssue(DateTime? issueDate,int companyId=0, int fromLocation=0, int fromWarhouse=0, int toLocation=0, int toWarehouse=0, int vehicleId=0)
        {
            List<StockTransferReqMaster> aList = stockTransferDAL.StockTransferListForIssue(issueDate, companyId, fromLocation, fromWarhouse, toLocation, toWarehouse, vehicleId);
            return View(aList);
        }

        public ActionResult StockTransferChallan()
        {
            return View();
        }


        public ActionResult StockTransferChallanListForPrint(DateTime fromDate, DateTime toDate, int fromWarehouse = 0, int fromLocation = 0, int toWarehouse = 0, int toLocation = 0)  
        {
            List<StockTransferIssueMaster> aList = stockTransferDAL.StockTransferChallanListForPrint(fromDate, toDate, fromWarehouse, fromLocation, toWarehouse, toLocation);
            return PartialView("_challanListForPrint", aList);
        }


        public ActionResult StockTransferChallanReports(SalesOrderReport aSalesOrderReport) 
        {
            ResultResponse aResponse = new ResultResponse();
            BasicInfoReportsParameter aBasicInfoReportsParameter = new BasicInfoReportsParameter();
            aBasicInfoReportsParameter.ReportValue = aSalesOrderReport.ReportValue;
            Session[SessionCollection.ReportValue] = aBasicInfoReportsParameter;
            Session[SessionCollection.ReportParam] = aSalesOrderReport;
            aResponse.isSuccess = true;
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }



        public ActionResult StockTransferListForIssue_Search(DateTime? issueDate, int companyId = 0, int fromLocation = 0, int fromWarhouse = 0, int toLocation = 0, int toWarehouse = 0, int vehicleId = 0)
        {
            List<StockTransferReqMaster> aList = stockTransferDAL.StockTransferListForIssue(issueDate, companyId, fromLocation, fromWarhouse, toLocation, toWarehouse, vehicleId);
            return Json(aList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LoadVehicle(DateTime deliveryDate)
        {
            List<VehicleInfo> vehicle = stockTransferDAL.VehicleInfo(deliveryDate);
            return Json(vehicle, JsonRequestBehavior.AllowGet);
        }

        public ActionResult StockTransferIssueEntry(int id = 0)
        {
            UtilityDAL utilityDAL = new UtilityDAL();
            ProductDAL productDAL = new ProductDAL();
            List<CompanyInformation> companyInformation = utilityDAL.CompanyInformation();
            List<Location> locations = utilityDAL.LocationInformation();
            List<tbl_ProductGroup> productGroups = productDAL.GetGroupList();
            var dataForView = new Tuple<IEnumerable<CompanyInformation>, IEnumerable<Location>, IEnumerable<tbl_ProductGroup>>(companyInformation, locations, productGroups);
            ViewBag.id = id;
            return View(dataForView);
        }
        public dynamic GetAddRow_issue(int tr)
        {
            TempData["trSl"] = tr;
            return PartialView("_partialProductList_issue");
        }

        public ActionResult GetStockedProductById(int stockTransferReqMasterId)
        {

            List<ViewProductByGroupAndWareHouse> productList = stockTransferDAL.GetStockedProductById(stockTransferReqMasterId);
            return Json(productList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetStockTransferMasterForIssue(int id)
        {
            StockTransferReqMaster aMaster = stockTransferDAL.GetStockTransferMaster(id);
            aMaster.StockTransferReqDetails = stockTransferDAL.GetStockTransferDetailsByMasterForIssue(id);
            return Json(aMaster, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveStockTransferIssue(StockTransferIssueMaster aMaster)
        {
            aMaster.CreateBy = Session[SessionCollection.UserName].ToString();
            ResultResponse aResponse = stockTransferDAL.SaveStockTransferIssueMaster (aMaster);
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetStockedProductByWarehouseAndGroup5_6(int warehouseId, int groupId)
        {

            List<ViewProductByGroupAndWareHouse> productList = stockTransferDAL.GetStockedProductByWarehouseAndGroup5_6(warehouseId, groupId);
            return Json(productList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetGroupByProduct(int id)
        {

            ViewProductByGroupAndWareHouse aListView = stockTransferDAL.GetGroupByProduct(id);

            return Json(aListView, JsonRequestBehavior.AllowGet);
        }
    }
}