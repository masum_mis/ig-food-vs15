﻿using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.ViewModel;
using Newtonsoft.Json;

namespace IGFoodProject.Controllers
{
    public class SalesRepresentativeController : Controller
    {
       private SalesRepresentativeDAL salesRepresentativeDAL = new SalesRepresentativeDAL();
        // GET: SalesRepresentative
        public ActionResult CreateSalesRepresentative(int srId = 0)
        {


            if (Session[SessionCollection.LoginUserEmpId] != null)
            {
                SRInfoMaster sRInfoMaster = salesRepresentativeDAL.GetSRDataForEdit(srId);
                return View(sRInfoMaster);
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
        }
        public ActionResult GetDealerLogListForSR(int srId = 0)
        {
            SRInfoMaster sRInfoMaster = salesRepresentativeDAL.GetSRDataForEdit(srId,true);
            return PartialView("_DealerListForSRLog", sRInfoMaster);
        }

        public ActionResult SaveOrUpdateSRInfo(SRInfoMaster sRInfo)
        {
            ResultResponse result = new ResultResponse();
            result = salesRepresentativeDAL.SaveOrUpdateSrInformation(sRInfo);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SalesRepresentativeList()
        {

            if (Session[SessionCollection.LoginUserEmpId] != null)
            {

                List<SRInfoMaster> sRInfoMasterList = salesRepresentativeDAL.GetSRList();

                return View(sRInfoMasterList);
            }
            else
            {

                return RedirectToAction("Login", "Home");
            }
           
        }

        public ActionResult SrSalaryProcessEntry()
        {

            if (Session[SessionCollection.LoginUserEmpId] != null)
            {

                List<SRInfoMaster> sRInfoMasterList = salesRepresentativeDAL.GetSRList();

                return View(sRInfoMasterList);
            }
            else
            {

                return RedirectToAction("Login", "Home");
            }

        }

        public ActionResult GetAllOrdersOfDealers(DateTime? fromDate, DateTime? toDate)
        {
            DataTable dt = salesRepresentativeDAL.GetAllOrdersOfDealers(fromDate,toDate);

            return PartialView("_OrderDeliveryPendingReport", dt);
        }
        
        public dynamic SrSalaryProcess(ViewSRSalaryProcess viewSRSalaryProcess)
        {

            ResultResponse result = new ResultResponse();
            DataTable dt=new DataTable();
            viewSRSalaryProcess.EmpId = Convert.ToInt32(Session[SessionCollection.LoginUserEmpId]);
            dt = salesRepresentativeDAL.SaveSrSalaryProcessData(viewSRSalaryProcess);
           string JSONString = JsonConvert.SerializeObject(dt);
            return Json(JSONString, JsonRequestBehavior.AllowGet);
        }

        public ActionResult testData()
        {
            return View();
        }

        public ActionResult SrSalaryPaySlip()
        {

            if (Session[SessionCollection.LoginUserEmpId] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }
        public ActionResult GetSRByCustomerId(int customerid)
        {
            List<SRInfoMaster> aList = salesRepresentativeDAL.GetSRByCustomerId(customerid);
            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSR(int customerid, DateTime fromDate, DateTime toDate)
        {
            List<SRInfoMaster> aList = salesRepresentativeDAL.GetSRList(customerid, fromDate, toDate);
            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCustomer(int typeid, DateTime fromDate, DateTime toDate)
        {
            List<ViewCustomerInfo> aList = salesRepresentativeDAL.GetCustomerList(typeid, fromDate, toDate);
            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DealerRetentionProcessEntry()
        {

            if (Session[SessionCollection.LoginUserEmpId] != null)
            {

                List<SRInfoMaster> sRInfoMasterList = salesRepresentativeDAL.GetSRList();

                return View();
            }
            else
            {

                return RedirectToAction("Login", "Home");
            }

        }
        public ActionResult GetAllOrdersOfDealersForRetention(DateTime? fromDate, DateTime? toDate)
        {
            DataTable dt = salesRepresentativeDAL.GetAllOrdersOfDealersForRetention(fromDate, toDate);
            string JSONString = JsonConvert.SerializeObject(dt);
            //return PartialView("_OrderDeliveryPendingReport", dt);
            return Json(JSONString, JsonRequestBehavior.AllowGet);
        }
        public dynamic SaveDealerRetentionProcess(DealerRetentionModel viewSRSalaryProcess)
        {

            ResultResponse result = new ResultResponse();
            DataTable dt = new DataTable();
            viewSRSalaryProcess.EmpId = Convert.ToInt32(Session[SessionCollection.LoginUserEmpId]);
            dt = salesRepresentativeDAL.SaveDealerRetentionProcessData(viewSRSalaryProcess);
            string JSONString = JsonConvert.SerializeObject(dt);
            return Json(JSONString, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DealerRetentionReport()
        {
            return View();
        }

        public ActionResult GetDealerRetentionSummary(DateTime fromDate, DateTime toDate, int customer)
        {           
            DataTable aObj = salesRepresentativeDAL.GetCustomerRetentionSummary(fromDate, toDate,  customer);
            string JSONresult;
            JSONresult = JsonConvert.SerializeObject(aObj);           
            return Json(JSONresult, JsonRequestBehavior.AllowGet);

        }

        public ActionResult DealerDamageProcessEntry()
        {

            if (Session[SessionCollection.LoginUserEmpId] != null)
            {

                List<SRInfoMaster> sRInfoMasterList = salesRepresentativeDAL.GetSRList();

                return View();
            }
            else
            {

                return RedirectToAction("Login", "Home");
            }

        }
        public ActionResult GetAllOrdersOfDealersForDamage(DateTime? fromDate, DateTime? toDate)
        {
            DataTable dt = salesRepresentativeDAL.GetAllOrdersOfDealersForDamage(fromDate, toDate);
            string JSONString = JsonConvert.SerializeObject(dt);
            //return PartialView("_OrderDeliveryPendingReport", dt);
            return Json(JSONString, JsonRequestBehavior.AllowGet);
        }
        public dynamic SaveDealerDamageProcess(DealerRetentionModel viewSRSalaryProcess)
        {

            ResultResponse result = new ResultResponse();
            DataTable dt = new DataTable();
            viewSRSalaryProcess.EmpId = Convert.ToInt32(Session[SessionCollection.LoginUserEmpId]);
            dt = salesRepresentativeDAL.SaveDealerDamageProcessData(viewSRSalaryProcess);
            string JSONString = JsonConvert.SerializeObject(dt);
            return Json(JSONString, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DealerDamageReport()
        {
            return View();
        }
        public ActionResult GetDealerDamageSummary(DateTime fromDate, DateTime toDate, int customer)
        {

            DataTable aObj = salesRepresentativeDAL.GetCustomerDamageSummary(fromDate, toDate, customer);
            string JSONresult;
            JSONresult = JsonConvert.SerializeObject(aObj);


            return Json(JSONresult, JsonRequestBehavior.AllowGet);

        }



        public ActionResult ListApproveSrSalary()
        {

            if (Session[SessionCollection.LoginUserEmpId] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }
        public ActionResult DataforListApproveSrSalary(int monthId, int yearId)
        {

            List<SrSalaryApproval> aList = salesRepresentativeDAL.DataforListApproveSrSalary(monthId,yearId);
        

            return Json(aList, JsonRequestBehavior.AllowGet);

        }


        public ActionResult ApproveSrSalary(List<SrSalaryApproval> entity)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                string entryBy = Session[SessionCollection.UserName].ToString();
                foreach (var item in entity)
                {
                     aResponse = salesRepresentativeDAL.ApproveSrSalary(item, entryBy);
                }


              
                return Json(aResponse, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public ActionResult ListApproveDamage()
        {

            if (Session[SessionCollection.LoginUserEmpId] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }
        public ActionResult DataforListApproveDamage(int monthId, int yearId)
        {

            List<DamageApproveViewModel> aList = salesRepresentativeDAL.DataforListApproveDamage(monthId, yearId);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }
        public ActionResult ApproveDamage(List<DamageApproveViewModel> entity)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                string entryBy = Session[SessionCollection.UserName].ToString();
                foreach (var item in entity)
                {
                    aResponse = salesRepresentativeDAL.ApproveDamage(item, entryBy);
                }



                return Json(aResponse, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult ListApproveRetention()
        {

            if (Session[SessionCollection.LoginUserEmpId] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }
        public ActionResult DataforListApproveRetention(int monthId, int yearId)
        {

            List<RetentionApprovalViewModel> aList = salesRepresentativeDAL.DataforListApproveRetention(monthId, yearId);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }


        public ActionResult ApproveRetention(List<RetentionApprovalViewModel> entity)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                string entryBy = Session[SessionCollection.UserName].ToString();
                foreach (var item in entity)
                {
                    aResponse = salesRepresentativeDAL.ApproveRetention(item, entryBy);
                }



                return Json(aResponse, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //Discount Process

        public ActionResult DealerDiscountProcessEntry()
        {

            if (Session[SessionCollection.LoginUserEmpId] != null)
            {
                return View();
            }
            else
            {

                return RedirectToAction("Login", "Home");
            }

        }
        public ActionResult GetAllOrdersOfDealersForDiscount(DateTime? fromDate, DateTime? toDate)
        {
            DataTable dt = salesRepresentativeDAL.GetAllOrdersOfDealersForDiscount(fromDate, toDate);
            string JSONString = JsonConvert.SerializeObject(dt);
            return Json(JSONString, JsonRequestBehavior.AllowGet);
        }
        public dynamic SaveDealerDiscountProcess(DealerRetentionModel viewSRSalaryProcess)
        {

            ResultResponse result = new ResultResponse();
            DataTable dt = new DataTable();
            viewSRSalaryProcess.EmpId = Convert.ToInt32(Session[SessionCollection.LoginUserEmpId]);
            dt = salesRepresentativeDAL.SaveDealerDiscountProcessData(viewSRSalaryProcess);
            string JSONString = JsonConvert.SerializeObject(dt);
            return Json(JSONString, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DealerDiscountReport()
        {
            return View();
        }
        public ActionResult GetDealerDiscountSummary(DateTime fromDate, DateTime toDate, int customer)
        {

            DataTable aObj = salesRepresentativeDAL.GetCustomerDiscountSummary(fromDate, toDate, customer);
            string JSONresult;
            JSONresult = JsonConvert.SerializeObject(aObj);


            return Json(JSONresult, JsonRequestBehavior.AllowGet);

        }
        public ActionResult ListApproveDiscount()
        {

            if (Session[SessionCollection.LoginUserEmpId] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

        }
        public ActionResult DataforListApproveDiscount(int monthId, int yearId)
        {

            List<DiscountApproveViewModel> aList = salesRepresentativeDAL.DataforListApproveDiscount(monthId, yearId);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }
        public ActionResult ApproveDiscount(List<DiscountApproveViewModel> entity)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                string entryBy = Session[SessionCollection.UserName].ToString();
                foreach (var item in entity)
                {
                    aResponse = salesRepresentativeDAL.ApproveDiscount(item, entryBy);
                }



                return Json(aResponse, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}