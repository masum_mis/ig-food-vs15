﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.Models;

namespace IGFoodProject.Controllers
{
    public class BankMastersController : Controller
    {
        private CodeDbSet db = new CodeDbSet();

        // GET: BankMasters
        public ActionResult Index()
        {
            return View(db.BankMaster.ToList());
        }

        // GET: BankMasters/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BankMaster bankMaster = db.BankMaster.Find(id);
            if (bankMaster == null)
            {
                return HttpNotFound();
            }
            return View(bankMaster);
        }

        // GET: BankMasters/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BankMasters/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "BankId,BankName,IsActive")] BankMaster bankMaster)
        {
            if (ModelState.IsValid)
            {
                var id = db.BankMaster.Where(x => x.BankName ==bankMaster.BankName);
                if (id.Any())
                {
                    return RedirectToAction("Index");
                }

                db.BankMaster.Add(bankMaster);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bankMaster);
        }

        // GET: BankMasters/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BankMaster bankMaster = db.BankMaster.Find(id);
            if (bankMaster == null)
            {
                return HttpNotFound();
            }
            return View(bankMaster);
        }

        // POST: BankMasters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "BankId,BankName,IsActive")] BankMaster bankMaster)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bankMaster).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bankMaster);
        }

        // GET: BankMasters/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BankMaster bankMaster = db.BankMaster.Find(id);
            if (bankMaster == null)
            {
                return HttpNotFound();
            }
            return View(bankMaster);
        }

        // POST: BankMasters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BankMaster bankMaster = db.BankMaster.Find(id);
            db.BankMaster.Remove(bankMaster);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
