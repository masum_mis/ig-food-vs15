﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DataManager;
using IGFoodProject.DAL;
using IGFoodProject.Models;

namespace IGFoodProject.Controllers
{
    public class FurtherProcessPriceConfigsController : Controller
    {
        private CodeDbSet db = new CodeDbSet();

        // GET: FurtherProcessPriceConfigs
        public ActionResult Index()
        {
            //var result = (from f in db.FurtherProcessPriceConfig
            //    join p in db.Product on f.ProductId equals p.ProductId
            //    join pk in db.PackSize on p.PackSizeId equals pk.PackSizeId
            //    select new
            //    {
            //        p.ProductName,
            //        pk.PackSizeName,
            //        f.MRP,
            //        f.TPPrice,
            //        f.DPPrice,
            //        f.IsActive,
            //        f.FromDate,
            //        f.ToDate,
            //        f.FurtherProcessPriceID
            //    }).ToList();
            return View(db.FurtherProcessPriceConfig.Include(p=> p.Product).ToList());
        }

        // GET: FurtherProcessPriceConfigs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FurtherProcessPriceConfig furtherProcessPriceConfig = db.FurtherProcessPriceConfig.Find(id);
            if (furtherProcessPriceConfig == null)
            {
                return HttpNotFound();
            }
            return View(furtherProcessPriceConfig);
        }

        // GET: FurtherProcessPriceConfigs/Create
        public ActionResult Create()
        {
            ProductDAL aDal = new ProductDAL();
            List<Product> products = aDal.GetProductList(6);
            ViewBag.ProductId = new SelectList(db.Product.Where(p => p.GroupId == 6 && p.GroupId == 12).Where(x => x.IsActive == true), "ProductId", "ProductName");
            ViewBag.CustomerTypeID = new SelectList(db.CustomerTypes.Where(x => x.IsActive==true), "CustomerTypeID", "CoustomerTypeName");
            return View();
        }

        // POST: FurtherProcessPriceConfigs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "FurtherProcessPriceID,ProductId,MRP,TPPrice,DPPrice,IsActive,FromDate,ToDate")] FurtherProcessPriceConfig furtherProcessPriceConfig)
        {
            if (ModelState.IsValid)
            {
                db.FurtherProcessPriceConfig.Add(furtherProcessPriceConfig);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(furtherProcessPriceConfig);
        }

        // GET: FurtherProcessPriceConfigs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FurtherProcessPriceConfig furtherProcessPriceConfig = db.FurtherProcessPriceConfig.Find(id);
            if (furtherProcessPriceConfig == null)
            {
                return HttpNotFound();
            }
            ProductDAL aDal = new ProductDAL();
            List<Product> products = aDal.GetProductList(6);
            ViewBag.ProductId = new SelectList(products.Where(x => x.IsActive == true), "ProductId", "ProductName", furtherProcessPriceConfig.ProductId);
            ViewBag.CustomerTypeId = new SelectList(db.CustomerTypes.Where(x => x.IsActive == true), "CustomerTypeID", "CoustomerTypeName", furtherProcessPriceConfig.CustomerTypeId);
            return View(furtherProcessPriceConfig);
        }

        // POST: FurtherProcessPriceConfigs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "FurtherProcessPriceID,ProductId,MRP,TPPrice,DPPrice,IsActive,FromDate,ToDate,CustomerTypeId")] FurtherProcessPriceConfig furtherProcessPriceConfig)
        {
            if (ModelState.IsValid)
            {
                db.Entry(furtherProcessPriceConfig).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            
            return View(furtherProcessPriceConfig);
        }

        // GET: FurtherProcessPriceConfigs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FurtherProcessPriceConfig furtherProcessPriceConfig = db.FurtherProcessPriceConfig.Find(id);
            if (furtherProcessPriceConfig == null)
            {
                return HttpNotFound();
            }
          
            return View(furtherProcessPriceConfig);
        }

        // POST: FurtherProcessPriceConfigs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            FurtherProcessPriceConfig furtherProcessPriceConfig = db.FurtherProcessPriceConfig.Find(id);
            db.FurtherProcessPriceConfig.Remove(furtherProcessPriceConfig);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
