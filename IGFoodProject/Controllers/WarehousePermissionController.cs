﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;

namespace IGFoodProject.Controllers
{
    public class WarehousePermissionController : Controller
    {


        WarehousePermissionDAL _wpPermissionDal = new WarehousePermissionDAL();
        // GET: WarehousePermission
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult GetAllIGUsers()
        {
            List<ViewUserWarehouse> aList = _wpPermissionDal.GetUsersOfIGFoods();
            return PartialView("_usersIgFoods", aList);
        }


        public ActionResult GetUserWiseWarehouse(int id)
        {
            List<ViewUserWarehouse> aList = _wpPermissionDal.GetUserWiseWarehouse(id);
            return PartialView("_userWiseWh", aList);
        }

        [HttpPost]
        public ActionResult SaveUserWisePermission(List<ViewUserWarehouse> aList)
        {
            ResultResponse aResponse = _wpPermissionDal.SaveUserWisePermission(aList, Session[SessionCollection.UserName].ToString());
            return Json(aResponse,JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetWhWiseUser(int id)
        {
            List<ViewUserWarehouse> aList = _wpPermissionDal.GetWhWiseUser(id);
            return PartialView("_usersIgFoods", aList);
        }
    }
}