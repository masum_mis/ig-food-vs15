﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.Utility;
using IGFoodProject.DAL;
using IGFoodProject.Models;

namespace IGFoodProject.Controllers
{
    public class UOMController : Controller
    {
        // GET: UOM
        public ActionResult Index()
        {
            UOMDAL aDal = new UOMDAL();
            List<tbl_UOM> alist = aDal.GetUOMList();
            return View(alist);
        }

        public ActionResult AddUOM(int id = 0)
        {
            ViewBag.uomid = id;
            return View();
        }
        public JsonResult CheckUOM(string uom)
        {

            UOMDAL aDAL = new UOMDAL();

            int desig = aDAL.CheckUOM(uom);

            return Json(desig ,JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveUOM(tbl_UOM aUOM)
        {
            ResultResponse aResponse = new ResultResponse();
            UOMDAL aDAL = new UOMDAL();

            string entryBy = Session[SessionCollection.UserName].ToString();
            aUOM.CreateBy = entryBy;
            aUOM.UpdateBy = entryBy;
            aResponse.isSuccess = aDAL.SaveUOM(aUOM);

            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetUOMByID(int id)
        {
            UOMDAL aDal = new UOMDAL();
            return Json(aDal.GetUOMForEdit(id), JsonRequestBehavior.AllowGet);

        }
        public JsonResult DeleteUOM(int id)
        {
            ResultResponse aResponse = new ResultResponse();
            UOMDAL aDAL = new UOMDAL();


            aResponse.isSuccess = aDAL.DeleteUOM(id);

            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
    }
}