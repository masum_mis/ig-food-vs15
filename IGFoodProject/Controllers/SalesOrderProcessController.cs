﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;

namespace IGFoodProject.Controllers
{
    public class SalesOrderProcessController : Controller
    {
        // GET: SalesOrderProcess



        CustomerDAL _customerDal = new CustomerDAL();
        SalesOrderProcessedDAL _salesProcess = new SalesOrderProcessedDAL();
        SalesOrderFurtherProcessDAL _salesFurtherProcess = new SalesOrderFurtherProcessDAL();
        SalesOrderByProductDAL _salesByProduct = new SalesOrderByProductDAL();

        UtilityDAL _utilityDal = new UtilityDAL();

        public ActionResult Index(int soId = 0, string eflag="ed")
        {
            ViewBag.SalesOrderId = soId;
            ViewBag.eflag = eflag;
            return View();
        }

        public ActionResult GetCustomer(int typeid)
        {
            List<ViewCustomerInfo> aList = _customerDal.GetCustomerList(typeid);
            return Json(aList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetDeliveryWarehouse(int warehouseTypeid)
        {
            List<WareHouse> aList = _customerDal.GetWareHousesByType(warehouseTypeid);
            return Json(aList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetDeliveryWarehouse_OtherPurchase(int warehouseTypeid)
        {
            List<WareHouse> aList = _customerDal.GetWareHousesByType_OtherPurchase(warehouseTypeid);
            return Json(aList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetWarehouseInfo(int warehouseTypeid, string UserId)
        {
            List<WareHouse> aList = _customerDal.GetWarehouseInfo(warehouseTypeid, UserId);
            return Json(aList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetProjectInfo()
        {
            List<IssueRequisition> aList = _customerDal.GetProjectInfo();
            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCreditAndLedgerBalance( int id, DateTime orderDate, int masterId)
        {
            ViewCustomerInfo aList = _customerDal.GetCreditAndLedgerBalance(id, orderDate, masterId); 
            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCustomerDetails(int id)
        {

            ViewCustomerInfo ci = _customerDal.GetCustomerViewById(id);
            return PartialView("_customerDetailsView", ci);
        }

        public ActionResult GetSalesPhoneNo(int empNo)
        {
            string PhoneNo = _salesProcess.GetSalesPersonPhone(empNo);

            return Json(PhoneNo, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult AddRowForOrder(int tr)
        //{
        //    TempData["trSl"] = tr;

        //    return PartialView("_addNewOrderRow");
        //}
        public ActionResult GetProductDetails(int productId, DateTime orderdate, int customertype, int wareHouseId, int customerId = 0)
        {
            ViewProductDetailsSales aDetailsSales = _salesProcess.GetProductDetails(productId, orderdate, customertype, wareHouseId, customerId);
            //public ActionResult GetProductDetails(int productId, DateTime orderdate, int customertype, int wareHouseId)
            //{
            //    ViewProductDetailsSales aDetailsSales = _salesProcess.GetProductDetails(productId, orderdate, customertype, wareHouseId);
            return Json(aDetailsSales, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetProductDetailsChallan(int productId, DateTime orderdate, int wareHouseId)
        {
            //ViewProductDetailsSales aDetailsSales = _salesProcess.GetProductDetails(productId, orderdate);
            ViewProductDetailsSales aDetailsSales = new ViewModel.ViewProductDetailsSales();
            return Json(aDetailsSales, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetProcessedSalePrice(int productId, decimal price)
        {
            decimal salePrice = _salesProcess.GetProductSalePrice(productId, price);
            return Json(salePrice, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public ActionResult SaveOrderPrecessed(SalesOrderProcessedMaster aMaster)
        {

            ResultResponse aResponse = _salesProcess.SaveSalesOrderProcessed(aMaster,
                Session[SessionCollection.UserName].ToString());
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SalesOrderList(DateTime? fromdate, DateTime? todate, string salespersonid, int customerid = 0, int deliveryWarehouseId = 0)
        {
            List<ViewSalesOrderProcessedMaster> aList = _salesProcess.GetSalesOrderProcessedMasterSearch(fromdate, todate, salespersonid, customerid, deliveryWarehouseId);


            return View(aList);
        }
        public ActionResult SalesOrderList_Search(DateTime? fromdate, DateTime? todate, string salespersonid, int customerid = 0,int deliveryWarehouseId=0)
        {
            List<ViewSalesOrderProcessedMaster> aList = _salesProcess.GetSalesOrderProcessedMasterSearch(fromdate, todate, salespersonid, customerid, deliveryWarehouseId);


            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewOrder(int id)
        {
            return PartialView("_processedOrder");
        }
        public ActionResult SalesOrderApproveList(DateTime? fromdate, DateTime? todate, string salespersonid, int customerid = 0, int deliveryWarehouseId = 0)
        {
            IEnumerable<ViewSalesOrderProcessedMaster> aList = _salesProcess.GetSalesOrderProcessedMasterSearch(fromdate, todate, salespersonid, customerid, deliveryWarehouseId);


            return View(aList);
        }
        public ActionResult SalesOrderApproveList_Search(DateTime? fromdate, DateTime? todate, string salespersonid, int customerid = 0,int deliveryWarehouseId = 0)
        {
            IEnumerable<ViewSalesOrderProcessedMaster> aList = _salesProcess.GetSalesOrderProcessedMasterSearch(fromdate, todate, salespersonid, customerid, deliveryWarehouseId);


            return Json(aList, JsonRequestBehavior.AllowGet);
        }


        public ActionResult SalesOrderApproveListForCancel(DateTime? fromdate, DateTime? todate, string salespersonid, int customerid = 0)
        {
            IEnumerable<ViewSalesOrderProcessedMaster> aList = _salesProcess.GetSalesOrderProcessedMasterForCancel(fromdate, todate, salespersonid, customerid);


            return View(aList);
        }
        public ActionResult SalesOrderApproveListForCancel_Search(DateTime? fromdate, DateTime? todate, string salespersonid, int customerid = 0)
        {
            IEnumerable<ViewSalesOrderProcessedMaster> aList = _salesProcess.GetSalesOrderProcessedMasterForCancel(fromdate, todate, salespersonid, customerid);


            return Json(aList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ApproveSalesProcessed(int id)
        {

            ViewBag.SalesOrder = id;
            return View();


        }

        public ActionResult GetSalesOrderMaster(int id)
        {
            SalesOrderProcessedMaster aMaster = _salesProcess.GetSalesOrderProcessedMaster(id);
            aMaster.SalesDetails = _salesProcess.GetSalesOderProcessedDetailsByMaster(id);
            return Json(aMaster, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetSalesOrderMaster_Challan(int id  , int pickId)
        {
            SalesOrderProcessedMaster aMaster = _salesProcess.GetSalesOrderProcessedMaster(id);
            aMaster.SalesDetails = _salesProcess.GetSalesOderProcessedDetailsByMaster_challan(id , pickId);
            return Json(aMaster, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetSalesOrderMaster_ForCancel(int id)
        {
            SalesOrderProcessedMaster aMaster = _salesProcess.GetSalesOrderProcessedMaster(id);
            aMaster.SalesDetails = _salesProcess.GetSalesOderProcessedDetailsApproveByMaster(id);
            return Json(aMaster, JsonRequestBehavior.AllowGet);
        }



        public ActionResult GetLocationIdByWarehouse(int id)
        {
            int location = _salesProcess.GetLocationIdByWareHouse(id);
            return Json(location, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ApproveSalesProcesed(int id, bool status)
        {
            ResultResponse aResponse = _salesProcess.ApproveSalesOrder(id, status,
                Session[SessionCollection.UserName].ToString());
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetTaxInfo(int productGroupId, decimal amount)
        {
            TaxConfig aTaxConfig = _utilityDal.GetTaxInfo(productGroupId, amount);
            return Json(aTaxConfig, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InvoicePrint()
        {
            // TaxConfig aTaxConfig = _utilityDal.GetTaxInfo(productGroupId, amount);
            // return Json(aTaxConfig, JsonRequestBehavior.AllowGet);
            return View();
        }

        public ActionResult SalesOrderListForPrint(DateTime fromdate, DateTime todate, string salespersonid, int customerid)
        {
            IEnumerable<ViewSalesOrderProcessedMaster> aList = Enumerable.Empty<ViewSalesOrderProcessedMaster>();

            aList = _salesProcess.GetSalesOrderProcessedMasterForInvoice(fromdate, todate, salespersonid, customerid);

            return PartialView("_salesOrderListForPrint", aList);
        }

        public ActionResult ApproveSalesProcesed_cancel(int id)
        {
            ResultResponse aResponse = _salesProcess.ApproveSalesOrder_Cancel(id,
                Session[SessionCollection.UserName].ToString());
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SODelete(int soId)
        {
            ResultResponse aResponse = _salesProcess.SODelete(soId);
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }



        public ActionResult SalesOrderWiseSalesPersonSalesReport(DateTime fromDate, DateTime toDate, int reportType, int summary=0, string salesPerson = "", string customerTypeId = "",int divisionId=0,int areaId=0,int territoryId=0,int marketId=0)
        {
            DataTable dt = new DataTable();
            if (reportType == 1)
            {
                dt = _salesProcess.SalesOrderWiseSalesPersonSalesReport(fromDate, toDate, reportType,  summary, salesPerson, customerTypeId,divisionId,areaId,territoryId,marketId);
            }
            if (reportType == 2 || reportType == 3)
            {
                 dt = _salesProcess.ChallanWiseSalesPersonSalesReport(fromDate, toDate, reportType, summary, salesPerson, customerTypeId, divisionId, areaId, territoryId, marketId);
            }
            if (summary == 1 || summary == 3)
            {
                return PartialView("_PartialSalesPersonSalesReport", dt);
            }
            if (summary == 2)
            {
                return PartialView("_PartialSalesPersonSalesSummary", dt);
            }
            return null;
        }

        public ActionResult CustomerWiseSalesOrderReport()
        {

            return View();
        }



        public ActionResult SalesOrderWiseCustomerSalesReport(DateTime fromDate, DateTime toDate, int reportType, int summary = 0, string customerTypeId = "", int customerId = 0)
        {
            DataTable dt = new DataTable();
            if (reportType == 1)
            {
                dt = _salesProcess.SalesOrderWiseCustomerSalesReport(fromDate, toDate, reportType, summary,customerTypeId, customerId);
            }
            if (reportType == 2 || reportType == 3)
            {
                dt = _salesProcess.ChallanWiseCustomerSalesReport(fromDate, toDate, reportType, summary, customerTypeId, customerId);
            }
            if (summary == 1 || summary == 3)
            {
                return PartialView("_PartialCustomerWiseSalesReport", dt);
            }
            if (summary == 2)
            {
                return PartialView("_PartialCustomerWiseSalesSummary", dt);
            }
            return null;
        }

        public ActionResult SalesPersonSalesReport()
        {
            return View();
        }

        public ActionResult PackWeight(int pid)
        {
            decimal weight = _salesProcess.PackWeight(pid);
            return Json(weight, JsonRequestBehavior.AllowGet);
        }

        public ActionResult QtyForKG(int pid)
        {
            decimal weight = _salesProcess.QtyForKG(pid);
            return Json(weight, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ProductWiseSalesSummery()
        {
            return View();
        }

        public ActionResult ProductWiseSalesSummeryReport(DateTime fromDate, DateTime toDate, string customerTypeId = "",int groupId=0, int divisionId = 0, int areaId = 0, int territoryId = 0, int marketId = 0,int customerId=0,int productId=0,int warehouseId=0)
        {
            DataTable dt =_salesProcess.ProductWiseSalesSummeryReport(fromDate,toDate,customerTypeId,groupId,divisionId,areaId,territoryId,marketId,customerId,productId,warehouseId);
           
                return PartialView("_PertialProductWiseSalesSummery", dt);
        }


        #region new code for approved sales order edit
        public ActionResult SalesOrderApprovalList(DateTime? fromdate, DateTime? todate, string salespersonid, int customerid = 0)
        {
            List<ViewSalesOrderProcessedMaster> aList = _salesProcess.GetSalesOrderProcessedMaster_Approved(fromdate, todate, salespersonid, customerid);


            return View(aList);
        }
        public ActionResult SalesOrderApprovalList_Search(DateTime? fromdate, DateTime? todate, string salespersonid, int customerid = 0)
        {
            List<ViewSalesOrderProcessedMaster> aList = _salesProcess.GetSalesOrderProcessedMaster_Approved(fromdate, todate, salespersonid, customerid);


            return Json(aList, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult SalesPersonWiseCustList()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult SalesPersonSalesDetailsReport()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult SalesOrderWiseSalesPersonSalesDetailsReport(DateTime fromDate, DateTime toDate, int reportType, int summary = 0, string salesPerson = "", string customerTypeId = "", int divisionId = 0, int areaId = 0, int territoryId = 0, int marketId = 0)
        {
            DataTable dt = new DataTable();
            if (reportType == 1)
            {
                dt = _salesProcess.SalesOrderWiseSalesPersonSalesDetailsReport(fromDate, toDate, summary, salesPerson, customerTypeId, divisionId, areaId, territoryId, marketId);
            }
            if (reportType == 2)
            {
                dt = _salesProcess.ChallanWiseSalesPersonSalesDetailsReport(fromDate, toDate, summary, salesPerson, customerTypeId, divisionId, areaId, territoryId, marketId);
            }
            if (summary == 0)
            {
                return PartialView("_PartialSalesPersonSalesDetailsReport", dt);
            }
            if (summary == 1)
            {
                return PartialView("_PartialSalesPersonSalesSummary", dt);
            }
            return null;
        }

        public ActionResult SalesPersonSalesCollectionDueReport()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult SalesPersonSalesCollectionDue(DateTime fromDate, DateTime toDate,string salesPerson = "", string customerTypeId = "", int divisionId = 0, int areaId = 0, int territoryId = 0,int marketId = 0)
        {
            DataTable dt = _salesProcess.SalesPersonSalesCollectionDue(fromDate, toDate, salesPerson, customerTypeId, divisionId, areaId, territoryId, marketId);
            return PartialView("_PartialSalesPersonSalesCollectionDueReport", dt);
        }

        public ActionResult CustomerTypeWiseCollectionDueSummary()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult CustomerTypeSalesCollectionDue(DateTime fromDate, DateTime toDate, string customerTypeId = "", int divisionId = 0, int areaId = 0, int territoryId = 0, int marketId = 0, int customerId = 0,int summary=0)
        {
            DataTable dt = _salesProcess.CustomerTypeSalesCollectionDue(fromDate, toDate, customerTypeId, divisionId, areaId, territoryId, marketId, customerId, summary);
            if (summary == 1)
            {
                return PartialView("_PartialCustomerTypeWiseCollectionDueSummary", dt);
            }
             if (summary == 2)
            {
                return PartialView("_PartialCustomerTypeWiseCollectionDueDetails", dt);
            }
            return null;
        }

        public ActionResult ProductWiseSalesChallanDelivery()
        {
            return View();
        }

        public ActionResult ProductWiseSalesChallanDeliveryReport(DateTime fromDate, DateTime toDate, string customerTypeId = "", int groupId = 0, int divisionId = 0, int areaId = 0, int territoryId = 0, int marketId = 0, int customerId = 0, int productId = 0, int warehouseId = 0)
        {
            DataTable dt = _salesProcess.ProductWiseSalesChallanDeliveryReport(fromDate, toDate, customerTypeId, groupId, divisionId, areaId, territoryId, marketId, customerId, productId, warehouseId);

            return PartialView("_PertialProductWiseSalesChallanDelivery", dt);
        }

        public ActionResult GetPolicyDetails(int groupId, int customertype, int customer, int ProductId)
        {
            List<PolicyDetails> aDetailsSales = _salesProcess.GetPolicyDetails(groupId, customertype, customer, ProductId);
        //public ActionResult GetPolicyDetails(int groupId, int customertype,int customer)
        //{
        //    List<PolicyDetails> aDetailsSales = _salesProcess.GetPolicyDetails(groupId, customertype, customer);
            return Json(aDetailsSales, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetSpecialOffers(int customertype, DateTime orderDate)
        {
            SpecialOffers alist = _salesProcess.GetSpecialOffers(customertype, orderDate);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddRowForOrder(int tr, int customertype)
        {
            TempData["trSl"] = tr;
            ViewBag.customertype = customertype;
            return PartialView("_addNewOrderRow");
        }

        public ActionResult GetCustomerType()
        {
            List<CustomerType> aList = _salesProcess.GetCustomerTypeList();
            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CustomerDayToDaySalesCollection()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult CustomerDayToDaySalesCollectionDue(DateTime fromDate, DateTime toDate, int customerId=0, int customerTypeId=0)
        {
            DataTable dt = _salesProcess.CustomerDayToDaySalesCollectionDue(fromDate, toDate, customerId, customerTypeId);
            return PartialView("_PartialCustomerDayToDaySalesCollection", dt);
        }
    }
}
