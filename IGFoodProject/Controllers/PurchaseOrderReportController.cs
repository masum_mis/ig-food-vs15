﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Models.ReportModel;
using IGFoodProject.Utility;

namespace IGFoodProject.Controllers
{
    public class PurchaseOrderReportController : Controller
    {
        // GET: PurchaseOrderReport
        public ActionResult Index()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }

        public ActionResult GetPurchaseReport(int company, int location, int warehouse, int  supplierId, DateTime fromdate, DateTime todate, int rstatus)
        {
            PurchaseOrderReportParameter aList = new PurchaseOrderReportParameter();

            aList.CompanyId = company;
            aList.LocationId = location;
            aList.WarehouseId = warehouse;
            aList.FromDate = fromdate;
            aList.ToDate = todate;
            aList.RStatus = rstatus;
            aList.SupplierId = supplierId;

            ResultResponse aResponse = new ResultResponse();
            BasicInfoReportsParameter aBasicInfoReportsParameter = new BasicInfoReportsParameter();
            aBasicInfoReportsParameter.ReportValue = "PurchaseOrderReport";
            Session[SessionCollection.ReportValue] = aBasicInfoReportsParameter;
            Session[SessionCollection.ReportParam] = aList;
            aResponse.isSuccess = true;
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TradePurchase()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult GetTradePurchaseReport(int company, int location, int warehouse, int supplierId, DateTime fromdate, DateTime todate)
        {
            TradePurchaseDAL aDal= new TradePurchaseDAL();
            DataTable dt = aDal.TradePurchaseOrderReport(company, location, warehouse, supplierId, fromdate, todate);
            return PartialView("_PartialTradePurchaseReport",dt);
        }
    }
}