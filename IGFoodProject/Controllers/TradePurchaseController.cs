﻿using IGFoodProject.DAL;
using IGFoodProject.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.Models;
using IGFoodProject.Utility;

namespace IGFoodProject.Controllers
{
    public class TradePurchaseController : Controller
    {

        TradePurchaseDAL _aDal = new TradePurchaseDAL();
        // GET: TradePurchase
        public ActionResult Index(int TradePurchaseMasterId=0)
        {
            ViewBag.TradePurchaseMasterId = TradePurchaseMasterId;
            return View();
        }
        public ActionResult GetCustomer()
        {
            List<ViewCustomerInfo> aList = _aDal.GetCustomerList();
            return Json(aList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LoadProduct()
        {
            List<ViewProductDescription> alist = _aDal.LoadProduct();
            return Json(alist, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAddRow(int tr)
        {
            TempData["trSl"] = tr;
            return PartialView("_partialTradeProduct");

        }
        public JsonResult Loadunit(int productid)
        {
            tbl_UOM alist = _aDal.Loadunit(productid);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveTradePurchase(TradePurchaseMaster aMaster)
        {

            ResultResponse aResponse = _aDal.SaveTradePurchase(aMaster,
                Session[SessionCollection.UserName].ToString());
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
        public ActionResult TradePurchaseList(DateTime? purchasedate,  int customerid=0, int supplierid = 0, int warehouseid=0)
        {
            List<ViewTradePurchaseList> aList = _aDal.GetTradePurchaseMaster(purchasedate,  customerid, supplierid,  warehouseid);


            return View(aList);
        }
        public ActionResult TradePurchaseList_Search(DateTime? purchasedate, int customerid = 0, int supplierid = 0, int warehouseid = 0)
        {
            List<ViewTradePurchaseList> aList = _aDal.GetTradePurchaseMaster(purchasedate, customerid, supplierid, warehouseid);


            return Json(aList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetTradeMaster(int id)
        {
            TradePurchaseMaster aMaster = _aDal.GetTradePurchaseMaster(id);
            aMaster.adetail = _aDal.GetTradePurchaseDetailsByMaster(id);
            return Json(aMaster, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TradePurchaseListForApprove(DateTime? purchasedate, int customerid = 0, int supplierid = 0, int warehouseid = 0)
        {
            List<ViewTradePurchaseList> aList = _aDal.GetTradePurchaseMasterListForApprove(purchasedate, customerid, supplierid, warehouseid);


            return View(aList);
        }
        public ActionResult TradePurchaseListForApprove_Search(DateTime? purchasedate, int customerid = 0, int supplierid = 0, int warehouseid = 0)
        {
            List<ViewTradePurchaseList> aList = _aDal.GetTradePurchaseMasterListForApprove(purchasedate, customerid, supplierid, warehouseid);


            return Json(aList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult TradeApprove(int TradePurchaseMasterId = 0)
        {
            ViewBag.TradePurchaseMasterId = TradePurchaseMasterId;
            return View();
        }
        public ActionResult GetAddRowApprove(int tr)
        {
            TempData["trSl"] = tr;
            return PartialView("_partialTradeProductApprove");

        }
        [HttpPost]
        public ActionResult SaveTradePurchaseApprove(TradePurchaseMaster aMaster)
        {

            ResultResponse aResponse = _aDal.SaveTradePurchaseApprove(aMaster,
                Session[SessionCollection.UserName].ToString());
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
    }
}