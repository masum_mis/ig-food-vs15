﻿using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IGFoodProject.Controllers
{
    public class StockAdjustmentController : Controller
    {
        // GET: StockAdjustment

        StockAdjustmentDAL aDal = new StockAdjustmentDAL();

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult DirectStockOutEntry()
        {
            //string loginUserEmpId = Session["loginuserempid"] as string;

            //if (string.IsNullOrEmpty(loginUserEmpId))
            //{
            //    return RedirectToAction(nameof(HomeController.Login), "Home");
            //}
            //else
            //{
                return View();
            //}
            
        }

        public ActionResult GetProductStockDetailsbyStockInDate(int productId)
        {
           
            DataTable dt = aDal.GetProductStockDetailsbyStockInDate(productId);

            return PartialView("_dateWiseProductStockDetailsList", dt);

        }

        public ActionResult SaveDirectStockOut(StockAdjustmentMaster adjustmentMaster)
        {
            string UserID = string.Empty;
            ResultResponse result = new ResultResponse();
            if (Session[SessionCollection.LoginUserEmpId] != null)
            {
                UserID = Session[SessionCollection.LoginUserEmpId].ToString();
                result.isSuccess = aDal.SaveDirectStockOut(adjustmentMaster, UserID);
            }
            else
            {
                result.isSuccess = false;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #region live bird stock out
        public ActionResult DirectStockOutEntry_LiveBird()
        {
            //string loginUserEmpId = Session["loginuserempid"] as string;

            //if (string.IsNullOrEmpty(loginUserEmpId))
            //{
            //    return RedirectToAction(nameof(HomeController.Login), "Home");
            //}
            //else
            //{
            return View();
            //}

        }

        public ActionResult GetProductStockDetailsbyStockInDate_LiveBird(int productId)
        {

            DataTable dt = aDal.GetProductStockDetailsbyStockInDate_LiveBird(productId);

            return PartialView("_dateWiseLiveBirdStockDetailsList", dt);

        }

        public ActionResult SaveDirectStockOut_LiveBird(StockAdjustmentMaster adjustmentMaster)
        {
            string UserID = string.Empty;
            ResultResponse result = new ResultResponse();
            if (Session[SessionCollection.LoginUserEmpId] != null)
            {
                UserID = Session[SessionCollection.LoginUserEmpId].ToString();
                result.isSuccess = aDal.SaveDirectStockOut_LiveBird(adjustmentMaster, UserID);
            }
            else
            {
                result.isSuccess = false;
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion


    }
}