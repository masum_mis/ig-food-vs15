﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.ViewModel;
using IGFoodProject.Models;
using IGFoodProject.Utility;

namespace IGFoodProject.Controllers
{
    public class SupplierPaymentOtherController : Controller
    {
        SupplierPaymentOtherDAL dal = new SupplierPaymentOtherDAL();
        // GET: SupplierPaymentOther
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult InvoiceListForPayment(int supplierId)
        {
            List<ViewInvoiceMaster> aList = dal.InvoiceListForPayment(supplierId);
            return PartialView("_partialInvoicePayment", aList);
        }
        public ActionResult SaveSupplierPayment(SupplierPaymentOtherMaster supplierPaymentOtherMaster)
        {
            ResultResponse aResponse = new ResultResponse();

            supplierPaymentOtherMaster.CreateBy = Session[SessionCollection.UserName].ToString();
            aResponse.isSuccess = dal.SaveSupplierPaymentDetails(supplierPaymentOtherMaster);
            return Json(aResponse, JsonRequestBehavior.AllowGet);

        }
        public ActionResult SupplierVoucherLedger()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult GetSupplierVoucherLedger(DateTime fromDate, DateTime toDate, int glId, int refDetailsId = 0)
        {
            DataTable aList = dal.GetSupplierVoucherLedger(fromDate, toDate, glId, refDetailsId);
            return PartialView("_PartialSupplierVoucherLedger", aList);
        }

        public ActionResult SupplierTdsVoucherLedger()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult GetSupplierTdsVoucherLedger(DateTime fromDate, DateTime toDate, int glId, int refDetailsId = 0, int sourchId = 0)
        {
            DataTable aList = dal.GetSupplierTdsVoucherLedger(fromDate, toDate, glId, refDetailsId, sourchId);
            return PartialView("_PartialSupplierTdsVoucherLedger", aList);
        }

        public ActionResult SupplierVdsVoucherLedger()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult GetSupplierVdsVoucherLedger(DateTime fromDate, DateTime toDate, int glId, int refDetailsId = 0, int sourchId = 0)
        {
            DataTable aList = dal.GetSupplierTdsVoucherLedger(fromDate, toDate, glId, refDetailsId, sourchId);
            return PartialView("_PartialSupplierVdsVoucherLedger", aList);
        }
    }
}