﻿using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IGFoodProject.Controllers
{
    public class IssueQCController : Controller
    {
        // GET: IssueQC

        IssueQCDAL _aDal = new IssueQCDAL();
        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult ReturnItemList(int warehouseId, DateTime returnDate) 
        {
            DataTable aList = _aDal.GetQcRequestedItemList(warehouseId, returnDate); 
            return PartialView("_partialQCRequestItemList", aList);
        }

        public ActionResult QCRequestedItemList() 
        {
            return View();
        }
        public ActionResult QCRequestedItemListPartial( DateTime? returnDate, int warehouseId = 0)  
        {
            DataTable aList = _aDal.QCRequestedItemListPartial(returnDate, warehouseId); 
            return PartialView("_partialQCRequestedReturnItemList", aList);
        }        
        public ActionResult SaveIssueRequest(OtherStockIssueMaster entity)
        {
            try
            {
                string entryBy = Session[SessionCollection.UserName].ToString();
                ResultResponse aResponse = _aDal.SaveIssueRequest(entity, entryBy); 
                return Json(aResponse, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public ActionResult RejectRequest(List<OtherStockIssueDetails> aMaster)
        {
            try
            {
                string entryBy = Session[SessionCollection.UserName].ToString();
                ResultResponse aResponse = _aDal.RejectRequest(aMaster, entryBy);
                return Json(aResponse, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw ex;
            }
         
        }
                
        public ActionResult ItemQcOperation()
        {
            return View();
        }

        public ActionResult GetReturnNo(DateTime returnDate) 
        {
            List<OtherStockIssueMaster> aList = _aDal.GetReturnNo(returnDate);
            return Json(aList, JsonRequestBehavior.AllowGet); 
        }

        public ActionResult QcOperationItemList(int warehouseId, int issueReturnID, DateTime returnDate) 
        {
            DataTable aList = _aDal.GetRemainingQcRequestedItemList(warehouseId, issueReturnID, returnDate);  
            return PartialView("_partialQCOperationItemList", aList);
        }

        public ActionResult SaveQcOperation(OtherStockIssueMaster entity) 
        {
            try
            {
                ResultResponse aResponse = _aDal.SaveQcOperation(entity); 
                return Json(aResponse, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public ActionResult GetQcItemHistoryOfQty(int IssueReturnDetailID, int ItemId) 
        {
            DataTable aList = _aDal.GetQcItemHistoryOfQty(IssueReturnDetailID, ItemId);  
            return PartialView("_partialItemHistoryQty", aList);
        }

        // Approval

        public ActionResult ItemQcOperationApproval() 
        {
            return View();
        }

        public ActionResult QcOperationApprovalList(int warehouseId, DateTime qcDate ,  int returnId) 
        {
            DataTable aList = _aDal.QcOperationApprovalList(warehouseId, qcDate, returnId);
            return PartialView("_partialQcOperationApprovalList", aList);
        }

        //public ActionResult SaveQcApproval(List<OtherStockIssueMaster> aMaster)  
        //{
        //    try
        //    {
        //        string entryBy = Session[SessionCollection.UserName].ToString();
        //        ResultResponse aResponse = _aDal.SaveQcApproval(aMaster, entryBy); 
        //        return Json(aResponse, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }

        //}
        public ActionResult SaveQcApproval(OtherStockIssueMaster aMaster)
        {
            try
            {
                string entryBy = Session[SessionCollection.UserName].ToString();
                ResultResponse aResponse = _aDal.SaveQcApproval(aMaster, entryBy);
                return Json(aResponse, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

    }
}