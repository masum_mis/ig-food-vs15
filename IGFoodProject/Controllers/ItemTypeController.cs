﻿using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IGFoodProject.Controllers
{
    public class ItemTypeController : Controller
    {
        ItemTypeDAL aDal = new ItemTypeDAL();
        // GET: ItemType
        public ActionResult AddItemType(int id = 0)
        {
            ViewBag.ItemTypeId = id;
            return View();
        }
        public ActionResult ItemTypeList()
        {
            List<ItemType> alist = aDal.GetItemTypeList();
            return View(alist);
        }
        public JsonResult LoadItemGroup()
        {
            return Json(aDal.GetItemGroupList(), JsonRequestBehavior.AllowGet); 
        }

        public JsonResult CheckItemType(string ptype) 
        {   
            int type = aDal.CheckItemType(ptype); 
            return Json(type, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveItemType(ItemType aType)  
        {
            ResultResponse aResponse = new ResultResponse();

            string entryBy = Session[SessionCollection.UserName].ToString();
            aType.CreateBy = entryBy;
            aType.UpdateBy = entryBy;
            aResponse.isSuccess = aDal.SaveItemType(aType);

            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteItemType(int id)
        {
            ResultResponse aResponse = new ResultResponse();
            aResponse.isSuccess = aDal.DeleteItemType(id); 
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetItemTypeByID(int id) 
        {
            return Json(aDal.GetItemTypeByID(id), JsonRequestBehavior.AllowGet);
        }

    }
}