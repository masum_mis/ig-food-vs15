﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.Utility;
using IGFoodProject.DAL;
using IGFoodProject.Models;

namespace IGFoodProject.Controllers
{
    public class ProductGroupController : Controller
    {
        // GET: ProductGroup
        public ActionResult ProductGroupList()
        {
            ProductDAL aDal = new ProductDAL();
            List<tbl_ProductGroup> alist = aDal.GetGroupList();
            return View(alist);
        }
        public ActionResult AddGroup(int id = 0)
        {
            ViewBag.groupid = id;
            return View();
        }
        public JsonResult CheckGroup(string group, string gcode)
        {

            ProductDAL aDAL = new ProductDAL();

            int desig = aDAL.CheckGroup(group, gcode);

            return Json(desig , JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveGroup(tbl_ProductGroup aGroup)
        {
            ResultResponse aResponse = new ResultResponse();
            ProductDAL aDAL = new ProductDAL();

            string entryBy = Session[SessionCollection.UserName].ToString();
            aGroup.CreateBy = entryBy;
            aGroup.UpdateBy = entryBy;
            aResponse.isSuccess = aDAL.SaveGroup(aGroup);

            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetGroupByID(int id)
        {
            ProductDAL aDal = new ProductDAL();
            return Json(aDal.GetGroupForEdit(id), JsonRequestBehavior.AllowGet);

        }
        public JsonResult DeleteGroup(int id)
        {
            ResultResponse aResponse = new ResultResponse();
            ProductDAL aDAL = new ProductDAL();


            aResponse.isSuccess = aDAL.DeleteGroup(id);

            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
    }
}