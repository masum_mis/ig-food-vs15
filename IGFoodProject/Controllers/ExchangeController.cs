﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;

namespace IGFoodProject.Controllers
{
    public class ExchangeController : Controller
    {
        ExchangeDAL aDal = new ExchangeDAL();
        // GET: Exchange
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ExchangeList() 
        {
            string empId = Session[SessionCollection.LoginUserEmpId].ToString();
            List<ItemExchange> aList = aDal.ExchangeList(empId);
            return View(aList);
        }

        public ActionResult ExchangeApprove()
        {
            List<ItemExchange> aList = aDal.ExchangeApproveList(); 
            return View(aList);
        }
        
        public JsonResult LoadItemGroup()
        {
            List<ItemExchange> alist = aDal.LoadItemGroup();
            return Json(alist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadItem( int groupId)
        {
            List<ItemExchange> alist = aDal.LoadItem(groupId);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadProductroup()
        {
            List<ItemExchange> alist = aDal.LoadProductroup(); 
            return Json(alist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadProduct(int groupId) 
        {
            List<ItemExchange> alist = aDal.LoadProduct(groupId);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadStockQty(int warehouseId, int groupId, int itemId) 
        {
            ItemExchange exchange = aDal.LoadStockQty(warehouseId,groupId, itemId); 
            return Json(exchange, JsonRequestBehavior.AllowGet); 
        }

        public ActionResult SaveExchangeRequisition(ItemExchange aMaster)  
        {
            aMaster.CreateBy = Session[SessionCollection.UserName].ToString();
            ResultResponse aResponse = aDal.SaveExchangeRequisition(aMaster); 
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveAccept(decimal appQty, int exchangeId)
        {
            string empId = Session[SessionCollection.UserName].ToString();
            ResultResponse aResponse = aDal.SaveAccept(appQty, exchangeId, empId);
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }


    }
}
