﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.Utility;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.ViewModel;
 namespace IGFoodProject.Controllers
{
    public class MrRController : Controller
    {
        // GET: MrR
        MrRDAL _aDal = new MrRDAL();
        public ActionResult Index(int id = 0 , string flag="")
        {
            if (id > 0)
            {
                ViewBag.MrrMasterId = id;
                ViewBag.Flag = flag;
            }
            return View();
        }
        public JsonResult GetMrrMaster(int id)
        {
            MrRMaster aMaster = _aDal.GetMrRMaster(id);
            return Json(aMaster,JsonRequestBehavior.AllowGet);
        }
         public ActionResult GetMrrMasterView(int id)
        {
            ViewMrrMaster aMaster = _aDal.GetMrRMasterView(id);
            return Json(aMaster, JsonRequestBehavior.AllowGet);
        }
         public PartialViewResult GetPurchaseOrderMasterForMrrEdit(int id)
        {
            ViewPurchaseOrderMaster aMaster = _aDal.GetPurchaseOrderMasterView(id);
            return PartialView("_parchaseOrderMaster", aMaster);
        }
         public JsonResult LoadPurchaseOrder(int locationId, int warehouseId)
        {
            List<ViewPurchaseOrderDDL> aMasterList = _aDal.GetRemainingPurchaseOrder( locationId,  warehouseId);
             return Json(aMasterList,JsonRequestBehavior.AllowGet);
         }
         public PartialViewResult LoadMrrDetails(int id)
        {
            List<ViewMrrDetails> aList = _aDal.GetMrrDetailsForEdit(id);
            return PartialView("_mrrDetails", aList);
        }
         public PartialViewResult GetPurchaseOrder(int id)
        {
            ViewPurchaseOrderMaster aMaster = _aDal.GetPurchaseOrderMasterView(id);
            aMaster.Details = _aDal.GetPurchaseOrderDetailsView(id);
            return PartialView("_parchaseOrderDetails", aMaster);
        }
         [HttpPost]
        public JsonResult SaveMrr( MrRMaster aMaster)
        {
            ResultResponse aResponse = _aDal.SaveMrrMaster(aMaster, Session[SessionCollection.UserName].ToString());
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
         public JsonResult GetMrrNo(int pk)
        {
            MrRMaster aMaster = _aDal.GetMrRMaster(pk);
            return Json(aMaster.MRRNo, JsonRequestBehavior.AllowGet);
        }
         public ActionResult MrrList()
        {
            //List<ViewMrrMaster> aList = _aDal.GetMrrList();
           // return View(aList);
            return View();
        }
        public ActionResult MrrCloseList()
        {
            //List<ViewMrrMaster> aList = _aDal.GetMrrList();
            // return View(aList);
            return View();
        }

        
         public ActionResult MrrReport()
        {
            return View();
        }
         public ActionResult GetMrrReportList(DateTime fromDate,DateTime toDate, int warehouse)
        {
            List<ViewMrrMaster> aMasters = _aDal.GetMrrListForReport(fromDate, toDate, warehouse);
            return PartialView("_PartialMrrReports", aMasters);
        }
        public ActionResult GetMrrListByDate(DateTime? fromdate, DateTime? todate, string supplierName = "", string PONo = "")
        {
            List<ViewMrrMaster> aList = _aDal.GetMrrListByDate(fromdate, todate, supplierName, PONo);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }
        public PartialViewResult ViewMrrDetails(int id = 0)
        {
            List<ViewMrrDetails> aList = _aDal.GetMrrDetailsForEdit(id);
            return PartialView("_PartialMrrDetailsReport", aList);
        }
        public ActionResult PrintMrrInvoice()
        {
            List<ViewMrrMaster> aList = _aDal.GetMrrList();
            return View(aList);
        }
        public ActionResult GetMrrListForPrint(int warehouseId, DateTime fromDate, DateTime toDate)
        {
            List<ViewMrrMaster> aList = _aDal.GetMrrListForPrintInvoice(warehouseId,fromDate,toDate);
            return PartialView("_PartialMrrPrintInvoice", aList);
        }
        public JsonResult LoadSupplier(int PONo)
        {
            List<tbl_SupplierInfo> alist = _aDal.SupplierInformation(PONo);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LoadPO(string fromdate, string todate)
        {
            List<ViewMrrMaster> alist = _aDal.LoadPO(fromdate, todate);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CloseMRR(int id)
        {
            ResultResponse aResponse = new ResultResponse();

            ViewMrrMaster aorder = new ViewMrrMaster();

            string entryBy = Session[SessionCollection.UserName].ToString();
            aorder.MRRMasterId = id;
            aorder.ClosedBy = entryBy;
            aorder.ClosedDate = DateTime.Now.ToString();
            aResponse.isSuccess = _aDal.CloseMRR(aorder);

            return RedirectToAction(nameof(MrRController.MrrCloseList), "MrR"); 
        }
        public ActionResult GetMrrCloseListByDate(DateTime? fromdate, DateTime? todate, string supplierName = "", string PONo = "")
        {
            List<ViewMrrMaster> aList = _aDal.GetMrrCloseListByDate(fromdate, todate, supplierName, PONo);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }
    }
}