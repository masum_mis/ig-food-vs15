﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Models.ReportModel;
using IGFoodProject.Utility;

namespace IGFoodProject.Controllers
{
    public class StockOutController : Controller
    {
        private StockOutDal _aDal = new StockOutDal();

        // GET: StockOut
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult IssueDetailsById(int id)
        {
            List<OtherStockIssueDetails> aPo = _aDal.IssueDetailsById(id);
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetIssueList(DateTime? fromdate, DateTime? todate, int warehouse = 0, string reqById = null)
        {
            List<OtherStockIssueMaster> aList = _aDal.IssueList(fromdate, todate, warehouse, reqById);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }
        public ActionResult PendingIssueList()
        {
            return View();
        }
        public ActionResult GetPendingIssueList(DateTime? fromdate, DateTime? todate, int warehouse = 0, string reqById = null)
        {
            List<IssueRequisition> aList = _aDal.GetPendingIssueList(fromdate, todate, warehouse, reqById);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }
        public ActionResult Issue()
        {
            return View();
        }

        public ActionResult MaterialIssueSummaryReport()
        {
            return View();
        }

        public ActionResult LoadMaterialIssueSummary(string stockMonth, int departmentId, int locationId, int wareHouseId, int groupId)
        {
            List<OtherStockIssueDetails> aList = _aDal.loadMaterialIssueSummaryReport(stockMonth, departmentId, locationId, wareHouseId, groupId);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }
        public ActionResult LoadMaterialIssueDetails(string stockMonth, int departmentId, int locationId, int wareHouseId, int groupId, int ItemId)
        {
            List<OtherStockIssueDetails> aList = _aDal.LoadMaterialIssueDetails(stockMonth, departmentId, locationId, wareHouseId, groupId, ItemId);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }

        public ActionResult IssueItems(int id)
        {

            ViewBag.MasterId = id;

            return View();
        }
        public ActionResult GetItemDetails(int itemId,int warehouseId)
        {
            OtherStockIssueDetails aDetails = _aDal.GetItemDetails(itemId,warehouseId);
            return Json(aDetails, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveStockIssue(OtherStockIssueMaster aMaster)
        {
            aMaster.CreateBy = Session[SessionCollection.UserName].ToString();
            ResultResponse aResponse = _aDal.SaveStockIssue(aMaster);
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        #region IssueRequisition
        public ActionResult RequisitionList()
        {
            return View();
        }
        public ActionResult AddRowForIssue(int tr, string eflag = "ed")
        {
            TempData["trSl"] = tr;
            ViewBag.eflag = eflag;
            return PartialView("_addNewGLRow");
        }
        public ActionResult IssueRequisition(int RequisitionId = 0, string eflag = "ed")
        {
            ViewBag.RequisitionId = RequisitionId;
            ViewBag.eflag = eflag;
            return View();
        }
        public ActionResult SaveIssueRequisition(IssueRequisition aMaster)
        {
            aMaster.CreateBy = Session[SessionCollection.UserName].ToString();
            ResultResponse aResponse = _aDal.SaveIssueRequisition(aMaster);
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
        public JsonResult RequisitionDetailsById(int id)
        {
            List<IssueRequisitionDetails> aPo = _aDal.RequisitionDetailsById(id);
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetRequisitionList(DateTime? fromdate, DateTime? todate, int warehouse = 0, string reqById = null)
        {
            List<IssueRequisition> aList = _aDal.IssueRequisitionList(fromdate, todate, warehouse, reqById);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetRequisitionByMasterId(int id)
        {
            IssueRequisition aMaster = _aDal.GetIssueRequisitionMasterById(id);
            aMaster.IssueRequisitionDetailses = _aDal.RequisitionDetailsByIdForIssue(id);
          
            return Json(aMaster, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RequisitionApprovalList()
        {
        
            return View();
        }
        public ActionResult GetRequisitionApprovalList(DateTime? fromdate, DateTime? todate, int warehouse = 0, string reqById = null)
        {
            List<IssueRequisition> aList = _aDal.IssueRequisitionApproveList(fromdate, todate, warehouse, reqById);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetRequisitionPurpose()
        {
            List<RequisitionPurpose> aList = _aDal.GetRequisitionPurpose();
            return Json(aList, JsonRequestBehavior.AllowGet);

        }
        #endregion

        public ActionResult IssueRequisitionReport()
        {
            return View();
        }
        public ActionResult GetRequisitionReport(int location, int warehouse, DateTime fromdate, DateTime todate, int rstatus)
        {
            IssueRequisition aList = new IssueRequisition();


            aList.LocationId = location;
            aList.WarehouseId = warehouse;
            aList.FromDate = fromdate;
            aList.ToDate = todate;
            aList.RStatus = rstatus;

            ResultResponse aResponse = new ResultResponse();
            BasicInfoReportsParameter aBasicInfoReportsParameter = new BasicInfoReportsParameter();
            aBasicInfoReportsParameter.ReportValue = "IssueRequisitionReport";
            Session[SessionCollection.ReportValue] = aBasicInfoReportsParameter;
            Session[SessionCollection.ReportParam] = aList;
            aResponse.isSuccess = true;
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
        public ActionResult IssueRequisitionList()
        {

            return View();
        }
        public ActionResult ShowRequisitionApprovedList(DateTime fromDate, DateTime toDate, int warehouse, string reqById)
        {
            List<IssueRequisition> aList = _aDal.IssueRequisitionPrintList(fromDate, toDate, warehouse, reqById);
            return PartialView("_requisitionInvoiceList", aList);
        }
        public ActionResult RequisitionSlipReport(RequisitionReport aSalesOrderReport)
        {
            ResultResponse aResponse = new ResultResponse();
            BasicInfoReportsParameter aBasicInfoReportsParameter = new BasicInfoReportsParameter();
            aBasicInfoReportsParameter.ReportValue = "IssueRequisitionSlip";
            Session[SessionCollection.ReportValue] = aBasicInfoReportsParameter;
            Session[SessionCollection.ReportParam] = aSalesOrderReport;
            aResponse.isSuccess = true;
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }



        public ActionResult IssueList()
        {

            return View();
        }
        public ActionResult ShowIssueList(DateTime fromDate, DateTime toDate, int warehouse, string reqById)
        {
            List<OtherStockIssueMaster> aList = _aDal.IssueList(fromDate, toDate, warehouse, reqById);
            return PartialView("_issueList", aList);
        }
        public ActionResult IssueSlipReport(RequisitionReport aSalesOrderReport)
        {
            ResultResponse aResponse = new ResultResponse();
            BasicInfoReportsParameter aBasicInfoReportsParameter = new BasicInfoReportsParameter();
            aBasicInfoReportsParameter.ReportValue = "IssuePrint";
            Session[SessionCollection.ReportValue] = aBasicInfoReportsParameter;
            Session[SessionCollection.ReportParam] = aSalesOrderReport;
            aResponse.isSuccess = true;
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Reject(int id)
        {
            var aPo = _aDal.Reject(id);
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }

        // For Issue Return
        public ActionResult ReturnIssue()
        {          
            return View();
        }
        public JsonResult GetDetailsForMrrReturn(int id)
        {
            OtherStockIssueMaster aMaster = _aDal.GetIssueInfoByMasterById(id);
            aMaster.StockIssueDetails = _aDal.IssueDetailsForReturnById(id); 
            return Json(aMaster, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReturnIssueItems(int id) 
        {
            ViewBag.MasterId = id;
            return View();
        }

        public ActionResult SaveIssueReturn(OtherStockIssueMaster entity)
        {
            try
            {
                string entryBy = Session[SessionCollection.UserName].ToString();
                ResultResponse aResponse = _aDal.SaveIssueReturn(entity, entryBy);
                return Json(aResponse, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public ActionResult ReturnedIssueList(DateTime? fromdate, DateTime? todate, int WareHouseId = 0, int LocationId = 0)  
        {
            List<OtherStockIssueMaster> aList = _aDal.ReturnedIssueList(fromdate, todate, WareHouseId, LocationId);
            return View(aList);
        }

        public ActionResult SearchForReturned(DateTime? fromdate, DateTime? todate, int WareHouseId = 0, int LocationId = 0)
        {
            List<OtherStockIssueMaster> aList = _aDal.ReturnedIssueList(fromdate, todate, WareHouseId, LocationId);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetLocationByWarehouseId(int warehouseId)  
        {
            List<OtherStockIssueMaster> aList = _aDal.GetLocationByWarehouseId(warehouseId); 
            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetReturnedDetailsById(int id)
        {
            List<OtherStockIssueDetails> aPo = _aDal.GetReturnedDetailsById(id);
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }

    }
}