﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Models.Accounts;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;

namespace IGFoodProject.Controllers
{
    public class UtilityController : Controller
    {
        // GET: Utility
        UtilityDAL _utilityDal = new UtilityDAL();
        public  JsonResult LoadCompany()
        {
            List<CompanyInformation> companies =  _utilityDal.CompanyInformation();
            var asd = Json(companies);
            return Json(companies,JsonRequestBehavior.AllowGet);
        }
        public JsonResult LoadVehicle()
        {
            List<VehicleInfo> vehicle = _utilityDal.VehicleInfo();
            return Json(vehicle, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LoadLocation(int comid)
        {
            List<Location> alist = _utilityDal.LocationInformation(comid);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LoadWareHouse(int locid=0,int wareHouseTypeid=0)
        {
            int loginId = Convert.ToInt32(Session[SessionCollection.LoginUserId]);
            List<WareHouse> alist = _utilityDal.WareHouseInformation(loginId,locid, wareHouseTypeid);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadWareHouseMultipleType(int locid, string wareHouseTypeid)
        {
            int loginId = Convert.ToInt32(Session[SessionCollection.LoginUserId]);
            List<WareHouse> alist = _utilityDal.WareHouseInformationMultipleType(loginId, locid, wareHouseTypeid);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadItemDescription(int itemCatagoryId = 0)
        {
            List<ItemDescription> itemDescription = _utilityDal.ItemDescriptions(itemCatagoryId);
            return Json(itemDescription,JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetEmpAutoComAll(string term)
        {
            try
            {

                UtilityDAL aUtilityDal = new UtilityDAL();
                List<AutoComplete> lst = aUtilityDal.GetEmpAutoComDALAll(term);

                return Json(lst , JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public JsonResult GetEmpNo(string empId)
        {
            UtilityDAL aDal = new UtilityDAL();
            return Json(aDal.GetEmpNo(empId),JsonRequestBehavior.AllowGet);
        }

       

        public ActionResult GetDivision()
        {
            List<Division> aList = _utilityDal.GetDivision();
            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetArea(int division=0)
        {
            List<Area> aList = _utilityDal.GetArea(division);
            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetTerritory(int area=0)
        {
            List<Territory> aList = _utilityDal.GetTerritory(area);
            return Json(aList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetMarket(int territory=0)
        {
            List<Market> aList = _utilityDal.GetMarket(territory);
            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDistrict(int division=0)
        {
            List<District> aList = _utilityDal.GetDistricts(division);
            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetThana(int district=0)
        {
            List<Thana> aList = _utilityDal.GetThanas(district);
            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCustomerType()
        {
            List<CustomerType> aList = _utilityDal.GetCustomerType();

            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPaymentType()
        {
            List<CustomerPaymentType> aList = _utilityDal.GetCustomerPaymentType();
            return Json(aList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetSupplierPaymentType()
        {
            List<CustomerPaymentType> aList = _utilityDal.GetSupplierPaymentType();
            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetProductByGroup(int groupId)
        {

            List<Product> aProducts = _utilityDal.GetProductByGroupId(groupId);
            return Json(aProducts, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetProductRetailByGroupId(int groupId)
        {

            List<Product> aProducts = _utilityDal.GetProductRetailByGroupId(groupId);
            return Json(aProducts, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetProductRestaturantByGroupId(int groupId)
        {

            List<Product> aProducts = _utilityDal.GetProductRestaturantByGroupId(groupId);
            return Json(aProducts, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetSaleableProductByGroup(int groupId)
        {

            List<Product> aProducts = _utilityDal.GetSaleableProductByGroupId(groupId);
            return Json(aProducts, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetNonSaleableProductByGroup(int groupId)
        {

            List<Product> aProducts = _utilityDal.GetNonSaleableProductByGroupId(groupId);
            return Json(aProducts, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetProductByGroup2(int groupId =0)
        {

            List<Product> aProducts = _utilityDal.GetProductByGroupId2(groupId);
            return Json(aProducts, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetEmployeeName(string empId)
        {
            return Json(_utilityDal.GetEmployeeName(empId), JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadItemDescriptionForMRRReceive(string mrrdate)
        {
            List<ItemDescription> itemDescription = _utilityDal.ItemDescriptionsForReceive(mrrdate);
            return Json(itemDescription, JsonRequestBehavior.AllowGet);
        }
        

        public ActionResult GetBankName()
        {
            List<BankMaster> aList = _utilityDal.GetBankName();
            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetBankBranch(int bankid)
        {
            List<BankBranch> aList = _utilityDal.GetBankBranch(bankid);
            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetProductGroup(int productId)
        {

            List<Product> aProducts = _utilityDal.GetProductGroupId(productId);
            return Json(aProducts, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDeliveryManAutoComAll(string term)
        {
            try
            {

                UtilityDAL aUtilityDal = new UtilityDAL();
                List<AutoComplete> lst = aUtilityDal.GetDeliveryManAutoComDALAll(term);

                return Json(lst, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        public ActionResult GetEmployee()
        {
            List<View_EmployeeInfo> aList = _utilityDal.GetEmployee();
            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetReason(int reasonFor)
        {
            List<tbl_Reason> aList = _utilityDal.GetReason(reasonFor);
            return Json(aList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LoadBusinessUnit(int companyId)
        {
            List<BusinessUnit> bu = _utilityDal.BusinessUnitByCompanyId(companyId);
            var asd = Json(bu);
            return Json(bu, JsonRequestBehavior.AllowGet);
        }
 
        public ActionResult GetFinancialYear()
        {
            List<FinancialYear> fy = _utilityDal.GetFinancialYear();
            return Json(fy, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetCurrency()
        {
            List<Currency> cu = _utilityDal.GetCurrency();
            return Json(cu, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetDebitVoucherGL()
        {
            List<ChartOfAccLayerSeven> cu = _utilityDal.GetDebitVoucherGL();
            return Json(cu, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetGLBankAccounts()
        {
            List<BankAccount> cu = _utilityDal.GetGLBankAccounts();
            return Json(cu, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetGLCashAccounts()
        {
            List<CashAccount> cu = _utilityDal.GetGLCashAccounts();
            return Json(cu, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetGLForJournal()
        {
            List<ChartOfAccLayerSeven> cu = _utilityDal.GetGLForJournal();
            return Json(cu, JsonRequestBehavior.AllowGet);
        }
    public ActionResult GetGLForContra()
        {
            List<ChartOfAccLayerSeven> cu = _utilityDal.GetGLForContra();
            return Json(cu, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllGL()
        {
            List<ChartOfAccLayerSeven> cu = _utilityDal.GetAllGL();
            return Json(cu, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetGLByGlType(int GlType)
        {
            List<ChartOfAccLayerSeven> cu = _utilityDal.GetGLByGlType(GlType);
            return Json(cu, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDepartment()
        {
            List<Department> aList = _utilityDal.GetDepartment();
            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetIgEmployee()
        {
            List<View_EmployeeInfo> aList = _utilityDal.GetIgFoodsEmployee();
            return Json(aList, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetCategoryByGroupId(int groupId)
        {
            List<tbl_ProductCategory> aPrca = _utilityDal.GetCategoryByGroupId(groupId);
            return Json(aPrca, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetProductByCategoryId(int categoryId)
        {
            List<Product> aPrca = _utilityDal.GetProductByCategoryId(categoryId);
            return Json(aPrca, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadWareHouseWithMto(int locid = 0)
        {
            int loginId = Convert.ToInt32(Session[SessionCollection.LoginUserId]);
            List<WareHouse> alist = _utilityDal.WareHouseInformationMto(loginId, locid);
            return Json(alist, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCostCenter()
        {
            List<CostCenterM> aList = _utilityDal.GetCostCenter();
            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetGLForRightOff()
        {
            List<CashAccount> cu = _utilityDal.GetGLForRightOff();
            return Json(cu, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetIOUAccounts()
        {
            string empId = Session[SessionCollection.UserName].ToString();
            List<ChartOfAccLayerSeven> cu = _utilityDal.GetIOUAccounts(empId);
            return Json(cu, JsonRequestBehavior.AllowGet);
        }
    }
}
