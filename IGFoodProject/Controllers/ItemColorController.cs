﻿using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IGFoodProject.Controllers
{
    public class ItemColorController : Controller
    {
        // GET: ItemColor

        ItemColorDAL aDAL = new ItemColorDAL();
     
        public ActionResult AddItemColor(int id = 0) 
        {
            ViewBag.ItemColorId = id;
            return View();
        }

        public ActionResult ItemColorList() 
        {
            List<ItemColor> alist = aDAL.GetItemColorList();  
            return View(alist);
        }
        public JsonResult CheckItemColor(string group)
        {
            int desig = aDAL.CheckItemColor(group); 
            return Json(desig, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveItemColor(ItemColor aGroup)
        {
            ResultResponse aResponse = new ResultResponse();

            string entryBy = Session[SessionCollection.UserName].ToString();
            aGroup.CreateBy = entryBy;
            aGroup.UpdateBy = entryBy;
            aResponse.isSuccess = aDAL.SaveItemColor(aGroup);  
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteItemColor(int id)
        {
            ResultResponse aResponse = new ResultResponse();
            aResponse.isSuccess = aDAL.DeleteItemColor(id); 
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetItemColorByID(int id)
        {
            return Json(aDAL.GetItemColorByID(id), JsonRequestBehavior.AllowGet); 

        }
    }
}