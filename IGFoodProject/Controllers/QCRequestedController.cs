﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;

namespace IGFoodProject.Controllers
{
    public class QCRequestedController : Controller
    {
        private QCRequestedDAL _aDal = new QCRequestedDAL();
        private CodeDbSet db = new CodeDbSet();
        // GET: QCRequested
        public ActionResult Index()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {

                return View();
            }
        }
        public ActionResult ReturnProductList(int warehouseId, DateTime challanDate)
        {


            DataTable aList = _aDal.GetQcRequestedProductList(warehouseId, challanDate);
            return PartialView("_partialQCRequestProdcutList", aList);
        }
        public ActionResult SaveRequestList(QcRequestedMaster aMaster)
        {
            ResultResponse aResponse = new ResultResponse();
            aMaster.CreateDate = DateTime.Now;
            aMaster.CreateBy = Session[SessionCollection.UserName].ToString();
            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    int stockInMasterId=0; int tradeStockInMasterId=0;
                    db.QcRequestedMasters.Add(aMaster);
                    db.SaveChanges();

                    var alist = (from s in db.QcRequestedProducts
                                 where s.QcRequestMasterId == aMaster.QcRequestMasterId
                                 group s by new { s.ProductId, s.IsTrade, s.ExpireDate }
                        into g
                                 select new
                                 {
                                     Id = g.Key.ProductId,
                                     ReturnQty = g.Sum(p => p.ReturnQty),
                                     ReturnKg = g.Sum(p => p.ReturnKg),
                                     IsTradeid = g.Key.IsTrade,
                                     ExpireDate=g.Key.ExpireDate

                                 }).ToList();
                    var aList2 = alist.Select(x => x.IsTradeid).Distinct();
                    //var alist =db.QcRequestedProducts.Where(x => x.QcRequestMasterId == aMaster.QcRequestMasterId).ToList();
                    StockInMaster aStockInMaster = new StockInMaster();
                    StockInMasterTrade aStockInMasterTrade = new StockInMasterTrade();
                    if (aList2.Count() == 2)
                    {

                        aStockInMaster.StockInDate = aMaster.ReturnDate;
                        aStockInMaster.StockInBy = aMaster.CreateBy;
                        aStockInMaster.WareHouseId = aMaster.WarehouseId;
                        aStockInMaster.CreateDate = DateTime.Now;
                        aStockInMaster.CreateBy = aMaster.CreateBy;
                        aStockInMaster.Comments = "Return Request Of Qc Opereation";
                        aStockInMaster.IsOpening = 8;
                        aStockInMaster.CompanyID = 7;
                        aStockInMaster.LocationId = 4;
                        aStockInMaster.IsPortion = 0;
                        db.StockInMasters.Add(aStockInMaster);
                        db.SaveChanges();
                        stockInMasterId = aStockInMaster.StockInMasterID;


                        aStockInMasterTrade.StockInDate = aMaster.ReturnDate;
                        aStockInMasterTrade.StockInBy = aMaster.CreateBy;
                        aStockInMasterTrade.WareHouseId = aMaster.WarehouseId;
                        aStockInMasterTrade.CreateDate = DateTime.Now;
                        aStockInMasterTrade.CreateBy = aMaster.CreateBy;
                        aStockInMasterTrade.Comments = "Return Request Of Qc Opereation";
                        aStockInMasterTrade.IsOpening = 8;
                        aStockInMasterTrade.CompanyID = 7;
                        aStockInMasterTrade.LocationId = 4;
                        db.StockInMasterTrades.Add(aStockInMasterTrade);
                        db.SaveChanges();
                        tradeStockInMasterId = aStockInMasterTrade.TradeStockInMasterID;

                    }
                    else
                    {
                        if (aList2.ToArray()[0] == true)
                        {

                            aStockInMasterTrade.StockInDate = aMaster.ReturnDate;
                            aStockInMasterTrade.StockInBy = aMaster.CreateBy;
                            aStockInMasterTrade.WareHouseId = aMaster.WarehouseId;
                            aStockInMasterTrade.CreateDate = DateTime.Now;
                            aStockInMasterTrade.CreateBy = aMaster.CreateBy;
                            aStockInMasterTrade.Comments = "Return Request Of Qc Opereation";
                            aStockInMasterTrade.IsOpening = 8;
                            aStockInMasterTrade.CompanyID = 7;
                            aStockInMasterTrade.LocationId = 4;
                            db.StockInMasterTrades.Add(aStockInMasterTrade);
                            db.SaveChanges();
                            tradeStockInMasterId = aStockInMasterTrade.TradeStockInMasterID;

                        }
                        else
                        {
                            aStockInMaster.StockInDate = aMaster.ReturnDate;
                            aStockInMaster.StockInBy = aMaster.CreateBy;
                            aStockInMaster.WareHouseId = aMaster.WarehouseId;
                            aStockInMaster.CreateDate = DateTime.Now;
                            aStockInMaster.CreateBy = aMaster.CreateBy;
                            aStockInMaster.Comments = "Return Request Of Qc Opereation";
                            aStockInMaster.IsOpening = 8;
                            aStockInMaster.CompanyID = 7;
                            aStockInMaster.LocationId = 4;
                            aStockInMaster.IsPortion = 0;
                            db.StockInMasters.Add(aStockInMaster);
                            db.SaveChanges();
                            stockInMasterId = aStockInMaster.StockInMasterID;
                        }
                    }

                    foreach (var item in alist)
                    {
                        if (item.IsTradeid == false)
                        {
                            StockInDetail aStockInDetail = new StockInDetail();
                            aStockInDetail.StockInMasterID = stockInMasterId;
                            aStockInDetail.ProductID = Convert.ToInt32(item.Id);
                            aStockInDetail.StockInDate = aMaster.ReturnDate;
                            aStockInDetail.StockInQty = Convert.ToDecimal(item.ReturnQty);
                            aStockInDetail.StockInKG = Convert.ToDecimal(item.ReturnKg);
                            aStockInDetail.IsOpening = 8;
                            aStockInDetail.CreateDate = DateTime.Now;
                            aStockInDetail.ExpireDate = item.ExpireDate;
                            db.StockInDetails.Add(aStockInDetail);
                            db.SaveChanges();
                            db.StockInDetails.Where(x => x.StockInDetailID == aStockInDetail.StockInDetailID)
                                .ToList()
                                .ForEach(a => a.RefStockInDetailID = aStockInDetail.StockInDetailID);
                            db.SaveChanges();
                            db.QcRequestedProducts.Where(
                                x => x.QcRequestMasterId == aMaster.QcRequestMasterId && x.ProductId == item.Id)
                                .ToList()
                                .ForEach(a => a.StockInDetailsId = aStockInDetail.StockInDetailID);
                            db.SaveChanges();
                        }
                        else
                        {
                            StockInDetailTrade aStockInDetailTrade = new StockInDetailTrade();
                            aStockInDetailTrade.TradeStockInMasterID = tradeStockInMasterId;
                            aStockInDetailTrade.ProductID = Convert.ToInt32(item.Id);
                            aStockInDetailTrade.StockInDate = aMaster.ReturnDate;
                            aStockInDetailTrade.StockInQty = Convert.ToDecimal(item.ReturnQty);
                            aStockInDetailTrade.StockInKG = Convert.ToDecimal(item.ReturnKg);
                            aStockInDetailTrade.IsOpening = 8;
                            aStockInDetailTrade.CreateDate = DateTime.Now;
                            db.StockInDetailTrades.Add(aStockInDetailTrade);
                            db.SaveChanges();
                            db.StockInDetailTrades.Where(x => x.TradeStockInDetailID == aStockInDetailTrade.TradeStockInDetailID)
                                .ToList()
                                .ForEach(a => a.RefTradeStockInDetailID = aStockInDetailTrade.TradeStockInDetailID);
                            db.SaveChanges();
                            db.QcRequestedProducts.Where(
                                x => x.QcRequestMasterId == aMaster.QcRequestMasterId && x.ProductId == item.Id)
                                .ToList()
                                .ForEach(a => a.StockInDetailsId = aStockInDetailTrade.TradeStockInDetailID);
                            db.SaveChanges();
                        }


                    }

                    dbContextTransaction.Commit();
                    aResponse.isSuccess = true;
                }
                catch (Exception ex)
                {
                    Trace.TraceInformation(ex.Message);
                    dbContextTransaction.Rollback();
                }
            }

            //aResponse = _aDal.SaveReturnRequst(aMaster);



            //foreach (var item in aMaster)
            //{
            //    item.CreateDate= DateTime.Now;
            //    item.CreateBy = Session[SessionCollection.UserName].ToString();
            //    item.UpdateDate=DateTime.Now;
            //    item.UpdateBy = Session[SessionCollection.UserName].ToString();
            //    aResponse = _aDal.SaveReturnRequst(item);
            //}
            //return Json(aResponse, JsonRequestBehavior.AllowGet);
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult QcOperation()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }

        public ActionResult QcOperationProductList(int warehouseId, int challanId, DateTime challanDate, int isTrade)
        {


            DataTable aList = _aDal.GetRemainingQcRequestedProductList(warehouseId, challanId, challanDate,isTrade);
            return PartialView("_partialQCOperationProductList", aList);
        }

        public ActionResult ProductUnitCost(int productId, int groupId, DateTime challanDate)
        {
            var unitCost = _aDal.GetProdutCostPrice(productId, groupId, challanDate);
            return Json(unitCost, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveQcOperation(QcOperationMaster entity)
        {
            entity.CreateDate = DateTime.Now;
            ResultResponse aResponse = new ResultResponse();
            aResponse = _aDal.SaveQcOperation(entity);
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
        //new develpoment shahadat 16-11-21
        public ActionResult ApproveQcOperation()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult ApproveQcOperationProductList(int warehouseId, int challanId, DateTime challanDate, int isTrade)
        {


            DataTable aList = _aDal.GetApprovalQcOperationProductList(warehouseId, challanId, challanDate, isTrade);
            return PartialView("_partialQCOperationProductListforApprove", aList);
        }
        public ActionResult saveApprovalQcOperation(QcOperationMaster entity)
        {
            entity.CreateDate = DateTime.Now;
            entity.CreateBy = Session[SessionCollection.UserName].ToString();
            ResultResponse aResponse = new ResultResponse();
            aResponse = _aDal.ApproveQcOperation(entity);
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
        ///end

        public ActionResult GetChallan(DateTime challanDate)
        {
            List<SalesOrderChallanMaster> aList = _aDal.GetChallanList(challanDate);
            return Json(aList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetChallanFReturn(int customerId, DateTime fromDate, DateTime toDate)
        {
            List<SalesOrderChallanMaster> aList = _aDal.GetChallanListFReturn( customerId,  fromDate,  toDate);
            return Json(aList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetQcProductHistoryOfQty(int challanId, int productId)
        {
            DataTable aList = _aDal.GetQcProductHistoryOfQty(challanId, productId);
            return PartialView("_partialProductHistoryQty", aList);
        }

        public ActionResult GetQcProductHistoryOfKg(int challanId, int productId)
        {
            DataTable aList = _aDal.GetQcProductHistoryOfKg(challanId, productId);
            return PartialView("_partialProductHistoryKg", aList);
        }
        public ActionResult GetReturnQcList(int customerId, DateTime challlanFromDate, DateTime challlanToDate, int warehouseId, int challanId,int? isTrade)
        {
            DataTable aList = _aDal.GetReturnQcList(customerId, challlanFromDate, challlanToDate, warehouseId, challanId, isTrade);
            return PartialView("_PartialQcReturnList", aList);
        }

        public ActionResult QcReport()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
        public ActionResult GetDeliveryList(int customerId, DateTime challlanFromDate, DateTime challlanToDate, int warehouseId, int challanId, int? isTrade)
        {
            DataTable aList = _aDal.GetDeliveryList(customerId, challlanFromDate, challlanToDate, warehouseId, challanId, isTrade);
            return PartialView("_PartialDeliveryList", aList);
        }
        public ActionResult DeliveryReport()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }
    }
}