﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.Utility;
using IGFoodProject.DAL;
using IGFoodProject.Models;

namespace IGFoodProject.Controllers
{
    public class ProductCategoryController : Controller
    {
        // GET: ProductCategory
        public ActionResult ProductCategoryList()
        {
            ProductCategoryDAL aDal = new ProductCategoryDAL();
            List<tbl_ProductCategory> alist = aDal.GetCategoryList();
            return View(alist);
        }

        public ActionResult AddCategory(int id = 0)
        {
            ViewBag.categoryid = id;
            return View();
        }
        public JsonResult LoadType(int groupid=0)
        {
            ProductCategoryDAL aDal = new ProductCategoryDAL();
            return Json(aDal.GetTypeList(groupid) , JsonRequestBehavior.AllowGet);
        }
        public JsonResult CheckCategory(string pcategory)
        {

            ProductCategoryDAL aDAL = new ProductCategoryDAL();

            int desig = aDAL.CheckCategory(pcategory);

            return Json(desig, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckCategoryCode(string pcategory)
        {

            ProductCategoryDAL aDAL = new ProductCategoryDAL();

            int desig = aDAL.CheckCategoryCode(pcategory);

            return Json(desig, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveCategory(tbl_ProductCategory aType)
        {
            ResultResponse aResponse = new ResultResponse();
            ProductCategoryDAL aDAL = new ProductCategoryDAL();

            string entryBy = Session[SessionCollection.UserName].ToString();
            aType.CreateBy = entryBy;
            aType.UpdateBy = entryBy;
            aResponse.isSuccess = aDAL.SaveCategory(aType);

            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCategoryByID(int id)
        {
            ProductCategoryDAL aDal = new ProductCategoryDAL();
            return Json(aDal.GetCategoryForEdit(id), JsonRequestBehavior.AllowGet);

        }

        public JsonResult DeleteCategory(int id)
        {
            ResultResponse aResponse = new ResultResponse();
            ProductCategoryDAL aDAL = new ProductCategoryDAL();


            aResponse.isSuccess = aDAL.DeleteCategory(id);

            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
    }
}