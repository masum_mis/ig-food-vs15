﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;

namespace IGFoodProject.Controllers
{
    public class StockReceiveController : Controller
    {
        // GET: StockReceive
        StockReceiveDAL _aDal = new StockReceiveDAL();
        public ActionResult Index()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;

            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
            
        }


        public ActionResult SearchReceive(int com, int wh, int vh, DateTime delDate)
        {

            
            List<ViewStockReceiveListView> aList = _aDal.GetStockReceiveSearch(com, wh, vh, delDate);
            return PartialView("_partialStockReceiveList", aList);
        }

        public ActionResult ReceiveIssue(int id)
        {
            string loginUserEmpId = Session["loginuserempid"] as string;

            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                ViewBag.IssueId = id;
                return View();
            }
            
        }

        public ActionResult GetIssueMaster(int id)
        {

            ViewStockReceiveListView aListView = _aDal.GetStockreceiveByIssueId(id);
            
            return Json(aListView, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetIssueDetails(int id)
        {
            List<ViewStockReceiveDetails> aList = _aDal.GetIssueDetails(id);
            return PartialView("_partialIssueDetails", aList);
        }


        public ActionResult SaveStockReceive(StockReceiveMaster aMaster)
        {

            ResultResponse aResponse = _aDal.SaveReceive(aMaster, Session[SessionCollection.UserName].ToString());
            return Json(aResponse , JsonRequestBehavior.AllowGet);
        }

  

    }
}