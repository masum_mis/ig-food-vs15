﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.ViewModel;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using IGFoodProject.Models.ReportModel;

namespace IGFoodProject.Controllers
{
    public class TradeMrrController : Controller
    {
        TradeMrrDAL _aDal = new TradeMrrDAL();
        // GET: TradeMrr
        public ActionResult Index(DateTime? fromdate, DateTime? todate, int poNo = 0, int supplierId = 0, int productId = 0)
        {
            List<ViewTradeMrr> aList = _aDal.PoList(fromdate, todate, poNo, supplierId, productId);
            return View(aList);
        }
        public ActionResult SearchMrrList(DateTime? fromdate, DateTime? todate, int poNo = 0, int supplierId = 0, int productId = 0)
        {
            List<ViewTradeMrr> aList = _aDal.PoList(fromdate, todate, poNo, supplierId, productId);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }

        public JsonResult LoadTradePONoForMRR()
        {
            List<ViewTradeMrr> aPo = _aDal.LoadTradePONoForMRR();
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPo_forModalPopUpDetailsById(int id)
        {
            List<TradeMrrDetails> aPo = _aDal.TradePoDetailsForMrr(id);
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReceiveMrr(int id)
        {

            ViewBag.MasterId = id;

            return View();
        }
        public JsonResult GetDetailsForMrrGenerate(int id)
        {
            TradeMrrMaster aMaster = _aDal.GetTradePoInfoByMasterById(id);
            aMaster.TradeMrrDetails = _aDal.TradePoDetailsForMrr(id);
            return Json(aMaster, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LoadPackSize()
        {
            List<tbl_ProductPackSize> aPo = _aDal.LoadPackSize();
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveTradeMrr(TradeMrrMaster entity)
        {
            try
            {
                string entryBy = Session[SessionCollection.UserName].ToString();
                ResultResponse aResponse = _aDal.SaveTradeMrr(entity, entryBy);
                return Json(aResponse, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public ActionResult MrrList(DateTime? fromdate, DateTime? todate, int mrrNo = 0, int supplierId = 0, int productId = 0)
        {
            List<ViewTradeMrr> aList = _aDal.TradeMrrList(fromdate, todate, mrrNo, supplierId, productId);
            return View(aList);
        }
        public JsonResult LoadMrrNo()
        {
            List<ViewTradeMrr> aPo = _aDal.LoadMrrNo();
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SearchTradeMrrList(DateTime? fromdate, DateTime? todate, int mrrNo = 0, int supplierId = 0, int productId = 0)
        {
            List<ViewTradeMrr> aList = _aDal.TradeMrrList(fromdate, todate, mrrNo, supplierId, productId);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetMRR_forModalPopUpDetailsById(int id)
        {
            List<TradeMrrDetails> aPo = _aDal.TradeMRRDetailsById(id);
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }
    
        public ActionResult EditMrr(int id,int PoMasterId,int IsApprove)
        {

            ViewBag.MasterId = id;
            ViewBag.PoMasterId = PoMasterId;
            ViewBag.IsApprove = IsApprove;
            return View();
        }

        public JsonResult GetMrrDetails(int id,int poMasteId)
        {
            TradeMrrMaster aMaster = _aDal.GetTradeMrrInfoById(id);
            aMaster.TradeMrrDetails = _aDal.TradeMRRDetailsById(id);
            return Json(aMaster, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MRRApprovalList(DateTime? fromdate, DateTime? todate, int mrrNo = 0, int supplierId = 0, int productId = 0)
        {
            List<ViewTradeMrr> aList = _aDal.TradeMrrApprovalList(fromdate, todate, mrrNo, supplierId, productId);
            return View(aList);
        }

        public ActionResult SearchTradeMrrApprovalList(DateTime? fromdate, DateTime? todate, int mrrNo = 0, int supplierId = 0, int productId = 0)
        {
            List<ViewTradeMrr> aList = _aDal.TradeMrrApprovalList(fromdate, todate, mrrNo, supplierId, productId);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }

        public ActionResult TradeMrrPrint(DateTime? fromdate, DateTime? todate, int mrrNo = 0, int supplierId = 0, int productId = 0)
        {
            List<ViewTradeMrr> aList = _aDal.TradeMrrPrint(fromdate, todate, mrrNo, supplierId, productId);
            return View(aList);
        }

        public ActionResult PrintMultipleTradeMRR(DirrectMrr aDirrectMrr)
        {
            ResultResponse aResponse = new ResultResponse();
            BasicInfoReportsParameter aBasicInfoReportsParameter = new BasicInfoReportsParameter();
            aBasicInfoReportsParameter.ReportValue = aDirrectMrr.ReportValue;
            Session[SessionCollection.ReportValue] = aBasicInfoReportsParameter;
            Session[SessionCollection.ReportParam] = aDirrectMrr;
            aResponse.isSuccess = true;
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
        #region ReturnMrr

        public ActionResult ListForMrrReturn(DateTime? fromdate, DateTime? todate, int mrrNO = 0, int supplierId = 0, int itemId = 0)
        {
            List<ViewTradeMrr> aList = _aDal.ListForReturn(fromdate, todate, mrrNO, supplierId, itemId);
            return View(aList);
        }

        public ActionResult SearchListForMrrReturn(DateTime? fromdate, DateTime? todate, int mrrNO = 0, int supplierId = 0, int itemId = 0)
        {
            List<ViewTradeMrr> aList = _aDal.ListForReturn(fromdate, todate, mrrNO, supplierId, itemId);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }
        public ActionResult ReturnMrr(int id)
        {

            ViewBag.MasterId = id;

            return View();
        }
        public JsonResult GetDetailsForMrrReturn(int id)
        {
            TradeMrrMaster aMaster = _aDal.GetTradeMrrInfoById(id);
            aMaster.TradeMrrDetails = _aDal.MrrDetailsforReturnById(id);
            return Json(aMaster, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveTradeMrrReturn(ViewTradeMrrReturnMaster entity)
        {
            try
            {
                string entryBy = Session[SessionCollection.UserName].ToString();
                ResultResponse aResponse = _aDal.SaveTradeMrrReturn(entity, entryBy);
                return Json(aResponse, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public ActionResult ReturnedMrrList(DateTime? fromdate, DateTime? todate, int supplierId = 0, int productId = 0)
        {
            List<ViewTradeMrrReturnMaster> aList = _aDal.ReturnedMrrList(fromdate, todate, supplierId, productId);
            return View(aList);
        }
        public ActionResult SearchForReturned(DateTime? fromdate, DateTime? todate, int supplierId = 0, int productId = 0)
        {
            List<ViewTradeMrrReturnMaster> aList = _aDal.ReturnedMrrList(fromdate, todate, supplierId, productId);
            return Json(aList, JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetTradeReturnedDetailsById(int id)
        {
            List<ViewTradeMrrReturnDetails> aPo = _aDal.TradeMrrReturnDetailsById(id);
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetMRRReturn_forModalPopUpDetailsById(int id)
        {
            List<TradeMrrDetails> aPo = _aDal.MrrDetailsforReturnById(id)
;
            return Json(aPo, JsonRequestBehavior.AllowGet);
        }
        #endregion





        public ActionResult TradeMrrReturnReport()
        {

            return View();
        }
        public ActionResult MrrReturnReport(int supplierId, int warehouse, DateTime fromdate, DateTime todate)
        {
            ViewTradeMrrReturnMaster aList = new ViewTradeMrrReturnMaster();


            aList.SupplierID = supplierId;
            aList.DelWareHouseID = warehouse;
            aList.FromDate = fromdate;
            aList.ToDate = todate;
           

            ResultResponse aResponse = new ResultResponse();
            BasicInfoReportsParameter aBasicInfoReportsParameter = new BasicInfoReportsParameter();
            aBasicInfoReportsParameter.ReportValue = "TradeReturnList";
            Session[SessionCollection.ReportValue] = aBasicInfoReportsParameter;
            Session[SessionCollection.ReportParam] = aList;
            aResponse.isSuccess = true;
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
   
    }
}