﻿using IGFoodProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IGFoodProject.DAL;
using IGFoodProject.ViewModel;
using IGFoodProject.Utility;

namespace IGFoodProject.Controllers
{
    public class TradeSalesOrderController : Controller
    {
        private TradeSalesOrderDAL _tradeSalesOrderDal= new TradeSalesOrderDAL();
        // GET: TradeSalesOrder
        public ActionResult TradeSalesOrder(int soId = 0, string eflag = "ed")

        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                ViewBag.SalesOrderId = soId;
                ViewBag.eflag = eflag;
                return View();
            }
        }
        public ActionResult AddRowForOrder(int tr)
        {
            TempData["trSl"] = tr;

            return PartialView("_addNewOrderRow");
        }
        public ActionResult GetProductFTrade()
        {

            List<Product> aProducts = _tradeSalesOrderDal.GetProductForTrade();
            return Json(aProducts, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetProductDetails(int productId, DateTime orderdate)
        {
            ViewProductDetailsSales aDetailsSales = _tradeSalesOrderDal.GetProductDetailsFTrade(productId, orderdate);
            return Json(aDetailsSales, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveOrderFTrade(SalesOrderMasterTrade aMaster)
        {
            aMaster.CreateDate = DateTime.Now;
            aMaster.CreateBy = Session[SessionCollection.LoginUserEmpId].ToString();
            ResultResponse aResponse = _tradeSalesOrderDal.SaveSalesOrderProcessed(aMaster, Session[SessionCollection.UserName].ToString());
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }
        public ActionResult TradeSalesOrderList(DateTime? fromdate, DateTime? todate, string salespersonid, int customerid = 0)
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                List<SalesOrderMasterTrade> aList = _tradeSalesOrderDal.GetSalesOrderProcessedMaster(fromdate, todate, salespersonid, customerid);


                return View(aList);
            }
        }
        public ActionResult SalesOrderList_Search(DateTime? fromdate, DateTime? todate, string salespersonid, int customerid = 0)
        {
            List<SalesOrderMasterTrade> aList = _tradeSalesOrderDal.GetSalesOrderProcessedMaster(fromdate, todate, salespersonid, customerid);


            return Json(aList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetSalesOrderMaster(int id)
        {
            SalesOrderMasterTrade aMaster = _tradeSalesOrderDal.GetSalesOrderProcessedMaster(id);
            aMaster.SalesDetailsTrade = _tradeSalesOrderDal.GetSalesOderProcessedDetailsByMaster(id);
            return Json(aMaster, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ViewOrder(int id)
        {
            ViewBag.SalesOrder = id;
            return PartialView("_PartialTradeSalesView");
        }
        public ActionResult ApproveSalesProcesed(int id, bool status)
        {
            ResultResponse aResponse = _tradeSalesOrderDal.ApproveSalesOrder(id, status,
                Session[SessionCollection.UserName].ToString());
            return Json(aResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TradeInvoicePrint()
        {
            string loginUserEmpId = Session["loginuserempid"] as string;
            if (string.IsNullOrEmpty(loginUserEmpId))
            {
                return RedirectToAction(nameof(HomeController.Login), "Home");
            }
            else
            {
                return View();
            }
        }

        public ActionResult SalesOrderListForPrint(DateTime fromdate, DateTime todate, string salespersonid="", int customerid=0)
        {
            IEnumerable<ViewSalesOrderProcessedMaster> aList = Enumerable.Empty<ViewSalesOrderProcessedMaster>();

            aList = _tradeSalesOrderDal.GetSalesOrderProcessedMasterForInvoice(fromdate, todate, salespersonid, customerid);

            return PartialView("_PartialTradeSalesOrderListForPrint", aList);
        }
        public ActionResult SalesOrderApproveList(DateTime? fromdate, DateTime? todate, string salespersonid, int customerid = 0)
        {
            IEnumerable<ViewSalesOrderProcessedMaster> aList = _tradeSalesOrderDal.GetSalesOrderProcessedMasterForApprove(fromdate, todate, salespersonid, customerid); ;


            return View(aList);
        }

        public ActionResult SalesOrderApproveListFSearch(DateTime? fromdate, DateTime? todate, string salespersonid, int customerid = 0)
        {
            IEnumerable<ViewSalesOrderProcessedMaster> aList = _tradeSalesOrderDal.GetSalesOrderProcessedMasterForApprove(fromdate, todate, salespersonid, customerid); ;


            return  Json(aList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ApproveSalesProcessed(int id)
        {

            ViewBag.SalesOrderId = id;
            return View();


        }
    }
}