﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using IGFoodProject.Models.Accounts;

namespace IGAccounts.Repository
{
    public class GenericRepository<TObject> where TObject : class
    {
        protected CodeDbSetAccounts _context;
        private ChartOfAccLayerTwo aEntity;
        internal DbSet<TObject> dbSet;

        public GenericRepository(CodeDbSetAccounts context)
        {
            _context = context;
        }

     

        public ICollection<TObject> GetAll()
        {
            try
            {
                return _context.Set<TObject>().ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<ICollection<TObject>> GetAllAsync()
        {
            return await _context.Set<TObject>().ToListAsync();
        }

        public TObject Get(int id)
        {
            return _context.Set<TObject>().Find(id);
        }
        public TObject Get(string id)
        {
            return _context.Set<TObject>().Find(id);
        }
        public async Task<TObject> GetAsync(int id)
        {
            return await _context.Set<TObject>().FindAsync(id);
        }

        public TObject Find(Expression<Func<TObject, bool>> match)
        {
            return _context.Set<TObject>().SingleOrDefault(match);
        }

        public async Task<TObject> FindAsync(Expression<Func<TObject, bool>> match)
        {
            return await _context.Set<TObject>().SingleOrDefaultAsync(match);
        }

        public ICollection<TObject> FindAll(Expression<Func<TObject, bool>> match)
        {
            return _context.Set<TObject>().Where(match).ToList();
        }

        public async Task<ICollection<TObject>> FindAllAsync(Expression<Func<TObject, bool>> match)
        {
            return await _context.Set<TObject>().Where(match).ToListAsync();
        }

        public bool Insert(TObject t)
        {
            try
            {
                _context.Set<TObject>().Add(t);
                int a = _context.SaveChanges();
                if (a == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception exception)
            {

                throw exception;
            }



        }

        public int InsertReturnKey(TObject t)
        {
            try
            {
                _context.Set<TObject>().Add(t);
                int a = _context.SaveChanges();
                
                return a;
            }
            catch (Exception exception)
            {

                throw exception;
            }



        }

        public async Task<TObject> AddAsync(TObject t)
        {
            _context.Set<TObject>().Add(t);
            await _context.SaveChangesAsync();
            return t;
        }

        public bool Update(TObject updated, int key)
        {
            if (updated == null)
                return false;
            bool a = false;
            TObject existing = _context.Set<TObject>().Find(key);
            if (existing == null) return a;
            _context.Entry(existing).CurrentValues.SetValues(updated);
            //dbSet.Attach(updated);
            //_context.Entry(updated).State=EntityState.Modified;
            int x = _context.SaveChanges();
            a = x == 1;
            return a;
        }

        public async Task<TObject> UpdateAsync(TObject updated, int key)
        {
            if (updated == null)
                return null;

            TObject existing = await _context.Set<TObject>().FindAsync(key);
            if (existing != null)
            {
                _context.Entry(existing).CurrentValues.SetValues(updated);
                await _context.SaveChangesAsync();
            }
            return existing;
        }

        public void Delete(TObject t)
        {
            _context.Set<TObject>().Remove(t);
            _context.SaveChanges();
        }

        public async Task<int> DeleteAsync(TObject t)
        {
            _context.Set<TObject>().Remove(t);
            return await _context.SaveChangesAsync();
        }

        public int Count()
        {
            return _context.Set<TObject>().Count();
        }

        public async Task<int> CountAsync()
        {
            return await _context.Set<TObject>().CountAsync();
        }

    }
}