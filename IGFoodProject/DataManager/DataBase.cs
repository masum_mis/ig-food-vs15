﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.DataManager
{
    public class DataBase
    {

        public static string ControlPanel = @"ControlPanelDB";
        public static string FarmManagement = @"FarmManagementDB";
        public static string SupplyChain = @"SupplyChainDB";
        public static string CommercialHatcheryDB = @"CommercialHatcheryDB";
        public static string IGFoodDB = @"IGFoodDB";
        public static string HumanResourceDB = @"HumanResourceDB";
        public static string IGFOODACCDB = @"IGFOODACCDB";
    }
}