﻿using IGFoodProject.DataManager;
using IGFoodProject.Models;
using IGFoodProject.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.DAL
{
    public class PurchaseOrderDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();

        public List<RequisitionMaster> RequisitionInformation(int companyId, int warehouseId, DateTime exPurchaseDate)
        {
            try
            {
                
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@companyId", companyId));
                parameters.Add(new SqlParameter("@wareHouseId", warehouseId));
                parameters.Add(new SqlParameter("@PurchaseOrderDate", exPurchaseDate));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetFilteredRequisition", parameters);

                List<RequisitionMaster> requisitionList = new List<RequisitionMaster>();
                while (dr.Read())
                {
                    RequisitionMaster requisition = new RequisitionMaster();
                    requisition.ItemRequisitionMasterId = Convert.ToInt32(dr["ItemRequisitionMasterId"]);
                    requisition.RequisitionNo = dr["RequisitionNo"].ToString();
                    requisitionList.Add(requisition);
                }
                return requisitionList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

        public List<RequisitionMaster> RequisitionInformationForEdit(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@PurchaseOrderID", id));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetRequisitionForEdit", parameters);
                List<RequisitionMaster> requisitionList = new List<RequisitionMaster>();
                while (dr.Read())
                {
                    RequisitionMaster requisition = new RequisitionMaster();
                    requisition.ItemRequisitionMasterId = Convert.ToInt32(dr["ItemRequisitionMasterId"]);
                    //requisition.Company = Convert.ToInt32(dr["CompanyId"]);
                    //requisition.WarehouseId = Convert.ToInt32(dr["WarehouseId"]);
                    //requisition.LocationId = Convert.ToInt32(dr["LocationId"]);
                    requisition.RequisitionNo = dr["RequisitionNo"].ToString();
                    requisitionList.Add(requisition);
                }
                return requisitionList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

        public List<tbl_SupplierInfo> SupplierInformation()
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                //List<SqlParameter> parameters = new List<SqlParameter>();
                //parameters.Add(new SqlParameter("@locationId", locationId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetSupplierInfo");
                List<tbl_SupplierInfo> aList = new List<tbl_SupplierInfo>();
                while (dr.Read())
                {
                    tbl_SupplierInfo supplier = new tbl_SupplierInfo();
                    supplier.SupplierID = Convert.ToInt32(dr["SupplierID"]);
                    supplier.SupplierName = dr["SupplierName"].ToString();
                    aList.Add(supplier);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

        public tbl_SupplierInfo SupplierInformationById(int supplierId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@SupplierID", supplierId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetSupplierDetails", parameters);
                tbl_SupplierInfo supplier = new tbl_SupplierInfo();
                while (dr.Read())
                {

                    supplier.SupplierID = Convert.ToInt32(dr["SupplierID"]);
                    supplier.SupplierName = dr["SupplierName"].ToString();
                    supplier.SupplierCode = dr["SupplierCode"].ToString();
                    //supplier.SupplierAddress = dr["SupplierAddress"].ToString();
                    supplier.SupplierPhone = dr["SupplierPhone"].ToString();
                    supplier.SupplierSecondPhone = dr["SupplierSecondPhone"].ToString();
                    supplier.SupplierEmail = dr["SupplierEmail"].ToString();
                    supplier.ContactPerson = dr["ContactPerson"].ToString();
                    //supplier.ContactPersonPhone = dr["ContactPersonPhone"].ToString();


                }
                return supplier;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

        public List<RequisitionDetails> GetRequisitionDetailByID(int reqId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@ItemRequisitionMasterId", reqId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetRequsitionDetailByID", parameters);
                List<RequisitionDetails> aList = new List<RequisitionDetails>();
                while (dr.Read())
                {
                    RequisitionDetails supplier = new RequisitionDetails();
                    supplier.ItemRequisitionDetailsId = Convert.ToInt32(dr["ItemRequisitionDetailsId"]);
                    supplier.ItemId = Convert.ToInt32(dr["ItemId"]);
                    if (dr["RemainingQty"] != DBNull.Value)
                    {
                        supplier.RemainingQty = Convert.ToDecimal(dr["RemainingQty"]);
                    }
                    if (dr["RemainingKG"] != DBNull.Value)
                    {
                        supplier.RemainingKG = Convert.ToDecimal(dr["RemainingKG"]);
                    }
                    supplier.ApprovedTentativePrice = Convert.ToDecimal(dr["ApprovedTentativePrice"]);
                    supplier.Remarks = dr["Remarks"].ToString();
                    supplier.ItemName = dr["ItemName"].ToString();
                    aList.Add(supplier);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

        public bool SavePurchaseOrder(tbl_PurchaseOrderMaster aMaster)
        {
            bool result = false;
            try

            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                int id = SavePurchaseOrderMaster(aMaster);


                foreach (tbl_PurchaseOrderDetail grade in aMaster.aDetail)
                {
                    List<SqlParameter> gSqlParameterList = new List<SqlParameter>();

                    gSqlParameterList.Add(new SqlParameter("@PurchaseOrderDetailID", grade.PurchaseOrderDetailID));
                    gSqlParameterList.Add(new SqlParameter("@PurchaseOrderID", id));
                    gSqlParameterList.Add(new SqlParameter("@ItemId", grade.ItemId));
                    gSqlParameterList.Add(new SqlParameter("@PurchaseQty", grade.PurchaseQty));
                    gSqlParameterList.Add(new SqlParameter("@PurchaseKG", grade.PurchaseKG));
                    gSqlParameterList.Add(new SqlParameter("@UnitPrice", grade.UnitPrice));
                    gSqlParameterList.Add(new SqlParameter("@TotalPrice", grade.TotalPrice));
                    gSqlParameterList.Add(new SqlParameter("@Remarks", grade.Remarks));
                    gSqlParameterList.Add(new SqlParameter("@ItemRequisitionDetailsId", grade.ItemRequisitionDetailsId));
                    gSqlParameterList.Add(new SqlParameter("@flag", aMaster.flag));
                    result = _accessManager.SaveData("sp_SavePurchaseOrderDetail", gSqlParameterList);
                }


            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
            return result;
        }
        public int SavePurchaseOrderMaster(tbl_PurchaseOrderMaster aMaster)
        {
            try
            {
                string empReceived = string.Empty;
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@PurchaseOrderID", aMaster.PurchaseOrderID));
                aSqlParameterList.Add(new SqlParameter("@ItemRequisitionMasterId", aMaster.ItemRequisitionMasterId));
                aSqlParameterList.Add(new SqlParameter("@OrderDate", aMaster.OrderDatestr));
                aSqlParameterList.Add(new SqlParameter("@ExpectedDeliveryDate", aMaster.ExpectedDeliveryDatestr));
                aSqlParameterList.Add(new SqlParameter("@SupplierID", aMaster.SupplierID));
                aSqlParameterList.Add(new SqlParameter("@Comments", aMaster.Comments));
                aSqlParameterList.Add(new SqlParameter("@OrderBY", aMaster.OrderBY.Split(':')[0].Trim()));
                aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@CreateDate", System.DateTime.Now));
                aSqlParameterList.Add(new SqlParameter("@UpdateBy", aMaster.UpdateBy));
                aSqlParameterList.Add(new SqlParameter("@UpdateDate", System.DateTime.Now)); 
                aSqlParameterList.Add(new SqlParameter("@ApproveBy", aMaster.ApproveBy));
                aSqlParameterList.Add(new SqlParameter("@ApproveDatetime", System.DateTime.Now)); 
                aSqlParameterList.Add(new SqlParameter("@flag", aMaster.flag));
                aSqlParameterList.Add(new SqlParameter("@TotalDue", aMaster.TotalDue));
                aSqlParameterList.Add(new SqlParameter("@TotalPaid", aMaster.TotalPaid));
                aSqlParameterList.Add(new SqlParameter("@TotalQty", aMaster.TotalQty));
                aSqlParameterList.Add(new SqlParameter("@Tolerance", aMaster.TolerancePercent));

                return _accessManager.SaveDataReturnPrimaryKey("sp_SavePurchaseOrder", aSqlParameterList);
            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }
        }
        public dynamic GetPurchaseOrderListByDate(DateTime? fromdate, DateTime? todate, string supplierName = "")
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameters = new List<SqlParameter>();
                aSqlParameters.Add(new SqlParameter("@supplierName", supplierName));
                aSqlParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aSqlParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
              //  SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetPurchaseOrderListbyDate");
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetPurchaseOrderListbyDate", aSqlParameters);
                List<tbl_PurchaseOrderMaster> requisitionMasterList = new List<tbl_PurchaseOrderMaster>();
                while (dr.Read())
                {
                    tbl_PurchaseOrderMaster requisitionMaster = new tbl_PurchaseOrderMaster();
                    requisitionMaster.PurchaseOrderID = Convert.ToInt32(dr["PurchaseOrderID"]);
                    requisitionMaster.PurchaseOrderNo = dr["PurchaseOrderNo"].ToString();
                    requisitionMaster.OrderDatestr = dr["OrderDate"].ToString();
                    requisitionMaster.ExpectedDeliveryDatestr = dr["ExpectedDeliveryDate"].ToString();
                    requisitionMaster.SupplierID = Convert.ToInt32(dr["SupplierID"]);
                    requisitionMaster.SupplierName = dr["SupplierName"].ToString();
                    requisitionMaster.Comments = dr["Comments"].ToString();
                    requisitionMaster.OrderBY = dr["OrderBY"].ToString();
                    requisitionMaster.CreateBy = dr["CreateBy"].ToString();
                    requisitionMaster.CreateDateStr = dr["CreateDate"].ToString();
                    requisitionMaster.remaingqty = Convert.ToDecimal(dr["remaingqty"]);
                    requisitionMaster.remaingkg = Convert.ToDecimal(dr["remaingkg"]);
                    requisitionMasterList.Add(requisitionMaster);

                }

                return requisitionMasterList;

            }
            catch (Exception exception)
            {


                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public dynamic GetPurchaseOrderCloseListByDate(DateTime? fromdate, DateTime? todate, string supplierName = "")
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameters = new List<SqlParameter>();
                aSqlParameters.Add(new SqlParameter("@supplierName", supplierName));
                aSqlParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aSqlParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                //  SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetPurchaseOrderListbyDate");
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetPurchaseOrderCloseListbyDate", aSqlParameters);
                List<tbl_PurchaseOrderMaster> requisitionMasterList = new List<tbl_PurchaseOrderMaster>();
                while (dr.Read())
                {
                    tbl_PurchaseOrderMaster requisitionMaster = new tbl_PurchaseOrderMaster();
                    requisitionMaster.PurchaseOrderID = Convert.ToInt32(dr["PurchaseOrderID"]);
                    requisitionMaster.PurchaseOrderNo = dr["PurchaseOrderNo"].ToString();
                    requisitionMaster.OrderDatestr = dr["OrderDate"].ToString();
                    requisitionMaster.ExpectedDeliveryDatestr = dr["ExpectedDeliveryDate"].ToString();
                    requisitionMaster.SupplierID = Convert.ToInt32(dr["SupplierID"]);
                    requisitionMaster.SupplierName = dr["SupplierName"].ToString();
                    requisitionMaster.Comments = dr["Comments"].ToString();
                    requisitionMaster.OrderBY = dr["OrderBY"].ToString();
                    requisitionMaster.CreateBy = dr["CreateBy"].ToString();
                    requisitionMaster.CreateDateStr = dr["CreateDate"].ToString();
                    requisitionMaster.remaingqty = Convert.ToDecimal(dr["remaingqty"]);
                    requisitionMaster.remaingkg = Convert.ToDecimal(dr["remaingkg"]);
                    requisitionMasterList.Add(requisitionMaster);

                }

                return requisitionMasterList;

            }
            catch (Exception exception)
            {


                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public dynamic PurchaseOrderList()
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameters = new List<SqlParameter>();
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetPurchaseOrderList");
                List<tbl_PurchaseOrderMaster> requisitionMasterList = new List<tbl_PurchaseOrderMaster>();
                while (dr.Read())
                {
                    tbl_PurchaseOrderMaster requisitionMaster = new tbl_PurchaseOrderMaster();
                    requisitionMaster.PurchaseOrderID = Convert.ToInt32(dr["PurchaseOrderID"]);
                    requisitionMaster.PurchaseOrderNo = dr["PurchaseOrderNo"].ToString();
                    requisitionMaster.OrderDatestr = dr["OrderDate"].ToString();
                    requisitionMaster.ExpectedDeliveryDatestr = dr["ExpectedDeliveryDate"].ToString();
                    requisitionMaster.SupplierID = Convert.ToInt32(dr["SupplierID"]);
                    requisitionMaster.SupplierName = dr["SupplierName"].ToString();
                    requisitionMaster.Comments = dr["Comments"].ToString();
                    requisitionMaster.OrderBY = dr["OrderBY"].ToString();
                    requisitionMaster.CreateBy = dr["CreateBy"].ToString();
                    requisitionMaster.CreateDate = Convert.ToDateTime(dr["CreateDate"]);
                    requisitionMaster.remaingqty = Convert.ToDecimal(dr["remaingqty"]);
                    requisitionMaster.remaingkg = Convert.ToDecimal(dr["remaingkg"]);
                    requisitionMasterList.Add(requisitionMaster);

                }

                return requisitionMasterList;

            }
            catch (Exception exception)
            {


                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public tbl_PurchaseOrderMaster GetPurchaseOrderForEdit(int id)
        {

            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@PurchaseOrderID", id));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetPurchaseOrderMasterDataById", aParameters);
                tbl_PurchaseOrderMaster aDetailsView = new tbl_PurchaseOrderMaster();
                while (dr.Read())
                {
                    aDetailsView.PurchaseOrderID = Convert.ToInt32(dr["PurchaseOrderID"]);
                    aDetailsView.PurchaseOrderNo = dr["PurchaseOrderNo"].ToString();
                    aDetailsView.ItemRequisitionMasterId = Convert.ToInt32(dr["ItemRequisitionMasterId"]);
                    aDetailsView.OrderDatestr = dr["OrderDate"].ToString();
                    aDetailsView.ExpectedDeliveryDatestr = dr["ExpectedDeliveryDate"].ToString();
                    aDetailsView.SupplierID = Convert.ToInt32(dr["SupplierID"]);
                    aDetailsView.Comments = dr["Comments"].ToString();
                    aDetailsView.OrderBY = dr["OrderBY"].ToString();
                    aDetailsView.CreateBy = dr["CreateBy"].ToString();
                    aDetailsView.CompanyId = Convert.ToInt32(dr["Company"]);
                    aDetailsView.DelLocationId = Convert.ToInt32(dr["LocationId"]);
                    aDetailsView.DelWareHouseID = Convert.ToInt32(dr["WarehouseId"]);
                    aDetailsView.TolerancePercent = Convert.ToInt32(dr["TolerancePercent"]);

                }
                return aDetailsView;

            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<tbl_PurchaseOrderDetail> GetPurchaseDetailByID(int reqId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@PurchaseOrderID", reqId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetPurchaseDetailForEdit", parameters);
                List<tbl_PurchaseOrderDetail> aList = new List<tbl_PurchaseOrderDetail>();
                while (dr.Read())
                {
                    tbl_PurchaseOrderDetail supplier = new tbl_PurchaseOrderDetail();
                    supplier.PurchaseOrderDetailID = Convert.ToInt32(dr["PurchaseOrderDetailID"]);
                    supplier.ItemRequisitionDetailsId = Convert.ToInt32(dr["ItemRequisitionDetailsId"]);
                    supplier.ItemId = Convert.ToInt32(dr["ItemId"]);
                    if (dr["RemainingQty"] != DBNull.Value)
                    {
                        supplier.RemainingQty = Convert.ToDecimal(dr["RemainingQty"]);
                    }
                    if (dr["RemainingKG"] != DBNull.Value)
                    {
                        supplier.RemainingKG = Convert.ToDecimal(dr["RemainingKG"]);
                    }

                    if (dr["PurchaseQty"] != DBNull.Value)
                    {
                        supplier.PurchaseQty = Convert.ToDecimal(dr["PurchaseQty"]);
                    }
                    if (dr["PurchaseKG"] != DBNull.Value)
                    {
                        supplier.PurchaseKG = Convert.ToDecimal(dr["PurchaseKG"]);
                    }
                    supplier.UnitPrice = Convert.ToDecimal(dr["UnitPrice"]);
                    supplier.TotalPrice = Convert.ToDecimal(dr["TotalPrice"]);
                    supplier.Remarks = dr["Remarks"].ToString();
                    supplier.ItemName = dr["ItemName"].ToString();
                    aList.Add(supplier);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }


        #region Po Report

        public List<ViewPurchaseOrderReportMaster> GetPurchaseOrderMasterReport(int company, int location, int wareHouse, string fromDate, string toDate, int status)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@company", company));
                parameters.Add(new SqlParameter("@location", location));
                parameters.Add(new SqlParameter("@wareHouse", wareHouse));
                parameters.Add(new SqlParameter("@fromDate", fromDate));
                parameters.Add(new SqlParameter("@toDate", toDate));
                parameters.Add(new SqlParameter("@status", status));
                List<ViewPurchaseOrderReportMaster> aList = new List<ViewPurchaseOrderReportMaster>();
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_RPT_Get_PurchaseOrdermaster", parameters);

                while (dr.Read())
                {
                    ViewPurchaseOrderReportMaster aMaster = new ViewPurchaseOrderReportMaster();
                    aMaster.PurchaseOrderID = Convert.ToInt32(dr["PurchaseOrderID"]);
                    aMaster.PurchaseOrderNo = dr["PurchaseOrderNo"].ToString();
                    aMaster.OrderDate = DBNull.Value == dr["OrderDate"] ? null : dr["OrderDate"] as DateTime?;
                    aMaster.OrderBY = dr["OrderBY"].ToString();
                    aMaster.ExpectedDeliveryDate = DBNull.Value == dr["ExpectedDeliveryDate"] ? null : dr["ExpectedDeliveryDate"] as DateTime?;
                    aMaster.RequisitionDate = DBNull.Value == dr["RequisitionDate"] ? null : dr["RequisitionDate"] as DateTime?;
                    aMaster.Comments = dr["Comments"].ToString();
                    aMaster.ApproveStatus = dr["ApproveStatus"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.CompanyName = dr["CompanyName"].ToString();
                    aMaster.SupplierName = dr["SupplierName"].ToString();
                    aMaster.ApproveBy = dr["ApproveBy"].ToString();
                    aMaster.StatusDes = dr["StatusDes"].ToString();
                    aMaster.RequisitionNo = dr["RequisitionNo"].ToString();
                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();

            }
        }


        public DataTable GetPurchaseOrderMasterReportDt(int company, int location, int wareHouse, string fromDate, string toDate, int status)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@company", company));
                parameters.Add(new SqlParameter("@location", location));
                parameters.Add(new SqlParameter("@wareHouse", wareHouse));
                parameters.Add(new SqlParameter("@fromDate", fromDate));
                parameters.Add(new SqlParameter("@toDate", toDate));
                parameters.Add(new SqlParameter("@status", status));
                List<ViewPurchaseOrderReportMaster> aList = new List<ViewPurchaseOrderReportMaster>();
                DataTable dt = _accessManager.GetDataTable("sp_RPT_Get_PurchaseOrdermaster", parameters);

                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();

            }
        }

        #endregion

        public List<tbl_PurchaseOrderDetail> GetLastFivePriceOfSuplier(int suplierId, int itemId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@SuplierId", suplierId));
                parameters.Add(new SqlParameter("@ItemId", itemId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetLastFivePriceOfSuplier", parameters);
                List<tbl_PurchaseOrderDetail> aList = new List<tbl_PurchaseOrderDetail>();
                while (dr.Read())
                {
                    tbl_PurchaseOrderDetail purchaseOrderDetail = new tbl_PurchaseOrderDetail();
                    purchaseOrderDetail.ItemName = dr["ItemName"].ToString();
                    purchaseOrderDetail.OrderDate = Convert.ToDateTime(dr["OrderDate"]);
                    purchaseOrderDetail.PurchaseQty = Convert.ToDecimal(dr["PurchaseQty"]);
                    purchaseOrderDetail.UnitPrice = Convert.ToDecimal(dr["PurchaseQty"]);
                    aList.Add(purchaseOrderDetail);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

        public bool DeletePurchaseOrder(tbl_PurchaseOrderMaster aMaster)
        {
            bool result = false;
            try

            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                
                    List<SqlParameter> gSqlParameterList = new List<SqlParameter>();

                    gSqlParameterList.Add(new SqlParameter("@PurchaseOrderID", aMaster.PurchaseOrderID));
                    gSqlParameterList.Add(new SqlParameter("@DeleteBy", aMaster.DeleteBy));
                    gSqlParameterList.Add(new SqlParameter("@DeleteDate", aMaster.DeleteDate));
                    gSqlParameterList.Add(new SqlParameter("@IsDelete", 1));
                    result = _accessManager.SaveData("sp_DeletePurchaseOrder", gSqlParameterList);

                return result;
            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
           
        }
        public bool ClosePurchaseOrder(tbl_PurchaseOrderMaster aMaster)
        {
            bool result = false;
            try

            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<SqlParameter> gSqlParameterList = new List<SqlParameter>();

                gSqlParameterList.Add(new SqlParameter("@PurchaseOrderID", aMaster.PurchaseOrderID));
                gSqlParameterList.Add(new SqlParameter("@ClosedBy", aMaster.ClosedBy));
                gSqlParameterList.Add(new SqlParameter("@ClosedDate", aMaster.ClosedDate));
                gSqlParameterList.Add(new SqlParameter("@IsClosed", 1));
                result = _accessManager.SaveData("sp_ClosePurchaseOrder", gSqlParameterList);

                return result;
            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }


        public List<ViewPurchaseOrderMaster> GetPurchaseOrderViewApproved( DateTime fromDate , DateTime toDate , int wareHouse = 0 , int supplier = 0)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add( new SqlParameter("@fromDate"  ,fromDate));
                aParameters.Add( new SqlParameter("@toDate", toDate));
                aParameters.Add( new SqlParameter("@wareHouse", wareHouse));
                aParameters.Add( new SqlParameter("@supplier", supplier));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_ApprovedPurchaseOrderForInvoice" , aParameters);
                List < ViewPurchaseOrderMaster > aList = new List<ViewPurchaseOrderMaster>();

                while (dr.Read())
                {
                    ViewPurchaseOrderMaster aMaster = new ViewPurchaseOrderMaster();
                    aMaster.PurchaseOrderNo = dr["PurchaseOrderNo"].ToString();
                    aMaster.RequisitionNo = dr["RequisitionNo"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.SupplierName = dr["SupplierName"].ToString();
                    aMaster.OrderDate = dr["OrderDate"] as DateTime?;
                    aMaster.TotalQty = Convert.ToDecimal(dr["TotalQty"]);
                    aMaster.PurchaseOrderID = Convert.ToInt32(dr["PurchaseOrderID"]);
                    aMaster.TotalDue = Convert.ToDecimal(dr["TotalDue"]);
                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public DataTable GetPurchaseOrderInvoice(string ids)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add( new SqlParameter("@PurchaseOrderID", ids));
                DataTable dt = _accessManager.GetDataTable("sp_Rpt_GetPurchaseOrderInvoice" , aParameters);
                return dt;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public DataTable PurchaseOrderReport(int company, int location, int warehouse, int supplierId, DateTime fromdate, DateTime todate, int rstatus)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                 aParameters.Add(new SqlParameter("@company", company));
                aParameters.Add(new SqlParameter("@location", location));
                aParameters.Add(new SqlParameter("@warehouse", warehouse));
                aParameters.Add(new SqlParameter("@supplierId", supplierId));
                aParameters.Add(new SqlParameter("@fromdate", fromdate));
                aParameters.Add(new SqlParameter("@todate", todate));
                aParameters.Add(new SqlParameter("@rstatus", rstatus));
                DataTable dt = _accessManager.GetDataTable("sp_Rpt_GetPurchaseOrderReport", aParameters);
                return dt;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
       
    }
}
