﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Management.Instrumentation;
using System.Web;
using IGFoodProject.DataManager;
using IGFoodProject.Models;
using IGFoodProject.ViewModel;

namespace IGFoodProject.DAL
{
    public class WarehousePermissionDAL
    {


      
        private  DataAccessManager _accessManager = new DataAccessManager();

        public List<ViewUserWarehouse> GetUsersOfIGFoods()
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_IGFoodsUsers");
                List < ViewUserWarehouse > aList = new List<ViewUserWarehouse>();
                while (dr.Read())
                {
                    ViewUserWarehouse aUserWarehouse = new ViewUserWarehouse();
                    aUserWarehouse.LoginUserId = Convert.ToInt32(dr["LoginUserId"]);
                    aUserWarehouse.Designation = dr["Designation"].ToString();
                    aUserWarehouse.EmpName = dr["EmpName"].ToString();
                    aUserWarehouse.LoginUserName = dr["LoginUserName"].ToString();
                    aList.Add( aUserWarehouse);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<ViewUserWarehouse> GetWhWiseUser(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add( new SqlParameter( "@id" , id));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_WarehouseWiseUser", aParameters);
                List<ViewUserWarehouse> aList = new List<ViewUserWarehouse>();
                while (dr.Read())
                {
                    ViewUserWarehouse aUserWarehouse = new ViewUserWarehouse();
                    aUserWarehouse.LoginUserId = Convert.ToInt32(dr["LoginUserId"]);
                    aUserWarehouse.Designation = dr["Designation"].ToString();
                    aUserWarehouse.EmpName = dr["EmpName"].ToString();
                    aUserWarehouse.LoginUserName = dr["LoginUserName"].ToString();
                    aUserWarehouse.IsActive = Convert.ToBoolean(dr["IsActive"]);
                    aList.Add(aUserWarehouse);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<ViewUserWarehouse> GetUserWiseWarehouse(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ViewUserWarehouse> aList = new List<ViewUserWarehouse>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add( new SqlParameter("@id" , id));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_UserWiseWarehouse", aParameters);
                while (dr.Read())
                {
                    ViewUserWarehouse aUserWarehouse = new ViewUserWarehouse();

                    aUserWarehouse.WareHouseId = Convert.ToInt32(dr["WareHouseId"]);
                    aUserWarehouse.WareHouseName = dr["WareHouseName"].ToString();
                    aUserWarehouse.LocationName = dr["LocationName"].ToString();
                    aUserWarehouse.IsActive =Convert.ToBoolean(dr["IsActive"]);
                    aList.Add( aUserWarehouse);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse SaveUserWisePermission(List<ViewUserWarehouse> aList, string user)
        {
            try
            {

                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                foreach (var item in aList)
                {
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@LoginUserId", item.LoginUserId));
                    aParameters.Add(new SqlParameter("@WareHouseId", item.WareHouseId));
                    aParameters.Add(new SqlParameter("@IsActive", item.IsActive));

                    aResponse.isSuccess = _accessManager.SaveData("sp_Save_UserWiseWHPermission", aParameters);
                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                }
                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        } 
    }
}