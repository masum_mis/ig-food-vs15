﻿using IGFoodProject.DataManager;
using IGFoodProject.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IGFoodProject.DAL
{
    public class ItemCategoryDAL
    {
        DataAccessManager accessManager = new DataAccessManager();
        public List<ItemCategory> GetItemTypeList() 
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ItemCategory> _List = new List<ItemCategory>();

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetItemTypeListForDD");
                while (dr.Read())
                {
                    ItemCategory aInfo = new ItemCategory();
                    aInfo.ItemTypeId = (int)dr["ItemTypeId"];
                    aInfo.ItemTypeName = dr["ItemTypeName"].ToString();

                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public int CheckItemCategory(string ptype) 
        {
            int p_type = 0;
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@type", ptype));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_CheckItemCategory", aSqlParameterList);

                while (dr.Read())
                {
                    p_type = Convert.ToInt32(dr["p_type"]);
                }
            }
            catch (Exception exception)
            {
                //accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
            return p_type;
        }

        public bool SaveItemCategory(ItemCategory aMaster)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@ItemTypeId", aMaster.ItemTypeId));
                aSqlParameterList.Add(new SqlParameter("@ItemCategoryId", aMaster.ItemCategoryId));
                aSqlParameterList.Add(new SqlParameter("@ItemCategoryName", aMaster.ItemCategoryName));
                aSqlParameterList.Add(new SqlParameter("@CreateDate", System.DateTime.Now));
                aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@UpdateDate", System.DateTime.Now));
                aSqlParameterList.Add(new SqlParameter("@UpdateBy", aMaster.UpdateBy));
                aSqlParameterList.Add(new SqlParameter("@IsActive", aMaster.IsActive));
                return accessManager.SaveData("sp_SaveItemCategory", aSqlParameterList);
            }
            catch (Exception exception)
            {
                accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public ItemCategory GetItemCategoryByID(int id)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ItemCategoryId", id));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetItemCategoryByID", aParameters);
                ItemCategory aDetailsView = new ItemCategory();
                while (dr.Read())
                {
                    aDetailsView.ItemTypeId = (int)dr["ItemTypeId"];
                    aDetailsView.ItemCategoryId = (int)dr["ItemCategoryId"];
                    aDetailsView.ItemCategoryName = dr["ItemCategoryName"].ToString();
                    aDetailsView.IsActive = (bool)dr["IsActive"];
                }
                return aDetailsView;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<ItemCategory> GetItemCategoryList() 
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ItemCategory> _List = new List<ItemCategory>();

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetItemCategoryList");
                while (dr.Read())
                {
                    ItemCategory aInfo = new ItemCategory();
                    aInfo.ItemTypeId = (int)dr["ItemTypeId"];
                    aInfo.ItemCategoryId = (int)dr["ItemCategoryId"];
                    aInfo.ItemTypeName = dr["ItemTypeName"].ToString();
                    aInfo.ItemCategoryName = dr["ItemCategoryName"].ToString();
                    if (dr["CreateDate"] != DBNull.Value)
                    {
                        aInfo.CreateDate = Convert.ToDateTime(dr["CreateDate"]);
                    }
                    aInfo.CreateBy = dr["CreateBy"].ToString();

                    aInfo.UpdateBy = dr["UpdateBy"].ToString();
                    if (dr["UpdateDate"] != DBNull.Value)
                    {
                        aInfo.UpdateDate = Convert.ToDateTime(dr["UpdateDate"]);
                    }
                    if (dr["IsActive"] != DBNull.Value)
                    {
                        aInfo.IsActive = Convert.ToBoolean(dr["IsActive"]);
                    }

                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public bool DeleteItemCategory(int id) 
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@ItemCategoryId", id));

                return accessManager.SaveData("sp_DeleteItemCategory", aSqlParameterList);
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
    }
}