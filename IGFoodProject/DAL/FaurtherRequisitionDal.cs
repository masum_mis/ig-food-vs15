﻿using IGFoodProject.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using IGFoodProject.DataManager;
using IGFoodProject.Models;

namespace IGFoodProject.DAL
{
    public class FaurtherRequisitionDal
    {
        DataAccessManager _accessManager = new DataAccessManager();
        public List<ViewProductDescription> LoadProduct(int wareHouseId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@WareHouseId", wareHouseId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetProductForFurtherProcess", parameters);
                List<ViewProductDescription> wareHouseList = new List<ViewProductDescription>();
                while (dr.Read())
                {
                    ViewProductDescription wareHouse = new ViewProductDescription();
                    wareHouse.ProductId = Convert.ToInt32(dr["ProductId"]);
                    wareHouse.ProductName = dr["ProductName"].ToString();
                    wareHouseList.Add(wareHouse);
                }
                return wareHouseList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public tbl_StockInDetail LoadProductStockForProtionIssue(int productId, int FromwareHouseId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@ProductID", productId));
                parameters.Add(new SqlParameter("@WareHouseId", FromwareHouseId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetProductStockForFurtherProcess", parameters);
                tbl_StockInDetail stockDetails = new tbl_StockInDetail();
                while (dr.Read())
                {

                    stockDetails.StockInQty = Convert.ToDecimal(dr["StockInQty"]);
                    stockDetails.StockInKG = Convert.ToDecimal(dr["StockInKG"]);
                    stockDetails.GroupId = Convert.ToInt32(dr["GroupId"]);
                }
                return stockDetails;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public bool SaveRequisition(FurtherProcessRequisitionMaster aMaster)
        {
            bool result = false;
            try

            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                int id = SaveRequisitionMaster(aMaster);


                foreach (FurtherProcessRequisitionDetail grade in aMaster.FurtherProcessRequisitionDetail)
                {
                    List<SqlParameter> gSqlParameterList = new List<SqlParameter>();
                    gSqlParameterList.Add(new SqlParameter("@PortionToFurtherProcessReqId", id));
                    gSqlParameterList.Add(new SqlParameter("@ProductId", grade.ProductId));
                    gSqlParameterList.Add(new SqlParameter("@ProductQty", grade.ProductQty));
                    gSqlParameterList.Add(new SqlParameter("@ProductKg", grade.ProductKg));
                    gSqlParameterList.Add(new SqlParameter("@Remarks", grade.Remarks));
                    //gSqlParameterList.Add(new SqlParameter("@WarehouseId", aMaster.WareHouseId));

                    result = _accessManager.SaveData("sp_SaveFurtherProcessRequisitionDetails", gSqlParameterList);
                }
                //if (result == true)
                //{
                //    List<SqlParameter> dSqlParameterList = new List<SqlParameter>();
                //    dSqlParameterList.Add(new SqlParameter("@PortionToFurtherProcessReqId", id));
                //    result = _accessManager.SaveData("sp_SaveFurtherProcessIssueStockdeduction", dSqlParameterList);
                //}
            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
            return result;
        }
        public int SaveRequisitionMaster(FurtherProcessRequisitionMaster aMaster)
        {
            try
            {
                string empReceived = string.Empty;
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@CompanyId", aMaster.CompanyId));
                aSqlParameterList.Add(new SqlParameter("@LocationId", aMaster.LocationId));
                aSqlParameterList.Add(new SqlParameter("@WareHouseId", aMaster.WareHouseId));
                aSqlParameterList.Add(new SqlParameter("@ToWarehouseId", aMaster.ToWareHouseId));
                aSqlParameterList.Add(new SqlParameter("@RequisitionDate", aMaster.RequisitionDate));
                aSqlParameterList.Add(new SqlParameter("@Comments", aMaster.Comments));
                aSqlParameterList.Add(new SqlParameter("@RequisitionBy", aMaster.RequisitionBy.Split(':')[0].Trim()));
                aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@CreateDate", System.DateTime.Now));
                aSqlParameterList.Add(new SqlParameter("@ProductionDate", aMaster.ProductionDate));


                return _accessManager.SaveDataReturnPrimaryKey("sp_SaveFurtherProcessRequisition", aSqlParameterList);
            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }
        }

        public DataTable GetFurtherProcessRequisitionList()
        {

            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@RequisitionId", 1));
                DataTable dr = _accessManager.GetDataTable("sp_GetFurtherProcessRequisitionList", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public dynamic GetFurtherProcessRequisition(int id,int warehouseId)
        {
            try
            {
                FurtherProcessRequisitionMaster furtherProcessRequisitionMaster = new FurtherProcessRequisitionMaster();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@PortionToFurtherProcessReqId", id));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetFurtherProcessRrequisitionMasterById", parameters);
                while (dr.Read())
                {
                    furtherProcessRequisitionMaster.PortionToFurtherProcessReqId = Convert.ToInt32(dr["PortionToFurtherProcessReqId"]);
                    furtherProcessRequisitionMaster.CompanyId = Convert.ToInt32(dr["CompanyId"]);
                    furtherProcessRequisitionMaster.LocationId = Convert.ToInt32(dr["LocationId"]);
                    furtherProcessRequisitionMaster.WareHouseId = Convert.ToInt32(dr["WareHouseId"]);
                    furtherProcessRequisitionMaster.ToWareHouseId = Convert.ToInt32(dr["ToWareHouseId"]);
                    furtherProcessRequisitionMaster.RequisitionDate = Convert.ToDateTime(dr["RequisitionDate"]);
                    furtherProcessRequisitionMaster.ProductionDate = Convert.ToDateTime(dr["ProductionDate"]);
                    furtherProcessRequisitionMaster.Comments = dr["Comments"].ToString();
                    furtherProcessRequisitionMaster.RequisitionBy = dr["RequisitionBy"].ToString();
                }
                _accessManager.SqlConnectionClose();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aparameters = new List<SqlParameter>();
                aparameters.Add(new SqlParameter("@PortionToFurtherProcessReqId", id));
                aparameters.Add(new SqlParameter("@WarehouseId", warehouseId));
                List<FurtherProcessRequisitionDetail> furtherProcessRequisitionDetails = new List<FurtherProcessRequisitionDetail>();
                SqlDataReader dar = _accessManager.GetSqlDataReader("sp_GetFurtherProcessDetailsByMasterId", aparameters);
                while (dar.Read())
                {
                    FurtherProcessRequisitionDetail afurtherRequisitionDetail = new FurtherProcessRequisitionDetail();
                    afurtherRequisitionDetail.PortionToFurtherProcessReqDetailId = Convert.ToInt32(dar["PortionToFurtherProcessReqDetailId"]);
                    afurtherRequisitionDetail.PortionToFurtherProcessReqId = Convert.ToInt32(dar["PortionToFurtherProcessReqId"]);
                    afurtherRequisitionDetail.ProductId = Convert.ToInt32(dar["ProductId"]);
                    afurtherRequisitionDetail.GroupId = Convert.ToInt32(dar["GroupId"]);
                    afurtherRequisitionDetail.ProductQty = Convert.ToDecimal(dar["ProductQty"]);
                    afurtherRequisitionDetail.ProductKg = Convert.ToDecimal(dar["ProductKg"]);
                    afurtherRequisitionDetail.Remarks = dar["Remarks"].ToString();
                    afurtherRequisitionDetail.ProductName = dar["ProductName"].ToString();
                    afurtherRequisitionDetail.StockQty = Convert.ToDecimal(dar["StockInQty"]);
                    afurtherRequisitionDetail.StockInKg = Convert.ToDecimal(dar["StockInKG"]);
                    if(dar["UnitPrice"] !=DBNull.Value)
                    {
                        afurtherRequisitionDetail.UnitPrice = Convert.ToDecimal(dar["UnitPrice"]);
                    }
                    else
                    {
                        afurtherRequisitionDetail.UnitPrice = 0;
                    }
                   

                    furtherProcessRequisitionDetails.Add(afurtherRequisitionDetail);
                }
                furtherProcessRequisitionMaster.FurtherProcessRequisitionDetail = furtherProcessRequisitionDetails;

                return furtherProcessRequisitionMaster;
            }


            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public bool SaveFurtherIssue(FurtherProcessIssueMaster aMaster)
        {
            try
            {
                var result = false;
                string empReceived = string.Empty;
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@CompanyId", aMaster.CompanyId));
                aSqlParameterList.Add(new SqlParameter("@LocationId", aMaster.LocationId));
                aSqlParameterList.Add(new SqlParameter("@WareHouseId", aMaster.WareHouseId));
                aSqlParameterList.Add(new SqlParameter("@ToWareHouseId", aMaster.ToWareHouseId));
                aSqlParameterList.Add(new SqlParameter("@RequisitionDate", aMaster.RequisitionDate));
                aSqlParameterList.Add(new SqlParameter("@ProductionDate", aMaster.ProductionDate));
                aSqlParameterList.Add(new SqlParameter("@Comments", aMaster.Comments));
                aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.IssueBy));
                aSqlParameterList.Add(new SqlParameter("@CreateDate", aMaster.IssueDate));
                aSqlParameterList.Add(new SqlParameter("@RequisitionBy", aMaster.RequisitionBy));
                aSqlParameterList.Add(new SqlParameter("@RequisitionId", aMaster.PortionToFurtherProcessReqId));
                var Key= _accessManager.SaveDataReturnPrimaryKey("sp_SaveFurtherProcessIssue", aSqlParameterList);

                foreach (FurtherProcessIssueDetail grade in aMaster.FurtherProcessIssueDetail)
                {
                    List<SqlParameter> gSqlParameterList = new List<SqlParameter>();
                    gSqlParameterList.Add(new SqlParameter("@PortionToFurtherProcessReqId", Key));
                    gSqlParameterList.Add(new SqlParameter("@ProductId", grade.ProductId));
                    gSqlParameterList.Add(new SqlParameter("@ProductQty", grade.IssueQty));
                    gSqlParameterList.Add(new SqlParameter("@ProductKg", grade.IssueKg));
                    gSqlParameterList.Add(new SqlParameter("@Remarks", grade.Remarks));
                    gSqlParameterList.Add(new SqlParameter("@WarehouseId", aMaster.WareHouseId));
                    gSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.IssueBy));
                    gSqlParameterList.Add(new SqlParameter("@CreateDate", aMaster.IssueDate));
                    gSqlParameterList.Add(new SqlParameter("@RequisitionDate", aMaster.RequisitionDate));

                    gSqlParameterList.Add(new SqlParameter("@FurtherProcessReqDetailsId", grade.ReqDetailId));

                     result = _accessManager.SaveData("sp_SaveFurtherProcessIssueDetails", gSqlParameterList);
                }
               
                _accessManager.SqlConnectionClose();
                return result;
            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }
        }
    }
}