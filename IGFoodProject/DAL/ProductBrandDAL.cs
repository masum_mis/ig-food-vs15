﻿using IGFoodProject.DataManager;
using IGFoodProject.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.DAL
{
    public class ProductBrandDAL
    {
        DataAccessManager accessManager = new DataAccessManager();
        public List<tbl_ProductBrand> GetBrandList()
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<tbl_ProductBrand> _List = new List<tbl_ProductBrand>();

                //List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetProductBrand");
                while (dr.Read())
                {
                    tbl_ProductBrand aInfo = new tbl_ProductBrand();
                    aInfo.BrandId = (int)dr["BrandId"];
                    aInfo.BrandName = dr["BrandName"].ToString();
                    if (dr["CreateDate"] != DBNull.Value)
                    {
                        aInfo.CreateDate = Convert.ToDateTime(dr["CreateDate"]);
                    }
                    aInfo.CreateBy = dr["CreateBy"].ToString();

                    aInfo.UpdateBy = dr["UpdateBy"].ToString();
                    if (dr["UpdateDate"] != DBNull.Value)
                    {
                        aInfo.UpdateDate = Convert.ToDateTime(dr["UpdateDate"]);
                    }
                    if (dr["IsActive"] != DBNull.Value)
                    {
                        aInfo.IsActive = Convert.ToBoolean(dr["IsActive"]);
                    }


                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public int CheckBrand(string group)
        {
            int pgroup = 0;
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@group", group));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_CheckBrand", aSqlParameterList);

                while (dr.Read())
                {
                    pgroup = Convert.ToInt32(dr["pgroup"]);
                }
            }
            catch (Exception exception)
            {
                //accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
            return pgroup;
        }

        public bool SaveBrand(tbl_ProductBrand aMaster)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@BrandId", aMaster.BrandId));
                aSqlParameterList.Add(new SqlParameter("@BrandName", aMaster.BrandName));
                aSqlParameterList.Add(new SqlParameter("@CreateDate", System.DateTime.Now));
                aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@UpdateDate", System.DateTime.Now));
                aSqlParameterList.Add(new SqlParameter("@UpdateBy", aMaster.UpdateBy));
                aSqlParameterList.Add(new SqlParameter("@IsActive", aMaster.IsActive));



                return accessManager.SaveData("sp_SaveBrand", aSqlParameterList);
            }
            catch (Exception exception)
            {
                accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public tbl_ProductBrand GetBrandForEdit(int id)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@BrandId", id));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetBrandByID", aParameters);
                tbl_ProductBrand aDetailsView = new tbl_ProductBrand();
                while (dr.Read())
                {


                    aDetailsView.BrandId = (int)dr["BrandId"];
                    aDetailsView.BrandName = dr["BrandName"].ToString();
                    aDetailsView.IsActive = (bool)dr["IsActive"];


                }
                return aDetailsView;

            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public bool DeleteBrand(int id)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@BrandId", id));

                return accessManager.SaveData("sp_DeleteBrand", aSqlParameterList);
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
    }
}
