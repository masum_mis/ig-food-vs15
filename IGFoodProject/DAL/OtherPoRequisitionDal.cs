﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using IGFoodProject.DataManager;
using IGFoodProject.Models;

namespace IGFoodProject.DAL
{
    public class OtherPoRequisitionDal
    {
        DataAccessManager _accessManager = new DataAccessManager();
        public ResultResponse SaveRequisition(OtherPoRequisitionMaster aMaster)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                ResultResponse aResponse = new ResultResponse();

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@MasterId", aMaster.RequisitionMasterId));
                aSqlParameterList.Add(new SqlParameter("@CompanyId", 7));
                aSqlParameterList.Add(new SqlParameter("@WarehouseId", aMaster.WarehouseId));

                aSqlParameterList.Add(new SqlParameter("@RequisitionBy", aMaster.RequisitionBy));
                aSqlParameterList.Add(new SqlParameter("@RequisitionDate", aMaster.RequisitionDate));

                aSqlParameterList.Add(new SqlParameter("@PoDate", aMaster.ExpectedPoDate));

                aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@Remarks", aMaster.Remarks));
                aSqlParameterList.Add(new SqlParameter("@eflag", aMaster.eflag));
                aSqlParameterList.Add(new SqlParameter("@Tolerance", aMaster.TolerancePercent));
                aSqlParameterList.Add(new SqlParameter("@IsLocal", aMaster.IsLocal));
                aSqlParameterList.Add(new SqlParameter("@TotalQty", aMaster.PoRequisitionDetailses.Sum(n=>n.RequisitionQty)));
                if (aMaster.eflag == "ed")
                {
                    aMaster.IsApprove = false;
                }
                else
                {
                    aMaster.IsApprove = false;
                }
                if (aMaster.eflag == "ap")
                {
                    aSqlParameterList.Add(new SqlParameter("@IsApprove", true));
                    aSqlParameterList.Add(new SqlParameter("@ApproveBy", aMaster.CreateBy));
                    aSqlParameterList.Add(new SqlParameter("@ApproveDate", DateTime.Now));
                }
                else
                {
                    aSqlParameterList.Add(new SqlParameter("@IsApprove", aMaster.IsApprove));
                    aSqlParameterList.Add(new SqlParameter("@RejectBy", aMaster.CreateBy));
                    aSqlParameterList.Add(new SqlParameter("@RejectDate", DateTime.Now));
                }



                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("SavePoRequisitionMaster", aSqlParameterList);
                if (aResponse.pk > 0)
                {
                    aResponse.isSuccess = SaveRequisitionDetails(aMaster.PoRequisitionDetailses, aResponse.pk);

                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                  

                }
                return aResponse;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        private bool SaveRequisitionDetails(List<OtherPoRequisitionDetails> reqDetails, int pk)
        {
            try
            {
                bool result = false;
                foreach (var item in reqDetails)
                {
                    List<SqlParameter> aParameters = new List<SqlParameter>();

                    aParameters.Add(new SqlParameter("@ReqMasterId", pk));
                    aParameters.Add(new SqlParameter("@detailsId", item.RequisitionDetailsId));
                    aParameters.Add(new SqlParameter("@ItemId", item.ItemId));
                    aParameters.Add(new SqlParameter("@RequisitionQty", item.RequisitionQty));
              
                    aParameters.Add(new SqlParameter("@Remarks", item.Remarks));
                    aParameters.Add(new SqlParameter("@IsDelete", item.IsDelete));
                    aParameters.Add(new SqlParameter("@eflag", item.eflag));
                    aParameters.Add(new SqlParameter("@ApprovedQty", item.ApprovedQty));
                    aParameters.Add(new SqlParameter("@PurposeID", item.PurposeID));
                    result = _accessManager.SaveData("SavePoRequisitionDetails", aParameters);

                    if (result == false)
                    {
                        break;
                    }

                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public ResultResponse Reject( int id,string createby)
        {
            try
            {
              
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                ResultResponse aResponse = new ResultResponse();
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@MasterId",id));
                aSqlParameterList.Add(new SqlParameter("@RejectBy", createby));
                aSqlParameterList.Add(new SqlParameter("@RejectDate", DateTime.Now));
                aResponse.isSuccess = _accessManager.SaveData("RejectPoRequisition", aSqlParameterList);

                if (aResponse.isSuccess == false)
                {
                    _accessManager.SqlConnectionClose(true);
                }
                return aResponse;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<OtherPoRequisitionMaster> RequisitionList(DateTime? fromdate, DateTime? todate, int warehouse, string reqById)
        {
            try
            {
                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-2);

                }

                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<OtherPoRequisitionMaster> aList = new List<OtherPoRequisitionMaster>();
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@WareHouseId", warehouse));
                aParameters.Add(new SqlParameter("@RequisitionBy", reqById));

                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetPoRequisitionList", aParameters);
                while (dr.Read())
                {
                    OtherPoRequisitionMaster aMaster = new OtherPoRequisitionMaster();
                    aMaster.RequisitionMasterId = Convert.ToInt32(dr["RequisitionMasterId"]);
                 
                    aMaster.TotalRequisitionQuantity = Convert.ToDecimal(dr["TotalQty"]);
                    aMaster.RequisitionNo = dr["RequisitionNo"].ToString();
                    aMaster.PurchaseStatus = dr["PurchaseStatus"].ToString();

                    aMaster.RequisitionDate = Convert.ToDateTime(dr["RequisitionDate"]);
                    aMaster.ExpectedPoDate = Convert.ToDateTime(dr["ExpctedPoDate"]);
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.RequisitionBy = dr["RequisitionByName"].ToString();
                    aMaster.Remarks = dr["Remarks"].ToString();
                    aMaster.IsApprove = Convert.ToBoolean(dr["IsApprove"]);
                    aMaster.IsLocal = Convert.ToBoolean(dr["IsLocal"]);



                    aList.Add(aMaster);
                }

                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<OtherPoRequisitionMaster> PurchaseRequisitionPrintList(DateTime? fromdate, DateTime? todate, int warehouse, string reqById)
        {
            try
            {
                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-2);

                }

                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<OtherPoRequisitionMaster> aList = new List<OtherPoRequisitionMaster>();
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@WareHouseId", warehouse));
                aParameters.Add(new SqlParameter("@RequisitionBy", reqById));

                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetPoRequisitionPrintList", aParameters);
                while (dr.Read())
                {
                    OtherPoRequisitionMaster aMaster = new OtherPoRequisitionMaster();
                    aMaster.RequisitionMasterId = Convert.ToInt32(dr["RequisitionMasterId"]);
                    aMaster.TotalRequisitionQuantity = Convert.ToDecimal(dr["TotalQty"]);
                    aMaster.RequisitionNo = dr["RequisitionNo"].ToString();

                    aMaster.RequisitionDate = Convert.ToDateTime(dr["RequisitionDate"]);
                    aMaster.ExpectedPoDate = Convert.ToDateTime(dr["ExpctedPoDate"]);
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.RequisitionBy = dr["RequisitionByName"].ToString();
                    aMaster.Remarks = dr["Remarks"].ToString();
                    aMaster.IsApprove = Convert.ToBoolean(dr["IsApprove"]);



                    aList.Add(aMaster);
                }

                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<OtherPoRequisitionDetails> RequisitionDetailsById(int id)
        {
            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<OtherPoRequisitionDetails> aList = new List<OtherPoRequisitionDetails>();
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@Id", id));

                SqlDataReader dr = _accessManager.GetSqlDataReader("GetPoRequisitionById", aParameters);
                while (dr.Read())
                {
                    OtherPoRequisitionDetails aMaster = new OtherPoRequisitionDetails();




                    aMaster.ItemCode = dr["ItemCode"].ToString();
                    aMaster.ItemName = dr["ItemName"].ToString();
                    aMaster.ItemDescription = dr["ItemDescription"].ToString();
                    aMaster.Remarks = dr["Remarks"].ToString();
                    aMaster.UOMName = dr["UOMName"].ToString();
                
                    aMaster.RequisitionQty = (dr["RequisitionQty"] == DBNull.Value) ? 0 : ((decimal)dr["RequisitionQty"]);
                    aMaster.RemainingPoQty = (dr["RemainingPoQty"] == DBNull.Value) ? 0 : ((decimal)dr["RemainingPoQty"]);
                    aMaster.ApprovedQty = (dr["ApprovedQty"] == DBNull.Value) ? 0 : ((decimal)dr["ApprovedQty"]);
                   
                    aMaster.ItemId = Convert.ToInt32(dr["ItemId"]);
                    aMaster.PurposeID = Convert.ToInt32(dr["PurposeID"]);
                    aMaster.UOMId = Convert.ToInt32(dr["UOMId"]);
                    aMaster.RequisitionDetailsId = Convert.ToInt32(dr["RequisitionDetailsId"]);
                    aMaster.RequisitionMasterId = Convert.ToInt32(dr["RequisitionMasterId"]);

                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public OtherPoRequisitionMaster GetRequisitionMasterById(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                OtherPoRequisitionMaster aMaster = new OtherPoRequisitionMaster();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@Id", id));

                SqlDataReader dr = _accessManager.GetSqlDataReader("GetPoRequisitionMasterById", aParameters);
                while (dr.Read())
                {
                    aMaster.RequisitionMasterId = Convert.ToInt32(dr["RequisitionMasterId"]);
                    aMaster.CompanyId = Convert.ToInt32(dr["CompanyId"]);
                    aMaster.WarehouseId = Convert.ToInt32(dr["WarehouseId"]);
                    aMaster.LocationId = Convert.ToInt32(dr["LocationId"]);
                    aMaster.TolerancePercent = Convert.ToInt32(dr["Tolerance"]);
                    aMaster.TotalRequisitionQuantity = Convert.ToInt32(dr["TotalQty"]);
                    aMaster.TotalQtyWithTolerance = Convert.ToInt32(dr["TotalQtyWithTolerance"]);
         
                    aMaster.RequisitionDate = Convert.ToDateTime(dr["RequisitionDate"]);
                    aMaster.ExpectedPoDate = Convert.ToDateTime(dr["ExpctedPoDate"]);
                    aMaster.RequisitionNo = dr["RequisitionNo"].ToString();
                    aMaster.RequisitionBy = dr["RequisitionBy"].ToString();
                    aMaster.Remarks = dr["Remarks"].ToString();
                    aMaster.CreateBy = dr["CreateBy"].ToString();
                    aMaster.CreateDate = Convert.ToDateTime(dr["CreateDate"]);
                    aMaster.IsApprove = Convert.ToBoolean(dr["IsApprove"]);
                    aMaster.IsLocal = Convert.ToBoolean(dr["IsLocal"]);
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.LocationName = dr["LocationName"].ToString();
                   
                    aMaster.RequisitionByName = dr["RequisitionByName"].ToString();
                }
                return aMaster;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<OtherPoRequisitionMaster> RequisitionApproveList(DateTime? fromdate, DateTime? todate, int warehouse, string reqById)
        {

            try
            {
                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-2);

                }

                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<OtherPoRequisitionMaster> aList = new List<OtherPoRequisitionMaster>();
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@WareHouseId", warehouse));
                aParameters.Add(new SqlParameter("@RequisitionBy", reqById));

                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetPoRequisitionApprovalList", aParameters);
                while (dr.Read())
                {
                    OtherPoRequisitionMaster aMaster = new OtherPoRequisitionMaster();
                    aMaster.RequisitionMasterId = Convert.ToInt32(dr["RequisitionMasterId"]);
                    aMaster.RequisitionNo = dr["RequisitionNo"].ToString();
                    aMaster.Remarks = dr["Remarks"].ToString();
                    aMaster.RequisitionDate = Convert.ToDateTime(dr["RequisitionDate"]);
                    aMaster.ExpectedPoDate = Convert.ToDateTime(dr["ExpctedPoDate"]);
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.RequisitionBy = dr["RequisitionByName"].ToString();
                    aMaster.IsApprove = Convert.ToBoolean(dr["IsApprove"]);



                    aList.Add(aMaster);
                }

                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<OtherPoRequisitionMaster> ApprovedRequisitionList(DateTime? fromdate, DateTime? todate, int? warehouse, string reqById, string UserId)
        {

            try
            {
                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-2);

                }

                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<OtherPoRequisitionMaster> aList = new List<OtherPoRequisitionMaster>();
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@WareHouseId", warehouse));
                aParameters.Add(new SqlParameter("@RequisitionBy", reqById));
                aParameters.Add(new SqlParameter("@UserId", UserId));

                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetPendingPoRequisition", aParameters);
                while (dr.Read())
                {
                    OtherPoRequisitionMaster aMaster = new OtherPoRequisitionMaster();
                    aMaster.RequisitionMasterId = Convert.ToInt32(dr["RequisitionMasterId"]);
                    aMaster.RequisitionNo = dr["RequisitionNo"].ToString();

                    aMaster.RequisitionDate = Convert.ToDateTime(dr["RequisitionDate"]);
                    aMaster.ExpectedPoDate = Convert.ToDateTime(dr["ExpctedPoDate"]);
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.RequisitionBy = dr["RequisitionByName"].ToString();
                    aMaster.IsApprove = Convert.ToBoolean(dr["IsApprove"]);
                    aMaster.IsLocal = Convert.ToBoolean(dr["IsLocal"]);



                    aList.Add(aMaster);
                }

                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }



        public DataTable GetRequisitionReport( int location, int warehouse, DateTime fromdate, DateTime todate, int rstatus)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
       
                parameters.Add(new SqlParameter("@LocationId", location));
                parameters.Add(new SqlParameter("@WarehouseId", warehouse));
                parameters.Add(new SqlParameter("@FromDate", fromdate));
                parameters.Add(new SqlParameter("@ToDate", todate));
                parameters.Add(new SqlParameter("@Status", rstatus));

                DataTable dr = _accessManager.GetDataTable("Rpt_PurchaseRequisitionListRpt", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public DataTable GetRequisitionInvoiceData(string ids)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();

                parameters.Add(new SqlParameter("@ids", ids));
                DataTable dt = _accessManager.GetDataTable("rpt_PurchaseRequisitionPrint", parameters);
                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
    }
}