﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using IGFoodProject.DataManager;
using IGFoodProject.Models;
using IGFoodProject.ViewModel;

namespace IGFoodProject.DAL
{
    public class FurtherProcessedDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();
        public List<DBIssuePortionMaster> IssueInformation(int warehouseid)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@WarehouseId", warehouseid));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetDBIssueNoForFurther", parameters);
                List<DBIssuePortionMaster> wareHouseList = new List<DBIssuePortionMaster>();
                while (dr.Read())
                {


                    DBIssuePortionMaster wareHouse = new DBIssuePortionMaster();
                    wareHouse.DBIssueMasterID = Convert.ToInt32(dr["DBIssueMasterID"]);
                    wareHouse.IssueNo = dr["IssueNo"].ToString();
                    wareHouseList.Add(wareHouse);
                }
                return wareHouseList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public int SaveFurtherProcessedStockMaster(tbl_StockInMaster aMaster)
        {
            try
            {
                string empReceived = string.Empty;
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@StockInMasterID", aMaster.StockInMasterID));
                aSqlParameterList.Add(new SqlParameter("@StockInDate", aMaster.StockInDate));
                aSqlParameterList.Add(new SqlParameter("@StockInBy", aMaster.StockInBy.Split(':')[0].Trim()));
                aSqlParameterList.Add(new SqlParameter("@WareHouseId", aMaster.WareHouseId));
                aSqlParameterList.Add(new SqlParameter("@IsOpening", aMaster.IsOpening));
                aSqlParameterList.Add(new SqlParameter("@Comments", aMaster.Comments));

                aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@CreateDate", System.DateTime.Now));

                aSqlParameterList.Add(new SqlParameter("@FurtherProcessStockInMasterLogId", aMaster.FurtherProcessStockInMasterLogId));

                aSqlParameterList.Add(new SqlParameter("@CompanyId", aMaster.CompanyId));
                aSqlParameterList.Add(new SqlParameter("@LocationId", aMaster.LocationId));
                aSqlParameterList.Add(new SqlParameter("@GroupId", aMaster.GroupId));


                return _accessManager.SaveDataReturnPrimaryKey("sp_SaveStockInMaster_FurtherProcessed", aSqlParameterList);
            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }
        }

        public bool SaveFurtherProcessedStockDetails(tbl_StockInMaster aMaster)
        {
            bool result = false;
            try

            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                int id = SaveFurtherProcessedStockMaster(aMaster);


                foreach (tbl_StockInDetail grade in aMaster.aDetail)
                {
                    List<SqlParameter> gSqlParameterList = new List<SqlParameter>();

                    gSqlParameterList.Add(new SqlParameter("@StockInDetailID", grade.StockInDetailID));
                    gSqlParameterList.Add(new SqlParameter("@StockInMasterID", id));
                    gSqlParameterList.Add(new SqlParameter("@ProductID", grade.ProductID));
                    gSqlParameterList.Add(new SqlParameter("@StockInDate", aMaster.StockInDate));
                    gSqlParameterList.Add(new SqlParameter("@StockInQty", grade.StockInQty));
                    
                    gSqlParameterList.Add(new SqlParameter("@ExpireDate", grade.ExpireDate));
                    gSqlParameterList.Add(new SqlParameter("@Remark", grade.Remark));
                    gSqlParameterList.Add(new SqlParameter("@UnitPrice", grade.UnitPrice));
                    gSqlParameterList.Add(new SqlParameter("@PUnitPrice", grade.PUnitPrice));
                    gSqlParameterList.Add(new SqlParameter("@IsOpening", aMaster.IsOpening)); 
                    gSqlParameterList.Add(new SqlParameter("@FurtherProcessStockInDetailLogID", grade.FurtherProcessStockInDetailLogID));

                    result = _accessManager.SaveData("sp_SaveStockInDetail_FurtherProcess", gSqlParameterList);
                }




            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
            return result;
        }

        public decimal GetSalablePriceByID(int productid, DateTime pdate)
        {
            decimal costingprice = 0;
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@productid", productid));
                parameters.Add(new SqlParameter("@challanDate", pdate));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetMatricPrice_FurtherProcess", parameters);

                while (dr.Read())
                {
                    if (dr["CostingPrice"] != DBNull.Value)
                    {
                        costingprice = Convert.ToDecimal(dr["CostingPrice"]);
                    }
                    else
                    {
                        costingprice = 0;
                    }
                }
                return costingprice;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }


        public int SaveFurtherProcessedStockMaster_Log(tbl_FurtherProcessStockInMaster_Log aMaster)
        {
            try
            {
                string empReceived = string.Empty;
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@FurtherProcessStockInMasterLogId", aMaster.FurtherProcessStockInMasterLogId));
                aSqlParameterList.Add(new SqlParameter("@StockInDate", aMaster.StockInDate));
                aSqlParameterList.Add(new SqlParameter("@StockInBy", aMaster.StockInBy.Split(':')[0].Trim()));
                aSqlParameterList.Add(new SqlParameter("@WareHouseId", aMaster.WareHouseId));
               
                aSqlParameterList.Add(new SqlParameter("@Comments", aMaster.Comments));

                aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@CreateDate", System.DateTime.Now));

                aSqlParameterList.Add(new SqlParameter("@FurtherProcessIssueMasterId", aMaster.FurtherProcessIssueMasterId));

                aSqlParameterList.Add(new SqlParameter("@CompanyId", aMaster.CompanyId));
                aSqlParameterList.Add(new SqlParameter("@LocationId", aMaster.LocationId));
                aSqlParameterList.Add(new SqlParameter("@GroupId", aMaster.GroupId));


                return _accessManager.SaveDataReturnPrimaryKey("sp_SaveStockInMaster_FurtherProcessed_Log", aSqlParameterList);
            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }
        }

        public bool SaveFurtherProcessedStockDetails_Log(tbl_FurtherProcessStockInMaster_Log aMaster)
        {
            bool result = false;
            try

            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                int id = SaveFurtherProcessedStockMaster_Log(aMaster);


                foreach (tbl_FurtherProcessStockInDetail_Log grade in aMaster.aDetail)
                {
                    List<SqlParameter> gSqlParameterList = new List<SqlParameter>();

                    gSqlParameterList.Add(new SqlParameter("@FurtherProcessStockInDetailLogID", grade.FurtherProcessStockInDetailLogID));
                    gSqlParameterList.Add(new SqlParameter("@FurtherProcessStockInMasterLogId", id));
                    gSqlParameterList.Add(new SqlParameter("@ProductID", grade.ProductID));
                    gSqlParameterList.Add(new SqlParameter("@StockInDate", aMaster.StockInDate));
                    gSqlParameterList.Add(new SqlParameter("@StockInQty", grade.StockInQty));

                    gSqlParameterList.Add(new SqlParameter("@ExpireDate", grade.ExpireDate));
                    gSqlParameterList.Add(new SqlParameter("@Remark", grade.Remark));
                    gSqlParameterList.Add(new SqlParameter("@UnitPrice", grade.UnitPrice));
                    gSqlParameterList.Add(new SqlParameter("@PUnitPrice", grade.PUnitPrice));
                  


                    result = _accessManager.SaveData("sp_SaveStockInDetail_FurtherProcess_Log", gSqlParameterList);
                }




            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
            return result;
        }

        public List<ViewFurtherStockInListForApproval> GetFurtherProcessStockInListForApproval(string fromdate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<ViewFurtherStockInListForApproval> aList = new List<ViewFurtherStockInListForApproval>();
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@StockIndate", fromdate));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetFurtherProcessStockLog", aParameters);
                while (dr.Read())
                {
                    ViewFurtherStockInListForApproval aMaster = new ViewFurtherStockInListForApproval();

                    aMaster.FurtherProcessStockInMasterLogId = Convert.ToInt32(dr["FurtherProcessStockInMasterLogId"]);
                    aMaster.StockInDate = dr["StockInDate"].ToString();
                    aMaster.StockInBy = dr["StockInBy"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.StockInQty = Convert.ToDecimal(dr["StockInQty"]);
                    
                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public ViewDressBirdMasterLog LoadLogMaster(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@FurtherProcessStockInMasterLogId", id));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetFurtherMasterStockLog", aParameters);
                ViewDressBirdMasterLog wareHouse = new ViewDressBirdMasterLog();
                while (dr.Read())
                {

                    wareHouse.DressBirdStockInMasterLogID = Convert.ToInt32(dr["FurtherProcessStockInMasterLogId"]);
                    wareHouse.CompanyID = Convert.ToInt32(dr["CompanyID"]);
                    wareHouse.DressLocation = Convert.ToInt32(dr["DressLocation"]);
                    wareHouse.DressWarehouse = Convert.ToInt32(dr["ToWareHouseId"]);
                    wareHouse.LocationId = Convert.ToInt32(dr["LocationId"]);
                    wareHouse.WareHouseId = Convert.ToInt32(dr["WareHouseId"]);


                }
                return wareHouse;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

        public List<tbl_DressBirdStockInDetail_Log> GetFurtherProcessDetailLogByID(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@FurtherProcessStockInMasterLogId", id));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetFurtherProcessDetailLogById", parameters);
                List<tbl_DressBirdStockInDetail_Log> aList = new List<tbl_DressBirdStockInDetail_Log>();
                while (dr.Read())
                {
                    tbl_DressBirdStockInDetail_Log supplier = new tbl_DressBirdStockInDetail_Log();
                    supplier.DressBirdStockInDetailLogID = Convert.ToInt32(dr["FurtherProcessStockInDetailLogID"]);
                    supplier.DressBirdStockInMasterLogID = Convert.ToInt32(dr["FurtherProcessStockInMasterLogId"]);
                   
                    supplier.ProductID = Convert.ToInt32(dr["ProductID"]);
                    if (dr["StockInQty"] != DBNull.Value)
                    {
                        supplier.StockInQty = Convert.ToDecimal(dr["StockInQty"]);
                    }
                    
                    supplier.UnitPrice = dr["UnitPrice"] == DBNull.Value ? 0 : Convert.ToDecimal(dr["UnitPrice"]);
                    supplier.Remark = dr["Remark"].ToString();
                    supplier.ExpireDate = dr["ExpireDate"].ToString();
                    aList.Add(supplier);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public List<FurtherProcessIssueDetail> GetFurtherIssueDetailsByIssueId(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_IssueDetailsByFurtherIssueId", aParameters);
                List<FurtherProcessIssueDetail> aList = new List<FurtherProcessIssueDetail>();
                while (dr.Read())
                {
                    FurtherProcessIssueDetail aDetail = new FurtherProcessIssueDetail();
                    aDetail.ProductId = Convert.ToInt32(dr["ProductID"]);
                    aDetail.IssueQty = Convert.ToDecimal(dr["IssueQty"]);
                    aDetail.IssueKg = Convert.ToDecimal(dr["IssueKg"]);
                    aDetail.ProductName = dr["ProductName"].ToString();

                    aList.Add(aDetail);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
    }
}