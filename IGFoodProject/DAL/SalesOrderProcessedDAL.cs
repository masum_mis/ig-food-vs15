﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.DynamicData.ModelProviders;
using System.Web.Mvc;
using IGFoodProject.DataManager;
using IGFoodProject.Models;
using IGFoodProject.ViewModel;

namespace IGFoodProject.DAL
{
    public class SalesOrderProcessedDAL
    {

        DataAccessManager _accessManager = new DataAccessManager();


        public string GetSalesPersonPhone(int empNo)
        {
            try
            {
                string PhoneNo = "";
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@empNo", empNo));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_SalesPersonPhoneNo", aParameters);
                while (dr.Read())
                {
                    PhoneNo = dr["PhoneNo"].ToString();
                }
                return PhoneNo;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

       public ViewProductDetailsSales GetProductDetails(int id, DateTime? orderdate, int customertype, int wareHouseId = 0, int customerId = 0)
        //public ViewProductDetailsSales GetProductDetails(int id, DateTime? orderdate, int customertype, int wareHouseId = 0)
        {
            try
            {
                ViewProductDetailsSales aDetails = new ViewProductDetailsSales();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                aParameters.Add(new SqlParameter("@wareHouseId", wareHouseId));
                aParameters.Add(new SqlParameter("@orderdate", orderdate));
                aParameters.Add(new SqlParameter("@customertypeid", customertype));
                aParameters.Add(new SqlParameter("@customerId", customerId));
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_ProductStockDetailsProcessed", aParameters);
                while (dr.Read())
                {
                    aDetails.ProductID = Convert.ToInt32(dr["ProductID"]);
                    aDetails.StockQty = DBNull.Value.Equals(dr["StockQty"]) ? 0 : Convert.ToDecimal(dr["StockQty"]);
                    aDetails.StockPrice = DBNull.Value.Equals(dr["StockPrice"]) ? 0 : Convert.ToDecimal(dr["StockPrice"]);
                    aDetails.StockKg = DBNull.Value.Equals(dr["StockKg"]) ? 0 : Convert.ToDecimal(dr["StockKg"]);
                    aDetails.groupid = Convert.ToInt32(dr["GroupId"]);

                    aDetails.BasePrice = DBNull.Value.Equals(dr["BasePrice"]) ? 0 : Convert.ToDecimal(dr["BasePrice"]);
                    aDetails.IsBulk = Convert.ToBoolean(dr["IsBulk"]);
                    aDetails.MTRetention = DBNull.Value.Equals(dr["MTRetention"]) ? 0 : Convert.ToDecimal(dr["MTRetention"]);
                    aDetails.PerKgStdQty = DBNull.Value.Equals(dr["PerKgStdQty"]) ? 0 : Convert.ToDecimal(dr["PerKgStdQty"]);
                    aDetails.Incentive = DBNull.Value.Equals(dr["Incentive"]) ? 0 : Convert.ToDecimal(dr["Incentive"]);

                }
                return aDetails;
            }
            catch (Exception ex)
            {

                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
       

        public decimal GetProductSalePrice(int id, decimal price)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                aParameters.Add(new SqlParameter("@price", price));
                decimal CostingPrice = 0;
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_SalePriceProcessedProduct", aParameters);
                while (dr.Read())
                {
                    CostingPrice = Convert.ToDecimal(dr["CostingPrice"]);
                }
                return CostingPrice;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public ResultResponse SaveSalesOrderProcessed(SalesOrderProcessedMaster aMaster, string user)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);


                if(aMaster.eflag=="ed")
                {
                    var addIn = aMaster.SalesDetails.Find(a => a.OperationIdAdd == 3); // Add In Challan if add in challan Is Approve =1
                    if (addIn != null)
                    {
                        aMaster.IsApprove = 1;
                    }
                    var deletIn = aMaster.SalesDetails.Find(a => a.OperationIdDelete == 3); // Delete from  Challan if delete in challan Is Approve =1
                    if (deletIn != null)
                    {
                        aMaster.IsApprove = 1;
                    }
                }
                else
                {
                    aMaster.IsApprove = 1;
                }

                


                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CustomerId", aMaster.CustomerId));
                aParameters.Add(new SqlParameter("@SalesOrderProcessedMasterId", aMaster.SalesOrderProcessedMasterId));
                aParameters.Add(new SqlParameter("@OrderType", aMaster.OrderType));
                if (aMaster.IsEmployee == true)
                {
                    aParameters.Add(new SqlParameter("@SalesPerson", aMaster.SalesPerson.Split(':')[0].Trim()));
                }
                else
                {
                    aParameters.Add(new SqlParameter("@SalesPerson", aMaster.SalesPerson));
                }
                aParameters.Add(new SqlParameter("@SrId", aMaster.SrId));
                aParameters.Add(new SqlParameter("@OrderDate", aMaster.OrderDate));
                aParameters.Add(new SqlParameter("@DeliveryDestination", aMaster.DeliveryDestination));
                aParameters.Add(new SqlParameter("@CreateBy", user));
                aParameters.Add(new SqlParameter("@OrderBy", user));
                aParameters.Add(new SqlParameter("@CreateDate", System.DateTime.Now));


                //aParameters.Add(new SqlParameter("@GroupId", aMaster.GroupId));
                //aParameters.Add(new SqlParameter("@PaymentType", aMaster.PaymentType));

                // aParameters.Add(new SqlParameter("@BankId", aMaster.BankId));
                // aParameters.Add(new SqlParameter("@BranchId", aMaster.BranchId));
                // aParameters.Add(new SqlParameter("@ChequeNo", aMaster.ChequeNo));
                //  aParameters.Add(new SqlParameter("@ChequeDate", aMaster.ChequeDate));



                aParameters.Add(new SqlParameter("@CompanyId", aMaster.CompanyId));
                aParameters.Add(new SqlParameter("@SalesPersonContact", aMaster.SalesPersonContact));
                aParameters.Add(new SqlParameter("@DeliveryDate", aMaster.DeliveryDate));
                aParameters.Add(new SqlParameter("@TotatalOrderAmtKg", aMaster.TotatalOrderAmtKg));
                aParameters.Add(new SqlParameter("@TotalIncTaxVat", aMaster.TotalIncTaxVat));

                aParameters.Add(new SqlParameter("@TotalExTaxVat", aMaster.TotalExTaxVat));
                aParameters.Add(new SqlParameter("@TaxVat", aMaster.TaxVat));

                aParameters.Add(new SqlParameter("@collectiontypeid", aMaster.CollectionType));
                aParameters.Add(new SqlParameter("@collectionamount", aMaster.CollectionAmount));
                aParameters.Add(new SqlParameter("@IsEmployee", aMaster.IsEmployee));
                aParameters.Add(new SqlParameter("@CustomerTypeID", aMaster.CustomerTypeId));

                aParameters.Add(new SqlParameter("@ColBankId", aMaster.ColBankId));
                aParameters.Add(new SqlParameter("@ColBranchId", aMaster.ColBranchId));

                aParameters.Add(new SqlParameter("@RefNo", aMaster.RefNo));
                aParameters.Add(new SqlParameter("@RefDate", aMaster.RefDate));



                aParameters.Add(new SqlParameter("@Remarks", aMaster.Remarks));
                if(aMaster.eflag=="ap")
                {
                    aParameters.Add(new SqlParameter("@IsApprove", 1));
                }
                else
                {
                    aParameters.Add(new SqlParameter("@IsApprove", aMaster.IsApprove));
                }
                
                aParameters.Add(new SqlParameter("@eflag", aMaster.eflag));
                aParameters.Add(new SqlParameter("@DeliveryWarehouseId", aMaster.DeliveryWarehouseId));
                aParameters.Add(new SqlParameter("@CreditAmount", aMaster.CreditAmount));
                aParameters.Add(new SqlParameter("@LedgerBalance", aMaster.LedgerBalance));



                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_Save_SalesOrderProcessed", aParameters);
                if (aResponse.pk > 0)
                {
                    if (aMaster.IsApprove == 1 || aMaster.IsApprove == 0) // if Approve or new Entry
                    {
                        aResponse.isSuccess = SaveSalesOrderProcessedDetails(aMaster.SalesDetails, aResponse.pk, user, aMaster.IsApprove);
                    }
                    if(aMaster.IsApprove==1)
                    {
                        //aResponse.isSuccess = SaveSOCreditVoucher(aResponse.pk);
                        aResponse.isSuccess = ManageCreditTransactionStock(aMaster.CustomerId, aMaster.OrderDate, aMaster.CreditAmount);
                    }
                    if (aMaster.IsApprove == 2) // if Reject
                    {
                        aResponse.isSuccess = true;
                    }

                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                }
                else
                {
                    _accessManager.SqlConnectionClose(true);
                }
                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public bool ManageCreditTransactionStock(int? CustomerId , DateTime? OrderDate, Decimal CreditAmount)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CustomerID", CustomerId));
                aParameters.Add(new SqlParameter("@orderDate", OrderDate));
                aParameters.Add(new SqlParameter("@CreditAmount", CreditAmount));


                result = _accessManager.SaveData("sp_CreditTransactionStock", aParameters); 

                return result;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool SaveSOCreditVoucher(int SalesOrderProcessedMasterId)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@SalesOrderProcessedMasterId", SalesOrderProcessedMasterId));
                result = _accessManager.SaveData("sp_SaveSOCreditVoucher", aParameters);

                return result;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public bool SaveSalesOrderProcessedDetails(List<SalesOrderProcessedDetails> aList, int pk, string user, int isApprove)
        {
            try
            {
                bool result = false;
                foreach (var item in aList)
                {
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@SalesOrderProcessedMasterId", pk));
                    aParameters.Add(new SqlParameter("@SalesOrderProcessDetailsId", item.SalesOrderProcessDetailsId));
                    aParameters.Add(new SqlParameter("@ProductId", item.ProductId));
                    aParameters.Add(new SqlParameter("@ProductQty", item.ProductQty));
                    aParameters.Add(new SqlParameter("@ProductKg", item.ProductKg));
                    aParameters.Add(new SqlParameter("@StockRate", item.StockRate));
                    aParameters.Add(new SqlParameter("@SaleRate", item.SaleRate));
                    aParameters.Add(new SqlParameter("@BaseRate", item.BaseRate));
                    aParameters.Add(new SqlParameter("@DiscountAmount", item.DiscountAmount));
                    aParameters.Add(new SqlParameter("@DiscountPercent", item.DiscountPercent));
                    aParameters.Add(new SqlParameter("@TotalPrice", item.TotalPrice));
                    aParameters.Add(new SqlParameter("@BTotalPrice", item.BTotalPrice));
                    aParameters.Add(new SqlParameter("@ProductTax", item.ProductTax));
                    aParameters.Add(new SqlParameter("@CreateBy", user));
                    aParameters.Add(new SqlParameter("@IsDelete", item.IsDelete));
                    aParameters.Add(new SqlParameter("@isApprove", isApprove));
                    aParameters.Add(new SqlParameter("@OperationIdAdd", item.OperationIdAdd));
                    aParameters.Add(new SqlParameter("@OperationIdDelete", item.OperationIdDelete));
                    aParameters.Add(new SqlParameter("@CreateDate", DateTime.Now));
                    aParameters.Add(new SqlParameter("@groupid", item.GroupId));
                    aParameters.Add(new SqlParameter("@haschallan", item.haschallan));
                    aParameters.Add(new SqlParameter("@eflag", item.eflag));
                    aParameters.Add(new SqlParameter("@alreadypicked", item.alreadypicked));
                    aParameters.Add(new SqlParameter("@isbulk", item.isbulk));
                    aParameters.Add(new SqlParameter("@MTRetentionFront", item.MTRetention));
                    aParameters.Add(new SqlParameter("@Incentive", item.Incentive));
                    result = _accessManager.SaveData("sp_Save_SavelOrderProcessedDetails", aParameters);
                    if (result == false)
                    {
                        break;
                    }

                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public List<ViewSalesOrderProcessedMaster> GetSalesOrderProcessedMaster(DateTime? fromdate, DateTime? todate, string salesperson, int customerid = 0)
        {
            try
            {

                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-1);

                }
                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }
                if (salesperson != null)
                {
                    salesperson = salesperson.Split(':')[0].Trim();
                }
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<ViewSalesOrderProcessedMaster> aList = new List<ViewSalesOrderProcessedMaster>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@customerid", customerid));
                aParameters.Add(new SqlParameter("@SalesPerson", salesperson));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_SalesOrderProcessedList", aParameters);
                while (dr.Read())
                {
                    ViewSalesOrderProcessedMaster aMaster = new ViewSalesOrderProcessedMaster();

                    aMaster.SalesOrderProcessedMasterId = Convert.ToInt32(dr["SalesOrderProcessedMasterId"]);
                    aMaster.TotatalOrderAmtKg = Convert.ToDecimal(dr["TotatalOrderAmtKg"]);
                    aMaster.TotalIncTaxVat = Convert.ToDecimal(dr["TotalIncTaxVat"]);
                    aMaster.TotalExTaxVat = Convert.ToDecimal(dr["TotalExTaxVat"]);
                    aMaster.TotalDue = Convert.ToDecimal(dr["TotalDue"]);
                    aMaster.TotalPaid = Convert.ToDecimal(dr["TotalPaid"]);

                    aMaster.SalesPerson = dr["SalesPerson"].ToString();
                    aMaster.SalesOrderNo = dr["SalesOrderNo"].ToString();
                    aMaster.CustomerName = dr["CustomerName"].ToString();
                    aMaster.DeliveryDestination = dr["DeliveryDestination"].ToString();
                    aMaster.SalesPersonContact = dr["SalesPersonContact"].ToString();

                    aMaster.CreateBy = dr["CreateBy"].ToString();
                    aMaster.ApproveBy = dr["ApproveBy"].ToString();
                    aMaster.ApproveDate = dr["ApproveDate"] as DateTime?;
                    aMaster.CreateDate = dr["CreateDate"] as DateTime?;
                    aMaster.OrderDate = dr["OrderDate"] as DateTime?;
                    aMaster.DeliveryDate = dr["DeliveryDate"] as DateTime?;
                    if (DBNull.Value == dr["IsApprove"])
                    {
                        aMaster.IsApprove = 0;
                    }
                    else
                    {
                        aMaster.IsApprove = Convert.ToInt32(dr["IsApprove"]);
                    }
                    if (DBNull.Value == dr["IsEmployee"])
                    {
                        aMaster.IsEmployee = null;
                    }
                    else
                    {
                        aMaster.IsEmployee = (bool)dr["IsEmployee"];
                    }
                    aMaster.CustomerType = dr["CoustomerTypeName"].ToString();
                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<ViewSalesOrderProcessedMaster> GetSalesOrderProcessedMasterSearch(DateTime? fromdate, DateTime? todate, string salesperson, int customerid = 0,int deliveryWarehouseId = 0)
        {
            try
            {

                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-1);

                }
                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }
                if (salesperson != null)
                {
                    salesperson = salesperson.Split(':')[0].Trim();
                }
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<ViewSalesOrderProcessedMaster> aList = new List<ViewSalesOrderProcessedMaster>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@customerid", customerid));
                aParameters.Add(new SqlParameter("@SalesPerson", salesperson));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                aParameters.Add(new SqlParameter("@deliveryWarehouseId", deliveryWarehouseId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_SalesOrderProcessedList", aParameters);
                while (dr.Read())
                {
                    ViewSalesOrderProcessedMaster aMaster = new ViewSalesOrderProcessedMaster();

                    aMaster.SalesOrderProcessedMasterId = Convert.ToInt32(dr["SalesOrderProcessedMasterId"]);
                    aMaster.TotatalOrderAmtKg = Convert.ToDecimal(dr["TotatalOrderAmtKg"]);
                    aMaster.TotalIncTaxVat = Convert.ToDecimal(dr["TotalIncTaxVat"]);
                    aMaster.TotalExTaxVat = Convert.ToDecimal(dr["TotalExTaxVat"]);
                    aMaster.TotalDue = Convert.ToDecimal(dr["TotalDue"]);
                    aMaster.TotalPaid = Convert.ToDecimal(dr["TotalPaid"]);

                    aMaster.SalesPerson = dr["SalesPerson"].ToString();
                    aMaster.SalesOrderNo = dr["SalesOrderNo"].ToString();
                    aMaster.CustomerName = dr["CustomerName"].ToString();
                    aMaster.DeliveryDestination = dr["DeliveryDestination"].ToString();
                    aMaster.SalesPersonContact = dr["SalesPersonContact"].ToString();

                    aMaster.CreateBy = dr["CreateBy"].ToString();
                    aMaster.ApproveBy = dr["ApproveBy"].ToString();
                    aMaster.ApproveDate = dr["ApproveDate"] as DateTime?;
                    aMaster.CreateDate = dr["CreateDate"] as DateTime?;
                    aMaster.OrderDate = dr["OrderDate"] as DateTime?;
                    aMaster.DeliveryDate = dr["DeliveryDate"] as DateTime?;
                    if (DBNull.Value == dr["IsApprove"])
                    {
                        aMaster.IsApprove = 0;
                    }
                    else
                    {
                        aMaster.IsApprove = Convert.ToInt32(dr["IsApprove"]);
                    }
                    if (DBNull.Value == dr["IsEmployee"])
                    {
                        aMaster.IsEmployee = null;
                    }
                    else
                    {
                        aMaster.IsEmployee = (bool)dr["IsEmployee"];
                    }
                    aMaster.CustomerType = dr["CoustomerTypeName"].ToString();
                    aMaster.DeliveryWarehouse= dr["WareHouseName"].ToString();
                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public SalesOrderProcessedMaster GetSalesOrderProcessedMaster(int id)
        {
            try
            {
                List<SqlParameter> aParameters = new List<SqlParameter>();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                aParameters.Add(new SqlParameter("@id", id));
                DataTable dt = _accessManager.GetDataTable("sp_Get_SalesOrderProcessedMasrerById", aParameters);
                List<SalesOrderProcessedMaster> aList = Helper.ToListof<SalesOrderProcessedMaster>(dt);
                return aList[0];
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public List<SalesOrderProcessedDetails> GetSalesOderProcessedDetailsByMaster(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                DataTable dt = _accessManager.GetDataTable("sp_Get_SalesOrderPrecessedDetailsByMaster", aParameters);
                List<SalesOrderProcessedDetails> aList = Helper.ToListof<SalesOrderProcessedDetails>(dt);
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<SalesOrderProcessedDetails> GetSalesOderProcessedDetailsByMaster_challan(int id, int picId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                aParameters.Add(new SqlParameter("@picId", picId));
                DataTable dt = _accessManager.GetDataTable("sp_Get_SalesOrderPrecessedDetailsByMaster_challan", aParameters);
                List<SalesOrderProcessedDetails> aList = Helper.ToListof<SalesOrderProcessedDetails>(dt);
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<SalesOrderProcessedDetails> GetSalesOderProcessedDetailsApproveByMaster(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                DataTable dt = _accessManager.GetDataTable("sp_Get_SalesOrderPrecessedDetailsByMaster", aParameters);
                List<SalesOrderProcessedDetails> aList = Helper.ToListof<SalesOrderProcessedDetails>(dt);
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public int GetLocationIdByWareHouse(int wareHouse)
        {
            try
            {
                int locationId = 0;
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@warehouseId", wareHouse));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_LocationIdByWarehouse", aParameters);
                while (dr.Read())
                {
                    locationId = Convert.ToInt32(dr["LocationId"]);
                }
                return locationId;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse ApproveSalesOrder(int id, bool status, string user)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                aParameters.Add(new SqlParameter("@status", status));
                aParameters.Add(new SqlParameter("@user", user));

                aResponse.isSuccess = _accessManager.SaveData("sp_Approve_SalesOrderProcessed", aParameters);
                return aResponse;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<ViewSalesOrderProcessedMaster> GetSalesOrderProcessedMasterForInvoice(DateTime fromdate, DateTime todate, string salesperson, int customerid)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<ViewSalesOrderProcessedMaster> aList = new List<ViewSalesOrderProcessedMaster>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@customerid", customerid));
                aParameters.Add(new SqlParameter("@SalesPerson", salesperson.Split(':')[0].Trim()));
                aParameters.Add(new SqlParameter("@FromDate", fromdate));
                aParameters.Add(new SqlParameter("@ToDate", todate));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_SalesOrderProcessedListForInvoice", aParameters);
                while (dr.Read())
                {
                    ViewSalesOrderProcessedMaster aMaster = new ViewSalesOrderProcessedMaster();

                    aMaster.SalesOrderProcessedMasterId = Convert.ToInt32(dr["SalesOrderProcessedMasterId"]);
                    aMaster.TotatalOrderAmtKg = Convert.ToDecimal(dr["TotatalOrderAmtKg"]);
                    aMaster.TotalIncTaxVat = Convert.ToDecimal(dr["TotalIncTaxVat"]);
                    aMaster.TotalExTaxVat = Convert.ToDecimal(dr["TotalExTaxVat"]);
                    aMaster.TotalDue = Convert.ToDecimal(dr["TotalDue"]);
                    aMaster.TotalPaid = Convert.ToDecimal(dr["TotalPaid"]);

                    aMaster.SalesPerson = dr["SalesPerson"].ToString();
                    aMaster.SalesOrderNo = dr["SalesOrderNo"].ToString();
                    aMaster.CustomerName = dr["CustomerName"].ToString();
                    aMaster.DeliveryDestination = dr["DeliveryDestination"].ToString();
                    aMaster.SalesPersonContact = dr["SalesPersonContact"].ToString();

                    aMaster.CreateBy = dr["CreateBy"].ToString();
                    aMaster.ApproveBy = dr["ApproveBy"].ToString();
                    aMaster.ApproveDate = dr["ApproveDate"] as DateTime?;
                    aMaster.CreateDate = dr["CreateDate"] as DateTime?;
                    aMaster.OrderDate = dr["OrderDate"] as DateTime?;
                    aMaster.DeliveryDate = dr["DeliveryDate"] as DateTime?;
                    if (DBNull.Value == dr["IsApprove"])
                    {
                        aMaster.IsApprove = 0;
                    }
                    else
                    {
                        aMaster.IsApprove = Convert.ToInt32(dr["IsApprove"]);
                    }
                    if (DBNull.Value == dr["IsEmployee"])
                    {
                        aMaster.IsEmployee = null;
                    }
                    else
                    {
                        aMaster.IsEmployee = (bool)dr["IsEmployee"];
                    }
                    aMaster.CustomerType = dr["CoustomerTypeName"].ToString();
                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public ResultResponse ApproveSalesOrder_Cancel(int id, string user)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));

                aParameters.Add(new SqlParameter("@user", user));
                aParameters.Add(new SqlParameter("@Canceldate", System.DateTime.Now));

                aResponse.isSuccess = _accessManager.SaveData("sp_Approve_SalesOrderProcessed_Cancel", aParameters);
                return aResponse;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public ResultResponse SODelete(int id)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                aResponse.isSuccess = _accessManager.SaveData("sp_SODelete", aParameters);
                return aResponse;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }



        public List<ViewSalesOrderProcessedMaster> GetSalesOrderProcessedMasterForCancel(DateTime? fromdate, DateTime? todate, string salesperson, int customerid = 0)
        {
            try
            {
                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-1);

                }
                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }
                if (salesperson != null)
                {
                    salesperson = salesperson.Split(':')[0].Trim();
                }
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<ViewSalesOrderProcessedMaster> aList = new List<ViewSalesOrderProcessedMaster>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@customerid", customerid));
                aParameters.Add(new SqlParameter("@SalesPerson", salesperson));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_SalesOrderProcessedApprovedListForCancel", aParameters);
                while (dr.Read())
                {
                    ViewSalesOrderProcessedMaster aMaster = new ViewSalesOrderProcessedMaster();

                    aMaster.SalesOrderProcessedMasterId = Convert.ToInt32(dr["SalesOrderProcessedMasterId"]);
                    aMaster.TotatalOrderAmtKg = Convert.ToDecimal(dr["TotatalOrderAmtKg"]);
                    aMaster.TotalIncTaxVat = Convert.ToDecimal(dr["TotalIncTaxVat"]);
                    aMaster.TotalExTaxVat = Convert.ToDecimal(dr["TotalExTaxVat"]);
                    aMaster.TotalDue = Convert.ToDecimal(dr["TotalDue"]);
                    aMaster.TotalPaid = Convert.ToDecimal(dr["TotalPaid"]);

                    aMaster.SalesPerson = dr["SalesPerson"].ToString();
                    aMaster.SalesOrderNo = dr["SalesOrderNo"].ToString();
                    aMaster.CustomerName = dr["CustomerName"].ToString();
                    aMaster.DeliveryDestination = dr["DeliveryDestination"].ToString();
                    aMaster.SalesPersonContact = dr["SalesPersonContact"].ToString();

                    aMaster.CreateBy = dr["CreateBy"].ToString();
                    aMaster.ApproveBy = dr["ApproveBy"].ToString();
                    aMaster.ApproveDate = dr["ApproveDate"] as DateTime?;
                    aMaster.CreateDate = dr["CreateDate"] as DateTime?;
                    aMaster.OrderDate = dr["OrderDate"] as DateTime?;
                    aMaster.DeliveryDate = dr["DeliveryDate"] as DateTime?;
                    if (DBNull.Value == dr["IsApprove"])
                    {
                        aMaster.IsApprove = 0;
                    }
                    else
                    {
                        aMaster.IsApprove = Convert.ToInt32(dr["IsApprove"]);
                    }
                    if (DBNull.Value == dr["IsEmployee"])
                    {
                        aMaster.IsEmployee = null;
                    }
                    else
                    {
                        aMaster.IsEmployee = (bool)dr["IsEmployee"];
                    }
                    aMaster.CustomerType = dr["CoustomerTypeName"].ToString();
                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        /// <summary>
        /// SalesOrderWiseSalesPersonSalesReport For Total Date To Date Sales Person Sales Order Report If Future Needed Plese Use This Method
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="salesPerson"></param>
        /// <param name="customerTypeId"></param>
        /// <returns></returns>


        //public DataTable SalesOrderWiseSalesPersonSalesReport(DateTime fromDate, DateTime toDate, string salesPerson = "", string customerTypeId = "")
        //{

        //    _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
        //    List<SqlParameter> aParameters = new List<SqlParameter>();
        //    aParameters.Add(new SqlParameter("@FormDate", fromDate));
        //    aParameters.Add(new SqlParameter("@ToDate", toDate));
        //    aParameters.Add(new SqlParameter("@SalesPerson", salesPerson));
        //    aParameters.Add(new SqlParameter("@CustomerTypeId", customerTypeId));

        //    DataTable dt = _accessManager.GetDataTable("sp_GetSalesReportBySalesPersonOrderWise", aParameters);

        //    return dt;
        //}

        public DataTable SalesOrderWiseSalesPersonSalesReport(DateTime fromDate, DateTime toDate, int reportType, int summary, string salesPerson = "", string customerTypeId = "", int divisionId = 0, int areaId = 0, int territoryId = 0, int marketId = 0)
        {
            DataTable dt = new DataTable();
            _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
            List<SqlParameter> aParameters = new List<SqlParameter>();
            aParameters.Add(new SqlParameter("@FormDate", fromDate));
            aParameters.Add(new SqlParameter("@ToDate", toDate));
            aParameters.Add(new SqlParameter("@SalesPerson", salesPerson));
            aParameters.Add(new SqlParameter("@CustomerTypeId", customerTypeId));
            aParameters.Add(new SqlParameter("@DivisionId", divisionId));
            aParameters.Add(new SqlParameter("@AreaId", areaId));
            aParameters.Add(new SqlParameter("@TerritoryId", territoryId));
            aParameters.Add(new SqlParameter("@MarketId", marketId));
            aParameters.Add(new SqlParameter("@reportType", reportType));
            if (summary == 2)
            {
                dt = _accessManager.GetDataTable("sp_GetSalesOrderSummerySalesPersonSalesAmount", aParameters,true);
            }
            if(summary == 1)
            {
                dt = _accessManager.GetDataTable("_spGetSalesOrderReportSalesPersonSalesAmount", aParameters,true);
            }
            if (summary == 3)
            {
                dt = _accessManager.GetDataTable("spGetSalesOrderReportSalesPersonSalesMonthly", aParameters,true);
            }


            return dt;
        }

        /// <summary>
        ///  ChallanWiseSalesPersonSalesReport For Total Date To Date Sales Person Challan Report If Future Needed Plese Use This Method
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="salesPerson"></param>
        /// <param name="customerTypeId"></param>
        /// <returns></returns>

        //public DataTable ChallanWiseSalesPersonSalesReport(DateTime fromDate, DateTime toDate, string salesPerson = "", string customerTypeId = "")
        //{

        //    _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
        //    List<SqlParameter> aParameters = new List<SqlParameter>();
        //    aParameters.Add(new SqlParameter("@FormDate", fromDate));
        //    aParameters.Add(new SqlParameter("@ToDate", toDate));
        //    aParameters.Add(new SqlParameter("@SalesPerson", salesPerson));
        //    aParameters.Add(new SqlParameter("@CustomerTypeId", customerTypeId));

        //    DataTable dt = _accessManager.GetDataTable("sp_GetSalesReportBySalesPersonChallanWise", aParameters);

        //    return dt;
        //}

        public DataTable ChallanWiseSalesPersonSalesReport(DateTime fromDate, DateTime toDate,int reportType, int summary, string salesPerson = "", string customerTypeId = "", int divisionId = 0, int areaId = 0, int territoryId = 0, int marketId = 0)
        {
            DataTable dt = new DataTable();
            _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
            List<SqlParameter> aParameters = new List<SqlParameter>();
            aParameters.Add(new SqlParameter("@FormDate", fromDate));
            aParameters.Add(new SqlParameter("@ToDate", toDate));
            aParameters.Add(new SqlParameter("@SalesPerson", salesPerson));
            aParameters.Add(new SqlParameter("@CustomerTypeId", customerTypeId));
            aParameters.Add(new SqlParameter("@DivisionId", divisionId));
            aParameters.Add(new SqlParameter("@AreaId", areaId));
            aParameters.Add(new SqlParameter("@TerritoryId", territoryId));
            aParameters.Add(new SqlParameter("@MarketId", marketId));
            aParameters.Add(new SqlParameter("@reportType", reportType));
            if (summary == 2)
            {
                dt = _accessManager.GetDataTable("sp_GetSalesChallanSummerySalesPersonSalesAmount", aParameters,true);
            }
            if (summary == 1)
            {
                dt = _accessManager.GetDataTable("_spGetSalesChallanReportSalesPersonSalesAmount", aParameters, true);
            }
            if (summary == 3)
            {
                dt = _accessManager.GetDataTable("spGetSalesChallanReportSalesPersonSalesMonthly", aParameters,true);
            }

            return dt;
        }

        /// <summary>
        /// SalesOrderWiseCustomerSalesReport For Total Date To Date All Customer Sales Order Report If Future Needed Plese Use This Method Date Wise Qt|Kg
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="customerTypeId"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>



        //public DataTable SalesOrderWiseCustomerSalesReport(DateTime fromDate, DateTime toDate, string customerTypeId = "", int customerId = 0)
        //{

        //    _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
        //    List<SqlParameter> aParameters = new List<SqlParameter>();
        //    aParameters.Add(new SqlParameter("@FormDate", fromDate));
        //    aParameters.Add(new SqlParameter("@ToDate", toDate));
        //    aParameters.Add(new SqlParameter("@CustomerId", customerId));
        //    aParameters.Add(new SqlParameter("@CustomerTypeId", customerTypeId));

        //    DataTable dt = _accessManager.GetDataTable("sp_GetSalesReportByCustomerOrderWise", aParameters);

        //    return dt;
        //}


        public DataTable SalesOrderWiseCustomerSalesReport(DateTime fromDate, DateTime toDate, int reportType, int summary, string customerTypeId = "", int customerId = 0)
        {

            _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
            List<SqlParameter> aParameters = new List<SqlParameter>();
            aParameters.Add(new SqlParameter("@FormDate", fromDate));
            aParameters.Add(new SqlParameter("@ToDate", toDate));
            aParameters.Add(new SqlParameter("@CustomerId", customerId));
            aParameters.Add(new SqlParameter("@CustomerTypeId", customerTypeId));
            aParameters.Add(new SqlParameter("@reportType", reportType));

            DataTable dt = new DataTable();
            //if (summary == 1)
            //{
            //    dt = _accessManager.GetDataTable("sp_GetSalesReportByCustomerOrderAmountCustomerWise", aParameters);
            //}
            //else
            //{
            //    dt = _accessManager.GetDataTable("sp_GetSalesReportByCustomerOrderAmount", aParameters);
            //}

            if (summary == 2)
            {
                dt = _accessManager.GetDataTable("sp_GetSalesOrderSummeryCustomerSalesAmount", aParameters, true);
            }
            if (summary == 1)
            {
                dt = _accessManager.GetDataTable("_spGetSalesOrderReportCustomerSalesAmount", aParameters, true);
            }
            if (summary == 3)
            {
                dt = _accessManager.GetDataTable("spGetSalesOrderReportCustomerSalesMonthly", aParameters, true);
            }


            return dt;
        }
        /// <summary>
        /// ChallanWiseCustomerSalesReport For Total Date To Date All Customer Challan Order Report If Future Needed Plese Use This Method Date Wise Qt|Kg
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="customerTypeId"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        //public DataTable ChallanWiseCustomerSalesReport(DateTime fromDate, DateTime toDate, string customerTypeId = "", int customerId = 0)
        //{

        //    _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
        //    List<SqlParameter> aParameters = new List<SqlParameter>();
        //    aParameters.Add(new SqlParameter("@FormDate", fromDate));
        //    aParameters.Add(new SqlParameter("@ToDate", toDate));
        //    aParameters.Add(new SqlParameter("@CustomerId", customerId));
        //    aParameters.Add(new SqlParameter("@CustomerTypeId", customerTypeId));

        //    DataTable dt = _accessManager.GetDataTable("sp_GetSalesReportByCustomerChallanWise", aParameters);

        //    return dt;
        //}

        public DataTable ChallanWiseCustomerSalesReport(DateTime fromDate, DateTime toDate, int reportType, int summary, string customerTypeId = "", int customerId = 0)
        {

            _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
            List<SqlParameter> aParameters = new List<SqlParameter>();
            aParameters.Add(new SqlParameter("@FormDate", fromDate));
            aParameters.Add(new SqlParameter("@ToDate", toDate));
            aParameters.Add(new SqlParameter("@CustomerId", customerId));
            aParameters.Add(new SqlParameter("@CustomerTypeId", customerTypeId));
            aParameters.Add(new SqlParameter("@reportType", reportType));

            DataTable dt = new DataTable();
            //if (summary == 1)
            //{
            //    dt = _accessManager.GetDataTable("sp_GetSalesReportByCustomerChallanAmountCustomerWise", aParameters);
            //}
            //else
            //{
            //    dt = _accessManager.GetDataTable("sp_GetSalesReportByCustomerChallanAmount", aParameters);
            //}

            if (summary == 2)
            {
                dt = _accessManager.GetDataTable("sp_GetSalesChallanSummeryCustomerSalesAmount", aParameters, true);
                //dt = _accessManager.GetDataTable("sp_GetSalesReportByCustomerChallanAmount", aParameters, true);
            }
            if (summary == 1)
               
            {
                dt = _accessManager.GetDataTable("_spGetSalesChallanReportCustomerSalesAmount", aParameters, true);
            }
            if (summary == 3)
            {
                dt = _accessManager.GetDataTable("spGetSalesChallanReportCustomerSalesMonthly", aParameters, true);
            }


            return dt;
        }

        public decimal PackWeight(int id)
        {
            decimal weight = 0;
            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@Productid", id));
                DataTable dt = _accessManager.GetDataTable("sp_GetPackWeight", aParameters);
                if (dt.Rows.Count > 0)
                {
                    weight = Convert.ToDecimal(dt.Rows[0]["PackWeight"]);
                }

                return weight;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public decimal QtyForKG(int id)
        {
            decimal weight = 0;
            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@Productid", id));
                DataTable dt = _accessManager.GetDataTable("sp_GetQtyForKG", aParameters);
                if (dt.Rows.Count > 0)
                {
                    weight = Convert.ToDecimal(dt.Rows[0]["PerKgStdQty"]);
                }

                return weight;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable ProductWiseSalesSummeryReport(DateTime fromDate, DateTime toDate, string customerTypeId = "", int groupId = 0, int divisionId = 0, int areaId = 0, int territoryId = 0, int marketId = 0, int customerId = 0, int productId = 0, int warehouseId = 0)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@FormDate", fromDate));
                aParameters.Add(new SqlParameter("@ToDate", toDate));
                aParameters.Add(new SqlParameter("@CustomerTypeId", customerTypeId));
                aParameters.Add(new SqlParameter("@GroupId", groupId));
                aParameters.Add(new SqlParameter("@DivisionId", divisionId));
                aParameters.Add(new SqlParameter("@AreaId", areaId));
                aParameters.Add(new SqlParameter("@TerritoryId", territoryId));
                aParameters.Add(new SqlParameter("@MarketId", marketId));
                aParameters.Add(new SqlParameter("@CustomerId", customerId));
                aParameters.Add(new SqlParameter("@WarehouseId", warehouseId));
                aParameters.Add(new SqlParameter("@ProductId", productId));
                DataTable dt = _accessManager.GetDataTable("sp_GetProductWiseSalesSummery", aParameters,true);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
                _accessManager.SqlConnectionClose();
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
            
        }
        #region new code for approved sales order edit

        public List<ViewSalesOrderProcessedMaster> GetSalesOrderProcessedMaster_Approved(DateTime? fromdate, DateTime? todate, string salesperson, int customerid = 0)
        {
            try
            {

                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-1);

                }
                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }
                if (salesperson != null)
                {
                    salesperson = salesperson.Split(':')[0].Trim();
                }
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<ViewSalesOrderProcessedMaster> aList = new List<ViewSalesOrderProcessedMaster>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@customerid", customerid));
                aParameters.Add(new SqlParameter("@SalesOrderNo", salesperson));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_SalesOrderProcessedApprovalList_Edit", aParameters);
                while (dr.Read())
                {
                    ViewSalesOrderProcessedMaster aMaster = new ViewSalesOrderProcessedMaster();

                    aMaster.SalesOrderProcessedMasterId = Convert.ToInt32(dr["SalesOrderProcessedMasterId"]);
                    aMaster.TotatalOrderAmtKg = Convert.ToDecimal(dr["TotatalOrderAmtKg"]);
                    aMaster.TotalIncTaxVat = Convert.ToDecimal(dr["TotalIncTaxVat"]);
                    aMaster.TotalExTaxVat = Convert.ToDecimal(dr["TotalExTaxVat"]);
                    aMaster.TotalDue = Convert.ToDecimal(dr["TotalDue"]);
                    aMaster.TotalPaid = Convert.ToDecimal(dr["TotalPaid"]);

                    aMaster.SalesPerson = dr["SalesPerson"].ToString();
                    aMaster.SalesOrderNo = dr["SalesOrderNo"].ToString();
                    aMaster.CustomerName = dr["CustomerName"].ToString();
                    aMaster.DeliveryDestination = dr["DeliveryDestination"].ToString();
                    aMaster.SalesPersonContact = dr["SalesPersonContact"].ToString();

                    aMaster.CreateBy = dr["CreateBy"].ToString();
                    aMaster.ApproveBy = dr["ApproveBy"].ToString();
                    aMaster.ApproveDate = dr["ApproveDate"] as DateTime?;
                    aMaster.CreateDate = dr["CreateDate"] as DateTime?;
                    aMaster.OrderDate = dr["OrderDate"] as DateTime?;
                    aMaster.DeliveryDate = dr["DeliveryDate"] as DateTime?;
                    if (DBNull.Value == dr["IsApprove"])
                    {
                        aMaster.IsApprove = 0;
                    }
                    else
                    {
                        aMaster.IsApprove = Convert.ToInt32(dr["IsApprove"]);
                    }
                    if (DBNull.Value == dr["IsEmployee"])
                    {
                        aMaster.IsEmployee = null;
                    }
                    else
                    {
                        aMaster.IsEmployee = (bool)dr["IsEmployee"];
                    }
                    aMaster.CustomerType = dr["CoustomerTypeName"].ToString();
                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        #endregion

        public DataTable SalsePersonWiseCustomer(string salesperson)
        {
            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@salesPerson", salesperson));
                DataTable dt = _accessManager.GetDataTable("sp_CustomerListSalesPersonWise", aParameters);
                
                return dt;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public DataTable SalsePersonWiseCustomerSales(string salesperson,int customerId, DateTime fromDate,DateTime toDate)
        {
            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@salesPerson", salesperson));
                aParameters.Add(new SqlParameter("@salesPerson", customerId));
                aParameters.Add(new SqlParameter("@salesPerson", fromDate));
                aParameters.Add(new SqlParameter("@salesPerson", toDate));
                DataTable dt = _accessManager.GetDataTable("sp_SalesPersonWiseSalesChallan", aParameters);
                
                return dt;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public DataTable SalsePersonWiseCustomerSalesDetails(string salesperson,int customerId, DateTime fromDate,DateTime toDate)
        {
            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@salesPerson", salesperson));
                aParameters.Add(new SqlParameter("@salesPerson", customerId));
                aParameters.Add(new SqlParameter("@salesPerson", fromDate));
                aParameters.Add(new SqlParameter("@salesPerson", toDate));
                DataTable dt = _accessManager.GetDataTable("sp_SalesPersonWiseSalesChallanDetalis", aParameters);
                
                return dt;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public DataTable SalsePersonWiseSalesCollection(string salesperson,int customerId, DateTime fromDate,DateTime toDate)
        {
            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@salesPerson", salesperson));
                aParameters.Add(new SqlParameter("@salesPerson", customerId));
                aParameters.Add(new SqlParameter("@salesPerson", fromDate));
                aParameters.Add(new SqlParameter("@salesPerson", toDate));
                DataTable dt = _accessManager.GetDataTable("sp_SalesPersonWiseCollection", aParameters);
                
                return dt;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable SalesOrderWiseSalesPersonSalesDetailsReport(DateTime fromDate, DateTime toDate, int summary, string salesPerson = "", string customerTypeId = "", int divisionId = 0, int areaId = 0, int territoryId = 0, int marketId = 0)
        {
            DataTable dt = new DataTable();
            _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
            List<SqlParameter> aParameters = new List<SqlParameter>();
            aParameters.Add(new SqlParameter("@FormDate", fromDate));
            aParameters.Add(new SqlParameter("@ToDate", toDate));
            aParameters.Add(new SqlParameter("@SalesPerson", salesPerson));
            aParameters.Add(new SqlParameter("@CustomerTypeId", customerTypeId));
            aParameters.Add(new SqlParameter("@DivisionId", divisionId));
            aParameters.Add(new SqlParameter("@AreaId", areaId));
            aParameters.Add(new SqlParameter("@TerritoryId", territoryId));
            aParameters.Add(new SqlParameter("@MarketId", marketId));
            if (summary == 1)
            {
               // dt = _accessManager.GetDataTable("sp_GetSalesOrderSummerySalesPersonSalesAmount", aParameters);
            }
            else
            {
                dt = _accessManager.GetDataTable("spGetSalesOrderDetailsReportSalesPersonSales", aParameters);
            }


            return dt;
        }

        public DataTable ChallanWiseSalesPersonSalesDetailsReport(DateTime fromDate, DateTime toDate, int summary, string salesPerson = "", string customerTypeId = "", int divisionId = 0, int areaId = 0, int territoryId = 0, int marketId = 0)
        {
            DataTable dt = new DataTable();
            _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
            List<SqlParameter> aParameters = new List<SqlParameter>();
            aParameters.Add(new SqlParameter("@FormDate", fromDate));
            aParameters.Add(new SqlParameter("@ToDate", toDate));
            aParameters.Add(new SqlParameter("@SalesPerson", salesPerson));
            aParameters.Add(new SqlParameter("@CustomerTypeId", customerTypeId));
            aParameters.Add(new SqlParameter("@DivisionId", divisionId));
            aParameters.Add(new SqlParameter("@AreaId", areaId));
            aParameters.Add(new SqlParameter("@TerritoryId", territoryId));
            aParameters.Add(new SqlParameter("@MarketId", marketId));
            if (summary == 1)
            {
               // dt = _accessManager.GetDataTable("sp_GetSalesChallanSummerySalesPersonSalesAmount", aParameters);
            }
            else
            {
                dt = _accessManager.GetDataTable("spGetSalesChallanDetailsReportSalesPersonSales", aParameters);
            }


            return dt;
        }

        public DataTable SalesPersonSalesCollectionDue(DateTime fromDate, DateTime toDate, string salesPerson = "", string customerTypeId = "", int divisionId = 0, int areaId = 0, int territoryId = 0, int marketId = 0)
        {
            DataTable dt = new DataTable();
            _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
            List<SqlParameter> aParameters = new List<SqlParameter>();
            aParameters.Add(new SqlParameter("@FormDate", fromDate));
            aParameters.Add(new SqlParameter("@ToDate", toDate));
            aParameters.Add(new SqlParameter("@SalesPerson", salesPerson));
            aParameters.Add(new SqlParameter("@CustomerTypeId", customerTypeId));
            aParameters.Add(new SqlParameter("@DivisionId", divisionId));
            aParameters.Add(new SqlParameter("@AreaId", areaId));
            aParameters.Add(new SqlParameter("@TerritoryId", territoryId));
            aParameters.Add(new SqlParameter("@MarketId", marketId));

            dt = _accessManager.GetDataTable("sp_GetSalesCollectionSalesPersonSales", aParameters);
            return dt;
        }
        public DataTable CustomerTypeSalesCollectionDue(DateTime fromDate, DateTime toDate, string customerTypeId = "", int divisionId = 0, int areaId = 0, int territoryId = 0, int marketId = 0,int customerId=0,int summary=0)
        {
            DataTable dt = new DataTable();
            _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
            List<SqlParameter> aParameters = new List<SqlParameter>();
            aParameters.Add(new SqlParameter("@FormDate", fromDate));
            aParameters.Add(new SqlParameter("@ToDate", toDate));
            aParameters.Add(new SqlParameter("@CustomerTypeId", customerTypeId));
            aParameters.Add(new SqlParameter("@DivisionId", divisionId));
            aParameters.Add(new SqlParameter("@AreaId", areaId));
            aParameters.Add(new SqlParameter("@TerritoryId", territoryId));
            aParameters.Add(new SqlParameter("@MarketId", marketId));
            aParameters.Add(new SqlParameter("@customerId", customerId));
            if (summary == 1)
            {
                dt = _accessManager.GetDataTable("sp_GetSalesCollectionCustomerTypeSales", aParameters,true);
            }
            if (summary == 2)
            {
                dt = _accessManager.GetDataTable("sp_GetSalesCollectionCustomerTypeDetailsSales", aParameters,true);
            }

            return dt;
        }

        public DataTable ProductWiseSalesChallanDeliveryReport(DateTime fromDate, DateTime toDate, string customerTypeId = "", int groupId = 0, int divisionId = 0, int areaId = 0, int territoryId = 0, int marketId = 0, int customerId = 0, int productId = 0, int warehouseId = 0)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@FormDate", fromDate));
                aParameters.Add(new SqlParameter("@ToDate", toDate));
                aParameters.Add(new SqlParameter("@CustomerTypeId", customerTypeId));
                aParameters.Add(new SqlParameter("@GroupId", groupId));
                aParameters.Add(new SqlParameter("@DivisionId", divisionId));
                aParameters.Add(new SqlParameter("@AreaId", areaId));
                aParameters.Add(new SqlParameter("@TerritoryId", territoryId));
                aParameters.Add(new SqlParameter("@MarketId", marketId));
                aParameters.Add(new SqlParameter("@CustomerId", customerId));
                aParameters.Add(new SqlParameter("@WarehouseId", warehouseId));
                aParameters.Add(new SqlParameter("@ProductId", productId));
                DataTable dt = _accessManager.GetDataTable("sp_Getproductwise_so_vs_challan_vs_delivery", aParameters, true);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
                _accessManager.SqlConnectionClose();
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public List<PolicyDetails> GetPolicyDetails(int groupId, int customertype, int customer, int ProductId)
        //public List<PolicyDetails> GetPolicyDetails(int groupId, int customertype, int customer)
        {
            try
            {


                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<PolicyDetails> aList = new List<PolicyDetails>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@groupId", groupId));
                aParameters.Add(new SqlParameter("@customertype", customertype));
                aParameters.Add(new SqlParameter("@customer", customer));
                aParameters.Add(new SqlParameter("@ProductId", ProductId));

                SqlDataReader dr = _accessManager.GetSqlDataReader("GetSalesPolicyDetails", aParameters);
                //SqlDataReader dr = _accessManager.GetSqlDataReader("GetSalesPolicyDetails_New", aParameters);
                while (dr.Read())
                {
                    PolicyDetails aMaster = new PolicyDetails();

                    aMaster.PolicyDetailId = Convert.ToInt32(dr["PolicyDetailId"]);
                    aMaster.PolicyMasterId = Convert.ToInt32(dr["PolicyMasterId"]);
                    aMaster.GroupId = DBNull.Value.Equals(dr["GroupId"]) ? 0 : Convert.ToInt32(dr["GroupId"]); 
                    aMaster.CustomerTypeId = Convert.ToInt32(dr["CustomerTypeId"]);
                    aMaster.Percentage = Convert.ToDecimal(dr["Percentage"]);


                    aMaster.PolicyTitle = dr["PolicyTitle"].ToString();
                    aMaster.Remarks = dr["Remarks"].ToString();

                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public SpecialOffers GetSpecialOffers(int customertype, DateTime orderDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SpecialOffers aMaster = new SpecialOffers();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@customertype", customertype));
                aParameters.Add(new SqlParameter("@orderDate", orderDate.Date));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetSpecialOffer", aParameters);


                while (dr.Read())
                {
                    aMaster.OfferId = Convert.ToInt32(dr["OfferId"]);

                    aMaster.Incentive = Convert.ToDecimal(dr["Incentive"]);

                    aMaster.OfferName = dr["OfferName"].ToString();


                }
                return aMaster;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<CustomerType> GetCustomerTypeList()
        {

            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<CustomerType> _List = new List<CustomerType>();

                //List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetCustomerTypeWithoutMTO");
                while (dr.Read())
                {
                    CustomerType aInfo = new CustomerType();
                    aInfo.CustomerTypeID = Convert.ToInt32(dr["CustomerTypeID"]);
                    aInfo.CoustomerTypeName = dr["CoustomerTypeName"].ToString();
                    aInfo.IsActive = Convert.ToBoolean(dr["IsActive"]);


                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable CustomerDayToDaySalesCollectionDue(DateTime fromDate, DateTime toDate, int customerId = 0, int customerTypeId=0)
        {
            DataTable dt = new DataTable();
            _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
            List<SqlParameter> aParameters = new List<SqlParameter>();
            aParameters.Add(new SqlParameter("@FormDate", fromDate));
            aParameters.Add(new SqlParameter("@ToDate", toDate));
            aParameters.Add(new SqlParameter("@CustomerId", customerId));
            aParameters.Add(new SqlParameter("@customerTypeId", customerTypeId));

            dt = _accessManager.GetDataTable("sp_GetCustomerDayToDaySalesCollection", aParameters);
            return dt;
        }

        public DataTable GetBreakdownofRevenue(int yearId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@Year", yearId));
                DataTable dt = _accessManager.GetDataTable("sp_GetYearlyBreakdownRevenue", aParameters,true);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
    }
}