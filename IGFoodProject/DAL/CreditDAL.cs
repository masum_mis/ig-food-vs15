﻿using IGFoodProject.DataManager;
using IGFoodProject.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using IGFoodProject.ViewModel;

namespace IGFoodProject.DAL
{
    public class CreditDAL
    {
        DataAccessManager accessManager = new DataAccessManager();

        public List<CustomerCredit> LoadCustomerType()
        {
            accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
            SqlDataReader dr = accessManager.GetSqlDataReader("sp_LoadCustomerType");
            List<CustomerCredit> creditList = new List<CustomerCredit>();
            while (dr.Read())
            {
                CustomerCredit credit = new CustomerCredit();
                credit.CustomerTypeID = Convert.ToInt32(dr["CustomerTypeID"]);
                credit.CustomerTypeName = dr["CoustomerTypeName"].ToString();
                creditList.Add(credit);
            }
            return creditList;
        }

        public List<CustomerCredit> LoadCustomerByType(int CustomerTypeID, int CustomerNameId)
        {
            accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
            List<SqlParameter> aParameters = new List<SqlParameter>();
            aParameters.Add(new SqlParameter("@customerTypeId", CustomerTypeID));
            aParameters.Add(new SqlParameter("@customerId", CustomerNameId));
            SqlDataReader dr = accessManager.GetSqlDataReader("sp_LoadCustomerByType", aParameters);
            List<CustomerCredit> creditList = new List<CustomerCredit>();
            while (dr.Read())
            {
                CustomerCredit credit = new CustomerCredit();
                credit.CustomerID = Convert.ToInt32(dr["CustomerID"]);
                credit.CustomerName = dr["CustomerName"].ToString();
                credit.Jan = Convert.ToDecimal(dr["Jan"]);
                credit.Feb = Convert.ToDecimal(dr["Feb"]);
                credit.Mar = Convert.ToDecimal(dr["Mar"]);
                credit.Apr = Convert.ToDecimal(dr["Apr"]);
                credit.May = Convert.ToDecimal(dr["May"]);
                credit.Jun = Convert.ToDecimal(dr["Jun"]);
                credit.Jul = Convert.ToDecimal(dr["Jul"]);
                credit.Aug = Convert.ToDecimal(dr["Aug"]);
                credit.Sep = Convert.ToDecimal(dr["Sep"]);
                credit.Oct = Convert.ToDecimal(dr["Oct"]);
                credit.Nov = Convert.ToDecimal(dr["Nov"]);
                credit.Dec = Convert.ToDecimal(dr["Dec"]);

                credit.JanUnUse = Convert.ToDecimal(dr["JanUnUse"]);
                credit.FebUnUse = Convert.ToDecimal(dr["FebUnUse"]);
                credit.MarUnUse = Convert.ToDecimal(dr["MarUnUse"]);
                credit.AprUnUse = Convert.ToDecimal(dr["AprUnUse"]);
                credit.MayUnUse = Convert.ToDecimal(dr["MayUnUse"]);
                credit.JunUnUse = Convert.ToDecimal(dr["JunUnUse"]);
                credit.JulUnUse = Convert.ToDecimal(dr["JulUnUse"]);
                credit.AugUnUse = Convert.ToDecimal(dr["AugUnUse"]);
                credit.SepUnUse = Convert.ToDecimal(dr["SepUnUse"]);
                credit.OctUnUse = Convert.ToDecimal(dr["OctUnUse"]);
                credit.NovUnUse = Convert.ToDecimal(dr["NovUnUse"]);
                credit.DecUnUse = Convert.ToDecimal(dr["DecUnUse"]);

                creditList.Add(credit);
            }
            return creditList;
        }

        public bool SaveMonthlyCredit(List<MonthlyCreditM> _monthlyCredit, string UserName)
        {
            bool result = false;
            try

            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                foreach (MonthlyCreditM item in _monthlyCredit)
                {
                    List<SqlParameter> gSqlParameterList = new List<SqlParameter>();

                    gSqlParameterList.Add(new SqlParameter("@CustomerId", item.CustomerId));
                    gSqlParameterList.Add(new SqlParameter("@Year", item.Year));
                    gSqlParameterList.Add(new SqlParameter("@TotalCredit", item.TotalCredit));
                    gSqlParameterList.Add(new SqlParameter("@UserId", UserName));
                    gSqlParameterList.Add(new SqlParameter("@MonthlyEntry", item.MonthlyEntry));

                    result = accessManager.SaveData("SP_SaveMonthlyCredit", gSqlParameterList);
                }

            }
            catch (Exception exception)
            {
                accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
            return result;
        }


    }
}
