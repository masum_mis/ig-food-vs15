﻿using IGFoodProject.DataManager;
using IGFoodProject.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IGFoodProject.DAL
{
    public class ItemStockDAL
    {
        DataAccessManager accessManager = new DataAccessManager();
        public List<tbl_ProductGroup> GetGroupList()
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<tbl_ProductGroup> _List = new List<tbl_ProductGroup>();

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetItemGroupForStock");
                while (dr.Read())
                {
                    tbl_ProductGroup aInfo = new tbl_ProductGroup();
                    aInfo.GroupId = (int)dr["GroupId"];
                    aInfo.GroupName = dr["GroupName"].ToString();                  


                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<Product> GetProductByGroupId2(int groupId = 0)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@groupId", groupId));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_ItemByGroupId", aParameters);

                List<Product> aList = new List<Product>();
                while (dr.Read())
                {
                    Product aProduct = new Product();
                    aProduct.ProductId = Convert.ToInt32(dr["ProductId"]);
                    aProduct.ProductName = dr["ProductName"].ToString();
                    aList.Add(aProduct);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        //public DataTable GetItemStock(int wareHouseId, DateTime stockDate, int groupId, int ProductId) 
        //{
        //    try
        //    {
        //        accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
        //        List<SqlParameter> parameters = new List<SqlParameter>();
        //        parameters.Add(new SqlParameter("@WareHouseId", wareHouseId));
        //        parameters.Add(new SqlParameter("@fromDate", stockDate));
        //        parameters.Add(new SqlParameter("@GroupId", groupId));
        //        parameters.Add(new SqlParameter("@ProductId", ProductId));
        //        DataTable dr = accessManager.GetDataTable("sp_ItemStock", parameters, true);
        //        return dr;
        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }
        //    finally
        //    {
        //        accessManager.SqlConnectionClose();
        //    }
        //}


        public DataTable GetItemStock(int wareHouseId, DateTime stockDate, DateTime todate, int groupId, int ProductId)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@WareHouseId", wareHouseId));
                parameters.Add(new SqlParameter("@fromDate", stockDate));
                parameters.Add(new SqlParameter("@toDate", todate));
                parameters.Add(new SqlParameter("@GroupId", groupId));
                parameters.Add(new SqlParameter("@ProductId", ProductId));
                DataTable dr = accessManager.GetDataTable("sp_ItemStock", parameters, true);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }


    }
}