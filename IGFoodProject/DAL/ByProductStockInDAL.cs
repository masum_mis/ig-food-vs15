﻿using IGFoodProject.DataManager;
using IGFoodProject.Models;
using IGFoodProject.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IGFoodProject.DAL
{
    public class ByProductStockInDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();
        public List<DBIssuePortionMaster> IssueInformation(int warehouseid)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@WarehouseId", warehouseid));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetDBIssueNoForByProduct", parameters);
                List<DBIssuePortionMaster> wareHouseList = new List<DBIssuePortionMaster>();
                while (dr.Read())
                {


                    DBIssuePortionMaster wareHouse = new DBIssuePortionMaster();
                    wareHouse.DBIssueMasterID = Convert.ToInt32(dr["DBIssueMasterID"]);
                    wareHouse.IssueNo = dr["IssueNo"].ToString();
                    wareHouseList.Add(wareHouse);
                }
                return wareHouseList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

        public List<DressedBirdMaster> DressbirdInformation(int warehouseid)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@WarehouseId", warehouseid));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetDBForByProduct", parameters);
                List<DressedBirdMaster> wareHouseList = new List<DressedBirdMaster>();
                while (dr.Read())
                {


                    DressedBirdMaster wareHouse = new DressedBirdMaster();
                    wareHouse.DressedBirdMasterID = Convert.ToInt32(dr["DressedBirdMasterID"]);
                    wareHouse.DressedBirdNo = dr["DressedBirdNo"].ToString();
                    wareHouseList.Add(wareHouse);
                }
                return wareHouseList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

        public List<ViewTargetPortionInfo> IssueDetail(int issueid)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@DBIssueMasterID", issueid));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetIssueMasterDetailForByProduct", parameters);
                List<ViewTargetPortionInfo> wareHouseList = new List<ViewTargetPortionInfo>();
                while (dr.Read())
                {


                    ViewTargetPortionInfo wareHouse = new ViewTargetPortionInfo();
                    
                    wareHouse.CompanyShortName = dr["CompanyShortName"].ToString();
                    wareHouse.LocationName = dr["LocationName"].ToString();
                    wareHouse.WareHouseID = Convert.ToInt32(dr["WareHouseID"]);
                    wareHouse.WareHouseName = dr["WareHouseName"].ToString();
                    
                    wareHouse.CompanyId = Convert.ToInt32(dr["CompanyId"]);
                    wareHouse.LocationId = Convert.ToInt32(dr["LocationId"]);

                    wareHouseList.Add(wareHouse);
                }
                return wareHouseList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public List<ViewProductDescription> LoadProduct()
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                //List<SqlParameter> parameters = new List<SqlParameter>();
                //parameters.Add(new SqlParameter("@locationId", locationId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetProductName_ByProduct");
                List<ViewProductDescription> wareHouseList = new List<ViewProductDescription>();
                while (dr.Read())
                {
                    ViewProductDescription wareHouse = new ViewProductDescription();
                    wareHouse.ProductId = Convert.ToInt32(dr["ProductId"]);
                    wareHouse.ProductName = dr["ProductName"].ToString();
                    wareHouseList.Add(wareHouse);
                }
                return wareHouseList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

        public bool SaveByProductStock(tbl_StockInMaster aMaster)
        {
            bool result = false;
            try

            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                int id = SaveByProductStockMaster(aMaster);


                foreach (tbl_StockInDetail grade in aMaster.aDetail)
                {
                    List<SqlParameter> gSqlParameterList = new List<SqlParameter>();

                    gSqlParameterList.Add(new SqlParameter("@StockInDetailID", grade.StockInDetailID));
                    gSqlParameterList.Add(new SqlParameter("@StockInMasterID", id));
                    gSqlParameterList.Add(new SqlParameter("@ProductID", grade.ProductID));
                    gSqlParameterList.Add(new SqlParameter("@StockInDate", aMaster.StockInDate));                   
                    gSqlParameterList.Add(new SqlParameter("@StockInKG", grade.StockInKG));
                    gSqlParameterList.Add(new SqlParameter("@ExpireDate", grade.ExpireDate));
                    gSqlParameterList.Add(new SqlParameter("@Remark", grade.Remark));
                    gSqlParameterList.Add(new SqlParameter("@UnitPrice", grade.UnitPrice));                    
                    gSqlParameterList.Add(new SqlParameter("@IsOpening", grade.IsOpening));


                    result = _accessManager.SaveData("sp_SaveStockInDetail_ByProduct", gSqlParameterList);
                }




            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
            return result;
        }
        public int SaveByProductStockMaster(tbl_StockInMaster aMaster)
        {
            try
            {
                string empReceived = string.Empty;
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@StockInMasterID", aMaster.StockInMasterID));
                aSqlParameterList.Add(new SqlParameter("@StockInDate", aMaster.StockInDate));
                aSqlParameterList.Add(new SqlParameter("@StockInBy", aMaster.StockInBy.Split(':')[0].Trim()));
                aSqlParameterList.Add(new SqlParameter("@WareHouseId", aMaster.WareHouseId));
                aSqlParameterList.Add(new SqlParameter("@IsOpening", aMaster.IsOpening));
                aSqlParameterList.Add(new SqlParameter("@Comments", aMaster.Comments));

                aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@CreateDate", System.DateTime.Now));

                aSqlParameterList.Add(new SqlParameter("@DBIssueMasterID", aMaster.DBIssueMasterID));

                aSqlParameterList.Add(new SqlParameter("@CompanyID", aMaster.CompanyId));
                aSqlParameterList.Add(new SqlParameter("@LocationId", aMaster.LocationId));
                aSqlParameterList.Add(new SqlParameter("@GroupId", aMaster.GroupId));
                aSqlParameterList.Add(new SqlParameter("@DressdMasterID", aMaster.DressdMasterID));


                return _accessManager.SaveDataReturnPrimaryKey("sp_SaveStickInMaster_ByProduct", aSqlParameterList);
            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }
        }
    }
}