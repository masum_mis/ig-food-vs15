﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using IGFoodProject.DataManager;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;

namespace IGFoodProject.DAL
{
    public class PickingProcessDAL
    {
        private DataAccessManager _accessManager = new DataAccessManager();
        public DataTable GetAllPickingDetailsList(DateTime? deliveryDate = null, int companyId = 0, int locationId = 0, int warehouseId = 0, int vehicleId = 0, int reqType = 1)
        {

            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                //parameters.Add(new SqlParameter("@locationId", locationId));
                parameters.Add(new SqlParameter("@CompanyId", companyId));
                parameters.Add(new SqlParameter("@LocationId", locationId));
                parameters.Add(new SqlParameter("@WarehouseId", warehouseId));
                parameters.Add(new SqlParameter("@VehicleId", vehicleId));
                parameters.Add(new SqlParameter("@DeliveryDate", deliveryDate));
                parameters.Add(new SqlParameter("@ReqType", reqType));
                DataTable dr = _accessManager.GetDataTable("sp_GetAllPickingDetailsList", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetPickingDetailsById(int pickingMasterId, int salesOrderId = 0)
        {

            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@PickingMasterId", pickingMasterId));
                parameters.Add(new SqlParameter("@salesOrderId", salesOrderId));
                DataTable dr = _accessManager.GetDataTable("sp_GetPickingDetailsByPickMasterId", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public dynamic GetSalesOrdersForPicking(PickingSearchParam pickingSearchParam)
        {
            try
            {


                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameters = new List<SqlParameter>();
                aSqlParameters.Add(new SqlParameter("@OrderFromDate", pickingSearchParam.OrderFromDate));
                aSqlParameters.Add(new SqlParameter("@OrderToDate", pickingSearchParam.OrderToDate));
                aSqlParameters.Add(new SqlParameter("@CustomerId", pickingSearchParam.CustomerId));
                aSqlParameters.Add(new SqlParameter("@CustomerTypeId", pickingSearchParam.CustomerTypeId));
                aSqlParameters.Add(new SqlParameter("@AreaId", pickingSearchParam.AreaId));
                aSqlParameters.Add(new SqlParameter("@TerritoryId", pickingSearchParam.TerritoryId));
                aSqlParameters.Add(new SqlParameter("@MarketId", pickingSearchParam.MarketId));
                aSqlParameters.Add(new SqlParameter("@DistrictId", pickingSearchParam.DistrictId));
                aSqlParameters.Add(new SqlParameter("@ThanaId", pickingSearchParam.ThanaId));
                aSqlParameters.Add(new SqlParameter("@PickingmasterId", pickingSearchParam.PickingMasterId));
                aSqlParameters.Add(new SqlParameter("@DeliveryWarehouseId", pickingSearchParam.DeliveryWarehouseId));
                SqlDataReader dr;
                //if (pickingSearchParam.PickingMasterId > 0)
                //{
                //    dr = _accessManager.GetSqlDataReader("sp_GetSalesOrderForPickingEdit", aSqlParameters);
                //}
                //else
                //{
                    //DataTable dt= _accessManager.GetDataTable("sp_GetSalesOrderForPicking", aSqlParameters);
                    dr = _accessManager.GetSqlDataReader("sp_GetSalesOrderForPicking", aSqlParameters);
                //}


                List<ViewSalesOrderDetailsForPicking> salesOrderDetailsForPickingList = new List<ViewSalesOrderDetailsForPicking>();
                while (dr.Read())
                {
                    ViewSalesOrderDetailsForPicking salesOrderDetailsForPicking = new ViewSalesOrderDetailsForPicking();
                    ViewSalesOrderMasterForPicking salesOrderMasterForPicking = new ViewSalesOrderMasterForPicking();
                    decimal orderedApproved = DBNull.Value == dr["OrderedApproved"] ? 0 : Convert.ToDecimal(dr["OrderedApproved"]);
                    decimal prevPicked = DBNull.Value == dr["AllreadyPicked"] ? 0 : Convert.ToDecimal(dr["AllreadyPicked"]);
                    if (orderedApproved != prevPicked)
                    {

                        salesOrderDetailsForPicking.PickingDetailsId = DBNull.Value == dr["PickingDetailsId"] ? 0 : Convert.ToInt32(dr["PickingDetailsId"]);
                        salesOrderDetailsForPicking.SalesOrderMasterId = DBNull.Value == dr["SalesOrderProcessedMasterId"] ? 0 : Convert.ToInt32(dr["SalesOrderProcessedMasterId"]);
                        salesOrderDetailsForPicking.SalesOrderDetailsId = DBNull.Value == dr["SalesOrderProcessDetailsId"] ? 0 : Convert.ToInt32(dr["SalesOrderProcessDetailsId"]);
                        salesOrderDetailsForPicking.OrderQty = DBNull.Value == dr["ApprovedQty"] ? 0 : Convert.ToDecimal(dr["ApprovedQty"]);
                        salesOrderDetailsForPicking.OrderKg = DBNull.Value == dr["ApprovedKg"] ? 0 : Convert.ToDecimal(dr["ApprovedKg"]);

                        salesOrderDetailsForPicking.Picked = DBNull.Value == dr["PickedQuantity"] ? 0 : Convert.ToDecimal(dr["PickedQuantity"]);
                        salesOrderDetailsForPicking.PrevPickedQty = DBNull.Value == dr["AllreadyPicked"] ? 0 : Convert.ToDecimal(dr["AllreadyPicked"]);
                        salesOrderDetailsForPicking.PrevInputedQty = DBNull.Value == dr["AllreadyPickedQty"] ? 0 : Convert.ToDecimal(dr["AllreadyPickedQty"]);
                        salesOrderDetailsForPicking.PrevInputedKg = DBNull.Value == dr["AllreadyPickedKg"] ? 0 : Convert.ToDecimal(dr["AllreadyPickedKg"]);

                        salesOrderDetailsForPicking.ProductId = DBNull.Value == dr["ProductId"] ? 0 : Convert.ToInt32(dr["ProductId"]);
                        salesOrderDetailsForPicking.ProductName = DBNull.Value == dr["ProductName"] ? null : Convert.ToString(dr["ProductName"]);
                        salesOrderDetailsForPicking.ProductGroupId = DBNull.Value == dr["GroupId"] ? 0 : Convert.ToInt32(dr["GroupId"]);

                        salesOrderMasterForPicking.PickingMasterId = DBNull.Value.Equals(dr["PickingMasterId"]) ? 0 : Convert.ToInt32(dr["PickingMasterId"]);
                        salesOrderMasterForPicking.SalesOrderMasterId = DBNull.Value == dr["SalesOrderProcessedMasterId"] ? 0 : Convert.ToInt32(dr["SalesOrderProcessedMasterId"]);
                        salesOrderMasterForPicking.SalesOrderNo = DBNull.Value == dr["SalesOrderNo"] ? null : Convert.ToString(dr["SalesOrderNo"]);
                        salesOrderMasterForPicking.Orderdate = Convert.ToDateTime(dr["Orderdate"]);
                        salesOrderMasterForPicking.DeliveryDate = Convert.ToDateTime(dr["DeliveryDate"]);
                        salesOrderMasterForPicking.CustomerName = DBNull.Value == dr["CustomerName"] ? null : Convert.ToString(dr["CustomerName"]);
                        salesOrderMasterForPicking.CustomerCode = DBNull.Value == dr["CustomerCode"] ? null : Convert.ToString(dr["CustomerCode"]);
                        salesOrderMasterForPicking.CustomerType = DBNull.Value == dr["CoustomerTypeName"] ? null : Convert.ToString(dr["CoustomerTypeName"]);
                        salesOrderMasterForPicking.Market = DBNull.Value == dr["MarketName"] ? null : Convert.ToString(dr["MarketName"]);
                        salesOrderMasterForPicking.Territory = DBNull.Value == dr["TerritoryName"] ? null : Convert.ToString(dr["TerritoryName"]);
                        salesOrderMasterForPicking.Area = DBNull.Value == dr["AreaName"] ? null : Convert.ToString(dr["AreaName"]);
                        salesOrderMasterForPicking.Division = DBNull.Value == dr["DivisionName"] ? null : Convert.ToString(dr["DivisionName"]);
                        salesOrderMasterForPicking.FullPicked = DBNull.Value == dr["FullPicked"] ? 0 : Convert.ToInt32(dr["FullPicked"]);

                        salesOrderDetailsForPicking.SalesOrderMasterForPicking = salesOrderMasterForPicking;
                        salesOrderDetailsForPickingList.Add(salesOrderDetailsForPicking);
                    }
                   


                }

                _accessManager.SqlConnectionClose();
                var a = salesOrderDetailsForPickingList.Select(i => new { i.SalesOrderMasterForPicking.SalesOrderNo }).Distinct().ToList();

                List<ViewSalesOrderMasterForPicking> salesOrderMasterForPicking2 = new List<ViewSalesOrderMasterForPicking>();

                foreach (var item in a)
                {
                    List<ViewSalesOrderDetailsForPicking> salesOrderDetailsForPickingList1 = new List<ViewSalesOrderDetailsForPicking>();
                    ViewSalesOrderMasterForPicking salesOrderMasterForPicking1 = new ViewSalesOrderMasterForPicking();
                    ViewSalesOrderMasterForPicking salesOrderMasterForPicking3 = new ViewSalesOrderMasterForPicking();
                    salesOrderDetailsForPickingList1 = salesOrderDetailsForPickingList.Where(i => i.SalesOrderMasterForPicking.SalesOrderNo == item.SalesOrderNo).ToList();


                    if (salesOrderDetailsForPickingList1.Any(i => i.SalesOrderMasterForPicking.PickingMasterId != 0))
                    {
                        salesOrderMasterForPicking1 = salesOrderDetailsForPickingList1.Where(i => i.SalesOrderMasterForPicking.PickingMasterId != 0).Select(u => u.SalesOrderMasterForPicking).FirstOrDefault();

                    }
                    else
                    {
                        salesOrderMasterForPicking1 = salesOrderDetailsForPickingList1.Where(i => i.SalesOrderMasterForPicking.SalesOrderNo == item.SalesOrderNo).Select(u => u.SalesOrderMasterForPicking).FirstOrDefault();

                    }


                    salesOrderMasterForPicking1.SalesOrderDetailsForPicking = salesOrderDetailsForPickingList1;
                    salesOrderMasterForPicking2.Add(salesOrderMasterForPicking1);
                }
                var qw = salesOrderMasterForPicking2.Where(r => r.FullPicked != 1).ToList();
                var df = salesOrderMasterForPicking2.Where(r => r.PickingMasterId == pickingSearchParam.PickingMasterId);
                var result = qw.Union(df).ToList();
                // return salesOrderMasterForPicking2;
                return result;

            }
            catch (Exception exception)
            {


                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public dynamic GetPickingInformation(int pickingMasterId)
        {
            try
            {


                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameters = new List<SqlParameter>();
                aSqlParameters.Add(new SqlParameter("@pickMasterId", pickingMasterId));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetAllPickingInfo", aSqlParameters);

                //  List<ViewSalesOrderMasterForPicking> salesOrderMasterForPickingList = new List<ViewSalesOrderMasterForPicking>();
                PickingRequestMaster pickingRequestMaster = new PickingRequestMaster();
                while (dr.Read())
                {
                    //  ViewSalesOrderDetailsForPicking salesOrderDetailsForPicking = new ViewSalesOrderDetailsForPicking();


                    //salesOrderDetailsForPicking.SalesOrderMasterId = DBNull.Value == dr["SalesOrderProcessedMasterId"] ? 0 : Convert.ToInt32(dr["SalesOrderProcessedMasterId"]);
                    //salesOrderDetailsForPicking.SalesOrderDetailsId = DBNull.Value == dr["SalesOrderProcessDetailsId"] ? 0 : Convert.ToInt32(dr["SalesOrderProcessDetailsId"]);
                    //salesOrderDetailsForPicking.OrderQty = DBNull.Value == dr["ApprovedQty"] ? 0 : Convert.ToDecimal(dr["ApprovedQty"]);
                    //salesOrderDetailsForPicking.PrevPickedQty = DBNull.Value == dr["AllreadyPicked"] ? 0 : Convert.ToDecimal(dr["AllreadyPicked"]);
                    //salesOrderDetailsForPicking.ProductId = DBNull.Value == dr["ProductId"] ? 0 : Convert.ToInt32(dr["ProductId"]);
                    //salesOrderDetailsForPicking.ProductName = DBNull.Value == dr["ProductName"] ? null : Convert.ToString(dr["ProductName"]);

                    pickingRequestMaster.PickingMasterId = DBNull.Value == dr["PickingMasterId"] ? 0 : Convert.ToInt32(dr["PickingMasterId"]);
                    pickingRequestMaster.PickingNo = DBNull.Value == dr["PickingNo"] ? null : Convert.ToString(dr["PickingNo"]);
                    pickingRequestMaster.DeliveryDate = Convert.ToDateTime(dr["DeliveryDate"]);
                    pickingRequestMaster.OrderdateFrom = dr["OrderdateFrom"] as DateTime?;
                    pickingRequestMaster.OrderDateTo = dr["OrderDateTo"] as DateTime?;
                    pickingRequestMaster.PickingTime = DBNull.Value == dr["PickingTime"] ? null : Convert.ToString(dr["PickingTime"]);
                    pickingRequestMaster.CustomerId = DBNull.Value == dr["CustomerId"] ? 0 : Convert.ToInt32(dr["CustomerId"]);
                    pickingRequestMaster.ConpamyId = DBNull.Value == dr["ConpamyId"] ? 0 : Convert.ToInt32(dr["ConpamyId"]);
                    pickingRequestMaster.CustomerTypeId = DBNull.Value == dr["CustomerTypeId"] ? 0 : Convert.ToInt32(dr["CustomerTypeId"]);
                    pickingRequestMaster.MarketId = DBNull.Value == dr["MarketId"] ? 0 : Convert.ToInt32(dr["MarketId"]);
                    pickingRequestMaster.TerritoryId = DBNull.Value == dr["TerritoryId"] ? 0 : Convert.ToInt32(dr["TerritoryId"]);
                    pickingRequestMaster.AreaId = DBNull.Value == dr["AreaId"] ? 0 : Convert.ToInt32(dr["AreaId"]);
                    pickingRequestMaster.ThanaId = DBNull.Value == dr["ThanaId"] ? 0 : Convert.ToInt32(dr["ThanaId"]);
                    pickingRequestMaster.DistrictId = DBNull.Value == dr["DistrictId"] ? 0 : Convert.ToInt32(dr["DistrictId"]);
                    pickingRequestMaster.LocationId = DBNull.Value == dr["LocationId"] ? 0 : Convert.ToInt32(dr["LocationId"]);
                    pickingRequestMaster.WareHouseId = DBNull.Value == dr["WareHouseId"] ? 0 : Convert.ToInt32(dr["WareHouseId"]);
                    pickingRequestMaster.VehicleId = DBNull.Value == dr["VehicleId"] ? 0 : Convert.ToInt32(dr["VehicleId"]);
                    //  salesOrderDetailsForPicking.SalesOrderMasterForPicking = salesOrderMasterForPicking;

                    // salesOrderMasterForPickingList.Add(salesOrderMasterForPicking);


                }

                _accessManager.SqlConnectionClose();
                //var a = salesOrderDetailsForPickingList.Select(i => new { i.SalesOrderMasterForPicking.SalesOrderNo }).Distinct().ToList();

                //List<ViewSalesOrderMasterForPicking> salesOrderMasterForPicking2 = new List<ViewSalesOrderMasterForPicking>();

                //foreach (var item in a)
                //{
                //    List<ViewSalesOrderDetailsForPicking> salesOrderDetailsForPickingList1 = new List<ViewSalesOrderDetailsForPicking>();
                //    ViewSalesOrderMasterForPicking salesOrderMasterForPicking1 = new ViewSalesOrderMasterForPicking();
                //    salesOrderDetailsForPickingList1 = salesOrderDetailsForPickingList.Where(i => i.SalesOrderMasterForPicking.SalesOrderNo == item.SalesOrderNo).ToList();
                //    salesOrderMasterForPicking1 = salesOrderDetailsForPickingList.Where(i => i.SalesOrderMasterForPicking.SalesOrderNo == item.SalesOrderNo).Select(u => u.SalesOrderMasterForPicking).FirstOrDefault();
                //    salesOrderMasterForPicking1.SalesOrderDetailsForPicking = salesOrderDetailsForPickingList1;
                //    salesOrderMasterForPicking2.Add(salesOrderMasterForPicking1);
                //}

                return pickingRequestMaster;

            }
            catch (Exception exception)
            {


                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public bool SavePickingRequest(ViewSalesOrderMasterForPicking salesOrderMasterForPicking, string userId)
        {
            try
            {



                int masterId = 0;
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CompanyId", salesOrderMasterForPicking.CompanyId));
                aParameters.Add(new SqlParameter("@LocationId", salesOrderMasterForPicking.LocationId));
                aParameters.Add(new SqlParameter("@WarehouseId", salesOrderMasterForPicking.WarehouseId));
                aParameters.Add(new SqlParameter("@TerritoryId", salesOrderMasterForPicking.TerritoryId));
                aParameters.Add(new SqlParameter("@MarketId", salesOrderMasterForPicking.MarketId));
                aParameters.Add(new SqlParameter("@DivisionId", salesOrderMasterForPicking.DivisionId));
                aParameters.Add(new SqlParameter("@ThanaId", salesOrderMasterForPicking.ThanaId));
                aParameters.Add(new SqlParameter("@AreaId", salesOrderMasterForPicking.AreaId));
                aParameters.Add(new SqlParameter("@CustomerId", salesOrderMasterForPicking.CustomerId));
                aParameters.Add(new SqlParameter("@CustomerTypeId", salesOrderMasterForPicking.CustomerTypeId));
                aParameters.Add(new SqlParameter("@VehicleId", salesOrderMasterForPicking.VehicleId));
                aParameters.Add(new SqlParameter("@DeliveryDate", salesOrderMasterForPicking.DeliveryDate));
                aParameters.Add(new SqlParameter("@UserId", userId));
                aParameters.Add(new SqlParameter("@PickingMasterId", salesOrderMasterForPicking.PickingMasterId));
                aParameters.Add(new SqlParameter("@OrderDateFrom", salesOrderMasterForPicking.OrderDateFrom));
                aParameters.Add(new SqlParameter("@OrderDateTo", salesOrderMasterForPicking.OrderDateTo));

                masterId = _accessManager.SaveDataReturnPrimaryKey("sp_SavePickingRequest_Master", aParameters);
                bool result = true;
                foreach (ViewSalesOrderDetailsForPicking item in salesOrderMasterForPicking.SalesOrderDetailsForPicking)
                {
                    List<SqlParameter> aList = new List<SqlParameter>();

                    var totalPicked = item.PrevPicked + item.Picked;

                    if (item.OrderedApproved == totalPicked)
                    {
                        aList.Add(new SqlParameter("@FullPicked", 1));
                    }
                    else
                    {
                        aList.Add(new SqlParameter("@FullPicked", 2));
                    }
                    aList.Add(new SqlParameter("@ProductId", item.ProductId));
                    aList.Add(new SqlParameter("@OrderedApproved", item.OrderedApproved));
                    aList.Add(new SqlParameter("@Picked", item.Picked));
                    aList.Add(new SqlParameter("@InputedPickQty", item.InputedPickQty));
                    aList.Add(new SqlParameter("@InputedPickKg", item.InputedPickKg));
                    aList.Add(new SqlParameter("@SalesOrderMasterId", item.SalesOrderMasterId));
                    aList.Add(new SqlParameter("@SalesOrderDetailsId", item.SalesOrderDetailsId));
                    aList.Add(new SqlParameter("@PickingMasterId", masterId));
                    aList.Add(new SqlParameter("@PickingDetailsId", item.PickingDetailsId));
                    aList.Add(new SqlParameter("@IsDelete", item.IsDelete));

                    result = _accessManager.SaveData("sp_SavePickingRequest_Details", aList);
                }
                return result;
            }
            catch (Exception e)
            {
                _accessManager.SqlConnectionClose(true);
                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public DataTable GetPickingSummary(string pickingMasterId, string stockTransferMasterId, string empId)
        {

            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@PickingId", pickingMasterId));
                parameters.Add(new SqlParameter("@StoMasterId", stockTransferMasterId));
                parameters.Add(new SqlParameter("@empId", empId));
                DataTable dr = _accessManager.GetDataTable("sp_pickingSummeryReport", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetPickingDetails(string pickingMasterId, string stockTransferMasterId, string empId)
        {

            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@PickingNo", pickingMasterId));
                parameters.Add(new SqlParameter("@StoMasterId", stockTransferMasterId));
                parameters.Add(new SqlParameter("@Empid", empId));
                DataTable dr = _accessManager.GetDataTable("sp_pickingDetailsReport", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        //public DataTable GetPickingList()
        //{

        //    try
        //    {
        //        _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
        //        DataTable dr = _accessManager.GetDataTable("sp_GetAllPickInList");
        //        return dr;
        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }
        //    finally
        //    {
        //        _accessManager.SqlConnectionClose();
        //    }
        //}
        public DataTable GetPickingList()
        {

            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@TestId", 1));
                DataTable dr = _accessManager.GetDataTable("sp_GetAllPickInList", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public dynamic LetsPick(int id, string type)
        {

            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@id", id));
                parameters.Add(new SqlParameter("@type", type));
                var result = _accessManager.UpdateData("sp_UpdatePickStatus", parameters);
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public WareHouse GetWarehouseDetails(int deliveryWarehouseId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@deliveryWarehouseId", deliveryWarehouseId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetWareHouseByWarehouseId", aParameters);

                WareHouse aChange = new WareHouse();
                while (dr.Read())
                {

                    aChange.WareHouseId =  Convert.ToInt32(dr["WareHouseId"]) ;
                    aChange.WareHouseName = dr["WareHouseName"].ToString();
                    aChange.LocationId = DBNull.Value.Equals(dr["LocationId"]) ? 0 : Convert.ToInt32(dr["LocationId"]);
                    aChange.WarehouseTypeId = DBNull.Value.Equals(dr["WarehouseTypeId"]) ? 0 : Convert.ToInt32(dr["WarehouseTypeId"]);
                }

                return aChange;
            }
            catch (Exception exception)
            {

                throw exception;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


    }
}