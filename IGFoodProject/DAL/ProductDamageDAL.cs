﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using IGFoodProject.DataManager;
using IGFoodProject.Models;

namespace IGFoodProject.DAL
{
    public class ProductDamageDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();

        public DataTable GetDamageList()
        {

            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                DataTable dr = _accessManager.GetDataTable("sp_GetDamageList", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public dynamic SaveProductDamage(ProductDamageMaster entity)
        {
           
                try
                {
                    _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                    ResultResponse aResponse = new ResultResponse();
                    List<SqlParameter> aParameters = new List<SqlParameter>();

                    aParameters.Add(new SqlParameter("@CompanyId", entity.CompanyId));
                    aParameters.Add(new SqlParameter("@LocationId", entity.LocationId));
                    aParameters.Add(new SqlParameter("@WarehouseId", entity.WarehouseId));
                    aParameters.Add(new SqlParameter("@StockOutDate", entity.StockOutDate));
                    aParameters.Add(new SqlParameter("@StockOutBy", entity.StockOutBy));
                    aParameters.Add(new SqlParameter("@Comment", entity.Comment));
                    aParameters.Add(new SqlParameter("@CreateBy", entity.CreateBy));
                    aParameters.Add(new SqlParameter("@CreateDate", entity.CreateDate));

                    aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_SaveProductDamage", aParameters);
                    if (aResponse.pk > 0)
                    {
                        foreach (var item in entity.DamageDetailses)
                        {
                            List<SqlParameter> sqlParameters = new List<SqlParameter>();
                            sqlParameters.Add(new SqlParameter("@DamageMasterId", aResponse.pk));
                            sqlParameters.Add(new SqlParameter("@ProductId", item.ProductId));
                            sqlParameters.Add(new SqlParameter("@StockOutQty", item.StockOutQty));
                            sqlParameters.Add(new SqlParameter("@StockOutKg", item.StockOutKg));
                            sqlParameters.Add(new SqlParameter("@Remarks", item.Remarks));
                            aResponse.isSuccess = _accessManager.SaveData("sp_SaveProductDamageDetails", sqlParameters);
                            if (aResponse.isSuccess == false)
                            {
                                _accessManager.SqlConnectionClose(true);
                            }
                        }


                    }
                    return aResponse;
                }
                catch (Exception ex)
                {
                    _accessManager.SqlConnectionClose(true);
                    throw ex;
                }
                finally
                {
                    _accessManager.SqlConnectionClose();
                }

            return null;
        }

        public DataTable GetDumpingStockForClearing(int wareHouseId,DateTime fromDate,DateTime toDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@WareHouseId", wareHouseId));
                aParameters.Add(new SqlParameter("@fromDate", fromDate));
                aParameters.Add(new SqlParameter("@toDate", toDate));

                DataTable dt = _accessManager.GetDataTable("sp_GetDamageStockForClearing", aParameters,true);
                return dt;


            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse SaveDumpingProcessed(DumpingMaster aMaster, string user)
        {
            try
            {

                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CompanyId", aMaster.CompanyId));
                aParameters.Add(new SqlParameter("@LocationId", aMaster.LocationId));
                aParameters.Add(new SqlParameter("@DumpingDate", aMaster.DumpingDate));
                aParameters.Add(new SqlParameter("@WareHouseId", aMaster.WareHouseId));
                aParameters.Add(new SqlParameter("@TotalDumpingQty", aMaster.TotalDumpingQty));
                aParameters.Add(new SqlParameter("@TotalDumpingKg", aMaster.TotalDumpingKg));
                aParameters.Add(new SqlParameter("@DumpingBy", user));
                aParameters.Add(new SqlParameter("@Remarks", aMaster.Remarks));
                aParameters.Add(new SqlParameter("@CreateDate", System.DateTime.Now));
                aParameters.Add(new SqlParameter("@CreateBy", user));

                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_SaveDumpingMaster", aParameters);


                if (aResponse.pk > 0)
                {


                    foreach (var item in aMaster.DumpingDetails)
                    {
                        List<SqlParameter> aParametersd = new List<SqlParameter>();
                        aParametersd.Add(new SqlParameter("@DumpingMasterId", aResponse.pk));
                        aParametersd.Add(new SqlParameter("@DumpingKg", item.DumpingKg));
                        aParametersd.Add(new SqlParameter("@UnitPrice", item.UnitPrice));
                        aParametersd.Add(new SqlParameter("@DumpingQty", item.DumpingQty));
                        aParametersd.Add(new SqlParameter("@ProductID", item.ProductID)); 
                        aParametersd.Add(new SqlParameter("@StockInMasterId", item.StockInMasterId));
                        aParametersd.Add(new SqlParameter("@RefStockInDetailID", item.RefStockInDetailID));
                        aParametersd.Add(new SqlParameter("@StockOutDate", aMaster.DumpingDate));
                        aParametersd.Add(new SqlParameter("@DumpingRemarks", item.DumpingRemarks));

                        aResponse.isSuccess = _accessManager.SaveData("sp_SaveDumpingDetails", aParametersd);
                    }
                }

                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetDumpingList()
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                DataTable dt = _accessManager.GetDataTable("sp_GetDumpingList", true);
                return dt;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetDumpingInfoById(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@DumpingMasterId", id));
                DataTable dt = _accessManager.GetDataTable("sp_GetDumpingInfoById", aParameters, true);
                return dt;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

    }
}