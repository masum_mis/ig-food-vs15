﻿using IGFoodProject.DataManager;
using IGFoodProject.Models;
using IGFoodProject.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IGFoodProject.DAL
{
    public class OwnConsumptionRequisitionDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();

        public List<CustomerType> GetCustomerTypeList()
        {

            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<CustomerType> _List = new List<CustomerType>();

                //List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetCustomerType_Requisition");
                while (dr.Read())
                {
                    CustomerType aInfo = new CustomerType();
                    aInfo.CustomerTypeID = Convert.ToInt32(dr["CustomerTypeID"]);
                    aInfo.CoustomerTypeName = dr["CoustomerTypeName"].ToString();
                    aInfo.IsActive = Convert.ToBoolean(dr["IsActive"]);


                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public ViewProductDetailsSales GetProductDetails_Own(int id, DateTime? orderdate, int customertype, int wareHouseId = 0)
        {
            try
            {
                ViewProductDetailsSales aDetails = new ViewProductDetailsSales();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                aParameters.Add(new SqlParameter("@wareHouseId", 20));
                aParameters.Add(new SqlParameter("@orderdate",Convert.ToDateTime(orderdate).Date));
                aParameters.Add(new SqlParameter("@customertypeid", customertype));
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_ProductStockDetailsProcessed_Own", aParameters);
                while (dr.Read())
                {
                    aDetails.ProductID = Convert.ToInt32(dr["ProductID"]);
                    aDetails.StockQty = DBNull.Value.Equals(dr["StockQty"]) ? 0 : Convert.ToDecimal(dr["StockQty"]);
                    aDetails.StockPrice = DBNull.Value.Equals(dr["StockPrice"]) ? 0 : Convert.ToDecimal(dr["StockPrice"]);
                    aDetails.StockKg = DBNull.Value.Equals(dr["StockKg"]) ? 0 : Convert.ToDecimal(dr["StockKg"]);
                    aDetails.groupid = Convert.ToInt32(dr["GroupId"]);

                    aDetails.BasePrice = DBNull.Value.Equals(dr["BasePrice"]) ? 0 : Convert.ToDecimal(dr["BasePrice"]);
                    aDetails.IsBulk = Convert.ToBoolean(dr["IsBulk"]);
                    aDetails.MTRetention = DBNull.Value.Equals(dr["MTRetention"]) ? 0 : Convert.ToDecimal(dr["MTRetention"]);

                }
                return aDetails;
            }
            catch (Exception ex)
            {

                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public ResultResponse SaveSalesOrderProcessed(SalesOrderProcessedMaster aMaster, string user)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);


                if (aMaster.eflag == "ed")
                {
                    var addIn = aMaster.SalesDetails.Find(a => a.OperationIdAdd == 3); // Add In Challan if add in challan Is Approve =1
                    if (addIn != null)
                    {
                        aMaster.IsApprove = 1;
                    }
                    var deletIn = aMaster.SalesDetails.Find(a => a.OperationIdDelete == 3); // Delete from  Challan if delete in challan Is Approve =1
                    if (deletIn != null)
                    {
                        aMaster.IsApprove = 1;
                    }
                }
                else
                {
                    aMaster.IsApprove = 1;
                }



                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CustomerId", aMaster.CustomerId));
                aParameters.Add(new SqlParameter("@SalesOrderProcessedMasterId", aMaster.SalesOrderProcessedMasterId));
                aParameters.Add(new SqlParameter("@OrderType", aMaster.OrderType));
                if (aMaster.IsEmployee == true)
                {
                    aParameters.Add(new SqlParameter("@SalesPerson", aMaster.SalesPerson.Split(':')[0].Trim()));
                }
                else
                {
                    aParameters.Add(new SqlParameter("@SalesPerson", aMaster.SalesPerson));
                }
                aParameters.Add(new SqlParameter("@SrId", aMaster.SrId));
                aParameters.Add(new SqlParameter("@OrderDate", aMaster.OrderDate));
                aParameters.Add(new SqlParameter("@DeliveryDestination", aMaster.DeliveryDestination));
                aParameters.Add(new SqlParameter("@CreateBy", user));
                aParameters.Add(new SqlParameter("@OrderBy", user));
                aParameters.Add(new SqlParameter("@CreateDate", System.DateTime.Now));


                //aParameters.Add(new SqlParameter("@GroupId", aMaster.GroupId));
                //aParameters.Add(new SqlParameter("@PaymentType", aMaster.PaymentType));

                // aParameters.Add(new SqlParameter("@BankId", aMaster.BankId));
                // aParameters.Add(new SqlParameter("@BranchId", aMaster.BranchId));
                // aParameters.Add(new SqlParameter("@ChequeNo", aMaster.ChequeNo));
                //  aParameters.Add(new SqlParameter("@ChequeDate", aMaster.ChequeDate));



                aParameters.Add(new SqlParameter("@CompanyId", aMaster.CompanyId));
                aParameters.Add(new SqlParameter("@SalesPersonContact", aMaster.SalesPersonContact));
                aParameters.Add(new SqlParameter("@DeliveryDate", aMaster.DeliveryDate));
                aParameters.Add(new SqlParameter("@TotatalOrderAmtKg", aMaster.TotatalOrderAmtKg));
                aParameters.Add(new SqlParameter("@TotalIncTaxVat", aMaster.TotalIncTaxVat));

                aParameters.Add(new SqlParameter("@TotalExTaxVat", aMaster.TotalExTaxVat));
                aParameters.Add(new SqlParameter("@TaxVat", aMaster.TaxVat));

                aParameters.Add(new SqlParameter("@collectiontypeid", aMaster.CollectionType));
                aParameters.Add(new SqlParameter("@collectionamount", aMaster.CollectionAmount));
                aParameters.Add(new SqlParameter("@IsEmployee", aMaster.IsEmployee));
                aParameters.Add(new SqlParameter("@CustomerTypeID", aMaster.CustomerTypeId));

                aParameters.Add(new SqlParameter("@ColBankId", aMaster.ColBankId));
                aParameters.Add(new SqlParameter("@ColBranchId", aMaster.ColBranchId));

                aParameters.Add(new SqlParameter("@RefNo", aMaster.RefNo));
                aParameters.Add(new SqlParameter("@RefDate", aMaster.RefDate));



                aParameters.Add(new SqlParameter("@Remarks", aMaster.Remarks));
                if (aMaster.eflag == "ap")
                {
                    aParameters.Add(new SqlParameter("@IsApprove", 1));
                }
                else
                {
                    aParameters.Add(new SqlParameter("@IsApprove", aMaster.IsApprove));
                }

                aParameters.Add(new SqlParameter("@eflag", aMaster.eflag));


                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_Save_SalesOrderProcessed_Own", aParameters);
                if (aResponse.pk > 0)
                {
                    if (aMaster.IsApprove == 1 || aMaster.IsApprove == 0) // if Approve or new Entry
                    {
                        aResponse.isSuccess = SaveSalesOrderProcessedDetails(aMaster.SalesDetails, aResponse.pk, user, aMaster.IsApprove);
                    }
                    if (aMaster.IsApprove == 2) // if Reject
                    {
                        aResponse.isSuccess = true;
                    }

                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                }
                else
                {
                    _accessManager.SqlConnectionClose(true);
                }
                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public bool SaveSalesOrderProcessedDetails(List<SalesOrderProcessedDetails> aList, int pk, string user, int isApprove)
        {
            try
            {
                bool result = false;
                foreach (var item in aList)
                {
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@SalesOrderProcessedMasterId", pk));
                    aParameters.Add(new SqlParameter("@SalesOrderProcessDetailsId", item.SalesOrderProcessDetailsId));
                    aParameters.Add(new SqlParameter("@ProductId", item.ProductId));
                    aParameters.Add(new SqlParameter("@ProductQty", item.ProductQty));
                    aParameters.Add(new SqlParameter("@ProductKg", item.ProductKg));
                    aParameters.Add(new SqlParameter("@StockRate", item.StockRate));
                    aParameters.Add(new SqlParameter("@SaleRate", item.SaleRate));
                    aParameters.Add(new SqlParameter("@BaseRate", item.BaseRate));
                    aParameters.Add(new SqlParameter("@DiscountAmount", item.DiscountAmount));
                    aParameters.Add(new SqlParameter("@DiscountPercent", item.DiscountPercent));
                    aParameters.Add(new SqlParameter("@TotalPrice", item.TotalPrice));
                    aParameters.Add(new SqlParameter("@BTotalPrice", item.BTotalPrice));
                    aParameters.Add(new SqlParameter("@ProductTax", item.ProductTax));
                    aParameters.Add(new SqlParameter("@CreateBy", user));
                    aParameters.Add(new SqlParameter("@IsDelete", item.IsDelete));
                    aParameters.Add(new SqlParameter("@isApprove", isApprove));
                    aParameters.Add(new SqlParameter("@OperationIdAdd", item.OperationIdAdd));
                    aParameters.Add(new SqlParameter("@OperationIdDelete", item.OperationIdDelete));
                    aParameters.Add(new SqlParameter("@CreateDate", DateTime.Now));
                    aParameters.Add(new SqlParameter("@groupid", item.GroupId));
                    aParameters.Add(new SqlParameter("@haschallan", item.haschallan));
                    aParameters.Add(new SqlParameter("@eflag", item.eflag));
                    aParameters.Add(new SqlParameter("@alreadypicked", item.alreadypicked));
                    aParameters.Add(new SqlParameter("@isbulk", item.isbulk));
                    aParameters.Add(new SqlParameter("@MTRetentionFront", item.MTRetention));
                    result = _accessManager.SaveData("sp_Save_SavelOrderProcessedDetails", aParameters);
                    if (result == false)
                    {
                        break;
                    }

                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<ViewSalesOrderProcessedMaster> GetSalesOrderProcessedMaster(DateTime? fromdate, DateTime? todate, string salesperson, int customerid = 0)
        {
            try
            {

                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-1);

                }
                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }
                if (salesperson != null)
                {
                    salesperson = salesperson.Split(':')[0].Trim();
                }
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<ViewSalesOrderProcessedMaster> aList = new List<ViewSalesOrderProcessedMaster>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@customerid", customerid));
                aParameters.Add(new SqlParameter("@SalesPerson", salesperson));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_SalesOrderProcessedList_Requisition", aParameters);
                while (dr.Read())
                {
                    ViewSalesOrderProcessedMaster aMaster = new ViewSalesOrderProcessedMaster();

                    aMaster.SalesOrderProcessedMasterId = Convert.ToInt32(dr["SalesOrderProcessedMasterId"]);
                    aMaster.TotatalOrderAmtKg = Convert.ToDecimal(dr["TotatalOrderAmtKg"]);
                    aMaster.TotalIncTaxVat = Convert.ToDecimal(dr["TotalIncTaxVat"]);
                    aMaster.TotalExTaxVat = Convert.ToDecimal(dr["TotalExTaxVat"]);
                    aMaster.TotalDue = Convert.ToDecimal(dr["TotalDue"]);
                    aMaster.TotalPaid = Convert.ToDecimal(dr["TotalPaid"]);

                    aMaster.SalesPerson = dr["SalesPerson"].ToString();
                    aMaster.SalesOrderNo = dr["SalesOrderNo"].ToString();
                    aMaster.CustomerName = dr["CustomerName"].ToString();
                    aMaster.DeliveryDestination = dr["DeliveryDestination"].ToString();
                    aMaster.SalesPersonContact = dr["SalesPersonContact"].ToString();

                    aMaster.CreateBy = dr["CreateBy"].ToString();
                    aMaster.ApproveBy = dr["ApproveBy"].ToString();
                    aMaster.ApproveDate = dr["ApproveDate"] as DateTime?;
                    aMaster.CreateDate = dr["CreateDate"] as DateTime?;
                    aMaster.OrderDate = dr["OrderDate"] as DateTime?;
                    aMaster.DeliveryDate = dr["DeliveryDate"] as DateTime?;
                    if (DBNull.Value == dr["IsApprove"])
                    {
                        aMaster.IsApprove = 0;
                    }
                    else
                    {
                        aMaster.IsApprove = Convert.ToInt32(dr["IsApprove"]);
                    }
                    if (DBNull.Value == dr["IsEmployee"])
                    {
                        aMaster.IsEmployee = null;
                    }
                    else
                    {
                        aMaster.IsEmployee = (bool)dr["IsEmployee"];
                    }
                    aMaster.CustomerType = dr["CoustomerTypeName"].ToString();
                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<ViewCustomerInfo> GetCustomerList()
        {

            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ViewCustomerInfo> _List = new List<ViewCustomerInfo>();
                //List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                //aSqlParameterList.Add(new SqlParameter("@customertypeid", typeid));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetCustomerListRequisition");
                while (dr.Read())
                {
                    ViewCustomerInfo aInfo = new ViewCustomerInfo();
                    aInfo.CustomerID = Convert.ToInt32(dr["CustomerID"]);
                    aInfo.CustomerName = dr["CustomerName"].ToString();
                    aInfo.CustomerCode = dr["CustomerCode"].ToString();
                    aInfo.DivisionName = dr["DivisionName"].ToString();
                    aInfo.DistrictName = dr["DistrictName"].ToString();
                    aInfo.MarketName = dr["MarketName"].ToString();
                    aInfo.CoustomerTypeName = dr["CoustomerTypeName"].ToString();
                    aInfo.PaymentType = dr["PaymentType"].ToString();
                    aInfo.CreateDate = dr["CreateDate"] as DateTime?;

                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<ViewSalesOrderProcessedMaster> GetRequisitionMaster(DateTime? fromdate, DateTime? todate, string salesperson, int customerid = 0)
        {
            try
            {

                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-1);

                }
                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }
                if (salesperson != null)
                {
                    salesperson = salesperson.Split(':')[0].Trim();
                }
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<ViewSalesOrderProcessedMaster> aList = new List<ViewSalesOrderProcessedMaster>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@customerid", customerid));
                aParameters.Add(new SqlParameter("@SalesPerson", salesperson));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_SalesOrderProcessedList_Requisition", aParameters);
                while (dr.Read())
                {
                    ViewSalesOrderProcessedMaster aMaster = new ViewSalesOrderProcessedMaster();

                    aMaster.SalesOrderProcessedMasterId = Convert.ToInt32(dr["SalesOrderProcessedMasterId"]);
                    aMaster.TotatalOrderAmtKg = Convert.ToDecimal(dr["TotatalOrderAmtKg"]);
                    aMaster.TotalIncTaxVat = Convert.ToDecimal(dr["TotalIncTaxVat"]);
                    aMaster.TotalExTaxVat = Convert.ToDecimal(dr["TotalExTaxVat"]);
                    aMaster.TotalDue = Convert.ToDecimal(dr["TotalDue"]);
                    aMaster.TotalPaid = Convert.ToDecimal(dr["TotalPaid"]);

                    aMaster.SalesPerson = dr["SalesPerson"].ToString();
                    aMaster.SalesOrderNo = dr["SalesOrderNo"].ToString();
                    aMaster.CustomerName = dr["CustomerName"].ToString();
                    aMaster.DeliveryDestination = dr["DeliveryDestination"].ToString();
                    aMaster.SalesPersonContact = dr["SalesPersonContact"].ToString();

                    aMaster.CreateBy = dr["CreateBy"].ToString();
                    aMaster.ApproveBy = dr["ApproveBy"].ToString();
                    aMaster.ApproveDate = dr["ApproveDate"] as DateTime?;
                    aMaster.CreateDate = dr["CreateDate"] as DateTime?;
                    aMaster.OrderDate = dr["OrderDate"] as DateTime?;
                    aMaster.DeliveryDate = dr["DeliveryDate"] as DateTime?;
                    if (DBNull.Value == dr["IsApprove"])
                    {
                        aMaster.IsApprove = 0;
                    }
                    else
                    {
                        aMaster.IsApprove = Convert.ToInt32(dr["IsApprove"]);
                    }
                    if (DBNull.Value == dr["IsEmployee"])
                    {
                        aMaster.IsEmployee = null;
                    }
                    else
                    {
                        aMaster.IsEmployee = (bool)dr["IsEmployee"];
                    }
                    aMaster.CustomerType = dr["CoustomerTypeName"].ToString();
                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<ViewSalesOrderProcessedMaster> GetRequisitionForIssue()
        {
            try

            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ViewSalesOrderProcessedMaster> aList = new List<ViewSalesOrderProcessedMaster>();
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_OwnRequisitionListForIssue");
                while (dr.Read())
                {
                    ViewSalesOrderProcessedMaster aMaster = new ViewSalesOrderProcessedMaster();

                    aMaster.SalesOrderProcessedMasterId = Convert.ToInt32(dr["SalesOrderProcessedMasterId"]);
                    //aMaster.PickingMasterId = Convert.ToInt32(dr["PickingMasterId"]);
                    aMaster.TotatalOrderAmtKg = Convert.ToDecimal(dr["TotatalOrderAmtKg"]);
                    aMaster.TotalIncTaxVat = Convert.ToDecimal(dr["TotalIncTaxVat"]);
                    aMaster.TotalExTaxVat = Convert.ToDecimal(dr["TotalExTaxVat"]);

                    aMaster.SalesPerson = dr["SalesPerson"].ToString();
                    aMaster.SalesOrderNo = dr["SalesOrderNo"].ToString();
                   
                    aMaster.CustomerName = dr["CustomerName"].ToString();
                    aMaster.DeliveryDestination = dr["DeliveryDestination"].ToString();
                    aMaster.SalesPersonContact = dr["SalesPersonContact"].ToString();

                    aMaster.CreateBy = dr["CreateBy"].ToString();
                    aMaster.ApproveBy = dr["ApproveBy"].ToString();
                    aMaster.ApproveDate = dr["ApproveDate"] as DateTime?;
                    aMaster.CreateDate = dr["CreateDate"] as DateTime?;
                    aMaster.OrderDate = dr["OrderDate"] as DateTime?;
                    aMaster.DeliveryDate = dr["DeliveryDate"] as DateTime?;
                    if (DBNull.Value == dr["IsApprove"])
                    {
                        aMaster.IsApprove = 0;
                    }
                    else
                    {
                        aMaster.IsApprove = Convert.ToInt32(dr["IsApprove"]);
                    }

                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public SalesOrderProcessedMaster GetSalesOrderProcessedMaster(int id)
        {
            try
            {
                List<SqlParameter> aParameters = new List<SqlParameter>();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                aParameters.Add(new SqlParameter("@id", id));
                DataTable dt = _accessManager.GetDataTable("sp_Get_SalesOrderProcessedMasrerById", aParameters);
                List<SalesOrderProcessedMaster> aList = Helper.ToListof<SalesOrderProcessedMaster>(dt);
                return aList[0];
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<SalesOrderProcessedDetails> GetRequisitionDetailsByMaster_Issue(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                DataTable dt = _accessManager.GetDataTable("sp_Get_RequisitionDetailsByMaster_Issue", aParameters);
                List<SalesOrderProcessedDetails> aList = Helper.ToListof<SalesOrderProcessedDetails>(dt);
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public ViewProductDetailsSales GetProductDetails(int id, DateTime? orderdate, int customertype, int wareHouseId = 0)
        {
            try
            {
                ViewProductDetailsSales aDetails = new ViewProductDetailsSales();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                aParameters.Add(new SqlParameter("@wareHouseId", wareHouseId));
                aParameters.Add(new SqlParameter("@orderdate", orderdate));
                aParameters.Add(new SqlParameter("@customertypeid", customertype));
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_ProductStockDetailsProcessed", aParameters);
                while (dr.Read())
                {
                    aDetails.ProductID = Convert.ToInt32(dr["ProductID"]);
                    aDetails.StockQty = DBNull.Value.Equals(dr["StockQty"]) ? 0 : Convert.ToDecimal(dr["StockQty"]);
                    aDetails.StockPrice = DBNull.Value.Equals(dr["StockPrice"]) ? 0 : Convert.ToDecimal(dr["StockPrice"]);
                    aDetails.StockKg = DBNull.Value.Equals(dr["StockKg"]) ? 0 : Convert.ToDecimal(dr["StockKg"]);
                    aDetails.groupid = Convert.ToInt32(dr["GroupId"]);

                    aDetails.BasePrice = DBNull.Value.Equals(dr["BasePrice"]) ? 0 : Convert.ToDecimal(dr["BasePrice"]);
                    aDetails.IsBulk = Convert.ToBoolean(dr["IsBulk"]);
                    aDetails.MTRetention = DBNull.Value.Equals(dr["MTRetention"]) ? 0 : Convert.ToDecimal(dr["MTRetention"]);

                }
                return aDetails;
            }
            catch (Exception ex)
            {

                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public ViewProductDetailsSales GetProductDetailsForChallan(int id, int wareHouseId, int customerType, DateTime orderDate, DateTime challanDate)
        {
            try
            {

                ViewProductDetailsSales aDetails = new ViewProductDetailsSales();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                aParameters.Add(new SqlParameter("@wareHouseId", wareHouseId));
                aParameters.Add(new SqlParameter("@orderdate", orderDate));
                aParameters.Add(new SqlParameter("@challanDate", challanDate));
                aParameters.Add(new SqlParameter("@customertypeid", customerType));
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_ProductStockDetailsProcessed_ForChallan", aParameters);
                while (dr.Read())
                {
                    aDetails.ProductID = Convert.ToInt32(dr["ProductID"]);
                    aDetails.StockQty = DBNull.Value.Equals(dr["StockQty"]) ? 0 : Convert.ToDecimal(dr["StockQty"]);
                    aDetails.StockPrice = DBNull.Value.Equals(dr["StockPrice"]) ? 0 : Convert.ToDecimal(dr["StockPrice"]);
                    aDetails.StockKg = DBNull.Value.Equals(dr["StockKg"]) ? 0 : Convert.ToDecimal(dr["StockKg"]);
                    aDetails.groupid = Convert.ToInt32(dr["GroupId"]);

                    aDetails.BasePrice = DBNull.Value.Equals(dr["BasePrice"]) ? 0 : Convert.ToDecimal(dr["BasePrice"]);


                }
                return aDetails;
            }
            catch (Exception ex)
            {

                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse SaveIssueProcessedChallan(SalesOrderChallanMaster aMaster, string user)
        {
            try
            {

                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CompanyId", aMaster.CompanyId));
                aParameters.Add(new SqlParameter("@SalesOrderId", aMaster.SalesOrderId));
                aParameters.Add(new SqlParameter("@DriverName", aMaster.DriverName));
                aParameters.Add(new SqlParameter("@DriverContact", aMaster.DriverContact));
                aParameters.Add(new SqlParameter("@VehicleId", aMaster.VehicleId));
                aParameters.Add(new SqlParameter("@WarehouseId", aMaster.WarehouseId));
                aParameters.Add(new SqlParameter("@ChallanAmount", aMaster.ChallanAmount));
                aParameters.Add(new SqlParameter("@TotalDeliveryQty", aMaster.TotalDeliveryQty));
                aParameters.Add(new SqlParameter("@TotalDeliveryKG", aMaster.TotalDeliveryKG));
                aParameters.Add(new SqlParameter("@ChallanDate", aMaster.ChallanDate));
                aParameters.Add(new SqlParameter("@CreateDate", System.DateTime.Now));
                aParameters.Add(new SqlParameter("@CreateBy", user));
                aParameters.Add(new SqlParameter("@Remarks", aMaster.Remarks));

                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_Save_OwnRequisitionIssueMaster", aParameters);


                if (aResponse.pk > 0)
                {
                    aResponse.isSuccess = SaveIssueDetailsProcessed(aMaster.ChallanDetails, aResponse.pk);
                    aResponse.isSuccess = true;

                    if (aResponse.isSuccess == true)
                    {
                        aResponse.isSuccess = UpdateChallanMaster(aMaster.SalesOrderId);
                    }
                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                }

                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public bool SaveIssueDetailsProcessed(List<SalesOrderChallanDetails> challanList, int pk)
        {
            try
            {
                bool result = false;
                //For Processed
                foreach (var item in challanList)
                {
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@SalesOrderDetailsId", item.SalesOrderDetailsId));
                    aParameters.Add(new SqlParameter("@SalesOrderChallanMasterId", pk));
                    aParameters.Add(new SqlParameter("@IssueKg", item.IssueKg));
                    aParameters.Add(new SqlParameter("@IssuePrice", item.IssuePrice));
                    aParameters.Add(new SqlParameter("@IssueQty", item.IssueQty));
                    aParameters.Add(new SqlParameter("@WarehouseId", item.WarehouseId));
                    aParameters.Add(new SqlParameter("@ProductId", item.ProductId));
                 
                    aParameters.Add(new SqlParameter("@aFlag", item.aFlag));

                    if (item.GroupId == 5)
                    {
                        result = _accessManager.SaveData("sp_Calculate_Challan_Processed_KG_Issue", aParameters);
                    }
                    if (item.GroupId == 6)
                    {
                        result = _accessManager.SaveData("sp_Calculate_Challan_Processed_Issue", aParameters);
                    }
                    if (item.GroupId == 7)
                    {
                        result = _accessManager.SaveData("sp_Calculate_Challan_Processed_ByProduct_Issue", aParameters);
                    }



                    if (result == false)
                    {
                        break;
                    }

                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {

            }
        }

        public bool UpdateChallanMaster(int SalesOrderId)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@SalesOrderId", SalesOrderId));
                result = _accessManager.SaveData("sp_update_SalesOrderMaster_Issue", aParameters);

                return result;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<ViewGeneratedChallan> GetGeneratedIssue(int id = 0)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_GeneratedIssue", aParameters);

                List<ViewGeneratedChallan> aList = new List<ViewGeneratedChallan>();
                while (dr.Read())
                {
                    ViewGeneratedChallan aChallan = new ViewGeneratedChallan();
                    aChallan.SalesOrderChallanMasterId = Convert.ToInt32(dr["SalesOrderChallanMasterId"]);
                    aChallan.SalesOrderChallanNo = dr["SalesOrderChallanNo"].ToString();
                    aChallan.DeliveryDestination = dr["DeliveryDestination"].ToString();
                    aChallan.SalesOrderNo = dr["SalesOrderNo"].ToString();
                    aChallan.OrderBy = dr["OrderBy"].ToString();
                    aChallan.GroupName = dr["GroupName"].ToString();
                    aChallan.CreateBy = dr["CreateBy"].ToString();
                    aChallan.ChallanAmount = Convert.ToDecimal(dr["ChallanAmount"].ToString());
                    aChallan.CreateDate = dr["CreateDate"] as DateTime?;
                    aChallan.OrderDate = dr["OrderDate"] as DateTime?;
                    aChallan.GroupId = DBNull.Value.Equals(dr["GroupId"]) ? 0 : Convert.ToInt32(dr["GroupId"]);
                    aChallan.CustomerName = dr["CustomerName"].ToString();
                    aChallan.ChallanQty = Convert.ToDecimal(dr["TotalChallanQty"].ToString());
                    aChallan.ChallanKg = Convert.ToDecimal(dr["TotalChallanKG"].ToString());

                    aList.Add(aChallan);
                }
                return aList;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        
        public List<ViewGeneratedChallanDetails> GetGeneratedIssueDetails(int id)
        {
            try
            {
                List<ViewGeneratedChallanDetails> aList = new List<ViewGeneratedChallanDetails>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                DataTable dt = _accessManager.GetDataTable("sp_Get_Challan_DetailsByMaster", aParameters);
                aList = Helper.ToListof<ViewGeneratedChallanDetails>(dt);
                return aList;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetOwnConsumptionSalesOrderInvoice(string orderId, bool isTrade = false)
        {
            try
            {
                DataTable dr = new DataTable();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@OrderId", orderId));
                dr = _accessManager.GetDataTable("sp_GetOwnConsumptionSalesOrderInvoice", parameters);

                //if (isTrade)
                //{
                //    dr = _accessManager.GetDataTable("sp_RptTradeSalesInvoice", parameters);
                //}
                //else
                //{
                //    dr = _accessManager.GetDataTable("sp_RptSalesOrderForProcess", parameters);
                //}

                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
    }
}