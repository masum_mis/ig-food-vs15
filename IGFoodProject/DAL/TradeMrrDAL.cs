﻿using IGFoodProject.DataManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IGFoodProject.Models;
using IGFoodProject.Models.Accounts;
using IGFoodProject.ViewModel;
using System.Data.SqlClient;
using System.Data;

namespace IGFoodProject.DAL
{
    public class TradeMrrDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();

        public List<ViewTradeMrr> PoList(DateTime? fromdate, DateTime? todate, int poNo = 0, int supplierId = 0, int productId = 0)
        {
            try
            {
                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-2);

                }
                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<ViewTradeMrr> aList = new List<ViewTradeMrr>();
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@PoNo", poNo));
                aParameters.Add(new SqlParameter("@SupplierId", supplierId));
                aParameters.Add(new SqlParameter("@productId", productId));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetTradePOListForMrr", aParameters);
                while (dr.Read())
                {
                    ViewTradeMrr aMaster = new ViewTradeMrr();
                    aMaster.TradePurchaseOrderId = Convert.ToInt32(dr["TradePurchaseMasterId"]);
                    aMaster.TradeNo = dr["TradeNo"].ToString();
                    aMaster.PurchaseDate = Convert.ToDateTime(dr["PurchaseDate"]);

                    aMaster.ExpectedDeliveryDate = (dr["ExpectedDeliveryDate"] == DBNull.Value) ? (DateTime?)null : ((DateTime)dr["ExpectedDeliveryDate"]);
                    aMaster.SupplierName = dr["SupplierName"].ToString();

                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.CurrencyName = dr["CurrencyCode"].ToString();

                    aMaster.OrderTotalAmt = Convert.ToDecimal(dr["TotalAmount"]);

                    aMaster.LastRcvDate = (dr["LastRcvDate"] == DBNull.Value) ? (DateTime?)null : ((DateTime)dr["LastRcvDate"]);

                    //aMaster.LastRcvDate = DBNull.Value.Equals(dr["LastRcvDate"]) ? '':


                    //   aMaster.LastRcvDate = Convert.ToDateTime(dr["LastRcvDate"]);
                    aMaster.TotalQtyRcv = Convert.ToDecimal(dr["TotalQtyRcv"]);
                    aMaster.TotalAmountRcv = Convert.ToDecimal(dr["TotalAmountRcv"]);
                    aMaster.RestAmount = Convert.ToDecimal(dr["RestAmount"]);

                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public List<ViewTradeMrr> LoadTradePONoForMRR()
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);


                SqlDataReader dr = _accessManager.GetSqlDataReader("GetTradePONoForMrr");
                List<ViewTradeMrr> ItemDescriptionList = new List<ViewTradeMrr>();
                while (dr.Read())
                {
                    ViewTradeMrr itemDescription = new ViewTradeMrr();
                    itemDescription.TradePurchaseOrderId = Convert.ToInt32(dr["TradePurchaseMasterId"]);
                    itemDescription.TradeNo = dr["TradeNo"].ToString();
                    ItemDescriptionList.Add(itemDescription);
                }

                return ItemDescriptionList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<TradeMrrDetails> TradePoDetailsForMrr(int id)
        {
            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<TradeMrrDetails> aList = new List<TradeMrrDetails>();
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@Id", id));

                SqlDataReader dr = _accessManager.GetSqlDataReader("GetTradePODetailsByPoIdForMrr", aParameters);
                while (dr.Read())
                {
                    TradeMrrDetails aMaster = new TradeMrrDetails();
                    aMaster.TradePurchaseOrderDetailsId = Convert.ToInt32(dr["TradePurchaseDetailId"]);
                    aMaster.ProductNo = dr["ProductNo"].ToString();
                    aMaster.ProductName = dr["ProductName"].ToString();
                    aMaster.UOMName = dr["UOMName"].ToString();
                    aMaster.ApproveQty = DBNull.Value.Equals(dr["ApproveQty"]) ? 0 : Convert.ToDecimal(dr["ApproveQty"]);
                    aMaster.ApproveKg = DBNull.Value.Equals(dr["ApproveKg"]) ? 0 : Convert.ToDecimal(dr["ApproveKg"]);
                    aMaster.MRRRemainingQty = DBNull.Value.Equals(dr["MRRRemainingQty"]) ? 0 : Convert.ToDecimal(dr["MRRRemainingQty"]);
                    aMaster.MRRRemainingKG = DBNull.Value.Equals(dr["MRRRemainingKG"]) ? 0 : Convert.ToDecimal(dr["MRRRemainingKG"]);
                    aMaster.ApproveUnitPrice = DBNull.Value.Equals(dr["ApproveUnitPrice"]) ? 0 : Convert.ToDecimal(dr["ApproveUnitPrice"]);
                    aMaster.ReceivedQty = DBNull.Value.Equals(dr["ReceivedQty"]) ? 0 : Convert.ToDecimal(dr["ReceivedQty"]);
                    aMaster.ReceivedKG = DBNull.Value.Equals(dr["ReceivedKG"]) ? 0 : Convert.ToDecimal(dr["ReceivedKG"]);
                    aMaster.ProductId = Convert.ToInt32(dr["ProductId"]);
                    aMaster.GroupId = Convert.ToInt32(dr["GroupId"]);


                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public TradeMrrMaster GetTradePoInfoByMasterById(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                TradeMrrMaster aMaster = new TradeMrrMaster();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@Id", id));

                SqlDataReader dr = _accessManager.GetSqlDataReader("GetTradePOInfoById", aParameters);
                while (dr.Read())
                {

                    aMaster.TradePurchaseOrderId = Convert.ToInt32(dr["TradePurchaseMasterId"]);
                    aMaster.SupplierID = Convert.ToInt32(dr["SupplierID"]);
                    aMaster.DelLocationId = Convert.ToInt32(dr["DelLocationId"]);
                    aMaster.TradeNo = dr["TradeNo"].ToString();
                    aMaster.PurchaseDate = Convert.ToDateTime(dr["PurchaseDate"]);
                    aMaster.SupplierName = dr["SupplierName"].ToString();
                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.CurrencyName = dr["CurrencyCode"].ToString();
                    //aMaster.Comments = dr["Comments"].ToString();
                    //aMaster.SupplierRef = dr["SupplierRef"].ToString();
                    //aMaster.DeliveryTo = dr["DeliveryTo"].ToString();
                }
                return aMaster;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<tbl_ProductPackSize> LoadPackSize()
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);


                SqlDataReader dr = _accessManager.GetSqlDataReader("GetAllPackSize");
                List<tbl_ProductPackSize> ItemDescriptionList = new List<tbl_ProductPackSize>();
                while (dr.Read())
                {
                    tbl_ProductPackSize itemDescription = new tbl_ProductPackSize();
                    itemDescription.PackSizeId = Convert.ToInt32(dr["PackSizeId"]);
                    itemDescription.PackSizeName = dr["PackSizeName"].ToString();
                    // itemDescription.PackWeight = Convert.ToDecimal(dr["PackWeight"]);
                    ItemDescriptionList.Add(itemDescription);
                }

                return ItemDescriptionList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse SaveTradeMrr(TradeMrrMaster aMaster, string user)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@TradeMrrMasterId", aMaster.TradeMrrMasterId));
                aParameters.Add(new SqlParameter("@TradePurchaseOrderId", aMaster.TradePurchaseOrderId));
                aParameters.Add(new SqlParameter("@MRRDate", aMaster.MRRDate));
                aParameters.Add(new SqlParameter("@ChallanDate", aMaster.ChallanDate));
                aParameters.Add(new SqlParameter("@SupplierID", aMaster.SupplierID));
                aParameters.Add(new SqlParameter("@ChallanNo", aMaster.ChallanNo));
                aParameters.Add(new SqlParameter("@LabResult", aMaster.LabResult));
                aParameters.Add(new SqlParameter("@VehicleNo", aMaster.VehicleNo));
                aParameters.Add(new SqlParameter("@VehicleInWeight", aMaster.VehicleInWeight));
                aParameters.Add(new SqlParameter("@VehicleOutWeight", aMaster.VehicleOutWeight));
                aParameters.Add(new SqlParameter("@SpecialInstruction", aMaster.SpecialInstruction));
                aParameters.Add(new SqlParameter("@LaborCost", aMaster.LaborCost));
                aParameters.Add(new SqlParameter("@TransportCost", aMaster.TransportCost));
                aParameters.Add(new SqlParameter("@TotalQty", aMaster.TradeMrrDetails.Sum(n => n.MRRQty)));
                aParameters.Add(new SqlParameter("@TotalKG", aMaster.TradeMrrDetails.Sum(n => n.MRRKG)));
                aParameters.Add(new SqlParameter("@TotalPrice", aMaster.TradeMrrDetails.Sum(n => n.TotalPrice)));
                aParameters.Add(new SqlParameter("@IsApprove", aMaster.IsApprove));
                aParameters.Add(new SqlParameter("@CreateBy", user));

                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("SaveTradeMrr", aParameters);

                if (aResponse.pk > 0)
                {
                    if (aMaster.TradeMrrDetails.Count > 0)
                    {
                        aResponse.isSuccess = SaveTradeMrrDetails(aMaster.TradeMrrDetails, aResponse.pk, aMaster.TradePurchaseOrderId);
                    }
                    if(aMaster.IsApprove == true)
                    {
                        aResponse.isSuccess = SaveTradeStockIn(aResponse.pk);
                        aResponse.isSuccess = CreateSystemJournal(aResponse.pk);
                    }
                 
                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                }
                else
                {
                    _accessManager.SqlConnectionClose(true);
                }
                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public bool SaveTradeMrrDetails(List<TradeMrrDetails> aMaster, int TradeMrrMasterId, int? poId)
        {
            try
            {
                bool result = false;

                foreach (var item in aMaster)
                {
                    List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                    ResultResponse aResponse = new ResultResponse();

                    aSqlParameterList.Add(new SqlParameter("@TradeMrrMasterId", TradeMrrMasterId));
                    aSqlParameterList.Add(new SqlParameter("@TradeMRRDetailID", item.TradeMRRDetailID));
                    aSqlParameterList.Add(new SqlParameter("@ProductId", item.ProductId));
                    aSqlParameterList.Add(new SqlParameter("@TradePurchaseOrderDetailsId", item.TradePurchaseOrderDetailsId));
                    aSqlParameterList.Add(new SqlParameter("@MRRQty", item.MRRQty));
                    aSqlParameterList.Add(new SqlParameter("@MRRKG", item.MRRKG));
                    aSqlParameterList.Add(new SqlParameter("@UnitPrice", item.UnitPrice));
                    aSqlParameterList.Add(new SqlParameter("@TotalPrice", item.TotalPrice));
                    aSqlParameterList.Add(new SqlParameter("@PackSizeId", item.PackSizeId));
                    aSqlParameterList.Add(new SqlParameter("@NoOfPack", item.NoOfPack));
                    aSqlParameterList.Add(new SqlParameter("@MfgDate", item.MfgDate));
                    aSqlParameterList.Add(new SqlParameter("@ExpairyDate", item.ExpairyDate));
                    aSqlParameterList.Add(new SqlParameter("@PoId", poId));

                    aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("SaveTradeMrrDetails", aSqlParameterList);
                }
                return result = true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool CreateSystemJournal(int TradeMrrMasterId)
        {

            try
            {

                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@TradeMrrMasterId", TradeMrrMasterId));
                result = _accessManager.SaveData("SaveTradeMrrJournal", aParameters);

                return result;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public bool SaveTradeStockIn(int TradeMrrMasterId)
        {

            try
            {

                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@TradeMrrMasterId", TradeMrrMasterId));
                result = _accessManager.SaveData("sp_SaveTradeMrrStock", aParameters);
                //   return result;

                return result = true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<ViewTradeMrr> TradeMrrList(DateTime? fromdate, DateTime? todate, int mrrNO = 0, int supplierId = 0, int productId = 0)
        {
            try
            {
                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-2);

                }
                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<ViewTradeMrr> aList = new List<ViewTradeMrr>();
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@MRRNo", mrrNO));
                aParameters.Add(new SqlParameter("@SupplierId", supplierId));
                aParameters.Add(new SqlParameter("@productId", productId));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetTradeMrrList", aParameters);
                while (dr.Read())
                {
                    ViewTradeMrr aMaster = new ViewTradeMrr();
                    aMaster.TradeMrrMasterId = Convert.ToInt32(dr["TradeMrrMasterId"]); 
                    aMaster.TradePurchaseOrderId= Convert.ToInt32(dr["TradePurchaseMasterId"]);
                    aMaster.TradeNo = dr["TradeNo"].ToString();
                    //aMaster.LastRcvDate = (dr["OrderDate"] == DBNull.Value) ? (DateTime?)null : ((DateTime)dr["OrderDate"]);
                    aMaster.PurchaseDate = (dr["PurchaseDate"] == DBNull.Value) ? (DateTime?)null : ((DateTime)dr["PurchaseDate"]);
                    //  aMaster.OrderDate = Convert.ToDateTime(dr["OrderDate"]);
                    aMaster.MrrDate = Convert.ToDateTime(dr["MrrDate"]);
                    aMaster.SupplierName = dr["SupplierName"].ToString();

                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.CurrencyName = dr["CurrencyCode"].ToString();
                    aMaster.TotalMrrAmt = DBNull.Value.Equals(dr["TotalAmount"]) ? 0: Convert.ToDecimal(dr["TotalAmount"]);
                    aMaster.MrrNo = dr["MrrNo"].ToString();
                    aMaster.IsApprove = Convert.ToBoolean(dr["IsApprove"]);
                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<TradeMrrDetails> TradeMRRDetailsById(int poMasteId)
        {
            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<TradeMrrDetails> aList = new List<TradeMrrDetails>();
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@poMasteId", poMasteId));

                SqlDataReader dr = _accessManager.GetSqlDataReader("GetTradeMrrDetailsById", aParameters);
                while (dr.Read())
                {
                    TradeMrrDetails aMaster = new TradeMrrDetails();
                    aMaster.TradeMRRDetailID= DBNull.Value.Equals(dr["TradeMRRDetailID"]) ? 0 : Convert.ToInt32(dr["TradeMRRDetailID"]);
                    aMaster.ProductNo = dr["ProductNo"].ToString();
                    aMaster.ProductName = dr["ProductName"].ToString();
                    aMaster.UOMName = dr["UOMName"].ToString();
                    aMaster.ApproveQty = DBNull.Value.Equals(dr["ApproveQty"]) ? 0 : Convert.ToDecimal(dr["ApproveQty"]);
                    aMaster.ApproveKg = DBNull.Value.Equals(dr["ApproveKg"]) ? 0 : Convert.ToDecimal(dr["ApproveKg"]);
                    aMaster.ReceivedQty= DBNull.Value.Equals(dr["ReceivedQty"]) ? 0 : Convert.ToDecimal(dr["ReceivedQty"]);
                    aMaster.ReceivedKG = DBNull.Value.Equals(dr["ReceivedKG"]) ? 0 : Convert.ToDecimal(dr["ReceivedKG"]);
                    aMaster.MRRRemainingQty = DBNull.Value.Equals(dr["MRRRemainingQty"]) ? 0 : Convert.ToDecimal(dr["MRRRemainingQty"]);
                    aMaster.MRRRemainingKG = DBNull.Value.Equals(dr["MRRRemainingKG"]) ? 0 : Convert.ToDecimal(dr["MRRRemainingKG"]);
                    aMaster.MRRQty = DBNull.Value.Equals(dr["MRRQty"]) ? 0 : Convert.ToDecimal(dr["MRRQty"]);
                    aMaster.MRRKG = DBNull.Value.Equals(dr["MRRKG"]) ? 0 : Convert.ToDecimal(dr["MRRKG"]);
                    aMaster.ApproveUnitPrice = DBNull.Value.Equals(dr["ApproveUnitPrice"]) ? 0 : Convert.ToDecimal(dr["ApproveUnitPrice"]);
                    aMaster.TotalPrice = DBNull.Value.Equals(dr["TotalPrice"]) ? 0 : Convert.ToDecimal(dr["TotalPrice"]);
                    aMaster.MfgDate = DBNull.Value.Equals(dr["MfgDate"]) ? DateTime.Now : Convert.ToDateTime(dr["MfgDate"]);// dr["MfgDate"] as DateTime?;
                    aMaster.ExpairyDate = DBNull.Value.Equals(dr["ExpairyDate"]) ? DateTime.Now : Convert.ToDateTime(dr["ExpairyDate"]);// dr["ExpairyDate"] as DateTime?;
                    aMaster.ProductId = Convert.ToInt32(dr["ProductId"]);
                    aMaster.GroupId = Convert.ToInt32(dr["GroupId"]);
                    aMaster.TradePurchaseOrderDetailsId = Convert.ToInt32(dr["TradePurchaseDetailId"]);


                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<ViewTradeMrr> LoadMrrNo()
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);


                SqlDataReader dr = _accessManager.GetSqlDataReader("GetTradeMrrNo");
                List<ViewTradeMrr> ItemDescriptionList = new List<ViewTradeMrr>();
                while (dr.Read())
                {
                    ViewTradeMrr itemDescription = new ViewTradeMrr();
                    itemDescription.TradeMrrMasterId = Convert.ToInt32(dr["TradeMrrMasterId"]);
                    itemDescription.MrrNo = dr["MrrNo"].ToString();
                    ItemDescriptionList.Add(itemDescription);
                }

                return ItemDescriptionList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public TradeMrrMaster GetTradeMrrInfoById(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                TradeMrrMaster aMaster = new TradeMrrMaster();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@Id", id));

                SqlDataReader dr = _accessManager.GetSqlDataReader("GetTradeMrrInfoById", aParameters);
                while (dr.Read())
                {
                    aMaster.TradeMrrMasterId = Convert.ToInt32(dr["TradeMrrMasterId"]);
                    aMaster.MRRDate = Convert.ToDateTime(dr["MRRDate"]);
                    aMaster.ChallanNo = dr["ChallanNo"].ToString();
                    aMaster.ChallanDate =(dr["ChallanDate"] == DBNull.Value) ? (DateTime?)null : ((DateTime)dr["ChallanDate"]);
                    aMaster.VehicleNo = dr["VehicleNo"].ToString();
                    aMaster.VehicleInWeight = DBNull.Value.Equals(dr["VehicleInWeight"])?0: Convert.ToDecimal(dr["VehicleInWeight"]);
                    aMaster.VehicleOutWeight = DBNull.Value.Equals(dr["VehicleOutWeight"]) ? 0 : Convert.ToDecimal(dr["VehicleOutWeight"]);
                    aMaster.LaborCost = DBNull.Value.Equals(dr["LaborCost"]) ? 0 : Convert.ToDecimal(dr["LaborCost"]);
                    aMaster.TransportCost = DBNull.Value.Equals(dr["TransportCost"]) ? 0 : Convert.ToDecimal(dr["TransportCost"]);
                    aMaster.LabResult = dr["Labesult"].ToString();
                    aMaster.SpecialInstruction = dr["SpecialInstruction"].ToString();
                    aMaster.TotalQty = DBNull.Value.Equals(dr["TotalQty"]) ? 0 : Convert.ToDecimal(dr["TotalQty"]);
                    aMaster.TotalKG = DBNull.Value.Equals(dr["TotalKG"]) ? 0 : Convert.ToDecimal(dr["TotalKG"]);
                    aMaster.TotalAmount = DBNull.Value.Equals(dr["TotalAmount"]) ? 0 : Convert.ToDecimal(dr["TotalAmount"]);
                    aMaster.TradePurchaseOrderId = Convert.ToInt32(dr["TradePurchaseMasterId"]);
                    aMaster.SupplierID = Convert.ToInt32(dr["SupplierID"]);
                    aMaster.DelLocationId = Convert.ToInt32(dr["DelLocationId"]);
                    aMaster.DelWareHouseID= Convert.ToInt32(dr["DelWareHouseID"]);
                    aMaster.TradeNo = dr["TradeNo"].ToString();
                    aMaster.PurchaseDate = Convert.ToDateTime(dr["PurchaseDate"]);
                    aMaster.SupplierName = dr["SupplierName"].ToString();
                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.CurrencyName = dr["CurrencyCode"].ToString();
                    aMaster.MrrNo = dr["MrrNo"].ToString();
                    //aMaster.SupplierRef = dr["SupplierRef"].ToString();
                    //aMaster.DeliveryTo = dr["DeliveryTo"].ToString();
                }
                return aMaster;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<ViewTradeMrr> TradeMrrApprovalList(DateTime? fromdate, DateTime? todate, int mrrNO = 0, int supplierId = 0, int productId = 0)
        {
            try
            {
                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-2);

                }
                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<ViewTradeMrr> aList = new List<ViewTradeMrr>();
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@MRRNo", mrrNO));
                aParameters.Add(new SqlParameter("@SupplierId", supplierId));
                aParameters.Add(new SqlParameter("@productId", productId));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetTradeMrrApprovalList", aParameters);
                while (dr.Read())
                {
                    ViewTradeMrr aMaster = new ViewTradeMrr();
                    aMaster.TradeMrrMasterId = Convert.ToInt32(dr["TradeMrrMasterId"]);
                    aMaster.TradePurchaseOrderId = Convert.ToInt32(dr["TradePurchaseMasterId"]);
                    aMaster.TradeNo = dr["TradeNo"].ToString();
                    //aMaster.LastRcvDate = (dr["OrderDate"] == DBNull.Value) ? (DateTime?)null : ((DateTime)dr["OrderDate"]);
                    aMaster.PurchaseDate = (dr["PurchaseDate"] == DBNull.Value) ? (DateTime?)null : ((DateTime)dr["PurchaseDate"]);
                    //  aMaster.OrderDate = Convert.ToDateTime(dr["OrderDate"]);
                    aMaster.MrrDate = Convert.ToDateTime(dr["MrrDate"]);
                    aMaster.SupplierName = dr["SupplierName"].ToString();

                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.CurrencyName = dr["CurrencyCode"].ToString();
                    aMaster.TotalMrrAmt = DBNull.Value.Equals(dr["TotalAmount"]) ? 0 : Convert.ToDecimal(dr["TotalAmount"]);
                    aMaster.MrrNo = dr["MrrNo"].ToString();
                    aMaster.IsApprove = Convert.ToBoolean(dr["IsApprove"]);
                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<ViewTradeMrr> TradeMrrPrint(DateTime? fromdate, DateTime? todate, int mrrNO = 0, int supplierId = 0, int productId = 0)
        {
            try
            {
                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-2);

                }
                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<ViewTradeMrr> aList = new List<ViewTradeMrr>();
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@MRRNo", mrrNO));
                aParameters.Add(new SqlParameter("@SupplierId", supplierId));
                aParameters.Add(new SqlParameter("@productId", productId));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("TradeMrrListForPrint", aParameters);
                while (dr.Read())
                {
                    ViewTradeMrr aMaster = new ViewTradeMrr();
                    aMaster.TradeMrrMasterId = Convert.ToInt32(dr["TradeMrrMasterId"]);
                    aMaster.TradePurchaseOrderId = Convert.ToInt32(dr["TradePurchaseMasterId"]);
                    aMaster.TradeNo = dr["TradeNo"].ToString();
                    //aMaster.LastRcvDate = (dr["OrderDate"] == DBNull.Value) ? (DateTime?)null : ((DateTime)dr["OrderDate"]);
                    aMaster.PurchaseDate = (dr["PurchaseDate"] == DBNull.Value) ? (DateTime?)null : ((DateTime)dr["PurchaseDate"]);
                    //  aMaster.OrderDate = Convert.ToDateTime(dr["OrderDate"]);
                    aMaster.MrrDate = Convert.ToDateTime(dr["MRRDate"]);
                    aMaster.SupplierName = dr["SupplierName"].ToString();

                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.CurrencyName = dr["CurrencyCode"].ToString();
                    aMaster.TotalMrrAmt = DBNull.Value.Equals(dr["TotalAmount"]) ? 0 : Convert.ToDecimal(dr["TotalAmount"]);
                    aMaster.MrrNo = dr["MrrNo"].ToString();
                    aMaster.IsApprove = Convert.ToBoolean(dr["IsApprove"]);
                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable PrintMultipleTradeMRR(string ids)
        {
            try
            {
                DataTable dr = new DataTable();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@Ids", ids));
                dr = _accessManager.GetDataTable("sp_PrintMultipleTradeMRR", parameters);

                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        #region Return
        public List<ViewTradeMrr> ListForReturn(DateTime? fromdate, DateTime? todate, int mrrNO = 0,int supplierId = 0, int productId = 0)
        {
            try
            {
                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-2);

                }

                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<ViewTradeMrr> aList = new List<ViewTradeMrr>();
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@MRRNo", mrrNO));
                aParameters.Add(new SqlParameter("@SupplierId", supplierId));
                aParameters.Add(new SqlParameter("@ProductId", productId));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetTradeMrrListForReturn", aParameters);
                while (dr.Read())
                {
                    ViewTradeMrr aMaster = new ViewTradeMrr();
                    aMaster.TradeMrrMasterId = Convert.ToInt32(dr["TradeMrrMasterId"]);
                    aMaster.TradeNo = dr["TradeNo"].ToString();
                    aMaster.PurchaseDate = (dr["PurchaseDate"] == DBNull.Value)? (DateTime?)null: ((DateTime)dr["PurchaseDate"]);
                    aMaster.MrrDate = Convert.ToDateTime(dr["MRRDate"]);
                    aMaster.TotalAmountRcv = DBNull.Value.Equals(dr["TotalAmount"]) ? 0 : Convert.ToDecimal(dr["TotalAmount"]);
                    aMaster.SupplierName = dr["SupplierName"].ToString();
                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.CurrencyName = dr["CurrencyCode"].ToString();
                    aMaster.TotalQtyRcv = DBNull.Value.Equals(dr["TotalQty"])?0: Convert.ToDecimal(dr["TotalQty"]);
                    aMaster.TotalKGRcv = DBNull.Value.Equals(dr["TotalKG"]) ? 0 : Convert.ToDecimal(dr["TotalKG"]);
                    aMaster.MrrNo = dr["MrrNo"].ToString();
                    aList.Add(aMaster);
                }

                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        //public ViewOthersMrr GetMrrInfoByMasterById(int id)
        //{
        //    try
        //    {
        //        _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
        //        ViewOthersMrr aMaster = new ViewOthersMrr();
        //        List<SqlParameter> aParameters = new List<SqlParameter>();
        //        aParameters.Add(new SqlParameter("@Id", id));

        //        SqlDataReader dr = _accessManager.GetSqlDataReader("GetMrrMasterInfoById", aParameters);
        //        while (dr.Read())
        //        {

        //            aMaster.MRRrOtherID = Convert.ToInt32(dr["MRRrOtherID"]);
        //            aMaster.SupplierID = Convert.ToInt32(dr["SupplierID"]);
        //            aMaster.DelLocationId = Convert.ToInt32(dr["DelLocationId"]);
        //            aMaster.DelWareHouseID = Convert.ToInt32(dr["DelWareHouseID"]);

        //            aMaster.MrrNo = dr["MrrNo"].ToString();
        //            aMaster.MrrDate = Convert.ToDateTime(dr["MRRDate"]);
        //            aMaster.PurchaseOrderNo = dr["PurchaseOrderNo"].ToString();

        //            aMaster.SupplierName = dr["SupplierName"].ToString();
        //            aMaster.LocationName = dr["LocationName"].ToString();
        //            aMaster.WareHouseName = dr["WareHouseName"].ToString();
        //            aMaster.CurrencyName = dr["CurrencyCode"].ToString();

        //            aMaster.SupplierRef = dr["SupplierRef"].ToString();
        //            aMaster.DeliveryTo = dr["DeliveryTo"].ToString();
        //            aMaster.ChallanNo = dr["ChallanNo"].ToString();
        //            aMaster.LabResult = dr["LabResult"].ToString();
        //            aMaster.VehicleNo = dr["VehicleNo"].ToString();
        //            aMaster.VehicleInWeight = dr["VehicleInWeight"].ToString();
        //            aMaster.VehicleOutWeight = dr["VehicleOutWeight"].ToString();
        //            aMaster.SpecialInstruction = dr["SpecialInstruction"].ToString();
        //            aMaster.LaborCost = Convert.ToDecimal(dr["LaborCost"]);
        //            aMaster.TransportCost = Convert.ToDecimal(dr["TransportCost"]);



        //        }

        //        return aMaster;
        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }
        //    finally
        //    {
        //        _accessManager.SqlConnectionClose();
        //    }
        //}

        public ResultResponse SaveTradeMrrReturn(ViewTradeMrrReturnMaster aMaster, string user)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@TradeMRRReturnMasterID", aMaster.TradeMRRReturnMasterID));
                aParameters.Add(new SqlParameter("@TradeMrrMasterId", aMaster.TradeMrrMasterId));
                aParameters.Add(new SqlParameter("@ReturnDate", DateTime.Now.Date));
                aParameters.Add(new SqlParameter("@SupplierID", aMaster.SupplierID));
                aParameters.Add(new SqlParameter("@Remarks", aMaster.Remarks));
                aParameters.Add(new SqlParameter("@DelLocationId", aMaster.DelLocationId));
                aParameters.Add(new SqlParameter("@DelWareHouseID", aMaster.DelWareHouseID));
                aParameters.Add(new SqlParameter("@TotalQty", aMaster.TradeMrrReturnDetails.Sum(n => n.ReturnQty)));
                aParameters.Add(new SqlParameter("@TotalKG", aMaster.TradeMrrReturnDetails.Sum(n => n.ReturnKG)));
                aParameters.Add(new SqlParameter("@TotalAmount", aMaster.TradeMrrReturnDetails.Sum(n => n.TotalPrice)));
                aParameters.Add(new SqlParameter("@CreateBy", user));

                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("SaveTradeMrrReturnMaster", aParameters);
                if (aResponse.pk > 0)
                {
                    if (aMaster.TradeMrrReturnDetails.Count > 0)
                    {
                        aResponse.isSuccess = SaveTradeMrrReturnDetails(aMaster.TradeMrrReturnDetails, aResponse.pk);
                    }

                   aResponse.isSuccess = TradeMrrReturnSystemJournal(aResponse.pk);
                    aResponse.isSuccess = SaveTradeStockOut(aResponse.pk, aMaster.TradeMrrReturnDetails);
                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                }
                else
                {
                    _accessManager.SqlConnectionClose(true);
                }

                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }



        private bool SaveTradeMrrReturnDetails(List<ViewTradeMrrReturnDetails> aMaster, int aResponsePk)
        {
            try
            {
                bool result = false;

                foreach (var item in aMaster)
                {
                        List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                        ResultResponse aResponse = new ResultResponse();

                        aSqlParameterList.Add(new SqlParameter("@TradeMRRReturnMasterID", aResponsePk));
                        aSqlParameterList.Add(new SqlParameter("@ProductId", item.ProductId));
                        aSqlParameterList.Add(new SqlParameter("@MRRQty", item.MRRQty));
                        aSqlParameterList.Add(new SqlParameter("@MRRKG", item.MRRKG));
                        aSqlParameterList.Add(new SqlParameter("@ReturnQty", item.ReturnQty));
                        aSqlParameterList.Add(new SqlParameter("@ReturnKG", item.ReturnKG));
                        aSqlParameterList.Add(new SqlParameter("@UnitPrice", item.UnitPrice));
                        aSqlParameterList.Add(new SqlParameter("@TotalPrice", item.TotalPrice));

                        aResponse.pk =
                            _accessManager.SaveDataReturnPrimaryKey("SaveTradeReturnedMrrDetails", aSqlParameterList);
                }

                return result = true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool TradeMrrReturnSystemJournal(int returnMasterId)
        {

            try
            {

                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@TradeMRRReturnMasterID", returnMasterId));


                result = _accessManager.SaveData("SavTradeReturnMrrJournal", aParameters);

                return result;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public bool SaveTradeStockOut(int aResponsePk, List<ViewTradeMrrReturnDetails> aMaster)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                ResultResponse aResponse = new ResultResponse();

                aSqlParameterList.Add(new SqlParameter("@TradeMRRReturnMasterID", aResponsePk));
                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("SaveTradeReturnedMrrStockIn", aSqlParameterList);

                foreach (var item in aMaster)
                {

                    if (aResponse.pk > 0)
                    {
                        List<SqlParameter> aSqlParameterList1 = new List<SqlParameter>();

                        aSqlParameterList1.Add(new SqlParameter("@TradeMRRReturnMasterID", aResponsePk));
                        aSqlParameterList1.Add(new SqlParameter("@StockInMasterId", aResponse.pk));
                        aSqlParameterList1.Add(new SqlParameter("@ProductId", item.ProductId));
                        aSqlParameterList1.Add(new SqlParameter("@ReturnQty", item.ReturnQty));
                        aSqlParameterList1.Add(new SqlParameter("@ReturnKG", item.ReturnKG));
                        result = _accessManager.SaveData("SaveTradeReturnedMrrStockInDetails", aSqlParameterList1);
                    }


                }
                return result = true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<TradeMrrDetails> MrrDetailsforReturnById(int id)
        {
            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<TradeMrrDetails> aList = new List<TradeMrrDetails>();
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@Id", id));

                SqlDataReader dr = _accessManager.GetSqlDataReader("GetTradeMrrDetailsforReturnById", aParameters);
                while (dr.Read())
                {
                    TradeMrrDetails aMaster = new TradeMrrDetails();
                    aMaster.ProductId = Convert.ToInt32(dr["ProductId"]);
                    aMaster.ProductNo = dr["ProductNo"].ToString();
                    aMaster.ProductName = dr["ProductName"].ToString();
                    aMaster.UOMName = dr["UOMName"].ToString();
                    aMaster.MRRQty = Convert.ToDecimal(dr["MRRQty"]);
                    aMaster.MRRKG = Convert.ToDecimal(dr["MRRKG"]);
                    aMaster.ReturnQty = Convert.ToDecimal(dr["ReturnQty"]);
                    aMaster.ReturnKG = Convert.ToDecimal(dr["ReturnKG"]);
                    aMaster.MRRRemainingQty = Convert.ToDecimal(dr["RemainingQuanity"]);
                    aMaster.MRRRemainingKG = Convert.ToDecimal(dr["RemainingKG"]);
                    aMaster.UnitPrice = Convert.ToDecimal(dr["UnitPrice"]);
                    aMaster.TotalPrice = Convert.ToDecimal(dr["RestTotalPrice"]);
                    aMaster.MfgDate = Convert.ToDateTime(dr["MfgDate"]) as DateTime ?;
                    aMaster.ExpairyDate = Convert.ToDateTime(dr["ExpairyDate"]) as DateTime?;
                    aMaster.GroupId=  Convert.ToInt32(dr["GroupId"]);
                    aList.Add(aMaster);
                }

                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<ViewTradeMrrReturnMaster> ReturnedMrrList(DateTime? fromdate, DateTime? todate, int supplierId = 0,int productId = 0)
        {
            try
            {
                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-2);

                }

                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<ViewTradeMrrReturnMaster> aList = new List<ViewTradeMrrReturnMaster>();
                List<SqlParameter> aParameters = new List<SqlParameter>();


                aParameters.Add(new SqlParameter("@SupplierId", supplierId));
                aParameters.Add(new SqlParameter("@productId", productId));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetTradeReturnedMrrList", aParameters);
                while (dr.Read())
                {
                    ViewTradeMrrReturnMaster aMaster = new ViewTradeMrrReturnMaster();
                    aMaster.TradeMrrMasterId = Convert.ToInt32(dr["TradeMrrMasterId"]);
                    aMaster.TradeMRRReturnMasterID = Convert.ToInt32(dr["TradeMRRReturnMasterID"]);
                    aMaster.TradeReturnNo = dr["TradeReturnNo"].ToString();
                    aMaster.TradeReturnDate = Convert.ToDateTime(dr["TradeReturnDate"]);
                    aMaster.SupplierName = dr["SupplierName"].ToString();
                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.Remarks = dr["Remarks"].ToString();
                    aMaster.TotalQty =DBNull.Value.Equals(dr["TotalQty"])?0: Convert.ToDecimal(dr["TotalQty"]);
                    aMaster.TotalKG = DBNull.Value.Equals(dr["TotalKG"]) ? 0 : Convert.ToDecimal(dr["TotalKG"]);
                    aMaster.TotalAmount = DBNull.Value.Equals(dr["TotalAmount"]) ? 0 : Convert.ToDecimal(dr["TotalAmount"]);
                    aMaster.MrrNo = dr["MrrNo"].ToString();
                    aList.Add(aMaster);
                }

                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<ViewTradeMrrReturnDetails> TradeMrrReturnDetailsById(int id)
        {
            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<ViewTradeMrrReturnDetails> aList = new List<ViewTradeMrrReturnDetails>();
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@Id", id));

                SqlDataReader dr = _accessManager.GetSqlDataReader("GetTradeMrrReturnDetailsById", aParameters);
                while (dr.Read())
                {
                    ViewTradeMrrReturnDetails aMaster = new ViewTradeMrrReturnDetails();
                    aMaster.ProductName = dr["ProductName"].ToString();
                    aMaster.UOMName = dr["UOMName"].ToString();
                    aMaster.ReturnQty = Convert.ToDecimal(dr["ReturnQty"]);
                    aMaster.ReturnKG = Convert.ToDecimal(dr["ReturnKG"]);
                    aMaster.MRRQty = Convert.ToDecimal(dr["MRRQty"]);
                    aMaster.MRRKG = Convert.ToDecimal(dr["MRRKG"]);
                    aMaster.UnitPrice = Convert.ToDecimal(dr["UnitPrice"]);
                    aMaster.TotalPrice = Convert.ToDecimal(dr["TotalPrice"]);
                    aMaster.ProductId = Convert.ToInt32(dr["ProductId"]);
                    aList.Add(aMaster);
                }

                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        #endregion End Return

        public List<ViewTradeMrrReturnMaster> ReturnPrintList(DateTime? fromdate, DateTime? todate, int warehouse,int supplierId)
        {
            try
            {
                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-2);

                }

                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<ViewTradeMrrReturnMaster> aList = new List<ViewTradeMrrReturnMaster>();
                List<SqlParameter> aParameters = new List<SqlParameter>();


                aParameters.Add(new SqlParameter("@Warehouse", warehouse));
                aParameters.Add(new SqlParameter("@SupplierId", supplierId));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetTradeReturnedMrrListForPrint", aParameters);
                while (dr.Read())
                {
                    ViewTradeMrrReturnMaster aMaster = new ViewTradeMrrReturnMaster();
                    aMaster.TradeMrrMasterId = Convert.ToInt32(dr["TradeMrrMasterId"]);
                    aMaster.TradeMRRReturnMasterID = Convert.ToInt32(dr["TradeMRRReturnMasterID"]);
                    aMaster.TradeReturnNo = dr["TradeReturnNo"].ToString();
                    aMaster.TradeReturnDate = Convert.ToDateTime(dr["TradeReturnDate"]);
                    aMaster.SupplierName = dr["SupplierName"].ToString();
                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.Remarks = dr["Remarks"].ToString();
                    aMaster.TotalQty = DBNull.Value.Equals(dr["TotalQty"]) ? 0 : Convert.ToDecimal(dr["TotalQty"]);
                    aMaster.TotalKG = DBNull.Value.Equals(dr["TotalKG"]) ? 0 : Convert.ToDecimal(dr["TotalKG"]);
                    aMaster.TotalAmount = DBNull.Value.Equals(dr["TotalAmount"]) ? 0 : Convert.ToDecimal(dr["TotalAmount"]);
                    aMaster.MrrNo = dr["MrrNo"].ToString();
                    aList.Add(aMaster);
                }

                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public DataTable GetReturnReport(int supplier, int warehouse, DateTime fromdate, DateTime todate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();

                parameters.Add(new SqlParameter("@SupplierId", supplier));
                parameters.Add(new SqlParameter("@fromDate", fromdate));
                parameters.Add(new SqlParameter("@toDate", todate));
                parameters.Add(new SqlParameter("@warehouse", warehouse));


                DataTable dr = _accessManager.GetDataTable("TradeMrrReturnListRpt", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }






    }
}