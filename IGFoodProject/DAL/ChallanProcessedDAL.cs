﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Configuration;
using System.Web;
using CrystalDecisions.Shared.Json;
using IGFoodProject.DataManager;
using IGFoodProject.Models;
using IGFoodProject.ViewModel;
using Microsoft.Ajax.Utilities;

namespace IGFoodProject.DAL
{
    public class ChallanProcessedDAL
    {

        DataAccessManager _accessManager = new DataAccessManager();

        public List<ViewSalesOrderProcessedMaster> GetSalesOrderProcessedForChallan(string userEmpId)
        {
            try

            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ViewSalesOrderProcessedMaster> aList = new List<ViewSalesOrderProcessedMaster>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@userEmpId", userEmpId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_SalesOrderProcesedListForClallan", aParameters);
                while (dr.Read())
                {
                    ViewSalesOrderProcessedMaster aMaster = new ViewSalesOrderProcessedMaster();

                    aMaster.SalesOrderProcessedMasterId = Convert.ToInt32(dr["SalesOrderProcessedMasterId"]);
                    aMaster.PickingMasterId = Convert.ToInt32(dr["PickingMasterId"]);
                    aMaster.TotatalOrderAmtKg = Convert.ToDecimal(dr["TotatalOrderAmtKg"]);
                    aMaster.TotalIncTaxVat = Convert.ToDecimal(dr["TotalIncTaxVat"]);
                    aMaster.TotalExTaxVat = Convert.ToDecimal(dr["TotalExTaxVat"]);

                    aMaster.SalesPerson = dr["SalesPerson"].ToString();
                    aMaster.SalesOrderNo = dr["SalesOrderNo"].ToString();
                    aMaster.PickingNo = dr["PickingNo"].ToString();
                    aMaster.CustomerName = dr["CustomerName"].ToString();
                    aMaster.DeliveryDestination = dr["DeliveryDestination"].ToString();
                    aMaster.SalesPersonContact = dr["SalesPersonContact"].ToString();

                    aMaster.CreateBy = dr["CreateBy"].ToString();
                    aMaster.ApproveBy = dr["ApproveBy"].ToString();
                    aMaster.ApproveDate = dr["ApproveDate"] as DateTime?;
                    aMaster.CreateDate = dr["CreateDate"] as DateTime?;
                    aMaster.OrderDate = dr["OrderDate"] as DateTime?;
                    aMaster.DeliveryDate = dr["DeliveryDate"] as DateTime?;
                    if (DBNull.Value == dr["IsApprove"])
                    {
                        aMaster.IsApprove = 0;
                    }
                    else
                    {
                        aMaster.IsApprove = Convert.ToInt32(dr["IsApprove"]);
                    }

                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public ResultResponse SaveSalesOrderProcessedChallan(SalesOrderChallanMaster aMaster, string user)
        {
            try
            {

                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CompanyId", aMaster.CompanyId));
                aParameters.Add(new SqlParameter("@SalesOrderId", aMaster.SalesOrderId));
                aParameters.Add(new SqlParameter("@DriverName", aMaster.DriverName));
                aParameters.Add(new SqlParameter("@DriverContact", aMaster.DriverContact));
                aParameters.Add(new SqlParameter("@VehicleId", aMaster.VehicleId));
                aParameters.Add(new SqlParameter("@PickingMasterId", aMaster.PickingMasterId));
                aParameters.Add(new SqlParameter("@WarehouseId", aMaster.WarehouseId));
                aParameters.Add(new SqlParameter("@ChallanAmount", aMaster.ChallanAmount));
                aParameters.Add(new SqlParameter("@TotalDeliveryQty", aMaster.TotalDeliveryQty));
                aParameters.Add(new SqlParameter("@TotalDeliveryKG", aMaster.TotalDeliveryKG));
                aParameters.Add(new SqlParameter("@ChallanDate", aMaster.ChallanDate));
                aParameters.Add(new SqlParameter("@CreateDate", System.DateTime.Now));
                aParameters.Add(new SqlParameter("@CreateBy", user));
                aParameters.Add(new SqlParameter("@Remarks", aMaster.Remarks));
                aParameters.Add(new SqlParameter("@TotalDiscountAmount", aMaster.TotalDiscountAmount));

                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_Save_ChallanMasterSalesOrderProcessed", aParameters);


                if (aResponse.pk > 0)
                {
                    aResponse.isSuccess = SaveChallanDetailsProcessed(aMaster.ChallanDetails, aResponse.pk);
                    aResponse.isSuccess = true;

                    if (aResponse.isSuccess == true)
                    {
                        aResponse.isSuccess = UpdateChallanMaster(aMaster.SalesOrderId);
                        aResponse.isSuccess = SaveChallanJournal(aResponse.pk);
                    }
                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                }

                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public bool SaveChallanDetails(List<SalesOrderChallanDetails> challanList , int pk )
        {
            try
            {
                bool result = false;
                //For Faurter Process
                foreach (var item in challanList)
                {
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@SalesOrderDetailsId", item.SalesOrderDetailsId));
                    aParameters.Add(new SqlParameter("@SalesOrderChallanMasterId", pk));
                    aParameters.Add(new SqlParameter("@IssueKg", item.IssueKg));
                    aParameters.Add(new SqlParameter("@IssuePrice", item.IssuePrice));
                    aParameters.Add(new SqlParameter("@IssueQty", item.IssueQty));
                    aParameters.Add(new SqlParameter("@WarehouseId", item.WarehouseId));
                    aParameters.Add(new SqlParameter("@ProductId", item.ProductId));
                    result = _accessManager.SaveData("sp_Calculate_Challan_Processed", aParameters);

                    if (result == false)
                    {
                        break;
                    }

                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                
            }
        }
        public string GetSOCheck(int so, int pick)
        {
            string isdo = string.Empty;
            try
            {


                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> SqlList = new List<SqlParameter>();
                

                SqlList.Add(new SqlParameter("@pickingid", pick));
                SqlList.Add(new SqlParameter("@salesorderid", so));



                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_CheckDuplicateChallan", SqlList);

                while (dr.Read())
                {

                    isdo = dr["ccount"].ToString();


                }

                return isdo;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }


        }


        public DateTime GetChallanDate(int challanno)
        {
            DateTime isdo = new DateTime();
            try
            {


                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> SqlList = new List<SqlParameter>();


                SqlList.Add(new SqlParameter("@ChallanId", challanno));
               
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetChallanDate", SqlList);

                while (dr.Read())
                {

                    isdo =Convert.ToDateTime(dr["ChallanDate"]) ;


                }

                return isdo;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }


        }

        //public ViewProductDetailsSales GetProductDetailsForChallan(int id,  int wareHouseId = 0)
        //{
        //    try
        //    {
        //        ViewProductDetailsSales aDetails = new ViewProductDetailsSales();
        //        List<SqlParameter> aParameters = new List<SqlParameter>();
        //        aParameters.Add(new SqlParameter("@id", id));
        //        aParameters.Add(new SqlParameter("@wareHouseId", wareHouseId));

        //        _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
        //        SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_ProductDetailsInChallan2", aParameters);
        //        while (dr.Read())
        //        {
        //            aDetails.ProductID = Convert.ToInt32(dr["ProductID"]);
        //            aDetails.StockQty = DBNull.Value.Equals(dr["StockQty"]) ? 0 : Convert.ToDecimal(dr["StockQty"]);

        //            aDetails.StockKg = DBNull.Value.Equals(dr["StockKg"]) ? 0 : Convert.ToDecimal(dr["StockKg"]);
        //            aDetails.groupid = Convert.ToInt32(dr["GroupId"]);


        //        }
        //        return aDetails;
        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }

        //    finally
        //    {
        //        _accessManager.SqlConnectionClose();
        //    }
        //}

        public ViewProductDetailsSales GetProductDetailsForChallan(int id,  int wareHouseId  , int customerType, DateTime orderDate, DateTime challanDate)
        {
            try
            {
                
                ViewProductDetailsSales aDetails = new ViewProductDetailsSales();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                aParameters.Add(new SqlParameter("@wareHouseId", wareHouseId));
                aParameters.Add(new SqlParameter("@orderdate", orderDate));
                aParameters.Add(new SqlParameter("@challanDate", challanDate));
                aParameters.Add(new SqlParameter("@customertypeid", customerType));
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_ProductStockDetailsProcessed_ForChallan", aParameters);
                while (dr.Read())
                {
                    aDetails.ProductID = Convert.ToInt32(dr["ProductID"]);
                    aDetails.StockQty = DBNull.Value.Equals(dr["StockQty"]) ? 0 : Convert.ToDecimal(dr["StockQty"]);
                    aDetails.StockPrice = DBNull.Value.Equals(dr["StockPrice"]) ? 0 : Convert.ToDecimal(dr["StockPrice"]);
                    aDetails.StockKg = DBNull.Value.Equals(dr["StockKg"]) ? 0 : Convert.ToDecimal(dr["StockKg"]);
                    aDetails.groupid = Convert.ToInt32(dr["GroupId"]);

                    aDetails.BasePrice = DBNull.Value.Equals(dr["BasePrice"]) ? 0 : Convert.ToDecimal(dr["BasePrice"]);


                }
                return aDetails;
            }
            catch (Exception ex)
            {

                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public bool SaveChallanDetailsProcessed(List<SalesOrderChallanDetails> challanList, int pk)
        {
            try
            {
                bool result = false;
                //For Processed
                foreach (var item in challanList)
                {
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@SalesOrderDetailsId", item.SalesOrderDetailsId));
                    aParameters.Add(new SqlParameter("@SalesOrderChallanMasterId", pk));
                    aParameters.Add(new SqlParameter("@IssueKg", item.IssueKg));
                    aParameters.Add(new SqlParameter("@IssuePrice", item.IssuePrice));
                    aParameters.Add(new SqlParameter("@IssueQty", item.IssueQty));
                    aParameters.Add(new SqlParameter("@WarehouseId", item.WarehouseId));
                    aParameters.Add(new SqlParameter("@ProductId", item.ProductId));
                    aParameters.Add(new SqlParameter("@ConfirmFullPick", item.ConfirmFullPick));
                    aParameters.Add(new SqlParameter("@aFlag", item.aFlag));

                    if (item.GroupId == 5)
                    {
                        result = _accessManager.SaveData("sp_Calculate_Challan_Processed_KG", aParameters);
                    }
                    if (item.GroupId == 6)
                    {
                        result = _accessManager.SaveData("sp_Calculate_Challan_Processed", aParameters);
                    }
                    if (item.GroupId == 7)
                    {
                        result = _accessManager.SaveData("sp_Calculate_Challan_Processed_ByProduct", aParameters);
                    }



                    if (result == false)
                    {
                        break;
                    }

                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {

            }
        }
        
        public bool UpdateChallanMaster(int SalesOrderId)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@SalesOrderId" , SalesOrderId));
                result = _accessManager.SaveData("sp_update_SalesOrderMaster", aParameters);

                return result;


            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        //public bool SaveChallanJournal(int ChallanId)
        //{
        //    try
        //    {
        //        bool result = false;
        //        List<SqlParameter> aParameters = new List<SqlParameter>();
        //        aParameters.Add(new SqlParameter("@ChallanMasterId", ChallanId));
        //        result = _accessManager.SaveData("saveAutoSalesChallanJournal", aParameters);

        //        return result;


        //    }
        //    catch (Exception ex)
        //    {
                
        //        throw ex;
        //    }
        //}

        public ViewPreviousIssue GetPreviousIssue(int orderId, int productId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add( new SqlParameter("@orderId", orderId ));
                aParameters.Add( new SqlParameter("@productId", productId));
                ViewPreviousIssue aIssue = new ViewPreviousIssue();
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_PreviousChallanQty", aParameters);
                while (dr.Read())
                {
                    aIssue.PrevIssueKg = Convert.ToDecimal(dr["PrevIssueKg"]);
                    aIssue.PrevIssueQty = Convert.ToDecimal(dr["PrevIssueQty"]);
                    aIssue.ProductId = Convert.ToInt32(dr["ProductId"]);
                }
                return aIssue;
            }
            catch (Exception ex)
            {

                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<ViewGeneratedChallan> GetGeneratedChallan(int id=0)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add( new SqlParameter("@id" , id));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_GeneratedChallan", aParameters);

                List<ViewGeneratedChallan> aList = new List<ViewGeneratedChallan>();
                while (dr.Read())
                {
                    ViewGeneratedChallan aChallan = new ViewGeneratedChallan();
                    aChallan.SalesOrderChallanMasterId = Convert.ToInt32(dr["SalesOrderChallanMasterId"]);
                    aChallan.SalesOrderChallanNo = dr["SalesOrderChallanNo"].ToString();
                    aChallan.DeliveryDestination = dr["DeliveryDestination"].ToString();
                    aChallan.SalesOrderNo = dr["SalesOrderNo"].ToString();
                    aChallan.OrderBy = dr["OrderBy"].ToString();
                    aChallan.GroupName = dr["GroupName"].ToString();
                    aChallan.CreateBy = dr["CreateBy"].ToString();
                    aChallan.ChallanAmount = Convert.ToDecimal(dr["ChallanAmount"].ToString());
                    aChallan.CreateDate = dr["CreateDate"] as DateTime?;
                    aChallan.OrderDate = dr["OrderDate"] as DateTime?;
                    aChallan.GroupId =DBNull.Value.Equals(dr["GroupId"]) ?0: Convert.ToInt32(dr["GroupId"]);
                    aChallan.CustomerName = dr["CustomerName"].ToString();
                    aList.Add(aChallan);
                }
                return aList;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public List<ViewGeneratedChallanDetails> GetGeneratedChallanDetails(int id)
        {
            try
            {
                List<ViewGeneratedChallanDetails> aList = new List<ViewGeneratedChallanDetails>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                DataTable dt = _accessManager.GetDataTable("sp_Get_Challan_DetailsByMaster", aParameters);
                aList = Helper.ToListof<ViewGeneratedChallanDetails>(dt);
                return aList;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<ViewSalesOrderProcessedMaster> GetChallanMasterMaster()
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<ViewSalesOrderProcessedMaster> aList = new List<ViewSalesOrderProcessedMaster>();

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_SalesOrderProcessedList");
                while (dr.Read())
                {
                    ViewSalesOrderProcessedMaster aMaster = new ViewSalesOrderProcessedMaster();

                    aMaster.SalesOrderProcessedMasterId = Convert.ToInt32(dr["SalesOrderProcessedMasterId"]);
                    aMaster.TotatalOrderAmtKg = Convert.ToDecimal(dr["TotatalOrderAmtKg"]);
                    aMaster.TotalIncTaxVat = Convert.ToDecimal(dr["TotalIncTaxVat"]);
                    aMaster.TotalExTaxVat = Convert.ToDecimal(dr["TotalExTaxVat"]);
                    aMaster.TotalDue = Convert.ToDecimal(dr["TotalDue"]);
                    aMaster.TotalPaid = Convert.ToDecimal(dr["TotalPaid"]);

                    aMaster.SalesPerson = dr["SalesPerson"].ToString();
                    aMaster.SalesOrderNo = dr["SalesOrderNo"].ToString();
                    aMaster.CustomerName = dr["CustomerName"].ToString();
                    aMaster.DeliveryDestination = dr["DeliveryDestination"].ToString();
                    aMaster.SalesPersonContact = dr["SalesPersonContact"].ToString();

                    aMaster.CreateBy = dr["CreateBy"].ToString();
                    aMaster.ApproveBy = dr["ApproveBy"].ToString();
                    aMaster.ApproveDate = dr["ApproveDate"] as DateTime?;
                    aMaster.CreateDate = dr["CreateDate"] as DateTime?;
                    aMaster.OrderDate = dr["OrderDate"] as DateTime?;
                    aMaster.DeliveryDate = dr["DeliveryDate"] as DateTime?;
                    if (DBNull.Value == dr["IsApprove"])
                    {
                        aMaster.IsApprove = 0;
                    }
                    else
                    {
                        aMaster.IsApprove = Convert.ToInt32(dr["IsApprove"]);
                    }
                    if (DBNull.Value == dr["IsEmployee"])
                    {
                        aMaster.IsEmployee = null;
                    }
                    else
                    {
                        aMaster.IsEmployee = (bool)dr["IsEmployee"];
                    }
                    aMaster.CustomerType = dr["CoustomerTypeName"].ToString();
                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<ViewGeneratedChallan> GetGeneratedChallanList(DateTime fromDate, DateTime toDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@challanDateFrom", fromDate));
                aParameters.Add(new SqlParameter("@challanDateTo", toDate));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_ChallanList", aParameters);

                List<ViewGeneratedChallan> aList = new List<ViewGeneratedChallan>();
                while (dr.Read())
                {
                    ViewGeneratedChallan aChallan = new ViewGeneratedChallan();
                    aChallan.SalesOrderChallanMasterId = Convert.ToInt32(dr["SalesOrderChallanMasterId"]);
                    aChallan.SalesOrderChallanNo = dr["SalesOrderChallanNo"].ToString();
                    aChallan.DeliveryDestination = dr["DeliveryDestination"].ToString();
                    aChallan.SalesOrderNo = dr["SalesOrderNo"].ToString();
                    aChallan.OrderBy = dr["OrderBy"].ToString();
                    aChallan.CreateBy = dr["CreateBy"].ToString();
                    aChallan.ChallanAmount = Convert.ToDecimal(dr["ChallanAmount"].ToString());
                    aChallan.CreateDate = dr["CreateDate"] as DateTime?;
                    aChallan.OrderDate = dr["OrderDate"] as DateTime?;
                    aChallan.GroupId = Convert.ToInt32(dr["GroupId"]);
                    aChallan.CustomerName = dr["CustomerName"].ToString();
                    aList.Add(aChallan);
                }
                return aList;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public List<ViewGeneratedChallan> GetGeneratedChallanPrintList(DateTime fromDate, DateTime toDate, int? customerType, int? customer, int? location, int? warehouse)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@challanDateFrom", fromDate));
                aParameters.Add(new SqlParameter("@challanDateTo", toDate));
                aParameters.Add(new SqlParameter("@customerType", customerType));
                aParameters.Add(new SqlParameter("@customer", customer));
                aParameters.Add(new SqlParameter("@location", location));
                aParameters.Add(new SqlParameter("@warehouse", warehouse));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_GeneratedChallan_filter", aParameters);

                List<ViewGeneratedChallan> aList = new List<ViewGeneratedChallan>();
                while (dr.Read())
                {
                    ViewGeneratedChallan aChallan = new ViewGeneratedChallan();
                    aChallan.SalesOrderChallanMasterId = Convert.ToInt32(dr["SalesOrderChallanMasterId"]);
                    aChallan.SalesOrderChallanNo = dr["SalesOrderChallanNo"].ToString();
                    aChallan.DeliveryDestination = dr["DeliveryDestination"].ToString();
                    aChallan.SalesOrderNo = dr["SalesOrderNo"].ToString();
                    aChallan.OrderBy = dr["OrderBy"].ToString();
                    aChallan.CreateBy = dr["CreateBy"].ToString();
                    aChallan.ChallanAmount = Convert.ToDecimal(dr["ChallanAmount"].ToString());
                    aChallan.ChallanDate = dr["ChallanDate"] as DateTime?;
                    aChallan.OrderDate = dr["OrderDate"] as DateTime?;
                    
                    aChallan.CustomerName = dr["CustomerName"].ToString();
                    aList.Add(aChallan);
                }
                return aList;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public DataTable GetGeneratedChallanDetailsForPrint(string id, bool isTrade= false)
        {
            try
            {
                DataTable dt= new DataTable();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                if (isTrade)
                {
                    dt = _accessManager.GetDataTable("sp_TradeSalesChallanPrint", aParameters);
                }
                else
                {

                     dt = _accessManager.GetDataTable("sp_GetGeneratedChallanDetailsForPrint", aParameters);
                }
               
                return dt;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        #region
        public List<ChallanListForDelivery> GetChallanListForDeliveryConfirm()
        {
            try

            {
              
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@groupid", 5));
                List<ChallanListForDelivery> aList = new List<ChallanListForDelivery>();
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_ChallanProcesedListForDelivery", aParameters);
                while (dr.Read())
                {
                    ChallanListForDelivery aMaster = new ChallanListForDelivery();

                    aMaster.SalesOrderChallanMasterId = Convert.ToInt32(dr["SalesOrderChallanMasterId"]);
                    aMaster.SalesOrderChallanNo = dr["SalesOrderChallanNo"].ToString();
                    aMaster.SalesOrderNo = dr["SalesOrderNo"].ToString();
                    if(dr["TotalChallanKG"] !=DBNull.Value)
                    {
                        aMaster.TotalChallanKG = Convert.ToDecimal(dr["TotalChallanKG"]);
                    }
                    
                    if(dr["TotalChallanQty"] !=DBNull.Value)
                    {
                        aMaster.TotalChallanQty = Convert.ToDecimal(dr["TotalChallanQty"]);
                    }
                    if (dr["ChallanAmount"] != DBNull.Value)
                    {
                        aMaster.ChallanAmount = Convert.ToDecimal(dr["ChallanAmount"]);
                    }

                    
                    
                    aMaster.CustomerName = dr["CustomerName"].ToString();
                    aMaster.ChallanDate = dr["ChallanDate"].ToString();
                    aMaster.OrderDate = dr["OrderDate"].ToString();

                    

                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public ViewChallanDetailForDelivery GetChallanProcessedMaster(int id)
        {
            try
            {
                List<SqlParameter> aParameters = new List<SqlParameter>();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                aParameters.Add(new SqlParameter("@id", id));
                DataTable dt = _accessManager.GetDataTable("sp_GetChallanProcessedMasterById", aParameters);
                List<ViewChallanDetailForDelivery> aList = Helper.ToListof<ViewChallanDetailForDelivery>(dt);
                return aList[0];
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<SalesOrderChallanDetails> GetChallanDetailsByMaster(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                DataTable dt = _accessManager.GetDataTable("sp_Get_ChallanPrecessedDetailsByMaster", aParameters);
                List<SalesOrderChallanDetails> aList = Helper.ToListof<SalesOrderChallanDetails>(dt);
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<SalesOrderChallanDetails> GetChallanDetailsByMaster_New(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                DataTable dt = _accessManager.GetDataTable("sp_Get_ChallanPrecessedDetailsByMaster_New", aParameters);
                List<SalesOrderChallanDetails> aList = Helper.ToListof<SalesOrderChallanDetails>(dt);
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse SaveChallanDelivery(SalesOrderChallanMaster aMaster, string user)
        {
            try
            {
                bool result = false;

                ResultResponse aResponse = new ResultResponse();

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@SalesOrderChallanMasterId", aMaster.SalesOrderChallanMasterId));

                aParameters.Add(new SqlParameter("@DeliveryAmount", aMaster.DeliveryAmount));
                aParameters.Add(new SqlParameter("@TotalDeliveryQty", aMaster.TotalDeliveryQty));
                aParameters.Add(new SqlParameter("@TotalDeliveryKG", aMaster.TotalDeliveryKG));
                aParameters.Add(new SqlParameter("@DeliveryDate", aMaster.DeliveryDate));
                aParameters.Add(new SqlParameter("@Remarks", aMaster.Remarks));
                aParameters.Add(new SqlParameter("@DeliveryBy", aMaster.DeliveryBy));

                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_Save_DeliveryConfirm", aParameters);


                if (aResponse.pk > 0)
                {
                    aResponse.isSuccess = SaveDeliveryDetailsProcessed(aMaster.ChallanDetails, aResponse.pk);
                   bool rt = SaveChallanReturnJournal(aResponse.pk);
                    result = SaveAdvancedAdjust(aResponse.pk);
                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                }

                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }



        public ResultResponse SaveChallanDelivery_New(SalesOrderChallanMaster aMaster, string user)
        {
            try
            {
                bool result = false;

                ResultResponse aResponse = new ResultResponse();

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@SalesOrderChallanMasterId", aMaster.SalesOrderChallanMasterId));

                aParameters.Add(new SqlParameter("@DeliveryAmount", aMaster.DeliveryAmount));
                aParameters.Add(new SqlParameter("@TotalDeliveryQty", aMaster.TotalDeliveryQty));
                aParameters.Add(new SqlParameter("@TotalDeliveryKG", aMaster.TotalDeliveryKG));
                aParameters.Add(new SqlParameter("@DeliveryDate", aMaster.DeliveryDate));
                aParameters.Add(new SqlParameter("@Remarks", aMaster.Remarks));
                aParameters.Add(new SqlParameter("@DeliveryBy", aMaster.DeliveryBy)); 
                aParameters.Add(new SqlParameter("@TotalDiscountAmount_delivery", aMaster.TotalDiscountAmount_delivery));
                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_Save_DeliveryConfirm", aParameters);


                if (aResponse.pk > 0)
                {
                    aResponse.isSuccess = SaveDeliveryDetailsProcessed_New(aMaster.ChallanDetails, aResponse.pk);
                    bool rt= SaveChallanReturnJournal(aResponse.pk);
                    bool rt2 = SaveChallanWeightGainJournal(aResponse.pk);
                    result = SaveAdvancedAdjust(aResponse.pk);
                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                }

                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public bool SaveChallanReturnJournal(int ChallanId)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ChallanMasterId", ChallanId));
                result = _accessManager.SaveData("saveChallanReturnJournal", aParameters);

                return result;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool SaveChallanWeightGainJournal(int ChallanId)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ChallanMasterId", ChallanId));
                result = _accessManager.SaveData("saveChallanJournalForWeightGain", aParameters);

                return result;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public bool SaveChallanReturnJournalFullReturn(int ChallanId,int challanSecondDetailsId)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ChallanMasterId", ChallanId));
                aParameters.Add(new SqlParameter("@ChallanSenconddetailsId", challanSecondDetailsId));
                result = _accessManager.SaveData("saveChallanReturnJournalFull", aParameters);

                return result;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public bool SaveChallanJournal(int ChallanId)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ChallanMasterId", ChallanId));
                result = _accessManager.SaveData("saveChallanJournal", aParameters);

                return result;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public bool SaveDeliveryDetailsProcessed(List<SalesOrderChallanDetails> challanList, int pk)
        {
            try
            {
                bool result = false;
                //For Processed
                ResultResponse aResponse = new ResultResponse();
                foreach (var item in challanList)
                {  /////////// new added by shahadat 11-10-21
              
                    /////////// new added by shahadat 11-10-21
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@SalesOrderChallanDetailsId", item.SalesOrderChallanDetailsId));
                   
                    aParameters.Add(new SqlParameter("@DeliveryKG", item.DeliveryKG));
                    aParameters.Add(new SqlParameter("@DeliveryAmount", item.DeliveryAmount));
                    aParameters.Add(new SqlParameter("@DeliveryQty", item.DeliveryQty));
                    aParameters.Add(new SqlParameter("@DeliveryRemarks", item.DeliveryRemarks));
                    aParameters.Add(new SqlParameter("@SalePriceDelivery", item.SaleRate));

                    aParameters.Add(new SqlParameter("@ReturnQty", item.ReturnQty));
                    aParameters.Add(new SqlParameter("@ReturnKG", item.ReturnKG));
                    aParameters.Add(new SqlParameter("@WeightLossQty", item.WeightLossQty));
                    aParameters.Add(new SqlParameter("@WeightLossKG", item.WeightLossKG));

                    /////////// new added by shahadat 11-10-21
                    //aParameters.Add(new SqlParameter("@ChallanSecondDetailsId", item.ChallanSecondDetailsId));
                    
                    //aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_SaveDeliveryDetails", aParameters);
                    //if (aResponse.pk > 0)
                    //{
                    //    bool t = SaveChallanReturnJournalFullReturn(pk, aResponse.pk);
                    //}

                    ///////// new added by shahadat 11-10-21 end 
                   
                     result = _accessManager.SaveData("sp_SaveDeliveryDetails", aParameters);


                    if (result == false)
                    {
                        break;
                    }
                    /////

                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {

            }
        }

        public bool SaveDeliveryDetailsProcessed_New(List<SalesOrderChallanDetails> challanList, int pk)
        {
            try
            {
                bool result = false;
              //  ResultResponse aResponse = new ResultResponse();
                //For Processed
                foreach (var item in challanList)
                {
                    /////////// new added by shahadat 11-10-21
                   
                    /////////// new added by shahadat 11-10-21

                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@SalesOrderChallanSecondDetailsId", item.SalesOrderChallanDetailsId));

                    aParameters.Add(new SqlParameter("@DeliveryKG", item.DeliveryKG));
                    aParameters.Add(new SqlParameter("@DeliveryAmount", item.DeliveryAmount));
                    aParameters.Add(new SqlParameter("@DeliveryQty", item.DeliveryQty));
                    aParameters.Add(new SqlParameter("@DeliveryRemarks", item.DeliveryRemarks));
                    aParameters.Add(new SqlParameter("@SalePriceDelivery", item.SaleRate));

                    aParameters.Add(new SqlParameter("@ReturnQty", item.ReturnQty));
                    aParameters.Add(new SqlParameter("@ReturnKG", item.ReturnKG));
                    aParameters.Add(new SqlParameter("@WeightLossQty", item.WeightLossQty));
                    aParameters.Add(new SqlParameter("@WeightLossKG", item.WeightLossKG));
                    aParameters.Add(new SqlParameter("@WeightGainKG", item.WeightGainKG));

                    /////////// new added by shahadat 11-10-21
                    //  aParameters.Add(new SqlParameter("@ChallanSecondDetailsId", item.ChallanSecondDetailsId));

                    ///  aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_SaveDeliveryDetails_New", aParameters);

                    //  if (aResponse.pk > 0)
                    //  {
                    //     bool t = SaveChallanReturnJournalFullReturn(pk, aResponse.pk);
                    //  }


                    ///////// new added by shahadat 11-10-21 end 

                    result = _accessManager.SaveData("sp_SaveDeliveryDetails_New", aParameters);

                    if (result == false)
                    {
                        break;
                    }

                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {

            }
        }

        public bool SaveAdvancedAdjust(int pk)
        {
            try
            {
                bool result = false;
                //For Processed
                
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@SalesOrderChallanMasterId", pk));
                
                    result = _accessManager.SaveData("sp_SaveAdvancedAdjustAmount", aParameters);


              
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {

            }
        }

        #endregion

        public DataTable ChallanWiseDeliverySummary(int customerId, DateTime fromDate, DateTime toDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CustomerId", customerId));
                aParameters.Add(new SqlParameter("@FromDate", fromDate));
                aParameters.Add(new SqlParameter("@ToDate", toDate));
                DataTable dt = _accessManager.GetDataTable("sp_challanWiseDeliverySummary", aParameters);

                return dt;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable CustomerWiseDeliveryConfirmationSummary(int customerId, DateTime fromDate, DateTime toDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CustomerId", customerId));
                aParameters.Add(new SqlParameter("@FromDate", fromDate));
                aParameters.Add(new SqlParameter("@ToDate", toDate));
                DataTable dt = _accessManager.GetDataTable("sp_CustomerWiseDeliveryConfirmationSummary", aParameters);

                return dt;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable DeliveryListForRoleBack( DateTime fromDate, DateTime toDate, int flag)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@fromDate", fromDate));
                aParameters.Add(new SqlParameter("@toDate", toDate));
                aParameters.Add(new SqlParameter("@bit", flag));
                DataTable dt = _accessManager.GetDataTable("sp_GetAllDeliveryList", aParameters);

                return dt;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public bool UpdateChallanMasterRoleBack(string SalesOrderId, int type,string user)
        {
            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                String[] strlist = SalesOrderId.Split(',');
                bool result = false;
                strlist = strlist.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                foreach (var s in strlist)
                {

                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@ChallanId", Convert.ToInt32(s)));
                    aParameters.Add(new SqlParameter("@ChallanType", type));
                    aParameters.Add(new SqlParameter("@UserId", user));
                    result = _accessManager.SaveData("ChallanRoleBack", aParameters);

                }


                return result;


            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {

                _accessManager.SqlConnectionClose();
            }
        }
        public DataTable GetChallanSummery(int id, int type)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@Id", id));
                aParameters.Add(new SqlParameter("@Type", type));
                DataTable dt = _accessManager.GetDataTable("sp_GetDeliveryConfirm", aParameters);

                return dt;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<ViewGeneratedChallan> GetConfirmedChallan(int id = 0)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_ConfirmedChallan", aParameters);

                List<ViewGeneratedChallan> aList = new List<ViewGeneratedChallan>();
                while (dr.Read())
                {
                    ViewGeneratedChallan aChallan = new ViewGeneratedChallan();
                    aChallan.SalesOrderChallanMasterId = Convert.ToInt32(dr["SalesOrderChallanMasterId"]);
                    aChallan.SalesOrderChallanNo = dr["SalesOrderChallanNo"].ToString();
                    aChallan.DeliveryDestination = dr["DeliveryDestination"].ToString();
                    aChallan.SalesOrderNo = dr["SalesOrderNo"].ToString();
                    aChallan.OrderBy = dr["OrderBy"].ToString();
                    aChallan.GroupName = dr["GroupName"].ToString();
                    aChallan.CreateBy = dr["CreateBy"].ToString();
                    aChallan.ChallanAmount = Convert.ToDecimal(dr["ChallanAmount"].ToString());
                    aChallan.CreateDate = dr["CreateDate"] as DateTime?;
                    aChallan.OrderDate = dr["OrderDate"] as DateTime?;
                    aChallan.GroupId = DBNull.Value.Equals(dr["GroupId"]) ? 0 : Convert.ToInt32(dr["GroupId"]);
                    aChallan.CustomerName = dr["CustomerName"].ToString();
                    aChallan.DeliveryBy = dr["DeliveryBy"].ToString();
                    aChallan.DeliveryDate = dr["DeliveryDate"] as DateTime?;
                    aChallan.DeliveryAmount = Convert.ToDecimal(dr["DeliveryAmount"].ToString());
                    aList.Add(aChallan);
                }
                return aList;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<ViewGeneratedChallanDetails> GetConfirmedChallanDetails(int id)
        {
            try
            {
                List<ViewGeneratedChallanDetails> aList = new List<ViewGeneratedChallanDetails>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                DataTable dt = _accessManager.GetDataTable("sp_Get_ConfirmedChallan_DetailsByMaster", aParameters);
                aList = Helper.ToListof<ViewGeneratedChallanDetails>(dt);
                return aList;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public DataTable GetVehicleWiseChallan(DateTime fromDate, DateTime toDate, int? vehicleId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@fromDate", fromDate));
                aParameters.Add(new SqlParameter("@toDate", toDate));
                aParameters.Add(new SqlParameter("@vehicleId", vehicleId));

                DataTable dt = _accessManager.GetDataTable("sp_GetVehicleWiseChallanAmount", aParameters, true);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        //public DataTable GetVehicleWiseVatChallan(DateTime fromDate, DateTime toDate, int? vehicleId)
        //{
        //    try
        //    {
        //        _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
        //        List<SqlParameter> aParameters = new List<SqlParameter>();
        //        aParameters.Add(new SqlParameter("@fromDate", fromDate));
        //        aParameters.Add(new SqlParameter("@toDate", toDate));
        //        aParameters.Add(new SqlParameter("@vehicleId", vehicleId));

        //        DataTable dt = _accessManager.GetDataTable("sp_GetVatChallan", aParameters, true);
        //        return dt;
        //    }
        //    catch (Exception e)
        //    {

        //        throw e;
        //    }
        //    finally
        //    {
        //        _accessManager.SqlConnectionClose();
        //    }
        //}
        public List<ViewVatChallan> GetVehicleWiseVatChallan(DateTime fromDate, DateTime toDate, int? vehicleId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@fromDate", fromDate));
                aParameters.Add(new SqlParameter("@toDate", toDate));
                aParameters.Add(new SqlParameter("@vehicleId", vehicleId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetVatChallan", aParameters);

                List<ViewVatChallan> aList = new List<ViewVatChallan>();
                while (dr.Read())
                {
                    ViewVatChallan aChallan = new ViewVatChallan();
                    aChallan.ProductName = dr["ProductName"].ToString();
                    aChallan.UnitPrice = Convert.ToDecimal(dr["UnitPrice"].ToString());
                    aChallan.Qty = Convert.ToDecimal(dr["Qty"].ToString());
                    aChallan.KG = Convert.ToDecimal(dr["KG"].ToString());
                    aChallan.TotalAmount = Convert.ToDecimal(dr["TotalAmount"].ToString());
                    aChallan.VatAmount = Convert.ToDecimal(dr["VatAmount"].ToString());
                    aChallan.TotalAmountWithVat = Convert.ToDecimal(dr["TotalAmountWithVat"].ToString());
                    aList.Add(aChallan);
                }
                return aList;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<ViewVatChallan> GetVehicleWisePickingSummary(DateTime fromDate, DateTime toDate, int? vehicleId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@fromDate", fromDate));
                aParameters.Add(new SqlParameter("@toDate", toDate));
                aParameters.Add(new SqlParameter("@vehicleId", vehicleId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_VehicleWisePickingSummary", aParameters);

                List<ViewVatChallan> aList = new List<ViewVatChallan>();
                while (dr.Read())
                {
                    ViewVatChallan aChallan = new ViewVatChallan();
                    aChallan.GroupName = dr["GroupName"].ToString();
                    aChallan.ProductName = dr["ProductName"].ToString();
                    aChallan.Qty = Convert.ToDecimal(dr["Qty"].ToString());
                    aChallan.KG = Convert.ToDecimal(dr["KG"].ToString());
                    aList.Add(aChallan);
                }
                return aList;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<SalesOrderChallanMaster> LoadChallanNoByCustomer(int customerId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CustomerId", customerId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetChallanNoByCustomerId", aParameters);
                List<SalesOrderChallanMaster> challanNoList = new List<SalesOrderChallanMaster>();
                while (dr.Read())
                {
                    SalesOrderChallanMaster challanNo = new SalesOrderChallanMaster();
                    challanNo.SalesOrderChallanMasterId = Convert.ToInt32(dr["SalesOrderChallanMasterId"]);
                    challanNo.SalesOrderChallanNo = dr["SalesOrderChallanNo"].ToString();
                    challanNoList.Add(challanNo);
                }
                return challanNoList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public DataTable GetChallanDetailsById(int customerId, int challanMasterId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CustomerId", customerId));
                aParameters.Add(new SqlParameter("@challanMasterId", challanMasterId));
                DataTable dt = _accessManager.GetDataTable("sp_GetChallanDetailsById", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse SavePrecessedReturn(List<ViewGeneratedChallanDetails> challanDetails, ViewGeneratedChallanDetails ObjData, string userName)
        {
            try
            {
                bool result = false;
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                ResultResponse aResponse = new ResultResponse();

                List<SqlParameter> aObjData = new List<SqlParameter>();
                //aObjData.Add(new SqlParameter("@SalesReturnId", ObjData.SalesReturnId));
                aObjData.Add(new SqlParameter("@SalesOrderChallanMasterId", ObjData.SalesOrderChallanMasterId));
                aObjData.Add(new SqlParameter("@ReturnDate", ObjData.ReturnDate));
                aObjData.Add(new SqlParameter("@UserName", userName));
                aObjData.Add(new SqlParameter("@WarehouseId", ObjData.WarehouseId));
                aObjData.Add(new SqlParameter("@Remarks", ObjData.Note));
               // aObjData.Add(new SqlParameter("@UploadFileName", ObjData.UploadFileName));

               // result = _accessManager.SaveData("sp_SaveSalesReturnMaster", aObjData);
                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_SaveSalesReturnMaster", aObjData);

                if (aResponse.pk == 0)
                {
                    aResponse.isSuccess = false;
                    aResponse.pk = 0;
                    _accessManager.SqlConnectionClose(true);
                }
                else
                {
                    foreach (var item in challanDetails)
                    {
                        List<SqlParameter> aParameters = new List<SqlParameter>();
                        aParameters.Add(new SqlParameter("@SalesReturnMasterId", aResponse.pk));
                        aParameters.Add(new SqlParameter("@SalesOrderChallanSecondDetailsId", item.SalesOrderChallanSecondDetailsId));
                        aParameters.Add(new SqlParameter("@ProductId", item.ProductId));
                        aParameters.Add(new SqlParameter("@ReturnQty", item.ReturnQty));
                        aParameters.Add(new SqlParameter("@ReturnKG", item.ReturnKG));
                        aParameters.Add(new SqlParameter("@ReturnAmount", item.ReturnAmount));
                        aParameters.Add(new SqlParameter("@Remarks", item.Remarks));

                        result = _accessManager.SaveData("sp_SaveSalesReturnDetails", aParameters);
                        

                        if (result == false)
                        {
                            break;
                        }
                        //aResponse.pk = item.SalesOrderChallanMasterId;
                    }

                    aResponse.isSuccess = true;
                    bool rt = SaveChallanJournalReversed(aResponse.pk);
                  bool t = AdvanceSalesCollectionVoucherReversed(aResponse.pk);
                }


               
               

                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse UpdateFilePath(string path, string PK)
        {
            try
            {
                bool result = false;
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                ResultResponse aResponse = new ResultResponse();

                List<SqlParameter> aObjData = new List<SqlParameter>();
                //aObjData.Add(new SqlParameter("@SalesReturnId", ObjData.SalesReturnId));
                aObjData.Add(new SqlParameter("@path", path));
                aObjData.Add(new SqlParameter("@PK", PK));
               
                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_UpdateFilePath", aObjData);

                if (aResponse.pk == 0)
                {
                    aResponse.isSuccess = false;
                    aResponse.pk = 0;
                    _accessManager.SqlConnectionClose(true);
                }
                else
                {
                    aResponse.isSuccess = true;
                }

                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public bool SaveChallanJournalReversed(int ChallanId)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ChallanMasterId", ChallanId));
                result = _accessManager.SaveData("saveChallanReturnJournalReversed", aParameters);

                return result;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool AdvanceSalesCollectionVoucherReversed(int ChallanId)
        {

            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ChallanMasterId", ChallanId));

                result = _accessManager.SaveData("SaveSalesCollectionVoucherReversedadvance", aParameters);

                return result;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}