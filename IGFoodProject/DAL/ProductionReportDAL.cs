﻿using IGFoodProject.DataManager;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IGFoodProject.DAL
{
    public class ProductionReportDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();
        public DataTable DailyBroilerPurchaseSummary(DateTime productionDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ProductionDate", productionDate));
                DataTable dt = _accessManager.GetDataTable("sp_GetDailyBroilerPurchaseSummary", aParameters, true);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
                _accessManager.SqlConnectionClose();
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

        public DataTable DailyBroilerInputDressBirdSummary(DateTime productionDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ProductionDate", productionDate));
                DataTable dt = _accessManager.GetDataTable("sp_GetDailyBroilerInputAndDressBirdSummary", aParameters, true);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
                _accessManager.SqlConnectionClose();
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public DataTable DailyBroilerInputDressBirdByProductProductionPercentage(DateTime productionDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ProductionDate", productionDate));
                DataTable dt = _accessManager.GetDataTable("sp_GetDailyBroilerInputDressBirdByProductProductionPercentage", aParameters, true);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
                _accessManager.SqlConnectionClose();
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public DataTable DailyPortionProduction(DateTime productionDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ProductionDate", productionDate));
                DataTable dt = _accessManager.GetDataTable("sp_GetDailyPortionProduction", aParameters, true);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
                _accessManager.SqlConnectionClose();
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public DataTable DailyPortionProductionLossPercentage(DateTime productionDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ProductionDate", productionDate));
                DataTable dt = _accessManager.GetDataTable("sp_GetDailyPortionProductionLossPercentage", aParameters, true);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
                _accessManager.SqlConnectionClose();
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public DataTable DailyNormalPortion(DateTime productionDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ProductionDate", productionDate));
                DataTable dt = _accessManager.GetDataTable("sp_GetDailyNormalPortionDetails", aParameters, true);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
                _accessManager.SqlConnectionClose();
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public DataTable DailyDressingPointByProductProductionPercentage(DateTime productionDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ProductionDate", productionDate));
                DataTable dt = _accessManager.GetDataTable("sp_GetDailyDressingPointByProductProductionPercentage", aParameters, true);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
                _accessManager.SqlConnectionClose();
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

        //Monthly
        public DataTable MonthlyBroilerPurchaseSummary(string productionDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ProductionDate", productionDate));
                DataTable dt = _accessManager.GetDataTable("sp_GetMonthlyBroilerPurchaseSummary", aParameters, true);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
                _accessManager.SqlConnectionClose();
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

        public DataTable MonthlyBroilerInputDressBirdSummary(string productionDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ProductionDate", productionDate));
                DataTable dt = _accessManager.GetDataTable("sp_GetMonthlyBroilerInputAndDressBirdSummary", aParameters, true);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
                _accessManager.SqlConnectionClose();
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public DataTable MonthlyBroilerInputDressBirdByProductProductionPercentage(string productionDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ProductionDate", productionDate));
                DataTable dt = _accessManager.GetDataTable("sp_GetMonthlyBroilerInputDressBirdByProductProductionPercentage", aParameters, true);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
                _accessManager.SqlConnectionClose();
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public DataTable MonthlyPortionProduction(string productionDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ProductionDate", productionDate));
                DataTable dt = _accessManager.GetDataTable("sp_GetMonthlyPortionProduction", aParameters, true);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
                _accessManager.SqlConnectionClose();
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public DataTable MonthlyPortionProductionLossPercentage(string productionDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ProductionDate", productionDate));
                DataTable dt = _accessManager.GetDataTable("sp_GetMonthlyPortionProductionLossPercentage", aParameters, true);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
                _accessManager.SqlConnectionClose();
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public DataTable MonthlyNormalPortion(string productionDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ProductionDate", productionDate));
                DataTable dt = _accessManager.GetDataTable("sp_GetMonthlyNormalPortionDetails", aParameters, true);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
                _accessManager.SqlConnectionClose();
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public DataTable MonthlyDressingPointByProductProductionPercentage(string productionDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ProductionDate", productionDate));
                DataTable dt = _accessManager.GetDataTable("sp_GetMonthlyDressingPointByProductProductionPercentage", aParameters, true);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
                _accessManager.SqlConnectionClose();
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

    }
}