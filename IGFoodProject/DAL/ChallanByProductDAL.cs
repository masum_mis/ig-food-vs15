﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using IGFoodProject.DataManager;
using IGFoodProject.Models;
using IGFoodProject.ViewModel;

namespace IGFoodProject.DAL
{
    public class ChallanByProductDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();
        public List<ViewSalesOrderProcessedMaster> GetSalesOrderProcessedForChallan()
        {
            try

            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ViewSalesOrderProcessedMaster> aList = new List<ViewSalesOrderProcessedMaster>();
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_SalesOrderByProductListForClallan");
                while (dr.Read())
                {
                    ViewSalesOrderProcessedMaster aMaster = new ViewSalesOrderProcessedMaster();

                    aMaster.SalesOrderProcessedMasterId = Convert.ToInt32(dr["SalesOrderProcessedMasterId"]);
                    aMaster.TotatalOrderAmtKg = Convert.ToDecimal(dr["TotatalOrderAmtKg"]);
                    aMaster.TotalIncTaxVat = Convert.ToDecimal(dr["TotalIncTaxVat"]);
                    aMaster.TotalExTaxVat = Convert.ToDecimal(dr["TotalExTaxVat"]);

                    aMaster.SalesPerson = dr["SalesPerson"].ToString();
                    aMaster.SalesOrderNo = dr["SalesOrderNo"].ToString();
                    aMaster.CustomerName = dr["CustomerName"].ToString();
                    aMaster.DeliveryDestination = dr["DeliveryDestination"].ToString();
                    aMaster.SalesPersonContact = dr["SalesPersonContact"].ToString();

                    aMaster.CreateBy = dr["CreateBy"].ToString();
                    aMaster.ApproveBy = dr["ApproveBy"].ToString();
                    aMaster.ApproveDate = dr["ApproveDate"] as DateTime?;
                    aMaster.CreateDate = dr["CreateDate"] as DateTime?;
                    aMaster.OrderDate = dr["OrderDate"] as DateTime?;
                    aMaster.DeliveryDate = dr["DeliveryDate"] as DateTime?;
                    if (DBNull.Value == dr["IsApprove"])
                    {
                        aMaster.IsApprove = 0;
                    }
                    else
                    {
                        aMaster.IsApprove = Convert.ToInt32(dr["IsApprove"]);
                    }

                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public ResultResponse SaveSalesOrderProcessedChallan(SalesOrderChallanMaster aMaster, string user)
        {
            try
            {

                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CompanyId", aMaster.CompanyId));
                aParameters.Add(new SqlParameter("@SalesOrderId", aMaster.SalesOrderId));
                aParameters.Add(new SqlParameter("@TotalDeliveryKG", aMaster.TotalDeliveryKG));
                aParameters.Add(new SqlParameter("@ChallanDate", aMaster.ChallanDate));
                aParameters.Add(new SqlParameter("@ChallanAmount", aMaster.ChallanAmount));
                aParameters.Add(new SqlParameter("@CreateDate", System.DateTime.Now));
                aParameters.Add(new SqlParameter("@CreateBy", user));

                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_Save_ChallanMasterSalesOrderProcessed",
                    aParameters);


                if (aResponse.pk > 0)
                {
                    aResponse.isSuccess = SaveChallanDetails(aMaster.ChallanDetails, aResponse.pk);

                    if (aResponse.isSuccess == true)
                    {
                        aResponse.isSuccess = UpdateChallanMaster(aMaster.SalesOrderId);
                    }
                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                }

                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public bool SaveChallanDetails(List<SalesOrderChallanDetails> challanList, int pk)
        {
            try
            {
                bool result = false;

                foreach (var item in challanList)
                {
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@SalesOrderDetailsId", item.SalesOrderDetailsId));
                    aParameters.Add(new SqlParameter("@SalesOrderChallanMasterId", pk));
                    aParameters.Add(new SqlParameter("@IssueKg", item.IssueKg));
                    aParameters.Add(new SqlParameter("@IssuePrice", item.IssuePrice));
                    aParameters.Add(new SqlParameter("@WarehouseId", item.WarehouseId));
                    aParameters.Add(new SqlParameter("@ProductId", item.ProductId));
                    result = _accessManager.SaveData("sp_Calculate_Challan_Processed_ByProduct", aParameters);

                    if (result == false)
                    {
                        break;
                    }

                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {

            }
        }

        public bool UpdateChallanMaster(int SalesOrderId)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@SalesOrderId", SalesOrderId));
                result = _accessManager.SaveData("sp_update_SalesOrderMasterByProduct", aParameters);

                return result;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public ViewPreviousIssue GetPreviousIssue(int orderId, int productId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@orderId", orderId));
                aParameters.Add(new SqlParameter("@productId", productId));
                ViewPreviousIssue aIssue = new ViewPreviousIssue();
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_PreviousChallanQty", aParameters);
                while (dr.Read())
                {
                    aIssue.PrevIssueKg = Convert.ToDecimal(dr["PrevIssueKg"]);
                    aIssue.PrevIssueQty = Convert.ToDecimal(dr["PrevIssueQty"]);
                    aIssue.ProductId = Convert.ToInt32(dr["ProductId"]);
                }
                return aIssue;
            }
            catch (Exception ex)
            {

                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
    }
}