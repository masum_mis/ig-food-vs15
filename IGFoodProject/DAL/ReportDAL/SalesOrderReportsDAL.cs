﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using IGFoodProject.DataManager;
using IGFoodProject.Models;
using IGFoodProject.Models.ReportModel;
using IGFoodProject.ViewModel;

namespace IGFoodProject.DAL.ReportDAL
{
    public class SalesOrderReportsDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();
        public dynamic GetDBPortionReqNO(DateTime fromDate, DateTime todate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@FromDate", fromDate));
                parameters.Add(new SqlParameter("@Todate", todate));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetAllOrderNo", parameters);
                List<SalesOrder> aList = new List<SalesOrder>();
                while (dr.Read())
                {
                    SalesOrder aMaster = new SalesOrder();
                    aMaster.SalesOrderProcessedMasterId = Convert.ToInt32(dr["SalesOrderProcessedMasterId"]);
                    aMaster.SalesOrderNo = dr["SalesOrderNo"].ToString();
                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetOrderDetailsForReport(string orderId, bool isTrade=false)
        {
            try
            {
                DataTable dr= new DataTable();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@OrderId", orderId));
                if (isTrade)
                {
                    dr = _accessManager.GetDataTable("sp_RptTradeSalesInvoice", parameters);
                }
                else
                {
                    dr = _accessManager.GetDataTable("sp_RptSalesOrderForProcess", parameters);
                }
                 
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetAllOrderSummaryForReport(DateTime fromDate,DateTime toDate, int groupId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@FromDate", fromDate));
                parameters.Add(new SqlParameter("@ToDate", toDate));
                parameters.Add(new SqlParameter("@ProductGroupId", groupId));
                DataTable dr = _accessManager.GetDataTable("sp_RPTDailySalesOrder", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetAllChallanSummaryForReport(DateTime fromDate, DateTime toDate, int groupId, int customerid,bool IsCommerce)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@FromDate", fromDate));
                parameters.Add(new SqlParameter("@ToDate", toDate));
                parameters.Add(new SqlParameter("@ProductGroupId", groupId));
                parameters.Add(new SqlParameter("@CustomerId", customerid));
                parameters.Add(new SqlParameter("@isEcommerce", IsCommerce));
                DataTable dr = _accessManager.GetDataTable("sp_RPTDailyChallanSummary", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public DataTable GetDeliveryReport_ChallanPrint(int SalesOrderChallanMasterId)  
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@SalesOrderChallanMasterId", SalesOrderChallanMasterId));
                DataTable dr = _accessManager.GetDataTable("sp_DeliveryReport_ChallanPrint", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public DataTable GetTradeChallanInvoice(DateTime fromDate, DateTime toDate, int groupId, int customerid, bool IsCommerce)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@FromDate", fromDate));
                parameters.Add(new SqlParameter("@ToDate", toDate));
                parameters.Add(new SqlParameter("@ProductGroupId", groupId));
                parameters.Add(new SqlParameter("@CustomerId", customerid));
                parameters.Add(new SqlParameter("@isEcommerce", IsCommerce));
                DataTable dr = _accessManager.GetDataTable("sp_DailyTradeChallanSummary", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetTradeDeliveryInvoice(DateTime fromDate, DateTime toDate, int groupId, int customerid, bool IsCommerce)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@FromDate", fromDate));
                parameters.Add(new SqlParameter("@ToDate", toDate));
                parameters.Add(new SqlParameter("@ProductGroupId", groupId));
                parameters.Add(new SqlParameter("@CustomerId", customerid));
                parameters.Add(new SqlParameter("@isEcommerce", IsCommerce));
                DataTable dr = _accessManager.GetDataTable("sp_DailyTradeDeliverySummary", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetAllDeliverySummaryForReport(DateTime fromDate, DateTime toDate, int groupId, int customerid,bool isEcommerce)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@FromDate", fromDate));
                parameters.Add(new SqlParameter("@ToDate", toDate));
                parameters.Add(new SqlParameter("@ProductGroupId", groupId));
                parameters.Add(new SqlParameter("@CustomerId", customerid));
                parameters.Add(new SqlParameter("@isEcommerce", isEcommerce));
                DataTable dr = _accessManager.GetDataTable("sp_RPTDailyDeliverySummary", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetOwnConsupmtionChallanPrint(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@Id", id));
               
                DataTable dr = _accessManager.GetDataTable("PrintOwnConsumptionChallanRpt", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }



        public DataTable GetAllPaymentCollectionReport(DateTime fromDate,DateTime toDate,int groupId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@FromDate", fromDate));
                parameters.Add(new SqlParameter("@ToDate", toDate));
                parameters.Add(new SqlParameter("@GroupId", groupId));
                DataTable dr = _accessManager.GetDataTable("sp_RptSalesCollectionDateToDateCustomerWise", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetAllPaymentCollectionReport(int customerId, DateTime fromDate, DateTime toDate, int groupId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@CustomerId", customerId));
                parameters.Add(new SqlParameter("@FromDate", fromDate));
                parameters.Add(new SqlParameter("@ToDate", toDate));
                parameters.Add(new SqlParameter("@GroupId", groupId));
                DataTable dr = _accessManager.GetDataTable("sp_RptSalesCollectionDateToDateCustomerWise", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetProductStock(int wareHouseId, DateTime stockDate,int groupId , int ProductId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@WareHouseId", wareHouseId));
                parameters.Add(new SqlParameter("@fromDate", stockDate));
                parameters.Add(new SqlParameter("@GroupId", groupId));
                parameters.Add(new SqlParameter("@ProductId", ProductId));
                DataTable dr = _accessManager.GetDataTable("sp_RptStockLedgerDateToDate", parameters,true);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetTradeProductStock(int wareHouseId, DateTime stockDate, int groupId, int ProductId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@WareHouseId", wareHouseId));
                parameters.Add(new SqlParameter("@fromDate", stockDate));
                parameters.Add(new SqlParameter("@GroupId", groupId));
                parameters.Add(new SqlParameter("@ProductId", ProductId));
                DataTable dr = _accessManager.GetDataTable("sp_RptTradeStockLedgerDateToDate", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public DataTable GetProductStockMonthWise(int wareHouseId, string stockDate,int groupId , int ProductId)
        {
            try
            {
                DataTable dr= new DataTable();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@WareHouseId", wareHouseId));
                parameters.Add(new SqlParameter("@fromDate", stockDate));
                parameters.Add(new SqlParameter("@GroupId", groupId));
                parameters.Add(new SqlParameter("@ProductId", ProductId));
                dr = _accessManager.GetDataTable("sp_RptStockLedgerMonthWise", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetTradeProductStockMonthWise(int wareHouseId, string stockDate, int groupId, int ProductId)
        {
            try
            {
                DataTable dr = new DataTable();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@WareHouseId", wareHouseId));
                parameters.Add(new SqlParameter("@fromDate", stockDate));
                parameters.Add(new SqlParameter("@GroupId", groupId));
                parameters.Add(new SqlParameter("@ProductId", ProductId));
                dr = _accessManager.GetDataTable("sp_RptTradeStockLedgerMonthWise", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public DataTable GetOrderDetailsForReport_False(string orderId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@OrderId", orderId));
                DataTable dr = _accessManager.GetDataTable("sp_RptSalesOrderForProcess_False", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public DataTable GetCustomerPayment(int PaymentId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@PaymentMasterId", PaymentId));
                DataTable dr = _accessManager.GetDataTable("sp_GetPaymentCollectionDetails", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        } public DataTable GetMoneyReceipt(string paymentId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@collectionId", paymentId));
                DataTable dr = _accessManager.GetDataTable("sp_GetCustomerMoneyReceipt", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        //public DataTable GetAllChallanSummaryForReport(DateTime fromDate, DateTime toDate, int groupId)
        //{
        //    try
        //    {
        //        _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
        //        List<SqlParameter> parameters = new List<SqlParameter>();
        //        parameters.Add(new SqlParameter("@FromDate", fromDate));
        //        parameters.Add(new SqlParameter("@ToDate", toDate));
        //        parameters.Add(new SqlParameter("@ProductGroupId", groupId));
        //        DataTable dr = _accessManager.GetDataTable("sp_RPTDailyChallanSummary", parameters);
        //        return dr;
        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }
        //    finally
        //    {
        //        _accessManager.SqlConnectionClose();
        //    }
        //}

        public List<ViewYearlySalesReport> YearlySalesReport(int yearId,int productType)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ViewYearlySalesReport> aList = new List<ViewYearlySalesReport>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@Year", yearId));
                aParameters.Add(new SqlParameter("@productType", productType));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetYearlySalseRptData", aParameters);
                while (dr.Read())
                {
                    ViewYearlySalesReport aMaster = new ViewYearlySalesReport();
                    aMaster.ProductName = dr["ProductName"].ToString();
                    aMaster.UOMName = dr["UOMName"].ToString();
                    aMaster.ProductId = Convert.ToInt32(dr["ProductId"]);
                    aMaster.RN = Convert.ToInt32(dr["RN"]);

                    aMaster.TotalSales1 = (dr["TotalSales1"] == DBNull.Value) ? 0 : ((decimal)dr["TotalSales1"]);
                    aMaster.TotalSales2 = (dr["TotalSales2"] == DBNull.Value) ? 0 : ((decimal)dr["TotalSales2"]);
                    aMaster.TotalSales3 = (dr["TotalSales3"] == DBNull.Value) ? 0 : ((decimal)dr["TotalSales3"]);
                    aMaster.TotalSales4 = (dr["TotalSales4"] == DBNull.Value) ? 0 : ((decimal)dr["TotalSales4"]);
                    aMaster.TotalSales5 = (dr["TotalSales5"] == DBNull.Value) ? 0 : ((decimal)dr["TotalSales5"]);
                    aMaster.TotalSales6 = (dr["TotalSales6"] == DBNull.Value) ? 0 : ((decimal)dr["TotalSales6"]);
                    aMaster.TotalSales7 = (dr["TotalSales7"] == DBNull.Value) ? 0 : ((decimal)dr["TotalSales7"]);
                    aMaster.TotalSales8 = (dr["TotalSales8"] == DBNull.Value) ? 0 : ((decimal)dr["TotalSales8"]);
                    aMaster.TotalSales9 = (dr["TotalSales9"] == DBNull.Value) ? 0 : ((decimal)dr["TotalSales9"]);
                    aMaster.TotalSales10 = (dr["TotalSales10"] == DBNull.Value) ? 0 : ((decimal)dr["TotalSales10"]);
                    aMaster.TotalSales11 = (dr["TotalSales11"] == DBNull.Value) ? 0 : ((decimal)dr["TotalSales11"]);
                    aMaster.TotalSales12 = (dr["TotalSales12"] == DBNull.Value) ? 0 : ((decimal)dr["TotalSales12"]);
                    aMaster.TotalSalesT = (dr["TotalSalesT"] == DBNull.Value) ? 0 : ((decimal)dr["TotalSalesT"]);



                    aMaster.IssueQty1 = (dr["IssueQty1"] == DBNull.Value) ? 0 : ((decimal)dr["IssueQty1"]);
                    aMaster.IssueQty2 = (dr["IssueQty2"] == DBNull.Value) ? 0 : ((decimal)dr["IssueQty2"]);
                    aMaster.IssueQty3 = (dr["IssueQty3"] == DBNull.Value) ? 0 : ((decimal)dr["IssueQty3"]);
                    aMaster.IssueQty4 = (dr["IssueQty4"] == DBNull.Value) ? 0 : ((decimal)dr["IssueQty4"]);
                    aMaster.IssueQty5 = (dr["IssueQty5"] == DBNull.Value) ? 0 : ((decimal)dr["IssueQty5"]);
                    aMaster.IssueQty6 = (dr["IssueQty6"] == DBNull.Value) ? 0 : ((decimal)dr["IssueQty6"]);
                    aMaster.IssueQty7 = (dr["IssueQty7"] == DBNull.Value) ? 0 : ((decimal)dr["IssueQty7"]);
                    aMaster.IssueQty8 = (dr["IssueQty8"] == DBNull.Value) ? 0 : ((decimal)dr["IssueQty8"]);
                    aMaster.IssueQty9 = (dr["IssueQty9"] == DBNull.Value) ? 0 : ((decimal)dr["IssueQty9"]);
                    aMaster.IssueQty10 = (dr["IssueQty10"] == DBNull.Value) ? 0 : ((decimal)dr["IssueQty10"]);
                    aMaster.IssueQty11 = (dr["IssueQty11"] == DBNull.Value) ? 0 : ((decimal)dr["IssueQty11"]);
                    aMaster.IssueQty12 = (dr["IssueQty12"] == DBNull.Value) ? 0 : ((decimal)dr["IssueQty12"]);
                    aMaster.IssueQtyT = (dr["IssueQtyT"] == DBNull.Value) ? 0 : ((decimal)dr["IssueQtyT"]);




                    aMaster.rating_average1 = (dr["rating_average1"] == DBNull.Value) ? 0 : ((decimal)dr["rating_average1"]);
                    aMaster.rating_average2 = (dr["rating_average2"] == DBNull.Value) ? 0 : ((decimal)dr["rating_average2"]);
                    aMaster.rating_average3 = (dr["rating_average3"] == DBNull.Value) ? 0 : ((decimal)dr["rating_average3"]);
                    aMaster.rating_average4 = (dr["rating_average4"] == DBNull.Value) ? 0 : ((decimal)dr["rating_average4"]);
                    aMaster.rating_average5 = (dr["rating_average5"] == DBNull.Value) ? 0 : ((decimal)dr["rating_average5"]);
                    aMaster.rating_average6 = (dr["rating_average6"] == DBNull.Value) ? 0 : ((decimal)dr["rating_average6"]);
                    aMaster.rating_average7 = (dr["rating_average7"] == DBNull.Value) ? 0 : ((decimal)dr["rating_average7"]);
                    aMaster.rating_average8 = (dr["rating_average8"] == DBNull.Value) ? 0 : ((decimal)dr["rating_average8"]);
                    aMaster.rating_average9 = (dr["rating_average9"] == DBNull.Value) ? 0 : ((decimal)dr["rating_average9"]);
                    aMaster.rating_average10 = (dr["rating_average10"] == DBNull.Value) ? 0 : ((decimal)dr["rating_average10"]);
                    aMaster.rating_average11 = (dr["rating_average11"] == DBNull.Value) ? 0 : ((decimal)dr["rating_average11"]);
                    aMaster.rating_average12 = (dr["rating_average12"] == DBNull.Value) ? 0 : ((decimal)dr["rating_average12"]);
                    aMaster.rating_averageT = (dr["rating_averageT"] == DBNull.Value) ? 0 : ((decimal)dr["rating_averageT"]);



                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

    }
}