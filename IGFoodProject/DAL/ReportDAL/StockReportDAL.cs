﻿using IGFoodProject.DataManager;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IGFoodProject.DAL
{
    public class StockReportDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();
        public DataTable GetLiveBirdStockInLedger(DateTime fromDate, DateTime toDate, int wareHouseId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameters = new List<SqlParameter>();
                aSqlParameters.Add(new SqlParameter("@fromDate", fromDate));
                aSqlParameters.Add(new SqlParameter("@toDate", toDate));
                aSqlParameters.Add(new SqlParameter("@wareHouseId", wareHouseId));
                DataTable dt = _accessManager.GetDataTable("monthlyLiveBirdStockInLedger", aSqlParameters);
                return dt;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetProductMovement(DateTime fromDate, DateTime toDate, int wareHouseId,int productId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameters = new List<SqlParameter>();
                aSqlParameters.Add(new SqlParameter("@FromDate", fromDate));
                aSqlParameters.Add(new SqlParameter("@ToDate", toDate));
                aSqlParameters.Add(new SqlParameter("@Warehouse", wareHouseId));
                aSqlParameters.Add(new SqlParameter("@ProductId", productId));
                DataTable dt = _accessManager.GetDataTable("sp_GetProductMovement", aSqlParameters);
                return dt;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetProductWiseStockVsChallan(DateTime fromDate, DateTime toDate, int wareHouseId,int productId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameters = new List<SqlParameter>();
                aSqlParameters.Add(new SqlParameter("@fromDate", fromDate));
                aSqlParameters.Add(new SqlParameter("@toDate", toDate));
                aSqlParameters.Add(new SqlParameter("@wareHouseId", wareHouseId));
                aSqlParameters.Add(new SqlParameter("@productId", productId));
                DataTable dt = _accessManager.GetDataTable("sp_product_wise_stock_vs_so_vs_challan", aSqlParameters);
                return dt;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetProductReceived(int companyId, int locationId, DateTime fromDate, DateTime todDate, string fromTime, string toTime, int warehouseId)
        {
            try
            {
                DateTime fdt = Convert.ToDateTime(fromTime);
                String fTime = fdt.ToString("HH:mm");

                DateTime tdt = Convert.ToDateTime(toTime);
                String tTime = tdt.ToString("HH:mm");
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CompanyId", companyId));
                aParameters.Add(new SqlParameter("@LocationId", locationId));
                aParameters.Add(new SqlParameter("@WarehouseId", warehouseId));
                aParameters.Add(new SqlParameter("@FromDate", fromDate));
                aParameters.Add(new SqlParameter("@ToDate", todDate));
                aParameters.Add(new SqlParameter("@FromTime", fTime));
                aParameters.Add(new SqlParameter("@ToTime", tTime));
                DataTable dt = _accessManager.GetDataTable("sp_GetReceiveStatus", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public DataTable GetProductIsssue(int warehouseId, DateTime fromDate, DateTime todDate, string fromTime, string toTime)
        {
            try
            {
                DateTime fdt = Convert.ToDateTime(fromTime);
                String fTime = fdt.ToString("HH:mm");

                DateTime tdt = Convert.ToDateTime(toTime);
                String tTime = tdt.ToString("HH:mm");

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@WarehouseId", warehouseId));
                aParameters.Add(new SqlParameter("@FromDate", fromDate));
                aParameters.Add(new SqlParameter("@ToDate", todDate));
                aParameters.Add(new SqlParameter("@FromTime", fTime));
                aParameters.Add(new SqlParameter("@ToTime", tTime));
                DataTable dt = _accessManager.GetDataTable("sp_GetProductIssueSenerio", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
      
        public DataTable GetUndeliverProduct(string soDate = "", string deliveryDate = "", int warehouseId = 0, int customerId = 0)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@SoDate", soDate));
                aParameters.Add(new SqlParameter("@DeliveryDate", deliveryDate));
                aParameters.Add(new SqlParameter("@WarehouseId", warehouseId));
                aParameters.Add(new SqlParameter("@CustomerId", customerId));
                DataTable dt = _accessManager.GetDataTable("sp_GetUndeliverProduct", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public DataTable GetAgingReport(int companyId, int locationId, DateTime fromDate, int groupid, int warehouseId)
        {
            try
            {
               
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CompanyId", companyId));
                aParameters.Add(new SqlParameter("@LocationId", locationId));
                aParameters.Add(new SqlParameter("@WarehouseId", warehouseId));
                aParameters.Add(new SqlParameter("@ProudctGroupId", groupid));
                aParameters.Add(new SqlParameter("@ReportDate", fromDate));
                
                DataTable dt = _accessManager.GetDataTable("sp_AgingReport", aParameters,true);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetCustomerProductSummary(DateTime fromDate, DateTime toDate, int groupid, int customertype, int customer, int reportType)
        {

            try
            {
                
               
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@FromDate", fromDate));
                aParameters.Add(new SqlParameter("@ToDate", toDate));
                aParameters.Add(new SqlParameter("@GroupId", groupid));
                aParameters.Add(new SqlParameter("@CustomerType", customertype));
                aParameters.Add(new SqlParameter("@CustomerId", customer));
                aParameters.Add(new SqlParameter("@Challanbit", reportType));
                //aParameters.Add(new SqlParameter("@deliverybit", 1));
               


                DataTable dt = new DataTable();

                dt = _accessManager.GetDataTable("sp_GetCustomerProductWisesummary", aParameters);





                return dt;

            }
            catch (Exception exception)
            {

                throw exception;
            }

            finally
            {

                _accessManager.SqlConnectionClose();
            }
        }


    }
}