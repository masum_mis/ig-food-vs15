﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using IGFoodProject.DataManager;
using IGFoodProject.Models;
using IGFoodProject.ViewModel;

namespace IGFoodProject.DAL
{
    public class QCRequestedDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();
        public DataTable GetQcRequestedProductList(int warehouseId, DateTime challlanDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@WarehouseId", warehouseId));
                aParameters.Add(new SqlParameter("@ChallanDate", challlanDate));
                DataTable dt = _accessManager.GetDataTable("sp_GetAllRequestedProdut", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse SaveReturnRequst(QcRequestedMaster aMaster)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@ProductId", aMaster.WarehouseId));
                aSqlParameterList.Add(new SqlParameter("@ChallanDate", aMaster.ChallanDate));
                aSqlParameterList.Add(new SqlParameter("@ReturnQty", aMaster.CreateDate));
                aSqlParameterList.Add(new SqlParameter("@ReturnKg", aMaster.CreateBy));
                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_SaveReturnRequestMaster", aSqlParameterList);

                foreach (var item in aMaster.AQcRequestedProducts)
                {
                    List<SqlParameter> dSqlParameterList = new List<SqlParameter>();
                    dSqlParameterList.Add(new SqlParameter("@ProductId", item.ProductId));
                    //aResponse.isSuccess = _accessManager.SaveData(" ", );
                }
                return aResponse;
            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public DataTable GetRemainingQcRequestedProductList(int warehouseId, int challanId, DateTime challlanDate, int isTrade)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@WarehouseId", warehouseId));
                aParameters.Add(new SqlParameter("@ChallanDate", challlanDate));
                aParameters.Add(new SqlParameter("@ChallanId", challanId));
                aParameters.Add(new SqlParameter("@isTrade", isTrade));
                DataTable dt = _accessManager.GetDataTable("sp_GetQcRequestedproduct", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public DataTable GetApprovalQcOperationProductList(int warehouseId, int challanId, DateTime challlanDate, int isTrade)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@WarehouseId", warehouseId));
                aParameters.Add(new SqlParameter("@ChallanDate", challlanDate));
                aParameters.Add(new SqlParameter("@ChallanId", challanId));
                aParameters.Add(new SqlParameter("@isTrade", isTrade));
                DataTable dt = _accessManager.GetDataTable("GetQcApprovalProductList", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public double GetProdutCostPrice(int productId, int groupId, DateTime challanDate)
        {
            try
            {
                var price = 0.0;
                DataTable dt = new DataTable();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@productid", productId));
                aParameters.Add(new SqlParameter("@challanDate", challanDate));
                if (groupId == 5)
                {
                    dt = _accessManager.GetDataTable("sp_GetMatricPrice", aParameters);
                }
                else if (groupId == 6)
                {
                    dt = _accessManager.GetDataTable("sp_GetMatricPrice_FurtherProcess", aParameters);
                }
                if (dt.Rows.Count > 0)
                {
                    price = Convert.ToDouble(DBNull.Value == dt.Rows[0].ItemArray[0] ? 0 : dt.Rows[0].ItemArray[0]);
                }
                else
                {
                    price = 0;
                }

                return price;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse SaveQcOperation(QcOperationMaster aMaster)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@QcDate", aMaster.QcDate));
                aSqlParameterList.Add(new SqlParameter("@QcComments", aMaster.QcComments));
                aSqlParameterList.Add(new SqlParameter("@ForwordedWarehouseId", aMaster.ForwordedWarehouseId));
                aSqlParameterList.Add(new SqlParameter("@SalesOrderChallanMasterId", aMaster.SalesOrderChallanMasterId));
                aSqlParameterList.Add(new SqlParameter("@ChallanDate", aMaster.ChallanDate));
                aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@CreateDate", aMaster.CreateDate));
                aSqlParameterList.Add(new SqlParameter("@isTrade", aMaster.IsTrade));
                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_SaveQcOperationMaster", aSqlParameterList);


                if (aResponse.pk > 0)
                {
                    foreach (var item in aMaster.QcOperationDetailses)
                    {
                        List<SqlParameter> sqlParameterList = new List<SqlParameter>();
                        sqlParameterList.Add(new SqlParameter("@QcOperationMasterId", aResponse.pk));
                       
                        sqlParameterList.Add(new SqlParameter("@ProductId", item.ProductId));
                        sqlParameterList.Add(new SqlParameter("@ProductQty", item.ProductQty));
                        sqlParameterList.Add(new SqlParameter("@ProductKg", item.ProductKg));
                        sqlParameterList.Add(new SqlParameter("@WeightlossQty", item.WeightlossQty));
                        sqlParameterList.Add(new SqlParameter("@WeightlossKg", item.WeightlossKg));
                        sqlParameterList.Add(new SqlParameter("@unitprice", item.UnitCost));
                        sqlParameterList.Add(new SqlParameter("@SalesOrderChallanDetailsId", item.SalesOrderChallanDetailsId));
                        aResponse.isSuccess = _accessManager.SaveData("sp_SaveQcOperationDetails", sqlParameterList);
                    }
                }



                //List<SqlParameter> sSqlParameterList = new List<SqlParameter>();
                //sSqlParameterList.Add(new SqlParameter("@StockInDate", aMaster.QcDate));
                //sSqlParameterList.Add(new SqlParameter("@StockInBy", aMaster.CreateBy));
                //sSqlParameterList.Add(new SqlParameter("@WareHouseId", aMaster.ForwordedWarehouseId));
                //sSqlParameterList.Add(new SqlParameter("@Comments", aMaster.QcComments));
                //sSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                //sSqlParameterList.Add(new SqlParameter("@CreateDate", aMaster.CreateDate));
                //sSqlParameterList.Add(new SqlParameter("@IsOpening", 8));
                //sSqlParameterList.Add(new SqlParameter("@IsTrade", aMaster.IsTrade));
                //var stockId = _accessManager.SaveDataReturnPrimaryKey("sp_SaveStockInMasterForQc", sSqlParameterList);


                //if (aResponse.pk > 0)
                //{
                //    foreach (var item in aMaster.QcOperationDetailses)
                //    {
                //        List<SqlParameter> sqlParameterList = new List<SqlParameter>();
                //        sqlParameterList.Add(new SqlParameter("@QcOperationMasterId", aResponse.pk));
                //        sqlParameterList.Add(new SqlParameter("@StockInId", stockId));
                //        sqlParameterList.Add(new SqlParameter("@ProductId", item.ProductId));
                //        sqlParameterList.Add(new SqlParameter("@ProductQty", item.ProductQty));
                //        sqlParameterList.Add(new SqlParameter("@ProductKg", item.ProductKg));
                //        sqlParameterList.Add(new SqlParameter("@StockInDetailID", item.StockInDetailID));
                //        sqlParameterList.Add(new SqlParameter("@WeightlossQty", item.WeightlossQty));
                //        sqlParameterList.Add(new SqlParameter("@WeightlossKg", item.WeightlossKg));
                //        sqlParameterList.Add(new SqlParameter("@StockInDate", aMaster.QcDate));
                //        sqlParameterList.Add(new SqlParameter("@IsOpening", 8));
                //        sqlParameterList.Add(new SqlParameter("@CreateDate", aMaster.CreateDate));
                //        sqlParameterList.Add(new SqlParameter("@unitprice", item.UnitCost));
                //        sqlParameterList.Add(new SqlParameter("@isTrade", item.IsTrade));
                //        sqlParameterList.Add(new SqlParameter("@SalesOrderChallanDetailsId", item.SalesOrderChallanDetailsId));
                //        aResponse.isSuccess = _accessManager.SaveData("sp_SaveQcOperationDetails", sqlParameterList);
                //    }
                //}

                return aResponse;
            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse ApproveQcOperation(QcOperationMaster aMaster)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

              


                List<SqlParameter> sSqlParameterList = new List<SqlParameter>();
                sSqlParameterList.Add(new SqlParameter("@StockInDate", aMaster.QcDate));
                sSqlParameterList.Add(new SqlParameter("@StockInBy", aMaster.CreateBy));
                sSqlParameterList.Add(new SqlParameter("@WareHouseId", aMaster.ForwordedWarehouseId));
                sSqlParameterList.Add(new SqlParameter("@Comments", aMaster.QcComments));
                sSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                sSqlParameterList.Add(new SqlParameter("@CreateDate", aMaster.CreateDate));
                sSqlParameterList.Add(new SqlParameter("@IsOpening", 8));
                sSqlParameterList.Add(new SqlParameter("@IsTrade", aMaster.IsTrade));
                var stockId = _accessManager.SaveDataReturnPrimaryKey("sp_SaveStockInMasterForQc", sSqlParameterList);


              
                    foreach (var item in aMaster.QcOperationDetailses)
                    {
                        List<SqlParameter> sqlParameterList1 = new List<SqlParameter>();
                        sqlParameterList1.Add(new SqlParameter("@QcOperationMasterId", item.QcOperationMasterId));
                        sqlParameterList1.Add(new SqlParameter("@QcOperationDetailsId", item.QcOperationDetailsId));
                        sqlParameterList1.Add(new SqlParameter("@StockInId", stockId));
                        sqlParameterList1.Add(new SqlParameter("@ProductId", item.ProductId));
                        sqlParameterList1.Add(new SqlParameter("@ProductQty", item.ProductQty));
                        sqlParameterList1.Add(new SqlParameter("@ProductKg", item.ProductKg));
                        sqlParameterList1.Add(new SqlParameter("@StockInDetailID", item.StockInDetailID));
                        sqlParameterList1.Add(new SqlParameter("@WeightlossQty", item.WeightlossQty));
                        sqlParameterList1.Add(new SqlParameter("@WeightlossKg", item.WeightlossKg));
                        sqlParameterList1.Add(new SqlParameter("@StockInDate", aMaster.QcDate));
                        sqlParameterList1.Add(new SqlParameter("@IsOpening", 8));
                        sqlParameterList1.Add(new SqlParameter("@CreateDate", aMaster.CreateDate));
                        sqlParameterList1.Add(new SqlParameter("@unitprice", item.UnitCost));
                        sqlParameterList1.Add(new SqlParameter("@isTrade", item.IsTrade));
                        sqlParameterList1.Add(new SqlParameter("@SalesOrderChallanDetailsId", item.SalesOrderChallanDetailsId));
                        sqlParameterList1.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                        aResponse.isSuccess = _accessManager.SaveData("sp_SaveQcOperationStockDetails", sqlParameterList1);
                    }
             

                return aResponse;
            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }




        public List<SalesOrderChallanMaster> GetChallanList(DateTime challanDate)
        {

            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SalesOrderChallanMaster> _List = new List<SalesOrderChallanMaster>();
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@ChallanDate", challanDate));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetChallanNo", aSqlParameterList);
                while (dr.Read())
                {
                    SalesOrderChallanMaster aInfo = new SalesOrderChallanMaster();
                    aInfo.SalesOrderChallanMasterId = Convert.ToInt32(dr["SalesOrderChallanMasterId"]);
                    aInfo.SalesOrderChallanNo = dr["SalesOrderChallanNo"].ToString();

                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetQcProductHistoryOfQty(int challanId, int productId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ChallanId", challanId));
                aParameters.Add(new SqlParameter("@ProductId", productId));
                DataTable dt = _accessManager.GetDataTable("sp_GetQcOperationHistoryOfQty", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetQcProductHistoryOfKg(int challanId, int productId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ChallanId", challanId));
                aParameters.Add(new SqlParameter("@ProductId", productId));
                DataTable dt = _accessManager.GetDataTable("sp_GetQcOperationHistoryOfKg", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public DataTable GetReturnQcList(int customerId,  DateTime challlanFromDate, DateTime challlanToDate, int warehouseId, int challanId, int? isTrade)
        {
            try { 
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CustomerId", customerId));
                aParameters.Add(new SqlParameter("@ChallanFormDate", challlanFromDate));
                aParameters.Add(new SqlParameter("@ChallanTodate", challlanToDate));
                aParameters.Add(new SqlParameter("@WarehouseId", warehouseId));
                aParameters.Add(new SqlParameter("@ChallanId", challanId));
                aParameters.Add(new SqlParameter("@IsTrade", isTrade));
                DataTable dt = _accessManager.GetDataTable("sp_GetProductReturn", aParameters,true);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public DataTable GetDeliveryList(int customerId, DateTime challlanFromDate, DateTime challlanToDate, int warehouseId, int challanId, int? isTrade)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CustomerId", customerId));
                aParameters.Add(new SqlParameter("@ChallanFormDate", challlanFromDate));
                aParameters.Add(new SqlParameter("@ChallanTodate", challlanToDate));
                aParameters.Add(new SqlParameter("@WarehouseId", warehouseId));
                aParameters.Add(new SqlParameter("@ChallanId", challanId));
                aParameters.Add(new SqlParameter("@IsTrade", isTrade));
                DataTable dt = _accessManager.GetDataTable("sp_GetProductDelivery", aParameters, true);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }



        public List<SalesOrderChallanMaster> GetChallanListFReturn(int customerId, DateTime fromDate,DateTime toDate)
        {

            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SalesOrderChallanMaster> _List = new List<SalesOrderChallanMaster>();
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@CustomerID", customerId));
                aSqlParameterList.Add(new SqlParameter("@ChallanFromDate", fromDate));
                aSqlParameterList.Add(new SqlParameter("@ChallanToDate", toDate));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetChallanNoFReturn", aSqlParameterList);
                while (dr.Read())
                {
                    SalesOrderChallanMaster aInfo = new SalesOrderChallanMaster();
                    aInfo.SalesOrderChallanMasterId = Convert.ToInt32(dr["SalesOrderChallanMasterId"]);
                    aInfo.SalesOrderChallanNo = dr["SalesOrderChallanNo"].ToString();

                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
    }
}