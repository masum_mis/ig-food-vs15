﻿using IGFoodProject.DataManager;
using IGFoodProject.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using IGFoodProject.ViewModel;

namespace IGFoodProject.DAL
{
    public class DBToPortionRequisitionDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();
        public List<CompanyInformation> CompanyInformation()
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                //List<SqlParameter> parameters = new List<SqlParameter>();
                //parameters.Add(new SqlParameter("@locationId", locationId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetAllCompanyInformation");
                List<CompanyInformation> wareHouseList = new List<CompanyInformation>();
                while (dr.Read())
                {
                    CompanyInformation wareHouse = new CompanyInformation();
                    wareHouse.CompanyId = Convert.ToInt32(dr["CompanyId"]);
                    wareHouse.CompanyShortName = dr["CompanyShortName"].ToString();
                    wareHouseList.Add(wareHouse);
                }
                return wareHouseList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public List<Location> LocationInformation(int comid)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@CompanyID", comid));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetAllLocationInformation", parameters);
                List<Location> wareHouseList = new List<Location>();
                while (dr.Read())
                {
                    Location wareHouse = new Location();
                    wareHouse.LocationId = Convert.ToInt32(dr["LocationId"]);
                    wareHouse.LocationName = dr["LocationName"].ToString();
                    wareHouseList.Add(wareHouse);
                }
                return wareHouseList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

        public List<WareHouse> WareHouseInformation(int loginId,int locid,int wareHouseTypeId)
        {
            try
            {
                
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@LocationId", locid));
                parameters.Add(new SqlParameter("@WarehouseTypeId", wareHouseTypeId));
                parameters.Add(new SqlParameter("@loginId", loginId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetWareHouseByLocation", parameters);
                List<WareHouse> wareHouseList = new List<WareHouse>();
                while (dr.Read())
                {
                    WareHouse wareHouse = new WareHouse();
                    wareHouse.WareHouseId = Convert.ToInt32(dr["WareHouseId"]);
                    wareHouse.WareHouseName = dr["WareHouseName"].ToString();
                    wareHouseList.Add(wareHouse);
                }
                return wareHouseList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

        public List<ViewDBRequsitionDetail> GetRequisitionDetailByID( int wareHouse)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@wareHouse", wareHouse));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetDBStockForportion", parameters);
                List<ViewDBRequsitionDetail> aList = new List<ViewDBRequsitionDetail>();
                while (dr.Read())
                {
                    ViewDBRequsitionDetail supplier = new ViewDBRequsitionDetail();
                    //supplier.StockInMasterID = Convert.ToInt32(dr["StockInMasterID"]);
                    //supplier.StockInDetailID = Convert.ToInt32(dr["StockInDetailID"]);
                    supplier.ProductID = Convert.ToInt32(dr["ProductID"]);
                    supplier.CategoryId = Convert.ToInt32(dr["CategoryId"]);
                    supplier.ProductName = dr["ProductName"].ToString();
                    supplier.TypeName = dr["TypeName"].ToString();
                    supplier.GroupName =dr["GroupName"].ToString();
                    if (dr["StockInQty"] != DBNull.Value)
                    {
                        supplier.StockInQty = Convert.ToDecimal(dr["StockInQty"]);
                    }
                    if (dr["StockInKG"] != DBNull.Value)
                    {
                        supplier.StockInKG = Convert.ToDecimal(dr["StockInKG"]);
                    }
                    supplier.UnitPrice = Convert.ToDecimal(dr["UnitPrice"]);
                    aList.Add(supplier);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

        public List<ViewProductDescription> LoadProduct(int wareHouseId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@WareHouseId", wareHouseId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetProductNameForDB", parameters);
                List<ViewProductDescription> wareHouseList = new List<ViewProductDescription>();
                while (dr.Read())
                {
                    ViewProductDescription wareHouse = new ViewProductDescription();
                    wareHouse.ProductId = Convert.ToInt32(dr["ProductId"]);
                    wareHouse.ProductName = dr["ProductName"].ToString();
                    wareHouseList.Add(wareHouse);
                }
                return wareHouseList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public bool SaveRequisition(tbl_DBToPortionRequisitionMaster aMaster)
        {
            bool result = false;
            try

            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                int id = SaveRequisitionMaster(aMaster);


                foreach (tbl_DBToPortionRequisitionDetail grade in aMaster.adetail)
                {
                    List<SqlParameter> gSqlParameterList = new List<SqlParameter>();
                    gSqlParameterList.Add(new SqlParameter("@DBToPortionRequisitionDetailID", 0));
                    gSqlParameterList.Add(new SqlParameter("@DBToPortionReqID", id));
                    gSqlParameterList.Add(new SqlParameter("@ProductID", grade.ProductID));
                    gSqlParameterList.Add(new SqlParameter("@ProductQty", grade.ProductQty));
                    gSqlParameterList.Add(new SqlParameter("@ProductQtyInKG", grade.ProductQtyInKG));
                    gSqlParameterList.Add(new SqlParameter("@Remarks", grade.Remarks));
                   

                    result = _accessManager.SaveData("sp_SaveDBToPortionRequisitionDetail_New", gSqlParameterList);
                }
                //if (result == true)
                //{
                //    List<SqlParameter> dSqlParameterList = new List<SqlParameter>();
                //    dSqlParameterList.Add(new SqlParameter("@DBToPortionReqID", id));
                //    result= _accessManager.SaveData("sp_SaveDbPortionIssueFromRequisitionAuto", dSqlParameterList);
                //}
            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
            return result;
        }
        public int SaveRequisitionMaster(tbl_DBToPortionRequisitionMaster aMaster)
        {
            try
            {
                string empReceived = string.Empty;
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@DBToPortionReqID", aMaster.DBToPortionReqID));
                aSqlParameterList.Add(new SqlParameter("@CompanyID", aMaster.CompanyID));
                aSqlParameterList.Add(new SqlParameter("@LocationID", aMaster.LocationID));
                aSqlParameterList.Add(new SqlParameter("@WareHouseID", aMaster.WareHouseID));
                aSqlParameterList.Add(new SqlParameter("@ToWarehouseId", aMaster.ToWarehouseId));
                aSqlParameterList.Add(new SqlParameter("@RequisitionDate", aMaster.RequisitionDate));
                aSqlParameterList.Add(new SqlParameter("@Comments", aMaster.Comments));
                aSqlParameterList.Add(new SqlParameter("@RequisitionBy", aMaster.RequisitionBy.Split(':')[0].Trim()));
                aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@CreateDate", System.DateTime.Now));
                aSqlParameterList.Add(new SqlParameter("@ProductionDate", aMaster.ProductionDate));

                
                return _accessManager.SaveDataReturnPrimaryKey("sp_SaveDBToPortionRequisitionMaster", aSqlParameterList);
            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }
        }
        public tbl_StockInDetail LoadProductStockForProtionIssue(int productId, int FromwareHouseId, DateTime reqDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@ProductID", productId));
                parameters.Add(new SqlParameter("@WareHouseId", FromwareHouseId));
                parameters.Add(new SqlParameter("@fromDate", reqDate));
                //SqlDataReader dr = _accessManager.GetSqlDataReader("sp_ProductStockForPortionIssue", parameters);

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_RptStockLedgerDateToDate", parameters);
                tbl_StockInDetail stockDetails = new tbl_StockInDetail();
                while (dr.Read())
                {
                    
                    stockDetails.PerKgStdQty = dr["PerKgStdQty"] == DBNull.Value ? 1 : Convert.ToDecimal(dr["PerKgStdQty"]);
                    stockDetails.StockInQty = Math.Round(Convert.ToDecimal(dr["CurrentStockQty"])) <0? Math.Round(Convert.ToDecimal(dr["CurrentStockKG"])* stockDetails.PerKgStdQty) : Math.Round(Convert.ToDecimal(dr["CurrentStockQty"]));
                    stockDetails.StockInKG = Convert.ToDecimal(dr["CurrentStockKG"]);
                }
                return stockDetails;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

       

        public DataTable GetPortionRequisition(int reqId=0, int fromwareHouseId=0)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@DBToPortionReqID", reqId));
                parameters.Add(new SqlParameter("@WareHouseId", fromwareHouseId));
                DataTable dr = _accessManager.GetDataTable("sp_RptForProtionRequisition", parameters);
                
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

        public DataTable GetDbToPorReqList(tbl_DBToPortionRequisitionMaster amaster)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@WarehouseId", amaster.WareHouseID));
                parameters.Add(new SqlParameter("@ToWarehouseId", amaster.ToWarehouseId));
                parameters.Add(new SqlParameter("@FromDate", amaster.FromDate));
                parameters.Add(new SqlParameter("@ToDate", amaster.ToDate));
                DataTable dr = _accessManager.GetDataTable("sp_GetProductListForPortionReport", parameters);

                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
    }
}
