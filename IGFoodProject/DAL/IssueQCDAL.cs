﻿using IGFoodProject.DataManager;
using IGFoodProject.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IGFoodProject.DAL
{
    public class IssueQCDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();
        public DataTable GetQcRequestedItemList(int warehouseId, DateTime returnDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@WarehouseId", warehouseId));
                aParameters.Add(new SqlParameter("@returnDate", returnDate));
                DataTable dt = _accessManager.GetDataTable("sp_GetQcRequestedItemList", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse SaveIssueRequest(OtherStockIssueMaster aMaster, string user)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@IssueQcRequestedMasterId", aMaster.IssueQcRequestedMasterId));
                aParameters.Add(new SqlParameter("@WarehouseId", aMaster.WarehouseId));
                aParameters.Add(new SqlParameter("@ReturnDate", aMaster.ReturnDate));
                aParameters.Add(new SqlParameter("@CreateBy", user));
                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_SaveIssueRequest", aParameters);
                if (aResponse.pk > 0)
                {
                    if (aMaster.StockIssueDetails.Count > 0)
                    {
                        aResponse.isSuccess = SaveIssueRequestDetails(aMaster.StockIssueDetails, aResponse.pk);
                    }
                    //aResponse.isSuccess = MrrReturnSystemJournal(aResponse.pk);
                    //aResponse.isSuccess = SaveOtherStockOut(aResponse.pk, aMaster.ReturnDetails);
                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                }
                else
                {
                    _accessManager.SqlConnectionClose(true);
                }
                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        private bool SaveIssueRequestDetails(List<OtherStockIssueDetails> aMaster, int aResponsePk)
        {
            try
            {
                bool result = false;
                foreach (var item in aMaster)
                {
                    if (item.ReturnQty > 0)
                    {
                        List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                        ResultResponse aResponse = new ResultResponse();
                        aSqlParameterList.Add(new SqlParameter("@IssueQcRequestedMasterId", aResponsePk));
                        aSqlParameterList.Add(new SqlParameter("@IssueReturnID", item.IssueReturnID));
                        aSqlParameterList.Add(new SqlParameter("@ItemId", item.ItemId));
                        aSqlParameterList.Add(new SqlParameter("@IssueQty", item.IssueQty));
                        aSqlParameterList.Add(new SqlParameter("@ReturnQty", item.ReturnQty));
                        aSqlParameterList.Add(new SqlParameter("@StockInDetailsId", item.StockInDetailsId));
                        aSqlParameterList.Add(new SqlParameter("@IssueReturnDetailID", item.IssueReturnDetailID));


                        aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_SaveIssueRequestDetails", aSqlParameterList);
                    }
                    //else
                    //{
                    //    break;
                    //}
                }
                return result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ResultResponse RejectRequest(List<OtherStockIssueDetails> aMaster, string entryBy)
        {

            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                foreach (var item in aMaster)
                {
                    List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                    aSqlParameterList.Add(new SqlParameter("@IssueReturnID", item.IssueReturnID));
                    aSqlParameterList.Add(new SqlParameter("@IssueReturnDetailID", item.IssueReturnDetailID));
                    aSqlParameterList.Add(new SqlParameter("@entryBy", entryBy));
                    aResponse.isSuccess = _accessManager.SaveData("sp_RejectItemReturnRequest", aSqlParameterList);

                }
                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public DataTable QCRequestedItemListPartial(DateTime? returnDate, int warehouseId = 0)
        {
            try
            {
                if (returnDate == null)
                {
                    returnDate = System.DateTime.Now;
                }
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@WarehouseId", warehouseId));
                aParameters.Add(new SqlParameter("@returnDate", returnDate));
                DataTable dt = _accessManager.GetDataTable("sp_QCRequestedItemListPartial", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<OtherStockIssueMaster> GetReturnNo(DateTime returnDate)
        {

            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<OtherStockIssueMaster> _List = new List<OtherStockIssueMaster>();
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@ReturnDate", returnDate));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetReturnNo", aSqlParameterList);
                while (dr.Read())
                {
                    OtherStockIssueMaster aInfo = new OtherStockIssueMaster();
                    aInfo.IssueReturnID = Convert.ToInt32(dr["IssueReturnID"]);
                    aInfo.ReturnNo = dr["ReturnNo"].ToString();

                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetRemainingQcRequestedItemList(int warehouseId, int issueReturnID, DateTime returnDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@WarehouseId", warehouseId));
                aParameters.Add(new SqlParameter("@ReturnDate", returnDate));
                aParameters.Add(new SqlParameter("@IssueReturnID", issueReturnID));
                DataTable dt = _accessManager.GetDataTable("sp_QcRequestedItemList", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse SaveQcOperation(OtherStockIssueMaster aMaster)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@IssueQcOperationMasterId", aMaster.IssueQcOperationMasterId));
                aParameters.Add(new SqlParameter("@QcDate", aMaster.QcDate));
                aParameters.Add(new SqlParameter("@ReturnDate", aMaster.ReturnDate));
                aParameters.Add(new SqlParameter("@QcComments", aMaster.QcComments));
                aParameters.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aParameters.Add(new SqlParameter("@ForwordedWarehouseId", aMaster.ForwordedWarehouseId));
                aParameters.Add(new SqlParameter("@IssueReturnID", aMaster.IssueReturnID));


                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_SaveItemQcOperation", aParameters);
                if (aResponse.pk > 0)
                {
                    if (aMaster.StockIssueDetails.Count > 0)
                    {
                        aResponse.isSuccess = SaveQcOperationDetails(aMaster.StockIssueDetails, aResponse.pk);
                    }
                    //aResponse.isSuccess = MrrReturnSystemJournal(aResponse.pk);
                    //aResponse.isSuccess = SaveOtherStockOut(aResponse.pk, aMaster.ReturnDetails);
                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                }
                else
                {
                    _accessManager.SqlConnectionClose(true);
                }
                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        private bool SaveQcOperationDetails(List<OtherStockIssueDetails> aMaster, int aResponsePk)
        {
            try
            {
                bool result = false;
                foreach (var item in aMaster)
                {
                    if (item.ItemQty > 0)
                    {
                        List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                        ResultResponse aResponse = new ResultResponse();
                        aSqlParameterList.Add(new SqlParameter("@IssueQcOperationMasterId", aResponsePk));
                        aSqlParameterList.Add(new SqlParameter("@ItemId", item.ItemId));
                        aSqlParameterList.Add(new SqlParameter("@ItemQty", item.ItemQty));
                        aSqlParameterList.Add(new SqlParameter("@StockInDetailsId", item.StockInDetailsId));
                        aSqlParameterList.Add(new SqlParameter("@UnitCost", item.UnitCost));
                        aSqlParameterList.Add(new SqlParameter("@IssueReturnDetailID", item.IssueReturnDetailID));
                        aSqlParameterList.Add(new SqlParameter("@IssueQCRequestId", item.IssueQCRequestId));


                        aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_SaveItemQcOperationDetails", aSqlParameterList);
                    }
                    //else
                    //{
                    //    break;
                    //}
                }
                return result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetQcItemHistoryOfQty(int IssueReturnDetailID, int ItemId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@IssueReturnDetailID", IssueReturnDetailID));
                aParameters.Add(new SqlParameter("@ItemId", ItemId));
                DataTable dt = _accessManager.GetDataTable("sp_GetQcItemHistoryOfQty", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public DataTable QcOperationApprovalList(int warehouseId, DateTime qcDate,int returnId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@WarehouseId", warehouseId));
                aParameters.Add(new SqlParameter("@qcDate", qcDate));
                aParameters.Add(new SqlParameter("@ReturnId", returnId));
                DataTable dt = _accessManager.GetDataTable("sp_QcOperationApprovalList", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }



        //public ResultResponse SaveQcApproval(List<OtherStockIssueMaster> aMaster, string entryBy)
        //{
        //    try
        //    {
        //        ResultResponse aResponse = new ResultResponse();
        //        _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
        //        foreach (var item in aMaster)
        //        {
        //            List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
        //            aSqlParameterList.Add(new SqlParameter("@WarehouseId", item.WarehouseId));
        //            aSqlParameterList.Add(new SqlParameter("@QcComments", item.QcComments));
        //            aSqlParameterList.Add(new SqlParameter("@LocationId", item.LocationId));
        //            aSqlParameterList.Add(new SqlParameter("@CompanyId", item.CompanyId));
        //            aSqlParameterList.Add(new SqlParameter("@IssueQcOperationMasterId", item.IssueQcOperationMasterId));
        //            aSqlParameterList.Add(new SqlParameter("@IssueQcOperationDetailsId", item.IssueQcOperationDetailsId));
        //            aSqlParameterList.Add(new SqlParameter("@entryBy", entryBy));

        //            aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_SaveQcApproval", aSqlParameterList);
        //            if (aResponse.pk > 0)
        //            {
        //                aResponse.isSuccess = SaveQcApprovalDetails(item, aResponse.pk, entryBy, item.IssueQcOperationDetailsId);

        //              aResponse.isSuccess = IssueReturnSystemJournal(aResponse.pk);

        //                if (aResponse.isSuccess == false)
        //                {
        //                    _accessManager.SqlConnectionClose(true);
        //                }
        //            }
        //            else
        //            {
        //                _accessManager.SqlConnectionClose(true);
        //            }

        //        }
        //        return aResponse;
        //    }
        //    catch (Exception ex)
        //    {
        //        _accessManager.SqlConnectionClose(true);
        //        throw ex;
        //    }
        //    finally
        //    {
        //        _accessManager.SqlConnectionClose();
        //    }
        //}

        public ResultResponse SaveQcApproval(OtherStockIssueMaster aMaster, string entryBy)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);


                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@WarehouseId", aMaster.ForwordedWarehouseId));
                aSqlParameterList.Add(new SqlParameter("@QcComments", aMaster.QcComments));
        
                aSqlParameterList.Add(new SqlParameter("@CompanyId", 7));
                //aSqlParameterList.Add(new SqlParameter("@IssueQcOperationMasterId", item.IssueQcOperationMasterId));
              
                aSqlParameterList.Add(new SqlParameter("@entryBy", entryBy));



                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_SaveQcApproval", aSqlParameterList);
                if (aResponse.pk > 0)
                {
                    aResponse.isSuccess = SaveQcApprovalDetails(aMaster.IssueQcDetails, aResponse.pk,entryBy);
                    aResponse.isSuccess = IssueReturnSystemJournal(aResponse.pk);

                }
               else
                {
                    _accessManager.SqlConnectionClose(true);
                }
                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        private bool IssueReturnSystemJournal(int pk)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@StockinId", pk));
                result = _accessManager.SaveData("SaveIssueReturnJournal", aParameters);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private bool SaveQcApprovalDetails(List<OtherQCOperationDetails> alist, int aResponsePk,string entryby)
        {
            try
            {
                bool result = false;



                foreach (var item in alist)
                {

                    List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                    ResultResponse aResponse = new ResultResponse();
                    aSqlParameterList.Add(new SqlParameter("@StockInMasterID", aResponsePk));
                    aSqlParameterList.Add(new SqlParameter("@ItemId", item.ItemId));
                    aSqlParameterList.Add(new SqlParameter("@QCQty", item.QCQty));
                    aSqlParameterList.Add(new SqlParameter("@ApproveQCQty", item.ApproveQCQty));
                    aSqlParameterList.Add(new SqlParameter("@UnitPrice", item.UnitPrice));
                    aSqlParameterList.Add(new SqlParameter("@CostValue", item.CostValue));
                    aSqlParameterList.Add(new SqlParameter("@IssueQcOperationDetailsId", item.IssueQcOperationDetailsId));
                    aSqlParameterList.Add(new SqlParameter("@IssueQcOperationMasterId", item.IssueQcOperationMasterId));
                    aSqlParameterList.Add(new SqlParameter("@CreateBy", entryby));
                    aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_SaveQcApprovalDetails", aSqlParameterList);
                }
                return result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //private bool SaveQcApprovalDetails(OtherStockIssueMaster item, int aResponsePk, string entryBy,int QcDetailsId) 
        //{
        //    try
        //    {
        //        bool result = false;

        //        List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
        //        ResultResponse aResponse = new ResultResponse();
        //        aSqlParameterList.Add(new SqlParameter("@StockInMasterID", aResponsePk));
        //        aSqlParameterList.Add(new SqlParameter("@ItemId", item.ItemId));
        //        aSqlParameterList.Add(new SqlParameter("@QCQty", item.QCQty));
        //        aSqlParameterList.Add(new SqlParameter("@UnitPrice", item.UnitPrice));
        //        aSqlParameterList.Add(new SqlParameter("@CostValue", item.CostValue));
        //        aSqlParameterList.Add(new SqlParameter("@IssueQcOperationDetailsId", QcDetailsId));

        //        aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_SaveQcApprovalDetails", aSqlParameterList);

        //        return result = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}



    }
}