﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using IGFoodProject.DataManager;
using IGFoodProject.Models;
using IGFoodProject.ViewModel;
using System.Data;
using IGFoodProject.Models.Accounts;

namespace IGFoodProject.DAL
{
    public class SalesCollectionDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();
        public List<SalesOrderChallanMaster> ChallanListForPaymentCollection(int customerId, DateTime paymentDate)
        {
            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();

                List<SalesOrderChallanMaster> aList = new List<SalesOrderChallanMaster>();

                aParameters.Add(new SqlParameter("@customerId", customerId));
                aParameters.Add(new SqlParameter("@paymentDate", paymentDate));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetChallanForSalesCollection", aParameters,true);
                while (dr.Read())
                {
                    //ViewSalesOrderProcessedMaster aMaster = new ViewSalesOrderProcessedMaster();

                    SalesOrderChallanMaster salesOrderChallanMaster = new SalesOrderChallanMaster();

                    salesOrderChallanMaster.SalesOrderChallanMasterId = Convert.ToInt32(dr["SalesOrderChallanMasterId"]);
                    salesOrderChallanMaster.SalesOrderChallanNo = Convert.ToString(dr["SalesOrderChallanNo"]);
                    salesOrderChallanMaster.ChallanDate = Convert.ToDateTime(dr["ChallanDate"]);
                    if (dr["DeliveryDate"] != DBNull.Value)
                    {
                        salesOrderChallanMaster.DeliveryDate = Convert.ToDateTime(dr["DeliveryDate"]);
                    }
                
                    salesOrderChallanMaster.DeliveryAmount = Convert.ToDecimal(dr["DeliveryAmount"]);
                    salesOrderChallanMaster.TotalPaid = Convert.ToDecimal(dr["TotalPaid"]);
                    salesOrderChallanMaster.SalesOrderNo = Convert.ToString(dr["SalesOrderNo"]);
                    salesOrderChallanMaster.SaleType = Convert.ToString(dr["SaleType"]);

                    salesOrderChallanMaster.TotalTax = Convert.ToDecimal(dr["TotalTax"]);
                    salesOrderChallanMaster.GrandTotal = Convert.ToDecimal(dr["GrandTotal"]);
                    salesOrderChallanMaster.restamount = Convert.ToDecimal(dr["restamount"]);
                    salesOrderChallanMaster.IsDeliveryConfirm = Convert.ToBoolean(dr["IsDeliveryConfirm"]);
                    salesOrderChallanMaster.Remarks = dr["Remarks"].ToString();

                    aList.Add(salesOrderChallanMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<SalesOrderChallanMaster> ChallanLocationListForPaymentCollection(int customerId, DateTime paymentDate, int locationId)
        {
            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();

                List<SalesOrderChallanMaster> aList = new List<SalesOrderChallanMaster>();

                aParameters.Add(new SqlParameter("@customerId", customerId));
                aParameters.Add(new SqlParameter("@paymentDate", paymentDate));
                aParameters.Add(new SqlParameter("@locationId", locationId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetChallanLocationForSalesCollection", aParameters);
                while (dr.Read())
                {
                    //ViewSalesOrderProcessedMaster aMaster = new ViewSalesOrderProcessedMaster();

                    SalesOrderChallanMaster salesOrderChallanMaster = new SalesOrderChallanMaster();

                    salesOrderChallanMaster.SalesOrderChallanMasterId = Convert.ToInt32(dr["SalesOrderChallanMasterId"]);
                    salesOrderChallanMaster.SalesOrderChallanNo = Convert.ToString(dr["SalesOrderChallanNo"]);
                    salesOrderChallanMaster.ChallanDate = Convert.ToDateTime(dr["ChallanDate"]);
                    salesOrderChallanMaster.DeliveryDate = Convert.ToDateTime(dr["DeliveryDate"]);
                    salesOrderChallanMaster.DeliveryAmount = Convert.ToDecimal(dr["DeliveryAmount"]);
                    salesOrderChallanMaster.TotalPaid = Convert.ToDecimal(dr["TotalPaid"]);
                    salesOrderChallanMaster.SalesOrderNo = Convert.ToString(dr["SalesOrderNo"]);
                    salesOrderChallanMaster.SaleType = Convert.ToString(dr["SaleType"]);

                    salesOrderChallanMaster.TotalTax = Convert.ToDecimal(dr["TotalTax"]);
                    salesOrderChallanMaster.GrandTotal = Convert.ToDecimal(dr["GrandTotal"]);
                    salesOrderChallanMaster.restamount = Convert.ToDecimal(dr["restamount"]);

                    aList.Add(salesOrderChallanMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        //public int SaveSalesCollectionMaster(SalesCollectionMaster aMaster)
        //{
        //    try
        //    {

        //        _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
        //        string empReceived = string.Empty;
        //        List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

        //        aSqlParameterList.Add(new SqlParameter("@CustomerId", aMaster.CustomerId));
        //        aSqlParameterList.Add(new SqlParameter("@PaymentAmount", aMaster.PaymentAmount));
        //        aSqlParameterList.Add(new SqlParameter("@PaymentDate", aMaster.PaymentDate));

        //        aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
        //        aSqlParameterList.Add(new SqlParameter("@CreateDate", System.DateTime.Now));
        //        aSqlParameterList.Add(new SqlParameter("@Remark", aMaster.Remark));
        //        aSqlParameterList.Add(new SqlParameter("@TaxAmount", aMaster.TaxAmount));
        //        aSqlParameterList.Add(new SqlParameter("@TaxPercent", aMaster.TaxPercent));
        //        aSqlParameterList.Add(new SqlParameter("@GrandAmount", aMaster.GrandAmount));
        //        aSqlParameterList.Add(new SqlParameter("@IsSrSalaryAdjust", aMaster.IsSrSalaryAdujst));

        //        int pk=_accessManager.SaveDataReturnPrimaryKey("sp_SaveSalesCollectionMaster", aSqlParameterList);
        //        _accessManager.SqlConnectionClose();

        //        if (aMaster.RestAmount > 0)
        //        {
        //            aMaster.PaymentAmount = aMaster.RestAmount;
        //            aMaster.MasterRefIdForAutoAdvance = pk;
        //            aMaster.IsAdvanced = true;
        //            aMaster.GrandAmount = aMaster.RestAmount;
        //            SaveAdvancedCollection(aMaster);
        //        }

        //        return pk;
        //    }
        //    catch (Exception exception)
        //    {
        //        _accessManager.SqlConnectionClose(true);
        //        throw exception;
        //    }

        //}
        public int SaveSalesCollectionMaster(SalesCollectionMaster aMaster)
        {
            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                string empReceived = string.Empty;
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@CustomerId", aMaster.CustomerId));
                aSqlParameterList.Add(new SqlParameter("@PaymentAmount", aMaster.PaymentAmount));
                aSqlParameterList.Add(new SqlParameter("@PaymentDate", aMaster.PaymentDate));

                aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@CreateDate", System.DateTime.Now));
                aSqlParameterList.Add(new SqlParameter("@Remark", aMaster.Remark));
                aSqlParameterList.Add(new SqlParameter("@TaxAmount", aMaster.TaxAmount));
                aSqlParameterList.Add(new SqlParameter("@TaxPercent", aMaster.TaxPercent));
                aSqlParameterList.Add(new SqlParameter("@GrandAmount", aMaster.GrandAmount));
                aSqlParameterList.Add(new SqlParameter("@IsSrSalaryAdjust", aMaster.IsSrSalaryAdujst));
                aSqlParameterList.Add(new SqlParameter("@IsRetaintionAdjust", aMaster.IsRetaintionAdjust));
                aSqlParameterList.Add(new SqlParameter("@IsDamageAdjust", aMaster.IsDamageAdjust));
                aSqlParameterList.Add(new SqlParameter("@FromAccount", aMaster.FromAccount));
                aSqlParameterList.Add(new SqlParameter("@IsPromotionAdjust", aMaster.IsPromotionAdjust));
                aSqlParameterList.Add(new SqlParameter("@IsFreezerRentAdjust", aMaster.IsFreezerRentAdjust));
                aSqlParameterList.Add(new SqlParameter("@IsChaldalAdjust", aMaster.IsChaldalAdjust));
                aSqlParameterList.Add(new SqlParameter("@IsDiscount", aMaster.IsDiscount));
                aSqlParameterList.Add(new SqlParameter("@IsDamageAdjustOther", aMaster.IsDamageAdjustOther));

                int pk = _accessManager.SaveDataReturnPrimaryKey("sp_SaveSalesCollectionMaster", aSqlParameterList);

                _accessManager.SqlConnectionClose();

                if (aMaster.RestAmount > 0)
                {
                    aMaster.PaymentAmount = aMaster.RestAmount;
                    aMaster.MasterRefIdForAutoAdvance = pk;
                    aMaster.IsAdvanced = true;
                    aMaster.GrandAmount = aMaster.RestAmount;
                    SaveAdvancedCollection(aMaster);

                }

                return pk;
            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }

        }

        //public bool SaveSalesCollectionDetails(SalesCollectionMaster aMaster)
        //{
        //    bool result = false;
        //    try

        //    {

        //        int pkId = SaveSalesCollectionMaster(aMaster);

        //        _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

        //        if (aMaster.SalesCollectionDetails != null)
        //        {
        //            foreach (SalesCollectionDetail item in aMaster.SalesCollectionDetails)
        //            {
        //                List<SqlParameter> gSqlParameterList = new List<SqlParameter>();
        //                gSqlParameterList.Add(new SqlParameter("@collectionMasterId", pkId));
        //                gSqlParameterList.Add(new SqlParameter("@paymentType", item.PaymentType));
        //                gSqlParameterList.Add(new SqlParameter("@paymentAmmount", item.PaymentAmount));
        //                gSqlParameterList.Add(new SqlParameter("@salesOrderChallanMasterId", item.SalesOrderChallanMasterId));
        //                gSqlParameterList.Add(new SqlParameter("@refNo", item.RefNo));
        //                gSqlParameterList.Add(new SqlParameter("@refDate", item.RefDate));
        //                gSqlParameterList.Add(new SqlParameter("@bankId", item.BankId));
        //                gSqlParameterList.Add(new SqlParameter("@branchId", item.BranchId));
        //                gSqlParameterList.Add(new SqlParameter("@Remark", item.Remark));
        //                gSqlParameterList.Add(new SqlParameter("@SaleType", item.SaleType));
        //                gSqlParameterList.Add(new SqlParameter("@TaxAmount", item.TaxAmountD));
        //                gSqlParameterList.Add(new SqlParameter("@GrandAmount", item.GrandTotalD));
        //                result = _accessManager.SaveData("sp_SaveSalesCollectionDetails", gSqlParameterList);
        //                //if (result == true)
        //                //{
        //                //    result = UpdateVocherMaster(pkId, aMaster.CreateBy);
        //                //}
        //            }
        //        }
        //        else
        //        {
        //            result = true;
        //        }


        //    }
        //    catch (Exception exception)
        //    {
        //        _accessManager.SqlConnectionClose(true);
        //        throw exception;
        //    }
        //    finally
        //    {
        //        _accessManager.SqlConnectionClose();
        //    }
        //    return result;
        //}
        public bool SaveSalesCollectionDetails(SalesCollectionMaster aMaster)
        {
            bool result = false;
            try

            {
                int pkId = 0;
                if (aMaster.IsSalesReturnAdvAdjust == 17)
                {

                }
                else
                {
                     pkId = SaveSalesCollectionMaster(aMaster);
                }
                   

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                if (aMaster.SalesCollectionDetails != null)
                {
                    foreach (SalesCollectionDetail item in aMaster.SalesCollectionDetails)
                    {
                        if (aMaster.IsSalesReturnAdvAdjust == 17)
                        {
                            List<SqlParameter> aParameters = new List<SqlParameter>();
                            aParameters.Add(new SqlParameter("@SalesOrderChallanMasterId", item.SalesOrderChallanMasterId));
                            aParameters.Add(new SqlParameter("@PaymentDate", aMaster.PaymentDate));
                            aParameters.Add(new SqlParameter("@Remark", aMaster.Remark));
                            result = _accessManager.SaveData("sp_SaveSalesReturnAdvancedAdjustAmount", aParameters);
                        }
                        else
                        {
                            List<SqlParameter> gSqlParameterList = new List<SqlParameter>();
                            gSqlParameterList.Add(new SqlParameter("@collectionMasterId", pkId));
                            gSqlParameterList.Add(new SqlParameter("@paymentType", item.PaymentType));
                            gSqlParameterList.Add(new SqlParameter("@paymentAmmount", item.PaymentAmount));
                            gSqlParameterList.Add(new SqlParameter("@salesOrderChallanMasterId", item.SalesOrderChallanMasterId));
                            gSqlParameterList.Add(new SqlParameter("@refNo", item.RefNo));
                            gSqlParameterList.Add(new SqlParameter("@refDate", item.RefDate));
                            gSqlParameterList.Add(new SqlParameter("@bankId", item.BankId));
                            gSqlParameterList.Add(new SqlParameter("@branchId", item.BranchId));
                            gSqlParameterList.Add(new SqlParameter("@Remark", item.Remark));
                            gSqlParameterList.Add(new SqlParameter("@SaleType", item.SaleType));
                            gSqlParameterList.Add(new SqlParameter("@TaxAmount", item.TaxAmountD));
                            gSqlParameterList.Add(new SqlParameter("@GrandAmount", item.GrandTotalD));
                            result = _accessManager.SaveData("sp_SaveSalesCollectionDetails", gSqlParameterList);
                        }


                     
                    }

                    //sr salary journal adjustment
                    if (aMaster.IsSrSalaryAdujst == 10)
                    {
                        bool s = SrSalaryAdjustVoucher(pkId);
                    }
                    //Retention journal adjustment

                    else if (aMaster.IsRetaintionAdjust == 12)
                    {
                        bool s = RetentionAdjustVoucher(pkId);
                    }
                    //Damage journal adjustment
                    else if ((aMaster.IsDamageAdjust == 13) || (aMaster.IsDamageAdjustOther == 18))
                    {
                        bool s = DamageAdjustVoucher(pkId);
                    }
                    //Promotion journal adjustment
                    else if (aMaster.IsPromotionAdjust == 14)
                    {
                        bool s = promotionAdjustVoucher(pkId);
                    }
                    //Rent journal adjustment
                    else if (aMaster.IsFreezerRentAdjust == 15)
                    {
                        bool s = RentAdjustVoucher(pkId);
                    }
                    //Chaldal journal adjustment
                    else if (aMaster.IsChaldalAdjust == 16)
                    {

                    }
                    //Sales Return Advanced adjustment
                    else if (aMaster.IsSalesReturnAdvAdjust == 17)
                    {

                    }
                    else
                    {
                        //open this for jopurnal
                        bool t = SalesCollectionVoucher(pkId, aMaster.IsDiscount);


                    }

                }

                if (result == true)
                {
                    List<SqlParameter> param = new List<SqlParameter>();
                    param.Add(new SqlParameter("@collectionMasterId", pkId));
                    param.Add(new SqlParameter("@CustomerId", aMaster.CustomerId));
                    param.Add(new SqlParameter("@PaymentAmount", aMaster.PaymentAmount));

                    result = _accessManager.SaveData("sp_SaveCreditMerger", param);

                }

                else
                {
                    result = true;
                }




            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
            return result;
        }


        public bool SavePreviousSalesCollection(SalesCollectionMaster aMaster)
        {
            bool result = false;
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                string empReceived = string.Empty;
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@CustomerId", aMaster.CustomerId));
                aSqlParameterList.Add(new SqlParameter("@PaymentAmount", aMaster.PaymentAmount));
                aSqlParameterList.Add(new SqlParameter("@PaymentDate", aMaster.PaymentDate));
                aSqlParameterList.Add(new SqlParameter("@InvoiceDate", aMaster.InvoiceDate));
                aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@CreateDate", System.DateTime.Now));
                aSqlParameterList.Add(new SqlParameter("@Remark", aMaster.Remark));

                aSqlParameterList.Add(new SqlParameter("@paymentType", aMaster.SalesCollectionDetails[0].PaymentType));
                aSqlParameterList.Add(new SqlParameter("@refNo", aMaster.SalesCollectionDetails[0].RefNo));
                aSqlParameterList.Add(new SqlParameter("@refDate", aMaster.SalesCollectionDetails[0].RefDate));
                aSqlParameterList.Add(new SqlParameter("@bankId", aMaster.SalesCollectionDetails[0].BankId));
                aSqlParameterList.Add(new SqlParameter("@branchId", aMaster.SalesCollectionDetails[0].BranchId));

                result = _accessManager.SaveData("sp_SavePreviousSalesCollection", aSqlParameterList);
            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
            return result;
        }



        public List<SalesCollectionDetail> GetPreviousCollectionDetails(int challanMasterId)
        {

            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SalesCollectionDetail> aList = new List<SalesCollectionDetail>();

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@salesOrderChallanMasterId", challanMasterId));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetPreviousCollectionDetails", aSqlParameterList);
                while (dr.Read())
                {
                    SalesCollectionDetail aDetail = new SalesCollectionDetail();
                    aDetail.PaymentTypeName = dr["PaymentType"].ToString();
                    aDetail.PaymentAmount = Convert.ToDecimal(dr["PaymentAmount"]);
                    if (dr["RefDate"] != DBNull.Value)
                    {
                        aDetail.RefDate = Convert.ToDateTime(dr["RefDate"]);
                    }
                    if (dr["PaymentDate"] != DBNull.Value)
                    {
                        aDetail.Collectiondate = Convert.ToDateTime(dr["PaymentDate"]);
                    }

                    if (dr["CreateDate"] != DBNull.Value)
                    {
                        aDetail.EntryDate = Convert.ToDateTime(dr["CreateDate"]);
                    }

                    aDetail.RefNo = dr["RefNo"].ToString();
                    aDetail.BankName = dr["BankName"].ToString();
                    aDetail.BranchName = dr["BranchName"].ToString();
                    aDetail.EntryBy = dr["CreateBy"].ToString();
                    aDetail.SerialNo = Convert.ToInt32(dr["SerialNo"]);
                    aDetail.Remark = dr["Remark"].ToString();

                    aDetail.TaxAmountD = Convert.ToDecimal(dr["TaxAmountD"]);
                    aDetail.GrandTotalD = Convert.ToDecimal(dr["GrandTotalD"]);

                    aList.Add(aDetail);
                }
                return aList;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        //public bool SaveAdvancedCollection(SalesCollectionMaster aMaster)
        //{
        //    bool result = false;
        //    try
        //    {
        //        _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
        //        string empReceived = string.Empty;
        //        List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

        //        aSqlParameterList.Add(new SqlParameter("@CustomerId", aMaster.CustomerId));
        //        aSqlParameterList.Add(new SqlParameter("@PaymentAmount", aMaster.PaymentAmount));
        //        aSqlParameterList.Add(new SqlParameter("@PaymentDate", aMaster.PaymentDate));

        //        aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
        //        aSqlParameterList.Add(new SqlParameter("@CreateDate", System.DateTime.Now));
        //        aSqlParameterList.Add(new SqlParameter("@Remark", aMaster.Remark));
        //        aSqlParameterList.Add(new SqlParameter("@IsAdvanced", aMaster.IsAdvanced));

        //        aSqlParameterList.Add(new SqlParameter("@paymentType", aMaster.SalesCollectionDetails[0].PaymentType));
        //        aSqlParameterList.Add(new SqlParameter("@refNo", aMaster.SalesCollectionDetails[0].RefNo));
        //        aSqlParameterList.Add(new SqlParameter("@refDate", aMaster.SalesCollectionDetails[0].RefDate));
        //        aSqlParameterList.Add(new SqlParameter("@bankId", aMaster.SalesCollectionDetails[0].BankId));
        //        aSqlParameterList.Add(new SqlParameter("@branchId", aMaster.SalesCollectionDetails[0].BranchId));


        //        aSqlParameterList.Add(new SqlParameter("@TaxAmount", aMaster.TaxAmount));
        //        aSqlParameterList.Add(new SqlParameter("@TaxPercent", aMaster.TaxPercent));
        //        aSqlParameterList.Add(new SqlParameter("@GrandAmount", aMaster.GrandAmount));
        //        aSqlParameterList.Add(new SqlParameter("@MasterRefIdForAutoAdvance", aMaster.MasterRefIdForAutoAdvance));
        //        result = _accessManager.SaveData("sp_SaveAdvancedCollection", aSqlParameterList);
        //    }
        //    catch (Exception exception)
        //    {
        //        _accessManager.SqlConnectionClose(true);
        //        throw exception;
        //    }
        //    finally
        //    {
        //        _accessManager.SqlConnectionClose();
        //    }
        //    return result;
        //}

        public bool SaveAdvancedCollection(SalesCollectionMaster aMaster)
        {
            bool result = false;
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                string empReceived = string.Empty;
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@CustomerId", aMaster.CustomerId));
                aSqlParameterList.Add(new SqlParameter("@PaymentAmount", aMaster.PaymentAmount));
                aSqlParameterList.Add(new SqlParameter("@PaymentDate", aMaster.PaymentDate));

                aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@CreateDate", System.DateTime.Now));
                aSqlParameterList.Add(new SqlParameter("@Remark", aMaster.Remark));
                aSqlParameterList.Add(new SqlParameter("@IsAdvanced", aMaster.IsAdvanced));

                aSqlParameterList.Add(new SqlParameter("@paymentType", aMaster.SalesCollectionDetails[0].PaymentType));
                aSqlParameterList.Add(new SqlParameter("@refNo", aMaster.SalesCollectionDetails[0].RefNo));
                aSqlParameterList.Add(new SqlParameter("@refDate", aMaster.SalesCollectionDetails[0].RefDate));
                aSqlParameterList.Add(new SqlParameter("@bankId", aMaster.SalesCollectionDetails[0].BankId));
                aSqlParameterList.Add(new SqlParameter("@branchId", aMaster.SalesCollectionDetails[0].BranchId));


                aSqlParameterList.Add(new SqlParameter("@TaxAmount", aMaster.TaxAmount));
                aSqlParameterList.Add(new SqlParameter("@TaxPercent", aMaster.TaxPercent));
                aSqlParameterList.Add(new SqlParameter("@GrandAmount", aMaster.GrandAmount));
                aSqlParameterList.Add(new SqlParameter("@MasterRefIdForAutoAdvance", aMaster.MasterRefIdForAutoAdvance));
                aSqlParameterList.Add(new SqlParameter("@FromAccount", aMaster.FromAccount));
                result = _accessManager.SaveData("sp_SaveAdvancedCollection", aSqlParameterList);

                //open this for jopurnal
                var t = AdvanceSalesCollectionVoucher(aMaster.MasterRefIdForAutoAdvance);

            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {

                _accessManager.SqlConnectionClose();
            }

            return result;
        }
        public DataTable GetAllCustomerLedger(DateTime fromDate, DateTime todDate, string customertypeId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@fromDate", fromDate));
                aParameters.Add(new SqlParameter("@toDate", todDate));
                aParameters.Add(new SqlParameter("@CustomerTypeId", customertypeId));
                DataTable dt = _accessManager.GetDataTable("sp_AllCustomerLedger", aParameters, true);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public DataTable GetCustomerLedger(int customerId, DateTime fromDate, DateTime todDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CustomerId", customerId));
                aParameters.Add(new SqlParameter("@fromDate", fromDate));
                aParameters.Add(new SqlParameter("@toDate", todDate));
                DataTable dt = _accessManager.GetDataTable("sp_CustomerLedger", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetCustomerPaymentList(int rpttype, DateTime? collfromDate, DateTime? colltoDate, DateTime? createfromDate, DateTime? createtoDate, int? customerId, string salesperson = "")
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CollectionFromDate", collfromDate));
                aParameters.Add(new SqlParameter("@CollectionToDate", colltoDate));
                aParameters.Add(new SqlParameter("@CreateFromDate", createfromDate));
                aParameters.Add(new SqlParameter("@CreateToDate", createtoDate));
                aParameters.Add(new SqlParameter("@Customer", customerId));
                aParameters.Add(new SqlParameter("@SalesPerson", salesperson));
                aParameters.Add(new SqlParameter("@AdvancedCollection", rpttype));
                DataTable dt = _accessManager.GetDataTable("sp_GetCustomerPaymentList", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public DataTable GetSdrSalary(int delarId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@DelarId", delarId));
                DataTable dt = _accessManager.GetDataTable("sp_GetSrSalaryMonthWise", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetCustomerAcLedger(int customerId, DateTime fromDate, DateTime toDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CustomerId", customerId));
                aParameters.Add(new SqlParameter("@FromDate", fromDate));
                aParameters.Add(new SqlParameter("@ToDate", toDate));
                DataTable dt = _accessManager.GetDataTable("sp_GetCustomerLedger", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public DataTable GetDayBookReport(int companyId, DateTime fromDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@PaymentDate", fromDate));
                aParameters.Add(new SqlParameter("@CompanyId", companyId));
                DataTable dt = _accessManager.GetDataTable("sp_GetDaybookCollection", aParameters, true);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetSalesPersonWiseCustomerList(int companyId, DateTime fromDate, DateTime toDate, string salesperson)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@FromDate", fromDate));
                aParameters.Add(new SqlParameter("@ToDate", toDate));
                aParameters.Add(new SqlParameter("@SalesPerson", salesperson));

                DataTable dt = _accessManager.GetDataTable("sp_GetSalesPersonWiseCustomer", aParameters, true);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public bool UpdateVocherMaster(int SalesCollectionMasterId, string entryby)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@SalesCollectionMasterId", SalesCollectionMasterId));
                aParameters.Add(new SqlParameter("@EntryBy", entryby));
                aParameters.Add(new SqlParameter("@EntryDate", DateTime.Now));
                result = _accessManager.SaveData("sp_Save_CreditVoucherMaster_SalesCollection", aParameters);

                return result;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool SalesCollectionVoucher(int CollectionId, int isDiscount)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CollectionMasterId", CollectionId));
                aParameters.Add(new SqlParameter("@isDiscount", isDiscount));

                result = _accessManager.SaveData("SaveSalesCollectionVoucher", aParameters);

                return result;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool SrSalaryAdjustVoucher(int CollectionId)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CollectionMasterId", CollectionId));

                result = _accessManager.SaveData("SaveSrSalaryAdjustVoucher", aParameters);

                return result;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private bool RetentionAdjustVoucher(int pkId)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CollectionMasterId", pkId));

                result = _accessManager.SaveData("SaveRetentionAdjustVoucher", aParameters);

                return result;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        private bool DamageAdjustVoucher(int pkId)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CollectionMasterId", pkId));

                result = _accessManager.SaveData("SaveDamageAdjustVoucher", aParameters);

                return result;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        private bool promotionAdjustVoucher(int pkId)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CollectionMasterId", pkId));

                result = _accessManager.SaveData("DebitNotePromotionAdjustVoucher", aParameters);

                return result;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        private bool RentAdjustVoucher(int pkId)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CollectionMasterId", pkId));

                result = _accessManager.SaveData("DebitNoteRentAdjustVoucher", aParameters);

                return result;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public bool AdvanceSalesCollectionVoucher(int MasterRefIdForAutoAdvance)
        {

            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CollectionMasterId", MasterRefIdForAutoAdvance));

                result = _accessManager.SaveData("SaveSalesCollectionVoucher_advance", aParameters);

                return result;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public DataTable GetRetaintionAdjust(int delarId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@DelarId", delarId));
                DataTable dt = _accessManager.GetDataTable("sp_GetRetaintionAdjustMonthWise", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public DataTable GetDamageAdjust(int delarId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@DelarId", delarId));
                DataTable dt = _accessManager.GetDataTable("sp_GetDamageAdjustMonthWise", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetSalesReturnAdvAdjust(int delarId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@DelarId", delarId));
                DataTable dt = _accessManager.GetDataTable("sp_GetSalesReturnAdvAdjust", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetCollectioninfoForVoid(int customerId, DateTime paymentDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CustomerId", customerId));
                aParameters.Add(new SqlParameter("@PaymentDate", paymentDate));
                DataTable dt = _accessManager.GetDataTable("sp_GetCollectioninfo", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse SaveCollectionVoid(SalesCollectionMaster aMaster, string user)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CollectionMasterId", aMaster.SalesCollectionMasterId));
                aParameters.Add(new SqlParameter("@PAmount", aMaster.PaymentAmount));
                aParameters.Add(new SqlParameter("@VoidBy", user));
                aParameters.Add(new SqlParameter("@VoidDate", DateTime.Now.Date));

                aResponse.isSuccess = _accessManager.SaveData("CollectionWrongEntry", aParameters);
                if (aResponse.isSuccess == true)
                {
                    aResponse.isSuccess = SalesCollectionVoucherRevise(aMaster.SalesCollectionMasterId);
                }

                if (aResponse.isSuccess == false)
                {
                    _accessManager.SqlConnectionClose(true);
                }
                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public bool SalesCollectionVoucherRevise(int CollectionId)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CollectionMasterId", CollectionId));

                result = _accessManager.SaveData("SaveSalesCollectionVoucherRevise", aParameters);

                return result;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public DataTable GetCustomerVoucherLedger(DateTime fromDate, DateTime todDate, int glId, int refDetailsId = 0)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@FromDate", fromDate));
                aParameters.Add(new SqlParameter("@ToDate", todDate));
                aParameters.Add(new SqlParameter("@GlId", glId));
                aParameters.Add(new SqlParameter("@RefDetailsId", refDetailsId));
                DataTable dt = _accessManager.GetDataTable("sp_GetCustomerVoucherLedger", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        
        public List<ChartOfAccLayerSeven> GetGLByGlType()
        {
            _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
            SqlDataReader dr = _accessManager.GetSqlDataReader("GetAllReferanceGL");
            List<ChartOfAccLayerSeven> _cList = new List<ChartOfAccLayerSeven>();
            while (dr.Read())
            {
                ChartOfAccLayerSeven aCur = new ChartOfAccLayerSeven();
                aCur.CALayerSevenId = Convert.ToInt32(dr["CALayerSevenId"]);
                aCur.CALayerSevenCode = dr["CALayerSevenCode"].ToString();
                aCur.CALayerSevenName = dr["CALayerSevenName"].ToString();

                _cList.Add(aCur);
            }
            return _cList;
        }


        public DataTable GetSubLedgerReport(DateTime fromDate, DateTime todDate, int glId) 
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@FromDate", fromDate));
                aParameters.Add(new SqlParameter("@ToDate", todDate));
                aParameters.Add(new SqlParameter("@GlId", glId));
                DataTable dt = _accessManager.GetDataTable("sp_GetSubLedgerReport", aParameters); 
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


    }
}