﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using IGFoodProject.DataManager;
using IGFoodProject.Models;
using IGFoodProject.ViewModel;

namespace IGFoodProject.DAL
{
    public class MtoDal
    {
        DataAccessManager _accessManager = new DataAccessManager();


        public string GetSalesPersonPhone(int empNo)
        {
            try
            {
                string PhoneNo = "";
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@empNo", empNo));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_SalesPersonPhoneNoMTO", aParameters);
                while (dr.Read())
                {
                    PhoneNo = dr["PhoneNo"].ToString();
                }
                return PhoneNo;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public ViewProductDetailsSales GetProductDetails(int id, DateTime? orderdate, int customertype, int wareHouseId = 0)
        {
            try
            {
                ViewProductDetailsSales aDetails = new ViewProductDetailsSales();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                aParameters.Add(new SqlParameter("@wareHouseId", wareHouseId));
                aParameters.Add(new SqlParameter("@orderdate", orderdate));
                aParameters.Add(new SqlParameter("@customertypeid", customertype));
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_ProductStockDetailsProcessedMto", aParameters);
                while (dr.Read())
                {
                    aDetails.ProductID = Convert.ToInt32(dr["ProductID"]);
                    aDetails.StockQty = DBNull.Value.Equals(dr["StockQty"]) ? 0 : Convert.ToDecimal(dr["StockQty"]);
                    aDetails.StockPrice = DBNull.Value.Equals(dr["StockPrice"]) ? 0 : Convert.ToDecimal(dr["StockPrice"]);
                    aDetails.StockKg = DBNull.Value.Equals(dr["StockKg"]) ? 0 : Convert.ToDecimal(dr["StockKg"]);
                    aDetails.groupid = Convert.ToInt32(dr["GroupId"]);

                    aDetails.BasePrice = DBNull.Value.Equals(dr["BasePrice"]) ? 0 : Convert.ToDecimal(dr["BasePrice"]);
                    aDetails.IsBulk = Convert.ToBoolean(dr["IsBulk"]);
                    aDetails.MTRetention = DBNull.Value.Equals(dr["MTRetention"]) ? 0 : Convert.ToDecimal(dr["MTRetention"]);
                    aDetails.PerKgStdQty = DBNull.Value.Equals(dr["PerKgStdQty"]) ? 0 : Convert.ToDecimal(dr["PerKgStdQty"]);
                    aDetails.Incentive = DBNull.Value.Equals(dr["Incentive"]) ? 0 : Convert.ToDecimal(dr["Incentive"]);

                }
                return aDetails;
            }
            catch (Exception ex)
            {

                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public decimal GetProductSalePrice(int id, decimal price)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                aParameters.Add(new SqlParameter("@price", price));
                decimal CostingPrice = 0;
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_SalePriceProcessedProductMTO", aParameters);
                while (dr.Read())
                {
                    CostingPrice = Convert.ToDecimal(dr["CostingPrice"]);
                }
                return CostingPrice;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<ViewSalesOrderProcessedMaster> GetSalesOrderProcessedMasterSearch(DateTime? fromdate, DateTime? todate, string salesperson, int customerid = 0, int deliveryWarehouseId = 0)
        {
            try
            {

                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-1);

                }
                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }
                if (salesperson != null)
                {
                    salesperson = salesperson.Split(':')[0].Trim();
                }
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<ViewSalesOrderProcessedMaster> aList = new List<ViewSalesOrderProcessedMaster>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@customerid", customerid));
                aParameters.Add(new SqlParameter("@SalesPerson", salesperson));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                aParameters.Add(new SqlParameter("@deliveryWarehouseId", deliveryWarehouseId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_SalesOrderProcessedListMTO", aParameters);
                while (dr.Read())
                {
                    ViewSalesOrderProcessedMaster aMaster = new ViewSalesOrderProcessedMaster();

                    aMaster.SalesOrderProcessedMasterId = Convert.ToInt32(dr["SalesOrderProcessedMasterId"]);
                    aMaster.TotatalOrderAmtKg = Convert.ToDecimal(dr["TotatalOrderAmtKg"]);
                    aMaster.TotalIncTaxVat = Convert.ToDecimal(dr["TotalIncTaxVat"]);
                    aMaster.TotalExTaxVat = Convert.ToDecimal(dr["TotalExTaxVat"]);
                    aMaster.TotalDue = Convert.ToDecimal(dr["TotalDue"]);
                    aMaster.TotalPaid = Convert.ToDecimal(dr["TotalPaid"]);

                    aMaster.SalesPerson = dr["SalesPerson"].ToString();
                    aMaster.SalesOrderNo = dr["SalesOrderNo"].ToString();
                    aMaster.CustomerName = dr["CustomerName"].ToString();
                    aMaster.DeliveryDestination = dr["DeliveryDestination"].ToString();
                    aMaster.SalesPersonContact = dr["SalesPersonContact"].ToString();

                    aMaster.CreateBy = dr["CreateBy"].ToString();
                    aMaster.ApproveBy = dr["ApproveBy"].ToString();
                    aMaster.ApproveDate = dr["ApproveDate"] as DateTime?;
                    aMaster.CreateDate = dr["CreateDate"] as DateTime?;
                    aMaster.OrderDate = dr["OrderDate"] as DateTime?;
                    aMaster.DeliveryDate = dr["DeliveryDate"] as DateTime?;
                    if (DBNull.Value == dr["IsApprove"])
                    {
                        aMaster.IsApprove = 0;
                    }
                    else
                    {
                        aMaster.IsApprove = Convert.ToInt32(dr["IsApprove"]);
                    }
                    if (DBNull.Value == dr["IsEmployee"])
                    {
                        aMaster.IsEmployee = null;
                    }
                    else
                    {
                        aMaster.IsEmployee = (bool)dr["IsEmployee"];
                    }
                    aMaster.CustomerType = dr["CoustomerTypeName"].ToString();
                    aMaster.DeliveryWarehouse = dr["WareHouseName"].ToString();
                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<ViewSalesOrderProcessedMaster> GetSalesOrderProcessedMasterForCancel(DateTime? fromdate, DateTime? todate, string salesperson, int customerid = 0)
        {
            try
            {
                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-1);

                }
                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }
                if (salesperson != null)
                {
                    salesperson = salesperson.Split(':')[0].Trim();
                }
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<ViewSalesOrderProcessedMaster> aList = new List<ViewSalesOrderProcessedMaster>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@customerid", customerid));
                aParameters.Add(new SqlParameter("@SalesPerson", salesperson));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_SalesOrderProcessedApprovedListForCancelMto", aParameters);
                while (dr.Read())
                {
                    ViewSalesOrderProcessedMaster aMaster = new ViewSalesOrderProcessedMaster();

                    aMaster.SalesOrderProcessedMasterId = Convert.ToInt32(dr["SalesOrderProcessedMasterId"]);
                    aMaster.TotatalOrderAmtKg = Convert.ToDecimal(dr["TotatalOrderAmtKg"]);
                    aMaster.TotalIncTaxVat = Convert.ToDecimal(dr["TotalIncTaxVat"]);
                    aMaster.TotalExTaxVat = Convert.ToDecimal(dr["TotalExTaxVat"]);
                    aMaster.TotalDue = Convert.ToDecimal(dr["TotalDue"]);
                    aMaster.TotalPaid = Convert.ToDecimal(dr["TotalPaid"]);

                    aMaster.SalesPerson = dr["SalesPerson"].ToString();
                    aMaster.SalesOrderNo = dr["SalesOrderNo"].ToString();
                    aMaster.CustomerName = dr["CustomerName"].ToString();
                    aMaster.DeliveryDestination = dr["DeliveryDestination"].ToString();
                    aMaster.SalesPersonContact = dr["SalesPersonContact"].ToString();

                    aMaster.CreateBy = dr["CreateBy"].ToString();
                    aMaster.ApproveBy = dr["ApproveBy"].ToString();
                    aMaster.ApproveDate = dr["ApproveDate"] as DateTime?;
                    aMaster.CreateDate = dr["CreateDate"] as DateTime?;
                    aMaster.OrderDate = dr["OrderDate"] as DateTime?;
                    aMaster.DeliveryDate = dr["DeliveryDate"] as DateTime?;
                    if (DBNull.Value == dr["IsApprove"])
                    {
                        aMaster.IsApprove = 0;
                    }
                    else
                    {
                        aMaster.IsApprove = Convert.ToInt32(dr["IsApprove"]);
                    }
                    if (DBNull.Value == dr["IsEmployee"])
                    {
                        aMaster.IsEmployee = null;
                    }
                    else
                    {
                        aMaster.IsEmployee = (bool)dr["IsEmployee"];
                    }
                    aMaster.CustomerType = dr["CoustomerTypeName"].ToString();
                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<CustomerType> GetCustomerTypeList()
        {

            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<CustomerType> _List = new List<CustomerType>();

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetCustomerTypeForMto");
                while (dr.Read())
                {
                    CustomerType aInfo = new CustomerType();
                    aInfo.CustomerTypeID = Convert.ToInt32(dr["CustomerTypeID"]);
                    aInfo.CoustomerTypeName = dr["CoustomerTypeName"].ToString();
                    aInfo.IsActive = Convert.ToBoolean(dr["IsActive"]);
                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<MtoLocationWarehouse> LoadWareHouseByMtoCustomer(int customerId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
   
                parameters.Add(new SqlParameter("@CustomerId", customerId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetMtoWarehouseLocationByCustomer", parameters);
                List<MtoLocationWarehouse> wareHouseList = new List<MtoLocationWarehouse>();
                while (dr.Read())
                {
                    MtoLocationWarehouse wareHouse = new MtoLocationWarehouse();
                    wareHouse.WarehouseId = Convert.ToInt32(dr["WarehouseId"]);
                    wareHouse.WareHouseName = dr["WareHouseName"].ToString();
                    wareHouseList.Add(wareHouse);
                }
                return wareHouseList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

        public List<MtoLocationWarehouse> LoadLocationByMtoCustomer(int customerId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();

                parameters.Add(new SqlParameter("@CustomerId", customerId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetMtoLocationByCustomer", parameters);
                List<MtoLocationWarehouse> wareHouseList = new List<MtoLocationWarehouse>();
                while (dr.Read())
                {
                    MtoLocationWarehouse wareHouse = new MtoLocationWarehouse();
                    wareHouse.LocationId = Convert.ToInt32(dr["LocationId"]);
                    wareHouse.LocationName = dr["LocationName"].ToString();
                    wareHouseList.Add(wareHouse);
                }
                return wareHouseList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

        public List<MtoLocationWarehouse> GetLocationIdByWareHouse(int wareHouse)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();

                parameters.Add(new SqlParameter("@wareHouse", wareHouse));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetMtoLocationByWarehouse", parameters);
                List<MtoLocationWarehouse> wareHouseList = new List<MtoLocationWarehouse>();
                while (dr.Read())
                {
                    MtoLocationWarehouse location = new MtoLocationWarehouse();
                    location.LocationId = Convert.ToInt32(dr["LocationId"]);
                    location.LocationName = dr["LocationName"].ToString();
                    wareHouseList.Add(location);
                }
                return wareHouseList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public SpecialOffers GetSpecialOffers(int customertype, DateTime orderDate, int location)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SpecialOffers aMaster = new SpecialOffers();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@customertype", customertype));
                aParameters.Add(new SqlParameter("@orderDate", orderDate.Date));
                aParameters.Add(new SqlParameter("@location", location));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetSpecialOffer", aParameters);


                while (dr.Read())
                {
                    aMaster.OfferId = Convert.ToInt32(dr["OfferId"]);

                    aMaster.Incentive = Convert.ToDecimal(dr["Incentive"]);

                    aMaster.OfferName = dr["OfferName"].ToString();


                }
                return aMaster;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public ResultResponse SaveSalesOrderProcessed(SalesOrderProcessedMaster aMaster, string user)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);


                if (aMaster.eflag == "ed")
                {
                    var addIn = aMaster.SalesDetails.Find(a => a.OperationIdAdd == 3); // Add In Challan if add in challan Is Approve =1
                    if (addIn != null)
                    {
                        aMaster.IsApprove = 1;
                    }
                    var deletIn = aMaster.SalesDetails.Find(a => a.OperationIdDelete == 3); // Delete from  Challan if delete in challan Is Approve =1
                    if (deletIn != null)
                    {
                        aMaster.IsApprove = 1;
                    }
                }
                else
                {
                    aMaster.IsApprove = 1;
                }




                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CustomerId", aMaster.CustomerId));
                aParameters.Add(new SqlParameter("@SalesOrderProcessedMasterId", aMaster.SalesOrderProcessedMasterId));
                aParameters.Add(new SqlParameter("@OrderType", aMaster.OrderType));
                if (aMaster.IsEmployee == true)
                {
                    aParameters.Add(new SqlParameter("@SalesPerson", aMaster.SalesPerson.Split(':')[0].Trim()));
                }
                else
                {
                    aParameters.Add(new SqlParameter("@SalesPerson", aMaster.SalesPerson));
                }
                aParameters.Add(new SqlParameter("@SrId", aMaster.SrId));
                aParameters.Add(new SqlParameter("@OrderDate", aMaster.OrderDate));
                aParameters.Add(new SqlParameter("@DeliveryDestination", aMaster.DeliveryDestination));
                aParameters.Add(new SqlParameter("@CreateBy", user));
                aParameters.Add(new SqlParameter("@OrderBy", user));
                aParameters.Add(new SqlParameter("@CreateDate", System.DateTime.Now));
                
                aParameters.Add(new SqlParameter("@CompanyId", aMaster.CompanyId));
                aParameters.Add(new SqlParameter("@SalesPersonContact", aMaster.SalesPersonContact));
                aParameters.Add(new SqlParameter("@DeliveryDate", aMaster.DeliveryDate));
                aParameters.Add(new SqlParameter("@TotatalOrderAmtKg", aMaster.TotatalOrderAmtKg));
                aParameters.Add(new SqlParameter("@TotalIncTaxVat", aMaster.TotalIncTaxVat));

                aParameters.Add(new SqlParameter("@TotalExTaxVat", aMaster.TotalExTaxVat));
                aParameters.Add(new SqlParameter("@TaxVat", aMaster.TaxVat));

                aParameters.Add(new SqlParameter("@collectiontypeid", aMaster.CollectionType));
                aParameters.Add(new SqlParameter("@collectionamount", aMaster.CollectionAmount));
                aParameters.Add(new SqlParameter("@IsEmployee", aMaster.IsEmployee));
                aParameters.Add(new SqlParameter("@CustomerTypeID", aMaster.CustomerTypeId));

                aParameters.Add(new SqlParameter("@ColBankId", aMaster.ColBankId));
                aParameters.Add(new SqlParameter("@ColBranchId", aMaster.ColBranchId));

                aParameters.Add(new SqlParameter("@RefNo", aMaster.RefNo));
                aParameters.Add(new SqlParameter("@RefDate", aMaster.RefDate));



                aParameters.Add(new SqlParameter("@Remarks", aMaster.Remarks));
              
                if (aMaster.eflag == "ap")
                {
                    aParameters.Add(new SqlParameter("@IsApprove", 1));
                }
                else
                {
                    aParameters.Add(new SqlParameter("@IsApprove", aMaster.IsApprove));
                }

                aParameters.Add(new SqlParameter("@eflag", aMaster.eflag));
                aParameters.Add(new SqlParameter("@DeliveryWarehouseId", aMaster.DeliveryWarehouseId));
                aParameters.Add(new SqlParameter("@LocationId", aMaster.LocationId));

                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_Save_SalesOrderProcessedMto", aParameters);
                if (aResponse.pk > 0)
                {
                    if (aMaster.IsApprove == 1 || aMaster.IsApprove == 0) // if Approve or new Entry
                    {
                        aResponse.isSuccess = SaveSalesOrderProcessedDetails(aMaster.SalesDetails, aResponse.pk, user, aMaster.IsApprove);
                    }
                    if (aMaster.IsApprove == 1)
                    {
                        //aResponse.isSuccess = SaveSOCreditVoucher(aResponse.pk);
                    }
                    if (aMaster.IsApprove == 2) // if Reject
                    {
                        aResponse.isSuccess = true;
                    }

                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                }
                else
                {
                    _accessManager.SqlConnectionClose(true);
                }
                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public bool SaveSOCreditVoucher(int SalesOrderProcessedMasterId)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@SalesOrderProcessedMasterId", SalesOrderProcessedMasterId));
                result = _accessManager.SaveData("sp_SaveSOCreditVoucher", aParameters);

                return result;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public bool SaveSalesOrderProcessedDetails(List<SalesOrderProcessedDetails> aList, int pk, string user, int isApprove)
        {
            try
            {
                bool result = false;
                foreach (var item in aList)
                {
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@SalesOrderProcessedMasterId", pk));
                    aParameters.Add(new SqlParameter("@SalesOrderProcessDetailsId", item.SalesOrderProcessDetailsId));
                    aParameters.Add(new SqlParameter("@ProductId", item.ProductId));
                    aParameters.Add(new SqlParameter("@ProductQty", item.ProductQty));
                    aParameters.Add(new SqlParameter("@ProductKg", item.ProductKg));
                    aParameters.Add(new SqlParameter("@StockRate", item.StockRate));
                    aParameters.Add(new SqlParameter("@SaleRate", item.SaleRate));
                    aParameters.Add(new SqlParameter("@BaseRate", item.BaseRate));
                    aParameters.Add(new SqlParameter("@DiscountAmount", item.DiscountAmount));
                    aParameters.Add(new SqlParameter("@DiscountPercent", item.DiscountPercent));
                    aParameters.Add(new SqlParameter("@TotalPrice", item.TotalPrice));
                    aParameters.Add(new SqlParameter("@BTotalPrice", item.BTotalPrice));
                    aParameters.Add(new SqlParameter("@ProductTax", item.ProductTax));
                    aParameters.Add(new SqlParameter("@CreateBy", user));
                    aParameters.Add(new SqlParameter("@IsDelete", item.IsDelete));
                    aParameters.Add(new SqlParameter("@isApprove", isApprove));
                    aParameters.Add(new SqlParameter("@OperationIdAdd", item.OperationIdAdd));
                    aParameters.Add(new SqlParameter("@OperationIdDelete", item.OperationIdDelete));
                    aParameters.Add(new SqlParameter("@CreateDate", DateTime.Now));
                    aParameters.Add(new SqlParameter("@groupid", item.GroupId));
                    aParameters.Add(new SqlParameter("@haschallan", item.haschallan));
                    aParameters.Add(new SqlParameter("@eflag", item.eflag));
                    aParameters.Add(new SqlParameter("@alreadypicked", item.alreadypicked));
                    aParameters.Add(new SqlParameter("@isbulk", item.isbulk));
                    aParameters.Add(new SqlParameter("@MTRetentionFront", item.MTRetention));
                    aParameters.Add(new SqlParameter("@Incentive", item.Incentive));
                    
                    result = _accessManager.SaveData("sp_Save_SavelOrderProcessedDetailsMto", aParameters);
                    if (result == false)
                    {
                        break;
                    }

                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public SalesOrderProcessedMaster GetSalesOrderProcessedMaster(int id)
        {
            try
            {
                List<SqlParameter> aParameters = new List<SqlParameter>();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                aParameters.Add(new SqlParameter("@id", id));
                DataTable dt = _accessManager.GetDataTable("sp_Get_SalesOrderProcessedMasrerById", aParameters);
                List<SalesOrderProcessedMaster> aList = Helper.ToListof<SalesOrderProcessedMaster>(dt);
                return aList[0];
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public List<SalesOrderProcessedDetails> GetSalesOderProcessedDetailsByMaster(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                DataTable dt = _accessManager.GetDataTable("sp_Get_SalesOrderPrecessedDetailsByMaster", aParameters);
                List<SalesOrderProcessedDetails> aList = Helper.ToListof<SalesOrderProcessedDetails>(dt);
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }






        public ResultResponse SaveSalesOrderProcessedApprove(SalesOrderProcessedMaster aMaster, string user)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);


                if (aMaster.eflag == "ed")
                {
                    var addIn = aMaster.SalesDetails.Find(a => a.OperationIdAdd == 3); // Add In Challan if add in challan Is Approve =1
                    if (addIn != null)
                    {
                        aMaster.IsApprove = 1;
                    }
                    var deletIn = aMaster.SalesDetails.Find(a => a.OperationIdDelete == 3); // Delete from  Challan if delete in challan Is Approve =1
                    if (deletIn != null)
                    {
                        aMaster.IsApprove = 1;
                    }
                }
                else
                {
                    aMaster.IsApprove = 1;
                }




                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CustomerId", aMaster.CustomerId));
                aParameters.Add(new SqlParameter("@SalesOrderProcessedMasterId", aMaster.SalesOrderProcessedMasterId));
                aParameters.Add(new SqlParameter("@OrderType", aMaster.OrderType));
                if (aMaster.IsEmployee == true)
                {
                    aParameters.Add(new SqlParameter("@SalesPerson", aMaster.SalesPerson.Split(':')[0].Trim()));
                }
                else
                {
                    aParameters.Add(new SqlParameter("@SalesPerson", aMaster.SalesPerson));
                }
                aParameters.Add(new SqlParameter("@SrId", aMaster.SrId));
                aParameters.Add(new SqlParameter("@OrderDate", aMaster.OrderDate));
                aParameters.Add(new SqlParameter("@DeliveryDestination", aMaster.DeliveryDestination));
                aParameters.Add(new SqlParameter("@CreateBy", user));
                aParameters.Add(new SqlParameter("@OrderBy", user));
                aParameters.Add(new SqlParameter("@CreateDate", System.DateTime.Now));

                aParameters.Add(new SqlParameter("@CompanyId", aMaster.CompanyId));
                aParameters.Add(new SqlParameter("@SalesPersonContact", aMaster.SalesPersonContact));
                aParameters.Add(new SqlParameter("@DeliveryDate", aMaster.DeliveryDate));
                aParameters.Add(new SqlParameter("@TotalOrderQty", aMaster.TotalOrderQty));
                aParameters.Add(new SqlParameter("@TotatalOrderAmtKg", aMaster.TotatalOrderAmtKg));
                aParameters.Add(new SqlParameter("@TotalIncTaxVat", aMaster.TotalIncTaxVat));

                aParameters.Add(new SqlParameter("@TotalExTaxVat", aMaster.TotalExTaxVat));
                aParameters.Add(new SqlParameter("@TaxVat", aMaster.TaxVat));

                aParameters.Add(new SqlParameter("@collectiontypeid", aMaster.CollectionType));
                aParameters.Add(new SqlParameter("@collectionamount", aMaster.CollectionAmount));
                aParameters.Add(new SqlParameter("@IsEmployee", aMaster.IsEmployee));
                aParameters.Add(new SqlParameter("@CustomerTypeID", aMaster.CustomerTypeId));

                aParameters.Add(new SqlParameter("@ColBankId", aMaster.ColBankId));
                aParameters.Add(new SqlParameter("@ColBranchId", aMaster.ColBranchId));

                aParameters.Add(new SqlParameter("@RefNo", aMaster.RefNo));
                aParameters.Add(new SqlParameter("@RefDate", aMaster.RefDate));



                aParameters.Add(new SqlParameter("@Remarks", aMaster.Remarks));

                if (aMaster.eflag == "ap")
                {
                    aParameters.Add(new SqlParameter("@IsApprove", 1));
                }
                else
                {
                    aParameters.Add(new SqlParameter("@IsApprove", aMaster.IsApprove));
                }

                aParameters.Add(new SqlParameter("@eflag", aMaster.eflag));
                aParameters.Add(new SqlParameter("@DeliveryWarehouseId", aMaster.DeliveryWarehouseId));
                aParameters.Add(new SqlParameter("@LocationId", aMaster.LocationId));


                if (aMaster.IsApprove == 2)
                {
                     aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_Save_SalesOrderProcessedMto", aParameters);

                    if (aResponse.pk > 0)
                    {
                        if (aMaster.IsApprove == 2) // if Reject
                        {
                            aResponse.isSuccess = true;
                        }

                        if (aResponse.isSuccess == false)
                        {
                            _accessManager.SqlConnectionClose(true);
                        }
                    }
                    else
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                }

                if (aMaster.IsApprove == 1)
                {
                    SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Save_SalesProcessedApproveForMTO", aParameters);
                    int salesOrderMasterId = 0;
                    int pickingMasterId = 0;
                    int challanMasterId = 0;
                    //int collectionMasterId = 0;

                    while (dr.Read())
                    {
                        salesOrderMasterId = Convert.ToInt32(dr["masteridTab"]);
                        pickingMasterId = Convert.ToInt32(dr["PickingMasterIdTab"]);
                        challanMasterId = Convert.ToInt32(dr["ChallanMasterIdTab"]);
                        //collectionMasterId = Convert.ToInt32(dr["CollectionMasterIdTab"]);

                    }
                    dr.Close();



                    if (salesOrderMasterId > 0 && pickingMasterId > 0 && challanMasterId > 0)
                    {
                        if (aMaster.IsApprove == 1) // if Approve or new Entry
                        {
                            aResponse.pk = salesOrderMasterId;
                            aResponse.isSuccess = SaveSalesOrderProcessedDetailsApprove(aMaster.SalesDetails, user, salesOrderMasterId, pickingMasterId, challanMasterId);
                        }
                        if (aMaster.IsApprove == 1)
                        {
                            //aResponse.isSuccess = SaveSOCreditVoucher(aResponse.pk);
                        }

                        if (aResponse.isSuccess == false)
                        {
                            _accessManager.SqlConnectionClose(true);
                        }
                    }
                    else
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                }

                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public bool SaveSalesOrderProcessedDetailsApprove(List<SalesOrderProcessedDetails> aList, string user, int salesOrderMasterId, int pickingMasterId, int challanMasterId)
        {
            try
            {
                bool result = false;
                foreach (var item in aList)
                {
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@SalesOrderProcessedMasterId", salesOrderMasterId));
                    aParameters.Add(new SqlParameter("@SalesOrderProcessDetailsId", item.SalesOrderProcessDetailsId));
                    aParameters.Add(new SqlParameter("@PickingMasterId", pickingMasterId));
                    aParameters.Add(new SqlParameter("@ChallanMasterId", challanMasterId));
                    aParameters.Add(new SqlParameter("@ProductId", item.ProductId));
                    aParameters.Add(new SqlParameter("@ProductQty", item.ProductQty));
                    aParameters.Add(new SqlParameter("@ProductKg", item.ProductKg));
                    aParameters.Add(new SqlParameter("@StockRate", item.StockRate));
                    aParameters.Add(new SqlParameter("@SaleRate", item.SaleRate));
                    aParameters.Add(new SqlParameter("@BaseRate", item.BaseRate));
                    aParameters.Add(new SqlParameter("@DiscountAmount", item.DiscountAmount));
                    aParameters.Add(new SqlParameter("@DiscountPercent", item.DiscountPercent));
                    aParameters.Add(new SqlParameter("@TotalPrice", item.TotalPrice));
                    aParameters.Add(new SqlParameter("@BTotalPrice", item.BTotalPrice));
                    aParameters.Add(new SqlParameter("@ProductTax", item.ProductTax));
                    aParameters.Add(new SqlParameter("@CreateBy", user));
                    aParameters.Add(new SqlParameter("@IsDelete", item.IsDelete));
                    aParameters.Add(new SqlParameter("@isApprove", 1));
                    aParameters.Add(new SqlParameter("@OperationIdAdd", item.OperationIdAdd));
                    aParameters.Add(new SqlParameter("@OperationIdDelete", item.OperationIdDelete));
                    aParameters.Add(new SqlParameter("@CreateDate", DateTime.Now));
                    aParameters.Add(new SqlParameter("@groupid", item.GroupId));
                    aParameters.Add(new SqlParameter("@haschallan", item.haschallan));
                    aParameters.Add(new SqlParameter("@eflag", item.eflag));
                    aParameters.Add(new SqlParameter("@alreadypicked", item.alreadypicked));
                    aParameters.Add(new SqlParameter("@isbulk", item.isbulk));
                    aParameters.Add(new SqlParameter("@MTRetentionFront", item.MTRetention));
                    aParameters.Add(new SqlParameter("@Incentive", item.Incentive));
                    result = _accessManager.SaveData("sp_Save_SalesProcessedDetailsApproveForMTO", aParameters);
                    if (result == false)
                    {
                        break;
                    }

                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}