﻿using IGFoodProject.DataManager;
using IGFoodProject.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IGFoodProject.DAL
{
    public class ItemDAL
    {
        DataAccessManager accessManager = new DataAccessManager();
        public List<ItemType> GetTypeList(int groupid)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ItemType> _List = new List<ItemType>();

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@GroupId", groupid));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetItemTypeByGroup", aSqlParameterList);
                while (dr.Read())
                {
                    ItemType aInfo = new ItemType();
                    aInfo.ItemTypeId = (int)dr["ItemTypeId"];

                    aInfo.ItemTypeName = dr["ItemTypeName"].ToString();



                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<ItemColor> GetItemColor()
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ItemColor> _List = new List<ItemColor>();                

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetItemColorForDDL");
                while (dr.Read())
                {
                    ItemColor aInfo = new ItemColor();

                    aInfo.ItemColorId = (int)dr["ItemColorId"];

                    aInfo.ItemColorName = dr["ItemColorName"].ToString();

                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<ItemCategory> GetCategoryListByType(int typeid)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ItemCategory> _List = new List<ItemCategory>();

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@typeid", typeid));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetItemCategoryByTpeID", aSqlParameterList);
                while (dr.Read())
                {
                    ItemCategory aInfo = new ItemCategory();

                    aInfo.ItemCategoryId = (int)dr["ItemCategoryId"];

                    aInfo.ItemCategoryName = dr["ItemCategoryName"].ToString();

                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public int CheckItem(string group) 
        {
            int pgroup = 0;
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@group", group));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_CheckDuplicateItem", aSqlParameterList);

                while (dr.Read())
                {
                    pgroup = Convert.ToInt32(dr["pgroup"]);
                }
            }
            catch (Exception exception)
            {
                //accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
            return pgroup;
        }

        public bool SaveItem(Item aMaster) 
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@ItemId", aMaster.ItemId));
                aSqlParameterList.Add(new SqlParameter("@ItemGroupId", aMaster.ItemGroupId));
                aSqlParameterList.Add(new SqlParameter("@ItemTypeId", aMaster.ItemTypeId));
                aSqlParameterList.Add(new SqlParameter("@ItemCategoryId", aMaster.ItemCategoryId));
                aSqlParameterList.Add(new SqlParameter("@PackSizeId", aMaster.PackSizeId));
                aSqlParameterList.Add(new SqlParameter("@ItemColorId", aMaster.ItemColorId));
                aSqlParameterList.Add(new SqlParameter("@UOMId", aMaster.UOMId));
                aSqlParameterList.Add(new SqlParameter("@ItemName", aMaster.ItemName));
                aSqlParameterList.Add(new SqlParameter("@ItemDescription", aMaster.ItemDescription));
                aSqlParameterList.Add(new SqlParameter("@Remarks", aMaster.Remarks));
                aSqlParameterList.Add(new SqlParameter("@CreateDate", System.DateTime.Now));
                aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@UpdateDate", System.DateTime.Now));
                aSqlParameterList.Add(new SqlParameter("@UpdateBy", aMaster.UpdateBy));
                aSqlParameterList.Add(new SqlParameter("@IsActive", aMaster.IsActive));


                return accessManager.SaveData("sp_SaveItemMaster", aSqlParameterList);
            }
            catch (Exception exception)
            {
                accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
           
        }

        public Item GetItemByIDForEdit(int id) 
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ItemId", id));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetItemByID", aParameters);
                Item aDetailsView = new Item();
                while (dr.Read())
                {


                    aDetailsView.ItemId = (int)dr["ItemId"];
                    aDetailsView.ItemGroupId = (int)dr["ItemGroupId"];
                    aDetailsView.ItemTypeId = (int)dr["ItemTypeId"];
                    aDetailsView.ItemCategoryId = (int)dr["ItemCategoryId"];
                    aDetailsView.ItemColorId = (int)dr["ItemColorId"];
                    aDetailsView.PackSizeId = (int)dr["PackSizeId"];
                    aDetailsView.UOMId = (int)dr["UOMId"];
                    aDetailsView.ItemName = dr["ItemName"].ToString();
                    aDetailsView.ItemName1 = dr["ItemName1"].ToString();
                    aDetailsView.Remarks = dr["Remarks"].ToString();
                    aDetailsView.ItemDescription = dr["ItemDescription"].ToString();
                    aDetailsView.IsActive = (bool)dr["IsActive"];

                }
                return aDetailsView;

            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<Item> GetItemList(int groupId = 0) 
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<Item> _List = new List<Item>();

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetItemList", aSqlParameterList);
                while (dr.Read())
                {
                    Item aInfo = new Item();                 
                    aInfo.ItemId = (int)dr["ItemId"];                 
                    aInfo.ItemName = dr["ItemName"].ToString();
                    aInfo.ItemDescription = dr["ItemDescription"].ToString();
                    aInfo.Remarks = dr["Remarks"].ToString();

                    aInfo.ItemCategoryName = dr["ItemCategoryName"].ToString();
                    aInfo.ItemColorName = dr["ItemColorName"].ToString();
                    aInfo.PackSizeName = dr["PackSizeName"].ToString();
                    aInfo.UOMName = dr["UOMName"].ToString();

                    aInfo.ItemGroupName = dr["ItemGroupName"].ToString();
                    aInfo.ItemTypeName = dr["ItemTypeName"].ToString();

                    if (dr["CreateDate"] != DBNull.Value)
                    {
                        aInfo.CreateDate = Convert.ToDateTime(dr["CreateDate"]);
                    }
                    aInfo.CreateBy = dr["CreateBy"].ToString();

                    aInfo.UpdateBy = dr["UpdateBy"].ToString();
                    if (dr["UpdateDate"] != DBNull.Value)
                    {
                        aInfo.UpdateDate = Convert.ToDateTime(dr["UpdateDate"]);
                    }
                    if (dr["IsActive"] != DBNull.Value)
                    {
                        aInfo.IsActive = Convert.ToBoolean(dr["IsActive"]);
                    }
                                  
                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public bool DeleteItem(int id) 
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@ItemId", id));

                return accessManager.SaveData("sp_DeleteItem", aSqlParameterList);
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }


    }
}