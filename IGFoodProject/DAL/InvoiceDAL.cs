﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using IGFoodProject.DataManager;
using IGFoodProject.Models;
using IGFoodProject.ViewModel;
using System.Data;
namespace IGFoodProject.DAL
{
    public class InvoiceDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();
        public List<ViewOthersMrr> LoadMrrNoBySupplier(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@Id", id));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetMrrNoBySupplierId", aParameters);
                List<ViewOthersMrr> ItemDescriptionList = new List<ViewOthersMrr>();
                while (dr.Read())
                {
                    ViewOthersMrr itemDescription = new ViewOthersMrr();
                    itemDescription.MRRrOtherID = Convert.ToInt32(dr["MRRrOtherID"]);
                    itemDescription.MrrNo = dr["MrrNo"].ToString();
                    ItemDescriptionList.Add(itemDescription);
                }
                return ItemDescriptionList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<ViewInvoiceMaster> LoadMrrInfo(int supplierId, int mrrId = 0, DateTime? mrrDate = null)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@SupplierId", supplierId));
                aParameters.Add(new SqlParameter("@MrrId", mrrId));
                aParameters.Add(new SqlParameter("@MrrDate", mrrDate));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetMrrInfoForInvoiceBySearch", aParameters);
                List<ViewInvoiceMaster> ItemDescriptionList = new List<ViewInvoiceMaster>();
                while (dr.Read())
                {
                    ViewInvoiceMaster itemDescription = new ViewInvoiceMaster();
                    itemDescription.MRRrOtherID = Convert.ToInt32(dr["MRRrOtherID"]);
                    itemDescription.PurchaseOrderId = Convert.ToInt32(dr["PurchaseOrderId"]);
                    itemDescription.POAdvance = Convert.ToDecimal(dr["POAdvance"]);
                    itemDescription.SupplierID = Convert.ToInt32(dr["SupplierID"]);
                    itemDescription.BillQuantity = Convert.ToDecimal(dr["BillQuantity"]);
                    itemDescription.ReceivedQty = Convert.ToDecimal(dr["ReceivedQty"]);
                    itemDescription.TotalAmount = Convert.ToDecimal(dr["TotalAmount"]);
                    itemDescription.MRRDate = Convert.ToDateTime(dr["MRRDate"]);
                    itemDescription.MrrNo = dr["MrrNo"].ToString();
                    itemDescription.PurchaseOrderNo = dr["PurchaseOrderNo"].ToString();
                    itemDescription.ItemDescription = dr["ItemDescription"].ToString();
                    ItemDescriptionList.Add(itemDescription);
                }
                return ItemDescriptionList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public ResultResponse SaveBill(ViewInvoiceMaster aMaster, string user)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@InvoiceMasterID", aMaster.InvoiceMasterID));
                aParameters.Add(new SqlParameter("@InvoiceDate", aMaster.InvoiceDate));
                aParameters.Add(new SqlParameter("@dueDate", aMaster.DueDate));
                aParameters.Add(new SqlParameter("@billDate", aMaster.SupplierBillDate));
                aParameters.Add(new SqlParameter("@billno", aMaster.SupplierBillNo));
                aParameters.Add(new SqlParameter("@VatPercent", aMaster.VatPercent));
                aParameters.Add(new SqlParameter("@VatAmount", aMaster.VatAmount));
                aParameters.Add(new SqlParameter("@TotalAmount", aMaster.TotalAmount));
                aParameters.Add(new SqlParameter("@DiscountAmt", aMaster.DiscountAmt));
                aParameters.Add(new SqlParameter("@SupplierID", aMaster.SupplierID));
                aParameters.Add(new SqlParameter("@TotalQty", aMaster.InvoiceDetails.Sum(n => n.InvoiceQty)));
                aParameters.Add(new SqlParameter("@CreateBy", user));
                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("SaveInvoice", aParameters);
                if (aResponse.pk > 0)
                {
                    if (aMaster.InvoiceDetails.Count > 0)
                    {
                        aResponse.isSuccess = SaveInvoiceDetails(aMaster.InvoiceDetails, aResponse.pk);
                    }
                   aResponse.isSuccess = CreateSystemJournal(aResponse.pk);
                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                }
                else
                {
                    _accessManager.SqlConnectionClose(true);
                }
                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        private bool SaveInvoiceDetails(List<ViewInvoiceDetails> aMaster, int aResponsePk)
        {
            try
            {
                bool result = false;
                foreach (var item in aMaster)
                {
                    if (item.InvoiceQty > 0)
                    {
                        List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                        ResultResponse aResponse = new ResultResponse();
                        aSqlParameterList.Add(new SqlParameter("@InvoiceId", aResponsePk));
                        aSqlParameterList.Add(new SqlParameter("@MRRID", item.MRRID));
                        aSqlParameterList.Add(new SqlParameter("@MRRDate", item.MRRDate));
                        aSqlParameterList.Add(new SqlParameter("@ReceivedQty", item.ReceivedQty));
                        aSqlParameterList.Add(new SqlParameter("@InvoiceQty", item.InvoiceQty));
                        aSqlParameterList.Add(new SqlParameter("@PriceWithoutVat", item.PriceWithoutVat));
                        aSqlParameterList.Add(new SqlParameter("@Advanced", item.Advanced));
                        aSqlParameterList.Add(new SqlParameter("@TotalInvoicePrice", item.TotalInvoicePrice));
                        aSqlParameterList.Add(new SqlParameter("@ItemDescription", item.ItemDescription));
                        aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("SaveInvoiceDetails", aSqlParameterList);
                        //if (item.TotalInvoicePrice > 0)
                        //{
                        //    List<SqlParameter> aSqlParameterList1 = new List<SqlParameter>();
                        //    aSqlParameterList1.Add(new SqlParameter("@mrrId", aResponsePk));
                        //    result = _accessManager.SaveData("SaveInvoiceJournal", aSqlParameterList1);
                        //}
                    }
                }
                return result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private bool CreateSystemJournal(int aResponsePk)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@Id", aResponsePk));
                result = _accessManager.SaveData("SaveInvoiceJournal", aParameters);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<ViewInvoiceMaster> GetGeneratedInvoices(int supplierId = 0, int invoiceId = 0, DateTime? fromDate = null, DateTime? toDate = null)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@supplierId", supplierId));
                aParameters.Add(new SqlParameter("@invoiceId", invoiceId));
                aParameters.Add(new SqlParameter("@fromdate", fromDate));
                aParameters.Add(new SqlParameter("@todate", toDate));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetGeneratedInvoiceList", aParameters);
                List<ViewInvoiceMaster> ItemDescriptionList = new List<ViewInvoiceMaster>();
                while (dr.Read())
                {
                    ViewInvoiceMaster itemDescription = new ViewInvoiceMaster();
                    itemDescription.InvoiceMasterID = Convert.ToInt32(dr["InvoiceMasterID"]);
                    itemDescription.SupplierID = Convert.ToInt32(dr["SupplierID"]);
                    itemDescription.TotalAmount = Convert.ToDecimal(dr["TotalAmount"]);
                    itemDescription.TotalQty = Convert.ToDecimal(dr["TotalQty"]);
                    itemDescription.InvoiceDate = Convert.ToDateTime(dr["InvoiceDate"]);
                    itemDescription.DueDate = Convert.ToDateTime(dr["DueDate"]);
                    itemDescription.InvoiceNo = dr["InvoiceNo"].ToString();
                    itemDescription.SupplierName = dr["SupplierName"].ToString();
                    ItemDescriptionList.Add(itemDescription);
                }
                return ItemDescriptionList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<ViewInvoiceDetails> InvoiceDetailsById(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ViewInvoiceDetails> aList = new List<ViewInvoiceDetails>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@Id", id));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetInvoiceDetailsById", aParameters);
                while (dr.Read())
                {
                    ViewInvoiceDetails aMaster = new ViewInvoiceDetails();
                    aMaster.InvoiceId = Convert.ToInt32(dr["InvoiceId"]);
                    aMaster.MRRID = Convert.ToInt32(dr["MRRID"]);
                    aMaster.MRRDate = Convert.ToDateTime(dr["MRRDate"]);
                    aMaster.ReceivedAmount = Convert.ToDecimal(dr["ReceivedAmount"]);
                    aMaster.InvoicedAmount = Convert.ToDecimal(dr["InvoicedAmount"]);
                    aMaster.PriceWithoutVat = Convert.ToDecimal(dr["PriceWithoutVat"]);
                    //  aMaster.VatPercent = Convert.ToDecimal(dr["VatPercent"]);
                    aMaster.TotalInvoicePrice = Convert.ToDecimal(dr["TotalInvoicePrice"]);
                    aMaster.ItemDescription = dr["ItemDescription"].ToString();
                    aMaster.MrrNo = dr["MrrNo"].ToString();
                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<ViewInvoiceMaster> LoadInvoiceNo()
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetInvoiceNo");
                List<ViewInvoiceMaster> ItemDescriptionList = new List<ViewInvoiceMaster>();
                while (dr.Read())
                {
                    ViewInvoiceMaster itemDescription = new ViewInvoiceMaster();
                    itemDescription.InvoiceMasterID = Convert.ToInt32(dr["InvoiceMasterID"]);
                    itemDescription.InvoiceNo = dr["InvoiceNo"].ToString();
                    ItemDescriptionList.Add(itemDescription);
                }
                return ItemDescriptionList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public DataTable GetIndivualInvoicePrintReport(int id)
        {
            try
            {
                DataTable dr = new DataTable();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@Id", id));
                dr = _accessManager.GetDataTable("GetIndivualInvoicePrintReport", parameters);
                return dr;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public DataTable GetMultipleInvoiceReport(string invoiceNo)
        {
            try
            {
                DataTable dr = new DataTable();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@Ids", invoiceNo));
                dr = _accessManager.GetDataTable("GetMultipleInvoiceReport", parameters);
                return dr;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
    }
}