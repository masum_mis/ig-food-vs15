﻿using IGFoodProject.DataManager;
using IGFoodProject.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using IGFoodProject.Models;
using System.Data;
using System.Web.Mvc;

namespace IGFoodProject.DAL
{
    public class TradePurchaseDAL
    {
        DataAccessManager accessManager = new DataAccessManager();
        public List<ViewCustomerInfo> GetCustomerList()
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ViewCustomerInfo> _List = new List<ViewCustomerInfo>();
                //List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                //aSqlParameterList.Add(new SqlParameter("@customertypeid", typeid));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetCustomerListTrade");
                while (dr.Read())
                {
                    ViewCustomerInfo aInfo = new ViewCustomerInfo();
                    aInfo.CustomerID = Convert.ToInt32(dr["CustomerID"]);
                    aInfo.CustomerName = dr["CustomerName"].ToString();
                    aInfo.CustomerCode = dr["CustomerCode"].ToString();
                    aInfo.DivisionName = dr["DivisionName"].ToString();
                    aInfo.DistrictName = dr["DistrictName"].ToString();
                    aInfo.MarketName = dr["MarketName"].ToString();
                    aInfo.CoustomerTypeName = dr["CoustomerTypeName"].ToString();
                    aInfo.PaymentType = dr["PaymentType"].ToString();
                    aInfo.CreateDate = dr["CreateDate"] as DateTime?;

                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public List<ViewProductDescription> LoadProduct()
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                //List<SqlParameter> parameters = new List<SqlParameter>();
                //parameters.Add(new SqlParameter("@locationId", locationId));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetProductName_trade");
                List<ViewProductDescription> wareHouseList = new List<ViewProductDescription>();
                while (dr.Read())
                {
                    ViewProductDescription wareHouse = new ViewProductDescription();
                    wareHouse.ProductId = Convert.ToInt32(dr["ProductId"]);
                    wareHouse.ProductName = dr["ProductName"].ToString();
                    wareHouse.TypeId = Convert.ToInt32(dr["TypeId"]);
                    wareHouseList.Add(wareHouse);
                }
                return wareHouseList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

        }
        public tbl_UOM Loadunit(int productid)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@ProductId", productid));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetUnitNameByProduct", parameters);
                tbl_UOM wareHouse = new tbl_UOM();
                while (dr.Read())
                {
                    
                    wareHouse.UOMId = Convert.ToInt32(dr["UOMId"]);
                    wareHouse.UOMName = dr["UOMName"].ToString();
                   
                }
                return wareHouse;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

        }

        public ResultResponse SaveTradePurchase(TradePurchaseMaster aMaster, string user)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                

                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@TradePurchaseMasterId", aMaster.TradePurchaseMasterId));
                aParameters.Add(new SqlParameter("@CompanyId", aMaster.CompanyId));
                aParameters.Add(new SqlParameter("@IsDeliveryCompanyOther", aMaster.IsDeliveryCompanyOther));
                

                aParameters.Add(new SqlParameter("@DelLocationId", aMaster.DelLocationId));
                aParameters.Add(new SqlParameter("@DelWareHouseID", aMaster.DelWareHouseID));

                aParameters.Add(new SqlParameter("@DeliveryOther", aMaster.DeliveryOther));
                aParameters.Add(new SqlParameter("@PurchaseDate", aMaster.PurchaseDate));

                aParameters.Add(new SqlParameter("@LocalOrSupplier", aMaster.LocalOrSupplier));
                aParameters.Add(new SqlParameter("@SupplierId", aMaster.SupplierId));
                aParameters.Add(new SqlParameter("@LocalSupplier", aMaster.LocalSupplier));
                aParameters.Add(new SqlParameter("@CommonOrFixCustomer", aMaster.CommonOrFixCustomer));
                aParameters.Add(new SqlParameter("@CustomerId", aMaster.CustomerId));
                aParameters.Add(new SqlParameter("@GrandTotalAmount", aMaster.GrandTotalAmount));
                aParameters.Add(new SqlParameter("@Discount", aMaster.Discount));
                aParameters.Add(new SqlParameter("@NetPayable", aMaster.NetPayable));
                

                aParameters.Add(new SqlParameter("@CreateBy", user));
                
                aParameters.Add(new SqlParameter("@CreateDate", System.DateTime.Now));
                aParameters.Add(new SqlParameter("@UpdateBy", user));
                aParameters.Add(new SqlParameter("@UpdateDate", System.DateTime.Now));
                
                

                aResponse.pk = accessManager.SaveDataReturnPrimaryKey("sp_SaveTradePurchaseMaster", aParameters);
                if (aResponse.pk > 0)
                {
                        aResponse.isSuccess = SaveTradePurchaseDetail(aMaster.adetail, aResponse.pk, user);                 
                }
                else
                {
                    accessManager.SqlConnectionClose(true);
                }
                return aResponse;
            }
            catch (Exception ex)
            {
                accessManager.SqlConnectionClose(true);
                throw ex;
            }

            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public bool SaveTradePurchaseDetail(List<TradePurchaseDetail> aList, int pk, string user)
        {
            try
            {
                bool result = false;
                foreach (var item in aList)
                {
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@TradePurchaseMasterId", pk));
                    aParameters.Add(new SqlParameter("@TradePurchaseDetailId", item.TradePurchaseDetailId)); 
                    aParameters.Add(new SqlParameter("@ProductId", item.ProductId));
                    aParameters.Add(new SqlParameter("@UOMId", item.UOMId));
                    aParameters.Add(new SqlParameter("@ProductQty", item.ProductQty));
                    aParameters.Add(new SqlParameter("@ProductKg", item.ProductKg));
                    aParameters.Add(new SqlParameter("@UnitPrice", item.UnitPrice));
                    aParameters.Add(new SqlParameter("@TotalPrice", item.TotalPrice)); 
                    aParameters.Add(new SqlParameter("@IsDelete", item.IsDelete));
                    result = accessManager.SaveData("sp_SaveTradePurchaseDetail", aParameters);
                    if (result == false)
                    {
                        break;
                    }

                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<ViewTradePurchaseList> GetTradePurchaseMaster(DateTime? purchasedate, int customerid = 0, int supplierid = 0, int warehouseid = 0)
        {
            try
            {

                if (purchasedate == null)
                {
                    purchasedate = System.DateTime.Now;

                }
                
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<ViewTradePurchaseList> aList = new List<ViewTradePurchaseList>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@PurchaseDate", String.Format("{0:MM/dd/yyyy}", purchasedate)));
                aParameters.Add(new SqlParameter("@SupplierId", supplierid));
                aParameters.Add(new SqlParameter("@CustomerId", customerid));
                
                aParameters.Add(new SqlParameter("@WarehouseId", warehouseid));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetTradeList", aParameters);
                while (dr.Read())
                {
                    ViewTradePurchaseList aMaster = new ViewTradePurchaseList();

                    aMaster.TradePurchaseMasterId = Convert.ToInt32(dr["TradePurchaseMasterId"]);
                    aMaster.TradeNo = dr["TradeNo"].ToString();
                    aMaster.CompanyName = dr["CompanyName"].ToString();
                    aMaster.IsDeliveryCompanyOther = dr["IsDeliveryCompanyOther"].ToString();
                    aMaster.DeliveryLocation = dr["DeliveryLocation"].ToString();
                    aMaster.deliveryWarehouse = dr["deliveryWarehouse"].ToString();

                    aMaster.DeliveryOther = dr["DeliveryOther"].ToString();
                    aMaster.PurchaseDate = dr["PurchaseDate"].ToString();
                    aMaster.LocalOrSupplier = dr["LocalOrSupplier"].ToString();
                    aMaster.SupplierName = dr["SupplierName"].ToString();
                    aMaster.LocalSupplier = dr["LocalSupplier"].ToString();

                    aMaster.CommonOrFixCustomer = dr["CommonOrFixCustomer"].ToString();
                    aMaster.CustomerName = dr["CustomerName"].ToString();
                    aMaster.GrandTotalAmount = dr["GrandTotalAmount"].ToString();
                    aMaster.Discount = dr["Discount"].ToString();
                    aMaster.NetPayable = dr["NetPayable"].ToString();
                    
                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        
       
        public TradePurchaseMaster GetTradePurchaseMaster(int id)
        {
            try
            {
                List<SqlParameter> aParameters = new List<SqlParameter>();
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                aParameters.Add(new SqlParameter("@TradePurchaseMasterId", id));
                DataTable dt = accessManager.GetDataTable("sp_GetTradePurchaseMasterByID", aParameters);
                List<TradePurchaseMaster> aList = Helper.ToListof<TradePurchaseMaster>(dt);
                return aList[0];
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public List<TradePurchaseDetail> GetTradePurchaseDetailsByMaster(int id)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@TradePurchaseMasterId", id));
                DataTable dt = accessManager.GetDataTable("sp_GetTradePurchaseDetailsByMaster", aParameters);
                List<TradePurchaseDetail> aList = Helper.ToListof<TradePurchaseDetail>(dt);
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public List<ViewTradePurchaseList> GetTradePurchaseMasterListForApprove(DateTime? purchasedate, int customerid = 0, int supplierid = 0, int warehouseid = 0)
        {
            try
            {

                if (purchasedate == null)
                {
                    purchasedate = System.DateTime.Now;

                }

                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<ViewTradePurchaseList> aList = new List<ViewTradePurchaseList>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@PurchaseDate", String.Format("{0:MM/dd/yyyy}", purchasedate)));
                aParameters.Add(new SqlParameter("@SupplierId", supplierid));
                aParameters.Add(new SqlParameter("@CustomerId", customerid));

                aParameters.Add(new SqlParameter("@WarehouseId", warehouseid));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetTradeListForApprove", aParameters);
                while (dr.Read())
                {
                    ViewTradePurchaseList aMaster = new ViewTradePurchaseList();

                    aMaster.TradePurchaseMasterId = Convert.ToInt32(dr["TradePurchaseMasterId"]);
                    aMaster.TradeNo = dr["TradeNo"].ToString();
                    aMaster.CompanyName = dr["CompanyName"].ToString();
                    aMaster.IsDeliveryCompanyOther = dr["IsDeliveryCompanyOther"].ToString();
                    aMaster.DeliveryLocation = dr["DeliveryLocation"].ToString();
                    aMaster.deliveryWarehouse = dr["deliveryWarehouse"].ToString();

                    aMaster.DeliveryOther = dr["DeliveryOther"].ToString();
                    aMaster.PurchaseDate = dr["PurchaseDate"].ToString();
                    aMaster.LocalOrSupplier = dr["LocalOrSupplier"].ToString();
                    aMaster.SupplierName = dr["SupplierName"].ToString();
                    aMaster.LocalSupplier = dr["LocalSupplier"].ToString();

                    aMaster.CommonOrFixCustomer = dr["CommonOrFixCustomer"].ToString();
                    aMaster.CustomerName = dr["CustomerName"].ToString();
                    aMaster.GrandTotalAmount = dr["GrandTotalAmount"].ToString();
                    aMaster.Discount = dr["Discount"].ToString();
                    aMaster.NetPayable = dr["NetPayable"].ToString();

                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse SaveTradePurchaseApprove(TradePurchaseMaster aMaster, string user)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);


                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@TradePurchaseMasterId", aMaster.TradePurchaseMasterId));
                aParameters.Add(new SqlParameter("@CompanyId", aMaster.CompanyId));
                aParameters.Add(new SqlParameter("@IsDeliveryCompanyOther", aMaster.IsDeliveryCompanyOther));


                aParameters.Add(new SqlParameter("@DelLocationId", aMaster.DelLocationId));
                aParameters.Add(new SqlParameter("@DelWareHouseID", aMaster.DelWareHouseID));

                aParameters.Add(new SqlParameter("@DeliveryOther", aMaster.DeliveryOther));
                aParameters.Add(new SqlParameter("@PurchaseDate", aMaster.PurchaseDate));

                aParameters.Add(new SqlParameter("@LocalOrSupplier", aMaster.LocalOrSupplier));
                aParameters.Add(new SqlParameter("@SupplierId", aMaster.SupplierId));
                aParameters.Add(new SqlParameter("@LocalSupplier", aMaster.LocalSupplier));
                aParameters.Add(new SqlParameter("@CommonOrFixCustomer", aMaster.CommonOrFixCustomer));
                aParameters.Add(new SqlParameter("@CustomerId", aMaster.CustomerId));
                aParameters.Add(new SqlParameter("@ApprovalGrandTotalAmount", aMaster.ApprovalGrandTotalAmount));
                aParameters.Add(new SqlParameter("@ApprovalDiscount", aMaster.ApprovalDiscount));
                aParameters.Add(new SqlParameter("@ApprovalNetPayable", aMaster.ApprovalNetPayable));


                aParameters.Add(new SqlParameter("@ApproveBy", user));

                aParameters.Add(new SqlParameter("@ApproveDate", System.DateTime.Now));
                aParameters.Add(new SqlParameter("@IsApprove", aMaster.IsApprove));
                



                aResponse.pk = accessManager.SaveDataReturnPrimaryKey("sp_SaveTradePurchaseMasterApprove", aParameters);
                if (aResponse.pk > 0)
                {
                    aResponse.isSuccess = SaveTradePurchaseDetailApprove(aMaster.adetail, aResponse.pk, user);
                    //if(aResponse.isSuccess==true && aMaster.IsApprove==1)
                    //{
                    //    List<SqlParameter> aParameter = new List<SqlParameter>();
                    //    aParameter.Add(new SqlParameter("@TradePurchaseMasterId", aResponse.pk));
                    //    aResponse.isSuccess = accessManager.SaveData("sp_SaveTradePurchaseStock", aParameter);
                    //}
                    
                }
                else
                {
                    accessManager.SqlConnectionClose(true);
                }
                return aResponse;
            }
            catch (Exception ex)
            {
                accessManager.SqlConnectionClose(true);
                throw ex;
            }

            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public bool SaveTradePurchaseDetailApprove(List<TradePurchaseDetail> aList, int pk, string user)
        {
            try
            {
                bool result = false;
                foreach (var item in aList)
                {
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@TradePurchaseMasterId", pk));
                    aParameters.Add(new SqlParameter("@TradePurchaseDetailId", item.TradePurchaseDetailId));
                    aParameters.Add(new SqlParameter("@ProductId", item.ProductId));
                    aParameters.Add(new SqlParameter("@UOMId", item.UOMId));
                    aParameters.Add(new SqlParameter("@ProductQty", item.ApproveQty));
                    aParameters.Add(new SqlParameter("@ProductKg", item.ApproveKg));
                    aParameters.Add(new SqlParameter("@UnitPrice", item.ApproveUnitPrice));
                    aParameters.Add(new SqlParameter("@TotalPrice", item.ApproveTotalAmount));

                    result = accessManager.SaveData("sp_SaveTradePurchaseDetailApproved", aParameters);
                    if (result == false)
                    {
                        break;
                    }

                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public DataTable TradePurchaseOrderReport(int company, int location, int warehouse, int supplierId, DateTime fromdate, DateTime todate)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@company", company));
                aParameters.Add(new SqlParameter("@location", location));
                aParameters.Add(new SqlParameter("@warehouse", warehouse));
                aParameters.Add(new SqlParameter("@supplierId", supplierId));
                aParameters.Add(new SqlParameter("@fromdate", fromdate));
                aParameters.Add(new SqlParameter("@todate", todate));
                DataTable dt = accessManager.GetDataTable("sp_Rpt_GetTradePurchaseOrderReport", aParameters);
                return dt;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
    }
}