﻿using IGFoodProject.DataManager;
using IGFoodProject.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using IGFoodProject.ViewModel;

namespace IGFoodProject.DAL
{
    public class CustomerDAL
    {
        DataAccessManager accessManager = new DataAccessManager();
        public List<CustomerType> GetCustomerTypeList()
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<CustomerType> _List = new List<CustomerType>();

                //List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetCustomerType");
                while (dr.Read())
                {
                    CustomerType aInfo = new CustomerType();
                    aInfo.CustomerTypeID = Convert.ToInt32(dr["CustomerTypeID"]);
                    aInfo.CoustomerTypeName = dr["CoustomerTypeName"].ToString();
                    aInfo.IsActive = Convert.ToBoolean(dr["IsActive"]);
                   

                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        
        public List<WareHouse> GetWareHousesByType(int warehouseTypeId)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<WareHouse> _List = new List<WareHouse>();
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@warehouseTypeId", warehouseTypeId));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetWarehouseByType", aSqlParameterList);
                while (dr.Read())
                {
                    WareHouse aInfo = new WareHouse();
                    aInfo.WareHouseId = Convert.ToInt32(dr["WareHouseId"]);
                    aInfo.WareHouseName = dr["WareHouseName"].ToString();
                    aInfo.WarehouseTypeId = DBNull.Value.Equals(dr["WarehouseTypeId"]) ? 0 : Convert.ToInt32(dr["WarehouseTypeId"]);
                    aInfo.LocationId = DBNull.Value.Equals(dr["LocationId"]) ? 0 : Convert.ToInt32(dr["LocationId"]);

                    _List.Add(aInfo);
                }
                return _List;

            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public List<WareHouse> GetWareHousesByType_OtherPurchase(int warehouseTypeId)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<WareHouse> _List = new List<WareHouse>();
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
               // aSqlParameterList.Add(new SqlParameter("@warehouseTypeId", warehouseTypeId));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetWarehouseByType_OtherPurchase");
                while (dr.Read())
                {
                    WareHouse aInfo = new WareHouse();
                    aInfo.WareHouseId = Convert.ToInt32(dr["WareHouseId"]);
                    aInfo.WareHouseName = dr["WareHouseName"].ToString();
                    aInfo.WarehouseTypeId = DBNull.Value.Equals(dr["WarehouseTypeId"]) ? 0 : Convert.ToInt32(dr["WarehouseTypeId"]);
                    aInfo.LocationId = DBNull.Value.Equals(dr["LocationId"]) ? 0 : Convert.ToInt32(dr["LocationId"]);

                    _List.Add(aInfo);
                }
                return _List;

            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public List<IssueRequisition> GetProjectInfo()
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<IssueRequisition> _List = new List<IssueRequisition>();

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetProjectInfo");
                while (dr.Read())
                {
                    IssueRequisition aInfo = new IssueRequisition();
                    aInfo.ProjectId = Convert.ToInt32(dr["ProjectId"]);
                    aInfo.ProjectName = dr["ProjectName"].ToString();
                    _List.Add(aInfo);
                }
                return _List;

            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public List<WareHouse> GetWarehouseInfo(int warehouseTypeId, string UserId)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<WareHouse> _List = new List<WareHouse>();
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@warehouseTypeId", warehouseTypeId));
                aSqlParameterList.Add(new SqlParameter("@UserId", UserId));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetWarehouseInfoByType", aSqlParameterList);
                while (dr.Read())
                {
                    WareHouse aInfo = new WareHouse();
                    aInfo.WareHouseId = Convert.ToInt32(dr["WareHouseId"]);
                    aInfo.WareHouseName = dr["WareHouseName"].ToString();
                    aInfo.WarehouseTypeId = DBNull.Value.Equals(dr["WarehouseTypeId"]) ? 0 : Convert.ToInt32(dr["WarehouseTypeId"]);
                    aInfo.LocationId = DBNull.Value.Equals(dr["LocationId"]) ? 0 : Convert.ToInt32(dr["LocationId"]);

                    _List.Add(aInfo);
                }
                return _List;

            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public int CheckProduct(string group)
        {
            int pgroup = 0;
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@group", group));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_CheckProduct", aSqlParameterList);

                while (dr.Read())
                {
                    pgroup = Convert.ToInt32(dr["pgroup"]);
                }
            }
            catch (Exception exception)
            {
                //accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
            return pgroup;
        }
        

        public dynamic GetCustomerById( int id)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@CustomerID", id));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetCustomer", aSqlParameterList);
                tbl_CustomerInformation aInfo = new tbl_CustomerInformation();
                while (dr.Read())
                {
                    aInfo.CustomerID = DBNull.Value.Equals(dr["CustomerID"])?0:Convert.ToInt32(dr["CustomerID"]);
                    aInfo.CustomerCode = DBNull.Value.Equals(dr["CustomerCode"])?"":dr["CustomerCode"].ToString();
                    aInfo.CustomerName = DBNull.Value.Equals(dr["CustomerName"])?"": dr["CustomerName"].ToString();
                    aInfo.CustomerShortName = DBNull.Value.Equals(dr["CustomerShortName"])?"": dr["CustomerShortName"].ToString();
                    aInfo.FatherName = DBNull.Value.Equals(dr["FatherName"])?"": dr["FatherName"].ToString();
                    aInfo.MotherName = DBNull.Value.Equals(dr["MotherName"])?"": dr["MotherName"].ToString();
                    aInfo.DateOfBirth = DBNull.Value.Equals(dr["DateOfBirth"]) ? null  : dr["DateOfBirth"] as DateTime?;
                    aInfo.PersonalAddress = DBNull.Value.Equals(dr["PersonalAddress"])?"": dr["PersonalAddress"].ToString();
                    aInfo.DistrictId = Convert.ToInt32(dr["DistrictId"]);
                    aInfo.DivisionId = Convert.ToInt32(dr["DivisionId"]);
                    aInfo.AreaId = Convert.ToInt32(dr["AreaId"]);
                    aInfo.TerritoryId = Convert.ToInt32(dr["TerritoryId"]);
                    aInfo.MarketId = Convert.ToInt32(dr["MarketId"]);
                    aInfo.ThanaId = Convert.ToInt32(dr["ThanaId"]);
                    aInfo.PaymentType = Convert.ToInt32(dr["PaymentType"]);
                    aInfo.CustomerType = Convert.ToInt32(dr["CustomerType"]);
                    aInfo.PaymentType = Convert.ToInt32(dr["PaymentType"]);

                   
                    aInfo.PersonalContact = DBNull.Value.Equals(dr["PersonalContact"])?"":dr["PersonalContact"].ToString();
                    aInfo.NID = DBNull.Value.Equals(dr["NID"])?"":dr["NID"].ToString();
                    aInfo.TradeLicence = DBNull.Value.Equals(dr["TradeLicence"])?"":dr["TradeLicence"].ToString();
                    aInfo.TIN = DBNull.Value.Equals(dr["TIN"])?"":dr["TIN"].ToString();
                    aInfo.GSTNo = DBNull.Value.Equals(dr["GSTNo"])?"": dr["GSTNo"].ToString();
                    aInfo.IsContractor = DBNull.Value.Equals(dr["IsContractor"])? false: Convert.ToBoolean(dr["IsContractor"]);
                    aInfo.CustomerCurrency = DBNull.Value.Equals(dr["CustomerCurrency"])?"":dr["CustomerCurrency"].ToString();
                    
                    aInfo.CreateDate = DBNull.Value.Equals(dr["CreateDate"])? (DateTime?)null: Convert.ToDateTime(dr["CreateDate"]);
                    aInfo.CreateBy = DBNull.Value.Equals(dr["CreateBy"])?"": dr["CreateBy"].ToString();
                    aInfo.UpdateBy = DBNull.Value.Equals(dr["UpdateBy"])?"": dr["UpdateBy"].ToString();
                    aInfo.UpdateDate = DBNull.Value.Equals(dr["UpdateDate"])?(DateTime?)null: Convert.ToDateTime(dr["UpdateDate"]);
                    aInfo.IsRetention = DBNull.Value.Equals(dr["IsRetention"])?false: Convert.ToBoolean(dr["IsRetention"]);
                    aInfo.TSRelease = DBNull.Value.Equals(dr["TSRelease"])?false: Convert.ToBoolean(dr["TSRelease"]);
                    aInfo.GLAccountGroup = DBNull.Value.Equals(dr["GLAccountGroup"])? (int?)null: Convert.ToInt32(dr["GLAccountGroup"]);
                    aInfo.Phone = DBNull.Value.Equals(dr["Phone"])?"": dr["Phone"].ToString();
                    aInfo.SecondaryPhoneNumber = DBNull.Value.Equals(dr["SecondaryPhoneNumber"])?"": dr["SecondaryPhoneNumber"].ToString();
                    aInfo.FaxNumber = DBNull.Value.Equals(dr["FaxNumber"])?"": dr["FaxNumber"].ToString();
                    aInfo.Email = DBNull.Value.Equals(dr["Email"])?"": dr["Email"].ToString();
                    aInfo.DiscountPercent = DBNull.Value.Equals(dr["DiscountPercent"])?0: Convert.ToInt32(dr["DiscountPercent"]);
                    
                    aInfo.CreditStatus = DBNull.Value.Equals(dr["CreditStatus"])?"": dr["CreditStatus"].ToString();
                    aInfo.RetentionPriority = DBNull.Value.Equals(dr["RetentionPriority"])?"": dr["RetentionPriority"].ToString();
                    aInfo.DefaultInventoryLocation = DBNull.Value.Equals(dr["DefaultInventoryLocation"])?"": dr["DefaultInventoryLocation"].ToString();
                    aInfo.DefaultShippingCompany = DBNull.Value.Equals(dr["DefaultShippingCompany"])?0: Convert.ToInt32(dr["DefaultShippingCompany"]);

                    aInfo.CustomerWarehouseName = dr["CustomerWarehouseName"].ToString();
                    aInfo.WareHouseArea = dr["WareHouseArea"].ToString();
                    aInfo.WareHouseAddress = dr["WareHouseAddress"].ToString();
                    aInfo.IsCredit= DBNull.Value.Equals(dr["IsCredit"]) ? false : Convert.ToBoolean(dr["IsCredit"]);
                    aInfo.CostCenterId = DBNull.Value.Equals(dr["CostCenterId"]) ? 0 : Convert.ToInt32(dr["CostCenterId"]);



                }
                return aInfo;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public ViewCustomerInfo GetCustomerViewById(int id)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                ViewCustomerInfo aInfo = new ViewCustomerInfo();
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_CustomerDetailsViewById", aParameters);
                while (dr.Read())
                {
                    

                    aInfo.CustomerName = dr["CustomerName"].ToString();
                    aInfo.CustomerCode = dr["CustomerCode"].ToString();
                    aInfo.DivisionName = dr["DivisionName"].ToString();
                    aInfo.AreaName = dr["AreaName"].ToString();
                    aInfo.TerritoryName = dr["TerritoryName"].ToString();
                    aInfo.DefaultInventoryLocation = dr["DefaultInventoryLocation"].ToString();
                    aInfo.PersonalAddress = dr["PersonalAddress"].ToString();
                    aInfo.CustomerTypeID =Convert.ToInt32(dr["CustomerType"]);
                    aInfo.PrimaryPhone = dr["Phone"].ToString();
                    aInfo.SecondaryPhone = dr["SecondaryPhoneNumber"].ToString();
                    aInfo.CreditType = dr["CreditType"].ToString();
                }

                return aInfo;

            }
            catch (Exception   ex)
            {

                throw ex;
            }

            finally
            {
                accessManager.SqlConnectionClose();
            }
        }


        public ViewCustomerInfo GetCreditAndLedgerBalance(int id, DateTime orderDate, int masterId) 
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CustomerId", id));
                aParameters.Add(new SqlParameter("@orderDate", orderDate));
                aParameters.Add(new SqlParameter("@masterId", masterId));


                ViewCustomerInfo aInfo = new ViewCustomerInfo();
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetCreditAndLedgerBalance", aParameters);
                while (dr.Read())
                {
                    aInfo.LadgerBalance = DBNull.Value.Equals(dr["LadgerBalance"]) ? default(decimal) : Convert.ToDecimal(dr["LadgerBalance"]);
                    aInfo.CreditAmount = DBNull.Value.Equals(dr["CreditAmount"]) ? default(decimal) : Convert.ToDecimal(dr["CreditAmount"]);
                    aInfo.OpeningCredit = DBNull.Value.Equals(dr["OpeningCredit"]) ? default(decimal) : Convert.ToDecimal(dr["OpeningCredit"]); 

                }

                return aInfo;

            }
            catch (Exception ex)
            {

                throw ex;
            }

            finally
            {
                accessManager.SqlConnectionClose();
            }
        }


        public Product GetProductForEdit(int id)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@productid", id));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetProductByID", aParameters);
                Product aDetailsView = new Product();
                while (dr.Read())
                {


                    aDetailsView.ProductId = (int)dr["ProductId"];
                    aDetailsView.GroupId = (int)dr["GroupId"];
                    aDetailsView.TypeId = (int)dr["TypeId"];
                    aDetailsView.CategoryId = (int)dr["CategoryId"];
                    aDetailsView.BrandId = (int)dr["BrandId"];
                    aDetailsView.PackSizeId = (int)dr["PackSizeId"];
                    aDetailsView.UOMId = (int)dr["UOMId"];
                    aDetailsView.ProductNo = dr["ProductNo"].ToString();
                    aDetailsView.ProductName = dr["ProductName"].ToString();
                    aDetailsView.ProductDescription = dr["ProductDescription"].ToString();
                    aDetailsView.IsActive = (bool)dr["IsActive"];


                }
                return aDetailsView;

            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public bool DeleteBrand(int id)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@BrandId", id));

                return accessManager.SaveData("sp_DeleteBrand", aSqlParameterList);
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public List<tbl_ProductCategory> GetCategoryListByType(int typeid)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<tbl_ProductCategory> _List = new List<tbl_ProductCategory>();

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@typeid", typeid));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetProductCategoryByTpeID", aSqlParameterList);
                while (dr.Read())
                {
                    tbl_ProductCategory aInfo = new tbl_ProductCategory();

                    aInfo.CategoryId = (int)dr["CategoryId"];

                    aInfo.CategoryName = dr["CategoryName"].ToString();



                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<tbl_ProductBrand> GetBrandList()
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<tbl_ProductBrand> _List = new List<tbl_ProductBrand>();

                //List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                //aSqlParameterList.Add(new SqlParameter("@typeid", typeid));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetProductBrandList");
                while (dr.Read())
                {
                    tbl_ProductBrand aInfo = new tbl_ProductBrand();

                    aInfo.BrandId = (int)dr["BrandId"];

                    aInfo.BrandName = dr["BrandName"].ToString();



                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<tbl_ProductPackSize> GetPackSizeList()
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<tbl_ProductPackSize> _List = new List<tbl_ProductPackSize>();

                //List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                //aSqlParameterList.Add(new SqlParameter("@typeid", typeid));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetPackSizeList");
                while (dr.Read())
                {
                    tbl_ProductPackSize aInfo = new tbl_ProductPackSize();

                    aInfo.PackSizeId = (int)dr["PackSizeId"];

                    aInfo.PackSizeName = dr["PackSizeName"].ToString();



                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }


        public List<tbl_UOM> GetUOMList()
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<tbl_UOM> _List = new List<tbl_UOM>();

                //List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                //aSqlParameterList.Add(new SqlParameter("@typeid", typeid));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetUOMList");
                while (dr.Read())
                {
                    tbl_UOM aInfo = new tbl_UOM();

                    aInfo.UOMId = (int)dr["UOMId"];

                    aInfo.UOMName = dr["UOMName"].ToString();



                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public string GetProductNo(int id)
        {
            string productno = string.Empty;
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ProductID", id));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetProductNo", aParameters);

                while (dr.Read())
                {


                    productno = dr["ProductNo"].ToString();



                }
                return productno;

            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public bool DeleteCustomer(int id)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@CustomerId", id));

                return accessManager.SaveData("sp_DeleteCustomer", aSqlParameterList);
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }


        public List<tbl_CustomerInformation> GetDealer()
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_Customer_Dealer");
                List<tbl_CustomerInformation> aList = new List<tbl_CustomerInformation>();
                while (dr.Read())
                {
                    tbl_CustomerInformation aCustomerInformation = new tbl_CustomerInformation();
                    aCustomerInformation.CustomerCode = dr["CustomerCode"].ToString();
                    aCustomerInformation.CustomerName = dr["CustomerName"].ToString();
                    aCustomerInformation.CustomerID = Convert.ToInt32(dr["CustomerID"]);
                    aList.Add(aCustomerInformation);

                }

                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<tbl_CustomerInformation> GetFilteredCustomerList(int divisionId=0, int areaId=0, int territoryId=0, int customerTypeId=0)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<tbl_CustomerInformation> _List = new List<tbl_CustomerInformation>();

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@divisionId", divisionId));
                aSqlParameterList.Add(new SqlParameter("@areaId", areaId));
                aSqlParameterList.Add(new SqlParameter("@territoryId", territoryId));
                aSqlParameterList.Add(new SqlParameter("@customerTypeId", customerTypeId));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_Filtered_Customers", aSqlParameterList);
                while (dr.Read())
                {
                    tbl_CustomerInformation aInfo = new tbl_CustomerInformation();
                    aInfo.CustomerID = Convert.ToInt32(dr["CustomerID"]);
                    aInfo.CustomerName = dr["CustomerName"].ToString();
                    aInfo.CustomerCode = dr["CustomerCode"].ToString();
                    aInfo.CustomerTypeName = dr["CoustomerTypeName"].ToString();
                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public ViewCustomerInfo GetCustomerNameEmpId(int empNo)
        {
            try
            {
                //string CustomerInfo = "";
                ViewCustomerInfo aInfo = new ViewCustomerInfo();
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@empNo", empNo));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_EmpByEmpNo", aParameters);
                while (dr.Read())
                {
                    aInfo.CustomerName = dr["EMPID"] + ":" + dr["EmpName"] + ":" + dr["Designation"];
                    aInfo.FatherName = dr["FatherName"].ToString();
                    aInfo.MotherName = dr["MotherName"].ToString();
                    aInfo.MobileNo = dr["PersonalMobileNo"].ToString();
                    aInfo.NationalIDNo = dr["NationalIDNo"].ToString();
                }
                return aInfo;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<ViewCustomerInfo> GetCustomerList(int typeid)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ViewCustomerInfo> _List = new List<ViewCustomerInfo>();
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@customertypeid", typeid));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetCustomerList", aSqlParameterList);
                while (dr.Read())
                {
                    ViewCustomerInfo aInfo = new ViewCustomerInfo();
                    aInfo.CustomerID = Convert.ToInt32(dr["CustomerID"]);
                    aInfo.CustomerName = dr["CustomerName"].ToString();
                    aInfo.CustomerCode = dr["CustomerCode"].ToString();
                    aInfo.DivisionName = dr["DivisionName"].ToString();
                    aInfo.DistrictName = dr["DistrictName"].ToString();
                    aInfo.MarketName = dr["MarketName"].ToString();
                    aInfo.CoustomerTypeName = dr["CoustomerTypeName"].ToString();
                    aInfo.PaymentType = dr["PaymentType"].ToString();
                    aInfo.CreateDate = dr["CreateDate"] as DateTime?;
                    aInfo.IsEcommerce = Convert.ToBoolean(dr["IsEcommerce"]);
                    aInfo.IsCredit = Convert.ToBoolean(dr["IsCredit"]);
                    aInfo.CostCenterName = dr["CostCenterName"].ToString();
                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse SaveCustomer(tbl_CustomerInformation aMaster)
        {

            try
            {

                ResultResponse aResponse = new ResultResponse();
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@CustomerID", aMaster.CustomerID));
                aSqlParameterList.Add(new SqlParameter("@CustomerName", aMaster.CustomerName));
                aSqlParameterList.Add(new SqlParameter("@CustomerShortName", aMaster.CustomerShortName));
                aSqlParameterList.Add(new SqlParameter("@FatherName", aMaster.FatherName));
                aSqlParameterList.Add(new SqlParameter("@MotherName", aMaster.MotherName));
                aSqlParameterList.Add(new SqlParameter("@DateOfBirth", aMaster.DateOfBirth));
                aSqlParameterList.Add(new SqlParameter("@PersonalAddress", aMaster.PersonalAddress));

                aSqlParameterList.Add(new SqlParameter("@NID", aMaster.NID));
                aSqlParameterList.Add(new SqlParameter("@TradeLicence", aMaster.TradeLicence));
                aSqlParameterList.Add(new SqlParameter("@TIN", aMaster.TIN));
                aSqlParameterList.Add(new SqlParameter("@DivisionId", aMaster.DivisionId));
                aSqlParameterList.Add(new SqlParameter("@AreaId", aMaster.AreaId));
                aSqlParameterList.Add(new SqlParameter("@TerritoryId", aMaster.TerritoryId));
                aSqlParameterList.Add(new SqlParameter("@MarketId", aMaster.MarketId));
                aSqlParameterList.Add(new SqlParameter("@DistrictId", aMaster.DistrictId));
                aSqlParameterList.Add(new SqlParameter("@ThanaId", aMaster.ThanaId));
                aSqlParameterList.Add(new SqlParameter("@HasDealerId", aMaster.HasDealerId));


                aSqlParameterList.Add(new SqlParameter("@PersonalContact", aMaster.PersonalContact));
                aSqlParameterList.Add(new SqlParameter("@GSTNo", aMaster.GSTNo));
                aSqlParameterList.Add(new SqlParameter("@IsContractor", aMaster.IsContractor));
                aSqlParameterList.Add(new SqlParameter("@CustomerCurrency", aMaster.CustomerCurrency));

                aSqlParameterList.Add(new SqlParameter("@CustomerType", aMaster.CustomerType));
                aSqlParameterList.Add(new SqlParameter("@CostCenterId", aMaster.CostCenterId));
                aSqlParameterList.Add(new SqlParameter("@IsRetention", aMaster.IsRetention));
                aSqlParameterList.Add(new SqlParameter("@TSRelease", aMaster.TSRelease));
                aSqlParameterList.Add(new SqlParameter("@IsCredit", aMaster.IsCredit));
                aSqlParameterList.Add(new SqlParameter("@GLAccountGroup", aMaster.GLAccountGroup));
                aSqlParameterList.Add(new SqlParameter("@Phone", aMaster.Phone));
                aSqlParameterList.Add(new SqlParameter("@SecondaryPhoneNumber", aMaster.SecondaryPhoneNumber));
                aSqlParameterList.Add(new SqlParameter("@FaxNumber", aMaster.FaxNumber));
                aSqlParameterList.Add(new SqlParameter("@Email", aMaster.Email));
                aSqlParameterList.Add(new SqlParameter("@DiscountPercent", aMaster.DiscountPercent));
                aSqlParameterList.Add(new SqlParameter("@PaymentType", aMaster.PaymentType));
                aSqlParameterList.Add(new SqlParameter("@CreditStatus", aMaster.CreditStatus));
                aSqlParameterList.Add(new SqlParameter("@RetentionPriority", aMaster.RetentionPriority));
                aSqlParameterList.Add(new SqlParameter("@DefaultInventoryLocation", aMaster.DefaultInventoryLocation));
                aSqlParameterList.Add(new SqlParameter("@DefaultShippingCompany", aMaster.DefaultShippingCompany));

                aSqlParameterList.Add(new SqlParameter("@CreateDate", DateTime.Now));
                aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@UpdateDate", DateTime.Now));
                aSqlParameterList.Add(new SqlParameter("@UpdateBy", aMaster.UpdateBy));


                aResponse.pk = accessManager.SaveDataReturnPrimaryKey("sp_SaveCustomer", aSqlParameterList);
                if (aResponse.pk == 0)
                {
                    aResponse.isSuccess = false;
                    accessManager.SqlConnectionClose(true);
                }

                if (aResponse.pk > 0 && aMaster.CustomerWareHouse != null)
                {
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@CustomerID", aResponse.pk));
                    aParameters.Add(new SqlParameter("@CustomerWarehouseName",
                    aMaster.CustomerWareHouse.CustomerWarehouseName));
                    aParameters.Add(new SqlParameter("@WareHouseArea", aMaster.CustomerWareHouse.WareHouseArea));
                    aParameters.Add(new SqlParameter("@WareHouseAddress", aMaster.CustomerWareHouse.WareHouseAddress));

                    aResponse.isSuccess = accessManager.SaveData("sp_Save_CustomerWareHouse", aParameters);

                    if (aResponse.isSuccess == false)
                    {
                        accessManager.SqlConnectionClose(true);
                    }
                }
                else
                {
                    aResponse.isSuccess = true;
                }

                return aResponse;
            }
            catch (Exception exception)
            {
                accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

        }



      

    }
}
