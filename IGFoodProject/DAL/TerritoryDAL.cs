﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using IGFoodProject.DataManager;
using IGFoodProject.Models;

namespace IGFoodProject.DAL
{
    public class TerritoryDAL 
    {
        private  DataAccessManager _accessManager= new DataAccessManager();
        public dynamic SaveTerritory(Territory entity)
        {
                try
                {
                    _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                    ResultResponse aResponse = new ResultResponse();
                    List<SqlParameter> aParameters = new List<SqlParameter>();

                    aParameters.Add(new SqlParameter("@TerritoryId", entity.TerritoryId));
                    aParameters.Add(new SqlParameter("@AreaId", entity.AreaId));
                    aParameters.Add(new SqlParameter("@TerritoryName", entity.TerritoryName));
                    aParameters.Add(new SqlParameter("@CreateBy", entity.CreateBy));
                    aParameters.Add(new SqlParameter("@CreateDate", entity.CreateDate));
                    
                    aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_SaveOrUpdateTerritory", aParameters);
                    

                    return aResponse;
                }
                catch (Exception ex)
                {
                    _accessManager.SqlConnectionClose(true);
                    throw ex;
                }
                finally
                {
                    _accessManager.SqlConnectionClose();
                }
        }
        public dynamic GetTerritory(int id)
        {
            try
            {
                Territory aTerritory = new Territory();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameters = new List<SqlParameter>();
                aSqlParameters.Add(new SqlParameter("@TerritoryId", id));
                DataTable dr = _accessManager.GetDataTable("sp_GetTerritory", aSqlParameters);
                _accessManager.SqlConnectionClose();
                if (dr.Rows.Count > 0)
                {
                     aTerritory = Helper.ToListof<Territory>(dr)[0];
                }
                
                
                return aTerritory;

            }
            catch (Exception exception)
            {


                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public dynamic GetAllTerritory()
        {
            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameters = new List<SqlParameter>();
                
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetTerritoryList");
                
                List<Territory> TerritoryList = new List<Territory>();
                while (dr.Read())
                {
                    Territory tr = new Territory();
                    tr.TerritoryId = Convert.ToInt32(dr["TerritoryId"]);
                    tr.AreaName = Convert.ToString(dr["AreaName"]);
                    tr.TerritoryName = Convert.ToString(dr["TerritoryName"]);
                    tr.CreateBy = Convert.ToString(dr["CreateBy"]);
                    tr.CreateDate = Convert.ToDateTime(dr["CreateDate"]);
          

                    TerritoryList.Add(tr);
                }
                return TerritoryList;

                _accessManager.SqlConnectionClose();

            }
            catch (Exception exception)
            {


                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
    }
}