﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using IGFoodProject.DataManager;
using IGFoodProject.Models;
using IGFoodProject.Models.Accounts;
using IGFoodProject.ViewModel;
namespace IGFoodProject.DAL
{
    public class OthersMrrDal
    {
        DataAccessManager _accessManager = new DataAccessManager();
        public List<ViewOthersMrr> PoList(DateTime? fromdate, DateTime? todate, int poNo=0, int supplierId=0, int itemId=0)
        {
            try
            {
                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-7);
                }
                if (todate == null)
                {
                    todate = System.DateTime.Now;
                }
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ViewOthersMrr> aList = new List<ViewOthersMrr>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@PoNo",poNo ));
                aParameters.Add(new SqlParameter("@SupplierId", supplierId ));
                aParameters.Add(new SqlParameter("@ItemId", itemId ));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetOthersMrrList", aParameters);
                while (dr.Read())
                {
                    ViewOthersMrr aMaster = new ViewOthersMrr();
                    aMaster.PurchaseOrderOtherID = Convert.ToInt32(dr["PurchaseOrderOtherID"]);
                    aMaster.PurchaseOrderNo = dr["PurchaseOrderNo"].ToString();
                    aMaster.OrderDate = Convert.ToDateTime(dr["OrderDate"]);
                    aMaster.ExpectedDeliveryDate = Convert.ToDateTime(dr["ExpectedDeliveryDate"]);
                    aMaster.SupplierName = dr["SupplierName"].ToString();
                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.CurrencyName = dr["CurrencyCode"].ToString();
                    aMaster.OrderTotalAmt = Convert.ToDecimal(dr["TotalAmount"]);
                    aMaster.LastRcvDate = (dr["LastRcvDate"] == DBNull.Value) ? (DateTime?)null : ((DateTime)dr["LastRcvDate"]);
                    //aMaster.LastRcvDate = DBNull.Value.Equals(dr["LastRcvDate"]) ? '':
                    //   aMaster.LastRcvDate = Convert.ToDateTime(dr["LastRcvDate"]);
                    aMaster.TotalQtyRcv = Convert.ToDecimal(dr["TotalQtyRcv"]);
                    aMaster.TotalAmountRcv = Convert.ToDecimal(dr["TotalAmountRcv"]);
                    aMaster.RestAmount = Convert.ToDecimal(dr["RestAmount"]);
                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<ViewOthersMrr> LoadPONoForMRR()
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetPONoForMrr");
                List<ViewOthersMrr> ItemDescriptionList = new List<ViewOthersMrr>();
                while (dr.Read())
                {
                    ViewOthersMrr itemDescription = new ViewOthersMrr();
                    itemDescription.PurchaseOrderOtherID = Convert.ToInt32(dr["PurchaseOrderOtherID"]);
                    itemDescription.PurchaseOrderNo = dr["PurchaseOrderNo"].ToString();
                    ItemDescriptionList.Add(itemDescription);
                }
                return ItemDescriptionList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<ViewOthersMrrDetails> PoDetailsForMrr(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ViewOthersMrrDetails> aList = new List<ViewOthersMrrDetails>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@Id", id));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetPODetailsByPoIdForMrr", aParameters);
                while (dr.Read())
                {
                    ViewOthersMrrDetails aMaster = new ViewOthersMrrDetails();
                    aMaster.ItemCode = dr["ItemCode"].ToString();
                    aMaster.ItemName = dr["ItemName"].ToString();
                    aMaster.UOMName = dr["UOMName"].ToString();
                    aMaster.PurchaseQty = Convert.ToDecimal(dr["PurchaseQty"]);
                    aMaster.MRRRemainingQty = DBNull.Value.Equals(dr["MRRRemainingQty"]) ? 0 : Convert.ToDecimal(dr["MRRRemainingQty"]);
                    aMaster.UnitPrice =  DBNull.Value.Equals(dr["UnitPrice"]) ? 0 : Convert.ToDecimal(dr["UnitPrice"]);
                    aMaster.ReceivedQty =DBNull.Value.Equals(dr["ReceivedQty"]) ? 0 : Convert.ToDecimal(dr["ReceivedQty"]);
                    aMaster.ItemId = Convert.ToInt32(dr["ItemId"]);
                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public ViewOthersMrr GetPoInfoByMasterById(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                ViewOthersMrr aMaster = new ViewOthersMrr();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@Id", id));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetOthersPOInfoById", aParameters);
                while (dr.Read())
                {
                    aMaster.PurchaseOrderOtherID = Convert.ToInt32(dr["PurchaseOrderOtherID"]);
                    aMaster.SupplierID = Convert.ToInt32(dr["SupplierID"]);
                    aMaster.DelLocationId = Convert.ToInt32(dr["DelLocationId"]);
                    aMaster.PurchaseOrderNo = dr["PurchaseOrderNo"].ToString();
                    aMaster.OrderDate = Convert.ToDateTime(dr["OrderDate"]);
                    aMaster.SupplierName = dr["SupplierName"].ToString();
                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.CurrencyName = dr["CurrencyCode"].ToString();
                    aMaster.Comments = dr["Comments"].ToString();
                    aMaster.SupplierRef = dr["SupplierRef"].ToString();
                    aMaster.DeliveryTo = dr["DeliveryTo"].ToString();
                    aMaster.SpecialInstruction = dr["SpecialInstruction"].ToString();
                    //aMaster.LastRcvDate = Convert.ToDateTime(dr["LastRcvDate"]);
                    //aMaster.TotalQtyRcv = Convert.ToDecimal(dr["TotalQtyRcv"]);
                    //aMaster.TotalAmountRcv = Convert.ToDecimal(dr["TotalAmountRcv"]);
                }
                return aMaster;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<tbl_ProductPackSize> LoadPackSize()
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetAllPackSize");
                List<tbl_ProductPackSize> ItemDescriptionList = new List<tbl_ProductPackSize>();
                while (dr.Read())
                {
                    tbl_ProductPackSize itemDescription = new tbl_ProductPackSize();
                    itemDescription.PackSizeId = Convert.ToInt32(dr["PackSizeId"]);
                    itemDescription.PackSizeName = dr["PackSizeName"].ToString();
                   // itemDescription.PackWeight = Convert.ToDecimal(dr["PackWeight"]);
                    ItemDescriptionList.Add(itemDescription);
                }
                return ItemDescriptionList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public ResultResponse SaveOthersMrr(ViewOthersMrr aMaster, string user)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@MRRrOtherID", aMaster.MRRrOtherID));
                aParameters.Add(new SqlParameter("@PurchaseOrderId", aMaster.PurchaseOrderOtherID));
                aParameters.Add(new SqlParameter("@MRRDate", aMaster.MrrDate));
                aParameters.Add(new SqlParameter("@ChallanDate", aMaster.ChallanDate));
                aParameters.Add(new SqlParameter("@SupplierID", aMaster.SupplierID));
                aParameters.Add(new SqlParameter("@ChallanNo", aMaster.ChallanNo));
                aParameters.Add(new SqlParameter("@Labesult", aMaster.LabResult));
                aParameters.Add(new SqlParameter("@VehicleNo", aMaster.VehicleNo));
                aParameters.Add(new SqlParameter("@VehicleInWeight", aMaster.VehicleInWeight));
                aParameters.Add(new SqlParameter("@VehicleOutWeight", aMaster.VehicleOutWeight));
                aParameters.Add(new SqlParameter("@SpecialInstruction", aMaster.SpecialInstruction));
                aParameters.Add(new SqlParameter("@LaborCost", aMaster.LaborCost));
                aParameters.Add(new SqlParameter("@TransportCost", aMaster.TransportCost));
                aParameters.Add(new SqlParameter("@TotalQty", aMaster.ViewOthersMrrDetailsList.Sum(n => n.MrrQty)));
                aParameters.Add(new SqlParameter("@CreateBy", user));
                aParameters.Add(new SqlParameter("@IsApprove", aMaster.IsApprove));
                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("SaveOtherMrr", aParameters);
                if (aResponse.pk > 0)
                {
                    if (aMaster.ViewOthersMrrDetailsList.Count > 0)
                    {
                        aResponse.isSuccess = SaveMrrDetails(aMaster.ViewOthersMrrDetailsList, aResponse.pk, aMaster.PurchaseOrderOtherID);

                    }
                    if (aMaster.IsApprove == true)
                    {
                        aResponse.isSuccess = SaveOtherStockIn(aResponse.pk);
                     aResponse.isSuccess = CreateSystemJournal(aResponse.pk);
                       
                    }

                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                }
                else
                {
                    _accessManager.SqlConnectionClose(true);
                }
                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public bool SaveMrrDetails(List<ViewOthersMrrDetails> aMaster, int mrrMasterId,int poId)
        {
            try
            {
                bool result = false;
                foreach (var item in aMaster)
                {
                    if (item.MrrQty > 0)
                    {
                        List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                        ResultResponse aResponse = new ResultResponse();
                        aSqlParameterList.Add(new SqlParameter("@MRRID", mrrMasterId));
                        aSqlParameterList.Add(new SqlParameter("@DetailsId", item.MRRDetailID));
                        aSqlParameterList.Add(new SqlParameter("@ItemId", item.ItemId));
                        aSqlParameterList.Add(new SqlParameter("@PurchaseQty", item.PurchaseQty));
                        aSqlParameterList.Add(new SqlParameter("@MRRQty", item.MrrQty));
                        aSqlParameterList.Add(new SqlParameter("@ChallanQty", item.ChallanQty));
                        aSqlParameterList.Add(new SqlParameter("@UnitPrice", item.UnitPrice));
                        aSqlParameterList.Add(new SqlParameter("@PackSizeId", item.PackSizeId));
                        aSqlParameterList.Add(new SqlParameter("@NoOfPack", item.NoOfPack));
                        aSqlParameterList.Add(new SqlParameter("@MfgDate", item.MFGDate));
                        aSqlParameterList.Add(new SqlParameter("@ExpireyDate", item.ExpireyDate));
                        aSqlParameterList.Add(new SqlParameter("@PoId", poId));
                        aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("SaveMrrDetails", aSqlParameterList);
                    }
                    //else
                    //{
                    //    break;
                    //}
                }
                return result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool CreateSystemJournal(int MrrMasterId)
        {
            //test inheritance
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@MRRMasterId", MrrMasterId));
                result = _accessManager.SaveData("SaveOtherMrrJournal", aParameters);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool SaveOtherStockIn(int MrrMasterId)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@MrrMasterId", MrrMasterId));
                result = _accessManager.SaveData("SaveOthersMrrStockInMasterDetails", aParameters);
             //   return result;
                return result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<ViewOthersMrr> LoadMrrNo()
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetMrrNo");
                List<ViewOthersMrr> ItemDescriptionList = new List<ViewOthersMrr>();
                while (dr.Read())
                {
                    ViewOthersMrr itemDescription = new ViewOthersMrr();
                    itemDescription.MRRrOtherID = Convert.ToInt32(dr["MRRrOtherID"]);
                    itemDescription.MrrNo = dr["MrrNo"].ToString();
                    ItemDescriptionList.Add(itemDescription);
                }
                return ItemDescriptionList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<ViewOthersMrr> MrrList(DateTime? fromdate, DateTime? todate, int mrrNO = 0, int supplierId = 0, int itemId = 0)
        {
            try
            {
                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-2);
                }
                if (todate == null)
                {
                    todate = System.DateTime.Now;
                }
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ViewOthersMrr> aList = new List<ViewOthersMrr>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@MRRNo", mrrNO));
                aParameters.Add(new SqlParameter("@SupplierId", supplierId));
                aParameters.Add(new SqlParameter("@ItemId", itemId));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetGeneratedMrrList", aParameters);
                while (dr.Read())
                {
                    ViewOthersMrr aMaster = new ViewOthersMrr();
                    aMaster.MRRrOtherID = Convert.ToInt32(dr["MRRrOtherID"]);
                    aMaster.PurchaseOrderOtherID = Convert.ToInt32(dr["PurchaseOrderId"]);
                    aMaster.PurchaseOrderNo = dr["PurchaseOrderNo"].ToString();
                    aMaster.InvoiceStatus = dr["InvoiceStatus"].ToString();
                    //aMaster.LastRcvDate = (dr["OrderDate"] == DBNull.Value) ? (DateTime?)null : ((DateTime)dr["OrderDate"]);
                    aMaster.OrderDate = (dr["OrderDate"] == DBNull.Value) ? (DateTime?)null : ((DateTime)dr["OrderDate"]);
                    //  aMaster.OrderDate = Convert.ToDateTime(dr["OrderDate"]);
                    aMaster.MrrDate = Convert.ToDateTime(dr["MrrDate"]);
                    aMaster.SupplierName = dr["SupplierName"].ToString();
                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.CurrencyName = dr["CurrencyCode"].ToString();
                    aMaster.MRRQty = Convert.ToDecimal(dr["MRRQty"]);
                    aMaster.IsApprove = Convert.ToBoolean(dr["IsApprove"]);
                    aMaster.MrrNo = dr["MrrNo"].ToString();
                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<ViewOthersMrr> MrrListForPrint(DateTime? fromdate, DateTime? todate, int mrrNO = 0, int supplierId = 0, int itemId = 0)
        {
            try
            {
                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-2);
                }
                if (todate == null)
                {
                    todate = System.DateTime.Now;
                }
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ViewOthersMrr> aList = new List<ViewOthersMrr>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@MRRNo", mrrNO));
                aParameters.Add(new SqlParameter("@SupplierId", supplierId));
                aParameters.Add(new SqlParameter("@ItemId", itemId));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetGeneratedMrrListForPrint", aParameters);
                while (dr.Read())
                {
                    ViewOthersMrr aMaster = new ViewOthersMrr();
                    aMaster.MRRrOtherID = Convert.ToInt32(dr["MRRrOtherID"]);
                    aMaster.PurchaseOrderOtherID = Convert.ToInt32(dr["PurchaseOrderId"]);
                    aMaster.PurchaseOrderNo = dr["PurchaseOrderNo"].ToString();
                    //aMaster.LastRcvDate = (dr["OrderDate"] == DBNull.Value) ? (DateTime?)null : ((DateTime)dr["OrderDate"]);
                    aMaster.OrderDate = (dr["OrderDate"] == DBNull.Value) ? (DateTime?)null : ((DateTime)dr["OrderDate"]);
                    //  aMaster.OrderDate = Convert.ToDateTime(dr["OrderDate"]);
                    aMaster.MrrDate = Convert.ToDateTime(dr["MrrDate"]);
                    aMaster.SupplierName = dr["SupplierName"].ToString();
                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.CurrencyName = dr["CurrencyCode"].ToString();
                    aMaster.MRRQty = Convert.ToDecimal(dr["MRRQty"]);
                    aMaster.IsApprove = Convert.ToBoolean(dr["IsApprove"]);
                    aMaster.MrrNo = dr["MrrNo"].ToString();
                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<ViewOthersMrrDetails> MRRDetailsById(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ViewOthersMrrDetails> aList = new List<ViewOthersMrrDetails>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@Id", id));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetMrrDetailsById", aParameters);
                while (dr.Read())
                {
                    ViewOthersMrrDetails aMaster = new ViewOthersMrrDetails();
                    aMaster.ItemCode = dr["ItemCode"].ToString();
                    aMaster.ItemName = dr["ItemName"].ToString();
                    aMaster.UOMName = dr["UOMName"].ToString();
                   // aMaster.PurchaseQty = Convert.ToDecimal(dr["PurchaseQty"]);
                    aMaster.PurchaseQty = (dr["PurchaseQty"] == DBNull.Value) ? 0 : ((decimal)dr["PurchaseQty"]);
                    aMaster.MrrQty = Convert.ToDecimal(dr["MrrQty"]);
                    aMaster.UnitPrice = Convert.ToDecimal(dr["UnitPrice"]);
                    aMaster.TotalPrice = Convert.ToDecimal(dr["TotalPrice"]);
                    aMaster.MFGDate = Convert.ToDateTime(dr["MfgDate"]);
                    aMaster.ExpireyDate = Convert.ToDateTime(dr["ExpairyDate"]);
                    aMaster.ItemId = Convert.ToInt32(dr["ItemId"]);
                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<ViewOthersMrrDetails> MRRDetailsByIdForEdit(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ViewOthersMrrDetails> aList = new List<ViewOthersMrrDetails>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@Id", id));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetMrrOtherDetailsForEdit", aParameters);
                while (dr.Read())
                {
                    ViewOthersMrrDetails aMaster = new ViewOthersMrrDetails();
                    aMaster.ItemCode = dr["ItemCode"].ToString();
                    aMaster.ItemName = dr["ItemName"].ToString();
                    aMaster.UOMName = dr["UOMName"].ToString();
                   // aMaster.PurchaseQty = Convert.ToDecimal(dr["PurchaseQty"]);
                    aMaster.PurchaseQty = (dr["PurchaseQty"] == DBNull.Value) ? 0 : ((decimal)dr["PurchaseQty"]);
                    aMaster.MrrQty = Convert.ToDecimal(dr["MrrQty"]);
                   // aMaster.ChallanQty = Convert.ToDecimal(dr["ChallanQty"]);
                    aMaster.UnitPrice = Convert.ToDecimal(dr["UnitPrice"]);
                    aMaster.TotalPrice = Convert.ToDecimal(dr["TotalPrice"]);
                    aMaster.MFGDate = Convert.ToDateTime(dr["MfgDate"]);
                    aMaster.ExpireyDate = Convert.ToDateTime(dr["ExpairyDate"]);
                    aMaster.ItemId = Convert.ToInt32(dr["ItemId"]);
                    aMaster.MRRDetailID = Convert.ToInt32(dr["MRRDetailID"]);
                    aMaster.MRRRemainingQty =  DBNull.Value.Equals(dr["MRRRemainingQty"]) ? 0 : Convert.ToDecimal(dr["MRRRemainingQty"]);
              
                    aMaster.ReceivedQty = DBNull.Value.Equals(dr["ReceivedQty"]) ? 0 : Convert.ToDecimal(dr["ReceivedQty"]);
                    aMaster.ChallanQty = DBNull.Value.Equals(dr["ChallanQty"]) ? 0 : Convert.ToDecimal(dr["ChallanQty"]);
                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public DataTable GetMrrPrintReport(int id)
        {
            try
            {
                DataTable dr = new DataTable();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@Id", id));
                dr = _accessManager.GetDataTable("GetMrrPrintById", parameters);
                return dr;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<ViewOthersMrr> ListForReturn(DateTime? fromdate, DateTime? todate, int mrrNO = 0,
            int supplierId = 0, int itemId = 0)
        {
            try
            {
                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-2);
                }
                if (todate == null)
                {
                    todate = System.DateTime.Now;
                }
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ViewOthersMrr> aList = new List<ViewOthersMrr>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@MRRNo", mrrNO));
                aParameters.Add(new SqlParameter("@SupplierId", supplierId));
                aParameters.Add(new SqlParameter("@ItemId", itemId));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetGeneratedMrrListForReturn", aParameters);
                while (dr.Read())
                {
                    ViewOthersMrr aMaster = new ViewOthersMrr();
                    aMaster.MRRrOtherID = Convert.ToInt32(dr["MRRrOtherID"]);
                    aMaster.PurchaseOrderNo = dr["PurchaseOrderNo"].ToString();
                    aMaster.OrderDate = (dr["OrderDate"] == DBNull.Value)
                        ? (DateTime?)null
                        : ((DateTime)dr["OrderDate"]);
                    //  aMaster.OrderDate = Convert.ToDateTime(dr["OrderDate"]);
                    aMaster.MrrDate = Convert.ToDateTime(dr["MrrDate"]);
                    aMaster.TotalAmountRcv = Convert.ToDecimal(dr["TotalAmount"]);
                    aMaster.SupplierName = dr["SupplierName"].ToString();
                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.CurrencyName = dr["CurrencyCode"].ToString();
                    aMaster.MRRQty = Convert.ToDecimal(dr["MRRQty"]);
                    aMaster.MrrNo = dr["MrrNo"].ToString();
                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public ViewOthersMrr GetMrrInfoByMasterById(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                ViewOthersMrr aMaster = new ViewOthersMrr();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@Id", id));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetMrrMasterInfoById", aParameters);
                while (dr.Read())
                {
                    aMaster.MRRrOtherID = Convert.ToInt32(dr["MRRrOtherID"]);
                    aMaster.SupplierID = Convert.ToInt32(dr["SupplierID"]);
                    aMaster.DelLocationId = Convert.ToInt32(dr["DelLocationId"]);
                    aMaster.DelWareHouseID = Convert.ToInt32(dr["DelWareHouseID"]);
                    aMaster.MrrNo = dr["MrrNo"].ToString();
                    aMaster.MrrDate = Convert.ToDateTime(dr["MRRDate"]);
                   // aMaster.ChallanDate = Convert.ToDateTime(dr["ChallanDate"]);
                    aMaster.OrderDate = Convert.ToDateTime(dr["OrderDate"]);
                    aMaster.PurchaseOrderNo = dr["PurchaseOrderNo"].ToString();
                    aMaster.SupplierName = dr["SupplierName"].ToString();
                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.CurrencyName = dr["CurrencyCode"].ToString();
                    aMaster.SupplierRef = dr["SupplierRef"].ToString();
                    aMaster.DeliveryTo = dr["DeliveryTo"].ToString();
                    aMaster.ChallanNo = dr["ChallanNo"].ToString();
                    aMaster.LabResult = dr["LabResult"].ToString();
                    aMaster.VehicleNo = dr["VehicleNo"].ToString();
                    aMaster.VehicleInWeight = dr["VehicleInWeight"].ToString();
                    aMaster.VehicleOutWeight = dr["VehicleOutWeight"].ToString();
                    aMaster.SpecialInstruction = dr["SpecialInstruction"].ToString();
                    aMaster.LaborCost = Convert.ToDecimal(dr["LaborCost"]);
                    aMaster.TransportCost = Convert.ToDecimal(dr["TransportCost"]);
                    aMaster.IsApprove = Convert.ToBoolean(dr["IsApprove"]);
                    aMaster.OrderTotalAmt = DBNull.Value.Equals(dr["TotalAmount"]) ? 0 : Convert.ToDecimal(dr["TotalAmount"]);
                    aMaster.ChallanDate = (dr["ChallanDate"] == DBNull.Value) ? (DateTime?)null : ((DateTime)dr["ChallanDate"]);
                }
                return aMaster;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public ResultResponse SaveMrrReturn(ViewMrrReturnMaster aMaster, string user)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ReturnID", aMaster.ReturnID));
                aParameters.Add(new SqlParameter("@MrrId", aMaster.MrrId));
                aParameters.Add(new SqlParameter("@ReturnDate", DateTime.Now.Date));
                aParameters.Add(new SqlParameter("@SupplierID", aMaster.SupplierID));
                aParameters.Add(new SqlParameter("@Remarks", aMaster.Remarks));
                aParameters.Add(new SqlParameter("@DelLocationId", aMaster.DelLocationId));
                aParameters.Add(new SqlParameter("@DelWareHouseID", aMaster.DelWareHouseID));
                aParameters.Add(new SqlParameter("@TotalQty", aMaster.ReturnDetails.Sum(n => n.ReturnQty)));
                aParameters.Add(new SqlParameter("@CreateBy", user));
                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("SaveMrrReturnMaster", aParameters);
                if (aResponse.pk > 0)
                {
                    if (aMaster.ReturnDetails.Count > 0)
                    {
                        aResponse.isSuccess = SaveMrrReturnDetails(aMaster.ReturnDetails, aResponse.pk);
                    }
                  aResponse.isSuccess = MrrReturnSystemJournal(aResponse.pk);
                    aResponse.isSuccess = SaveOtherStockOut(aResponse.pk, aMaster.ReturnDetails);
                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                }
                else
                {
                    _accessManager.SqlConnectionClose(true);
                }
                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        private bool SaveMrrReturnDetails(List<ViewMrrReturnDetails> aMaster, int aResponsePk)
        {
            try
            {
                bool result = false;
                foreach (var item in aMaster)
                {
                    if (item.ReturnQty > 0)
                    {
                        List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                        ResultResponse aResponse = new ResultResponse();
                        aSqlParameterList.Add(new SqlParameter("@ReturnID", aResponsePk));
                        aSqlParameterList.Add(new SqlParameter("@ItemId", item.ItemId));
                        aSqlParameterList.Add(new SqlParameter("@MRRQty", item.MRRQty));
                        aSqlParameterList.Add(new SqlParameter("@ReturnQty", item.ReturnQty));
                        aSqlParameterList.Add(new SqlParameter("@UnitPrice", item.UnitPrice));
                        aSqlParameterList.Add(new SqlParameter("@TotalPrice", item.TotalPrice));
                        aResponse.pk =
                            _accessManager.SaveDataReturnPrimaryKey("SaveReturnedMrrDetails", aSqlParameterList);
                    }
                    //else
                    //{
                    //    break;
                    //}
                }
                return result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool MrrReturnSystemJournal(int returnMasterId)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ReturnMasterId", returnMasterId));
                result = _accessManager.SaveData("SavReturnMrrJournal", aParameters);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool SaveOtherStockOut(int aResponsePk, List<ViewMrrReturnDetails> aMaster)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                ResultResponse aResponse = new ResultResponse();
              //  aSqlParameterList.Add(new SqlParameter("@ReturnMasterId", aResponsePk));
             //   aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("SaveReturnedMrrStockIn", aSqlParameterList);
                foreach (var item in aMaster)
                {
                    if (/*aResponse.pk > 0 &&*/ item.ReturnQty > 0)
                    {
                        List<SqlParameter> aSqlParameterList1 = new List<SqlParameter>();
                        aSqlParameterList1.Add(new SqlParameter("@returnId", aResponsePk));
                      //  aSqlParameterList1.Add(new SqlParameter("@StockInMasterId", aResponse.pk));
                        aSqlParameterList1.Add(new SqlParameter("@itemId", item.ItemId));
                        aSqlParameterList1.Add(new SqlParameter("@returnQty", item.ReturnQty));
                        result = _accessManager.SaveData("SaveReturnedMrrStockInDetails", aSqlParameterList1);
                    }
                }
                return result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<ViewOthersMrrDetails> MrrDetailsforReturnById(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ViewOthersMrrDetails> aList = new List<ViewOthersMrrDetails>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@Id", id));
                SqlDataReader dr = _accessManager.GetSqlDataReader("getMrrDetailsforReturnById", aParameters);
                while (dr.Read())
                {
                    ViewOthersMrrDetails aMaster = new ViewOthersMrrDetails();
                    aMaster.ItemCode = dr["ItemCode"].ToString();
                    aMaster.ItemName = dr["ItemName"].ToString();
                    aMaster.UOMName = dr["UOMName"].ToString();
                    aMaster.PurchaseQty = (dr["PurchaseQty"] == DBNull.Value) ? 0 : ((decimal)dr["PurchaseQty"]);
                    aMaster.MrrQty = Convert.ToDecimal(dr["MrrQty"]);
                    aMaster.UnitPrice = Convert.ToDecimal(dr["UnitPrice"]);
                    aMaster.TotalPrice = Convert.ToDecimal(dr["TotalPrice"]);
                    aMaster.MFGDate = Convert.ToDateTime(dr["MfgDate"]);
                    aMaster.ExpireyDate = Convert.ToDateTime(dr["ExpairyDate"]);
                    aMaster.ItemId = Convert.ToInt32(dr["ItemId"]);
                    aMaster.ReturnQty = Convert.ToDecimal(dr["ReturnQty"]);
                    aMaster.RemainingQuanity = Convert.ToDecimal(dr["RemainingQuanity"]);
                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<ViewMrrReturnMaster> ReturnedMrrList(DateTime? fromdate, DateTime? todate, int supplierId = 0,
           int itemId = 0)
        {
            try
            {
                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-7);
                }
                if (todate == null)
                {
                    todate = System.DateTime.Now;
                }
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ViewMrrReturnMaster> aList = new List<ViewMrrReturnMaster>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@SupplierId", supplierId));
                aParameters.Add(new SqlParameter("@ItemId", itemId));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetReturnedMrrList", aParameters);
                while (dr.Read())
                {
                    ViewMrrReturnMaster aMaster = new ViewMrrReturnMaster();
                    aMaster.MrrId = Convert.ToInt32(dr["MrrId"]);
                    aMaster.ReturnID = Convert.ToInt32(dr["ReturnID"]);
                    aMaster.ReturnNo = dr["ReturnNo"].ToString();
                    aMaster.ReturnDate = Convert.ToDateTime(dr["ReturnDate"]);
                    aMaster.SupplierName = dr["SupplierName"].ToString();
                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.Remarks = dr["Remarks"].ToString();
                    aMaster.TotalQty = Convert.ToDecimal(dr["TotalQty"]);
                    aMaster.TotalAmount = Convert.ToDecimal(dr["TotalAmount"]);
                    aMaster.MrrNo = dr["MrrNo"].ToString();
                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<ViewMrrReturnDetails> MrrReturnDetailsById(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ViewMrrReturnDetails> aList = new List<ViewMrrReturnDetails>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@Id", id));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetMrrReturnDetailsById", aParameters);
                while (dr.Read())
                {
                    ViewMrrReturnDetails aMaster = new ViewMrrReturnDetails();
                    aMaster.ItemName = dr["ItemName"].ToString();
                    aMaster.UOMName = dr["UOMName"].ToString();
                    aMaster.ReturnQty = Convert.ToDecimal(dr["ReturnQty"]);
                    aMaster.MRRQty = Convert.ToDecimal(dr["MRRQty"]);
                    aMaster.UnitPrice = Convert.ToDecimal(dr["UnitPrice"]);
                    aMaster.TotalPrice = Convert.ToDecimal(dr["TotalPrice"]);
                    aMaster.ItemId = Convert.ToInt32(dr["ItemId"]);
                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<ViewOthersMrr> MrrApprovalList(DateTime? fromdate, DateTime? todate, int mrrNO = 0, int supplierId = 0, int itemId = 0)
        {
            try
            {
                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-2);
                }
                if (todate == null)
                {
                    todate = System.DateTime.Now;
                }
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ViewOthersMrr> aList = new List<ViewOthersMrr>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@MRRNo", mrrNO));
                aParameters.Add(new SqlParameter("@SupplierId", supplierId));
                aParameters.Add(new SqlParameter("@ItemId", itemId));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("MrrApprovalList", aParameters);
                while (dr.Read())
                {
                    ViewOthersMrr aMaster = new ViewOthersMrr();
                    aMaster.MRRrOtherID = Convert.ToInt32(dr["MRRrOtherID"]);
                    aMaster.PurchaseOrderOtherID = Convert.ToInt32(dr["PurchaseOrderId"]);
                    aMaster.PurchaseOrderNo = dr["PurchaseOrderNo"].ToString();
                    //aMaster.LastRcvDate = (dr["OrderDate"] == DBNull.Value) ? (DateTime?)null : ((DateTime)dr["OrderDate"]);
                    aMaster.OrderDate = (dr["OrderDate"] == DBNull.Value) ? (DateTime?)null : ((DateTime)dr["OrderDate"]);
                    //  aMaster.OrderDate = Convert.ToDateTime(dr["OrderDate"]);
                    aMaster.MrrDate = Convert.ToDateTime(dr["MrrDate"]);
                    aMaster.SupplierName = dr["SupplierName"].ToString();
                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.CurrencyName = dr["CurrencyCode"].ToString();
                    aMaster.MRRQty = Convert.ToDecimal(dr["MRRQty"]);
                    aMaster.IsApprove = Convert.ToBoolean(dr["IsApprove"]);
                    aMaster.MrrNo = dr["MrrNo"].ToString();
                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public DataTable GetReturnReport(int supplier, int warehouse, DateTime fromdate, DateTime todate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();

                parameters.Add(new SqlParameter("@SupplierId", supplier));
                parameters.Add(new SqlParameter("@fromDate", fromdate));
                parameters.Add(new SqlParameter("@toDate", todate));
                parameters.Add(new SqlParameter("@warehouse", warehouse));


                DataTable dr = _accessManager.GetDataTable("MrrReturnListRpt", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetSupplierMrrDetails(DateTime fromDate, DateTime toDate, int supplierId)
        {

            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@FromDate", fromDate));
                aParameters.Add(new SqlParameter("@ToDate", toDate));
                aParameters.Add(new SqlParameter("@SupplierId", supplierId));

                DataTable dt = new DataTable();

                dt = _accessManager.GetDataTable("sp_GetMRRDetails", aParameters);
                return dt;

            }
            catch (Exception exception)
            {

                throw exception;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

    }
}