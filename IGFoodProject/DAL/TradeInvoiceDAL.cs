﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IGFoodProject.DataManager;
using IGFoodProject.Models;
using IGFoodProject.ViewModel;
using System.Data;
using System.Data.SqlClient;

namespace IGFoodProject.DAL
{
    public class TradeInvoiceDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();

        public List<ViewOthersMrr> LoadMrrNoBySupplier(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@Id", id));

                SqlDataReader dr = _accessManager.GetSqlDataReader("GetMrrNoBySupplierId", aParameters);

                List<ViewOthersMrr> ItemDescriptionList = new List<ViewOthersMrr>();
                while (dr.Read())
                {
                    ViewOthersMrr itemDescription = new ViewOthersMrr();
                    itemDescription.MRRrOtherID = Convert.ToInt32(dr["MRRrOtherID"]);
                    itemDescription.MrrNo = dr["MrrNo"].ToString();
                    ItemDescriptionList.Add(itemDescription);
                }

                return ItemDescriptionList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<ViewTradeInvoiceMaster> LoadTradeMrrInfo(int supplierId , DateTime? mrrDate = null)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@SupplierId", supplierId));
                aParameters.Add(new SqlParameter("@MrrDate", mrrDate));

                SqlDataReader dr = _accessManager.GetSqlDataReader("GetTradeMrrInfoForInvoiceBySearch", aParameters);

                List<ViewTradeInvoiceMaster> ItemDescriptionList = new List<ViewTradeInvoiceMaster>();
                while (dr.Read())
                {
                    ViewTradeInvoiceMaster itemDescription = new ViewTradeInvoiceMaster();

                    itemDescription.TradeMrrMasterId = Convert.ToInt32(dr["TradeMrrMasterId"]);
                    itemDescription.TradePurchaseOrderId = Convert.ToInt32(dr["TradePurchaseOrderId"]);
                    itemDescription.SupplierID = Convert.ToInt32(dr["SupplierID"]);
                    itemDescription.PreInvoiceAmount = Convert.ToDecimal(dr["PreInvoiceAmount"]);
                    itemDescription.RestMrrAmount = Convert.ToDecimal(dr["RestMrrAmount"]);
                    itemDescription.RestInvoiceAmount = Convert.ToDecimal(dr["RestInvoiceAmount"]);
                    itemDescription.TradePOAdvance = Convert.ToDecimal(dr["TradePOAdvance"]);
                    itemDescription.LaborCost = Convert.ToDecimal(dr["LaborCost"]);
                    itemDescription.TransportCost = Convert.ToDecimal(dr["TransportCost"]);
                    itemDescription.MRRDate = Convert.ToDateTime(dr["MRRDate"]);
                    itemDescription.MrrNo = dr["MrrNo"].ToString();
                    itemDescription.TradeNo = dr["TradeNo"].ToString();
                    itemDescription.ProductDescpriction = dr["ProductDescriptions"].ToString();

                    ItemDescriptionList.Add(itemDescription);
                }

                return ItemDescriptionList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse SaveTradeInvoice(ViewTradeInvoiceMaster aMaster, string user)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@TradeInvoiceMasterID", aMaster.TradeInvoiceMasterID));
                aParameters.Add(new SqlParameter("@TradeInvoiceDate", DateTime.Now.Date));
                aParameters.Add(new SqlParameter("@dueDate", aMaster.DueDate));
                aParameters.Add(new SqlParameter("@billDate", aMaster.SupplierBillDate));
                aParameters.Add(new SqlParameter("@billno", aMaster.SupplierBillNo));
                aParameters.Add(new SqlParameter("@VatPercent", aMaster.VatPercent));
                aParameters.Add(new SqlParameter("@VatAmount", aMaster.VatAmount));
                aParameters.Add(new SqlParameter("@TotalAmount", aMaster.TotalAmount));
                aParameters.Add(new SqlParameter("@SupplierID", aMaster.SupplierID));
                aParameters.Add(new SqlParameter("@CreateBy", user));

                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("SaveTradeInvoice", aParameters);
                if (aResponse.pk > 0)
                {
                    if (aMaster.InvoiceDetails.Count > 0)
                    {
                        aResponse.isSuccess = SaveTradeInvoiceDetails(aMaster.InvoiceDetails, aResponse.pk);
                    }
                  aResponse.isSuccess = CreateSystemJournal(aResponse.pk);

                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                }
                else
                {
                    _accessManager.SqlConnectionClose(true);
                }
                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        private bool SaveTradeInvoiceDetails(List<ViewTradeInvoiceDetails> aMaster, int aResponsePk)
        {

            try
            {
                bool result = false;

                foreach (var item in aMaster)
                {
                        List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                        ResultResponse aResponse = new ResultResponse();

                        aSqlParameterList.Add(new SqlParameter("@TradeInvoiceMasterID", aResponsePk));
                        aSqlParameterList.Add(new SqlParameter("@TradeMRRMasterID", item.TradeMRRMasterID));
                        aSqlParameterList.Add(new SqlParameter("@TradeMRRDate", item.TradeMRRDate));
                        aSqlParameterList.Add(new SqlParameter("@MrrAmount", item.MrrAmount));
                        aSqlParameterList.Add(new SqlParameter("@InvoiceAmount", item.InvoiceAmount));
                        aSqlParameterList.Add(new SqlParameter("@TradePoAdvanced", item.TradePoAdvanced));
                        aSqlParameterList.Add(new SqlParameter("@TotalInvoicePrice", item.TotalInvoicePrice));
                        aSqlParameterList.Add(new SqlParameter("@ProductDescription", item.ProductDescription));

                        aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("SaveTradeInvoiceDetails", aSqlParameterList);

                }
                return result = true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private bool CreateSystemJournal(int aResponsePk)
        {

            try
            {

                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@Id", aResponsePk));


                result = _accessManager.SaveData("SaveTradeInvoiceJournal", aParameters);

                return result;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<ViewTradeInvoiceMaster> GetTradeInvoices(int supplierId = 0, int tradeInvoiceId = 0, DateTime? fromDate = null, DateTime? toDate = null)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@supplierId", supplierId));
                aParameters.Add(new SqlParameter("@tradeInvoiceId", tradeInvoiceId));
                aParameters.Add(new SqlParameter("@fromdate", fromDate));
                aParameters.Add(new SqlParameter("@todate", toDate));

                SqlDataReader dr = _accessManager.GetSqlDataReader("GetTradeInvoiceList", aParameters);

                List<ViewTradeInvoiceMaster> ItemDescriptionList = new List<ViewTradeInvoiceMaster>();
                while (dr.Read())
                {
                    ViewTradeInvoiceMaster itemDescription = new ViewTradeInvoiceMaster();

                    itemDescription.TradeInvoiceMasterID = Convert.ToInt32(dr["TradeInvoiceMasterID"]);
                    itemDescription.SupplierID = Convert.ToInt32(dr["SupplierID"]);
                    itemDescription.TotalAmount = Convert.ToDecimal(dr["TotalAmount"]);
                    itemDescription.TotalQty = DBNull.Value.Equals(dr["TotalQty"])?0: Convert.ToDecimal(dr["TotalQty"]);
                    itemDescription.TotalKG = DBNull.Value.Equals(dr["TotalKG"]) ? 0 : Convert.ToDecimal(dr["TotalKG"]);
                    itemDescription.TradeInvoiceDate = Convert.ToDateTime(dr["TradeInvoiceDate"]);
                    itemDescription.DueDate = Convert.ToDateTime(dr["DueDate"]);
                    itemDescription.TradeInvoiceNo = dr["TradeInvoiceNo"].ToString();
                    itemDescription.SupplierName = dr["SupplierName"].ToString();
                    ItemDescriptionList.Add(itemDescription);
                }

                return ItemDescriptionList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<ViewTradeInvoiceDetails> TradeInvoiceDetailsById(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ViewTradeInvoiceDetails> aList = new List<ViewTradeInvoiceDetails>();
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@Id", id));

                SqlDataReader dr = _accessManager.GetSqlDataReader("GetTradeInvoiceDetailsById", aParameters);
                while (dr.Read())
                {
                    ViewTradeInvoiceDetails aMaster = new ViewTradeInvoiceDetails();

                    aMaster.TradeInvoiceId = Convert.ToInt32(dr["TradeInvoiceId"]);
                    aMaster.TradeMRRMasterID = Convert.ToInt32(dr["TradeMRRMasterID"]);
                    aMaster.TradeMRRDate = Convert.ToDateTime(dr["TradeMRRDate"]);
                    aMaster.MrrAmount = DBNull.Value.Equals(dr["MrrAmount"]) ? 0 : Convert.ToDecimal(dr["MrrAmount"]);
                    aMaster.InvoiceAmount = DBNull.Value.Equals(dr["InvoiceAmount"]) ? 0 : Convert.ToDecimal(dr["InvoiceAmount"]);
                    aMaster.TotalInvoicePrice = DBNull.Value.Equals(dr["TotalInvoicePrice"]) ? 0 : Convert.ToDecimal(dr["TotalInvoicePrice"]);
                    aMaster.ProductDescription = dr["ProductDescription"].ToString();
                    aMaster.MrrNo = dr["MrrNo"].ToString();
                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public List<ViewTradeInvoiceMaster> LoadTradeInvoiceNo()
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);


                SqlDataReader dr = _accessManager.GetSqlDataReader("GetTradeInvoiceNo");
                List<ViewTradeInvoiceMaster> ItemDescriptionList = new List<ViewTradeInvoiceMaster>();
                while (dr.Read())
                {
                    ViewTradeInvoiceMaster itemDescription = new ViewTradeInvoiceMaster();
                    itemDescription.TradeInvoiceMasterID = Convert.ToInt32(dr["TradeInvoiceMasterID"]);
                    itemDescription.TradeInvoiceNo = dr["TradeInvoiceNo"].ToString();
                    ItemDescriptionList.Add(itemDescription);
                }

                return ItemDescriptionList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetIndivualInvoicePrintReport(int id)
        {
            try
            {
                DataTable dr = new DataTable();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@Id", id));
                dr = _accessManager.GetDataTable("GetIndivualInvoicePrintReport", parameters);

                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public DataTable GetMultipleTradeInvoiceReport(string TradeInvoiceNo)
        {
            try
            {
                DataTable dr = new DataTable();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@Ids", TradeInvoiceNo));
                dr = _accessManager.GetDataTable("GetMultipleTradeInvoiceReport", parameters);

                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
    }
}