﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using IGFoodProject.DataManager;
using IGFoodProject.Models;
using IGFoodProject.ViewModel;

namespace IGFoodProject.DAL
{
    public class StockReceiveDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();
        public List<ViewStockReceiveListView> GetStockReceiveSearch(int company, int warehouse, int vehicle, DateTime deliveryDate)
        {

            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ViewStockReceiveListView> aList = new List<ViewStockReceiveListView>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@company", company));
                aParameters.Add(new SqlParameter("@warehouse", warehouse));
                aParameters.Add(new SqlParameter("@vehicle", vehicle));
                aParameters.Add(new SqlParameter("@deliveryDate", deliveryDate));

                SqlDataReader aReader = _accessManager.GetSqlDataReader("sp_Get_StockIssueForReceive", aParameters);
                while (aReader.Read())
                {
                    ViewStockReceiveListView aListView = new ViewStockReceiveListView();
                    aListView.FromWareHouseName = aReader["FromWareHouseName"].ToString();
                    aListView.IssueBy = aReader["IssueBy"].ToString();
                    aListView.IssueDate = aReader["IssueDate"] as DateTime?;
                    aListView.IssueKg = Convert.ToDecimal(aReader["IssueKg"]);
                    if (aReader["IssueQty"] != DBNull.Value)
                    {
                        aListView.IssueQty = Convert.ToDecimal(aReader["IssueQty"]);
                    }
                    //aListView.IssueQty = Convert.ToDecimal(aReader["IssueQty"]);
                    aListView.StockTransferIssueMasterId = Convert.ToInt32(aReader["StockTransferIssueMasterId"]);
                    aListView.IssueNo = aReader["IssueNo"].ToString();
                    aListView.ToWareHouseName = aReader["ToWareHouseName"].ToString();
                    aListView.VehicleRegNo = aReader["VehicleRegNo"].ToString();

                    aList.Add(aListView);
                }
                return aList;
            }
            catch (Exception ex)
            {

                string err = ex.ToString();

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public ViewStockReceiveListView GetStockreceiveByIssueId(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));

                SqlDataReader aReader = _accessManager.GetSqlDataReader("sp_GetIssueMasterViewById", aParameters);

                ViewStockReceiveListView aListView = new ViewStockReceiveListView();
                while (aReader.Read())
                {
                    aListView.FromWareHouseName = aReader["FromWareHouseName"].ToString();
                    aListView.IssueBy = aReader["IssueBy"].ToString();
                    aListView.IssueDate = aReader["IssueDate"] as DateTime?;
                    aListView.IssueKg = Convert.ToDecimal(aReader["IssueKg"]);
                    aListView.IssueQty = Convert.ToDecimal(aReader["IssueQty"]);
                    aListView.StockTransferIssueMasterId = Convert.ToInt32(aReader["StockTransferIssueMasterId"]);
                    aListView.FromWarehouseId = Convert.ToInt32(aReader["FromWarehouseId"]);
                    aListView.ToWarehouseId = Convert.ToInt32(aReader["ToWarehouseId"]);
                    aListView.CompanyId = Convert.ToInt32(aReader["CompanyId"]);
                    aListView.VehicleId = Convert.ToInt32(aReader["VehicleId"]);
                    aListView.IssueNo = aReader["IssueNo"].ToString();
                    aListView.ToWareHouseName = aReader["ToWareHouseName"].ToString();
                    aListView.VehicleRegNo = aReader["VehicleRegNo"].ToString();
                }
                return aListView;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public List<ViewStockReceiveDetails> GetIssueDetails(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add( new SqlParameter("@id" , id) );
                List<ViewStockReceiveDetails> aList = new List<ViewStockReceiveDetails>();
                SqlDataReader dt = _accessManager.GetSqlDataReader("sp_Get_IssueDetailsViewById", aParameters);
                while (dt.Read())
                {
                    ViewStockReceiveDetails aDetails = new ViewStockReceiveDetails();
                    aDetails.StockTransferIssueDetailsId = Convert.ToInt32(dt["StockTransferIssueDetailsId"]);
                    aDetails.ProductId = Convert.ToInt32(dt["ProductId"]);
                    aDetails.GroupId = Convert.ToInt32(dt["GroupId"]);
                    aDetails.IssueKg = Convert.ToDecimal(dt["IssueKg"]);
                    aDetails.IssueQty = Convert.ToDecimal(dt["IssueQty"]);
                    aDetails.PrevIssueQty = Convert.ToDecimal(dt["PrevIssueQty"]);
                    aDetails.PrevIssueKg = Convert.ToDecimal(dt["PrevIssueKg"]);
                    aDetails.ExpireDate = dt["PExpireDate"].ToString(); 
                    aDetails.ProductName = dt["ProductName"].ToString();
                    aList.Add(aDetails);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public ResultResponse SaveReceive(StockReceiveMaster aMaster, string user)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@StockTransferIssueMasterId", aMaster.StockTransferIssueMasterId));
                aParameters.Add(new SqlParameter("@CompanyId", aMaster.CompanyId));
                aParameters.Add(new SqlParameter("@FromWarehouseId", aMaster.FromWarehouseId));
                aParameters.Add(new SqlParameter("@ToWarehouseId", aMaster.ToWarehouseId));
                
                aParameters.Add(new SqlParameter("@ReceiveDate", aMaster.ReceiveDate));
                aParameters.Add(new SqlParameter("@ReceiveBy", aMaster.ReceiveBy.Split(':')[0].Trim()));
                aParameters.Add(new SqlParameter("@ReceiveTime", aMaster.ReceiveTime));
                aParameters.Add(new SqlParameter("@CreateBy", user));
                aParameters.Add(new SqlParameter("@CreateDate", System.DateTime.Now));



                int stockReceiveMasterId = _accessManager.SaveDataReturnPrimaryKey("sp_SaveStockReceiveMaster", aParameters);

                // ResultResponse aResponse = new ResultResponse();
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@StockInDate", aMaster.ReceiveDate));

                aSqlParameterList.Add(new SqlParameter("@StockInBy", aMaster.ReceiveBy.Split(':')[0].Trim()));
                aSqlParameterList.Add(new SqlParameter("@CreateBy", user));
                aSqlParameterList.Add(new SqlParameter("@CreateDate", System.DateTime.Now));

                aSqlParameterList.Add(new SqlParameter("@WareHouseId", aMaster.ToWarehouseId));
                aSqlParameterList.Add(new SqlParameter("@CompanyID", aMaster.CompanyId));
                aSqlParameterList.Add(new SqlParameter("@ReceiveMasterId", stockReceiveMasterId));

                aSqlParameterList.Add(new SqlParameter("@IsOpening", 6));
                
                
                int stockInMasterId = _accessManager.SaveDataReturnPrimaryKey("sp_Save_StockinMasterFromReceive", aSqlParameterList);
                aResponse.isSuccess = SaveDetails(aMaster.Details, stockReceiveMasterId, stockInMasterId);


                if (aResponse.isSuccess == false)
                {
                    _accessManager.SqlConnectionClose(true);
                }
                //else
                //{
                //    ResultResponse aResponse2 = new ResultResponse();
                //    aResponse2 = SaveReceiveStockMaster(aMaster, user, aResponse.pk);
                //    if (aResponse2.isSuccess == false)
                //    {
                //        _accessManager.SqlConnectionClose(true);
                //    }
                //}
                return aResponse;
            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public bool SaveDetails(List<StockReceiveDetails> aList, int pk,int stockInMasterId)
        {

            bool result = false;
            try
            {
                foreach (var item in aList)
                {
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@StockReceiveMasterId", pk));
                    aParameters.Add(new SqlParameter("@StockTransferIssueDetailsId", item.StockTransferIssueDetailsId));
                    aParameters.Add(new SqlParameter("@ProductId", item.ProductId));
                    aParameters.Add(new SqlParameter("@ReceiveKg", item.ReceiveKg));
                    aParameters.Add(new SqlParameter("@ReceiveQty", item.ReceiveQty));
                    aParameters.Add(new SqlParameter("@IssueQty", item.IssueQty));
                    aParameters.Add(new SqlParameter("@IssueKG", item.IssueKG));
                    aParameters.Add(new SqlParameter("@WeightLossQty", item.WeightLossQty));
                    aParameters.Add(new SqlParameter("@WeightLossKg", item.WeightLossKg));
                    aParameters.Add(new SqlParameter("@ReturnQty", item.ReturnQty));
                    aParameters.Add(new SqlParameter("@ReturnKG", item.ReturnKG));
                    aParameters.Add(new SqlParameter("@LostQty", item.LostQty));
                    aParameters.Add(new SqlParameter("@LostKG", item.LostKG));
                    aParameters.Add(new SqlParameter("@ExpireDate", item.ExpireDate));
                    aParameters.Add(new SqlParameter("@Remarks", item.Remarks));
                    aParameters.Add(new SqlParameter("@StockInMasterID", stockInMasterId));

                    result = _accessManager.SaveData("sp_SaveTransferReceiveDetails", aParameters);
                    if (result == false)
                    {
                        break;
                    }
                }
            }
            catch (Exception)
            {
                
                throw;
            }
            return result;
        }


        //public ResultResponse SaveReceiveStockMaster(StockReceiveMaster aMaster, string user, int kp)
        //{
        //    try
        //    {
        //        ResultResponse aResponse = new ResultResponse();
        //        List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
        //        aSqlParameterList.Add(new SqlParameter("@StockInDate", aMaster.ReceiveDate));
                
        //        aSqlParameterList.Add(new SqlParameter("@StockInBy", aMaster.ReceiveBy.Split(':')[0].Trim()));
        //        aSqlParameterList.Add(new SqlParameter("@CreateBy", user));
        //        aSqlParameterList.Add(new SqlParameter("@CreateDate", System.DateTime.Now));

        //        aSqlParameterList.Add(new SqlParameter("@WareHouseId", aMaster.ToWarehouseId));
        //        aSqlParameterList.Add(new SqlParameter("@CompanyID", aMaster.CompanyId));
        //        aSqlParameterList.Add(new SqlParameter("@ReceiveMasterId", kp));
                
        //        aSqlParameterList.Add(new SqlParameter("@IsOpening", 6));
        //        aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_Save_StockinMasterFromReceive",
        //             aSqlParameterList);



        //        if (aResponse.pk > 0)
        //        {

        //            foreach (StockReceiveDetails grade in aMaster.Details)
        //            {
        //                List<SqlParameter> gSqlParameterList = new List<SqlParameter>();

                        
        //                gSqlParameterList.Add(new SqlParameter("@StockInMasterID", aResponse.pk));
                        
        //                gSqlParameterList.Add(new SqlParameter("@ProductID", grade.ProductId));
        //                gSqlParameterList.Add(new SqlParameter("@StockInDate", aMaster.ReceiveDate));
        //                gSqlParameterList.Add(new SqlParameter("@StockInQty", grade.ReceiveQty));
        //                gSqlParameterList.Add(new SqlParameter("@StockInKG", grade.ReceiveKg));
                        
        //                gSqlParameterList.Add(new SqlParameter("@ExpireDate", grade.ExpireDate));
        //                gSqlParameterList.Add(new SqlParameter("@Remark", grade.Remarks));
                        
        //                gSqlParameterList.Add(new SqlParameter("@isopening", 6));

        //                aResponse.isSuccess = _accessManager.SaveData("sp_Save_StockInDetailsFromReceive",
        //                    gSqlParameterList);
        //                if (aResponse.isSuccess == false)
        //                {
        //                    break;
        //                }
        //            }
        //        }

        //        return aResponse;


        //    }
        //    catch (Exception exception )
        //    {
                
        //        throw;
        //    }
        //}




    }
}