﻿using IGFoodProject.DataManager;
using IGFoodProject.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.DAL
   {
    public class ItemRequisitionDal
    {
        DataAccessManager _accessManager = new DataAccessManager();

        public async Task<List<Location>> LocationInformation()
        {
            _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
            SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetLocation");
            List<Location> locationList = new List<Location>();
            while (dr.Read())
            {
                Location location = new Location();
                location.LocationId = Convert.ToInt32(dr["LocationId"]);
                location.LocationName = dr["LocationName"].ToString();
                locationList.Add(location);
            }

            return locationList;
        }

        public List<WareHouse> WareHouseInformation(int locationId,int warehouseTypeId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@locationId", locationId));
                parameters.Add(new SqlParameter("@WarehouseTypeId", warehouseTypeId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetWareHouse", parameters);
                List<WareHouse> wareHouseList = new List<WareHouse>();
                while (dr.Read())
                {
                    WareHouse wareHouse = new WareHouse();
                    wareHouse.WareHouseId = Convert.ToInt32(dr["WareHouseId"]);
                    wareHouse.WareHouseName = dr["WareHouseName"].ToString();
                    wareHouseList.Add(wareHouse);
                }
                return wareHouseList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

        public dynamic SaveRequisition(RequisitionMaster requisitionMaster)
        {
            if (requisitionMaster.Details != null)
            {
                try
                {
                    _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                    ResultResponse aResponse = new ResultResponse();
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@ItemRequisitionMasterId", requisitionMaster.ItemRequisitionMasterId));
                    aParameters.Add(new SqlParameter("@RequisitionNo", requisitionMaster.RequisitionNo));
                    aParameters.Add(new SqlParameter("@RequisitionDate", requisitionMaster.RequisitionDate));
                    aParameters.Add(new SqlParameter("@Company", requisitionMaster.Company));
                    aParameters.Add(new SqlParameter("@WarehouseId", requisitionMaster.WarehouseId));
                    aParameters.Add(new SqlParameter("@ExpectedPurchaseDate", requisitionMaster.StrtExpectedPurchaseDate));
                    aParameters.Add(new SqlParameter("@RequisitionBy", requisitionMaster.RequisitionBy.Trim()));
                    aParameters.Add(new SqlParameter("@Remarks", requisitionMaster.Remarks));
                    aParameters.Add(new SqlParameter("@CreateBy", requisitionMaster.CreateBy));
                    aParameters.Add(new SqlParameter("@CreateDate", requisitionMaster.CreateDate));
                    aParameters.Add(new SqlParameter("@UpdateBy", requisitionMaster.UpdateBy));
                    aParameters.Add(new SqlParameter("@UpdateDate", requisitionMaster.UpdateDate));
                    aParameters.Add(new SqlParameter("@IsApprove ", requisitionMaster.IsApprove));
                    aParameters.Add(new SqlParameter("@ApproveBy", requisitionMaster.ApproveBy));
                    aParameters.Add(new SqlParameter("@ApproveDate", requisitionMaster.ApproveDate));
                    aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_SaveRequisitionMaster", aParameters);
                    if (aResponse.pk > 0)
                    {
                        foreach (var item in requisitionMaster.Details)
                        {
                            List<SqlParameter> sqlParameters = new List<SqlParameter>();
                            sqlParameters.Add(new SqlParameter("@ItemRequisitionDetailsId", item.ItemRequisitionDetailsId));
                            sqlParameters.Add(new SqlParameter("@ItemRequisitionMasterId", aResponse.pk));
                            sqlParameters.Add(new SqlParameter("@ItemId", item.ItemId));
                            sqlParameters.Add(new SqlParameter("@ProposedReqisitionQty", item.ProposedReqisitionQty));
                            sqlParameters.Add(new SqlParameter("@ProposedRequisitionKG", item.ProposedRequisitionKG));
                            sqlParameters.Add(new SqlParameter("@ProposedTentativePrice", item.ProposedTentativePrice));
                            sqlParameters.Add(new SqlParameter("@ApprovedReqisitionQty", item.ApprovedReqisitionQty));
                            sqlParameters.Add(new SqlParameter("@ApprovedReqisitionKG", item.ApprovedReqisitionKG));
                            sqlParameters.Add(new SqlParameter("@ApprovedTentativePrice", item.ApprovedTentativePrice));
                            sqlParameters.Add(new SqlParameter("@RemainingQty", item.RemainingQty));
                            sqlParameters.Add(new SqlParameter("@RemainingKG", item.RemainingKG));
                            sqlParameters.Add(new SqlParameter("@Remarks", item.Remarks));
                            aResponse.isSuccess = _accessManager.SaveData("sp_SaveRequisitionDetails", sqlParameters);
                            if (aResponse.isSuccess == false)
                            {
                                _accessManager.SqlConnectionClose(true);
                            }
                        }
                    }
                    else
                    {
                        foreach (var item in requisitionMaster.Details)
                        {
                            List<SqlParameter> sqlParameters = new List<SqlParameter>();
                            sqlParameters.Add(new SqlParameter("@ItemRequisitionDetailsId", item.ItemRequisitionDetailsId));
                            sqlParameters.Add(new SqlParameter("@ItemRequisitionMasterId", requisitionMaster.ItemRequisitionMasterId));
                            sqlParameters.Add(new SqlParameter("@ItemId", item.ItemId));
                            sqlParameters.Add(new SqlParameter("@ProposedReqisitionQty", item.ProposedReqisitionQty));
                            sqlParameters.Add(new SqlParameter("@ProposedRequisitionKG", item.ProposedRequisitionKG));
                            sqlParameters.Add(new SqlParameter("@ProposedTentativePrice", item.ProposedTentativePrice));
                            sqlParameters.Add(new SqlParameter("@ApprovedReqisitionQty", item.ApprovedReqisitionKG));
                            sqlParameters.Add(new SqlParameter("@ApprovedReqisitionKG", item.ApprovedReqisitionKG));
                            sqlParameters.Add(new SqlParameter("@ApprovedTentativePrice", item.ApprovedTentativePrice));
                            sqlParameters.Add(new SqlParameter("@RemainingQty", item.RemainingQty));
                            sqlParameters.Add(new SqlParameter("@RemainingKG", item.RemainingKG));
                            sqlParameters.Add(new SqlParameter("@Remarks", item.Remarks));
                            aResponse.isSuccess = _accessManager.SaveData("sp_SaveRequisitionDetails", sqlParameters);
                            if (aResponse.isSuccess == false)
                            {
                                _accessManager.SqlConnectionClose(true);
                            }
                        }
                    }
                    return aResponse;
                }
                catch (Exception ex)
                {
                    _accessManager.SqlConnectionClose(true);
                    throw ex;
                }
                finally
                {
                    _accessManager.SqlConnectionClose();
                }

            }

            return null;
        }
        public dynamic RequisitionList()
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameters = new List<SqlParameter>();
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetAllRequisitionList");
                List<RequisitionMaster> requisitionMasterList = new List<RequisitionMaster>();
                while (dr.Read())
                {
                    RequisitionMaster requisitionMaster = new RequisitionMaster();
                    requisitionMaster.ItemRequisitionMasterId = Convert.ToInt32(dr["ItemRequisitionMasterId"]);
                    requisitionMaster.Company = Convert.ToInt32(dr["Company"]);
                    requisitionMaster.CompanyName = dr["CompanyName"].ToString();
                    requisitionMaster.WarehouseId = Convert.ToInt32(dr["WarehouseId"]);
                    requisitionMaster.WareHouseName = dr["WareHouseName"].ToString();
                    requisitionMaster.RequisitionDate = dr["RequisitionDate"].ToString();
                    requisitionMaster.RequisitionNo = dr["RequisitionNo"].ToString();
                    requisitionMaster.Remarks = dr["Remarks"].ToString();
                    requisitionMaster.IsApprove = Convert.ToInt32(dr["IsApprove"]);
                    requisitionMaster.EmpName = dr["EmpName"].ToString();
                    requisitionMasterList.Add(requisitionMaster);

                }

                return requisitionMasterList;

            }
            catch (Exception exception)
            {


                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public dynamic GetRequisitionById(int id)
        {
            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameters = new List<SqlParameter>();
                aSqlParameters.Add(new SqlParameter("@ItemRequisitionMasterId", id));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetRequisitionById", aSqlParameters);
                RequisitionMaster requisitionMaster = new RequisitionMaster();
                while (dr.Read())
                {
                    requisitionMaster.ItemRequisitionMasterId = Convert.ToInt32(dr["ItemRequisitionMasterId"]);
                    requisitionMaster.Company = Convert.ToInt32(dr["Company"]);
                    requisitionMaster.WarehouseId = Convert.ToInt32(dr["WarehouseId"]);
                    requisitionMaster.LocationId = Convert.ToInt32(dr["LocationId"]);
                    requisitionMaster.RequisitionDate = dr["RequisitionDate"].ToString();
                    requisitionMaster.ExpectedPurchaseDate = dr["ExpectedPurchaseDate"].ToString();
                    requisitionMaster.RequisitionNo = dr["RequisitionNo"].ToString();
                    requisitionMaster.Remarks = dr["Remarks"].ToString();
                    requisitionMaster.IsApprove = Convert.ToInt32(dr["IsApprove"]);
                    requisitionMaster.EmpName = dr["RequisitionBy"].ToString();
                }
                _accessManager.SqlConnectionClose();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SqlDataReader drDetails = _accessManager.GetSqlDataReader("sp_GetRequisitionDetailList", aSqlParameters);
                List<RequisitionDetails> requisitionDetailsList = new List<RequisitionDetails>();
                while (drDetails.Read())
                {
                    RequisitionDetails requisitionDetails = new RequisitionDetails();
                    requisitionDetails.ItemRequisitionDetailsId = Convert.ToInt32(drDetails["ItemRequisitionDetailsId"]);
                    requisitionDetails.ItemRequisitionMasterId = Convert.ToInt32(drDetails["ItemRequisitionMasterId"]);
                    requisitionDetails.ItemId = Convert.ToInt32(drDetails["ItemId"]);
                    requisitionDetails.ProposedReqisitionQty = Convert.ToDecimal(drDetails["ProposedReqisitionQty"]);
                    requisitionDetails.ProposedRequisitionKG = DBNull.Value== drDetails["ProposedRequisitionKG"]?0: Convert.ToDecimal(drDetails["ProposedRequisitionKG"]);
                    requisitionDetails.ProposedTentativePrice = DBNull.Value == drDetails["ProposedTentativePrice"] ? 0 : Convert.ToDecimal(drDetails["ProposedTentativePrice"]); 
                    requisitionDetails.Remarks = drDetails["Remarks"].ToString();
                    requisitionDetailsList.Add(requisitionDetails);
                }
                requisitionMaster.Details = requisitionDetailsList;
                return requisitionMaster;

            }
            catch (Exception exception)
            {


                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public dynamic ApprovedRequisitionList()
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameters = new List<SqlParameter>();
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetAllRequisitionListFApproved");
                List<RequisitionMaster> requisitionMasterList = new List<RequisitionMaster>();
                while (dr.Read())
                {
                    RequisitionMaster requisitionMaster = new RequisitionMaster();
                    requisitionMaster.ItemRequisitionMasterId = Convert.ToInt32(dr["ItemRequisitionMasterId"]);
                    requisitionMaster.Company = Convert.ToInt32(dr["Company"]);
                    requisitionMaster.CompanyName = dr["CompanyName"].ToString();
                    requisitionMaster.WarehouseId = Convert.ToInt32(dr["WarehouseId"]);
                    requisitionMaster.WareHouseName = dr["WareHouseName"].ToString();
                    requisitionMaster.RequisitionDate = dr["RequisitionDate"].ToString();
                    requisitionMaster.RequisitionNo = dr["RequisitionNo"].ToString();
                    requisitionMaster.Remarks = dr["Remarks"].ToString();
                    requisitionMaster.IsApprove = Convert.ToInt32(dr["IsApprove"]);
                    requisitionMaster.EmpName = dr["EmpName"].ToString();
                    requisitionMasterList.Add(requisitionMaster);

                }

                return requisitionMasterList;

            }
            catch (Exception exception)
            {


                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public dynamic ApprovedRequisition(RequisitionMaster requisitionMaster)
        {
            if (requisitionMaster.Details != null)
            {
                try
                {
                    _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                    ResultResponse aResponse = new ResultResponse();
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@ItemRequisitionMasterId", requisitionMaster.ItemRequisitionMasterId));
                    aParameters.Add(new SqlParameter("@IsApprove ", requisitionMaster.IsApprove));
                    aParameters.Add(new SqlParameter("@ApproveBy", requisitionMaster.ApproveBy));
                    aParameters.Add(new SqlParameter("@ApproveDate", requisitionMaster.ApproveDate));
                    aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_ApprovedRequisition", aParameters);

                    foreach (var item in requisitionMaster.Details)
                    {
                        List<SqlParameter> sqlParameters = new List<SqlParameter>();
                        sqlParameters.Add(new SqlParameter("@ItemRequisitionDetailsId", item.ItemRequisitionDetailsId));
                        sqlParameters.Add(new SqlParameter("@ItemRequisitionMasterId", requisitionMaster.ItemRequisitionMasterId));
                        sqlParameters.Add(new SqlParameter("@ItemId", item.ItemId));
                        sqlParameters.Add(new SqlParameter("@ApprovedReqisitionQty", item.ApprovedReqisitionQty));
                        sqlParameters.Add(new SqlParameter("@ApprovedReqisitionKG", item.ApprovedReqisitionKG));
                        sqlParameters.Add(new SqlParameter("@ApprovedTentativePrice", item.ApprovedTentativePrice));
                        aResponse.isSuccess = _accessManager.SaveData("sp_ApprovedRequisitionDetails", sqlParameters);
                        if (aResponse.isSuccess == false)
                        {
                            _accessManager.SqlConnectionClose(true);
                        }
                    }

                    return aResponse;
                }
                catch (Exception ex)
                {
                    _accessManager.SqlConnectionClose(true);
                    throw ex;
                }
                finally
                {
                    _accessManager.SqlConnectionClose();
                }

            }

            return null;
        }

        public dynamic RejectRequisition(RequisitionMaster requisitionMaster)
        {
           
                try
                {
                    _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                    ResultResponse aResponse = new ResultResponse();
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@ItemRequisitionMasterId", requisitionMaster.ItemRequisitionMasterId));
                    aParameters.Add(new SqlParameter("@IsApprove ", requisitionMaster.IsApprove));
                    aParameters.Add(new SqlParameter("@ApproveBy", requisitionMaster.ApproveBy));
                    aParameters.Add(new SqlParameter("@ApproveDate", requisitionMaster.ApproveDate));
                    aResponse.isSuccess = _accessManager.SaveData("sp_ApprovedRequisition", aParameters);

                    return aResponse;
                }
                catch (Exception ex)
                {
                    _accessManager.SqlConnectionClose(true);
                    throw ex;
                }
                finally
                {
                    _accessManager.SqlConnectionClose();
                }
            
        }
        public double LastPriceOfItem(int itemId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                double unitPrice = 0.00;
                parameters.Add(new SqlParameter("@ItemId", itemId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetLastPurchasePriceOfItem", parameters);

                while (dr.Read())
                {

                    unitPrice = Convert.ToDouble(dr["UnitPrice"]);

                }
                return unitPrice;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
    }
}
