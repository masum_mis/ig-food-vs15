﻿using IGFoodProject.DataManager;
using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using IGFoodProject.Models;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Text;
using IGFoodProject.Models.Accounts;
using IGFoodProject.ViewModel;

namespace IGFoodProject.DAL
{
    public class UtilityDAL
    {
        DataAccessManager accessManager = new DataAccessManager();
        public bool CheckMenuAccess(string controllerName, string actionName, string userName)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.ControlPanel);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@ModuleId", "13"));
                aSqlParameterList.Add(new SqlParameter("@LoginUserName", userName));
                aSqlParameterList.Add(new SqlParameter("@ControllerName", controllerName));
                aSqlParameterList.Add(new SqlParameter("@ActionName", actionName));


                DataTable dt = accessManager.GetDataTable("sp_CheckMenuAccess", aSqlParameterList);
                if (dt.Rows.Count > 0)
                {
                    return true;
                }


                return false;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<CompanyInformation> CompanyInformation()
        {
            accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
            SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetAllCompanyInformation");
            List<CompanyInformation> companyInformationList = new List<CompanyInformation>();
            while (dr.Read())
            {
                CompanyInformation companyInformation = new CompanyInformation();
                companyInformation.CompanyId = Convert.ToInt32(dr["CompanyId"]);
                companyInformation.CompanyName = dr["CompanyName"].ToString();
                companyInformation.CompanyShortName = dr["CompanyShortName"].ToString();
                companyInformationList.Add(companyInformation);
            }

            return   companyInformationList;
        }

        public List<Location> LocationInformation(int comid=0)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@CompanyID", comid));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetAllLocationInformation", parameters);
                List<Location> locationList = new List<Location>();
                while (dr.Read())
                {
                    Location location = new Location();
                    location.LocationId = Convert.ToInt32(dr["LocationId"]);
                    location.LocationName = dr["LocationName"].ToString();
                    locationList.Add(location);
                }
                return locationList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

        }

        public List<InternalTransferComments> InternalStoComments()
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetAllInternalComments");
                List<InternalTransferComments> commentList = new List<InternalTransferComments>();
                while (dr.Read())
                {
                    InternalTransferComments comment = new InternalTransferComments();
                    comment.InternalTransferCommentId = Convert.ToInt32(dr["InternalTransferCommentId"]);
                    comment.Comment = dr["Comment"].ToString();
                    commentList.Add(comment);
                }
                return commentList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

        }

        public string GetUserInfoStrig(string id)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                string result = string.Empty;
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_UserInfoString", aParameters);
                while (dr.Read())
                {
                    result = dr["EmpName"].ToString();
                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public List<WareHouse> WareHouseInformation(int loginId , int locid=0,int wareHouseTypeId=0)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@LocationId", locid));
                parameters.Add(new SqlParameter("@WarehouseTypeId", wareHouseTypeId));
                parameters.Add(new SqlParameter("@loginId", loginId));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetWareHouseByLocation", parameters);
                List<WareHouse> wareHouseList = new List<WareHouse>();
                while (dr.Read())
                {
                    WareHouse wareHouse = new WareHouse();
                    wareHouse.WareHouseId = Convert.ToInt32(dr["WareHouseId"]);
                    wareHouse.WareHouseName = dr["WareHouseName"].ToString();
                    wareHouseList.Add(wareHouse);
                }
                return wareHouseList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

        }
        public List<WareHouse> WareHouseInformationMultipleType(int loginId ,int locid, string wareHouseTypeId)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@LocationId", locid));
                parameters.Add(new SqlParameter("@WarehouseTypeId", wareHouseTypeId));
                parameters.Add(new SqlParameter("@loginId", loginId));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetWareHouseByLocationMultipleType", parameters);
                List<WareHouse> wareHouseList = new List<WareHouse>();
                while (dr.Read())
                {
                    WareHouse wareHouse = new WareHouse();
                    wareHouse.WareHouseId = Convert.ToInt32(dr["WareHouseId"]);
                    wareHouse.WareHouseName = dr["WareHouseName"].ToString();
                    wareHouseList.Add(wareHouse);
                }
                return wareHouseList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

        }
        public List<ItemDescription> ItemDescriptions(int itemCategoryId = 0)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ItemCategoryId", itemCategoryId));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetItemDescription", aParameters);
                List<ItemDescription> ItemDescriptionList = new List<ItemDescription>();
                while (dr.Read())
                {
                    ItemDescription itemDescription = new ItemDescription();
                    itemDescription.ItemId = Convert.ToInt32(dr["ItemId"]);
                    itemDescription.ItemName = dr["ItemName"].ToString();
                    ItemDescriptionList.Add(itemDescription);
                }

                return ItemDescriptionList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<AutoComplete> GetEmpAutoComDALAll(string term)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.ControlPanel);
                List<AutoComplete> lst = new List<AutoComplete>();

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@Param", term));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetEmpForAutoComALL_IGFoods", aSqlParameterList);
                AutoComplete autoComplete;
                while (dr.Read())
                {
                    autoComplete = new AutoComplete();
                    autoComplete.value = dr["EMPID"] + ":" + dr["EmpName"] + ":" + dr["Designation"];
                    //autoComplete.value = dr["EMPID"] + ":" + dr["EmpName"];
                    autoComplete.label = dr["EMPID"] + ":" + dr["EmpName"] + ":" + dr["Designation"];
                    lst.Add(autoComplete);
                }


                return lst;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public int GetEmpNo(string empId)
        {
            try
            {
                int empNo = 0;
                accessManager.SqlConnectionOpen(DataBase.HumanResourceDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@empId", empId));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_EmpNoById", aParameters);
                while (dr.Read())
                {
                    empNo = (int)dr["EmpNo"];
                }
                return empNo;
            }
            catch (Exception exception)
            {

                throw exception;
            }

            finally
            {
                accessManager.SqlConnectionClose();
            }
        }



        public List<Division> GetDivision()
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_DivisionInfo");
                List < Division > _divList = new List<Division>();
                while (dr.Read())
                {
                    Division aDivision = new Division();
                    aDivision.DivisionId = Convert.ToInt32(dr["DivisionId"]);
                    aDivision.DivisionName = dr["DivisionName"].ToString().Trim();
                    _divList.Add(aDivision);
                }
                return _divList;
                //
                //    sp_Get_Territory


            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<Area> GetArea( int divisionId=0)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@divisionId" , divisionId));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_AreaInfo" , aParameters);
                List<Area> _divList = new List<Area>();
                while (dr.Read())
                {
                    Area aDivision = new Area();
                    aDivision.AreaId = Convert.ToInt32(dr["AreaId"]);
                    aDivision.AreaName = dr["AreaName"].ToString().Trim();
                    _divList.Add(aDivision);
                }
                return _divList;
               


            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<Territory> GetTerritory(int areaId=0)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@areaId", areaId));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_Territory", aParameters);
                List<Territory> _divList = new List<Territory>();
                while (dr.Read())
                {
                    Territory aDivision = new Territory();
                    aDivision.TerritoryId = Convert.ToInt32(dr["TerritoryId"]);
                    aDivision.TerritoryName = dr["TerritoryName"].ToString().Trim();
                    _divList.Add(aDivision);
                }
                return _divList;
               

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }


        public List<Market> GetMarket(int territoryId=0)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@territoryId", @territoryId));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_Market", aParameters);
                List<Market> _divList = new List<Market>();
                while (dr.Read())
                {
                    Market aMarket = new Market();
                    aMarket.MarketId = Convert.ToInt32(dr["MarketId"]);
                    aMarket.MarketName = dr["MarketName"].ToString().Trim();
                    _divList.Add(aMarket);
                }
                return _divList;


            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }


        public List<District> GetDistricts(int division=0)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@division", division));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_District", aParameters);
                List<District> _divList = new List<District>();
                while (dr.Read())
                {
                    District aDivision = new District();
                    aDivision.DistrictId = Convert.ToInt32(dr["DistrictId"]);
                    aDivision.DistrictName = dr["DistrictName"].ToString().Trim();
                    _divList.Add(aDivision);
                }
                return _divList;


            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<Thana> GetThanas(int district=0)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@district", district));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_Thana", aParameters);
                List<Thana> _divList = new List<Thana>();
                while (dr.Read())
                {
                    Thana aDivision = new Thana();
                    aDivision.ThanaId = Convert.ToInt32(dr["ThanaId"]);
                    aDivision.ThanaName = dr["ThanaName"].ToString().Trim();
                    _divList.Add(aDivision);
                }
                return _divList;


            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }


        public List<CustomerType> GetCustomerType()
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
               
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_CustomerTyle");
                List<CustomerType> _divList = new List<CustomerType>();
                while (dr.Read())
                {
                    CustomerType aDivision = new CustomerType();
                    aDivision.CustomerTypeID = Convert.ToInt32(dr["CustomerTypeID"]);
                    aDivision.CoustomerTypeName = dr["CoustomerTypeName"].ToString().Trim();
                    _divList.Add(aDivision);
                }
                return _divList;


            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }


        public List<CustomerPaymentType> GetCustomerPaymentType()
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_CustomerPaymentType");
                List<CustomerPaymentType> _divList = new List<CustomerPaymentType>();
                while (dr.Read())
                {
                    CustomerPaymentType aDivision = new CustomerPaymentType();
                    aDivision.PaymentTypeId = Convert.ToInt32(dr["PaymentTypeId"]);
                    aDivision.PaymentType = dr["PaymentType"].ToString().Trim();
                    _divList.Add(aDivision);
                }
                return _divList;


            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<CustomerPaymentType> GetSupplierPaymentType()
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_SupplierPaymentType");
                List<CustomerPaymentType> _divList = new List<CustomerPaymentType>();
                while (dr.Read())
                {
                    CustomerPaymentType aDivision = new CustomerPaymentType();
                    aDivision.PaymentTypeId = Convert.ToInt32(dr["PaymentTypeId"]);
                    aDivision.PaymentType = dr["PaymentType"].ToString().Trim();
                    _divList.Add(aDivision);
                }
                return _divList;


            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<Product> GetProductByGroupId(int groupId)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@groupId", groupId));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_ProductsByGroupId", aParameters);

                List<Product> aList = new List<Product>();
                while (dr.Read())
                {
                    Product aProduct = new Product();
                    aProduct.ProductNo = dr["ProductNo"].ToString();
                    aProduct.TypeId = Convert.ToInt32(dr["TypeId"]);
                    aProduct.ProductId = Convert.ToInt32(dr["ProductId"]);
                    aProduct.ProductDescription = dr["ProductDescription"].ToString();
                    aProduct.ProductName = dr["ProductName"].ToString();
                    aList.Add(aProduct);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public List<Product> GetProductRetailByGroupId(int groupId)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@groupId", groupId));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_ProductsRetailByGroupId", aParameters);

                List<Product> aList = new List<Product>();
                while (dr.Read())
                {
                    Product aProduct = new Product();
                    aProduct.ProductNo = dr["ProductNo"].ToString();
                    aProduct.TypeId = Convert.ToInt32(dr["TypeId"]);
                    aProduct.ProductId = Convert.ToInt32(dr["ProductId"]);
                    aProduct.ProductDescription = dr["ProductDescription"].ToString();
                    aProduct.ProductName = dr["ProductName"].ToString();
                    aList.Add(aProduct);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<Product> GetProductRestaturantByGroupId(int groupId)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@groupId", groupId));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_ProductsRestaturantByGroupId", aParameters);

                List<Product> aList = new List<Product>();
                while (dr.Read())
                {
                    Product aProduct = new Product();
                    aProduct.ProductNo = dr["ProductNo"].ToString();
                    aProduct.TypeId = Convert.ToInt32(dr["TypeId"]);
                    aProduct.ProductId = Convert.ToInt32(dr["ProductId"]);
                    aProduct.ProductDescription = dr["ProductDescription"].ToString();
                    aProduct.ProductName = dr["ProductName"].ToString();
                    aList.Add(aProduct);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public List<Product> GetSaleableProductByGroupId(int groupId)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@groupId", groupId));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_SaleableProductsByGroupId", aParameters);

                List<Product> aList = new List<Product>();
                while (dr.Read())
                {
                    Product aProduct = new Product();
                    aProduct.ProductNo = dr["ProductNo"].ToString();
                    aProduct.TypeId = Convert.ToInt32(dr["TypeId"]);
                    aProduct.ProductId = Convert.ToInt32(dr["ProductId"]);
                    aProduct.ProductDescription = dr["ProductDescription"].ToString();
                    aProduct.ProductName = dr["ProductName"].ToString();
                    aList.Add(aProduct);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<Product> GetNonSaleableProductByGroupId(int groupId)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@groupId", groupId));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_NonSaleableProductsByGroupId", aParameters);

                List<Product> aList = new List<Product>();
                while (dr.Read())
                {
                    Product aProduct = new Product();
                    aProduct.ProductNo = dr["ProductNo"].ToString();
                    aProduct.TypeId = Convert.ToInt32(dr["TypeId"]);
                    aProduct.ProductId = Convert.ToInt32(dr["ProductId"]);
                    aProduct.ProductDescription = dr["ProductDescription"].ToString();
                    aProduct.ProductName = dr["ProductName"].ToString();
                    aList.Add(aProduct);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public List<Product> GetProductByGroupId2(int groupId =0)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@groupId", groupId));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_ProductsByGroupId_2", aParameters);

                List<Product> aList = new List<Product>();
                while (dr.Read())
                {
                    Product aProduct = new Product();
                    aProduct.ProductNo = dr["ProductNo"].ToString();
                    aProduct.TypeId = Convert.ToInt32(dr["TypeId"]);
                    aProduct.ProductId = Convert.ToInt32(dr["ProductId"]);
                    aProduct.ProductDescription = dr["ProductDescription"].ToString();
                    aProduct.ProductName = dr["ProductName"].ToString();
                    aList.Add(aProduct);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public string GetEmployeeName(string empId)
        {
            try
            {
                string employee = "";
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add( new SqlParameter("@empId" , empId));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_EmployeeDetailsByEmpId", aParameters);
                while (dr.Read())
                {
                    employee = dr["Employee"].ToString();

                }
                return employee;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }



        public TaxConfig GetTaxInfo(int productGroupId, decimal amount)
        {
            try
            {
                TaxConfig aConfig = new TaxConfig();
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@productGroupId", productGroupId));
                aParameters.Add(new SqlParameter("@amount", amount));
                DataTable dt = accessManager.GetDataTable("sp_Get_TaxInfoByGroupNamount", aParameters);
                if (dt != null || dt.Rows.Count > 0)
                {
                    aConfig = Helper.ToListof<TaxConfig>(dt)[0];
                }
                return aConfig;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetReportHeader(int reportHeadId)
        {
            try
            {
                DataTable dt = new DataTable();
                accessManager.SqlConnectionOpen(DataBase.ControlPanel);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@ReportHeadId", reportHeadId));
                dt = accessManager.GetDataTable("sp_Get_Report_Header", aSqlParameterList);

                return dt;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {

                accessManager.SqlConnectionClose();
            }
        }

        public List<ItemDescription> ItemDescriptionsForReceive(string mrrdate)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@MRRdate", mrrdate));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetItemDescriptionForReceiveDate", aParameters);
                List<ItemDescription> ItemDescriptionList = new List<ItemDescription>();
                while (dr.Read())
                {
                    ItemDescription itemDescription = new ItemDescription();
                    itemDescription.ItemId = Convert.ToInt32(dr["ItemId"]);
                    itemDescription.ItemName = dr["ItemName"].ToString();
                    ItemDescriptionList.Add(itemDescription);
                }

                return ItemDescriptionList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<BankMaster> GetBankName()
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_BankName");
                List<BankMaster> _divList = new List<BankMaster>();
                while (dr.Read())
                {
                    BankMaster aDivision = new BankMaster();
                    aDivision.BankId = Convert.ToInt32(dr["BankId"]);
                    aDivision.BankName = dr["BankName"].ToString().Trim();
                    _divList.Add(aDivision);
                }
                return _divList;


            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<BankBranch> GetBankBranch(int bankid)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@Bankid", bankid));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetBankBranch", aParameters);
                List<BankBranch> _divList = new List<BankBranch>();
                while (dr.Read())
                {
                    BankBranch aDivision = new BankBranch();
                    aDivision.BranchId = Convert.ToInt32(dr["BranchId"]);
                    aDivision.BranchName = dr["BranchName"].ToString().Trim();
                    _divList.Add(aDivision);
                }
                return _divList;


            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<VehicleInfo> VehicleInfo()
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetAllVehicle");
                List<VehicleInfo> VehicleInfoList = new List<VehicleInfo>();
                while (dr.Read())
                {
                    VehicleInfo vehicle = new VehicleInfo();
                    vehicle.VehicleInfoId = Convert.ToInt32(dr["VehicleInfoId"]);
                    vehicle.VehicleRegNo = dr["VehicleRegNo"].ToString();
                    VehicleInfoList.Add(vehicle);
                }
                return VehicleInfoList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

        }
        public List<Product> GetProductGroupId(int productId)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@productId", productId));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_ProductGroupId", aParameters);

                List<Product> aList = new List<Product>();
                while (dr.Read())
                {
                    Product aProduct = new Product();
                    aProduct.GroupId = Convert.ToInt32(dr["GroupId"]);
                    aList.Add(aProduct);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<AutoComplete> GetDeliveryManAutoComDALAll(string term)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<AutoComplete> lst = new List<AutoComplete>();

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@Param", term));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetDeliveryAutoComplete", aSqlParameterList);
                AutoComplete autoComplete;
                while (dr.Read())
                {
                    autoComplete = new AutoComplete();
                    autoComplete.value = dr["DeliveryMan"].ToString();
                    //autoComplete.value = dr["EMPID"] + ":" + dr["EmpName"];
                    autoComplete.label = dr["DeliveryMan"].ToString();
                    lst.Add(autoComplete);
                }


                return lst;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<View_EmployeeInfo> GetEmployee()
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_Employee");
                List<View_EmployeeInfo> _divList = new List<View_EmployeeInfo>();
                while (dr.Read())
                {
                    View_EmployeeInfo aEmp = new View_EmployeeInfo();
                    aEmp.EmpNo = Convert.ToInt32(dr["EmpNo"]);
                    aEmp.EmpName =  dr["EmpName"].ToString().Trim();

                    _divList.Add(aEmp);
                }
                return _divList;


            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

       public List<tbl_Reason> GetReason(int reasonFor)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@reasonFor", reasonFor));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetReason", aParameters);
                List<tbl_Reason> _reList = new List<tbl_Reason>();
                while (dr.Read())
                {
                    tbl_Reason aDivision = new tbl_Reason();
                    aDivision.ReasonId = Convert.ToInt32(dr["ReasonId"]);
                    aDivision.Name = dr["Name"].ToString().Trim();
                    _reList.Add(aDivision);
                }
                return _reList;


            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        
        public List<BusinessUnit> BusinessUnitByCompanyId(int companyId)
        {
            accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
            List<SqlParameter> aParameters = new List<SqlParameter>();
            aParameters.Add(new SqlParameter("@companyId", companyId));
            SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetBusinessUnitByCompany", aParameters);
            List<BusinessUnit> businessUnitList = new List<BusinessUnit>();
            while (dr.Read())
            {
                BusinessUnit businessUnit = new BusinessUnit();
                businessUnit.BUId = Convert.ToInt32(dr["BUId"]);
                businessUnit.Name = dr["Name"].ToString();
                businessUnit.ShortName = dr["ShortName"].ToString();
                businessUnitList.Add(businessUnit);
            }
            return businessUnitList;
        }

        public List<FinancialYear> GetFinancialYear()
        {
            accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
            SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetFinancialYear");
            List<FinancialYear> _yList = new List<FinancialYear>();
            while (dr.Read())
            {
                FinancialYear aYear = new FinancialYear();
                aYear.FinancialYearId = Convert.ToInt32(dr["FinancialYearId"]);
                aYear.FYear = dr["FYear"].ToString(); ;

                _yList.Add(aYear);
            }
            return _yList;
        }

        public List<Currency> GetCurrency()
        {
            accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
            SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetCurrency");
            List<Currency> _cList = new List<Currency>();
            while (dr.Read())
            {
                Currency aCur = new Currency();
                aCur.CurrencyId = Convert.ToInt32(dr["CurrencyId"]);
                aCur.CurrencyCode = dr["CurrencyCode"].ToString(); ;

                _cList.Add(aCur);
            }
            return _cList;
        }

        public List<ChartOfAccLayerSeven> GetDebitVoucherGL()
        {
            accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
            SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetDebitVoucherGL");
            List<ChartOfAccLayerSeven> _cList = new List<ChartOfAccLayerSeven>();
            while (dr.Read())
            {
                ChartOfAccLayerSeven aCur = new ChartOfAccLayerSeven();
                aCur.CALayerSevenId = Convert.ToInt32(dr["CALayerSevenId"]);
                aCur.CALayerSevenCode = dr["CALayerSevenCode"].ToString();
                aCur.CALayerSevenName = dr["CALayerSevenName"].ToString();

                _cList.Add(aCur);
            }
            return _cList;
        }
        public List<BankAccount> GetGLBankAccounts()
        {
            accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
            SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetGLBankAccounts");
            List<BankAccount> _cList = new List<BankAccount>();
            while (dr.Read())
            {
                BankAccount aCur = new BankAccount();
                aCur.CALayerSevenId = Convert.ToInt32(dr["CALayerSevenId"]);
                aCur.AccountNo = dr["AccountNo"].ToString();
                _cList.Add(aCur);
            }
            return _cList;
        }
        public List<CashAccount> GetGLCashAccounts()
        {
            accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
            SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetGLCashAccounts");
            List<CashAccount> _cList = new List<CashAccount>();
            while (dr.Read())
            {
                CashAccount aCur = new CashAccount();
                aCur.CALayerSevenId = Convert.ToInt32(dr["CALayerSevenId"]);
                aCur.AccountName = dr["AccountName"].ToString();
                _cList.Add(aCur);
            }
            return _cList;
        }
        public List<ChartOfAccLayerSeven> GetGLForJournal()
        {
            accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
            SqlDataReader dr = accessManager.GetSqlDataReader("GetGLForJournal");
            List<ChartOfAccLayerSeven> _cList = new List<ChartOfAccLayerSeven>();
            while (dr.Read())
            {
                ChartOfAccLayerSeven aCur = new ChartOfAccLayerSeven();
                aCur.CALayerSevenId = Convert.ToInt32(dr["CALayerSevenId"]);
                aCur.CALayerSevenCode = dr["CALayerSevenCode"].ToString();
                aCur.CALayerSevenName = dr["CALayerSevenName"].ToString();

                _cList.Add(aCur);
            }
            return _cList;
        }
        public List<ChartOfAccLayerSeven> GetGLForContra()
        {
            accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
            SqlDataReader dr = accessManager.GetSqlDataReader("GetGLForContra");
            List<ChartOfAccLayerSeven> _cList = new List<ChartOfAccLayerSeven>();
            while (dr.Read())
            {
                ChartOfAccLayerSeven aCur = new ChartOfAccLayerSeven();
                aCur.CALayerSevenId = Convert.ToInt32(dr["CALayerSevenId"]);
                aCur.CALayerSevenCode = dr["CALayerSevenCode"].ToString();
                aCur.CALayerSevenName = dr["CALayerSevenName"].ToString();

                _cList.Add(aCur);
            }
            return _cList;
        }
        public List<ChartOfAccLayerSeven> GetAllGL()
        {
            accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
            SqlDataReader dr = accessManager.GetSqlDataReader("GetAllGL");
            List<ChartOfAccLayerSeven> _cList = new List<ChartOfAccLayerSeven>();
            while (dr.Read())
            {
                ChartOfAccLayerSeven aCur = new ChartOfAccLayerSeven();
                aCur.CALayerSevenId = Convert.ToInt32(dr["CALayerSevenId"]);
                aCur.CALayerSevenCode = dr["CALayerSevenCode"].ToString();
                aCur.CALayerSevenName = dr["CALayerSevenName"].ToString();

                _cList.Add(aCur);
            }
            return _cList;
        }
        public List<ChartOfAccLayerSeven> GetGLByGlType(int GlType)
        {
            accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
            List<SqlParameter> aParameters = new List<SqlParameter>();
            aParameters.Add(new SqlParameter("@GlType", GlType));
            SqlDataReader dr = accessManager.GetSqlDataReader("GetGLByGlType", aParameters);
            List<ChartOfAccLayerSeven> _cList = new List<ChartOfAccLayerSeven>();
            while (dr.Read())
            {
                ChartOfAccLayerSeven aCur = new ChartOfAccLayerSeven();
                aCur.CALayerSevenId = Convert.ToInt32(dr["CALayerSevenId"]);
                aCur.CALayerSevenCode = dr["CALayerSevenCode"].ToString();
                aCur.CALayerSevenName = dr["CALayerSevenName"].ToString();

                _cList.Add(aCur);
            }
            return _cList;
        }

        public List<Department> GetDepartment()
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();

                SqlDataReader dr = accessManager.GetSqlDataReader("GetAllDepartment");
                List<Department> _divList = new List<Department>();
                while (dr.Read())
                {
                    Department aDivision = new Department();
                    aDivision.DepartmentId = Convert.ToInt32(dr["DepartmentId"]);
                    aDivision.DepartmentName = dr["DepartmentName"].ToString().Trim();
                    _divList.Add(aDivision);
                }
                return _divList;


            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }


        public List<View_EmployeeInfo> GetIgFoodsEmployee()
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();

                SqlDataReader dr = accessManager.GetSqlDataReader("GetIgFoodsEmployee");
                List<View_EmployeeInfo> _divList = new List<View_EmployeeInfo>();
                while (dr.Read())
                {
                    View_EmployeeInfo aEmp = new View_EmployeeInfo();
                    aEmp.EMPID = dr["EMPID"].ToString();
                    aEmp.EmpName = dr["EmpName"].ToString().Trim();

                    _divList.Add(aEmp);
                }
                return _divList;


            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<tbl_ProductCategory> GetCategoryByGroupId(int groupId)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@GroupId", groupId));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_CategoryByGroupId", aParameters);

                List<tbl_ProductCategory> aList = new List<tbl_ProductCategory>();
                while (dr.Read())
                {
                    tbl_ProductCategory aProduct = new tbl_ProductCategory();
                    aProduct.CategoryId = Convert.ToInt32(dr["CategoryId"]);
                    aProduct.CategoryName = dr["CategoryName"].ToString();
                    aList.Add(aProduct);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<Product> GetProductByCategoryId(int categoryId)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CategoryId", categoryId));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_ProductByCategoryId", aParameters);

                List<Product> aList = new List<Product>();
                while (dr.Read())
                {
                    Product aProduct = new Product();
                    aProduct.ProductId = Convert.ToInt32(dr["ProductId"]);
                    aProduct.ProductName = dr["ProductName"].ToString();
                    aList.Add(aProduct);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<WareHouse> WareHouseInformationMto(int loginId, int locid = 0)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@LocationId", locid));

                parameters.Add(new SqlParameter("@loginId", loginId));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetWareHouseByLocationMTO", parameters);
                List<WareHouse> wareHouseList = new List<WareHouse>();
                while (dr.Read())
                {
                    WareHouse wareHouse = new WareHouse();
                    wareHouse.WareHouseId = Convert.ToInt32(dr["WareHouseId"]);
                    wareHouse.WareHouseName = dr["WareHouseName"].ToString();
                    wareHouseList.Add(wareHouse);
                }
                return wareHouseList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

        }

        public List<CostCenterM> GetCostCenter()
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();

                SqlDataReader dr = accessManager.GetSqlDataReader("SP_GetCostCenter");
                List<CostCenterM> _centerMs = new List<CostCenterM>();
                while (dr.Read())
                {
                    CostCenterM _costCenterM = new CostCenterM();
                    _costCenterM.CostCenterId = Convert.ToInt32(dr["CostCenterId"]);
                    _costCenterM.CostCenterName = dr["CostCenterName"].ToString();
                    _centerMs.Add(_costCenterM);
                }
                return _centerMs;


            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }


        public List<CashAccount> GetGLForRightOff()
        {
            accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
            SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetGLForRightOff");
            List<CashAccount> _cList = new List<CashAccount>();
            while (dr.Read())
            {
                CashAccount aCur = new CashAccount();
                aCur.CALayerSevenId = Convert.ToInt32(dr["CALayerSevenId"]);
                aCur.AccountName = dr["AccountName"].ToString();
                _cList.Add(aCur);
            }
            return _cList;
        }

        public List<ChartOfAccLayerSeven> GetIOUAccounts( string empId) 
        {
            accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);

            List<SqlParameter> aParameters = new List<SqlParameter>();
            aParameters.Add(new SqlParameter("@empId", empId));
            SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetIOUAccounts", aParameters);
            List<ChartOfAccLayerSeven> _cList = new List<ChartOfAccLayerSeven>();
            while (dr.Read())
            {
                ChartOfAccLayerSeven aCur = new ChartOfAccLayerSeven();
                aCur.CALayerSevenId = Convert.ToInt32(dr["CALayerSevenId"]);
                aCur.CALayerSevenName = dr["CALayerSevenName"].ToString();

                _cList.Add(aCur);
            }
            return _cList;
        }


    }
}
