﻿using IGFoodProject.DataManager;
using IGFoodProject.Models;
using IGFoodProject.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IGFoodProject.DAL
{
    public class SupplierPaymentOtherDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();
        public List<ViewInvoiceMaster> InvoiceListForPayment(int supplierId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();

                List<ViewInvoiceMaster> aList = new List<ViewInvoiceMaster>();

                aParameters.Add(new SqlParameter("@supplierId", supplierId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetInvoiceForPayment", aParameters);
                while (dr.Read())
                {
                    ViewInvoiceMaster aMaster = new ViewInvoiceMaster();

                    aMaster.InvoiceMasterID = Convert.ToInt32(dr["InvoiceMasterID"]);
                    aMaster.InvoiceNo = dr["InvoiceNo"].ToString();
                    aMaster.InvoiceDate = Convert.ToDateTime(dr["InvoiceDate"]);
                    aMaster.TotalDue = Convert.ToDecimal(dr["TotalDue"]);
                    aMaster.TotalPaid = Convert.ToDecimal(dr["TotalPaid"]);
                    aMaster.TotalTax = Convert.ToDecimal(dr["TotalTax"]);
                    aMaster.TotalTaxPaid = Convert.ToDecimal(dr["TotalTaxPaid"]);
                    aMaster.GrandTotal = Convert.ToDecimal(dr["GrandTotal"]);
                    aMaster.InvoiceType = dr["InvoiceType"].ToString();
                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public bool SaveSupplierPaymentDetails(SupplierPaymentOtherMaster aMaster)
        {
            bool result = false;
            try

            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                int pkId = SaveSupplierPaymentrMaster(aMaster);


                foreach (SupplierPaymentOtherDetail item in aMaster.SupplierPaymentOtherDetails)
                {
                    List<SqlParameter> gSqlParameterList = new List<SqlParameter>();

                    gSqlParameterList.Add(new SqlParameter("@SupplierPaymentOtherMasterId", pkId));
                    gSqlParameterList.Add(new SqlParameter("@paymentType", item.PaymentType));
                    gSqlParameterList.Add(new SqlParameter("@restPaymentAmmount", item.PaymentAmount));
                    gSqlParameterList.Add(new SqlParameter("@InvoiceMasterID", item.InvoiceMasterID));
                    gSqlParameterList.Add(new SqlParameter("@CurrencyId", item.CurrencyId));
                    gSqlParameterList.Add(new SqlParameter("@ConversionRate", item.ConversionRate));
                    gSqlParameterList.Add(new SqlParameter("@FromAccount", item.FromAccount));
                    gSqlParameterList.Add(new SqlParameter("@refNo", item.RefNo));
                    gSqlParameterList.Add(new SqlParameter("@refDate", item.RefDate));
                    gSqlParameterList.Add(new SqlParameter("@BankId", item.BankId));
                    gSqlParameterList.Add(new SqlParameter("@BranchId", item.BranchId));
                    gSqlParameterList.Add(new SqlParameter("@Remark", item.Remark));
                    gSqlParameterList.Add(new SqlParameter("@TaxAmount", item.TaxAmount));
                    gSqlParameterList.Add(new SqlParameter("@GrandTotalD", item.GrandTotalD));
                    gSqlParameterList.Add(new SqlParameter("@InvoiceType", item.InvoiceType));
                    gSqlParameterList.Add(new SqlParameter("@IOUAccount", item.IOUAccount));
                    gSqlParameterList.Add(new SqlParameter("@RefDetailsId", item.RefDetailsId));
                    result = _accessManager.SaveData("sp_SaveSupplierPaymentOtherDetails", gSqlParameterList);



                }

                if (result == true &&
                        (aMaster.PaymentTypeId == 1 ||
                        aMaster.PaymentTypeId == 2 ||
                        aMaster.PaymentTypeId == 3 ||
                        aMaster.PaymentTypeId == 4 ||
                        aMaster.PaymentTypeId == 5 ||
                        aMaster.PaymentTypeId == 6 ||
                        aMaster.PaymentTypeId == 8 ||
                        aMaster.PaymentTypeId == 18 ||
                        aMaster.PaymentTypeId == 19))
                {
                    result = UpdateVocherMaster(pkId, aMaster.CreateBy);
                }
                if (result == true && aMaster.PaymentTypeId == 11)
                {
                    result = CreateIouAdjustVoucher(pkId, aMaster.CreateBy);
                }


            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
            return result;
        }

        public int SaveSupplierPaymentrMaster(SupplierPaymentOtherMaster aMaster)
        {
            try
            {
                string empReceived = string.Empty;
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@SupplierId", aMaster.SupplierId));
                aSqlParameterList.Add(new SqlParameter("@PaymentAmount", aMaster.PaymentAmount));
                aSqlParameterList.Add(new SqlParameter("@PaymentDate", aMaster.PaymentDate));

                aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@CreateDate", System.DateTime.Now));
                aSqlParameterList.Add(new SqlParameter("@Remark", aMaster.Remark));
                aSqlParameterList.Add(new SqlParameter("@TaxAmount", aMaster.TaxAmount));

                aSqlParameterList.Add(new SqlParameter("@Discount", aMaster.Discount)); 


                //aSqlParameterList.Add(new SqlParameter("@TaxPercent", aMaster.TaxPercent));

                aSqlParameterList.Add(new SqlParameter("@GrandTotal", aMaster.TotalAfterDiscount)); 

                return _accessManager.SaveDataReturnPrimaryKey("sp_SaveSupplierPaymentOtherMaster", aSqlParameterList);
            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }
        }

        public bool UpdateVocherMaster(int SupplierPaymentId, string entryby)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@SupplierPaymentMasterId", SupplierPaymentId));
                aParameters.Add(new SqlParameter("@EntryBy", entryby));
                aParameters.Add(new SqlParameter("@EntryDate", DateTime.Now));
                result = _accessManager.SaveData("sp_Save_DebitVoucherMaster_SupllierPaymentOther", aParameters);

                return result;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool CreateIouAdjustVoucher(int SupplierPaymentId, string entryby)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@SupplierPaymentMasterId", SupplierPaymentId));
                aParameters.Add(new SqlParameter("@EntryBy", entryby));
                aParameters.Add(new SqlParameter("@EntryDate", DateTime.Now));
               // aParameters.Add(new SqlParameter("@GLRefDetailsIdFrom", GLRefDetailsIdFrom));
                result = _accessManager.SaveData("IouAdjustVoucherOnPayment", aParameters);

                return result;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public DataTable GetSupplierVoucherLedger(DateTime fromDate, DateTime todDate, int glId, int refDetailsId = 0)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@FromDate", fromDate));
                aParameters.Add(new SqlParameter("@ToDate", todDate));
                aParameters.Add(new SqlParameter("@GlId", glId));
                aParameters.Add(new SqlParameter("@RefDetailsId", refDetailsId));
                DataTable dt = _accessManager.GetDataTable("sp_GetSupplierVoucherLedger", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetSupplierTdsVoucherLedger(DateTime fromDate, DateTime todDate, int glId, int refDetailsId = 0,int sourchId =0)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@FromDate", fromDate));
                aParameters.Add(new SqlParameter("@ToDate", todDate));
                aParameters.Add(new SqlParameter("@GlId", glId));
                aParameters.Add(new SqlParameter("@RefDetailsId", refDetailsId));
                aParameters.Add(new SqlParameter("@SupplierOrCustomerOrEmployee", sourchId));
                DataTable dt = _accessManager.GetDataTable("sp_GetTDSPayableLedger", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

    }
}