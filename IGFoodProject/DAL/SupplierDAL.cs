﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using IGFoodProject.DataManager;
using IGFoodProject.Models;

namespace IGFoodProject.DAL
{
    public class SupplierDAL
    {

        DataAccessManager _accessManager = new DataAccessManager();

        public ResultResponse SaveSupplier(SupplierInfo aInfo, string user)
        {
            try
            {

                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add( new SqlParameter("@SupplierId", aInfo.SupplierId));
                aParameters.Add( new SqlParameter("@SuplierTypeId", aInfo.SuplierTypeId));
                aParameters.Add( new SqlParameter("@SupplierName", aInfo.SupplierName));
                aParameters.Add( new SqlParameter("@ProprietorName", aInfo.ProprietorName));
                aParameters.Add( new SqlParameter("@MailingAddress", aInfo.MailingAddress));
                aParameters.Add( new SqlParameter("@PhysicalAddress", aInfo.PhysicalAddress));
                aParameters.Add( new SqlParameter("@SupplierPhone", aInfo.SupplierPhone));
                aParameters.Add( new SqlParameter("@SupplierSecondPhone", aInfo.SupplierSecondPhone));
               
                aParameters.Add( new SqlParameter("@BankAccount", aInfo.BankAccount));
                aParameters.Add( new SqlParameter("@GSTNo", aInfo.GSTNo));
                aParameters.Add( new SqlParameter("@CreditLimit", aInfo.CreditLimit));
                aParameters.Add( new SqlParameter("@ContactPerson", aInfo.ContactPerson));
                aParameters.Add( new SqlParameter("@Remarks", aInfo.Remarks));
               // aParameters.Add( new SqlParameter("@ContactPersonPhone", aInfo.ContactPersonPhone));
                aParameters.Add( new SqlParameter("@IsActive", aInfo.IsActive));

                if (aInfo.SupplierId == 0)
                {
                    aParameters.Add(new SqlParameter("@CreateBy", user));
                    aParameters.Add(new SqlParameter("@CreateDate", DateTime.Now));
                }
                else
                {
                    aParameters.Add(new SqlParameter("@UpdateBy", user));
                    aParameters.Add(new SqlParameter("@UpdateDate", DateTime.Now));
                }

                aResponse.isSuccess = _accessManager.SaveData("sp_Save_SupplierInfo", aParameters);
                return aResponse;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

        public List<SupplierInfo> GetSuplierType()
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetSuplierType");
                List<SupplierInfo> _divList = new List<SupplierInfo>();
                while (dr.Read())
                {
                    SupplierInfo aEmp = new SupplierInfo();
                    aEmp.SuplierTypeId = Convert.ToInt32(dr["SuplierTypeId"]);
                    aEmp.SuplierType = dr["SuplierType"].ToString().Trim();

                    _divList.Add(aEmp);
                }
                return _divList;


            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<SupplierInfo> GetSupplierList(int SupplierId = 0)
        {
            try
            {

                List<SupplierInfo> aList = new List<SupplierInfo>();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add( new SqlParameter("@SupplierId" , SupplierId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_SupplierInfo", aParameters);
                while (dr.Read())
                {
                    SupplierInfo aInfo = new SupplierInfo();
                    aInfo.SupplierId = Convert.ToInt32(dr["SupplierId"]);
                    aInfo.SuplierTypeId = Convert.ToInt32(dr["SupplierTypeId"]);
                    aInfo.CreditLimit = DBNull.Value == dr["CreditLimit"] ? 0 : Convert.ToDecimal(dr["CreditLimit"]);
                    aInfo.SupplierName = dr["SupplierName"].ToString();
                    aInfo.ProprietorName = dr["ProprietorName"].ToString();
                    aInfo.SupplierCode = dr["SupplierCode"].ToString();
                    aInfo.PhysicalAddress = dr["PhysicalAddress"].ToString();
                    aInfo.MailingAddress = dr["MailingAddress"].ToString();
                    aInfo.SupplierPhone = dr["SupplierPhone"].ToString();
                    aInfo.SupplierSecondPhone = dr["SupplierSecondPhone"].ToString();
                    aInfo.SupplierEmail = dr["SupplierEmail"].ToString();
                    aInfo.ContactPerson = dr["ContactPerson"].ToString();
                    aInfo.Remarks = dr["Remarks"].ToString();
                    
                    aInfo.GSTNo = dr["GSTNo"].ToString();
                    aInfo.BankAccount = dr["BankAccount"].ToString();
                    aInfo.CreateBy = dr["CreateBy"].ToString();
                    aInfo.UpdateBy = dr["UpdateBy"].ToString();
                    aInfo.CreateDate = dr["CreateDate"] as DateTime?;
                    aInfo.UpdateDate = dr["UpdateDate"] as DateTime?;
                    aInfo.IsActive = Convert.ToBoolean(dr["IsActive"]);

                    aList.Add(aInfo);
                }

                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        } 
    }
}