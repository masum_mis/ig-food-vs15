﻿using IGFoodProject.DataManager;
using IGFoodProject.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.DAL
{
    public class ProductDAL
    {
        DataAccessManager accessManager = new DataAccessManager();
        public List<tbl_ProductGroup> GetGroupList()
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<tbl_ProductGroup> _List = new List<tbl_ProductGroup>();

                //List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetProductGroup");
                while (dr.Read())
                {
                    tbl_ProductGroup aInfo = new tbl_ProductGroup();
                    aInfo.GroupId = (int)dr["GroupId"];
                    aInfo.GroupName = dr["GroupName"].ToString();
                    aInfo.GroupCode = dr["GroupCode"].ToString();
                    if (dr["CreateDate"] != DBNull.Value)
                    {
                        aInfo.CreateDate = Convert.ToDateTime(dr["CreateDate"]);
                    }
                    aInfo.CreateBy = dr["CreateBy"].ToString();

                    aInfo.UpdateBy = dr["UpdateBy"].ToString();
                    if (dr["UpdateDate"] != DBNull.Value)
                    {
                        aInfo.UpdateDate = Convert.ToDateTime(dr["UpdateDate"]);
                    }
                    if (dr["IsActive"] != DBNull.Value)
                    {
                        aInfo.IsActive = Convert.ToBoolean(dr["IsActive"]);
                    }
                    


                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public int CheckGroup(string group, string gcode)
        {
            int pgroup = 0;
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@group", group));
                aSqlParameterList.Add(new SqlParameter("@gcode", gcode));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_CheckGroup", aSqlParameterList);

                while (dr.Read())
                {
                    pgroup = Convert.ToInt32(dr["pgroup"]);
                }
            }
            catch (Exception exception)
            {
                //accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
            return pgroup;
        }

        public bool SaveGroup(tbl_ProductGroup aMaster)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@GroupId", aMaster.GroupId));
                aSqlParameterList.Add(new SqlParameter("@GroupName", aMaster.GroupName));
                aSqlParameterList.Add(new SqlParameter("@GroupCode", aMaster.GroupCode));
                aSqlParameterList.Add(new SqlParameter("@CreateDate", System.DateTime.Now));
                aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@UpdateDate", System.DateTime.Now));
                aSqlParameterList.Add(new SqlParameter("@UpdateBy", aMaster.UpdateBy));
                aSqlParameterList.Add(new SqlParameter("@IsActive", aMaster.IsActive));



                return accessManager.SaveData("sp_SaveGroup", aSqlParameterList);
            }
            catch (Exception exception)
            {
                accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public tbl_ProductGroup GetGroupForEdit(int id)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@GroupId", id));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetGroupByID", aParameters);
                tbl_ProductGroup aDetailsView = new tbl_ProductGroup();
                while (dr.Read())
                {


                    aDetailsView.GroupId = (int)dr["GroupId"];
                    aDetailsView.GroupName = dr["GroupName"].ToString();
                    aDetailsView.GroupCode = dr["GroupCode"].ToString();
                    aDetailsView.IsActive = (bool)dr["IsActive"];


                }
                return aDetailsView;

            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public bool DeleteGroup(int id)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@GroupID", id));

                return accessManager.SaveData("sp_DeleteGroup", aSqlParameterList);
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        ////////////product type start
        ///
        public List<tbl_ProductType> GetTypeList()
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<tbl_ProductType> _List = new List<tbl_ProductType>();

                //List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetProductType");
                while (dr.Read())
                {
                    tbl_ProductType aInfo = new tbl_ProductType();
                    aInfo.TypeId = (int)dr["TypeId"];
                    aInfo.GroupId = (int)dr["GroupId"];
                    aInfo.TypeName = dr["TypeName"].ToString();
                    aInfo.GroupName = dr["GroupName"].ToString();
                    if (dr["CreateDate"] != DBNull.Value)
                    {
                        aInfo.CreateDate = Convert.ToDateTime(dr["CreateDate"]);
                    }
                    aInfo.CreateBy = dr["CreateBy"].ToString();

                    aInfo.UpdateBy = dr["UpdateBy"].ToString();
                    if (dr["UpdateDate"] != DBNull.Value)
                    {
                        aInfo.UpdateDate = Convert.ToDateTime(dr["UpdateDate"]);
                    }
                    if (dr["IsActive"] != DBNull.Value)
                    {
                        aInfo.IsActive = Convert.ToBoolean(dr["IsActive"]);
                    }


                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public int CheckType(string ptype)
        {
            int p_type = 0;
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@type", ptype));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_CheckType", aSqlParameterList);

                while (dr.Read())
                {
                    p_type = Convert.ToInt32(dr["p_type"]);
                }
            }
            catch (Exception exception)
            {
                //accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
            return p_type;
        }

        public bool SaveType(tbl_ProductType aMaster)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@TypeId", aMaster.TypeId));
                aSqlParameterList.Add(new SqlParameter("@GroupId", aMaster.GroupId));
                aSqlParameterList.Add(new SqlParameter("@TypeName", aMaster.TypeName));
                aSqlParameterList.Add(new SqlParameter("@CreateDate", System.DateTime.Now));
                aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@UpdateDate", System.DateTime.Now));
                aSqlParameterList.Add(new SqlParameter("@UpdateBy", aMaster.UpdateBy));
                aSqlParameterList.Add(new SqlParameter("@IsActive", aMaster.IsActive));



                return accessManager.SaveData("sp_SaveType", aSqlParameterList);
            }
            catch (Exception exception)
            {
                accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public tbl_ProductType GetTypeForEdit(int id)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@TypeId", id));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetTypeByID", aParameters);
                tbl_ProductType aDetailsView = new tbl_ProductType();
                while (dr.Read())
                {

                    aDetailsView.TypeId = (int)dr["TypeId"];
                    aDetailsView.GroupId = (int)dr["GroupId"];
                    aDetailsView.TypeName = dr["TypeName"].ToString();
                    aDetailsView.IsActive = (bool)dr["IsActive"];


                }
                return aDetailsView;

            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public bool DeleteType(int id)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@TypeID", id));

                return accessManager.SaveData("sp_DeleteType", aSqlParameterList);
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        ////////////////////////////product start
        ///
        /// 
        ///

        public List<Product> GetProductList(int groupId = 0)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<Product> _List = new List<Product>();

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@groupId", groupId));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetProductList", aSqlParameterList);
                while (dr.Read())
                {
                    Product aInfo = new Product();
                    aInfo.GroupId = (int)dr["GroupId"];
                    aInfo.TypeId = (int)dr["TypeId"];
                    aInfo.ProductId = (int)dr["ProductId"];
                    aInfo.CategoryId = (int)dr["CategoryId"];
                    aInfo.BrandId = (int)dr["BrandId"];
                    aInfo.PackSizeId = (int)dr["PackSizeId"];
                    aInfo.UOMId = (int)dr["UOMId"];
                    aInfo.ProductNo = dr["ProductNo"].ToString();
                    aInfo.ProductName = dr["ProductName"].ToString();
                    aInfo.ProductDescription = dr["ProductDescription"].ToString();
                    aInfo.CategoryName = dr["CategoryName"].ToString();
                    aInfo.BrandName = dr["BrandName"].ToString();
                    aInfo.PackSizeName = dr["PackSizeName"].ToString();
                    aInfo.UOMName = dr["UOMName"].ToString();

                    aInfo.GroupName = dr["GroupName"].ToString();
                    aInfo.TypeName = dr["TypeName"].ToString();

                    if (dr["CreateDate"] != DBNull.Value)
                    {
                        aInfo.CreateDate = Convert.ToDateTime(dr["CreateDate"]);
                    }
                    aInfo.CreateBy = dr["CreateBy"].ToString();

                    aInfo.UpdateBy = dr["UpdateBy"].ToString();
                    if (dr["UpdateDate"] != DBNull.Value)
                    {
                        aInfo.UpdateDate = Convert.ToDateTime(dr["UpdateDate"]);
                    }
                    if (dr["IsActive"] != DBNull.Value)
                    {
                        aInfo.IsActive = Convert.ToBoolean(dr["IsActive"]);
                    }if (dr["IsBulk"] != DBNull.Value)
                    {
                        aInfo.IsBulk = Convert.ToBoolean(dr["IsBulk"]);
                    }

                    if (dr["PerKgStdQty"] != DBNull.Value)
                    {
                        aInfo.PerKgStdQty = Convert.ToDecimal(dr["PerKgStdQty"]);
                    }
                    if (dr["ProductSerialId"] != DBNull.Value)
                    {
                        aInfo.ProductSerialId = Convert.ToInt32(dr["ProductSerialId"]);
                    }
                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public int CheckProduct(string group)
        {
            int pgroup = 0;
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@group", group));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_CheckProduct", aSqlParameterList);

                while (dr.Read())
                {
                    pgroup = Convert.ToInt32(dr["pgroup"]);
                }
            }
            catch (Exception exception)
            {
                //accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
            return pgroup;
        }

        public int SaveProduct(Product aMaster)
        {
            int result = 0;
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@ProductId", aMaster.ProductId));
                aSqlParameterList.Add(new SqlParameter("@GroupId", aMaster.GroupId));
                aSqlParameterList.Add(new SqlParameter("@TypeId", aMaster.TypeId));
                aSqlParameterList.Add(new SqlParameter("@CategoryId", aMaster.CategoryId));
                aSqlParameterList.Add(new SqlParameter("@PackSizeId", aMaster.PackSizeId));
                aSqlParameterList.Add(new SqlParameter("@BrandId", aMaster.BrandId));
                aSqlParameterList.Add(new SqlParameter("@UOMId", aMaster.UOMId));
                aSqlParameterList.Add(new SqlParameter("@ProductName", aMaster.ProductName));
                aSqlParameterList.Add(new SqlParameter("@ProductDescription", aMaster.ProductDescription));
                aSqlParameterList.Add(new SqlParameter("@CreateDate", System.DateTime.Now));
                aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@UpdateDate", System.DateTime.Now));
                aSqlParameterList.Add(new SqlParameter("@UpdateBy", aMaster.UpdateBy));
                aSqlParameterList.Add(new SqlParameter("@IsActive", aMaster.IsActive));
                aSqlParameterList.Add(new SqlParameter("@IsBulk", aMaster.IsBulk));
                aSqlParameterList.Add(new SqlParameter("@PerKgStdQty", aMaster.PerKgStdQty));
                aSqlParameterList.Add(new SqlParameter("@ProductSerialId", aMaster.ProductSerialId));


                result = accessManager.SaveDataReturnPrimaryKey("sp_SaveProduct", aSqlParameterList);
            }
            catch (Exception exception)
            {
                accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
            return result;
        }

        public Product GetProductForEdit(int id)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@productid", id));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetProductByID", aParameters);
                Product aDetailsView = new Product();
                while (dr.Read())
                {


                    aDetailsView.ProductId = (int)dr["ProductId"];
                    aDetailsView.GroupId = (int)dr["GroupId"];
                    aDetailsView.TypeId = (int)dr["TypeId"];
                    aDetailsView.CategoryId = (int)dr["CategoryId"];
                    aDetailsView.BrandId = (int)dr["BrandId"];
                    aDetailsView.PackSizeId = (int)dr["PackSizeId"];
                    aDetailsView.UOMId = (int)dr["UOMId"];
                    aDetailsView.ProductNo = dr["ProductNo"].ToString();
                    aDetailsView.ProductName = dr["ProductName"].ToString();
                    aDetailsView.ProductName1 = dr["ProductName1"].ToString();
                    aDetailsView.ProductDescription = dr["ProductDescription"].ToString();
                    aDetailsView.IsActive = (bool)dr["IsActive"];
                    aDetailsView.IsBulk = DBNull.Value != dr["IsBulk"] && (bool)dr["IsBulk"];
                    aDetailsView.PerKgStdQty = DBNull.Value == dr["PerKgStdQty"] ? 0 : Convert.ToDecimal(dr["PerKgStdQty"]);
                    aDetailsView.ProductSerialId = DBNull.Value == dr["ProductSerialId"] ? 0 : Convert.ToInt32(dr["ProductSerialId"]);


                }
                return aDetailsView;

            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<CustomerType> GetCustomerType()
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetCustomerType");
                List<CustomerType> customersType = new List<CustomerType>();
                while (dr.Read())
                {
                    CustomerType customerType = new CustomerType();
                    customerType.CustomerTypeID = Convert.ToInt32(dr["CustomerTypeID"]);
                    customerType.CoustomerTypeName = dr["CoustomerTypeName"].ToString();
                    customersType.Add(customerType);
                }
                return customersType;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public bool DeleteBrand(int id)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@BrandId", id));

                return accessManager.SaveData("sp_DeleteBrand", aSqlParameterList);
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public List<tbl_ProductCategory> GetCategoryListByType(int typeid)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<tbl_ProductCategory> _List = new List<tbl_ProductCategory>();

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@typeid", typeid));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetProductCategoryByTpeID", aSqlParameterList);
                while (dr.Read())
                {
                    tbl_ProductCategory aInfo = new tbl_ProductCategory();

                    aInfo.CategoryId = (int)dr["CategoryId"];

                    aInfo.CategoryName = dr["CategoryName"].ToString();



                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<tbl_ProductBrand> GetBrandList()
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<tbl_ProductBrand> _List = new List<tbl_ProductBrand>();

                //List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                //aSqlParameterList.Add(new SqlParameter("@typeid", typeid));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetProductBrandList");
                while (dr.Read())
                {
                    tbl_ProductBrand aInfo = new tbl_ProductBrand();

                    aInfo.BrandId = (int)dr["BrandId"];

                    aInfo.BrandName = dr["BrandName"].ToString();



                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<tbl_ProductPackSize> GetPackSizeList()
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<tbl_ProductPackSize> _List = new List<tbl_ProductPackSize>();

                //List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                //aSqlParameterList.Add(new SqlParameter("@typeid", typeid));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetPackSizeList");
                while (dr.Read())
                {
                    tbl_ProductPackSize aInfo = new tbl_ProductPackSize();

                    aInfo.PackSizeId = (int)dr["PackSizeId"];

                    aInfo.PackSizeName = dr["PackSizeName"].ToString();



                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }


        public List<tbl_UOM> GetUOMList()
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<tbl_UOM> _List = new List<tbl_UOM>();

                //List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                //aSqlParameterList.Add(new SqlParameter("@typeid", typeid));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetUOMList");
                while (dr.Read())
                {
                    tbl_UOM aInfo = new tbl_UOM();

                    aInfo.UOMId = (int)dr["UOMId"];

                    aInfo.UOMName = dr["UOMName"].ToString();



                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public string GetProductNo(int id)
        {
            string productno = string.Empty;
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ProductID", id));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetProductNo", aParameters);

                while (dr.Read())
                {


                    productno = dr["ProductNo"].ToString();



                }
                return productno;

            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public int CheckProductSerial(int pserial, int? productId)
        {
            int serial = 0;
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@pserial", pserial));
                aSqlParameterList.Add(new SqlParameter("@productId", productId));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_CheckProductSerial", aSqlParameterList);

                while (dr.Read())
                {
                    serial = Convert.ToInt32(dr["pserial"]);
                }
            }
            catch (Exception exception)
            {
                //accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
            return serial;
        }

    }
}
