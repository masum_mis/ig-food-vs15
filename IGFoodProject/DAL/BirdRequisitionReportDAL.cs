﻿using IGFoodProject.DataManager;
using IGFoodProject.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using IGFoodProject.Models;

namespace IGFoodProject.DAL
{
    public class BirdRequisitionReportDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();
        public List<RequisitionMaster> GetRequisitionApproved(DateTime fromDate, DateTime toDate, int company, int location, int warehouse)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CompanyId", company));
                aParameters.Add(new SqlParameter("@LocationId", location));
                aParameters.Add(new SqlParameter("@WarehouseId", warehouse));
                aParameters.Add(new SqlParameter("@FromDate", fromDate));
                aParameters.Add(new SqlParameter("@ToDate", toDate));
               
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetRequisitionReportInvoiceList", aParameters);
                List<RequisitionMaster> aList = new List<RequisitionMaster>();

                while (dr.Read())
                {
                    RequisitionMaster aMaster = new RequisitionMaster();
                    aMaster.RequisitionNo = dr["RequisitionNo"].ToString();
                    aMaster.RequisitionBy = dr["RequisitionBy"].ToString();
                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.CompanyName = dr["CompanyShortName"].ToString();
                    aMaster.RequisitionDate = dr["RequisitionDate"].ToString();
                    aMaster.ExpectedPurchaseDate = dr["ExpectedPurchaseDate"].ToString();
                    aMaster.TotalQty = Convert.ToDecimal(dr["TotalQty"]);
                    aMaster.ItemRequisitionMasterId = Convert.ToInt32(dr["ItemRequisitionMasterId"]);
                    aMaster.ApprovedTentativePrice = Convert.ToDecimal(dr["ApprovedTentativePrice"]);
                    aMaster.TotalKG = Convert.ToDecimal(dr["TotalKG"]);
                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
    }

}