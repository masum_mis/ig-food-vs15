﻿using IGFoodProject.DataManager;
using IGFoodProject.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IGFoodProject.DAL
{
    public class ProcessPriceConfigDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();
        public List<ProceesPriceConfig> GetPurchasePrice(DateTime orderdate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@orderdate", orderdate));
                

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetPurchaseAvgPrice", aParameters);
                List<ProceesPriceConfig> aList = new List<ProceesPriceConfig>();

                while (dr.Read())
                {
                    ProceesPriceConfig aMaster = new ProceesPriceConfig();
                    if (dr["ItemCategoryId"] != DBNull.Value)
                    {
                        aMaster.ItemCategoryId = Convert.ToInt32(dr["ItemCategoryId"]);
                    }
                    aMaster.ItemCategoryName = dr["ItemCategoryName"].ToString();
                    if(dr["UnitPrice"] !=DBNull.Value)
                    {
                        aMaster.UnitPrice = Convert.ToDecimal(dr["UnitPrice"]);
                    }
                    if (dr["oldFlag"] != DBNull.Value)
                    {
                        aMaster.oldFlag = Convert.ToInt32(dr["oldFlag"]);
                    }
                    if (dr["orderdate"] != DBNull.Value)
                    {
                        aMaster.orderdate = Convert.ToDateTime(dr["orderdate"]);
                    }
                   
                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public List<ProceesPriceConfig> GetPurchasePrice_Egg(DateTime orderdate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@orderdate", orderdate));


                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetPurchaseAvgPrice_Egg", aParameters);
                List<ProceesPriceConfig> aList = new List<ProceesPriceConfig>();

                while (dr.Read())
                {
                    ProceesPriceConfig aMaster = new ProceesPriceConfig();
                    if (dr["ItemCategoryId"] != DBNull.Value)
                    {
                        aMaster.ItemCategoryId = Convert.ToInt32(dr["ItemCategoryId"]);
                    }
                    aMaster.ItemCategoryName = dr["ItemCategoryName"].ToString();
                    if (dr["UnitPrice"] != DBNull.Value)
                    {
                        aMaster.UnitPrice = Convert.ToDecimal(dr["UnitPrice"]);
                    }
                    if (dr["oldFlag"] != DBNull.Value)
                    {
                        aMaster.oldFlag = Convert.ToInt32(dr["oldFlag"]);
                    }
                    if (dr["orderdate"] != DBNull.Value)
                    {
                        aMaster.orderdate = Convert.ToDateTime(dr["orderdate"]);
                    }

                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public bool SaveSalesPriceProcessed(List<ProceesPriceConfig> aMaster)
        {
            try
            {
                bool result = false;
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                foreach(var item in aMaster)
                {
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@ProcessSaleMasterID", item.ProcessSaleMasterID));
                    aParameters.Add(new SqlParameter("@SalePriceDate", item.orderdate));
                    aParameters.Add(new SqlParameter("@ItemCatId", item.ItemCategoryId));
                    aParameters.Add(new SqlParameter("@AvgPurchasePrice", item.UnitPrice));
                    
                    result = _accessManager.SaveData("sp_ProcessSalePrice", aParameters);
                }

                

                return result;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
    }
}