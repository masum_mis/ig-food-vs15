﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using IGFoodProject.DataManager;
using IGFoodProject.ViewModel;

namespace IGFoodProject.DAL
{
    public class TopSheetDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();


      public   List<ViewChallanTopSheet> GetTopSheet(int company, int warehouse, int vehicle, DateTime deliveryDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ViewChallanTopSheet> aList = new List<ViewChallanTopSheet>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@company", company));
                aParameters.Add(new SqlParameter("@warehouse", warehouse));
                aParameters.Add(new SqlParameter("@vehicle", vehicle));
                aParameters.Add(new SqlParameter("@deliveryDate", deliveryDate));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_TopSheetSrarchChallan", aParameters);

                while (dr.Read())
                {
                    ViewChallanTopSheet aSheet = new ViewChallanTopSheet();
                    aSheet.IssueQty = DBNull.Value == dr["IssueQty"] ? 0 : Convert.ToDecimal(dr["IssueQty"]);
                    aSheet.IssueKg = DBNull.Value == dr["IssueKg"] ? 0 : Convert.ToDecimal(dr["IssueKg"]);
                    aSheet.MasterId = DBNull.Value == dr["MasterId"] ? 0 : Convert.ToInt32(dr["MasterId"]);

                    aSheet.VehicleRegNo = dr["VehicleRegNo"].ToString();
                    aSheet.SalesOrderNo = dr["SalesOrderNo"].ToString();
                    aSheet.CustomerName = dr["CustomerName"].ToString();
                    aSheet.WareHouseName = dr["WareHouseName"].ToString();
                    aSheet.SalesOrderChallanNo = dr["SalesOrderChallanNo"].ToString();
                    aSheet.DeliveryDestination = dr["DeliveryDestination"].ToString();
                    aSheet.Gateway = dr["Gateway"].ToString();
                    aSheet.DeliveryDate = dr["DeliveryDate"] as DateTime?;
                    aList.Add(aSheet);
                }
                return aList;


            }
            catch (Exception)
            {
                _accessManager.SqlConnectionClose();
                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public DataTable GenerateTopSheet(string chList, string stList)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@chList", chList));
                aParameters.Add(new SqlParameter("@stList", stList));
                DataTable dt = _accessManager.GetDataTable("sp_Get_GenerateTopSheet", aParameters);
                return dt;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
    }
}