﻿using IGFoodProject.DataManager;
using IGFoodProject.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.DAL
{
    public class ProductPackSizeDAL
    {
        DataAccessManager accessManager = new DataAccessManager();
        public List<tbl_ProductPackSize> GetPackSizeList()
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<tbl_ProductPackSize> _List = new List<tbl_ProductPackSize>();

                //List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetProductPackSize");
                while (dr.Read())
                {
                    tbl_ProductPackSize aInfo = new tbl_ProductPackSize();
                    aInfo.PackSizeId = (int)dr["PackSizeId"];
                    aInfo.PackSizeName = dr["PackSizeName"].ToString();
                    if (dr["CreateDate"] != DBNull.Value)
                    {
                        aInfo.CreateDate = Convert.ToDateTime(dr["CreateDate"]);
                    }
                    aInfo.CreateBy = dr["CreateBy"].ToString();

                    aInfo.UpdateBy = dr["UpdateBy"].ToString();
                    if (dr["UpdateDate"] != DBNull.Value)
                    {
                        aInfo.UpdateDate = Convert.ToDateTime(dr["UpdateDate"]);
                    }
                    if (dr["IsActive"] != DBNull.Value)
                    {
                        aInfo.IsActive = Convert.ToBoolean(dr["IsActive"]);
                    }


                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public int CheckPackSize(string group)
        {
            int pgroup = 0;
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@group", group));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_CheckPackSize", aSqlParameterList);

                while (dr.Read())
                {
                    pgroup = Convert.ToInt32(dr["pgroup"]);
                }
            }
            catch (Exception exception)
            {
                //accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
            return pgroup;
        }

        public bool SavePackSize(tbl_ProductPackSize aMaster)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@PackSizeId", aMaster.PackSizeId));
                aSqlParameterList.Add(new SqlParameter("@PackSizeName", aMaster.PackSizeName));
                aSqlParameterList.Add(new SqlParameter("@PackWeight", aMaster.PackWeight));
                aSqlParameterList.Add(new SqlParameter("@CreateDate", System.DateTime.Now));
                aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@UpdateDate", System.DateTime.Now));
                aSqlParameterList.Add(new SqlParameter("@UpdateBy", aMaster.UpdateBy));
                aSqlParameterList.Add(new SqlParameter("@IsActive", aMaster.IsActive));



                return accessManager.SaveData("sp_SavePackSize", aSqlParameterList);
            }
            catch (Exception exception)
            {
                accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public tbl_ProductPackSize GetPackSizeForEdit(int id)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@PackSizeId", id));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetPackSizeByID", aParameters);
                tbl_ProductPackSize aDetailsView = new tbl_ProductPackSize();
                while (dr.Read())
                {


                    aDetailsView.PackSizeId = (int)dr["PackSizeId"];
                    aDetailsView.PackSizeName = dr["PackSizeName"].ToString();
                    aDetailsView.IsActive = (bool)dr["IsActive"];


                }
                return aDetailsView;

            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public bool DeletePacksize(int id)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@PackSizeId", id));

                return accessManager.SaveData("sp_DeletePackSize", aSqlParameterList);
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
    }
}
