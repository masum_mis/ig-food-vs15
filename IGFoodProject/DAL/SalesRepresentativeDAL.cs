﻿using IGFoodProject.DataManager;
using IGFoodProject.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using IGFoodProject.Models;
using System.Data;
namespace IGFoodProject.DAL
{
    public class SalesRepresentativeDAL
    {
        private DataAccessManager accessManager = new DataAccessManager();

        public SRInfoMaster GetSRDataForEdit(int srId=0,bool isLog=false)
        {
            try
            {


                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameters = new List<SqlParameter>();
                aSqlParameters.Add(new SqlParameter("@srId", srId));
                var a = DateTime.Today;
                SqlDataReader dr;
                if (isLog == true)
                    dr = accessManager.GetSqlDataReader("sp_GetDealerLogListForSR", aSqlParameters);
               
                else
                    dr = accessManager.GetSqlDataReader("sp_GetDealerListForSR", aSqlParameters);

                List<SRDealerDetails> sRDealerDetailsList = new List<SRDealerDetails>();
                SRInfoMaster sRInfoMaster = new SRInfoMaster();
                int count = 1;
                while (dr.Read())
                {
                    SRDealerDetails sRDealerDetails = new SRDealerDetails();
                    tbl_CustomerInformation customerInfo = new tbl_CustomerInformation();

                    if (sRInfoMaster.SrId == 0)
                    {
                        sRInfoMaster.SrId = DBNull.Value.Equals(dr["SrId"]) ? 0 : Convert.ToInt32(dr["SrId"]);
                        sRInfoMaster.SrMobileNumber = DBNull.Value == dr["SrMobileNumber"] ?"" : Convert.ToString(dr["SrMobileNumber"]);
                        sRInfoMaster.SrEmail = DBNull.Value == dr["SrEmail"] ? null : Convert.ToString(dr["SrEmail"]);
                        sRInfoMaster.SrAddress = DBNull.Value == dr["SrAddress"] ? null : Convert.ToString(dr["SrAddress"]);
                        sRInfoMaster.SrName = DBNull.Value == dr["SrName"] ? null : Convert.ToString(dr["SrName"]);
                    }


                    sRDealerDetails.SrDealerDetailsId = DBNull.Value == dr["SrDealerDetailsId"] ? 0 : Convert.ToInt32(dr["SrDealerDetailsId"]);
                    sRDealerDetails.SrId = DBNull.Value == dr["SrId"] ? 0 : Convert.ToInt32(dr["SrId"]);
                    sRDealerDetails.DealerId = DBNull.Value == dr["DealerId"] ? 0 : Convert.ToInt32(dr["DealerId"]);
                    sRDealerDetails.ActiveStatus = DBNull.Value == dr["ActiveStatus"] ? null : Convert.ToString(dr["ActiveStatus"]);
                    if (DBNull.Value != dr["IsActive"]) sRDealerDetails.IsActive =  Convert.ToBoolean(dr["IsActive"]);
                    if(DBNull.Value != dr["ActiveDate"])  sRDealerDetails.ActiveDate = Convert.ToDateTime(dr["ActiveDate"]);
                    if (DBNull.Value != dr["DeactiveDate"]) sRDealerDetails.DeactiveDate = Convert.ToDateTime(dr["DeactiveDate"]);

                    customerInfo.CustomerID = DBNull.Value == dr["CustomerID"] ? 0 : Convert.ToInt32(dr["CustomerID"]);
                    customerInfo.CustomerName = DBNull.Value == dr["CustomerName"] ? null : Convert.ToString(dr["CustomerName"]);
                    customerInfo.CustomerCode = DBNull.Value == dr["CustomerCode"] ? null : Convert.ToString(dr["CustomerCode"]);
                    customerInfo.Phone = DBNull.Value == dr["Phone"] ? null : Convert.ToString(dr["Phone"]);

                    sRDealerDetails.CustomerInformation = customerInfo;

                    sRDealerDetailsList.Add(sRDealerDetails); 

                     count++;
                }

                sRInfoMaster.SRDealerDetailsList = sRDealerDetailsList;


                return sRInfoMaster;

            }
            catch (Exception exception)
            {


                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
       
        static DataTable CreateTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SrDealerDetailsId", typeof(Int32));
            dt.Columns.Add("SrId", typeof(Int32));
            dt.Columns.Add("DealerId", typeof(Int32));
            dt.Columns.Add("IsActive", typeof(bool));
            dt.Columns.Add("ActiveDate", typeof(DateTime));
            dt.Columns.Add("DeactiveDate", typeof(DateTime));
            return dt;
        }
        public ResultResponse SaveOrUpdateSrInformation(SRInfoMaster sRInfoMaster)
        {
            
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> SqlParameterList = new List<SqlParameter>();
                DataTable myTable = CreateTable();
                ResultResponse result = new ResultResponse();

                foreach (SRDealerDetails item in sRInfoMaster.SRDealerDetailsList)
                {
                    myTable.Rows.Add(item.SrDealerDetailsId, sRInfoMaster.SrId,item.DealerId,item.IsActive,item.ActiveDate,item.DeactiveDate);
                   
                }

                SqlParameterList.Add(new SqlParameter("@srId", sRInfoMaster.SrId));
                SqlParameterList.Add(new SqlParameter("@name", sRInfoMaster.SrName));
                SqlParameterList.Add(new SqlParameter("@address", sRInfoMaster.SrAddress));
                SqlParameterList.Add(new SqlParameter("@mobileNo", sRInfoMaster.SrMobileNumber));
                SqlParameterList.Add(new SqlParameter("@email", sRInfoMaster.SrEmail));
                SqlParameterList.Add(new SqlParameter("@userId", sRInfoMaster.SrId));
                SqlParameterList.Add(new SqlParameter("@srDealerDetails", myTable));

                result.isSuccess = accessManager.SaveData("sp_Save_SR_Info_and_SrDealer_Details", SqlParameterList);
                return result;

            }
            catch (Exception exception)
            {
                accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }


        }

        public List<SRInfoMaster> GetSRList()
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SRInfoMaster> sRInfoMasterList = new List<SRInfoMaster>();
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_sales_representative_list");

                while (dr.Read())
                {
                    SRInfoMaster sRInfoMaster = new SRInfoMaster();

                    sRInfoMaster.SrId = DBNull.Value.Equals(dr["SrId"]) ? 0 : Convert.ToInt32(dr["SrId"]);
                    sRInfoMaster.SrMobileNumber = DBNull.Value == dr["SrMobileNumber"] ? "" : Convert.ToString(dr["SrMobileNumber"]);
                    sRInfoMaster.SrEmail = DBNull.Value == dr["SrEmail"] ? null : Convert.ToString(dr["SrEmail"]);
                    sRInfoMaster.SrAddress = DBNull.Value == dr["SrAddress"] ? null : Convert.ToString(dr["SrAddress"]);
                    sRInfoMaster.SrName = DBNull.Value == dr["SrName"] ? null : Convert.ToString(dr["SrName"]);

                    sRInfoMasterList.Add(sRInfoMaster);
                }


                return sRInfoMasterList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetAllOrdersOfDealers(DateTime? fromDate, DateTime? toDate)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();

                parameters.Add(new SqlParameter("@fromDate", fromDate));
                parameters.Add(new SqlParameter("@toDate", toDate));

                DataTable dr = accessManager.GetDataTable("sp_GetAllOrdersOfDealers", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public dynamic SaveSrSalaryProcessData(ViewSRSalaryProcess viewSRSalaryProcess)
        {

            try
            {
                DataTable dt= new DataTable();
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> SqlParameterList = new List<SqlParameter>();
                DataTable myTable = CreateTable();
                ResultResponse result = new ResultResponse();

                foreach (SRDealerDetails item in viewSRSalaryProcess.SRDealerDetailsList)
                {
                    myTable.Rows.Add(0, item.SrId, item.DealerId, 0, viewSRSalaryProcess.FromDate, viewSRSalaryProcess.ToDate);

                }
                var fromdate = viewSRSalaryProcess.FromDate.ToString("yyyy-MM-dd HH:mm");
                var todate = viewSRSalaryProcess.ToDate.ToString("yyyy-MM-dd HH:mm");
                SqlParameterList.Add(new SqlParameter("@srDealerDetails", myTable));
                SqlParameterList.Add(new SqlParameter("@fromDate", fromdate));
                SqlParameterList.Add(new SqlParameter("@toDate", todate));
                SqlParameterList.Add(new SqlParameter("@empId", viewSRSalaryProcess.EmpId));

                dt = accessManager.GetDataTable("sp_Save_SR_Salary_Process_Data", SqlParameterList);
                return dt;

            }
            catch (Exception exception)
            {
                accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }


        }

        public List<SRInfoMaster> GetSRByCustomerId(int customerid)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SRInfoMaster> _List = new List<SRInfoMaster>();
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@customerid", customerid));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetSRList_ByCustomerId", aSqlParameterList);
                while (dr.Read())
                {
                    SRInfoMaster aInfo = new SRInfoMaster();
                    aInfo.SrId = Convert.ToInt32(dr["SrId"]);
                    aInfo.SrName = dr["SrName"].ToString();
                    aInfo.SrMobileNumber = dr["SrMobileNumber"].ToString();

                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<SRInfoMaster> GetSRList(int customerid, DateTime fromDate, DateTime toDate)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SRInfoMaster> _List = new List<SRInfoMaster>();
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@customerid", customerid));
                aSqlParameterList.Add(new SqlParameter("@FromDate", fromDate));
                aSqlParameterList.Add(new SqlParameter("@ToDate", toDate));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetSRList_SRPaySlip", aSqlParameterList);
                while (dr.Read())
                {
                    SRInfoMaster aInfo = new SRInfoMaster();
                    aInfo.SrId = Convert.ToInt32(dr["SrId"]);
                    aInfo.SrName = dr["SrName"].ToString();
                    

                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<ViewCustomerInfo> GetCustomerList(int typeid, DateTime fromDate, DateTime toDate)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ViewCustomerInfo> _List = new List<ViewCustomerInfo>();
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@customertypeid", typeid));
                aSqlParameterList.Add(new SqlParameter("@FromDate", fromDate));
                aSqlParameterList.Add(new SqlParameter("@ToDate", toDate));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetCustomerList_SRPaySlip", aSqlParameterList);
                while (dr.Read())
                {
                    ViewCustomerInfo aInfo = new ViewCustomerInfo();
                    aInfo.CustomerID = Convert.ToInt32(dr["CustomerID"]);
                    aInfo.CustomerName = dr["CustomerName"].ToString();
                    aInfo.CustomerCode = dr["CustomerCode"].ToString();
                    
                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }


        public DataTable GetSRPaySlipData(int customerid, DateTime fromDate, DateTime toDate, int srid)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();

                parameters.Add(new SqlParameter("@customerid", customerid));
                parameters.Add(new SqlParameter("@FromDate", fromDate));
                parameters.Add(new SqlParameter("@ToDate", toDate));
                parameters.Add(new SqlParameter("@SrId", srid));

                DataTable dr = accessManager.GetDataTable("sp_GetSRPaySlip", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetAllOrdersOfDealersForRetention(DateTime? fromDate, DateTime? toDate)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();

                parameters.Add(new SqlParameter("@fromDate", fromDate));
                parameters.Add(new SqlParameter("@toDate", toDate));

                DataTable dr = accessManager.GetDataTable("sp_GetAllOrdersOfDealersForRetention", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public dynamic SaveDealerRetentionProcessData(DealerRetentionModel dealerRetentionModel)
        {

            try
            {
                DataTable dt = new DataTable();
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> SqlParameterList = new List<SqlParameter>();

                ResultResponse result = new ResultResponse();


                var fromdate = dealerRetentionModel.FromDate.ToString("yyyy-MM-dd HH:mm");
                var todate = dealerRetentionModel.ToDate.ToString("yyyy-MM-dd HH:mm");

                SqlParameterList.Add(new SqlParameter("@fromDate", fromdate));
                SqlParameterList.Add(new SqlParameter("@toDate", todate));
                SqlParameterList.Add(new SqlParameter("@empId", dealerRetentionModel.EmpId));

                dt = accessManager.GetDataTable("sp_Save_Retention_Process_Data", SqlParameterList);
                return dt;

            }
            catch (Exception exception)
            {
                accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }


        }

        public DataTable GetCustomerRetentionSummary(DateTime fromDate, DateTime toDate, int customer)
        {

            try
            {


                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@FromDate", fromDate));
                aParameters.Add(new SqlParameter("@ToDate", toDate));             
                aParameters.Add(new SqlParameter("@CustomerId", customer));
                
                DataTable dt = new DataTable();

                dt = accessManager.GetDataTable("sp_GetDealerRetentionReport_New", aParameters);





                return dt;

            }
            catch (Exception exception)
            {

                throw exception;
            }

            finally
            {

                accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetAllOrdersOfDealersForDamage(DateTime? fromDate, DateTime? toDate)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();

                parameters.Add(new SqlParameter("@fromDate", fromDate));
                parameters.Add(new SqlParameter("@toDate", toDate));

                DataTable dr = accessManager.GetDataTable("sp_GetAllOrdersOfDealersForDamage", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public dynamic SaveDealerDamageProcessData(DealerRetentionModel dealerRetentionModel)
        {

            try
            {
                DataTable dt = new DataTable();
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> SqlParameterList = new List<SqlParameter>();

                ResultResponse result = new ResultResponse();


                var fromdate = dealerRetentionModel.FromDate.ToString("yyyy-MM-dd HH:mm");
                var todate = dealerRetentionModel.ToDate.ToString("yyyy-MM-dd HH:mm");

                SqlParameterList.Add(new SqlParameter("@fromDate", fromdate));
                SqlParameterList.Add(new SqlParameter("@toDate", todate));
                SqlParameterList.Add(new SqlParameter("@empId", dealerRetentionModel.EmpId));

                dt = accessManager.GetDataTable("sp_Save_Damage_Process_Data", SqlParameterList);
                return dt;

            }
            catch (Exception exception)
            {
                accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }


        }

        public DataTable GetCustomerDamageSummary(DateTime fromDate, DateTime toDate, int customer)
        {

            try
            {


                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@FromDate", fromDate));
                aParameters.Add(new SqlParameter("@ToDate", toDate));
                aParameters.Add(new SqlParameter("@CustomerId", customer));

                DataTable dt = new DataTable();

                dt = accessManager.GetDataTable("sp_GetDealerDamageReport_New", aParameters);





                return dt;

            }
            catch (Exception exception)
            {

                throw exception;
            }

            finally
            {

                accessManager.SqlConnectionClose();
            }
        }


        public List<SrSalaryApproval> DataforListApproveSrSalary(int monthId, int yearId )
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@MonthId", monthId));
                aParameters.Add(new SqlParameter("@YearId", yearId));
         
                SqlDataReader dr = accessManager.GetSqlDataReader("GetSrSalaryApprovalList", aParameters);
                List<SrSalaryApproval> ItemDescriptionList = new List<SrSalaryApproval>();
                while (dr.Read())
                {
                    SrSalaryApproval itemDescription = new SrSalaryApproval();
                    itemDescription.SrId = Convert.ToInt32(dr["SrId"]);
                    itemDescription.DealerId = Convert.ToInt32(dr["DealerId"]);
           
                    itemDescription.SalaryAmount =
                        DBNull.Value.Equals(dr["SalaryAmount"]) ? 0 : Convert.ToDecimal(dr["SalaryAmount"]);

                   
                    itemDescription.DealerInfo = dr["DealerInfo"].ToString();
                    itemDescription.SRInfo = dr["SRInfo"].ToString();
                  
                    ItemDescriptionList.Add(itemDescription);
                }
                return ItemDescriptionList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse ApproveSrSalary(SrSalaryApproval entity,string entryBy)
        {


            try
            {
                ResultResponse aResponse = new ResultResponse();
               accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@Year", entity.YearId));
                aSqlParameterList.Add(new SqlParameter("@Month", entity.MonthId));
                aSqlParameterList.Add(new SqlParameter("@DealerId", entity.DealerId));
                aSqlParameterList.Add(new SqlParameter("@SrId", entity.SrId));
                aSqlParameterList.Add(new SqlParameter("@ApprovedBy", entryBy));
                aResponse.isSuccess = accessManager.SaveData("ApproveProcessedSrSalary", aSqlParameterList);
                if (aResponse.isSuccess == true)
                {

                    aResponse.isSuccess = CreateSystemJournal(entity);
                    //accessManager.SqlConnectionClose();
                  
                }
                else
                {
                   accessManager.SqlConnectionClose(true);
                }
                return aResponse;
            }
            catch (Exception ex)
            {
                accessManager.SqlConnectionClose(true);
                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

            
        }

        private bool CreateSystemJournal(SrSalaryApproval entity)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@DealerId", entity.DealerId));
                aParameters.Add(new SqlParameter("@Year", entity.YearId));
                aParameters.Add(new SqlParameter("@Month", entity.MonthId));
                result = accessManager.SaveData("SaveSrSalaryJournal", aParameters);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<RetentionApprovalViewModel> DataforListApproveRetention(int monthId, int yearId)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@MonthId", monthId));
                aParameters.Add(new SqlParameter("@YearId", yearId));

                SqlDataReader dr = accessManager.GetSqlDataReader("GetRetentionApprovalList", aParameters);
                List<RetentionApprovalViewModel> ItemDescriptionList = new List<RetentionApprovalViewModel>();
                while (dr.Read())
                {
                    RetentionApprovalViewModel itemDescription = new RetentionApprovalViewModel();
                 
                    itemDescription.DealerId = Convert.ToInt32(dr["DealerId"]);
                    itemDescription.RetentionAmount =
                        DBNull.Value.Equals(dr["RetentionAmount"]) ? 0 : Convert.ToDecimal(dr["RetentionAmount"]);

                  
                    itemDescription.DealerInfo = dr["DealerInfo"].ToString();
                

                    ItemDescriptionList.Add(itemDescription);
                }
                return ItemDescriptionList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse ApproveRetention(RetentionApprovalViewModel item, string entryBy)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@Year", item.YearId));
                aSqlParameterList.Add(new SqlParameter("@Month", item.MonthId));
                aSqlParameterList.Add(new SqlParameter("@DealerId", item.DealerId));
                aSqlParameterList.Add(new SqlParameter("@ApprovedBy", entryBy));
                aResponse.isSuccess = accessManager.SaveData("ApproveProcessedRetention", aSqlParameterList);
                if (aResponse.isSuccess == true)
                {

                    aResponse.isSuccess = CreateRetentionSystemJournal(item);
                    //accessManager.SqlConnectionClose();

                }
                else
                {
                    accessManager.SqlConnectionClose(true);
                }
                return aResponse;
            }
            catch (Exception ex)
            {
                accessManager.SqlConnectionClose(true);
                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }


        }

        private bool CreateRetentionSystemJournal(RetentionApprovalViewModel entity)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@DealerId", entity.DealerId));
                aParameters.Add(new SqlParameter("@Year", entity.YearId));
                aParameters.Add(new SqlParameter("@Month", entity.MonthId));
                result = accessManager.SaveData("SaveRetentionApprovalJournal", aParameters);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DamageApproveViewModel> DataforListApproveDamage(int monthId, int yearId)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@MonthId", monthId));
                aParameters.Add(new SqlParameter("@YearId", yearId));

                SqlDataReader dr = accessManager.GetSqlDataReader("GetDamageApprovalList", aParameters);
                List<DamageApproveViewModel> ItemDescriptionList = new List<DamageApproveViewModel>();
                while (dr.Read())
                {
                    DamageApproveViewModel itemDescription = new DamageApproveViewModel();

                    itemDescription.DealerId = Convert.ToInt32(dr["DealerId"]);
                    itemDescription.DamageAmount = DBNull.Value.Equals(dr["DamageAmount"]) ? 0 : Convert.ToDecimal(dr["DamageAmount"]);
                
                    itemDescription.DealerInfo = dr["DealerInfo"].ToString();


                    ItemDescriptionList.Add(itemDescription);
                }
                return ItemDescriptionList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse ApproveDamage(DamageApproveViewModel item, string entryBy)
        {

            try
            {
                ResultResponse aResponse = new ResultResponse();
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@Year", item.YearId));
                aSqlParameterList.Add(new SqlParameter("@Month", item.MonthId));
                aSqlParameterList.Add(new SqlParameter("@DealerId", item.DealerId));
                aSqlParameterList.Add(new SqlParameter("@ApprovedBy", entryBy));
                aResponse.isSuccess = accessManager.SaveData("ApproveProcessedDamage", aSqlParameterList);
                if (aResponse.isSuccess == true)
                {

                    aResponse.isSuccess = CreateDamageSystemJournal(item);
                    //accessManager.SqlConnectionClose();

                }
                else
                {
                    accessManager.SqlConnectionClose(true);
                }
                return aResponse;
            }
            catch (Exception ex)
            {
                accessManager.SqlConnectionClose(true);
                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        private bool CreateDamageSystemJournal(DamageApproveViewModel entity)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@DealerId", entity.DealerId));
                aParameters.Add(new SqlParameter("@Year", entity.YearId));
                aParameters.Add(new SqlParameter("@Month", entity.MonthId));
                result = accessManager.SaveData("SaveDamageApprovalJournal", aParameters);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        //Discount
        public DataTable GetAllOrdersOfDealersForDiscount(DateTime? fromDate, DateTime? toDate)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();

                parameters.Add(new SqlParameter("@fromDate", fromDate));
                parameters.Add(new SqlParameter("@toDate", toDate));

                DataTable dr = accessManager.GetDataTable("sp_GetAllOrdersOfDealersForDiscount", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public dynamic SaveDealerDiscountProcessData(DealerRetentionModel dealerRetentionModel)
        {

            try
            {
                DataTable dt = new DataTable();
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> SqlParameterList = new List<SqlParameter>();

                ResultResponse result = new ResultResponse();


                var fromdate = dealerRetentionModel.FromDate.ToString("yyyy-MM-dd HH:mm");
                var todate = dealerRetentionModel.ToDate.ToString("yyyy-MM-dd HH:mm");

                SqlParameterList.Add(new SqlParameter("@fromDate", fromdate));
                SqlParameterList.Add(new SqlParameter("@toDate", todate));
                SqlParameterList.Add(new SqlParameter("@empId", dealerRetentionModel.EmpId));

                dt = accessManager.GetDataTable("sp_Save_Discount_Process_Data", SqlParameterList);
                return dt;

            }
            catch (Exception exception)
            {
                accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }


        }

        public DataTable GetCustomerDiscountSummary(DateTime fromDate, DateTime toDate, int customer)
        {

            try
            {


                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@FromDate", fromDate));
                aParameters.Add(new SqlParameter("@ToDate", toDate));
                aParameters.Add(new SqlParameter("@CustomerId", customer));

                DataTable dt = new DataTable();

                dt = accessManager.GetDataTable("sp_GetDealerDiscountReport_New", aParameters);





                return dt;

            }
            catch (Exception exception)
            {

                throw exception;
            }

            finally
            {

                accessManager.SqlConnectionClose();
            }
        }
        public List<DiscountApproveViewModel> DataforListApproveDiscount(int monthId, int yearId)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@MonthId", monthId));
                aParameters.Add(new SqlParameter("@YearId", yearId));

                SqlDataReader dr = accessManager.GetSqlDataReader("GetDiscountApprovalList", aParameters);
                List<DiscountApproveViewModel> ItemDescriptionList = new List<DiscountApproveViewModel>();
                while (dr.Read())
                {
                    DiscountApproveViewModel itemDescription = new DiscountApproveViewModel();

                    itemDescription.DealerId = Convert.ToInt32(dr["DealerId"]);
                    itemDescription.DeliveryDiscount = DBNull.Value.Equals(dr["DeliveryDiscount"]) ? 0 : Convert.ToDecimal(dr["DeliveryDiscount"]);

                    itemDescription.DealerInfo = dr["DealerInfo"].ToString();


                    ItemDescriptionList.Add(itemDescription);
                }
                return ItemDescriptionList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse ApproveDiscount(DiscountApproveViewModel item, string entryBy)
        {

            try
            {
                ResultResponse aResponse = new ResultResponse();
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@Year", item.YearId));
                aSqlParameterList.Add(new SqlParameter("@Month", item.MonthId));
                aSqlParameterList.Add(new SqlParameter("@DealerId", item.DealerId));
                aSqlParameterList.Add(new SqlParameter("@ApprovedBy", entryBy));
                aResponse.isSuccess = accessManager.SaveData("ApproveProcessedDiscount", aSqlParameterList);
                if (aResponse.isSuccess == true)
                {

                    aResponse.isSuccess = CreateDiscountSystemJournal(item);
                    //accessManager.SqlConnectionClose();

                }
                else
                {
                    accessManager.SqlConnectionClose(true);
                }
                return aResponse;
            }
            catch (Exception ex)
            {
                accessManager.SqlConnectionClose(true);
                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        private bool CreateDiscountSystemJournal(DiscountApproveViewModel entity)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@DealerId", entity.DealerId));
                aParameters.Add(new SqlParameter("@Year", entity.YearId));
                aParameters.Add(new SqlParameter("@Month", entity.MonthId));
                result = accessManager.SaveData("SaveDiscountApprovalJournal", aParameters);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}