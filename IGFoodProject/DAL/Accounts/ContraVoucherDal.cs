﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using IGFoodProject.DataManager;
using IGFoodProject.Models;
using IGFoodProject.Models.Accounts;

namespace IGFoodProject.DAL.Accounts
{
    public class ContraVoucherDal
    {
        DataAccessManager _accessManager = new DataAccessManager();

        public ResultResponse SaveContra(VoucherMaster aMaster, string user)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);

                if (aMaster.eflag == "ed")
                {
                    aMaster.IsApprove = 100;
                }
                else
                {
                    aMaster.IsApprove = 1;
                }

                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@VoucherMasterId", aMaster.VoucherMasterId));
                aParameters.Add(new SqlParameter("@VoucherType", aMaster.VoucherType));
                aParameters.Add(new SqlParameter("@VoucherDate", aMaster.VoucherDate));
                aParameters.Add(new SqlParameter("@CompanyId", aMaster.CompanyId));
                aParameters.Add(new SqlParameter("@FinancialYearId", aMaster.FinancialYearId));
                aParameters.Add(new SqlParameter("@CurrencyId", aMaster.CurrencyId));
                aParameters.Add(new SqlParameter("@CurrencyConRate", aMaster.CurrencyConRate));
                aParameters.Add(new SqlParameter("@Narration", aMaster.Narration));
                aParameters.Add(new SqlParameter("@TotalAmount", aMaster.TotalAmount));
                aParameters.Add(new SqlParameter("@TotalAmountBdt", aMaster.TotalAmountBDT));
                aParameters.Add(new SqlParameter("@BUId", aMaster.BUId));
                aParameters.Add(new SqlParameter("@EntryBy", user));
                aParameters.Add(new SqlParameter("@UpdateBy", user));

                if (aMaster.eflag == "ap")
                {
                    aParameters.Add(new SqlParameter("@IsApprove", true));
                }
                else
                {
                    aParameters.Add(new SqlParameter("@IsApprove", aMaster.IsApprove));
                }
                aParameters.Add(new SqlParameter("@eflag", aMaster.eflag));



                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("SaveContra", aParameters);
                if (aResponse.pk > 0)
                {
                    //if (aMaster.VoucherDetails )
                    //{
                    aResponse.isSuccess = SaveContraDetails(aMaster.VoucherDetails, aResponse.pk);
                    // }

                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                }
                else
                {
                    _accessManager.SqlConnectionClose(true);
                }
                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public bool SaveContraDetails(List<VoucherDetails> aMaster, int jvMasterId)
        {
            try
            {
                bool result = false;
                foreach (var item in aMaster)
                {
                    List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                    ResultResponse aResponse = new ResultResponse();

                    aSqlParameterList.Add(new SqlParameter("@VoucherMasterId", jvMasterId));
                    aSqlParameterList.Add(new SqlParameter("@VoucherDetailId", item.VoucherDetailId));
                    aSqlParameterList.Add(new SqlParameter("@CALayerSevenId", item.CALayerSevenId));
                    aSqlParameterList.Add(new SqlParameter("@DrCr", item.DrCr));
                    aSqlParameterList.Add(new SqlParameter("@DrAmt", item.DrAmt));
                    aSqlParameterList.Add(new SqlParameter("@CrAmt", item.CrAmt));
                    aSqlParameterList.Add(new SqlParameter("@DrAmtBDT", item.DrAmtBDT));
                    aSqlParameterList.Add(new SqlParameter("@CrAmtBDT", item.CrAmtBDT));
                    aSqlParameterList.Add(new SqlParameter("@Remarks", item.Remarks));
                    aSqlParameterList.Add(new SqlParameter("@IsDelete", item.IsDelete));
                    //aSqlParameterList.Add(new SqlParameter("@flag", item.eflag));

                    aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("SaveContraDetails", aSqlParameterList);
                    // result = _accessManager.SaveData("SaveJournalDetails", aSqlParameterList);




                    if (item.VoucherTransections != null &&  item.VoucherTransections.RefDetailsId > 0)
                    {
                        List<SqlParameter> aSqlParameterList1 = new List<SqlParameter>();

                        aSqlParameterList1.Add(new SqlParameter("@VoucherDetailId", aResponse.pk));
                        aSqlParameterList1.Add(new SqlParameter("@amount", item.VoucherTransections.Amt));
                        aSqlParameterList1.Add(new SqlParameter("@amountBdt", item.VoucherTransections.AmtBDT));
                        aSqlParameterList1.Add(new SqlParameter("@SalesOrderChallanMasterId", item.VoucherTransections.SalesOrderChallanMasterId));
                        aSqlParameterList1.Add(new SqlParameter("@MRRMasterId", item.VoucherTransections.MRRMasterId));
                        aSqlParameterList1.Add(new SqlParameter("@CostCenterId", item.VoucherTransections.CostCenterId));
                        aSqlParameterList1.Add(new SqlParameter("@RefDetailsId", item.VoucherTransections.RefDetailsId));
                        result = _accessManager.SaveData("SaveContraTransaction", aSqlParameterList1);



                        // aResponse.isSuccess = SaveJournalTransactionDetails(item.VoucherTransections, aResponse.pk);
                    }
                    //else
                    //{
                    //    break;
                    //}

                }
              
                return result=true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



        public List<VoucherMasterView> ContraList(DateTime? fromdate, DateTime? todate, int CompanyId = 0, int BUId = 0)
        {
            try
            {

                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-1);

                }
                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);

                List<VoucherMasterView> aList = new List<VoucherMasterView>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CompanyId", CompanyId));
                aParameters.Add(new SqlParameter("@BUId", BUId));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetAllContra", aParameters);
                while (dr.Read())
                {
                    VoucherMasterView aMaster = new VoucherMasterView();
                    aMaster.VoucherMasterId = Convert.ToInt32(dr["VoucherMasterId"]);
                    aMaster.VoucherType = dr["VoucherType"].ToString();
                    aMaster.VoucherCode = dr["VoucherCode"].ToString();
                    aMaster.VoucherDate = Convert.ToDateTime(dr["VoucherDate"]);
                    aMaster.CompanyId = Convert.ToInt32(dr["CompanyId"]);
                    aMaster.Company = dr["CompanyShortName"].ToString();
                    aMaster.FinancialYearId = Convert.ToInt32(dr["FinancialYearId"]);
                    aMaster.FinancialYear = dr["FinancialYear"].ToString();
                    aMaster.CurrencyId = Convert.ToInt32(dr["CurrencyId"]);
                    aMaster.Currency = dr["CurrencyCode"].ToString();
                    aMaster.Narration = dr["Narration"].ToString();
                    aMaster.ReviewReasion = dr["ReviewReasion"].ToString();
                    aMaster.TotalAmount = Convert.ToDecimal(dr["TotalAmount"]);
                    aMaster.TotalAmountBDT = Convert.ToDecimal(dr["TotalAmountBDT"]);
                    aMaster.PaymentType = dr["PaymentType"].ToString();
                    aMaster.ChequeNo = dr["ChequeNo"].ToString();
                    aMaster.ChequeDate = dr["ChequeDate"] as DateTime?;
                    aMaster.RefNo = dr["RefNo"].ToString();
                    aMaster.RefDate = dr["RefDate"] as DateTime?;
                    aMaster.EntryBy = dr["EntryBy"].ToString();
                    aMaster.EntryDate = Convert.ToDateTime(dr["EntryDate"]);
                    aMaster.BUId = Convert.ToInt32(dr["BUId"]);
                    aMaster.BusinessUnit = dr["ShortName"].ToString();
                    aMaster.IsApprove = Convert.ToInt32(dr["IsApprove"]);
                    aMaster.IsReject = Convert.ToBoolean(dr["IsReject"]);
                    aMaster.BankId = DBNull.Value.Equals(dr["BankId"]) ? 0 : Convert.ToInt32(dr["BankId"]);
                    aMaster.Bank = dr["BankName"].ToString();
                    aMaster.BranchId = DBNull.Value.Equals(dr["BranchId"]) ? 0 : Convert.ToInt32(dr["BranchId"]);
                    aMaster.Branch = dr["BranchName"].ToString();

                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

        //------------Contra Approval-----------
        public List<VoucherMasterView> ContraListForApproval(DateTime? fromdate, DateTime? todate, int CompanyId = 0, int BUId = 0,int statusType=100)
        {
            try
            {

                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-1);

                }
                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);

                List<VoucherMasterView> aList = new List<VoucherMasterView>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CompanyId", CompanyId));
                aParameters.Add(new SqlParameter("@BUId", BUId));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                aParameters.Add(new SqlParameter("@statusType", statusType));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetAllContraForApprovalList", aParameters);
                while (dr.Read())
                {
                    VoucherMasterView aMaster = new VoucherMasterView();
                    aMaster.VoucherMasterId = Convert.ToInt32(dr["VoucherMasterId"]);
                    aMaster.VoucherType = dr["VoucherType"].ToString();
                    aMaster.VoucherCode = dr["VoucherCode"].ToString();
                    aMaster.VoucherDate = Convert.ToDateTime(dr["VoucherDate"]);
                    aMaster.CompanyId = Convert.ToInt32(dr["CompanyId"]);
                    aMaster.Company = dr["CompanyShortName"].ToString();
                    aMaster.FinancialYearId = Convert.ToInt32(dr["FinancialYearId"]);
                    aMaster.FinancialYear = dr["FinancialYear"].ToString();
                    aMaster.CurrencyId = Convert.ToInt32(dr["CurrencyId"]);
                    aMaster.Currency = dr["CurrencyCode"].ToString();
                    aMaster.Narration = dr["Narration"].ToString();
                    aMaster.ReviewReasion = dr["ReviewReasion"].ToString();
                    aMaster.TotalAmount = Convert.ToDecimal(dr["TotalAmount"]);
                    aMaster.TotalAmountBDT = Convert.ToDecimal(dr["TotalAmountBDT"]);
                    aMaster.PaymentType = dr["PaymentType"].ToString();
                    aMaster.ChequeNo = dr["ChequeNo"].ToString();
                    aMaster.ChequeDate = dr["ChequeDate"] as DateTime?;
                    aMaster.RefNo = dr["RefNo"].ToString();
                    aMaster.RefDate = dr["RefDate"] as DateTime?;
                    aMaster.EntryBy = dr["EntryBy"].ToString();
                    aMaster.EntryDate = Convert.ToDateTime(dr["EntryDate"]);
                    aMaster.BUId = Convert.ToInt32(dr["BUId"]);
                    aMaster.BusinessUnit = dr["ShortName"].ToString();
                    aMaster.IsApprove = Convert.ToInt32(dr["IsApprove"]);
                    aMaster.IsReject = Convert.ToBoolean(dr["IsReject"]);
                    aMaster.BankId = DBNull.Value.Equals(dr["BankId"]) ? 0 : Convert.ToInt32(dr["BankId"]);
                    aMaster.Bank = dr["BankName"].ToString();
                    aMaster.BranchId = DBNull.Value.Equals(dr["BranchId"]) ? 0 : Convert.ToInt32(dr["BranchId"]);
                    aMaster.Branch = dr["BranchName"].ToString();
                   
                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
    }
}