﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using IGFoodProject.DataManager;
using IGFoodProject.Models;
using IGFoodProject.Models.Accounts;
using IGFoodProject.ViewModel;

namespace IGFoodProject.DAL.Accounts
{
    public class JournalVoucherDal
    {
        DataAccessManager _accessManager = new DataAccessManager();


        public ResultResponse SaveJournal(VoucherMaster aMaster, string user)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);

                if (aMaster.eflag == "ed")
                {
                    aMaster.IsApprove = 100;
                }
                else
                {
                    aMaster.IsApprove = 1;
                }


                List<SqlParameter> aParameters = new List<SqlParameter>();
          
                aParameters.Add(new SqlParameter("@VoucherMasterId", aMaster.VoucherMasterId));
                aParameters.Add(new SqlParameter("@VoucherType", aMaster.VoucherType));
                aParameters.Add(new SqlParameter("@VoucherDate", aMaster.VoucherDate));
                aParameters.Add(new SqlParameter("@CompanyId", aMaster.CompanyId));
                aParameters.Add(new SqlParameter("@FinancialYearId", aMaster.FinancialYearId));
                aParameters.Add(new SqlParameter("@CurrencyId", aMaster.CurrencyId));
                aParameters.Add(new SqlParameter("@CurrencyConRate", aMaster.CurrencyConRate));
                aParameters.Add(new SqlParameter("@Narration", aMaster.Narration));
                aParameters.Add(new SqlParameter("@TotalAmount", aMaster.TotalAmount));
                aParameters.Add(new SqlParameter("@TotalAmountBdt", aMaster.TotalAmountBDT));
                aParameters.Add(new SqlParameter("@BUId", aMaster.BUId));
                aParameters.Add(new SqlParameter("@EntryBy", user));
                aParameters.Add(new SqlParameter("@UpdateBy", user));

                if (aMaster.eflag == "ap")
                {
                    aParameters.Add(new SqlParameter("@IsApprove", true));
                }
                else
                {
                    aParameters.Add(new SqlParameter("@IsApprove", aMaster.IsApprove));
                }
                aParameters.Add(new SqlParameter("@eflag", aMaster.eflag));

                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("SaveJournal", aParameters);
                if (aResponse.pk > 0)
                {
                    //if (aMaster.VoucherDetails )
                    //{
                        aResponse.isSuccess = SaveJournalDetails(aMaster.VoucherDetails, aResponse.pk);
                   // }
                   
                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                }
                else
                {
                    _accessManager.SqlConnectionClose(true);
                }
                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public bool SaveJournalDetails(List<VoucherDetails> aMaster, int jvMasterId)
        {
            try
            {
                bool result = false;
                foreach (var item in aMaster)
                {
                    List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                    ResultResponse aResponse = new ResultResponse();

                    aSqlParameterList.Add(new SqlParameter("@VoucherMasterId", jvMasterId));
                    aSqlParameterList.Add(new SqlParameter("@VoucherDetailId", item.VoucherDetailId));
                    aSqlParameterList.Add(new SqlParameter("@CALayerSevenId", item.CALayerSevenId));
                    aSqlParameterList.Add(new SqlParameter("@DrCr", item.DrCr));
                    aSqlParameterList.Add(new SqlParameter("@DrAmt", item.DrAmt));
                   // aSqlParameterList.Add(new SqlParameter("@CrAmt", item.CrAmt));
                    aSqlParameterList.Add(new SqlParameter("@DrAmtBDT", item.DrAmtBDT));
                  //  aSqlParameterList.Add(new SqlParameter("@CrAmtBDT", item.CrAmtBDT));
                    aSqlParameterList.Add(new SqlParameter("@Remarks", item.Remarks));
                    aSqlParameterList.Add(new SqlParameter("@IsDelete", item.IsDelete));
                     //aSqlParameterList.Add(new SqlParameter("@flag", item.eflag));

                    aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("SaveJournalDetails", aSqlParameterList);
                    // result = _accessManager.SaveData("SaveJournalDetails", aSqlParameterList);


           

                    if ( item.VoucherTransections != null &&  item.VoucherTransections.RefDetailsId>0)
                    {
                        List<SqlParameter> aSqlParameterList1 = new List<SqlParameter>();

                        aSqlParameterList1.Add(new SqlParameter("@VoucherDetailId", aResponse.pk));
                        aSqlParameterList1.Add(new SqlParameter("@amount", item.VoucherTransections.Amt));
                        aSqlParameterList1.Add(new SqlParameter("@amountBdt", item.VoucherTransections.AmtBDT));
                        aSqlParameterList1.Add(new SqlParameter("@SalesOrderChallanMasterId", item.VoucherTransections.SalesOrderChallanMasterId));
                        aSqlParameterList1.Add(new SqlParameter("@MRRMasterId", item.VoucherTransections.MRRMasterId));
                        aSqlParameterList1.Add(new SqlParameter("@CostCenterId", item.VoucherTransections.CostCenterId));
                        aSqlParameterList1.Add(new SqlParameter("@RefDetailsId", item.VoucherTransections.RefDetailsId));
                        result = _accessManager.SaveData("SaveJournalTransaction", aSqlParameterList1);
                   


                        // aResponse.isSuccess = SaveJournalTransactionDetails(item.VoucherTransections, aResponse.pk);
                    }
                    //else
                    //{
                    //    break;
                    //}

                }
                return result=true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool SaveJournalTransactionDetails(VoucherTransection aMaster, int voucherDetailsId)
        {
            try
            {
                       bool result = false;
             
                    List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                   aSqlParameterList.Add(new SqlParameter("@VoucherDetailId", voucherDetailsId));
                    aSqlParameterList.Add(new SqlParameter("@amount", aMaster.Amt));
                    aSqlParameterList.Add(new SqlParameter("@amountBdt", aMaster.AmtBDT));
                    aSqlParameterList.Add(new SqlParameter("@SalesOrderChallanMasterId", aMaster.SalesOrderChallanMasterId));
                    aSqlParameterList.Add(new SqlParameter("@MRRMasterId", aMaster.MRRMasterId));
                    aSqlParameterList.Add(new SqlParameter("@CostCenterId", aMaster.CostCenterId));
                    aSqlParameterList.Add(new SqlParameter("@RefDetailsId", aMaster.RefDetailsId));
                     result = _accessManager.SaveData("SaveJournalTransaction", aSqlParameterList);
                 
                  

             
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<VoucherMasterView> JournalList(DateTime? fromdate, DateTime? todate, int CompanyId = 0, int BUId = 0)
        {
            try
            {
                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-1);

                }
                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);

                List<VoucherMasterView> aList = new List<VoucherMasterView>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CompanyId", CompanyId));
                aParameters.Add(new SqlParameter("@BUId", BUId));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetAllJournal", aParameters);
                while (dr.Read())
                {
                    VoucherMasterView aMaster = new VoucherMasterView();
                    aMaster.VoucherMasterId = Convert.ToInt32(dr["VoucherMasterId"]);
                    aMaster.VoucherType = dr["VoucherType"].ToString();
                    aMaster.VoucherCode = dr["VoucherCode"].ToString();
                    aMaster.VoucherDate = Convert.ToDateTime(dr["VoucherDate"]);
                    aMaster.CompanyId = Convert.ToInt32(dr["CompanyId"]);
                    aMaster.Company = dr["CompanyShortName"].ToString();
                    aMaster.FinancialYearId = Convert.ToInt32(dr["FinancialYearId"]);
                    aMaster.FinancialYear = dr["FinancialYear"].ToString();
                    aMaster.CurrencyId = Convert.ToInt32(dr["CurrencyId"]);
                    aMaster.Currency = dr["CurrencyCode"].ToString();
                    aMaster.Narration = dr["Narration"].ToString();
                    aMaster.ReviewReasion = dr["ReviewReasion"].ToString();
                    aMaster.TotalAmount = Convert.ToDecimal(dr["TotalAmount"]);
                    aMaster.TotalAmountBDT = Convert.ToDecimal(dr["TotalAmountBDT"]);
                    aMaster.PaymentType = dr["PaymentType"].ToString();
                    aMaster.ChequeNo = dr["ChequeNo"].ToString();
                    aMaster.ChequeDate = dr["ChequeDate"] as DateTime?;
                    aMaster.RefNo = dr["RefNo"].ToString();
                    aMaster.RefDate = dr["RefDate"] as DateTime?;
                    aMaster.EntryBy = dr["EntryBy"].ToString();
                    aMaster.EntryDate = Convert.ToDateTime(dr["EntryDate"]);
                    aMaster.BUId = Convert.ToInt32(dr["BUId"]);
                    aMaster.BusinessUnit = dr["ShortName"].ToString();
                    aMaster.IsApprove = Convert.ToInt32(dr["IsApprove"]);
                    aMaster.IsReject = Convert.ToBoolean(dr["IsReject"]);
                    aMaster.BankId = DBNull.Value.Equals(dr["BankId"]) ? 0 : Convert.ToInt32(dr["BankId"]);
                    aMaster.Bank = dr["BankName"].ToString();
                    aMaster.BranchId = DBNull.Value.Equals(dr["BranchId"]) ? 0 : Convert.ToInt32(dr["BranchId"]);
                    aMaster.Branch = dr["BranchName"].ToString();

                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetVoucherPrintReport(int voucherMasterId)
        {
            try
            {
                DataTable dr = new DataTable();
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@id", voucherMasterId));
                dr = _accessManager.GetDataTable("sp_Get_PrintVoucherByMasterId", parameters);

                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetDeliveryReport_ReturnChallan(int SalesOrderChallanMasterId, string category)
        {
            try
            {
                DataTable dr = new DataTable();
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@SalesOrderChallanMasterId", SalesOrderChallanMasterId));
                parameters.Add(new SqlParameter("@category", category));

                dr = _accessManager.GetDataTable("sp_GetDeliveryReport_ReturnChallan", parameters);

                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public List<ReferanceTypeDetails> GetReferenceTypeByGlId(int glId)
        {
            _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
            List<SqlParameter> aParameters = new List<SqlParameter>();
            aParameters.Add(new SqlParameter("@glId", glId));
            SqlDataReader dr = _accessManager.GetSqlDataReader("GetReferenceTypeByGlIdInquiry", aParameters);
            List<ReferanceTypeDetails> _cList = new List<ReferanceTypeDetails>();
            while (dr.Read())
            {
                ReferanceTypeDetails aCur = new ReferanceTypeDetails();
                aCur.RefDetailsId = Convert.ToInt32(dr["RefDetailsId"]);
                aCur.RefName = dr["RefName"].ToString();
                _cList.Add(aCur);
            }
            return _cList;
        }


        public List<GLInquiry> LoadDataGLInquiry(DateTime? fromdate, DateTime? todate, int glId = 0, string refdetailid = null)
        {
            try
            {
                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-1);

                }
                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }
                if (refdetailid == "0")
                {
                    refdetailid = null;

                }
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);

                List<GLInquiry> aList = new List<GLInquiry>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@glId", glId));
                aParameters.Add(new SqlParameter("@refdetailid", refdetailid));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetGeneralLedger", aParameters,true);
                while (dr.Read())
                {
                    GLInquiry aMaster = new GLInquiry();
                    aMaster.VoucherMasterId = Convert.ToInt32(dr["VoucherMasterId"]);
                    aMaster.VoucherType = dr["VoucherType"].ToString();
                    aMaster.VoucherRefNo = dr["VoucherRefNo"].ToString();

                    aMaster.Memo = dr["Memo"].ToString();
                    aMaster.Particular = dr["Particular"].ToString();
                    aMaster.PersonOrItem = dr["PersonOrItem"].ToString();
                    aMaster.VoucherTime = dr["VoucherTime"].ToString();

                    aMaster.VoucherDate = DBNull.Value.Equals(dr["VoucherDate"]) ? (DateTime?)null : Convert.ToDateTime(dr["VoucherDate"]);
                    aMaster.DebitAmount = DBNull.Value.Equals(dr["DrAmtBDT"]) ? default(decimal) : Convert.ToDecimal(dr["DrAmtBDT"]);
                    aMaster.CreditAmount = DBNull.Value.Equals(dr["CrAmtBDT"]) ? default(decimal) : Convert.ToDecimal(dr["CrAmtBDT"]);
                    aMaster.Balance = DBNull.Value.Equals(dr["Balance"]) ? default(decimal) : Convert.ToDecimal(dr["Balance"]);
                    aMaster.LedgerGroupName = dr["LedgerGroupName"].ToString();

                    aMaster.OpeningDate = DBNull.Value.Equals(dr["OpeningDate"]) ? (DateTime?)null : Convert.ToDateTime(dr["OpeningDate"]);

                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public GLInquiry GetClosingBalanceOfGL(int glId = 0, string refdetailid = null)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
                GLInquiry aMaster = new GLInquiry();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@glId", glId));
                aParameters.Add(new SqlParameter("@refdetailid", refdetailid));

                SqlDataReader dr = _accessManager.GetSqlDataReader("GetClosingBalanceOfGl", aParameters,true);
                while (dr.Read())
                {
                    aMaster.Balance = DBNull.Value.Equals(dr["Balance"])
                        ? default(decimal)
                        : Convert.ToDecimal(dr["Balance"]);


                }
                return aMaster;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetTrailBalancePrintReport(DateTime fromDate, DateTime toDate, bool all, bool tran)
        {
            try
            {
                DataTable dr = new DataTable();
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@fromDate", fromDate));
                parameters.Add(new SqlParameter("@toDate", toDate));
                parameters.Add(new SqlParameter("@all", all));
                parameters.Add(new SqlParameter("@tran", tran));
                dr = _accessManager.GetDataTable("sp_TrailBalance", parameters,true);

                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public OpeningBalance GetRefDetailsDuDate(int GlId, int RefDetailsId)
        {
            _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
            List<SqlParameter> aParameters = new List<SqlParameter>();
            aParameters.Add(new SqlParameter("@GlId", GlId));
            aParameters.Add(new SqlParameter("@RefDetailsId", RefDetailsId));
            SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetRefDetailsDuDate", aParameters);
            OpeningBalance aCur = new OpeningBalance();
            while (dr.Read())
            {
             
                aCur.DueDate = Convert.ToDateTime(dr["DueDate"]) as DateTime ? ;
                aCur.OBLC = DBNull.Value.Equals(dr["OBLC"])?0: Convert.ToDecimal(dr["OBLC"]);
            }
            return aCur;
        }

        //----Journal Voucher Approval-------
        public List<VoucherMasterView> JournalListForApproval(DateTime? fromdate, DateTime? todate, int CompanyId = 0, int BUId = 0, int statusType = 100)
        {
            try
            {
                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-1);

                }
                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);

                List<VoucherMasterView> aList = new List<VoucherMasterView>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CompanyId", CompanyId));
                aParameters.Add(new SqlParameter("@BUId", BUId));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                aParameters.Add(new SqlParameter("@statusType", statusType));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetAllJournalForApprovalList", aParameters);
                while (dr.Read())
                {
                    VoucherMasterView aMaster = new VoucherMasterView();
                    aMaster.VoucherMasterId = Convert.ToInt32(dr["VoucherMasterId"]);
                    aMaster.VoucherType = dr["VoucherType"].ToString();
                    aMaster.VoucherCode = dr["VoucherCode"].ToString();
                    aMaster.VoucherDate = Convert.ToDateTime(dr["VoucherDate"]);
                    aMaster.CompanyId = Convert.ToInt32(dr["CompanyId"]);
                    aMaster.Company = dr["CompanyShortName"].ToString();
                    aMaster.FinancialYearId = Convert.ToInt32(dr["FinancialYearId"]);
                    aMaster.FinancialYear = dr["FinancialYear"].ToString();
                    aMaster.CurrencyId = Convert.ToInt32(dr["CurrencyId"]);
                    aMaster.Currency = dr["CurrencyCode"].ToString();
                    aMaster.Narration = dr["Narration"].ToString();
                    aMaster.ReviewReasion = dr["ReviewReasion"].ToString();
                    aMaster.TotalAmount = Convert.ToDecimal(dr["TotalAmount"]);
                    aMaster.TotalAmountBDT = Convert.ToDecimal(dr["TotalAmountBDT"]);
                    aMaster.PaymentType = dr["PaymentType"].ToString();
                    aMaster.ChequeNo = dr["ChequeNo"].ToString();
                    aMaster.ChequeDate = dr["ChequeDate"] as DateTime?;
                    aMaster.RefNo = dr["RefNo"].ToString();
                    aMaster.RefDate = dr["RefDate"] as DateTime?;
                    aMaster.EntryBy = dr["EntryBy"].ToString();
                    aMaster.EntryDate = Convert.ToDateTime(dr["EntryDate"]);
                    aMaster.BUId = Convert.ToInt32(dr["BUId"]);
                    aMaster.BusinessUnit = dr["ShortName"].ToString();
                    aMaster.IsApprove = Convert.ToInt32(dr["IsApprove"]);
                    aMaster.IsReject = Convert.ToBoolean(dr["IsReject"]);
                    aMaster.BankId = DBNull.Value.Equals(dr["BankId"]) ? 0 : Convert.ToInt32(dr["BankId"]);
                    aMaster.Bank = dr["BankName"].ToString();
                    aMaster.BranchId = DBNull.Value.Equals(dr["BranchId"]) ? 0 : Convert.ToInt32(dr["BranchId"]);
                    aMaster.Branch = dr["BranchName"].ToString();

                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public VoucherMaster GetFiscalyearByVoucherDate(DateTime voucherDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@voucherDate", voucherDate));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetFiscalyearByVoucherDate", aParameters);
                VoucherMaster vim = new VoucherMaster(); 
                while (dr.Read())
                {
                    vim.FinancialYearId = Convert.ToInt32(dr["FinancialYearId"]); 
                  
                }
                return vim;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
           
        }


    }
}