﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using IGAccounts.Repository;
using System.Data.Entity;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Data.SqlClient;
using System.Web.Mvc.Html;
using IGFoodProject.Controllers.Accounts;
using IGFoodProject.DataManager;
using IGFoodProject.Models;
using IGFoodProject.Models.Accounts;
using IGFoodProject.ViewModel;

namespace IGAccounts.DAL
{
    public class ChartOfAccountDAL
    {
        CodeDbSetAccounts aContext = new CodeDbSetAccounts();
        DataAccessManager _accessManager = new DataAccessManager();

        public dynamic GetSectosAndDetails()
        {
            Repository<LedgerGroup> one = new Repository<LedgerGroup>(aContext);
            Repository<ChartOfAccLayerTwo> two = new Repository<ChartOfAccLayerTwo>(aContext);
            Repository<ChartOfAccLayerThree> three = new Repository<ChartOfAccLayerThree>(aContext);
            Repository<ChartOfAccLayerFour> four = new Repository<ChartOfAccLayerFour>(aContext);
            Repository<ChartOfAccLayerFive> five = new Repository<ChartOfAccLayerFive>(aContext);
            Repository<ChartOfAccLayerSix> six = new Repository<ChartOfAccLayerSix>(aContext);
            Repository<ChartOfAccLayerSeven> seven = new Repository<ChartOfAccLayerSeven>(aContext);
            List<LedgerGroup> aList2 = one.GetAll().ToList().Select(a => new LedgerGroup()
            {
                LedgerGroupId = a.LedgerGroupId,
                LedgerGroupName = a.LedgerGroupName,
                LedgerGroupCode = a.LedgerGroupCode,
                ChartOfAccLayerTwoList = two.GetAll().Where(x => x.LedgerGroupId == a.LedgerGroupId).ToList().Select(b => new ChartOfAccLayerTwo()
                {
                    LedgerGroupId = b.LedgerGroupId,
                    CALayerTwoId = b.CALayerTwoId,
                    CALayerTwoName = b.CALayerTwoName,
                    //Code =a.LedgerGroupCode+"-"+ b.CALayerTwoCode,
                    Code =  b.CALayerTwoCode,
                    CALayerTwoCode =b.CALayerTwoCode,
                    IsActive = b.IsActive,
                    Description = b.Description,
                    ChartOfAccLayerThreeList = three.GetAll().Where(x => x.CALayerTwoId == b.CALayerTwoId)
                       .ToList().Select(c => new ChartOfAccLayerThree()
                       {
                           CALayerTwoId = c.CALayerTwoId,
                           CALayerThreeId = c.CALayerThreeId,
                           CALayerThreeName = c.CALayerThreeName,
                           CALayerThreeCode = c.CALayerThreeCode,
                           Code= c.CALayerThreeCode,
                           IsActive = c.IsActive,
                           Description = c.Description,
                           ChartOfAccLayerFourList = four.GetAll().Where(x => x.CALayerThreeId == c.CALayerThreeId).ToList().Select(d => new ChartOfAccLayerFour()
                           {
                               CALayerThreeId = d.CALayerThreeId,
                               CALayerFourId = d.CALayerFourId,
                               CALayerFourName = d.CALayerFourName,
                               CALayerFourCode = d.CALayerFourCode,
                               Code= d.CALayerFourCode,
                               IsActive = d.IsActive,
                               Description = d.Description,
                               ChartOfAccLayerFiveList = five.GetAll().Where(x => x.CALayerFourId == d.CALayerFourId).ToList().Select(e => new ChartOfAccLayerFive()
                               {
                                   CALayerFourId = e.CALayerFourId,
                                   CALayerFiveId = e.CALayerFiveId,
                                   CALayerFiveName = e.CALayerFiveName,
                                   CALayerFiveCode = e.CALayerFiveCode,
                                   Code = e.CALayerFiveCode,
                                   IsActive = e.IsActive,
                                   Description = e.Description,
                                   ChartOfAccLayerSixList = six.GetAll().Where(x => x.CALayerFiveId == e.CALayerFiveId).ToList().Select(f => new ChartOfAccLayerSix()
                                   {
                                       CALayerFiveId = f.CALayerFiveId,
                                       CALayerSixId = f.CALayerSixId,
                                       CALayerSixName = f.CALayerSixName,
                                       CALayerSixCode = f.CALayerSixCode,
                                       Code = f.CALayerSixCode,
                                       IsActive = f.IsActive,
                                       Description = f.Description,
                                       ChartOfAccLayerSevenList = seven.GetAll().Where(x => x.CALayerSixId == f.CALayerSixId).ToList().Select(g => new ChartOfAccLayerSeven()
                                       {
                                           CALayerSixId = g.CALayerSixId,
                                           CALayerSevenId = g.CALayerSevenId,
                                           CALayerSevenName = g.CALayerSevenName,
                                           CALayerSevenCode = g.CALayerSevenCode,
                                           Code = g.CALayerSevenCode,
                                           IsActive = g.IsActive,
                                           Description = g.Description,
                                       }).ToList()
                                   }).ToList()
                               }).ToList()
                           }).ToList()

                       }).ToList()
                }).ToList()


            }).ToList();
            return aList2;
        }

        public GLSetup GenerateGLCode(int parentId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ParentId", parentId));

                GLSetup gl = new GLSetup();
                SqlDataReader dr = _accessManager.GetSqlDataReader("GenerateGLCode", aParameters);
                while (dr.Read())
                {
                    gl.NewGLCode = dr["NewGLCode"].ToString();
                  
                }
                return gl;
            }
            catch (Exception ex)
            {

                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public List<ChartOfAccLayerSix> GetCoaAllLayerSix( int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
                List<SqlParameter> parameters = new List<SqlParameter>();

                parameters.Add(new SqlParameter("@Id", id));

                SqlDataReader dr = _accessManager.GetSqlDataReader("GetAllCoaLevelSix", parameters);
                List<ChartOfAccLayerSix> coaList = new List<ChartOfAccLayerSix>();
                while (dr.Read())
                {


                    ChartOfAccLayerSix cSix = new ChartOfAccLayerSix();

                    cSix.CALayerSixName = dr["CALayerSixName"].ToString();
                    cSix.CALayerSixId = Convert.ToInt32(dr["CALayerSixId"]);
                    

                    coaList.Add(cSix);
                }
                return coaList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public List<LedgerGroup> GetAllLedgerGroup()
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
              
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetAllLedgerGroup");
                List<LedgerGroup> coaList = new List<LedgerGroup>();
                while (dr.Read())
                {


                    LedgerGroup cSix = new LedgerGroup();

                    cSix.LedgerGroupName = dr["LedgerGroupName"].ToString();
                    cSix.LedgerGroupId = Convert.ToInt32(dr["LedgerGroupId"]);
                    

                    coaList.Add(cSix);
                }
                return coaList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public List<ChartOfAccLayerTwo> GetAllLayerTwoByLayerOne(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
                List<SqlParameter> parameters = new List<SqlParameter>();

                parameters.Add(new SqlParameter("@Id", id));

                SqlDataReader dr = _accessManager.GetSqlDataReader("GetAllLayerTwoByOne", parameters);
                List<ChartOfAccLayerTwo> coaList = new List<ChartOfAccLayerTwo>();
                while (dr.Read())
                {


                    ChartOfAccLayerTwo cSix = new ChartOfAccLayerTwo();

                    cSix.CALayerTwoName = dr["CALayerTwoName"].ToString();
                    cSix.CALayerTwoId = Convert.ToInt32(dr["CALayerTwoId"]);
                    

                    coaList.Add(cSix);
                }
                return coaList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

        public List<ChartOfAccLayerThree> GetAllLayerThreeByLayerTwo(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
                List<SqlParameter> parameters = new List<SqlParameter>();

                parameters.Add(new SqlParameter("@Id", id));

                SqlDataReader dr = _accessManager.GetSqlDataReader("GetAllLayerThreeByTwo", parameters);
                List<ChartOfAccLayerThree> coaList = new List<ChartOfAccLayerThree>();
                while (dr.Read())
                {


                    ChartOfAccLayerThree cSix = new ChartOfAccLayerThree();

                    cSix.CALayerThreeName = dr["CALayerThreeName"].ToString();
                    cSix.CALayerThreeId = Convert.ToInt32(dr["CALayerThreeId"]);


                    coaList.Add(cSix);
                }
                return coaList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public List<ChartOfAccLayerFour> GetAllLayerFourByLayerThree(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@Id", id));

                SqlDataReader dr = _accessManager.GetSqlDataReader("GetAllLayerFourByThree", parameters);
               
                List<ChartOfAccLayerFour> coaList = new List<ChartOfAccLayerFour>();
                while (dr.Read())
                {


                    ChartOfAccLayerFour cSix = new ChartOfAccLayerFour();

                    cSix.CALayerFourName = dr["CALayerFourName"].ToString();
                    cSix.CALayerFourId = Convert.ToInt32(dr["CALayerFourId"]);


                    coaList.Add(cSix);
                }
                return coaList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public List<ChartOfAccLayerFive> GetAllLayerFiveByLayerFour(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@Id",id));
              
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetAllLayerFiveByLayerFour", parameters);
                List<ChartOfAccLayerFive> coaList = new List<ChartOfAccLayerFive>();
                while (dr.Read())
                {


                    ChartOfAccLayerFive cSix = new ChartOfAccLayerFive();

                    cSix.CALayerFiveName = dr["CALayerFiveName"].ToString();
                    cSix.CALayerFiveId = Convert.ToInt32(dr["CALayerFiveId"]);


                    coaList.Add(cSix);
                }
                return coaList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

        public List<CostCenter> GetAllCostCenter(int glid)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@glid", glid));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetAllCostCenters",parameters);
                List<CostCenter> ccList = new List<CostCenter>();
                while (dr.Read())
                {


                    CostCenter cc = new CostCenter();

                    cc.CostCenterName = dr["CostCenterName"].ToString();
                    cc.CostCenterId = Convert.ToInt32(dr["CostCenterId"]);
                    cc.isCostCenter = Convert.ToInt32(dr["isCostCenter"]);
                    

                    ccList.Add(cc);
                }
                return ccList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

        public List<ReferanceType> GetAllReferanceType(int glid)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@glid", glid));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetAllReferanceType",parameters);
                List<ReferanceType> ccList = new List<ReferanceType>();
                while (dr.Read())
                {


                    ReferanceType cc = new ReferanceType();

                    cc.ReferanceTypeName = dr["ReferanceTypeName"].ToString();
                    cc.ReferanceTypeId = Convert.ToInt32(dr["ReferanceTypeId"]);
                    cc.isrefrence = Convert.ToInt32(dr["isrefrence"]);

                    ccList.Add(cc);
                }
                return ccList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }


        }
        public ResultResponse SaveNewGL(LedgerGroup aMaster, string user)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);





                List<SqlParameter> aParameters = new List<SqlParameter>();

                //aParameters.Add(new SqlParameter("@CompanyId", aMaster.CompanyId));
                aParameters.Add(new SqlParameter("@GLID", aMaster.LedgerGroupId));
                aParameters.Add(new SqlParameter("@ParentId", aMaster.ParentId));
                aParameters.Add(new SqlParameter("@GLName", aMaster.LedgerGroupName));
                aParameters.Add(new SqlParameter("@Description", aMaster.Description));
                aParameters.Add(new SqlParameter("@IsActive", aMaster.IsActive));
                aParameters.Add(new SqlParameter("@Code", aMaster.Code));
                aParameters.Add(new SqlParameter("@IsRef", aMaster.IsRefarence));
                aParameters.Add(new SqlParameter("@IsCostCenter", aMaster.IsCostCenter));
                //aParameters.Add(new SqlParameter("@Buid", aMaster.BUId));
                aParameters.Add(new SqlParameter("@CreateBy", user));

                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("SaveGL", aParameters);
                if (aResponse.pk > 0)
                {
                    aResponse.isSuccess = true;
                    if (aMaster.IsRefarence == true ) 
                    {
                        aResponse.isSuccess = SaveRefarenceMap(aMaster.RefDetails, aResponse.pk);
                    }
                     if (aMaster.IsCostCenter == true ) 
                    {
                        aResponse.isSuccess = SaveCostcenterMap(aMaster.CostCenters, aResponse.pk);
                    }
                    if (aMaster.LayerFiveId == 18) //bank insert into tbl_BankAccount
                    {
                        aResponse.isSuccess = SaveGLBank(aResponse.pk,aMaster);
                    }
                    if (aMaster.LayerFiveId == 19) //cash insert into tbl_CashAccount
                    {
                        aResponse.isSuccess = SaveGLCash(aResponse.pk, aMaster);
                    }

                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                }
                else
                {
                    _accessManager.SqlConnectionClose(true);
                }
                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

       

        public bool SaveRefarenceDetails(List<RefarenceTypeDetails> aMaster, string user)
        {
            try
            {
                bool result = false;
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
                foreach (var item in aMaster)
                {
                    List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                 
                    aSqlParameterList.Add(new SqlParameter("@DetailsId", item.RefDetailsId));
                    aSqlParameterList.Add(new SqlParameter("@RefId", item.ReferanceTypeId));
                    aSqlParameterList.Add(new SqlParameter("@CreateBy",user));
                    result = _accessManager.SaveData("SaveReferanceDetails", aSqlParameterList);
                    if (result == false)
                    {
                        break;
                    }

                }
                return result;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
     
        public bool SaveRefarenceMap(List<ReferanceType> aMaster, int glId)
        {
            try
            {
                bool result = false;
                foreach (var item in aMaster)
                {
                    List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                  //  _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
                    aSqlParameterList.Add(new SqlParameter("@GlMasterId", glId));
                    aSqlParameterList.Add(new SqlParameter("@RefId", item.ReferanceTypeId));
                    aSqlParameterList.Add(new SqlParameter("@IsChecked", item.isChecked));
                    aSqlParameterList.Add(new SqlParameter("@IsRef", item.isrefrence));
                    result = _accessManager.SaveData("saveGLandReferanceMapping", aSqlParameterList);
                    if (result == false)
                    {
                        break;
                    }

                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public bool SaveGLBank( int glId, LedgerGroup model)
        {
            try
            {
                bool result = false;
                //foreach (var item in aMaster)
                //{
                    List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                 //   _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
                    aSqlParameterList.Add(new SqlParameter("@GlMasterId", glId));
                    aSqlParameterList.Add(new SqlParameter("@BankId", model.BankId));
                    aSqlParameterList.Add(new SqlParameter("@BranchId", model.BranchId));
                    aSqlParameterList.Add(new SqlParameter("@AccountName", model.LedgerGroupName));
                  
                    result = _accessManager.SaveData("SaveBankAccountFromGLEntry", aSqlParameterList);
                    //if (result == false)
                    //{
                    //    break;
                    //}

                //}
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public bool SaveGLCash( int glId, LedgerGroup model)
        {
            try
            {
                bool result = false;
                //foreach (var item in aMaster)
                //{
                    List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                 //   _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
                    aSqlParameterList.Add(new SqlParameter("@GlMasterId", glId));
                    aSqlParameterList.Add(new SqlParameter("@AccountName", model.LedgerGroupName));
                    aSqlParameterList.Add(new SqlParameter("@description", model.Description));
                  
                    result = _accessManager.SaveData("SaveCashAccountFromGLEntry", aSqlParameterList);
                    //if (result == false)
                    //{
                    //    break;
                    //}

                //}
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public bool SaveCostcenterMap(List<CostCenter> aMaster, int glId)
        {
            try
            {
                bool result = false;
                foreach (var item in aMaster)
                {
                    List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                 //   _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
                    aSqlParameterList.Add(new SqlParameter("@GlMasterId", glId));
                    aSqlParameterList.Add(new SqlParameter("@CCId", item.CostCenterId));
                    aSqlParameterList.Add(new SqlParameter("@IsChecked", item.isChecked));
                    aSqlParameterList.Add(new SqlParameter("@IsCostCenter", item.isCostCenter));
                    result = _accessManager.SaveData("saveGLandCostCenterMapping", aSqlParameterList);
                    if (result == false)
                    {
                        break;
                    }

                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public DataTable GetAllGL(int CompanyId = 0, int BUId = 0, int LedgerGroupId = 0)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CompanyId", CompanyId));
                aParameters.Add(new SqlParameter("@BUId", BUId));
                aParameters.Add(new SqlParameter("@LedgerGroupId", LedgerGroupId));

                DataTable dt = _accessManager.GetDataTable("sp_GetAllGLInfo", aParameters, true);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
                _accessManager.SqlConnectionClose();
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }





       


        public List<ViewCustomerInfo> GetCustomerListForReferanceDetails(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
                List<SqlParameter> parameters = new List<SqlParameter>();

                parameters.Add(new SqlParameter("@RefTypeId", id));

                SqlDataReader dr = _accessManager.GetSqlDataReader("GetCustomerForReferanceDetails", parameters);
                List<ViewCustomerInfo> coaList = new List<ViewCustomerInfo>();
                while (dr.Read())
                {
                    ViewCustomerInfo cSix = new ViewCustomerInfo();
                    cSix.CustomerID = Convert.ToInt32(dr["CustomerID"]);
                    cSix.CustomerName = dr["CustomerName"].ToString();
                    cSix.CustomerCode = dr["CustomerCode"].ToString();
                 


                    coaList.Add(cSix);
                }
                return coaList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public List<tbl_SupplierInfo> GetSupplierListForReferanceDetails(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
                List<SqlParameter> parameters = new List<SqlParameter>();

                parameters.Add(new SqlParameter("@RefTypeId", id));

                SqlDataReader dr = _accessManager.GetSqlDataReader("GetSupplierForReferanceDetails", parameters);
                List<tbl_SupplierInfo> coaList = new List<tbl_SupplierInfo>();
                while (dr.Read())
                {
                    tbl_SupplierInfo cSix = new tbl_SupplierInfo();
                    cSix.SupplierID = Convert.ToInt32(dr["SupplierId"]);
                    cSix.SupplierCode = dr["SupplierCode"].ToString();
                    cSix.SupplierName = dr["SupplierName"].ToString();
                 


                    coaList.Add(cSix);
                }
                return coaList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public List<View_EmployeeInfo> GetEmployeeListForReferanceDetails(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
                List<SqlParameter> parameters = new List<SqlParameter>();

                parameters.Add(new SqlParameter("@RefTypeId", id));

                SqlDataReader dr = _accessManager.GetSqlDataReader("GetEmployeeForReferanceDetails", parameters);
                List<View_EmployeeInfo> coaList = new List<View_EmployeeInfo>();
                while (dr.Read())
                {
                    View_EmployeeInfo cSix = new View_EmployeeInfo();
                 
                    cSix.EmpName = dr["EmpName"].ToString();
                    cSix.EmId = Convert.ToInt32(dr["EMPID"]);
                
                 


                    coaList.Add(cSix);
                }
                return coaList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
public List<Modules> GetModuleForReferanceDetails(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
                List<SqlParameter> parameters = new List<SqlParameter>();

                parameters.Add(new SqlParameter("@RefTypeId", id));

                SqlDataReader dr = _accessManager.GetSqlDataReader("GetModuleForReferanceDetails", parameters);
                List<Modules> coaList = new List<Modules>();
                while (dr.Read())
                {
                    Modules cSix = new Modules();
                    cSix.ModuleId = Convert.ToInt32(dr["ModuleId"]);
                    cSix.ModuleName = dr["ModuleName"].ToString();
                 
                
                 


                    coaList.Add(cSix);
                }
                return coaList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

        #region gl edit
        public GLViewModel GetGLDetailsById(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
                List<SqlParameter> parameters = new List<SqlParameter>();

                parameters.Add(new SqlParameter("@CALayerSevenId", id));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetGlDetailsById", parameters);
                GLViewModel agl = new GLViewModel();
                while (dr.Read())
                {
                    
                    agl.LedgerGroupId = Convert.ToInt32(dr["LedgerGroupId"]);
                    agl.CALayerTwoId = Convert.ToInt32(dr["CALayerTwoId"]);
                    agl.CALayerThreeId = Convert.ToInt32(dr["CALayerThreeId"]);
                    agl.CALayerFourId = Convert.ToInt32(dr["CALayerFourId"]);
                    agl.CALayerFiveId = Convert.ToInt32(dr["CALayerFiveId"]);
                    agl.CALayerSixId = Convert.ToInt32(dr["CALayerSixId"]);
                    agl.CALayerSevenId = Convert.ToInt32(dr["CALayerSevenId"]);
                    agl.CALayerSevenName = dr["CALayerSevenName"].ToString();
                    agl.CALayerSevenCode = dr["CALayerSevenCode"].ToString();
                    agl.IsActive = Convert.ToBoolean(dr["IsActive"]);
                    agl.IsCostCenter = Convert.ToBoolean(dr["IsCostCenter"]);
                    agl.IsReferance = Convert.ToBoolean(dr["IsReferance"]);
                    agl.Description = dr["Description"].ToString();

                }
                return agl;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }


        public GLViewModel GetBankAccountByGlId(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
                List<SqlParameter> parameters = new List<SqlParameter>();

                parameters.Add(new SqlParameter("@glId", id));

                SqlDataReader dr = _accessManager.GetSqlDataReader("GetBankAccountByGlId", parameters);
                GLViewModel agl = new GLViewModel();
                while (dr.Read())
                {

                    agl.BankId = Convert.ToInt32(dr["BankId"]);
                    agl.BranchId = Convert.ToInt32(dr["BranchId"]);
                    

                }
                return agl;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        #endregion gl edit









    } 
}