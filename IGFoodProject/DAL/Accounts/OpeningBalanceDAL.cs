﻿using IGFoodProject.DataManager;
using IGFoodProject.Models.Accounts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IGFoodProject.DAL.Accounts
{
    public class OpeningBalanceDAL
    {
        DataAccessManager accessManager = new DataAccessManager();
        public List<ChartOfAccLayerSeven> GetAllGL(int id)
        {
            accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
            List<SqlParameter> aParameters = new List<SqlParameter>();
            aParameters.Add(new SqlParameter("@Id", id));
            SqlDataReader dr = accessManager.GetSqlDataReader("GetGLById", aParameters);
            List<ChartOfAccLayerSeven> _cList = new List<ChartOfAccLayerSeven>();
            while (dr.Read())
            {
                ChartOfAccLayerSeven aCur = new ChartOfAccLayerSeven();
                aCur.CALayerSevenId = Convert.ToInt32(dr["CALayerSevenId"]);
                aCur.CALayerSevenCode = dr["CALayerSevenCode"].ToString();
                aCur.CALayerSevenName = dr["CALayerSevenName"].ToString();

                _cList.Add(aCur);
            }
            return _cList;
        }

        public List<OpeningBalanceView> GetOpeningValance(DateTime date, int LG = 0, int LevelTwo = 0, int LevelThree = 0, int LevelFour = 0, int LevelFive = 0, int LevelSix = 0, int GL = 0)
        {
            accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
            List<SqlParameter> aParameters = new List<SqlParameter>();
            aParameters.Add(new SqlParameter("@date", date));
            aParameters.Add(new SqlParameter("@LG", LG));
            aParameters.Add(new SqlParameter("@LevelTwo", LevelTwo));
            aParameters.Add(new SqlParameter("@LevelThree", LevelThree));
            aParameters.Add(new SqlParameter("@LevelFour", LevelFour));
            aParameters.Add(new SqlParameter("@LevelFive", LevelFive));
            aParameters.Add(new SqlParameter("@LevelSix", LevelSix));
            aParameters.Add(new SqlParameter("@GL", GL));

            SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetOpeningValance", aParameters);
            List<OpeningBalanceView> _cList = new List<OpeningBalanceView>();
            while (dr.Read())
            {
                OpeningBalanceView aCur = new OpeningBalanceView();
                aCur.DueDate = Convert.ToDateTime(dr["DueDate"]);
                aCur.OpeningBalanceId = DBNull.Value.Equals(dr["OpeningBalanceId"])?0: Convert.ToInt32(dr["OpeningBalanceId"]);
                aCur.CALayerSevenId = Convert.ToInt32(dr["CALayerSevenId"]);
                aCur.CALayerSevenCode = dr["CALayerSevenCode"].ToString();
                aCur.CALayerSevenName = dr["CALayerSevenName"].ToString();
                aCur.ReferanceTypeId = DBNull.Value.Equals(dr["ReferanceTypeId"]) ? 0 : Convert.ToInt32(dr["ReferanceTypeId"]);
                aCur.RefDetailsId = DBNull.Value.Equals(dr["RefDetailsId"]) ? 0 : Convert.ToInt32(dr["RefDetailsId"]);
                aCur.RefName= dr["RefName"].ToString();
                aCur.DrCr= dr["DrCr"].ToString();
                aCur.OpeningBalance= DBNull.Value.Equals(dr["OBLC"]) ? 0 : Convert.ToInt32(dr["OBLC"]);

                _cList.Add(aCur);
            }
            return _cList;

        }


        public bool SaveOpeningBalance(List<OpeningBalance> openingBalance, string UserName)
        {
            bool result = false;
            try

            {
                accessManager.SqlConnectionOpen(DataBase.IGFOODACCDB);
                foreach (OpeningBalance item in openingBalance)
                {
                    List<SqlParameter> gSqlParameterList = new List<SqlParameter>();

                    gSqlParameterList.Add(new SqlParameter("@OpeningBalanceId", item.OpeningBalanceId));
                    gSqlParameterList.Add(new SqlParameter("@DueDate", item.DueDate));
                    gSqlParameterList.Add(new SqlParameter("@CALayerSevenId", item.CALayerSevenId));
                    gSqlParameterList.Add(new SqlParameter("@RefTypeId", item.RefTypeId));
                    gSqlParameterList.Add(new SqlParameter("@RefDetailsId", item.RefDetailsId));
                    gSqlParameterList.Add(new SqlParameter("@DrCr", item.DrCr));
                    gSqlParameterList.Add(new SqlParameter("@OBLC", item.OBLC));
                    if(item.OpeningBalanceId >0)
                    {

                        gSqlParameterList.Add(new SqlParameter("@UpdateBy", UserName));
                        gSqlParameterList.Add(new SqlParameter("@UpdateDate", DateTime.Now));
                    }
                    else
                    {

                        gSqlParameterList.Add(new SqlParameter("@CreateBy", UserName));
                        gSqlParameterList.Add(new SqlParameter("@CreateDate", DateTime.Now));

                    }

                    result = accessManager.SaveData("sp_SaveOpeningBalance", gSqlParameterList);
                }

            }
            catch (Exception exception)
            {
                accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
            return result;
        }
    }
}