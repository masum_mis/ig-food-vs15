﻿using IGFoodProject.DataManager;
using IGFoodProject.ViewModel;
using IGFoodProject.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;


namespace IGFoodProject.DAL
{
    public class LiveBirdMortalityDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();


        public List<ViewMrrMaster> GetMrrList(int supplierId, DateTime fromDate, DateTime toDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<ViewMrrMaster> aList = new List<ViewMrrMaster>();
                List<SqlParameter> sqlParameterList = new List<SqlParameter>();
                sqlParameterList.Add(new SqlParameter("@supplierId", supplierId));
                sqlParameterList.Add(new SqlParameter("@fromDate", fromDate));
                sqlParameterList.Add(new SqlParameter("@toDate", toDate));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetMrrListForMortalityEntry", sqlParameterList);

                while (dr.Read())
                {

                    ViewMrrMaster aMaster = new ViewMrrMaster();
                    aMaster.MRRMasterId = Convert.ToInt32(dr["MRRMasterId"]);
                    aMaster.MRRNo = dr["MRRNo"].ToString();
                    aMaster.MRRDate = Convert.ToDateTime(dr["MRRDate"]);
                    aMaster.CreateDate = dr["CreateDate"] as DateTime?;
                    aMaster.ReceiveBy = dr["ReceiveBy"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.TotalReceiveQty = Convert.ToDecimal(dr["TotalReceiveQty"]);
                    aMaster.TotalReceiveKg = Convert.ToDecimal(dr["TotalReceiveKg"]);
                    aList.Add(aMaster);
                }

                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse SaveMortalityMaster(LiveBirdMortalityMaster mortalityMaster)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                ResultResponse aResponse = new ResultResponse();

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@mrrMasterId",mortalityMaster.MrrMasterId));
                aSqlParameterList.Add(new SqlParameter("@entryBy",mortalityMaster.EntryBy));
             
                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_SaveMortalityMaster", aSqlParameterList);
                if (aResponse.pk > 0)
                {
                    aResponse.isSuccess = SaveMortalityDetails(mortalityMaster.BirdMortalityDetails, aResponse.pk);

                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                    //else
                    //{
                    //    aResponse.isSuccess = StockTransferLogEntry(aMaster);

                    //}

                }
                return aResponse;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public bool SaveMortalityDetails(List<LiveBirdMortalityDetails> mortalityDetails, int pk)
        {
            try
            {
                bool result = false;
                foreach (var item in mortalityDetails)
                {
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@mortalityMasterId",pk));
                    aParameters.Add(new SqlParameter("@mrrDetailsId", item.MrrDetailsId));
                    aParameters.Add(new SqlParameter("@itemId", item.ItemId));
                    aParameters.Add(new SqlParameter("@mortalityQty", item.MortalityQty));
                    aParameters.Add(new SqlParameter("@mortalityKg", item.MortalityKg));

                    result = _accessManager.SaveData("sp_SaveMortalityDetails", aParameters);

                    if (result == false)
                    {
                        break;
                    }

                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public DataTable GetMortalityList( DateTime fromDate, DateTime toDate,int? supplierId,int? warehouseId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<SqlParameter> sqlParameterList = new List<SqlParameter>();
                sqlParameterList.Add(new SqlParameter("@fromDate", fromDate));
                sqlParameterList.Add(new SqlParameter("@toDate", toDate));
                sqlParameterList.Add(new SqlParameter("@supplierId", supplierId));
                sqlParameterList.Add(new SqlParameter("@warehouseId", warehouseId));

                DataTable dt = _accessManager.GetDataTable("sp_GetMortalityMaster", sqlParameterList);


                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
    }
}