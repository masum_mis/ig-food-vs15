﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IGFoodProject.Models;
using IGFoodProject.DataManager;
using System.Data.SqlClient;
using System.Data;

namespace IGFoodProject.DAL
{
    public class PurchaseOrderOtherDAL
    {
        DataAccessManager accessManager = new DataAccessManager();
        public List<ItemDescription> GetItem()
        {
            accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
            SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetItemOther");
            List<ItemDescription> _cList = new List<ItemDescription>();
            while (dr.Read())
            {
                ItemDescription aCur = new ItemDescription();
                aCur.ItemId = Convert.ToInt32(dr["ItemId"]);
                aCur.ItemCode = dr["ItemCode"].ToString();
                aCur.ItemName = dr["ItemName"].ToString();

                _cList.Add(aCur);
            }
            return _cList;
        }
        public List<tbl_SupplierInfo> SupplierInformation()
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetSupplierInfo");
                List<tbl_SupplierInfo> aList = new List<tbl_SupplierInfo>();
                while (dr.Read())
                {
                    tbl_SupplierInfo supplier = new tbl_SupplierInfo();
                    supplier.SupplierID = Convert.ToInt32(dr["SupplierID"]);
                    supplier.SupplierName = dr["SupplierName"].ToString();
                    aList.Add(supplier);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

        }
        public List<tbl_SupplierInfo> SupplierInformationByType(string type)
        {
            try
            {
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@SearchTerm", type));
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SqlDataReader dr = accessManager.GetSqlDataReader("GetSupplierForPurchaseByType", aParameters);
                List<tbl_SupplierInfo> aList = new List<tbl_SupplierInfo>();
                while (dr.Read())
                {
                    tbl_SupplierInfo supplier = new tbl_SupplierInfo();
                    supplier.SupplierID = Convert.ToInt32(dr["SupplierID"]);
                    supplier.SupplierName = dr["SupplierName"].ToString();
                    aList.Add(supplier);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

        }
        public ItemDescription GetItemDetails(int itemId)
        {
            try
            {
                ItemDescription aDetails = new ItemDescription();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@Id", itemId));

                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetItemDescriptionById", aParameters);
                while (dr.Read())
                {
                    aDetails.ItemId = Convert.ToInt32(dr["ItemId"]);
                    aDetails.ItemCode = dr["ItemCode"].ToString();
                    aDetails.ItemName = dr["ItemName"].ToString();
                    aDetails.Item_Description = dr["ItemDescription"].ToString();
                    aDetails.UOMId = Convert.ToInt32(dr["UOMId"]);
                }
                return aDetails;
            }
            catch (Exception ex)
            {

                throw ex;
            }

            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse SavePurchaseOrderOther(PurchaseOrderOtherMaster aMaster, string user)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);


                if (aMaster.eflag == "ed")
                {
                    aMaster.IsApprove = false;
                }
                else
                {
                    aMaster.IsApprove = false;
                }

                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@PurchaseOrderOtherID", aMaster.PurchaseOrderOtherID));
                aParameters.Add(new SqlParameter("@CompanyId", aMaster.CompanyId));
                aParameters.Add(new SqlParameter("@BUId", aMaster.BUId));
                aParameters.Add(new SqlParameter("@OrderType", aMaster.OrderType));
                aParameters.Add(new SqlParameter("@OrderDate", aMaster.OrderDate));
                aParameters.Add(new SqlParameter("@ExpectedDeliveryDate", aMaster.ExpectedDeliveryDate));
                aParameters.Add(new SqlParameter("@SupplierID", aMaster.SupplierID));
                aParameters.Add(new SqlParameter("@DelWareHouseID", aMaster.DelWareHouseID));
                aParameters.Add(new SqlParameter("@DeliveryTo", aMaster.DeliveryTo));
                aParameters.Add(new SqlParameter("@ContactInfo", aMaster.ContactInfo));
                aParameters.Add(new SqlParameter("@SupplierRef", aMaster.SupplierRef));
                aParameters.Add(new SqlParameter("@Adjustment", aMaster.Adjustment));
                aParameters.Add(new SqlParameter("@Advance", aMaster.Advance));
                aParameters.Add(new SqlParameter("@IOUHead", aMaster.IOUHead));
                aParameters.Add(new SqlParameter("@FromAccount", aMaster.FromAccount));
                aParameters.Add(new SqlParameter("@CurrencyId", aMaster.CurrencyId));
                aParameters.Add(new SqlParameter("@ConversionRate", aMaster.ConversionRate));
                aParameters.Add(new SqlParameter("@BankId", aMaster.BankId));
                aParameters.Add(new SqlParameter("@BranchId", aMaster.BranchId));
                aParameters.Add(new SqlParameter("@ChequeNo", aMaster.ChequeNo));
                aParameters.Add(new SqlParameter("@ChequeDate", aMaster.ChequeDate));
                aParameters.Add(new SqlParameter("@RefNo", aMaster.RefNo));
                aParameters.Add(new SqlParameter("@RefDate", aMaster.RefDate));
                aParameters.Add(new SqlParameter("@TotalQty", aMaster.TotalQty));
                aParameters.Add(new SqlParameter("@TotalAmount", aMaster.TotalAmount));
                aParameters.Add(new SqlParameter("@DiscountAmt", aMaster.DiscountAmt));
                aParameters.Add(new SqlParameter("@eflag", aMaster.eflag));
                aParameters.Add(new SqlParameter("@SpecialInstruction", aMaster.SpecialInstruction));
                aParameters.Add(new SqlParameter("@RequisitionId", aMaster.RequisitionId));
                aParameters.Add(new SqlParameter("@EntryBy", user));
                aParameters.Add(new SqlParameter("@EntryDate", System.DateTime.Now));
                aParameters.Add(new SqlParameter("@Comments", aMaster.Comments));
                aParameters.Add(new SqlParameter("@Tolarance", aMaster.Tolarance));
                aParameters.Add(new SqlParameter("@IsLocal", aMaster.IsLocal));
                aParameters.Add(new SqlParameter("@ApproveBy", aMaster.ApproveBy));

                if (aMaster.eflag == "ap")
                {
                    aParameters.Add(new SqlParameter("@IsApprove", true));
                }
                else
                {
                    aParameters.Add(new SqlParameter("@IsApprove", aMaster.IsApprove));
                }

                aResponse.pk = accessManager.SaveDataReturnPrimaryKey("sp_Save_PurchaseOrderOtherMaster", aParameters);
                if (aResponse.pk > 0)
                {
                    aResponse.isSuccess = SavePurchaseOrderOtherDetails(aMaster.POOtherDetails, aResponse.pk, user, aMaster.IsApprove);
                    aResponse.isSuccess = SaveTermsAndCondition(aMaster.TermsAndconditions, aResponse.pk, user);

                    if (aResponse.isSuccess == false)
                    {
                        accessManager.SqlConnectionClose(true);
                    }
                }
                else
                {
                    accessManager.SqlConnectionClose(true);
                }
                return aResponse;
            }
            catch (Exception ex)
            {
                accessManager.SqlConnectionClose(true);
                throw ex;
            }

            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public bool SavePurchaseOrderOtherDetails(List<PurchaseOrderOtherDetails> aList, int pk, string user, bool isApprove)
        {

            try
            {
                bool result = false;
                foreach (var item in aList)
                {
                    if (item.PurchaseQty > 0)
                    {
                        
                 
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@PurchaseOrderOtherID", pk));
                    aParameters.Add(new SqlParameter("@PurchaseOrderDetailID", item.PurchaseOrderDetailID));
                    aParameters.Add(new SqlParameter("@ItemId", item.ItemId));
                    aParameters.Add(new SqlParameter("@PurchaseQty", item.PurchaseQty));
                    aParameters.Add(new SqlParameter("@UOMId", item.UOMId));
                    aParameters.Add(new SqlParameter("@UnitPrice", item.UnitPrice));
                    aParameters.Add(new SqlParameter("@DiscountRate", item.DiscountRate));
                    aParameters.Add(new SqlParameter("@TotalPrice", item.TotalPrice));
                    aParameters.Add(new SqlParameter("@IsDelete", item.IsDelete));
                    aParameters.Add(new SqlParameter("@eflag", item.eflag));
                    aParameters.Add(new SqlParameter("@RequisitionQty", item.RequisitionQty));

                    result = accessManager.SaveData("sp_Save_PurchaseOrderOtherDetails", aParameters);
                    if (result == false)
                    {
                        break;
                    }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public bool SaveTermsAndCondition(TermsAndcondition aobj, int pk, string user)
        {
            try
            {
                bool result = false;

                List<SqlParameter> aParametersTr = new List<SqlParameter>();
                aParametersTr.Add(new SqlParameter("@TermsAndConditionId", aobj.TermsAndConditionId));
                aParametersTr.Add(new SqlParameter("@PurchaseOrderOtherID", pk));
                aParametersTr.Add(new SqlParameter("@TermsOfDelivery", aobj.TermsOfDelivery));
                aParametersTr.Add(new SqlParameter("@TermsOfPackaging", aobj.TermsOfPackaging));
                aParametersTr.Add(new SqlParameter("@TermsOfPayments", aobj.TermsOfPayments));
                aParametersTr.Add(new SqlParameter("@TermsOfBilling_Doc", aobj.TermsOfBilling_Doc));
                aParametersTr.Add(new SqlParameter("@OtherTerms", aobj.OtherTerms));
                aParametersTr.Add(new SqlParameter("@SpecialInstruction", aobj.SpecialInstruction));

                result = accessManager.SaveData("sp_Save_POTermsAndCondition", aParametersTr);


                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public PurchaseOrderOtherMaster GetPurchaseOrderOtherMasterById(int id)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                PurchaseOrderOtherMaster aMaster = new PurchaseOrderOtherMaster();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_PurchaseOrderOtherById", aParameters);
                while (dr.Read())
                {
                    aMaster.PurchaseOrderOtherID = Convert.ToInt32(dr["PurchaseOrderOtherID"]);
                    aMaster.CompanyId = Convert.ToInt32(dr["CompanyId"]);
                    aMaster.BUId = Convert.ToInt32(dr["BUId"]);
                    aMaster.OrderType = Convert.ToInt32(dr["OrderType"]);
                    aMaster.OrderDate = Convert.ToDateTime(dr["OrderDate"]);
                    aMaster.ExpectedDeliveryDate = Convert.ToDateTime(dr["ExpectedDeliveryDate"]);
                    aMaster.SupplierID = Convert.ToInt32(dr["SupplierID"]);
                    aMaster.DelWareHouseID = Convert.ToInt32(dr["DelWareHouseID"]);
                    aMaster.PurchaseOrderNo = dr["PurchaseOrderNo"].ToString();
                    aMaster.DeliveryTo = dr["DeliveryTo"].ToString();
                    aMaster.ContactInfo = dr["ContactInfo"].ToString();
                    aMaster.SupplierRef = dr["SupplierRef"].ToString();
                    aMaster.Adjustment = DBNull.Value.Equals(dr["Adjustment"]) ? 0 : Convert.ToDecimal(dr["Adjustment"]);
                    aMaster.Advance = DBNull.Value.Equals(dr["Advance"]) ? 0 : Convert.ToDecimal(dr["Advance"]);
                    aMaster.Tolarance = DBNull.Value.Equals(dr["TolarancePercent"]) ? 0 : Convert.ToInt32(dr["TolarancePercent"]);
                    aMaster.IOUHead = DBNull.Value.Equals(dr["IOUHead"]) ? 0 : Convert.ToInt32(dr["IOUHead"]);
                    aMaster.FromAccount = DBNull.Value.Equals(dr["FromAccount"]) ? 0 : Convert.ToInt32(dr["FromAccount"]);
                    aMaster.CurrencyId = Convert.ToInt32(dr["CurrencyId"]);
                    aMaster.ConversionRate = DBNull.Value.Equals(dr["ConversionRate"]) ? 0 : Convert.ToDecimal(dr["ConversionRate"]);
                    aMaster.BankId = DBNull.Value.Equals(dr["BankId"]) ? 0 : Convert.ToInt32(dr["BankId"]);
                    aMaster.BranchId = DBNull.Value.Equals(dr["BranchId"]) ? 0 : Convert.ToInt32(dr["BranchId"]);
                    aMaster.ChequeNo = dr["ChequeNo"].ToString();
                    aMaster.ChequeDate = dr["ChequeDate"] as DateTime?;
                    aMaster.RefNo = dr["RefNo"].ToString();
                    aMaster.RefDate = dr["RefDate"] as DateTime?;
                    aMaster.TotalQty = Convert.ToDecimal(dr["TotalQty"]);
                    aMaster.TotalAmount = Convert.ToDecimal(dr["TotalAmount"]);
                    aMaster.DiscountAmt = Convert.ToDecimal(dr["DiscountAmt"]);
                    aMaster.CreateBy = dr["CreateBy"].ToString();
                    aMaster.CreateDate = Convert.ToDateTime(dr["CreateDate"]);
                    aMaster.IsApprove = Convert.ToBoolean(dr["IsApprove"]);
                    aMaster.IsLocal = Convert.ToBoolean(dr["IsLocal"]);
                    aMaster.AccountType = DBNull.Value.Equals(dr["AccountType"]) ? 0 : Convert.ToInt32(dr["AccountType"]);
                    aMaster.SpecialInstruction = dr["SpecialInstruction"].ToString();
                    aMaster.Comments = dr["Comments"].ToString();
                }
                return aMaster;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<PurchaseOrderOtherDetails> GetPurchaseOrderOtherDetailsById(int id)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<PurchaseOrderOtherDetails> aDetails = new List<PurchaseOrderOtherDetails>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_PurchaseOrderOtherDetailsById", aParameters);
                while (dr.Read())
                {
                    PurchaseOrderOtherDetails deta = new PurchaseOrderOtherDetails();
                    deta.PurchaseOrderDetailID = Convert.ToInt32(dr["PurchaseOrderDetailID"]);
                    deta.ItemId = Convert.ToInt32(dr["ItemId"]);
                    deta.ItemName = dr["ItemName"].ToString();
                    deta.ItemDescription = dr["ItemDescription"].ToString();
                    deta.PurchaseQty = DBNull.Value.Equals(dr["PurchaseQty"]) ? 0 : Convert.ToDecimal(dr["PurchaseQty"]);
                    deta.RemainingPoQty = DBNull.Value.Equals(dr["RemainingPoQty"]) ? 0 : Convert.ToDecimal(dr["RemainingPoQty"]);
                    deta.approvedRequisitionQty = DBNull.Value.Equals(dr["approvedRequisitionQty"]) ? 0 : Convert.ToDecimal(dr["approvedRequisitionQty"]);
                    deta.RequisitionQty = DBNull.Value.Equals(dr["RequisitionQty"]) ? 0 : Convert.ToDecimal(dr["RequisitionQty"]);
                    deta.UnitPrice = DBNull.Value.Equals(dr["UnitPrice"]) ? 0 : Convert.ToDecimal(dr["UnitPrice"]);
                    deta.DiscountRate = DBNull.Value.Equals(dr["DiscountRate"]) ? 0 : Convert.ToDecimal(dr["DiscountRate"]);
                    deta.TotalPrice = DBNull.Value.Equals(dr["TotalPrice"]) ? 0 : Convert.ToDecimal(dr["TotalPrice"]);
                    deta.UOMId = DBNull.Value.Equals(dr["UOMId"]) ? 0 : Convert.ToInt32(dr["UOMId"]);
                    aDetails.Add(deta);
                }
                return aDetails;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public TermsAndcondition GetTermsAndconditionById(int id)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                TermsAndcondition aDetails = new TermsAndcondition();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_POTermsAndconditionById", aParameters);
                while (dr.Read())
                {

                    aDetails.TermsAndConditionId = Convert.ToInt32(dr["TermsAndConditionId"]);
                    aDetails.TermsOfDelivery = dr["TermsOfDelivery"].ToString();
                    aDetails.TermsOfPackaging = dr["TermsOfPackaging"].ToString();
                    aDetails.TermsOfPayments = dr["TermsOfPayments"].ToString();
                    aDetails.TermsOfBilling_Doc = dr["TermsOfBilling_Doc"].ToString();
                    aDetails.OtherTerms = dr["OtherTerms"].ToString();
                    aDetails.SpecialInstruction = dr["SpecialInstruction"].ToString();
                }
                return aDetails;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public TermsAndcondition GetTermsAndConditionDefault()
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                TermsAndcondition aDetails = new TermsAndcondition();
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_POTermsAndconditionDefault");
                while (dr.Read())
                {

                    aDetails.TermsAndConditionId = Convert.ToInt32(dr["TermsAndConditionId"]);
                    aDetails.TermsOfDelivery = dr["TermsOfDelivery"].ToString();
                    aDetails.TermsOfPackaging = dr["TermsOfPackaging"].ToString();
                    aDetails.TermsOfPayments = dr["TermsOfPayments"].ToString();
                    aDetails.TermsOfBilling_Doc = dr["TermsOfBilling_Doc"].ToString();
                    aDetails.OtherTerms = dr["OtherTerms"].ToString();
                }
                return aDetails;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<PurchaseOrderOtherMaster> GetPurchaseOrderOtherMaster(DateTime? fromdate, DateTime? todate, int SupplierId = 0, int DelWareHouseId = 0)
        {
            try
            {

                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-1);

                }
                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<PurchaseOrderOtherMaster> aList = new List<PurchaseOrderOtherMaster>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@SupplierId", SupplierId));
                aParameters.Add(new SqlParameter("@DelWareHouseId", DelWareHouseId));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_PurchaseOrderOtherList", aParameters);
                while (dr.Read())
                {
                    PurchaseOrderOtherMaster aMaster = new PurchaseOrderOtherMaster();

                    aMaster.PurchaseOrderOtherID = Convert.ToInt32(dr["PurchaseOrderOtherID"]);
                    aMaster.PurchaseOrderNo = dr["PurchaseOrderNo"].ToString();
                    aMaster.ReceivedStatus = dr["ReceivedStatus"].ToString();
                    aMaster.PurchaseType = dr["PurchaseType"].ToString();
                    aMaster.OrderDate = Convert.ToDateTime(dr["OrderDate"]);
                    aMaster.ExpectedDeliveryDate = Convert.ToDateTime(dr["ExpectedDeliveryDate"]);
                    aMaster.OrderTypeName = dr["OrderTypeName"].ToString();
                    aMaster.SupplierName = dr["SupplierName"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.DeliveryTo = dr["DeliveryTo"].ToString();
                    aMaster.ContactInfo = dr["ContactInfo"].ToString();
                    aMaster.TotalAmount = DBNull.Value.Equals(dr["TotalAmount"]) ? 0 : Convert.ToDecimal(dr["TotalAmount"]);
                    aMaster.CreateDate = Convert.ToDateTime(dr["CreateDate"]);
                    aMaster.IsApprove = Convert.ToBoolean(dr["IsApprove"]);

                    //aMaster.FinancialYearId = Convert.ToInt32(dr["FinancialYearId"]);
                    //aMaster.FinancialYear = dr["FinancialYear"].ToString();
                    //aMaster.CurrencyId = Convert.ToInt32(dr["CurrencyId"]);



                    //aMaster.TotalAmountBDT = Convert.ToDecimal(dr["TotalAmountBDT"]);
                    //aMaster.PaymentType = dr["PaymentType"].ToString();
                    //aMaster.ChequeNo = dr["ChequeNo"].ToString();
                    //aMaster.ChequeDate = dr["CreateDate"] as DateTime?;
                    //aMaster.RefNo = dr["RefNo"].ToString();
                    //aMaster.RefDate = dr["RefDate"] as DateTime?;
                    //aMaster.EntryBy = dr["EntryBy"].ToString();
                    //aMaster.EntryDate = Convert.ToDateTime(dr["EntryDate"]);
                    //aMaster.BUId = Convert.ToInt32(dr["BUId"]);
                    //aMaster.BusinessUnit = dr["ShortName"].ToString();
                    //aMaster.IsApprove = Convert.ToBoolean(dr["IsApprove"]);
                    //aMaster.BankId = DBNull.Value.Equals(dr["BankId"]) ? 0 : Convert.ToInt32(dr["BankId"]);
                    //aMaster.Bank = dr["BankName"].ToString();
                    //aMaster.BranchId = DBNull.Value.Equals(dr["BranchId"]) ? 0 : Convert.ToInt32(dr["BranchId"]);
                    //aMaster.Branch = dr["BranchName"].ToString();

                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public List<PurchaseOrderOtherMaster> GetPurchaseOrderOtherMasterForApproval(DateTime? fromdate, DateTime? todate, int SupplierId = 0, int DelWareHouseId = 0)
        {
            try
            {

                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-1);

                }
                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<PurchaseOrderOtherMaster> aList = new List<PurchaseOrderOtherMaster>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@SupplierId", SupplierId));
                aParameters.Add(new SqlParameter("@DelWareHouseId", DelWareHouseId));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_PurchaseOrderOtherListForApproval", aParameters);
                while (dr.Read())
                {
                    PurchaseOrderOtherMaster aMaster = new PurchaseOrderOtherMaster();

                    aMaster.PurchaseOrderOtherID = Convert.ToInt32(dr["PurchaseOrderOtherID"]);
                    aMaster.PurchaseOrderNo = dr["PurchaseOrderNo"].ToString();
                    aMaster.OrderDate = Convert.ToDateTime(dr["OrderDate"]);
                    aMaster.ExpectedDeliveryDate = Convert.ToDateTime(dr["ExpectedDeliveryDate"]);
                    aMaster.OrderTypeName = dr["OrderTypeName"].ToString();
                    aMaster.PurchaseType = dr["PurchaseType"].ToString();
                    aMaster.SupplierName = dr["SupplierName"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.DeliveryTo = dr["DeliveryTo"].ToString();
                    aMaster.ContactInfo = dr["ContactInfo"].ToString();
                    aMaster.TotalAmount = DBNull.Value.Equals(dr["TotalAmount"]) ? 0 : Convert.ToDecimal(dr["TotalAmount"]);
                    aMaster.CreateDate = Convert.ToDateTime(dr["CreateDate"]);
                    aMaster.IsApprove = Convert.ToBoolean(dr["IsApprove"]);

                    //aMaster.FinancialYearId = Convert.ToInt32(dr["FinancialYearId"]);
                    //aMaster.FinancialYear = dr["FinancialYear"].ToString();
                    //aMaster.CurrencyId = Convert.ToInt32(dr["CurrencyId"]);



                    //aMaster.TotalAmountBDT = Convert.ToDecimal(dr["TotalAmountBDT"]);
                    //aMaster.PaymentType = dr["PaymentType"].ToString();
                    //aMaster.ChequeNo = dr["ChequeNo"].ToString();
                    //aMaster.ChequeDate = dr["CreateDate"] as DateTime?;
                    //aMaster.RefNo = dr["RefNo"].ToString();
                    //aMaster.RefDate = dr["RefDate"] as DateTime?;
                    //aMaster.EntryBy = dr["EntryBy"].ToString();
                    //aMaster.EntryDate = Convert.ToDateTime(dr["EntryDate"]);
                    //aMaster.BUId = Convert.ToInt32(dr["BUId"]);
                    //aMaster.BusinessUnit = dr["ShortName"].ToString();
                    //aMaster.IsApprove = Convert.ToBoolean(dr["IsApprove"]);
                    //aMaster.BankId = DBNull.Value.Equals(dr["BankId"]) ? 0 : Convert.ToInt32(dr["BankId"]);
                    //aMaster.Bank = dr["BankName"].ToString();
                    //aMaster.BranchId = DBNull.Value.Equals(dr["BranchId"]) ? 0 : Convert.ToInt32(dr["BranchId"]);
                    //aMaster.Branch = dr["BranchName"].ToString();

                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public List<PurchaseOrderOtherMaster> GetPurchaseOrderOtherMasterApprove(DateTime? fromdate, DateTime? todate, int SupplierId = 0, int DelWareHouseId = 0)
        {
            try
            {

                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-1);

                }
                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<PurchaseOrderOtherMaster> aList = new List<PurchaseOrderOtherMaster>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@SupplierId", SupplierId));
                aParameters.Add(new SqlParameter("@DelWareHouseId", DelWareHouseId));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_PurchaseOrderOtherApproveList", aParameters);
                while (dr.Read())
                {
                    PurchaseOrderOtherMaster aMaster = new PurchaseOrderOtherMaster();

                    aMaster.PurchaseOrderOtherID = Convert.ToInt32(dr["PurchaseOrderOtherID"]);
                    aMaster.PurchaseOrderNo = dr["PurchaseOrderNo"].ToString();
                    aMaster.OrderDate = Convert.ToDateTime(dr["OrderDate"]);
                    aMaster.ExpectedDeliveryDate = Convert.ToDateTime(dr["ExpectedDeliveryDate"]);
                    aMaster.OrderTypeName = dr["OrderTypeName"].ToString();
                    aMaster.SupplierName = dr["SupplierName"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.DeliveryTo = dr["DeliveryTo"].ToString();
                    aMaster.ContactInfo = dr["ContactInfo"].ToString();
                    aMaster.TotalAmount = DBNull.Value.Equals(dr["TotalAmount"]) ? 0 : Convert.ToDecimal(dr["TotalAmount"]);
                    aMaster.CreateDate = Convert.ToDateTime(dr["CreateDate"]);
                    aMaster.IsApprove = Convert.ToBoolean(dr["IsApprove"]);

                    //aMaster.FinancialYearId = Convert.ToInt32(dr["FinancialYearId"]);
                    //aMaster.FinancialYear = dr["FinancialYear"].ToString();
                    //aMaster.CurrencyId = Convert.ToInt32(dr["CurrencyId"]);



                    //aMaster.TotalAmountBDT = Convert.ToDecimal(dr["TotalAmountBDT"]);
                    //aMaster.PaymentType = dr["PaymentType"].ToString();
                    //aMaster.ChequeNo = dr["ChequeNo"].ToString();
                    //aMaster.ChequeDate = dr["CreateDate"] as DateTime?;
                    //aMaster.RefNo = dr["RefNo"].ToString();
                    //aMaster.RefDate = dr["RefDate"] as DateTime?;
                    //aMaster.EntryBy = dr["EntryBy"].ToString();
                    //aMaster.EntryDate = Convert.ToDateTime(dr["EntryDate"]);
                    //aMaster.BUId = Convert.ToInt32(dr["BUId"]);
                    //aMaster.BusinessUnit = dr["ShortName"].ToString();
                    //aMaster.IsApprove = Convert.ToBoolean(dr["IsApprove"]);
                    //aMaster.BankId = DBNull.Value.Equals(dr["BankId"]) ? 0 : Convert.ToInt32(dr["BankId"]);
                    //aMaster.Bank = dr["BankName"].ToString();
                    //aMaster.BranchId = DBNull.Value.Equals(dr["BranchId"]) ? 0 : Convert.ToInt32(dr["BranchId"]);
                    //aMaster.Branch = dr["BranchName"].ToString();

                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetPurchaseOrderOtherReport(int pooMasterId)
        {
            try
            {
                DataTable dr = new DataTable();
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@id", pooMasterId));
                dr = accessManager.GetDataTable("sp_Get_PrintPurchaseOrderByMasterId", parameters);

                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public DataTable GetPurchaseOrderOtherMultipleReport(string PurchaseOrderMasterId)
        {
            try
            {
                DataTable dr = new DataTable();
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@PurchaseOrderMasterId", PurchaseOrderMasterId));
                dr = accessManager.GetDataTable("sp_Get_PrintPurchaseOrderMultiple", parameters);

                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
    }
}