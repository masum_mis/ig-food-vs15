﻿using IGFoodProject.DataManager;
using IGFoodProject.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IGFoodProject.DAL
{
    public class DirrectMrrDAL
    {
        private DataAccessManager accessManager = new DataAccessManager();
        public List<DirrectMrr> LoadReceiveWarehouse()
        {
            accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
            SqlDataReader dr = accessManager.GetSqlDataReader("sp_LoadReceiveWarehouse");
            List<DirrectMrr> mrrList = new List<DirrectMrr>(); 
            while (dr.Read())
            {
                DirrectMrr mrr = new DirrectMrr();
                mrr.WareHouseId = Convert.ToInt32(dr["WareHouseId"]);
                mrr.WareHouseName = dr["WareHouseName"].ToString();
                mrrList.Add(mrr); 
            }
            return mrrList;
        }

        public List<DirrectMrr> LoadSupplier() 
        {
            accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
            SqlDataReader dr = accessManager.GetSqlDataReader("sp_LoadSupplier");
            List<DirrectMrr> mrrList = new List<DirrectMrr>();
            while (dr.Read())
            {
                DirrectMrr mrr = new DirrectMrr();
                mrr.SupplierId = Convert.ToInt32(dr["SupplierId"]);
                mrr.SupplierName = dr["SupplierName"].ToString();
                mrrList.Add(mrr);
            }
            return mrrList;
        }

        public List<DirrectMrr> GetItemList() 
        {
            accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
            SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetItemListForMrr");
            List<DirrectMrr> mrrList = new List<DirrectMrr>();
            while (dr.Read())
            {
                DirrectMrr mrr = new DirrectMrr();
                mrr.ItemId = Convert.ToInt32(dr["ItemId"]);
                mrr.ItemCode = dr["ItemCode"].ToString();
                mrrList.Add(mrr);
            }
            return mrrList;
        }
        
        public DirrectMrr GetItemDetails(int itemId)
        {
            try
            {
                DirrectMrr aDetails = new DirrectMrr();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@Id", itemId));

                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetItemDetailsForMrr", aParameters);
                while (dr.Read())
                {
                    aDetails.ItemId = Convert.ToInt32(dr["ItemId"]);
                    aDetails.ItemCode = dr["ItemCode"].ToString();
                    aDetails.ItemDescription = dr["ItemDescription"].ToString();
                    aDetails.UOMName = dr["UOMName"].ToString();
                    aDetails.PackSizeName = dr["PackSizeName"].ToString();

                }
                return aDetails;
            }
            catch (Exception ex)
            {

                throw ex;
            }

            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse SaveDirrectMrr(DirrectMrr aMaster, string user)
        {

            try
            {
                ResultResponse aResponse = new ResultResponse();
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@MRRrOtherID", aMaster.MRRrOtherID));
                aParameters.Add(new SqlParameter("@OrderType", aMaster.OrderType));
                aParameters.Add(new SqlParameter("@MRRDate", aMaster.OrderDate));
                aParameters.Add(new SqlParameter("@OrderComments", aMaster.OrderComments));
                aParameters.Add(new SqlParameter("@ChallanNo", aMaster.ChallanNo));
                aParameters.Add(new SqlParameter("@DelWareHouseID", aMaster.WareHouseId));
                aParameters.Add(new SqlParameter("@DeliverTo", aMaster.DeliverTo));
                aParameters.Add(new SqlParameter("@Labesult", aMaster.LabResult));
                aParameters.Add(new SqlParameter("@ContactInfo", aMaster.ContactInfo));
                aParameters.Add(new SqlParameter("@SupplierId", aMaster.SupplierId));
                aParameters.Add(new SqlParameter("@SupplierCredit", aMaster.SupplierCredit));
                aParameters.Add(new SqlParameter("@SupplierRef", aMaster.SupplierReference));
                aParameters.Add(new SqlParameter("@Adjustment", aMaster.Adjustment));
                aParameters.Add(new SqlParameter("@VehicleNo", aMaster.VehicleNo));
                aParameters.Add(new SqlParameter("@VehicleInWeight", aMaster.VehicleInWeight));
                aParameters.Add(new SqlParameter("@VehicleOutWeight", aMaster.VehicleOutWeight));
                aParameters.Add(new SqlParameter("@QualityControl", aMaster.QC));
                aParameters.Add(new SqlParameter("@IOUHead", aMaster.CALayerSevenId));
                aParameters.Add(new SqlParameter("@LaborCost", aMaster.LabourCost));
                aParameters.Add(new SqlParameter("@TransportCost", aMaster.TransportCost));
                aParameters.Add(new SqlParameter("@SpecialInstruction", aMaster.SpecialInstruction));
                aParameters.Add(new SqlParameter("@TotalAmount", aMaster.TotalAmount));
                aParameters.Add(new SqlParameter("@TotalQty", aMaster.TotalQty));
                aParameters.Add(new SqlParameter("@CreateBy", user));

                aResponse.pk = accessManager.SaveDataReturnPrimaryKey("SaveDirrectMrr", aParameters);
                if (aResponse.pk > 0)
                {
                    if (aMaster.DirrectMrrList.Count > 0)
                    {
                        aResponse.isSuccess = SaveDirrectMrrDetails(aMaster.DirrectMrrList, aResponse.pk); 
                    }

                    //aResponse.isSuccess = SaveOtherStockIn(aResponse.pk);
                    //aResponse.isSuccess = CreateSystemJournal(aResponse.pk);
                    aResponse.isSuccess = SaveTermsAndcondition(aMaster, aResponse.pk); 
                    if (aResponse.isSuccess == false)
                    {
                        accessManager.SqlConnectionClose(true);
                    }
                }
                else
                {
                    accessManager.SqlConnectionClose(true);
                }
                return aResponse;
            }
            catch (Exception ex)
            {
                accessManager.SqlConnectionClose(true);
                throw ex;
            }

            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public bool SaveDirrectMrrDetails(List<DirrectMrr> aMaster, int mrrMasterId) 
        {
            try
            {
                bool result = false; 

                foreach (var item in aMaster)
                {
                    if (item.OrderQty > 0)
                    {
                        List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                        aSqlParameterList.Add(new SqlParameter("@MRRID", mrrMasterId));
                        aSqlParameterList.Add(new SqlParameter("@ItemId", item.ItemId));
                        aSqlParameterList.Add(new SqlParameter("@MRRQty", item.OrderQty));
                        aSqlParameterList.Add(new SqlParameter("@UnitPrice", item.UnitPrice));
                        aSqlParameterList.Add(new SqlParameter("@TotalPrice", item.TotalPrice));
                        aSqlParameterList.Add(new SqlParameter("@MfgDate", item.MfgDate));
                        aSqlParameterList.Add(new SqlParameter("@ExpireyDate", item.ExpireDate));

                        result = accessManager.SaveData("SaveDirrectMrrDetails", aSqlParameterList);
                    }
                    

                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public bool SaveTermsAndcondition(DirrectMrr aMaster, int mrrMasterId) 
        {
            try
            {
                bool result = false;

                   if (mrrMasterId > 0)
                    {
                        List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                        aSqlParameterList.Add(new SqlParameter("@MRRID", mrrMasterId));
                        aSqlParameterList.Add(new SqlParameter("@DeliveryTerms", aMaster.DeliveryTerms));
                        aSqlParameterList.Add(new SqlParameter("@PackagingTerms", aMaster.PackagingTerms));
                        aSqlParameterList.Add(new SqlParameter("@PaymentsTerms", aMaster.PaymentsTerms));
                        aSqlParameterList.Add(new SqlParameter("@BillingTerms", aMaster.BillingTerms));
                        aSqlParameterList.Add(new SqlParameter("@OtherTerms", aMaster.OtherTerms));

                        result = accessManager.SaveData("SaveDirrectMrrTermsAndcondition", aSqlParameterList);
                    }
           
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public bool CreateSystemJournal(int MrrMasterId)
        {

            try
            {

                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@MRRMasterId", MrrMasterId));


                result = accessManager.SaveData("SaveOtherMrrJournal", aParameters);

                return result;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public bool SaveOtherStockIn(int MrrMasterId)
        {

            try
            {

                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@MrrMasterId", MrrMasterId));


                result = accessManager.SaveData("SaveOthersMrrStockInMasterDetails", aParameters);

                //   return result;

                return result = true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public DataTable GetDirectMrrPrintReport(string ids)
        {
            try
            {
                DataTable dr = new DataTable();
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@Ids", ids));
                dr = accessManager.GetDataTable("GetDirectMrrPrintReport", parameters);

                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }


    }
}