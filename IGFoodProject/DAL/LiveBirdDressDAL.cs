﻿using IGFoodProject.DataManager;
using IGFoodProject.Models;
using IGFoodProject.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.DAL
{
    public class LiveBirdDressDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();

        public async Task<MrRDetails> ItemStock(int itemId, int wareHouseId,DateTime receivedate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@ItemID", itemId));
                parameters.Add(new SqlParameter("@WareHouseId", wareHouseId));
                parameters.Add(new SqlParameter("@ReceiveDate", receivedate));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetProductStockById", parameters);
                MrRDetails mrRDetails = new MrRDetails();
                while (dr.Read())
                {

                    mrRDetails.ItemKG = DBNull.Value== dr["ItemKG"] ? 0: Convert.ToDecimal(dr["ItemKG"]);
                    mrRDetails.ItemQty = DBNull.Value == dr["ItemQty"] ? 0 : Convert.ToDecimal(dr["ItemQty"]);
                }

                return mrRDetails;
            }
            catch (Exception ex)
            {

                throw ex;
                _accessManager.SqlConnectionClose(true);
            }
            finally
            {

                _accessManager.SqlConnectionClose(true);
            }

        }
        public dynamic SaveDressBird(DressedBirdMaster dressedBirdMaster)
        {
            if (dressedBirdMaster.Details != null)
            {
                try
                {
                    _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                    ResultResponse aResponse = new ResultResponse();
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@DressedBirdMasterID", dressedBirdMaster.DressedBirdMasterID));
                    aParameters.Add(new SqlParameter("@DressedBirdNo", dressedBirdMaster.DressedBirdNo));
                    aParameters.Add(new SqlParameter("@DressingDate", dressedBirdMaster.DressingDate));
                    aParameters.Add(new SqlParameter("@DressedBy", dressedBirdMaster.DressedBy));
                    aParameters.Add(new SqlParameter("@CreateBy", dressedBirdMaster.CreateBy));
                    aParameters.Add(new SqlParameter("@CreatDate", dressedBirdMaster.CreatDate));
                    aParameters.Add(new SqlParameter("@UpdateBy", dressedBirdMaster.UpdateBy));
                    aParameters.Add(new SqlParameter("@UpdateDate", dressedBirdMaster.UpdateDate));
                    aParameters.Add(new SqlParameter("@Remarks", dressedBirdMaster.Remarks));
                    aParameters.Add(new SqlParameter("@StockInStatus", dressedBirdMaster.StockInStatus));
                    aParameters.Add(new SqlParameter("@DressedStatus", dressedBirdMaster.DressedStatus)); 
                    aParameters.Add(new SqlParameter("@WareHouseId", dressedBirdMaster.WareHouseId));
                    aParameters.Add(new SqlParameter("@ToWareHouseId", dressedBirdMaster.ToWareHouseId));
                    aParameters.Add(new SqlParameter("@MrrDate", dressedBirdMaster.MRRDate));
                    aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_SavedressBirdMaster", aParameters);
                    if (aResponse.pk > 0)
                    {
                        foreach (var item in dressedBirdMaster.Details)
                        {
                            List<SqlParameter> sqlParameters = new List<SqlParameter>();

                            sqlParameters.Add(new SqlParameter("@DressedBirdMasterID", aResponse.pk));
                            sqlParameters.Add(new SqlParameter("@ItemID", item.ItemID));
                            sqlParameters.Add(new SqlParameter("@ApprovedQty", item.DressedQty));
                            sqlParameters.Add(new SqlParameter("@ApprovedKG", item.DressedInKG));
                            sqlParameters.Add(new SqlParameter("@Remark", item.Remark));
                            sqlParameters.Add(new SqlParameter("@WareHouseId", dressedBirdMaster.WareHouseId));
                            sqlParameters.Add(new SqlParameter("@CreateBy", dressedBirdMaster.CreateBy));
                            sqlParameters.Add(new SqlParameter("@CreateDate", dressedBirdMaster.CreatDate));
                            sqlParameters.Add(new SqlParameter("@DressingDate", dressedBirdMaster.DressingDate));
                            sqlParameters.Add(new SqlParameter("@DressedBy", dressedBirdMaster.DressedBy));
                            aResponse.isSuccess = _accessManager.SaveData("sp_SaveDressBirdDetails", sqlParameters);
                            
                        }

                       // Lipi for accounting
                        List < SqlParameter > Param = new List<SqlParameter>();
                       Param.Add(new SqlParameter("@DressedBirdMasterID", aResponse.pk));
                        aResponse.isSuccess = _accessManager.SaveData("sp_LiveBirdToDressingVoucherEntry", Param);

                        if (aResponse.isSuccess == false)
                        {
                            _accessManager.SqlConnectionClose(true);
                        }
                    }

                    return aResponse;
                }
                catch (Exception ex)
                {
                    _accessManager.SqlConnectionClose(true);
                    throw ex;
                }
                finally
                {
                    _accessManager.SqlConnectionClose();
                }

            }

            return null;
        }

        public dynamic SaveDressBird11(DressedBirdMaster dressedBirdMaster)
        {
            if (dressedBirdMaster.Details != null)
            {
                try
                {
                    _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                    ResultResponse aResponse = new ResultResponse();
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@DressedBirdMasterID", dressedBirdMaster.DressedBirdMasterID));
                    aParameters.Add(new SqlParameter("@DressedBirdNo", dressedBirdMaster.DressedBirdNo));
                    aParameters.Add(new SqlParameter("@DressingDate", dressedBirdMaster.DressingDate));
                    aParameters.Add(new SqlParameter("@DressedBy", dressedBirdMaster.DressedBy));
                    aParameters.Add(new SqlParameter("@CreateBy", dressedBirdMaster.CreateBy));
                    aParameters.Add(new SqlParameter("@CreatDate", dressedBirdMaster.CreatDate));
                    aParameters.Add(new SqlParameter("@UpdateBy", dressedBirdMaster.UpdateBy));
                    aParameters.Add(new SqlParameter("@UpdateDate", dressedBirdMaster.UpdateDate));
                    aParameters.Add(new SqlParameter("@Remarks", dressedBirdMaster.Remarks));
                    aParameters.Add(new SqlParameter("@StockInStatus", dressedBirdMaster.StockInStatus));
                    aParameters.Add(new SqlParameter("@DressedStatus", dressedBirdMaster.DressedStatus));
                    aParameters.Add(new SqlParameter("@WareHouseId", dressedBirdMaster.WareHouseId));
                    aParameters.Add(new SqlParameter("@ToWareHouseId", dressedBirdMaster.ToWareHouseId));
                    aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_SavedressBirdMaster", aParameters);
                    if (aResponse.pk > 0)
                    {
                        foreach (var item in dressedBirdMaster.Details)
                        {
                            List<SqlParameter> sqlParameters = new List<SqlParameter>();

                            sqlParameters.Add(new SqlParameter("@DressedBirdMasterID", aResponse.pk));
                            sqlParameters.Add(new SqlParameter("@ItemID", item.ItemID));
                            sqlParameters.Add(new SqlParameter("@DressedQty", item.DressedQty));
                            sqlParameters.Add(new SqlParameter("@DressedInKG", item.DressedInKG));
                            sqlParameters.Add(new SqlParameter("@Remark", item.Remark));
                            sqlParameters.Add(new SqlParameter("@WareHouseId", dressedBirdMaster.WareHouseId));
                            sqlParameters.Add(new SqlParameter("@CreateBy", dressedBirdMaster.CreateBy));
                            sqlParameters.Add(new SqlParameter("@CreateDate", dressedBirdMaster.CreatDate));
                            sqlParameters.Add(new SqlParameter("@DressingDate", dressedBirdMaster.DressingDate));
                            sqlParameters.Add(new SqlParameter("@DressedBy", dressedBirdMaster.DressedBy));
                            aResponse.isSuccess = _accessManager.SaveData("sp_SaveDressBirdDetails", sqlParameters);
                            if (aResponse.isSuccess == false)
                            {
                                _accessManager.SqlConnectionClose(true);
                            }
                        }
                    }

                    return aResponse;
                }
                catch (Exception ex)
                {
                    _accessManager.SqlConnectionClose(true);
                    throw ex;
                }
                finally
                {
                    _accessManager.SqlConnectionClose();
                }

            }

            return null;
        }
        public dynamic LiveBirdDressList()
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameters = new List<SqlParameter>();
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetLiveBirdDressList");
                List<DressedBirdMaster> dressedBirdMasterList = new List<DressedBirdMaster>();
                while (dr.Read())
                {
                    DressedBirdMaster dressedBirdMaster = new DressedBirdMaster();
                    dressedBirdMaster.DressedBirdMasterID = Convert.ToInt32(dr["DressedBirdMasterID"]);
                    dressedBirdMaster.DressedBirdNo = dr["DressedBirdNo"].ToString();
                    dressedBirdMaster.DressingDate = dr["DressingDate"].ToString();
                    dressedBirdMaster.DressedBy = dr["DressedBy"].ToString();
                    dressedBirdMaster.Remarks = dr["Remarks"].ToString();
                    dressedBirdMaster.EmpName = dr["EmpName"].ToString();
                    dressedBirdMaster.WareHouseName = dr["WareHouseName"].ToString();
                    if (!DBNull.Value.Equals(dr["IsApprove"]))
                    {

                        dressedBirdMaster.IsApprove = Convert.ToBoolean(dr["IsApprove"]);
                    }
                    dressedBirdMasterList.Add(dressedBirdMaster);
                }

                return dressedBirdMasterList;

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public dynamic GetLiveBirdDressById(int id)
        {
            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameters = new List<SqlParameter>();
                aSqlParameters.Add(new SqlParameter("@DressedBirdMasterID", id));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetLiveBirdDressList", aSqlParameters);
                DressedBirdMaster dressedBirdMaster = new DressedBirdMaster();
                while (dr.Read())
                {
                    dressedBirdMaster.DressedBirdNo = dr["DressedBirdNo"].ToString();
                    dressedBirdMaster.DressingDate = dr["DressingDate"].ToString();
                    dressedBirdMaster.EmpName = dr["EmpName"].ToString();
                    dressedBirdMaster.Remarks = dr["Remarks"].ToString();
                    dressedBirdMaster.WareHouseName =dr["WareHouseName"].ToString();
                }
                _accessManager.SqlConnectionClose();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SqlDataReader drDetails = _accessManager.GetSqlDataReader("sp_GetLiveBirdDressDetailsById", aSqlParameters);
                List<DressedBirdDetail> DressedBirdDetailsList = new List<DressedBirdDetail>();
                while (drDetails.Read())
                {
                    DressedBirdDetail dressedBirdDetails = new DressedBirdDetail();
                   // dressedBirdDetails.DressedBirdDetailID = Convert.ToInt32(drDetails["DressedBirdDetailID"]);
                    dressedBirdDetails.DressedBirdMasterID = Convert.ToInt32(drDetails["DressedBirdMasterID"]);
                    dressedBirdDetails.ItemID = Convert.ToInt32(drDetails["ItemID"]);
                    dressedBirdDetails.DressedQty = Convert.ToDecimal(drDetails["DressedQty"]);
                    dressedBirdDetails.DressedInKG = Convert.ToDecimal(drDetails["DressedInKG"]);
                    dressedBirdDetails.Remark = drDetails["Remark"].ToString();
                    dressedBirdDetails.ItemName = drDetails["ItemName"].ToString();
                    DressedBirdDetailsList.Add(dressedBirdDetails);
                }
                dressedBirdMaster.Details = DressedBirdDetailsList;
                return dressedBirdMaster;

            }
            catch (Exception exception)
            {


                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public dynamic LoadAllWareHouse()
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameters = new List<SqlParameter>();
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetAllWareHouse");
                List<WareHouse> wareHouseList = new List<WareHouse>();
                while (dr.Read())
                {
                    WareHouse wareHouse = new WareHouse();
                    wareHouse.WareHouseId = Convert.ToInt32(dr["WareHouseId"]);
                    wareHouse.WarehouseTypeId = Convert.ToInt32(dr["WarehouseTypeId"]);
                    wareHouse.WareHouseName = dr["WareHouseName"].ToString();
                   
                    wareHouseList.Add(wareHouse);
                }

                return wareHouseList;

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public dynamic LoadReceiveDate()
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameters = new List<SqlParameter>();
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetReceiveDate");
                List<MrRMaster> wareHouseList = new List<MrRMaster>();
                while (dr.Read())
                {
                    MrRMaster rece = new MrRMaster();
                    rece.ReceiveDateStr = dr["MRRDate"].ToString();
                    //rece.ReceiveDateStr = dr["MRRDate"].ToString();
                    

                    wareHouseList.Add(rece);
                }

                return wareHouseList;

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public dynamic GetLiveBirdDressForApprove(int id)
        {
            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameters = new List<SqlParameter>();
                aSqlParameters.Add(new SqlParameter("@DressedBirdMasterID", id));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetLiveBirdDressList", aSqlParameters);
                DressedBirdMaster dressedBirdMaster = new DressedBirdMaster();
                while (dr.Read())
                {
                    dressedBirdMaster.DressedBirdMasterID = Convert.ToInt32(dr["DressedBirdMasterID"]);
                    dressedBirdMaster.DressedBirdNo = dr["DressedBirdNo"].ToString();
                    dressedBirdMaster.DressingDate = dr["DressingDate"].ToString();
                    dressedBirdMaster.EmpName = dr["EmpName"].ToString();
                    dressedBirdMaster.Remarks = dr["Remarks"].ToString();
                    dressedBirdMaster.WareHouseName = dr["WareHouseName"].ToString();
                }
                _accessManager.SqlConnectionClose();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SqlDataReader drDetails = _accessManager.GetSqlDataReader("sp_GetDressBirdDetails", aSqlParameters);
                List<DressedBirdDetail> DressedBirdDetailsList = new List<DressedBirdDetail>();
                while (drDetails.Read())
                {
                    DressedBirdDetail dressedBirdDetails = new DressedBirdDetail();
                    dressedBirdDetails.DressedBirdDetailID = Convert.ToInt32(drDetails["DressedBirdDetailID"]);
                    dressedBirdDetails.DressedBirdMasterID = Convert.ToInt32(drDetails["DressedBirdMasterID"]);
                    dressedBirdDetails.ItemID = Convert.ToInt32(drDetails["ItemID"]);
                    dressedBirdDetails.ItemName = drDetails["ItemName"].ToString();
                    dressedBirdDetails.DressedQty = Convert.ToDecimal(drDetails["DressedQty"]);
                    dressedBirdDetails.DressedInKG = Convert.ToDecimal(drDetails["DressedInKG"]);
                    dressedBirdDetails.StockQty = Convert.ToDecimal(drDetails["StockInQty"]);
                    dressedBirdDetails.StockKg = Convert.ToDecimal(drDetails["StockInKG"]);
                    dressedBirdDetails.Remark = drDetails["Remark"].ToString();
                    DressedBirdDetailsList.Add(dressedBirdDetails);
                }
                dressedBirdMaster.Details = DressedBirdDetailsList;
                return dressedBirdMaster;

            }
            catch (Exception exception)
            {


                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public dynamic ApproveDressBird(DressedBirdMaster dressedBirdMaster)
        {
            if (dressedBirdMaster.Details != null)
            {
                try
                {
                    _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                    ResultResponse aResponse = new ResultResponse();
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@DressedBirdMasterID", dressedBirdMaster.DressedBirdMasterID));
                    aParameters.Add(new SqlParameter("@ApprovedBy", dressedBirdMaster.ApprovedBy));
                    aParameters.Add(new SqlParameter("@ApprovedDate", dressedBirdMaster.ApprovedDate));
                    aResponse.isSuccess = _accessManager.SaveData("sp_ApproveLiveBirdDress", aParameters);
                    if (aResponse.isSuccess)
                    {
                        foreach (var item in dressedBirdMaster.Details)
                        {
                            List<SqlParameter> sqlParameters = new List<SqlParameter>();

                            sqlParameters.Add(new SqlParameter("@DressedBirdDetailID", item.DressedBirdDetailID));
                            sqlParameters.Add(new SqlParameter("@ItemID", item.ItemID));
                            sqlParameters.Add(new SqlParameter("@ApprovedQty", item.ApprovedQty));
                            sqlParameters.Add(new SqlParameter("@ApprovedKG", item.ApprovedKG));
                            sqlParameters.Add(new SqlParameter("@Remark", item.Remark));
                            sqlParameters.Add(new SqlParameter("@WareHouseId", dressedBirdMaster.WareHouseId));
                            sqlParameters.Add(new SqlParameter("@CreateBy", dressedBirdMaster.ApprovedBy));
                            sqlParameters.Add(new SqlParameter("@CreateDate", dressedBirdMaster.ApprovedDate));
                            sqlParameters.Add(new SqlParameter("@DressingDate", dressedBirdMaster.DressingDate));
                            sqlParameters.Add(new SqlParameter("@DressedBy", dressedBirdMaster.DressedBy));
                            aResponse.isSuccess = _accessManager.SaveData("sp_SaveDressBirdDetails", sqlParameters);
                            if (aResponse.isSuccess == false)
                            {
                                _accessManager.SqlConnectionClose(true);
                            }
                        }
                    }

                    return aResponse;
                }
                catch (Exception ex)
                {
                    _accessManager.SqlConnectionClose(true);
                    throw ex;
                }
                finally
                {
                    _accessManager.SqlConnectionClose();
                }

            }

            return null;
        }

        public DataTable GetLiveBirdDressReport(DateTime fromDate, DateTime toDate, int itemId = 0)
        {
            _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
            ResultResponse aResponse = new ResultResponse();
            List<SqlParameter> aParameters = new List<SqlParameter>();
            aParameters.Add(new SqlParameter("@FromDate", fromDate));
            aParameters.Add(new SqlParameter("@ToDate", toDate));
            aParameters.Add(new SqlParameter("@ItemId", itemId));
            DataTable dt = _accessManager.GetDataTable("sp_LiveBirdDressReport", aParameters);
            return dt;
        }
        public dynamic LiveBirdDressNoList(DateTime dressDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameters = new List<SqlParameter>();
                aSqlParameters.Add(new SqlParameter("@DressDate", dressDate));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetDressBirdNo",aSqlParameters);
                List<DressedBirdMaster> dressedBirdMasterList = new List<DressedBirdMaster>();
                while (dr.Read())
                {
                    DressedBirdMaster dressedBirdMaster = new DressedBirdMaster();
                    dressedBirdMaster.DressedBirdMasterID = Convert.ToInt32(dr["DressedBirdMasterID"]);
                    dressedBirdMaster.DressedBirdNo = dr["DressedBirdNo"].ToString();
                    dressedBirdMasterList.Add(dressedBirdMaster);
                }

                return dressedBirdMasterList;

            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public DataTable GetLiveBirdDressReportInvoice(int dressedBirdMasterId)
        {
            _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
            ResultResponse aResponse = new ResultResponse();
            List<SqlParameter> aParameters = new List<SqlParameter>();
            aParameters.Add(new SqlParameter("@DressedBirdMasterID", dressedBirdMasterId));
            DataTable dt = _accessManager.GetDataTable("sp_LiveBirdDressReportInvoice", aParameters);
            return dt;
        }
     
    }
}
