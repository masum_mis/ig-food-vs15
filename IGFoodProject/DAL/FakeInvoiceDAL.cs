﻿using IGFoodProject.DataManager;
using IGFoodProject.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using IGFoodProject.ViewModel;
using System.Data;

namespace IGFoodProject.DAL
{
    public class FakeInvoiceDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();
        public List<ViewSaleOrder> GetMainInvoice(int customerid, string orderdate)
        {

            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ViewSaleOrder> aList = new List<ViewSaleOrder>();

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@CustomerId", customerid));
                aSqlParameterList.Add(new SqlParameter("@OrderDate", orderdate));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetInvoiceList", aSqlParameterList);
                while (dr.Read())
                {
                    ViewSaleOrder aDetail = new ViewSaleOrder();
                    
                    aDetail.SalesOrderProcessedMasterId =Convert.ToInt32( dr["SalesOrderProcessedMasterId"]);
                    aDetail.SalesOrderNo = dr["SalesOrderNo"].ToString();
                    
                    aList.Add(aDetail);
                }
                return aList;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public ViewSaleOrder GetOrderInfo(int orderid)
        {

            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
               

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@SalesOrderProcessedMasterId", orderid));
                ViewSaleOrder aDetail = new ViewSaleOrder();

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetOrderInfo", aSqlParameterList);
                while (dr.Read())
                {
                    

                    aDetail.SalesOrderProcessedMasterId = Convert.ToInt32(dr["SalesOrderProcessedMasterId"]);
                    aDetail.SalesOrderNo = dr["SalesOrderNo"].ToString();
                    aDetail.OrderDate=Convert.ToDateTime( dr["OrderDate"]);


                }
                return aDetail;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse SaveSalesOrderProcessed(SalesOrderProcessedMaster_FalseInvoice aMaster, string user)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);



                

                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@FalseSalesOrderProcessedMasterId", aMaster.FalseSalesOrderProcessedMasterId));
                aParameters.Add(new SqlParameter("@SalesOrderProcessedMasterId", aMaster.SalesOrderProcessedMasterId));
                aParameters.Add(new SqlParameter("@InvoiceDate", aMaster.InvoiceDate));
                aParameters.Add(new SqlParameter("@TotalExTaxVat", aMaster.TotalExTaxVat));
                aParameters.Add(new SqlParameter("@TotalIncTaxVat", aMaster.TotalIncTaxVat));
                aParameters.Add(new SqlParameter("@TotalDue", aMaster.TotalDue));
                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_Save_SalesOrderProcessed_FalseInvoice", aParameters);
                if (aResponse.pk > 0)
                {
                    
                        aResponse.isSuccess = SaveSalesOrderProcessedDetails(aMaster.SalesDetails, aResponse.pk, aMaster.SalesOrderProcessedMasterId, user);
                    

                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                }
                else
                {
                    _accessManager.SqlConnectionClose(true);
                }
                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public bool SaveSalesOrderProcessedDetails(List<SalesOrderProcessedDetails_FalseInvoice> aList, int pk, int salesmasterid,string user)
        {
            try
            {
                bool result = false;
                foreach (var item in aList)
                {
                    
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@FalseSalesOrderProcessDetailsId", item.FalseSalesOrderProcessDetailsId));
                    aParameters.Add(new SqlParameter("@FalseSalesOrderProcessedMasterId", pk));
                    aParameters.Add(new SqlParameter("@SalesOrderProcessedMasterId", salesmasterid));
                    
                    aParameters.Add(new SqlParameter("@SalesOrderProcessDetailsId", item.SalesOrderProcessDetailsId));
                    aParameters.Add(new SqlParameter("@ProductId", item.ProductId));
                    aParameters.Add(new SqlParameter("@ProductQty", item.ProductQty));
                    aParameters.Add(new SqlParameter("@ProductKg", item.ProductKg));
                    aParameters.Add(new SqlParameter("@StockRate", item.StockRate));
                    aParameters.Add(new SqlParameter("@SaleRate", item.SaleRate));
                    aParameters.Add(new SqlParameter("@DiscountAmount", item.DiscountAmount));
                    aParameters.Add(new SqlParameter("@DiscountPercent", item.DiscountPercent));
                    aParameters.Add(new SqlParameter("@CreateBy", user));
                    aParameters.Add(new SqlParameter("@CreateDate", DateTime.Now));
                    aParameters.Add(new SqlParameter("@TotalPrice", item.TotalPrice));
                    aParameters.Add(new SqlParameter("@ProductTax", item.ProductTax));
                   
                    aParameters.Add(new SqlParameter("@groupid", item.GroupId));
                    result = _accessManager.SaveData("sp_Save_SavelOrderProcessedDetails_FalseInvoice", aParameters);
                    if (result == false)
                    {
                        break;
                    }

                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<ViewSalesOrderProcessedMaster_False> GetSalesOrderProcessedMaster(DateTime? invoicedate,  int customerid = 0)
        {
            try
            {

                if (invoicedate == null)
                {
                    invoicedate = System.DateTime.Now;

                }
                
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<ViewSalesOrderProcessedMaster_False> aList = new List<ViewSalesOrderProcessedMaster_False>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CustomerId", customerid));
                
                aParameters.Add(new SqlParameter("@InvoiceDate", String.Format("{0:MM/dd/yyyy}", invoicedate)));
                
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetFakeInvoiceList", aParameters);
                while (dr.Read())
                {
                    ViewSalesOrderProcessedMaster_False aMaster = new ViewSalesOrderProcessedMaster_False();
                    aMaster.FalseSalesOrderProcessedMasterId = Convert.ToInt32(dr["FalseSalesOrderProcessedMasterId"]);
                    aMaster.SalesOrderProcessedMasterId = Convert.ToInt32(dr["SalesOrderProcessedMasterId"]);
                    
                    aMaster.InvoiceNo = dr["InvoiceNo"].ToString();
                    aMaster.SalesOrderNo = dr["SalesOrderNo"].ToString();
                    aMaster.CustomerName = dr["customername"].ToString();
                    
                    
                    aMaster.OrderDate = dr["OrderDate"] as DateTime?;
                    aMaster.InvoiceDate = dr["InvoiceDate"] as DateTime?;
                    
                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public SalesOrderProcessedMaster_FalseInvoice GetSalesOrderProcessedMaster_False(int id)
        {
            try
            {
                List<SqlParameter> aParameters = new List<SqlParameter>();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                aParameters.Add(new SqlParameter("@id", id));
                DataTable dt = _accessManager.GetDataTable("sp_Get_FalseSalesOrderProcessedMasrerById", aParameters);
                List<SalesOrderProcessedMaster_FalseInvoice> aList = Helper.ToListof<SalesOrderProcessedMaster_FalseInvoice>(dt);
                return aList[0];
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<SalesOrderProcessedDetails_FalseInvoice> GetSalesOderProcessedDetailsByMaster_False(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                DataTable dt = _accessManager.GetDataTable("sp_Get_SalesOrderPrecessedDetailsByMaster_False", aParameters);
                List<SalesOrderProcessedDetails_FalseInvoice> aList = Helper.ToListof<SalesOrderProcessedDetails_FalseInvoice>(dt);
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
    }
}