﻿using IGFoodProject.DataManager;
using IGFoodProject.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using IGFoodProject.ViewModel;

namespace IGFoodProject.DAL
{
    public class IssuePortionDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();

        public dynamic GetDBPortionReqNOByWareHouse()
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<SqlParameter> aParameters = new List<SqlParameter>();

                List<tbl_DBToPortionRequisitionMaster> aList = new List<tbl_DBToPortionRequisitionMaster>();

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetPortionReqNoWarehouse");
                while (dr.Read())
                {
                    tbl_DBToPortionRequisitionMaster aMaster = new tbl_DBToPortionRequisitionMaster();
                    aMaster.DBToPortionReqID = Convert.ToInt32(dr["DBToPortionReqID"]);
                    aMaster.RequisitionNo = dr["RequisitionNo"].ToString();
                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public dynamic GetDbPortionReqInfoById(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@DBToPortionReqID", id));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetPortionReqNo", parameters);
                tbl_DBToPortionRequisitionMaster aMaster = new tbl_DBToPortionRequisitionMaster();
                while (dr.Read())
                {

                    aMaster.CompanyID = Convert.ToInt32(dr["CompanyID"]);
                    aMaster.LocationID = Convert.ToInt32(dr["LocationID"]);
                    aMaster.WareHouseID = Convert.ToInt32(dr["WareHouseID"]);

                    aMaster.RequisitionDate = Convert.ToDateTime(dr["RequisitionDate"]).ToString("dd-MMM-yyyy");

                }
                return aMaster;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public dynamic GetDbPortionReqViewInfoById(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@DBToPortionReqID", id));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_PortionReqViewById", parameters);
                ViewDbPortionReqMaster aMaster = new ViewDbPortionReqMaster();
                while (dr.Read())
                {

                    aMaster.FromWarehouseName = dr["FromWarehouseName"].ToString();
                    aMaster.ToWarehouseName = dr["ToWarehouseName"].ToString();
                    aMaster.ToWarehouseId = Convert.ToInt32(dr["ToWarehouseId"].ToString());
                    aMaster.WareHouseID = Convert.ToInt32(dr["WareHouseID"].ToString());

                    aMaster.RequisitionDate = Convert.ToDateTime(dr["RequisitionDate"]).ToString("dd-MMM-yyyy");
                    aMaster.ProductionDate = Convert.ToDateTime(dr["ProductionDate"]).ToString("dd-MMM-yyyy");

                }
                return aMaster;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public dynamic GetDbPortionReqDetailsById(int dBToPortionReqID)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@DBToPortionReqID", dBToPortionReqID));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_DbToPortionReqDetails", parameters);
                List<tbl_DBToPortionRequisitionDetail> aList = new List<tbl_DBToPortionRequisitionDetail>();
                while (dr.Read())
                {
                    tbl_DBToPortionRequisitionDetail aMaster = new tbl_DBToPortionRequisitionDetail();
                    aMaster.DBToPortionReqID = Convert.ToInt32(dr["DBToPortionReqID"]);
                    aMaster.ProductID = Convert.ToInt32(dr["ProductID"]);
                    aMaster.ProductQty = Convert.ToDecimal(dr["ProductQty"]);
                    aMaster.ProductQtyInKG = Convert.ToDecimal(dr["ProductQtyInKG"]);
                    aMaster.DBToPortionRequisitionDetailID = Convert.ToInt32(dr["DBToPortionRequisitionDetailID"]);
                    aMaster.Remarks = DBNull.Value == dr["Remarks"] ? "" : dr["Remarks"].ToString();
                    aMaster.ProductName = dr["ProductName"].ToString();
                    aMaster.PerKgStdQty = DBNull.Value == dr["PerKgStdQty"] ? 0 : Convert.ToDecimal(dr["PerKgStdQty"]);
                    aMaster.GroupId = Convert.ToInt32(dr["GroupId"]);
                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public dynamic GetDbPortionReqDetails2ById(int DBToPortionReqID)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@DBToPortionReqID", DBToPortionReqID));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetDbToPortionReqDetails2", parameters);
                List<tbl_DBToPortionRequisition2ndDetail> aList = new List<tbl_DBToPortionRequisition2ndDetail>();
                while (dr.Read())
                {
                    tbl_DBToPortionRequisition2ndDetail aMaster = new tbl_DBToPortionRequisition2ndDetail();

                    aMaster.DBToPortionReqID = Convert.ToInt32(dr["DBToPortionReqID"]);
                    aMaster.ProductID = Convert.ToInt32(dr["ProductID"]);
                    aMaster.ProductQty = Convert.ToDecimal(dr["ProductQty"]);
                    aMaster.ProductQtyInKG = Convert.ToDecimal(dr["ProductQtyInKG"]);
                    aMaster.Remark = DBNull.Value == dr["Remark"] ? "" : dr["Remark"].ToString();
                    aMaster.ProductName = dr["ProductName"].ToString();
                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public dynamic DbToPortionIssue(DBIssuePortionMaster entity)
        {
            if (entity.DbIssuePortionDetails != null)
            {
                try
                {
                    _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                    ResultResponse aResponse = new ResultResponse();
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@DBToPortionReqID", entity.DBToPortionReqID));
                    aParameters.Add(new SqlParameter("@CompanyID", entity.CompanyID));
                    aParameters.Add(new SqlParameter("@LocationID", entity.LocationID));
                    aParameters.Add(new SqlParameter("@WareHouseID", entity.WareHouseID));
                    aParameters.Add(new SqlParameter("@IssueDate", entity.IssueDate));
                    aParameters.Add(new SqlParameter("@ProductionDate", entity.ProductionDate));
                    aParameters.Add(new SqlParameter("@Comments", entity.Comments));
                    aParameters.Add(new SqlParameter("@CreateBy", entity.CreateBy));
                    aParameters.Add(new SqlParameter("@CreateDate", entity.CreateDate));
                    aParameters.Add(new SqlParameter("@StockInStatus_Portion", 3));
                    aParameters.Add(new SqlParameter("@StockInStatus_ByProduct", 3));
                    aParameters.Add(new SqlParameter("@IssueBy", entity.IssueBy));
                    aParameters.Add(new SqlParameter("@ToWareHouseID", entity.ToWarehouseId));
                    aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_SaveDbPortionIssue", aParameters);
                    if (aResponse.pk > 0)
                    {
                        foreach (var item in entity.DbIssuePortionDetails)
                        {
                            List<SqlParameter> sqlParameters = new List<SqlParameter>();
                            sqlParameters.Add(new SqlParameter("@DBIssueMasterID", aResponse.pk));
                            sqlParameters.Add(new SqlParameter("@DBToPortionRequisitionDetailID",
                                item.DBToPortionRequisitionDetailID));
                            sqlParameters.Add(new SqlParameter("@ProductID", item.ProductID));
                            sqlParameters.Add(new SqlParameter("@WareHouseID", entity.WareHouseID));
                            sqlParameters.Add(new SqlParameter("@IssueQty", item.IssueQty));
                            sqlParameters.Add(new SqlParameter("@IssueQtyInKG", item.IssueQtyInKG));

                            sqlParameters.Add(new SqlParameter("@Remarks", item.Remarks));
                            aResponse.isSuccess = _accessManager.SaveData("sp_SaveDbProtionIssueDetail", sqlParameters);
                            if (aResponse.isSuccess == false)
                            {
                                _accessManager.SqlConnectionClose(true);
                            }
                        }


                    }
                    return aResponse;
                }
                catch (Exception ex)
                {
                    _accessManager.SqlConnectionClose(true);
                    throw ex;
                }
                finally
                {
                    _accessManager.SqlConnectionClose();
                }

            }

            return null;
        }



        public dynamic GetDbPortionIssueList()
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                SqlDataReader dr = _accessManager.GetSqlDataReader("sq_GetDbToPortionIssueList");
                List<DBIssuePortionMaster> aList = new List<DBIssuePortionMaster>();
                while (dr.Read())
                {
                    DBIssuePortionMaster aMaster = new DBIssuePortionMaster();

                    aMaster.DBIssueMasterID = Convert.ToInt32(dr["DBIssueMasterID"].ToString());
                    aMaster.IssueNo = dr["IssueNo"].ToString();
                    aMaster.RequisitionNo = dr["RequisitionNo"].ToString();
                    aMaster.CompanyName = dr["CompanyName"].ToString();
                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.IssueDate = Convert.ToDateTime(dr["IssueDate"]);
                    aMaster.ProductionDate = Convert.ToDateTime(dr["ProductionDate"]);
                    aMaster.Comments = dr["Comments"].ToString();
                    aMaster.IssueBy = dr["IssueBy"].ToString();
                    aList.Add(aMaster);

                }
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public List<ViewDBPortionIssueDetail> GetIssueDetailsByIssueId(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_IssueDetailsByIssueId", aParameters);
                List<ViewDBPortionIssueDetail> aList = new List<ViewDBPortionIssueDetail>();
                while (dr.Read())
                {
                    ViewDBPortionIssueDetail aDetail = new ViewDBPortionIssueDetail();
                    aDetail.ProductID = Convert.ToInt32(dr["ProductID"]);
                    aDetail.IssueQty = Convert.ToDecimal(dr["IssueQty"]);
                    aDetail.IssueQtyInKG = Convert.ToDecimal(dr["IssueQtyInKG"]);
                    aDetail.ProductName = dr["ProductName"].ToString();

                    aList.Add(aDetail);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse SaveStockTemp(PortionStockInMaster_Log aInMasterLog, string user)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aList = new List<SqlParameter>();
                aList.Add(new SqlParameter("@CompanyID", aInMasterLog.CompanyID));
                aList.Add(new SqlParameter("@LocationId", aInMasterLog.LocationId));
                aList.Add(new SqlParameter("@StockInDate", aInMasterLog.StockInDate));
                aList.Add(new SqlParameter("@WareHouseId", aInMasterLog.WareHouseId));
                aList.Add(new SqlParameter("@IssueId", aInMasterLog.IssueId));
                aList.Add(new SqlParameter("@StockInBy", aInMasterLog.StockInBy.Split(':')[0].Trim()));
                aList.Add(new SqlParameter("@Comments", aInMasterLog.Comments));
                aList.Add(new SqlParameter("@CreateBy", user));
                aList.Add(new SqlParameter("@CreateDate", DateTime.Now));

                ResultResponse aResponse = new ResultResponse();
                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_Save_PortionStockInTemp", aList);

                if (aResponse.pk > 0)
                {
                    aResponse.isSuccess = SaveStockTempDetails(aInMasterLog.ADetails, aResponse.pk);

                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                }
                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public bool SaveStockTempDetails(List<PortionStockInDetail_Log> aList, int pk)
        {
            bool result = false;


            foreach (var item in aList)
            {
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ProductID", item.ProductID));
                aParameters.Add(new SqlParameter("@PortionStockInMasterLogID", pk));
                aParameters.Add(new SqlParameter("@StockInQty", item.StockInQty));
                aParameters.Add(new SqlParameter("@StockInKG", item.StockInKG));
                aParameters.Add(new SqlParameter("@ExpireDate", item.ExpireDate));
                aParameters.Add(new SqlParameter("@Remark", item.Remark));
                aParameters.Add(new SqlParameter("@UnitPrice", item.UnitPrice));
                aParameters.Add(new SqlParameter("@PUnitPrice", item.PUnitPrice));

                result = _accessManager.SaveData("sp_Save_PortionStockInTemp_details", aParameters);
                if (result == false)
                {
                    break;


                }

            }
            return result;
        }


        public List<DBIssuePortionMaster> GetIssueForStockIn(int wh)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@wh", wh));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_PortionIssueForStockTemp", aParameters);
                List<DBIssuePortionMaster> aList = new List<DBIssuePortionMaster>();
                while (dr.Read())
                {
                    DBIssuePortionMaster aMaster = new DBIssuePortionMaster();

                    aMaster.DBIssueMasterID = Convert.ToInt32(dr["DBIssueMasterID"]);
                    aMaster.IssueNo = dr["IssueNo"].ToString();
                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public List<ViewPortionStockTemp> GetTempStockForApprove(DateTime aDateTime)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@aDate", aDateTime));
                DataTable dt = _accessManager.GetDataTable("sp_Get_PortinIssueForApprove", aParameters);
                List<ViewPortionStockTemp> aList = Helper.ToListof<ViewPortionStockTemp>(dt);
                return aList;
            }
            catch (Exception exception)
            {

                throw exception;
            }
        }

        public PortionStockInMaster_Log GetIssueLogMasterById(int id)
        {

            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_PortionIssueLogMasterById", aParameters);
                PortionStockInMaster_Log aInMasterLog = new PortionStockInMaster_Log();
                while (dr.Read())
                {
                    aInMasterLog.WareHouseId = Convert.ToInt32(dr["WareHouseId"]);
                    aInMasterLog.ToWarehouseId = Convert.ToInt32(dr["ToWarehouseId"]);
                    aInMasterLog.CompanyID = Convert.ToInt32(dr["CompanyID"]);
                    aInMasterLog.LocationId = Convert.ToInt32(dr["LocationId"]);
                }
                return aInMasterLog;
            }
            catch (Exception exception)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<ViewPortionIssusDetailsLog> GetDetailsByMasterId(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                List<ViewPortionIssusDetailsLog> aList = new List<ViewPortionIssusDetailsLog>();
                aParameters.Add(new SqlParameter("@id", id));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_PortionIssueLogDetailsById", aParameters);
                while (dr.Read())
                {
                    ViewPortionIssusDetailsLog aDetailsLog = new ViewPortionIssusDetailsLog();
                    aDetailsLog.ProductName = dr["ProductName"].ToString();
                    aDetailsLog.ProductID = Convert.ToInt32(dr["ProductID"]);
                    aDetailsLog.PortionStockInDetailLogID = Convert.ToInt32(dr["PortionStockInDetailLogID"]);
                    aDetailsLog.StockInKG = Convert.ToDecimal(dr["StockInKG"]);
                    aDetailsLog.StockInQty = Convert.ToDecimal(dr["StockInQty"]);
                    aDetailsLog.UnitPrice = Convert.ToDecimal(dr["UnitPrice"]);
                    aDetailsLog.ExpireDate = dr["ExpireDate"] as DateTime?;
                    aList.Add(aDetailsLog);
                }
                return aList;


            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public ResultResponse SaveApproveDBPortion(PortionStockInMaster_Log aInMasterLog)
        {


            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@WareHouseId", aInMasterLog.WareHouseId));
                aParameters.Add(new SqlParameter("@StockInBy", aInMasterLog.StockInBy.Split(':')[0].Trim()));
                aParameters.Add(new SqlParameter("@CompanyID", aInMasterLog.CompanyID));
                aParameters.Add(new SqlParameter("@LocationId", aInMasterLog.LocationId));
                aParameters.Add(new SqlParameter("@StockInDate", aInMasterLog.StockInDate));
                aParameters.Add(new SqlParameter("@ApproveDate", DateTime.Now));
                aParameters.Add(new SqlParameter("@PortionStockInMasterLogID", aInMasterLog.PortionStockInMasterLogID));
                aParameters.Add(new SqlParameter("@Comments", aInMasterLog.Comments));
                if (aInMasterLog.ADetails.Count > 0)
                {
                    aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_Approve_PortionStock", aParameters);
                    if (aResponse.pk > 0)
                    {
                        aResponse.isSuccess = SaveApproveDBPortionDetail(aInMasterLog.ADetails, aInMasterLog.StockInBy, aResponse.pk, aInMasterLog.StockInDate);
                    }
                }


                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public bool SaveApproveDBPortionDetail(List<PortionStockInDetail_Log> aList, string user, int pk, DateTime? StockInDate)
        {
            try
            {
                bool result = false;
                foreach (var item in aList)
                {
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@ApproveInQty", item.ApproveInQty));
                    aParameters.Add(new SqlParameter("@ApproveInKG", item.ApproveInKG));
                    aParameters.Add(new SqlParameter("@ExpireDate", item.ExpireDate));
                    aParameters.Add(new SqlParameter("@ProductID", item.ProductID));
                    aParameters.Add(new SqlParameter("@StockInDate", StockInDate));
                    aParameters.Add(new SqlParameter("@pk", pk));
                    aParameters.Add(new SqlParameter("@StockInBy", user));
                    aParameters.Add(new SqlParameter("@PortionStockInDetailLogID", item.PortionStockInDetailLogID));
                    result = _accessManager.SaveData("sp_Approve_PortionStock_detail", aParameters);
                    if (result == false)
                    {
                        break;
                    }


                }
                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public ViewDBPortionIssueDetail GetProductStock(int ProductId,int wareHouseId,  int groupId, DateTime stockDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@WareHouseId", wareHouseId));
                parameters.Add(new SqlParameter("@fromDate", stockDate));
                parameters.Add(new SqlParameter("@GroupId", groupId));
                parameters.Add(new SqlParameter("@ProductId", ProductId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_RptStockLedgerDateToDate", parameters);
                ViewDBPortionIssueDetail aDetail = new ViewDBPortionIssueDetail();
                while (dr.Read())
                {

                    aDetail.ProductID = Convert.ToInt32(dr["ProductID"]);
                    aDetail.IssueQty = Convert.ToDecimal(dr["CurrentStockQty"]);
                    aDetail.IssueQtyInKG = Convert.ToDecimal(dr["CurrentStockKG"]);
                    
                }
                return aDetail;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
    }
}