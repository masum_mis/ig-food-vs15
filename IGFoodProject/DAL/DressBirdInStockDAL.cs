﻿using IGFoodProject.DataManager;
using IGFoodProject.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using IGFoodProject.ViewModel;

namespace IGFoodProject.DAL
{
    public class DressBirdInStockDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();
        public List<ViewDressedBirdMaster> DressBirdInformation(int wareid)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@WarehouseId", wareid));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetDressedBird", parameters);
                List<ViewDressedBirdMaster> wareHouseList = new List<ViewDressedBirdMaster>();
                while (dr.Read())
                {
                    ViewDressedBirdMaster wareHouse = new ViewDressedBirdMaster();
                    wareHouse.DressedBirdMasterID = Convert.ToInt32(dr["DressedBirdMasterID"]);
                    wareHouse.DressedBirdNo = dr["DressedBirdNo"].ToString();
                    wareHouseList.Add(wareHouse);
                }
                return wareHouseList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

        public List<ViewDressedDirdDetail> GetDressedBirdDetailByID(int reqId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@DressedBirdMasterID", reqId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetDressedBirdDetailById", parameters);
                List<ViewDressedDirdDetail> aList = new List<ViewDressedDirdDetail>();
                while (dr.Read())
                {
                    ViewDressedDirdDetail supplier = new ViewDressedDirdDetail();
                    supplier.DressedBirdDetailIDs = dr["DressedBirdDetailID"].ToString();
                    supplier.DressedBirdMasterID = Convert.ToInt32(dr["DressedBirdMasterID"]);
                    supplier.ItemId = Convert.ToInt32(dr["ItemId"]);
                    if (dr["Remainingqty"] != DBNull.Value)
                    {
                        supplier.Remainingqty = Convert.ToDecimal(dr["Remainingqty"]);
                    }
                    if (dr["RemainingKG"] != DBNull.Value)
                    {
                        supplier.RemainingKG = Convert.ToDecimal(dr["RemainingKG"]);
                    }
                    supplier.UnitPrice = dr["RUnitPrice"] ==DBNull.Value?0: Convert.ToDecimal(dr["RUnitPrice"]);
                    supplier.Remark = dr["Remark"].ToString();
                    supplier.ItemName = dr["ItemName"].ToString();
                    supplier.ItemCategoryId = Convert.ToInt32(dr["ItemCategoryId"]);
                    aList.Add(supplier);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

        public List<ViewProductDescription> LoadProduct()
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                //List<SqlParameter> parameters = new List<SqlParameter>();
                //parameters.Add(new SqlParameter("@locationId", locationId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetProductName");
                List<ViewProductDescription> wareHouseList = new List<ViewProductDescription>();
                while (dr.Read())
                {
                    ViewProductDescription wareHouse = new ViewProductDescription();
                    wareHouse.ProductId = Convert.ToInt32(dr["ProductId"]);
                    wareHouse.ProductName = dr["ProductName"].ToString();
                    wareHouse.TypeId = Convert.ToInt32(dr["TypeId"]);
                    wareHouseList.Add(wareHouse);
                }
                return wareHouseList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public List<ViewDressedDirdDetail> LoadProduct_opening()
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
               
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetProductName");
                List<ViewDressedDirdDetail> wareHouseList = new List<ViewDressedDirdDetail>();
                while (dr.Read())
                {
                    ViewDressedDirdDetail wareHouse = new ViewDressedDirdDetail();
                    wareHouse.ItemId = Convert.ToInt32(dr["ProductId"]);
                    wareHouse.ItemName = dr["ProductName"].ToString();
                    wareHouseList.Add(wareHouse);
                }
                return wareHouseList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

        public bool SaveDressBird(tbl_StockInMaster aMaster)
        {
            bool result = false;
            try

            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                int id = SaveStockInMaster(aMaster);


                foreach (tbl_StockInDetail grade in aMaster.aDetail)
                {
                    List<SqlParameter> gSqlParameterList = new List<SqlParameter>();

                    gSqlParameterList.Add(new SqlParameter("@StockInDetailID", grade.StockInDetailID));
                    gSqlParameterList.Add(new SqlParameter("@StockInMasterID", id));
                    gSqlParameterList.Add(new SqlParameter("@ItemID", grade.ItemID));
                    gSqlParameterList.Add(new SqlParameter("@ProductID", grade.ProductID));
                    gSqlParameterList.Add(new SqlParameter("@StockInDate", grade.StockInDate));
                    gSqlParameterList.Add(new SqlParameter("@StockInQty", grade.StockInQty));
                    gSqlParameterList.Add(new SqlParameter("@StockInKG", grade.StockInKG));
                    gSqlParameterList.Add(new SqlParameter("@UnitPrice", grade.UnitPrice));
                    gSqlParameterList.Add(new SqlParameter("@ExpireDate", grade.ExpireDate));
                    gSqlParameterList.Add(new SqlParameter("@Remark", grade.Remark));
                    //gSqlParameterList.Add(new SqlParameter("@DressedBirdDetailID", grade.DressedBirdDetailID));
                    gSqlParameterList.Add(new SqlParameter("@DressBirdStockInDetailLogID", grade.DressedBirdDetailLogId)); 
                         gSqlParameterList.Add(new SqlParameter("@isopening", aMaster.IsOpening));
                    gSqlParameterList.Add(new SqlParameter("@PUnitPrice", grade.PUnitPrice));

                    result = _accessManager.SaveData("sp_SaveStockInDetail", gSqlParameterList);
                }

                // Accounting by Lipi
                result = SaveStockInMasterJV(id);
                if (result == false)
                {
                    _accessManager.SqlConnectionClose(true);
                }


            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
            return result;
        }
        public int SaveStockInMaster(tbl_StockInMaster aMaster)
        {
            try
            {
                string empReceived = string.Empty;
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@StockInMasterID", aMaster.StockInMasterID));
                //aSqlParameterList.Add(new SqlParameter("@DressdMasterID", aMaster.DressdMasterID));@
                aSqlParameterList.Add(new SqlParameter("@DressdMasterLogId", aMaster.DressdMasterLogId));
                aSqlParameterList.Add(new SqlParameter("@StockInDate", aMaster.StockInDate));
                aSqlParameterList.Add(new SqlParameter("@Comments", aMaster.Comments));
                aSqlParameterList.Add(new SqlParameter("@StockInBy", aMaster.StockInBy.Split(':')[0].Trim()));             
                aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@CreateDate", System.DateTime.Now));

                aSqlParameterList.Add(new SqlParameter("@WareHouseId", aMaster.WareHouseId));
                aSqlParameterList.Add(new SqlParameter("@CompanyID", aMaster.CompanyId));
                aSqlParameterList.Add(new SqlParameter("@LocationId", aMaster.LocationId));
                aSqlParameterList.Add(new SqlParameter("@IsOpening", aMaster.IsOpening)); 
               
                aSqlParameterList.Add(new SqlParameter("@GroupId", aMaster.GroupId));




                return _accessManager.SaveDataReturnPrimaryKey("sp_SaveStockInMaster", aSqlParameterList);
            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }
        }
        

        public bool SaveStockInMasterJV(int StockInMasterID)  
        {
            try
            {
                string empReceived = string.Empty;
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@StockInMasterID", StockInMasterID));

                return _accessManager.SaveData("DressBirdStockInJVEntry", aSqlParameterList);
            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }
        }

        public decimal GetSalablePriceByID(int productid, DateTime sdate)
        {
            decimal costingprice = 0;
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@productid", productid)); 
                    parameters.Add(new SqlParameter("@challanDate", sdate));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetMatricPrice", parameters);
               
                while (dr.Read())
                {
                   if(dr["CostingPrice"] !=DBNull.Value)
                    {
                        costingprice = Convert.ToDecimal(dr["CostingPrice"]);
                    }
                    else
                    {
                        costingprice = 0;
                    }
                }
                return costingprice;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }



        public bool SaveDressBird_Log(tbl_DressBirdStockInMaster_Log aMaster)
        {
            bool result = false;
            try

            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                int id = SaveStockInMaster_Log(aMaster);


                foreach (tbl_DressBirdStockInDetail_Log grade in aMaster.aDetail)
                {
                    List<SqlParameter> gSqlParameterList = new List<SqlParameter>();

                    gSqlParameterList.Add(new SqlParameter("@StockInDetailID", grade.DressBirdStockInDetailLogID));
                    gSqlParameterList.Add(new SqlParameter("@StockInMasterID", id));
                    gSqlParameterList.Add(new SqlParameter("@ItemID", grade.ItemID));
                    gSqlParameterList.Add(new SqlParameter("@ProductID", grade.ProductID));
                    gSqlParameterList.Add(new SqlParameter("@StockInDate", grade.StockInDate));
                    gSqlParameterList.Add(new SqlParameter("@StockInQty", grade.StockInQty));
                    gSqlParameterList.Add(new SqlParameter("@StockInKG", grade.StockInKG));
                    gSqlParameterList.Add(new SqlParameter("@UnitPrice", grade.UnitPrice));
                    gSqlParameterList.Add(new SqlParameter("@ExpireDate", grade.ExpireDate));
                    gSqlParameterList.Add(new SqlParameter("@Remark", grade.Remark));
                    gSqlParameterList.Add(new SqlParameter("@DressedBirdDetailIDs", grade.DressedBirdDetailIDs));
                    gSqlParameterList.Add(new SqlParameter("@DressdMasterID", aMaster.DressdMasterID));
                    gSqlParameterList.Add(new SqlParameter("@IsPortion", aMaster.IsPortion));
                    gSqlParameterList.Add(new SqlParameter("@PUnitPrice", grade.PUnitPrice));

                    result = _accessManager.SaveData("sp_SaveStockInDetail_Log", gSqlParameterList);
                }


            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
            return result;
        }
        public int SaveStockInMaster_Log(tbl_DressBirdStockInMaster_Log aMaster)
        {
            try
            {
                string empReceived = string.Empty;
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@StockInMasterID", aMaster.DressBirdStockInMasterLogID));
                aSqlParameterList.Add(new SqlParameter("@DressdMasterID", aMaster.DressdMasterID));
                aSqlParameterList.Add(new SqlParameter("@StockInDate", aMaster.StockInDate));
                aSqlParameterList.Add(new SqlParameter("@Comments", aMaster.Comments));
                aSqlParameterList.Add(new SqlParameter("@StockInBy", aMaster.StockInBy.Split(':')[0].Trim()));
                aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@CreateDate", System.DateTime.Now));

                aSqlParameterList.Add(new SqlParameter("@WareHouseId", aMaster.WareHouseId));
                aSqlParameterList.Add(new SqlParameter("@CompanyID", aMaster.CompanyId));
                aSqlParameterList.Add(new SqlParameter("@LocationId", aMaster.LocationId));
               
                aSqlParameterList.Add(new SqlParameter("@IsPortion", aMaster.IsPortion));
                aSqlParameterList.Add(new SqlParameter("@GroupId", aMaster.GroupId));




                return _accessManager.SaveDataReturnPrimaryKey("sp_SaveStockInMaster_Log", aSqlParameterList);
            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }
        }
        public List<DressBirdStockInListForApproval> GetDressBirdStockInListForApproval(string fromdate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<DressBirdStockInListForApproval> aList = new List<DressBirdStockInListForApproval>();
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@StockIndate", fromdate));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetDressBirdStockLog", aParameters);
                while (dr.Read())
                {
                    DressBirdStockInListForApproval aMaster = new DressBirdStockInListForApproval();

                    aMaster.DressBirdStockInMasterLogID = Convert.ToInt32(dr["DressBirdStockInMasterLogID"]);
                    aMaster.StockInDate = dr["StockInDate"].ToString();
                    aMaster.StockInBy = dr["StockInBy"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.StockInQty = Convert.ToDecimal(dr["StockInQty"]);

                    aMaster.StockInKG = Convert.ToDecimal(dr["StockInKG"]);

                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public ViewDressBirdMasterLog LoadLogMaster(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@DressBirdStockInMasterLogID", id));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetDressMasterStockLog", aParameters);
                ViewDressBirdMasterLog wareHouse = new ViewDressBirdMasterLog();
                while (dr.Read())
                {
                   
                    wareHouse.DressBirdStockInMasterLogID = Convert.ToInt32(dr["DressBirdStockInMasterLogID"]);
                    wareHouse.CompanyID = Convert.ToInt32(dr["CompanyID"]);
                    wareHouse.DressLocation = Convert.ToInt32(dr["DressLocation"]);
                    wareHouse.DressWarehouse = Convert.ToInt32(dr["ToWareHouseId"]);
                    wareHouse.LocationId = Convert.ToInt32(dr["LocationId"]);
                    wareHouse.WareHouseId = Convert.ToInt32(dr["WareHouseId"]);
                   

                }
                return wareHouse;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public List<tbl_DressBirdStockInDetail_Log> GetDressedBirdDetailLogByID(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@DressBirdStockInMasterLogID", id));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetDressedBirdDetailLogById", parameters);
                List<tbl_DressBirdStockInDetail_Log> aList = new List<tbl_DressBirdStockInDetail_Log>();
                while (dr.Read())
                {
                    tbl_DressBirdStockInDetail_Log supplier = new tbl_DressBirdStockInDetail_Log();
                    supplier.DressBirdStockInDetailLogID = Convert.ToInt32(dr["DressBirdStockInDetailLogID"]);
                    supplier.DressBirdStockInMasterLogID = Convert.ToInt32(dr["DressBirdStockInMasterLogID"]);
                    supplier.rowid = Convert.ToInt32(dr["rowid"]);
                    //supplier.DressedBirdDetailID= Convert.ToInt32(dr["DressedBirdDetailID"]);
                    supplier.ItemID = Convert.ToInt32(dr["ItemID"]);
                    supplier.ProductID = Convert.ToInt32(dr["ProductID"]);
                    supplier.ProductName = dr["ProductName"].ToString();
                    if (dr["StockInQty"] != DBNull.Value)
                    {
                        supplier.StockInQty = Convert.ToDecimal(dr["StockInQty"]);
                    }
                    if (dr["StockInKG"] != DBNull.Value)
                    {
                        supplier.StockInKG = Convert.ToDecimal(dr["StockInKG"]);
                    }
                    supplier.UnitPrice = dr["UnitPrice"] == DBNull.Value ? 0 : Convert.ToDecimal(dr["UnitPrice"]);
                    supplier.Remark = dr["Remark"].ToString();
                    supplier.ExpireDate = dr["ExpireDate"].ToString();
                    supplier.ItemCategoryId = Convert.ToInt32(dr["ItemCategoryId"]);
                    supplier.itemName= dr["ItemName"].ToString();
                    aList.Add(supplier);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
    }
}
