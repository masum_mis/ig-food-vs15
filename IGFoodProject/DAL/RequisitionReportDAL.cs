﻿using IGFoodProject.DataManager;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IGFoodProject.DAL
{
    public class RequisitionReportDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();
        public DataTable GetRequisitionReport(int company, int location, int warehouse, DateTime fromdate, DateTime todate, int rstatus)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@CompanyId", company));
                parameters.Add(new SqlParameter("@LocationId", location));
                parameters.Add(new SqlParameter("@WarehouseId", warehouse));
                parameters.Add(new SqlParameter("@FromDate", fromdate));
                parameters.Add(new SqlParameter("@ToDate", todate));
                parameters.Add(new SqlParameter("@Status", rstatus));
               
                DataTable dr = _accessManager.GetDataTable("sp_GetRequisitionReport", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public DataTable GetRequisitionInvoiceData(string ids)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();

                parameters.Add( new SqlParameter("@ids" , ids));
                DataTable dt = _accessManager.GetDataTable("sp_BirdRequisitionInvoice", parameters);
                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

    }
}