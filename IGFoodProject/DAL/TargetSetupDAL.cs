﻿using IGFoodProject.DataManager;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using IGFoodProject.Models;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;

namespace IGFoodProject.DAL
{
    public class TargetSetupDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();
        public DataSet GetTargetSetup(DateTime fromDate, DateTime toDate)
        {

            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@FromDate", fromDate));
                aParameters.Add(new SqlParameter("@ToDate", toDate));

                DataSet ds = new DataSet();

                ds = _accessManager.GetDataSet("sp_GetTargetMappingData", aParameters);

                return ds;

            }
            catch (Exception exception)
            {

                throw exception;
            }

            finally
            {

                _accessManager.SqlConnectionClose();
            }
        }
        public ResultResponse SaveTargetSetup(TargetSetupMaster aMaster, string user)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@TargetSetupMasterId", aMaster.TargetSetupMasterId));
                aParameters.Add(new SqlParameter("@FromDate", aMaster.FromDate));
                aParameters.Add(new SqlParameter("@ToDate", aMaster.ToDate));
                aParameters.Add(new SqlParameter("@Title", aMaster.Title));
                aParameters.Add(new SqlParameter("@Remarks", aMaster.Remarks));
                aParameters.Add(new SqlParameter("@IsEdit", aMaster.IsEdit));
                aParameters.Add(new SqlParameter("@CreateBy", user));

                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("SaveTatgetSetup", aParameters);

                if (aResponse.pk > 0)
                {
                    if (aMaster.TargetSetupDetails.Count > 0)
                    {
                        aResponse.isSuccess = SaveTargetSetupDetails(aMaster.TargetSetupDetails, aResponse.pk);
                    }

                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                }
                else
                {
                    _accessManager.SqlConnectionClose(true);
                }
                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
     
        public bool SaveTargetSetupDetails(List<TargetSetupDetails> aMaster, int TargetSetupMasterId)
        {
            try
            {
                bool result = false;

                foreach (var item in aMaster)
                {
                    List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                    ResultResponse aResponse = new ResultResponse();
                    aSqlParameterList.Add(new SqlParameter("@TargetSetupDetailsId", item.TargetSetupDetailsId));
                    aSqlParameterList.Add(new SqlParameter("@TargetSetupMasterId", TargetSetupMasterId));
                    aSqlParameterList.Add(new SqlParameter("@CategoryId", item.CategoryId));
                    aSqlParameterList.Add(new SqlParameter("@AreaId", item.AreaId));
                    aSqlParameterList.Add(new SqlParameter("@SalesOfficeId", item.SalesOfficeId));
                    aSqlParameterList.Add(new SqlParameter("@TargetKG", item.TargetKG));
                    aSqlParameterList.Add(new SqlParameter("@TargetAmount", item.TargetAmount));
                    aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("SaveTargetSetupDetails", aSqlParameterList);
                }
                return result = true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public int TargetSetupExist(DateTime? fromDate, DateTime? toDate)
        {
            var count = 0;
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@FromDate", fromDate));
                aParameters.Add(new SqlParameter("@ToDate", toDate));

                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_GetTargetMappingDataExist", aParameters);

                if (aResponse.pk > 0)
                {
                    count = aResponse.pk;
                }
                else
                {
                    _accessManager.SqlConnectionClose(true);
                }
                return count;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<TargetSetupMaster> TargetSetupList(DateTime? fromdate, DateTime? todate)
        {
            try
            {
                var now = DateTime.Now;
                var first = new DateTime(now.Year, now.Month, 1);
                if (fromdate == null)
                {
                   

                    fromdate = first;

                }
                if (todate == null)
                {
                    var last = first.AddMonths(1).AddDays(-1);
                    todate = last;

                }
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<TargetSetupMaster> aList = new List<TargetSetupMaster>();
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetTargetSetup", aParameters);
                while (dr.Read())
                {
                    TargetSetupMaster aMaster = new TargetSetupMaster();
                    aMaster.TargetSetupMasterId = Convert.ToInt32(dr["TargetSetupMasterId"]);
                    
                    aMaster.FromDate = Convert.ToDateTime(dr["FromDate"]); 
                    aMaster.ToDate =  Convert.ToDateTime(dr["ToDate"]);
                    aMaster.Title = dr["Title"].ToString();
                    aMaster.Remarks = dr["Remarks"].ToString();
                    aMaster.MonthName = dr["MonthName"].ToString();
                    aMaster.YearName = dr["YearName"].ToString();
                    aMaster.TargetSetupNo = dr["TargetSetupNo"].ToString();

                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable TargetVsAchievementReport(DateTime fromDate, DateTime toDate, int monthly, int groupId = 0, int categoryId = 0, int productId = 0)
        {
            DataTable dt = new DataTable();
            _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
            List<SqlParameter> aParameters = new List<SqlParameter>();
            aParameters.Add(new SqlParameter("@FormDate", fromDate));
            aParameters.Add(new SqlParameter("@ToDate", toDate));
            aParameters.Add(new SqlParameter("@CategoryId", categoryId));
            aParameters.Add(new SqlParameter("@GroupId", groupId));
            aParameters.Add(new SqlParameter("@ProductId", productId));
            if (monthly == 1)
            {
            }
            if (monthly == 2)
            {
            }
            if (monthly == 3)
            {
                dt = _accessManager.GetDataTable("sp_GetTargetVsAchivmentReport", aParameters, true);
            }

            return dt;
        }

        public DataSet GetTargetMappingDataById(int targetSetupId)
        {

            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@targetSetupId", targetSetupId));

                DataSet ds = new DataSet();

                ds = _accessManager.GetDataSet("sp_GetTargetMappingDataById", aParameters);

                return ds;

            }
            catch (Exception exception)
            {

                throw exception;
            }

            finally
            {

                _accessManager.SqlConnectionClose();
            }
        }

        public decimal CategorySalesRate(int categoryId)
        {
            decimal salseRate = 0;
            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CategoryId", categoryId));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetCategoryConversionRate", aParameters);
                while (dr.Read())
                {
                    salseRate = DBNull.Value.Equals(dr["SalesRate"])?0:Convert.ToDecimal(dr["SalesRate"]);
                }
                return salseRate;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<TargetSetupDetails> GetProductCategory(DateTime fromDate, DateTime toDate, int salesOfficerId, int areaId)
        {

            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@FromDate", fromDate));
                aParameters.Add(new SqlParameter("@ToDate", toDate));
                aParameters.Add(new SqlParameter("@salesOfficerId", salesOfficerId));
                aParameters.Add(new SqlParameter("@areaId", areaId));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetProductCategoryForTarget", aParameters);
                List<TargetSetupDetails> aDetailsList = new List<TargetSetupDetails>();
                while (dr.Read())
                {
                    TargetSetupDetails aDetailsView = new TargetSetupDetails();
                    aDetailsView.TargetSetupDetailsId = 0;
                    aDetailsView.TargetKG = 0;
                    aDetailsView.TargetAmount = 0;
                    aDetailsView.RestTargetKG = DBNull.Value.Equals(dr["RestTargetKG"])?0: Convert.ToDecimal(dr["RestTargetKG"]);
                    aDetailsView.RestTargetAmount = DBNull.Value.Equals(dr["RestTargetAmount"]) ? 0 : Convert.ToDecimal(dr["RestTargetAmount"]);
                    aDetailsView.CategoryId = DBNull.Value.Equals(dr["CategoryId"]) ? 0 : Convert.ToInt32(dr["CategoryId"]);
                    aDetailsView.CategoryName = dr["CategoryName"].ToString();

                    aDetailsList.Add(aDetailsView);


                }
                return aDetailsList;

            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<Area> GetArea()
        {

            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

  
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@divisionId", 0));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_AreaInfo", aParameters);
                List<Area> aDetailsList = new List<Area>();
                while (dr.Read())
                {
                    Area aDetailsView = new Area();
                    aDetailsView.AreaId = (int)dr["AreaId"];
                    aDetailsView.AreaName = dr["AreaName"].ToString();

                    aDetailsList.Add(aDetailsView);


                }
                return aDetailsList;

            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<View_EmployeeInfo> GetSalesOfficerByArea(int areaId)
        {

            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);


                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@AreaId", areaId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetSalesOfficerByArea", aParameters);
                List<View_EmployeeInfo> aDetailsList = new List<View_EmployeeInfo>();
                while (dr.Read())
                {
                    View_EmployeeInfo aDetailsView = new View_EmployeeInfo();
                    aDetailsView.SalesOfficerId = (int)dr["SalesOfficerId"];
                    aDetailsView.EmpName = dr["EmpName"].ToString();
                    aDetailsView.Designation = dr["Designation"].ToString();

                    aDetailsList.Add(aDetailsView);


                }
                return aDetailsList;

            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public TargetSetupMaster GetTargetSetupMasterById(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                TargetSetupMaster aMaster = new TargetSetupMaster();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@TargetSetupMasterId", id));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetCustomerTargetSetupById", aParameters);
                while (dr.Read())
                {
                    aMaster.TargetSetupMasterId = Convert.ToInt32(dr["TargetSetupMasterId"]);
                    aMaster.FromDate = Convert.ToDateTime(dr["FromDate"]);
                    aMaster.ToDate = Convert.ToDateTime(dr["ToDate"]);
                    aMaster.YearId = Convert.ToInt32(dr["YearName"]);
                    aMaster.MonthId = Convert.ToInt32(dr["MonthId"]);
                    aMaster.Title = dr["Title"].ToString();
                    aMaster.CustomerTargetSetupNo = dr["TargetSetupNo"].ToString();
                    aMaster.Remarks = dr["Remarks"].ToString();
                }
                return aMaster;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<TargetSetupDetails> TargetSetupDetailsById(int Id)
        {
            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<TargetSetupDetails> aList = new List<TargetSetupDetails>();
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@TargetSetupMasterId", Id));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetCustomerTargetSetupDetailsById", aParameters);
                while (dr.Read())
                {
                    TargetSetupDetails aMaster = new TargetSetupDetails();
                    aMaster.TargetSetupDetailsId = DBNull.Value.Equals(dr["TargetSetupDetailsId"]) ? 0 : Convert.ToInt32(dr["TargetSetupDetailsId"]);
                    aMaster.TargetSetupMasterId = Convert.ToInt32(dr["TargetSetupMasterId"]); 
                    aMaster.CustomerId = DBNull.Value.Equals(dr["CustomerId"]) ? 0 : Convert.ToInt32(dr["CustomerId"]);
                    aMaster.CustomerTypeId = DBNull.Value.Equals(dr["CustomerType"]) ? 0 : Convert.ToInt32(dr["CustomerType"]);
                    aMaster.AreaId = DBNull.Value.Equals(dr["AreaId"]) ? 0 : Convert.ToInt32(dr["AreaId"]);
                    aMaster.SalesOfficeId = DBNull.Value.Equals(dr["SalesOfficeId"]) ? 0 : Convert.ToInt32(dr["SalesOfficeId"]);
                    aMaster.CategoryId = DBNull.Value.Equals(dr["CategoryId"]) ? 0 : Convert.ToInt32(dr["CategoryId"]);
                    aMaster.CategoryName = dr["CategoryName"].ToString();
                    aMaster.TargetKG = DBNull.Value.Equals(dr["TargetKG"]) ? 0 : Convert.ToDecimal(dr["TargetKG"]);
                    aMaster.TargetAmount = DBNull.Value.Equals(dr["TargetAmount"]) ? 0 : Convert.ToDecimal(dr["TargetAmount"]);
                    aMaster.RestTargetKG= DBNull.Value.Equals(dr["RestTargetKG"]) ? 0 : Convert.ToDecimal(dr["RestTargetKG"]);
                    aMaster.ActualTargetKG = DBNull.Value.Equals(dr["ActualTargetKG"]) ? 0 : Convert.ToDecimal(dr["ActualTargetKG"]);
                    aMaster.RestTargetAmount = DBNull.Value.Equals(dr["RestTargetAmount"]) ? 0 : Convert.ToDecimal(dr["RestTargetAmount"]);
                    aMaster.ActualTargetAmount = DBNull.Value.Equals(dr["ActualTargetAmount"]) ? 0 : Convert.ToDecimal(dr["ActualTargetAmount"]);
                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse SaveCustomerTargetSetup(TargetSetupMaster aMaster, string user)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@TargetSetupMasterId", aMaster.TargetSetupMasterId));
                //aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", aMaster.FromDate)));
                //aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", aMaster.ToDate)));
                aParameters.Add(new SqlParameter("@FromDate", aMaster.FromDate));
                aParameters.Add(new SqlParameter("@ToDate", aMaster.ToDate));
                aParameters.Add(new SqlParameter("@Title", aMaster.Title));
                aParameters.Add(new SqlParameter("@Remarks", aMaster.Remarks));
                aParameters.Add(new SqlParameter("@IsEdit", aMaster.IsEdit));
                aParameters.Add(new SqlParameter("@CreateBy", user.Trim()));

                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("SaveCustomerTatgetSetup", aParameters);

                if (aResponse.pk > 0)
                {
                    if (aMaster.TargetSetupDetails.Count > 0)
                    {
                        aResponse.isSuccess = SaveCustomerTargetSetupDetails(aMaster.TargetSetupDetails, aResponse.pk);
                    }

                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                }
                else
                {
                    _accessManager.SqlConnectionClose(true);
                }
                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public bool SaveCustomerTargetSetupDetails(List<TargetSetupDetails> aMaster, int TargetSetupMasterId)
        {
            try
            {
                bool result = false;

                foreach (var item in aMaster)
                {
                    List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                    ResultResponse aResponse = new ResultResponse();
                    aSqlParameterList.Add(new SqlParameter("@TargetSetupDetailsId", item.TargetSetupDetailsId));
                    aSqlParameterList.Add(new SqlParameter("@TargetSetupMasterId", TargetSetupMasterId));
                    aSqlParameterList.Add(new SqlParameter("@CategoryId", item.CategoryId));
                    aSqlParameterList.Add(new SqlParameter("@AreaId", item.AreaId));
                    aSqlParameterList.Add(new SqlParameter("@SalesOfficeId", item.SalesOfficeId));
                    aSqlParameterList.Add(new SqlParameter("@CustomerId", item.CustomerId));
                    aSqlParameterList.Add(new SqlParameter("@TargetKG", item.TargetKG));
                    aSqlParameterList.Add(new SqlParameter("@TargetAmount", item.TargetAmount));
                    aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("SaveCustomerTargetSetupDetails", aSqlParameterList);
                }
                return result = true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public decimal ExistingTargetKg(DateTime fromDate, DateTime toDate, int categoryId, int salesOfficerId, int areaId)
        {
            decimal targetKg = 0;
            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@fromDate", fromDate));
                aParameters.Add(new SqlParameter("@toDate", toDate));
                aParameters.Add(new SqlParameter("@CategoryId", categoryId));
                aParameters.Add(new SqlParameter("@SalesOfficerId", salesOfficerId));
                aParameters.Add(new SqlParameter("@AreaId", areaId));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetExistingTargetKg", aParameters);
                while (dr.Read())
                {
                    targetKg = DBNull.Value.Equals(dr["TargetKG"]) ? 0 : Convert.ToDecimal(dr["TargetKG"]);
                }
                return targetKg;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public decimal ExistingTargetKgSearch(int YearId, int MonthId, int categoryId, int salesOfficerId, int areaId)
        {
            decimal targetKg = 0;
            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@YearId", YearId));
                aParameters.Add(new SqlParameter("@MonthId", MonthId));
                aParameters.Add(new SqlParameter("@CategoryId", categoryId));
                aParameters.Add(new SqlParameter("@SalesOfficerId", salesOfficerId));
                aParameters.Add(new SqlParameter("@AreaId", areaId));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetExistingTargetKgSearch", aParameters);
                while (dr.Read())
                {
                    targetKg = DBNull.Value.Equals(dr["TargetKG"]) ? 0 : Convert.ToDecimal(dr["TargetKG"]);
                }
                return targetKg;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<TargetSetupMaster> CustomerTargetSetupList(DateTime? fromdate, DateTime? todate)
        {
            try
            {
                var now = DateTime.Now;
                var first = new DateTime(now.Year, now.Month, 1);
                if (fromdate == null)
                {


                    fromdate = first;

                }
                if (todate == null)
                {
                    var last = first.AddMonths(1).AddDays(-1);
                    todate = last;

                }
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<TargetSetupMaster> aList = new List<TargetSetupMaster>();
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetCustomerTargetSetup", aParameters);
                while (dr.Read())
                {
                    TargetSetupMaster aMaster = new TargetSetupMaster();
                    aMaster.TargetSetupMasterId = Convert.ToInt32(dr["TargetSetupMasterId"]);
                    aMaster.FromDate = Convert.ToDateTime(dr["FromDate"]);
                    aMaster.ToDate = Convert.ToDateTime(dr["ToDate"]);
                    aMaster.Title = dr["Title"].ToString();
                    aMaster.Remarks = dr["Remarks"].ToString();
                    aMaster.MonthName = dr["MonthName"].ToString();
                    aMaster.YearName = dr["YearName"].ToString(); 
                    aMaster.CustomerTargetSetupNo = dr["TargetSetupNo"].ToString();
                    aMaster.CustomerName = dr["CustomerName"].ToString();

                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public int CustomerTargetSetupExist(DateTime? fromDate, DateTime? toDate,int customerId)
        {
            var count = 0;
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@FromDate", fromDate));
                aParameters.Add(new SqlParameter("@ToDate", toDate));
                aParameters.Add(new SqlParameter("@CustomerId", customerId));

                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_GetCustomerTargetMappingDataExist", aParameters);

                if (aResponse.pk > 0)
                {
                    count = aResponse.pk;
                }
                else
                {
                    _accessManager.SqlConnectionClose(true);
                }
                return count;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public TargetSetupMaster CustomerTargetSetupExistAndNextDate(DateTime? fromDate, DateTime? toDate, int customerId)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@FromDate", fromDate));
                aParameters.Add(new SqlParameter("@ToDate", toDate));
                aParameters.Add(new SqlParameter("@CustomerId", customerId));
                TargetSetupMaster tMaster = new TargetSetupMaster();
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetCustomerTargetMappingDataExist", aParameters);
                while (dr.Read())
                {
                    tMaster.RowCountNo = DBNull.Value.Equals(dr["RowCountNo"]) ? 0 : Convert.ToInt32(dr["RowCountNo"]);
                    tMaster.NextDate = (dr["NextDate"]) as DateTime? ;
                }
                return tMaster;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public TargetSetupMaster CustomerTargetSetupExistForEdit(DateTime? fromDate, DateTime? toDate, int customerId,int customerTargetSetupId)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@FromDate", fromDate));
                aParameters.Add(new SqlParameter("@ToDate", toDate));
                aParameters.Add(new SqlParameter("@CustomerId", customerId));
                aParameters.Add(new SqlParameter("@CustomerTargetSetupId", customerTargetSetupId));
                TargetSetupMaster tMaster = new TargetSetupMaster();
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetCustomerTargetMappingDataExistForEdit", aParameters);
                while (dr.Read())
                {
                    tMaster.RowCountNo = DBNull.Value.Equals(dr["RowCountNo"]) ? 0 : Convert.ToInt32(dr["RowCountNo"]);
                }
                return tMaster;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable CustomerTargetVsAchievementReport(DateTime fromDate, DateTime toDate, int monthly,int CustomerTypeId=0, int CustomerId = 0, int AreaId = 0, int SalesOfficerId = 0)
        {
            DataTable dt = new DataTable();
            _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
            List<SqlParameter> aParameters = new List<SqlParameter>();
            aParameters.Add(new SqlParameter("@fromDate", fromDate));
            aParameters.Add(new SqlParameter("@toDate", toDate));
            aParameters.Add(new SqlParameter("@CustomerTypeId", CustomerTypeId));
            aParameters.Add(new SqlParameter("@CustomerId", CustomerId));
            aParameters.Add(new SqlParameter("@AreaId", AreaId));
            aParameters.Add(new SqlParameter("@SalesOfficerId", SalesOfficerId));
            if (monthly == 1)
            {
            }
            if (monthly == 2)
            {
            }
            if (monthly == 3)
            {
                dt = _accessManager.GetDataTable("sp_GetTargetVsAchievementDetails", aParameters, true);
            }

            return dt;
        }

        public List<CustomerType> GetCustomerType()
        {

            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_CustomerTypeInfo");
                List<CustomerType> aDetailsList = new List<CustomerType>();
                while (dr.Read())
                {
                    CustomerType aDetailsView = new CustomerType();
                    aDetailsView.CustomerTypeID = (int)dr["CustomerTypeID"];
                    aDetailsView.CoustomerTypeName = dr["CoustomerTypeName"].ToString();

                    aDetailsList.Add(aDetailsView);


                }
                return aDetailsList;

            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataSet GetCustomerTypeTargetSetup(DateTime fromDate, DateTime toDate)
        {

            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@FromDate", fromDate));
                aParameters.Add(new SqlParameter("@ToDate", toDate));

                DataSet ds = new DataSet();

                ds = _accessManager.GetDataSet("sp_GetCustomerTypeTargetMappingData", aParameters);

                return ds;

            }
            catch (Exception exception)
            {

                throw exception;
            }

            finally
            {

                _accessManager.SqlConnectionClose();
            }
        }

        public int CustomerTypeTargetSetupExist(DateTime? fromDate, DateTime? toDate, int areaId = 0, int salesOfficerId = 0)
        {
            var count = 0;
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@FromDate", fromDate));
                aParameters.Add(new SqlParameter("@ToDate", toDate));
                aParameters.Add(new SqlParameter("@AreaId", areaId));
                aParameters.Add(new SqlParameter("@SalesOfficerId", salesOfficerId));

                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_GetCustomerTypeTargetMappingDataExist", aParameters);

                if (aResponse.pk > 0)
                {
                    count = aResponse.pk;
                }
                else
                {
                    _accessManager.SqlConnectionClose(true);
                }
                return count;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse SaveCustomerTypeTargetSetup(CustomerTypeTargetSetupMaster aMaster, string user)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CustomerTypeTargetSetupMasterId", aMaster.CustomerTypeTargetSetupMasterId));
                aParameters.Add(new SqlParameter("@FromDate", aMaster.FromDate));
                aParameters.Add(new SqlParameter("@ToDate", aMaster.ToDate));
                aParameters.Add(new SqlParameter("@Title", aMaster.Title));
                aParameters.Add(new SqlParameter("@Remarks", aMaster.Remarks));
                aParameters.Add(new SqlParameter("@IsEdit", aMaster.IsEdit));
                aParameters.Add(new SqlParameter("@CreateBy", user));

                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("SaveCustomerTypeTatgetSetup", aParameters);

                if (aResponse.pk > 0)
                {
                    if (aMaster.CustomerTypeTargetSetupDetails.Count > 0)
                    {
                        aResponse.isSuccess = SaveCustomerTypeTargetSetupDetails(aMaster.CustomerTypeTargetSetupDetails, aResponse.pk);
                    }

                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                }
                else
                {
                    _accessManager.SqlConnectionClose(true);
                }
                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public bool SaveCustomerTypeTargetSetupDetails(List<CustomerTypeTargetSetupDetails> aMaster, int CustomerTypeTargetSetupMasterId)
        {
            try
            {
                bool result = false;

                foreach (var item in aMaster)
                {
                    List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                    ResultResponse aResponse = new ResultResponse();
                    aSqlParameterList.Add(new SqlParameter("@CustomerTypeTargetSetupDetailsId", item.CustomerTypeTargetSetupDetailsId));
                    aSqlParameterList.Add(new SqlParameter("@CustomerTypeTargetSetupMasterId", CustomerTypeTargetSetupMasterId));
                    aSqlParameterList.Add(new SqlParameter("@CustomerTypeId", item.CustomerTypeId));
                    aSqlParameterList.Add(new SqlParameter("@TypeId", item.TypeId));
                    aSqlParameterList.Add(new SqlParameter("@AreaId", item.AreaId));
                    aSqlParameterList.Add(new SqlParameter("@SalesOfficeId", item.SalesOfficeId));
                    aSqlParameterList.Add(new SqlParameter("@TargetKG", item.TargetKG));
                    aSqlParameterList.Add(new SqlParameter("@TargetAmount", item.TargetAmount));
                    aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("SaveCustomerTypeTargetSetupDetails", aSqlParameterList);
                }
                return result = true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public List<CustomerTypeTargetSetupMaster> CustomerTypeTargetSetupList(DateTime? fromdate, DateTime? todate, int areaId = 0, int salesOfficerId = 0)
        {
            try
            {
                var now = DateTime.Now;
                var first = new DateTime(now.Year, now.Month, 1);
                if (fromdate == null)
                {


                    fromdate = first;

                }
                if (todate == null)
                {
                    var last = first.AddMonths(1).AddDays(-1);
                    todate = last;

                }
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<CustomerTypeTargetSetupMaster> aList = new List<CustomerTypeTargetSetupMaster>();
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                aParameters.Add(new SqlParameter("@AreaId", areaId));
                aParameters.Add(new SqlParameter("@SalesOfficerId", salesOfficerId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetCustomerTypeTargetSetup", aParameters);
                while (dr.Read())
                {
                    CustomerTypeTargetSetupMaster aMaster = new CustomerTypeTargetSetupMaster();
                    aMaster.CustomerTypeTargetSetupMasterId = Convert.ToInt32(dr["CustomerTypeTargetSetupMasterId"]);
                    aMaster.FromDate = Convert.ToDateTime(dr["FromDate"]);
                    aMaster.ToDate = Convert.ToDateTime(dr["ToDate"]);
                    aMaster.Title = dr["Title"].ToString();
                    aMaster.Remarks = dr["Remarks"].ToString();
                    aMaster.MonthName = dr["MonthName"].ToString();
                    aMaster.YearName = dr["YearName"].ToString();
                    aMaster.TargetSetupNo = dr["TargetSetupNo"].ToString();
                    aMaster.CreateBy = dr["EmpName"].ToString();
                    aMaster.AreaName = dr["AreaName"].ToString();
                    aMaster.SalesOfficer = dr["SalesOfficer"].ToString();

                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataSet GetCustomerTypeTargetMappingDataById(int customerTypeTargetSetupId)
        {

            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@customerTypeTargetSetupId", customerTypeTargetSetupId));

                DataSet ds = new DataSet();

                ds = _accessManager.GetDataSet("sp_GetCustomerTypeTargetMappingDataById", aParameters);

                return ds;

            }
            catch (Exception exception)
            {

                throw exception;
            }

            finally
            {

                _accessManager.SqlConnectionClose();
            }
        }

        public TargetSetupDetails GetAvailableTarget(DateTime FromDate,DateTime ToDate,int CategoryId,int AreaId,int SalesOfficerId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                TargetSetupDetails aMaster = new TargetSetupDetails();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@FromDate", FromDate));
                aParameters.Add(new SqlParameter("@ToDate", ToDate));
                aParameters.Add(new SqlParameter("@CategoryId", CategoryId));
                aParameters.Add(new SqlParameter("@AreaId", AreaId));
                aParameters.Add(new SqlParameter("@SalesOfficerId", SalesOfficerId));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetAvailableTarget", aParameters);
                while (dr.Read())
                {
                    aMaster.TypeId = Convert.ToInt32(dr["TypeId"]);
                    aMaster.AreaId = Convert.ToInt32(dr["AreaId"]);
                    aMaster.AreaName = dr["AreaName"].ToString();
                    aMaster.SalesOfficeName = dr["SalesOfficeName"].ToString();
                    aMaster.SalesOfficeId = Convert.ToInt32(dr["SalesOfficeId"]);
                    aMaster.OverallTargetKG = DBNull.Value.Equals(dr["OverallTargetKG"])?0: Convert.ToDecimal(dr["OverallTargetKG"]);
                    aMaster.AvailableTargetKG = DBNull.Value.Equals(dr["AvailableTargetKG"]) ? 0 : Convert.ToDecimal(dr["AvailableTargetKG"]);
                    aMaster.OverallTargetAmount = DBNull.Value.Equals(dr["OverallTargetAmount"]) ? 0 : Convert.ToDecimal(dr["OverallTargetAmount"]);
                    aMaster.AvailableTargetAmount = DBNull.Value.Equals(dr["AvailableTargetAmount"]) ? 0 : Convert.ToDecimal(dr["AvailableTargetAmount"]);
                }
                return aMaster;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<ViewCustomerInfo> GetCustomerList(int typeid,int salesOfficerId)
        {

            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ViewCustomerInfo> _List = new List<ViewCustomerInfo>();
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@customerTypeid", typeid));
                aSqlParameterList.Add(new SqlParameter("@salesOfficerId", salesOfficerId));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_SalesOfficerCustomer", aSqlParameterList);
                while (dr.Read())
                {
                    ViewCustomerInfo aInfo = new ViewCustomerInfo();
                    aInfo.CustomerID = Convert.ToInt32(dr["CustomerID"]);
                    aInfo.CustomerName = dr["CustomerName"].ToString();
                    aInfo.CustomerCode = dr["CustomerCode"].ToString();
                    aInfo.CoustomerTypeName = dr["CoustomerTypeName"].ToString();
                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
    }
}