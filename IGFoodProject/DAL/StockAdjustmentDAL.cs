﻿using IGFoodProject.DataManager;
using IGFoodProject.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IGFoodProject.DAL
{
    public class StockAdjustmentDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();
        public DataTable GetProductStockDetailsbyStockInDate(int productId)
        {

            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@productId", productId));
               
                DataTable dt = _accessManager.GetDataTable("sp_GetProductStockDetailsbyStockInDate", parameters);
                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public bool SaveDirectStockOut(StockAdjustmentMaster stockAdjustment, string userId)
        {
            try
            {


                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@WarehouseId", stockAdjustment.WarehouseId));
                aParameters.Add(new SqlParameter("@ProductId", stockAdjustment.ProductId));
                aParameters.Add(new SqlParameter("@AdjustmentDate", stockAdjustment.AdjustmentDate));
                aParameters.Add(new SqlParameter("@ReasonId", stockAdjustment.ReasonId));
                aParameters.Add(new SqlParameter("@Reason", stockAdjustment.Reason));
                aParameters.Add(new SqlParameter("@UserId", userId));

                var masterId = _accessManager.SaveDataReturnPrimaryKey("sp_SaveStockAdjustment_Master", aParameters);
                bool result = true;
                foreach (StockAdjustmentDetails item in stockAdjustment.AdjustmentDetails)
                {
                    List<SqlParameter> aList = new List<SqlParameter>();

                    aList.Add(new SqlParameter("@MasterId", masterId));
                    aList.Add(new SqlParameter("@RefStockInDetailsId", item.RefStockInDetailsId));
                    aList.Add(new SqlParameter("@ProductId", stockAdjustment.ProductId));
                    aList.Add(new SqlParameter("@AdjustmentDate", stockAdjustment.AdjustmentDate));
                    aList.Add(new SqlParameter("@StockOutQty", item.StockOutQty));
                    aList.Add(new SqlParameter("@StockOutKg", item.StockOutKg));

                    result = _accessManager.SaveData("sp_SaveStockAdjustment_Details", aList);
                }
                return result;
            }
            catch (Exception e)
            {
                _accessManager.SqlConnectionClose(true);
                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetProductStockDetailsbyStockInDate_LiveBird(int itemId)
        {

            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@itemId", itemId));

                DataTable dt = _accessManager.GetDataTable("sp_GetItemStockDetailsbyStockInDate", parameters);
                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public bool SaveDirectStockOut_LiveBird(StockAdjustmentMaster stockAdjustment, string userId)
        {
            try
            {


                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@WarehouseId", stockAdjustment.WarehouseId));
                aParameters.Add(new SqlParameter("@ProductId", stockAdjustment.ProductId));
                aParameters.Add(new SqlParameter("@AdjustmentDate", stockAdjustment.AdjustmentDate));
                aParameters.Add(new SqlParameter("@Reason", stockAdjustment.Reason));
                aParameters.Add(new SqlParameter("@UserId", userId));

                var masterId = _accessManager.SaveDataReturnPrimaryKey("sp_SaveStockAdjustment_Master_LiveBird", aParameters);
                bool result = true;
                foreach (StockAdjustmentDetails item in stockAdjustment.AdjustmentDetails)
                {
                    List<SqlParameter> aList = new List<SqlParameter>();

                    aList.Add(new SqlParameter("@MasterId", masterId));
                    aList.Add(new SqlParameter("@RefStockInDetailsId", item.RefStockInDetailsId));
                    aList.Add(new SqlParameter("@ProductId", stockAdjustment.ProductId));
                    aList.Add(new SqlParameter("@AdjustmentDate", stockAdjustment.AdjustmentDate));
                    aList.Add(new SqlParameter("@StockOutQty", item.StockOutQty));
                    aList.Add(new SqlParameter("@StockOutKg", item.StockOutKg));
                    aList.Add(new SqlParameter("@Createby", userId));
                    aList.Add(new SqlParameter("@warehouseid", stockAdjustment.WarehouseId));

                    result = _accessManager.SaveData("sp_SaveStockAdjustment_Details_LiveBird", aList);
                }
                return result;
            }
            catch (Exception e)
            {
                _accessManager.SqlConnectionClose(true);
                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
    }
}