﻿using IGFoodProject.DataManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IGFoodProject.Models;
using System.Data.SqlClient;

namespace IGFoodProject.DAL
{
    public class UOMDAL
    {
        DataAccessManager accessManager = new DataAccessManager();

        public List<tbl_UOM> GetUOMList()
        { 

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<tbl_UOM> _List = new List<tbl_UOM>();

                //List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetUOM");
                while (dr.Read())
                {
                    tbl_UOM aInfo = new tbl_UOM();
                    aInfo.UOMId = (int)dr["UOMId"];
                    aInfo.UOMName = dr["UOMName"].ToString();
                    if (dr["CreateDate"] != DBNull.Value)
                    {
                        aInfo.CreateDate = Convert.ToDateTime(dr["CreateDate"]);
                    }
                    aInfo.CreateBy = dr["CreateBy"].ToString();
                    
                    aInfo.UpdateBy = dr["UpdateBy"].ToString();
                    if(dr["UpdateDate"] !=DBNull.Value)
                    {
                        aInfo.UpdateDate = Convert.ToDateTime(dr["UpdateDate"]);
                    }
                    if (dr["IsActive"] != DBNull.Value)
                    {
                        aInfo.IsActive = Convert.ToBoolean(dr["IsActive"]);
                    }

                  
                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public int CheckUOM(string uom)
        {
            int c_uom = 0;
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@uom", uom));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_CheckUOM", aSqlParameterList);

                while (dr.Read())
                {
                    c_uom = Convert.ToInt32(dr["uom"]);
                }
            }
            catch (Exception exception)
            {
                //accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
            return c_uom;
        }
        public bool SaveUOM(tbl_UOM aMaster)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@UOMId", aMaster.UOMId));
                aSqlParameterList.Add(new SqlParameter("@UOMName", aMaster.UOMName));
                aSqlParameterList.Add(new SqlParameter("@CreateDate", System.DateTime.Now));
                aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@UpdateDate", System.DateTime.Now));
                aSqlParameterList.Add(new SqlParameter("@UpdateBy", aMaster.UpdateBy));
                aSqlParameterList.Add(new SqlParameter("@IsActive", aMaster.IsActive));
               


                return accessManager.SaveData("sp_SaveUOM", aSqlParameterList);
            }
            catch (Exception exception)
            {
                accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public tbl_UOM GetUOMForEdit(int id)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@UOMId", id));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetUOMByID", aParameters);
                tbl_UOM aDetailsView = new tbl_UOM();
                while (dr.Read())
                {


                    aDetailsView.UOMId = (int)dr["UOMId"];
                    aDetailsView.UOMName =dr["UOMName"].ToString();
                    aDetailsView.IsActive =(bool) dr["IsActive"];


                }
                return aDetailsView;

            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public bool DeleteUOM(int id)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@UomID", id));

                return accessManager.SaveData("sp_DeleteUOM", aSqlParameterList);
            }
            catch (Exception exception)
            {
                
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
    }
}
