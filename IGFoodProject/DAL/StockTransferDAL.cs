﻿using IGFoodProject.DataManager;
using IGFoodProject.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using IGFoodProject.ViewModel;
using System.Data;

namespace IGFoodProject.DAL
{
    public class StockTransferDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();

        public ResultResponse SaveStockTransferRequisitionMaster(StockTransferReqMaster aMaster)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                ResultResponse aResponse = new ResultResponse();

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@reqMasterId", aMaster.StockTransferReqMasterId));
                aSqlParameterList.Add(new SqlParameter("@CompanyId", aMaster.CompanyId));
                aSqlParameterList.Add(new SqlParameter("@FromWarehouseId", aMaster.FromWarehouseId));
                aSqlParameterList.Add(new SqlParameter("@ToWarehouseId", aMaster.ToWarehouseId));
                aSqlParameterList.Add(new SqlParameter("@FromLocationId", aMaster.FromLocationId));
                aSqlParameterList.Add(new SqlParameter("@ToLocationId", aMaster.ToLocationId));
                aSqlParameterList.Add(new SqlParameter("@RequisitionBy", aMaster.RequisitionBy));
                aSqlParameterList.Add(new SqlParameter("@RequisitionDate", aMaster.RequisitionDate));
                aSqlParameterList.Add(new SqlParameter("@ExpectedDeliveryDate", aMaster.ExpectedDeliveryDate));
                aSqlParameterList.Add(new SqlParameter("@EmpId", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@Remarks", aMaster.Remarks));


                aSqlParameterList.Add(new SqlParameter("@VehicleId", aMaster.VehicleId));
                aSqlParameterList.Add(new SqlParameter("@DriverName", aMaster.DriverName));
                aSqlParameterList.Add(new SqlParameter("@DriverMobile", aMaster.DriverMobile)); 
                aSqlParameterList.Add(new SqlParameter("@Groupid", aMaster.GroupId));

                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_SaveOrUpdateStockTransferReqMaster", aSqlParameterList);
                if (aResponse.pk > 0)
                {
                    aResponse.isSuccess = SaveStockTransferReqDetails(aMaster.StockTransferReqDetails, aResponse.pk);

                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                    //else
                    //{
                    //    aResponse.isSuccess = StockTransferLogEntry(aMaster);

                    //}

                }
                return aResponse;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public bool SaveStockTransferReqDetails(List<StockTransferReqDetails> aList, int pk)
        {
            try
            {
                bool result = false;
                foreach (var item in aList)
                {
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@reqDetailsId", item.StockTransferReqDetailsId));
                    aParameters.Add(new SqlParameter("@reqMasterId", pk));
                    aParameters.Add(new SqlParameter("@productId", item.ProductId));
                    aParameters.Add(new SqlParameter("@requisitionQuantity", item.RequisitionQuantity));
                    aParameters.Add(new SqlParameter("@requisitionKg", item.RequisitionKg));
                    aParameters.Add(new SqlParameter("@remarks", item.Remarks));

                    result = _accessManager.SaveData("sp_SaveOrUpdateStockTransferReqDetails", aParameters);

                    if (result == false)
                    {
                        break;
                    }

                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public ResultResponse SaveInternalStockTransferMaster(StockTransferReqMaster aMaster)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                ResultResponse aResponse = new ResultResponse();

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@reqMasterId", aMaster.StockTransferReqMasterId));
                aSqlParameterList.Add(new SqlParameter("@CompanyId", aMaster.CompanyId));
                aSqlParameterList.Add(new SqlParameter("@FromWarehouseId", aMaster.FromWarehouseId));
                aSqlParameterList.Add(new SqlParameter("@ToWarehouseId", aMaster.ToWarehouseId));
                aSqlParameterList.Add(new SqlParameter("@FromLocationId", aMaster.FromLocationId));
                aSqlParameterList.Add(new SqlParameter("@ToLocationId", aMaster.ToLocationId));
                aSqlParameterList.Add(new SqlParameter("@RequisitionBy", aMaster.RequisitionBy));
                aSqlParameterList.Add(new SqlParameter("@RequisitionDate", aMaster.RequisitionDate));
                aSqlParameterList.Add(new SqlParameter("@ExpectedDeliveryDate", aMaster.ExpectedDeliveryDate));
                aSqlParameterList.Add(new SqlParameter("@EmpId", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@Remarks", aMaster.Remarks));
                aSqlParameterList.Add(new SqlParameter("@VehicleId", aMaster.VehicleId));
                //aSqlParameterList.Add(new SqlParameter("@DriverName", aMaster.DriverName));
                //aSqlParameterList.Add(new SqlParameter("@DriverMobile", aMaster.DriverMobile));
                aSqlParameterList.Add(new SqlParameter("@Groupid", aMaster.GroupId));
                aSqlParameterList.Add(new SqlParameter("@InternalTransferCommentId", aMaster.InternalTransferCommentId));
                aSqlParameterList.Add(new SqlParameter("@IsInterSTO", aMaster.IsInterSTO));

               // aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_SaveInternalStockTransferMaster", aSqlParameterList);

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_SaveInternalStockTransferMaster", aSqlParameterList);

                int transferReqMasterId = 0;
                int transferIssueMasterId = 0;
                int transferReceiveMasterId = 0;
                int stockInMasterId = 0;

                while (dr.Read())
                {
                    transferReqMasterId = Convert.ToInt32(dr["TransferReqMasterIdTab"]);
                    transferIssueMasterId = Convert.ToInt32(dr["TransferIssueMasterIdTab"]);
                    transferReceiveMasterId = Convert.ToInt32(dr["TransferReceiveMasterIdTab"]);
                    stockInMasterId = Convert.ToInt32(dr["StockInMasterIdTab"]);

                }
                dr.Close();


                if (transferReqMasterId > 0 && transferIssueMasterId > 0 && transferReceiveMasterId > 0 && stockInMasterId > 0)
                {
                    aResponse.pk = transferReqMasterId;
                    aResponse.isSuccess = SaveInternalStockTransferDetails(aMaster.StockTransferReqDetails, transferReqMasterId, transferIssueMasterId, transferReceiveMasterId, stockInMasterId);

                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                    //else
                    //{
                    //    aResponse.isSuccess = StockTransferLogEntry(aMaster);

                    //}

                }
                return aResponse;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public bool SaveInternalStockTransferDetails(List<StockTransferReqDetails> aList, int transferReqMasterId, int transferIssueMasterId, int transferReceiveMasterId, int stockInMasterId)
        {
            try
            {
                bool result = false;
                foreach (var item in aList)
                {
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@reqDetailsId", item.StockTransferReqDetailsId));
                    aParameters.Add(new SqlParameter("@reqMasterId", transferReqMasterId));
                    aParameters.Add(new SqlParameter("@issueMasterId", transferIssueMasterId));
                    aParameters.Add(new SqlParameter("@receiveMasterId", transferReceiveMasterId));
                    aParameters.Add(new SqlParameter("@stockInMasterId", stockInMasterId));
                    aParameters.Add(new SqlParameter("@productId", item.ProductId));
                    aParameters.Add(new SqlParameter("@requisitionQuantity", item.RequisitionQuantity));
                    aParameters.Add(new SqlParameter("@requisitionKg", item.RequisitionKg));
                    aParameters.Add(new SqlParameter("@remarks", item.Remarks));

                    result = _accessManager.SaveData("sp_SaveInternalStockTransferDetails", aParameters);

                    if (result == false)
                    {
                        break;
                    }

                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool StockTransferLogEntry(StockTransferIssueMaster stockTransferMaster)
        {
            try
            {

                bool result = false;
                foreach (var item in stockTransferMaster.StockTransferReqDetails)
                {
                    List<SqlParameter> aParameters = new List<SqlParameter>();

                    aParameters.Add(new SqlParameter("@requisitionBy", stockTransferMaster.IssueBy));
                    aParameters.Add(new SqlParameter("@requisitionDate", stockTransferMaster.DeliveryDate));
                    aParameters.Add(new SqlParameter("@remarks", stockTransferMaster.Remarks));
                    aParameters.Add(new SqlParameter("@fromLocationId", stockTransferMaster.FromLocationId));
                    aParameters.Add(new SqlParameter("@toLocationId", stockTransferMaster.ToLocationId));
                    
                    aParameters.Add(new SqlParameter("@toWareHouse", stockTransferMaster.ToWarehouseId));
                    aParameters.Add(new SqlParameter("@fromWareHouse", stockTransferMaster.FromWarehouseId));
                    aParameters.Add(new SqlParameter("@productId", item.ProductId));
                    aParameters.Add(new SqlParameter("@requisitionQuantity", item.IssueQty));
                    aParameters.Add(new SqlParameter("@requisitionKg", item.IssueKg));

                    result = _accessManager.SaveData("sp_UpdateStockInDetailsForStockIssue", aParameters);

                    if (result == false)
                    {
                        break;
                    }

                }
                return result;

            }
            catch (Exception ex)
            {

                throw ex;
            }
           

        }

        public DataTable GetProductIsssueReceive(DateTime fromDate, DateTime todDate, int warehouseId=0,int toWarehouseId=0, bool isInterSTO = false)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@FromDate", fromDate));
                aParameters.Add(new SqlParameter("@ToDate", todDate));
                aParameters.Add(new SqlParameter("@FromWareHouseId", warehouseId));
                aParameters.Add(new SqlParameter("@ToWareHouseId", toWarehouseId));
                aParameters.Add(new SqlParameter("@IsInterSTO", isInterSTO));
                DataTable dt = _accessManager.GetDataTable("sp_GetProductRequestIssueReceive", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<ViewProductByGroupAndWareHouse> GetStockedProductByWarehouseAndGroup(int warehouseId, int groupId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@WareHouseId", warehouseId));
                aSqlParameterList.Add(new SqlParameter("@GroupId", groupId));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetStockedProductByWarehouseAndProductGroup", aSqlParameterList);
                List<ViewProductByGroupAndWareHouse> ViewProductByGroupAndWareHouseList = new List<ViewProductByGroupAndWareHouse>();
                while (dr.Read())
                {
                    ViewProductByGroupAndWareHouse viewProductByGroupAndWareHouse = new ViewProductByGroupAndWareHouse();
                    viewProductByGroupAndWareHouse.ProductId = Convert.ToInt32(dr["ProductID"]);
                    viewProductByGroupAndWareHouse.ProductName = dr["ProductName"].ToString();
                    viewProductByGroupAndWareHouse.StockQuantity = DBNull.Value == dr["StockInQty"] ? 0 : Convert.ToDecimal(dr["StockInQty"]);
                    viewProductByGroupAndWareHouse.StockKg = DBNull.Value == dr["StockInKG"] ? 0 : Convert.ToDecimal(dr["StockInKG"]);
                    viewProductByGroupAndWareHouse.PerKgStdQty = DBNull.Value == dr["PerKgStdQty"] ? 0 : Convert.ToDecimal(dr["PerKgStdQty"]);

                    ViewProductByGroupAndWareHouseList.Add(viewProductByGroupAndWareHouse);
                }

                return ViewProductByGroupAndWareHouseList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

        public List<StockTransferReqMaster> GetStockTransferRequestByDate(DateTime reqDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@reqDate", reqDate));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetStockTransferRequestByDate", aSqlParameterList);
                List<StockTransferReqMaster> stockTransferReqMasterList = new List<StockTransferReqMaster>();
                while (dr.Read())
                {
                    StockTransferReqMaster stockTransferReqMaster = new StockTransferReqMaster();
                    stockTransferReqMaster.StockTransferReqMasterId = Convert.ToInt32(dr["StockTransferReqMasterId"]);
                    stockTransferReqMaster.RequisitionNo = dr["RequisitionNo"].ToString();

                    stockTransferReqMasterList.Add(stockTransferReqMaster);
                }

                return stockTransferReqMasterList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }


        public List<StockTransferReqMaster> StockTransferList(DateTime? reqDate)
        {
            try
            {
                if (reqDate == null)
                {
                    reqDate = System.DateTime.Now;

                }
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@RequisitionDate", String.Format("{0:MM/dd/yyyy}", reqDate)));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetStockTransferList", aSqlParameterList);
                List<StockTransferReqMaster> stockTransferReqMasterList = new List<StockTransferReqMaster>();
                while (dr.Read())
                {
                    StockTransferReqMaster stockTransferReqMaster = new StockTransferReqMaster();
                    stockTransferReqMaster.StockTransferReqMasterId = Convert.ToInt32(dr["StockTransferReqMasterId"]);
                    stockTransferReqMaster.RequisitionNo = dr["RequisitionNo"].ToString();
                    stockTransferReqMaster.RequisitionDate = Convert.ToDateTime(dr["RequisitionDate"]);
                    stockTransferReqMaster.ExpectedDeliveryDate = Convert.ToDateTime(dr["ExpectedDeliveryDate"]);
                    stockTransferReqMaster.RequisitionBy = dr["RequisitionBy"].ToString();
                    stockTransferReqMaster.CompanyName = dr["CompanyName"].ToString();
                    stockTransferReqMaster.FromLocation = dr["fLocation"].ToString();
                    stockTransferReqMaster.fromWarehouse = dr["fWarehouse"].ToString();
                    stockTransferReqMaster.ToLocation = dr["toLocation"].ToString();
                    stockTransferReqMaster.ToWarehouse = dr["ToWarehouse"].ToString();
                    stockTransferReqMaster.TotalRequisitionQuantity = Convert.ToDecimal(dr["RequisitionQuantity"]);
                    stockTransferReqMaster.TotalRequisitionKg = Convert.ToDecimal(dr["RequisitionKg"]);
                    stockTransferReqMaster.FullPicked = Convert.ToInt32(dr["FullPicked"]);
                    stockTransferReqMaster.PickingStatus = dr["PickingStatus"].ToString();

                    stockTransferReqMasterList.Add(stockTransferReqMaster);
                }

                return stockTransferReqMasterList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public List<StockTransferReqMaster> InternalStockTransferList(DateTime? fromDate, DateTime? toDate)
        {
            try
            {
                if (fromDate == null)
                {
                    fromDate = System.DateTime.Now;
                }
                if (toDate == null)
                {
                    toDate = System.DateTime.Now;
                }
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@fromDate", String.Format("{0:MM/dd/yyyy}", fromDate)));
                aSqlParameterList.Add(new SqlParameter("@toDate", String.Format("{0:MM/dd/yyyy}", toDate)));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetInternalStockTransferList", aSqlParameterList);
                List<StockTransferReqMaster> stockTransferReqMasterList = new List<StockTransferReqMaster>();
                while (dr.Read())
                {
                    StockTransferReqMaster stockTransferReqMaster = new StockTransferReqMaster();
                    stockTransferReqMaster.StockTransferReqMasterId = Convert.ToInt32(dr["StockTransferReqMasterId"]);
                    stockTransferReqMaster.RequisitionNo = dr["RequisitionNo"].ToString();
                    stockTransferReqMaster.RequisitionDate = Convert.ToDateTime(dr["RequisitionDate"]);
                    stockTransferReqMaster.ExpectedDeliveryDate = Convert.ToDateTime(dr["ExpectedDeliveryDate"]);
                    stockTransferReqMaster.RequisitionBy = dr["RequisitionBy"].ToString();
                    stockTransferReqMaster.CompanyName = dr["CompanyName"].ToString();
                    stockTransferReqMaster.FromLocation = dr["fLocation"].ToString();
                    stockTransferReqMaster.fromWarehouse = dr["fWarehouse"].ToString();
                    stockTransferReqMaster.ToLocation = dr["toLocation"].ToString();
                    stockTransferReqMaster.ToWarehouse = dr["ToWarehouse"].ToString();
                    stockTransferReqMaster.TotalRequisitionQuantity = Convert.ToDecimal(dr["RequisitionQuantity"]);
                    stockTransferReqMaster.TotalRequisitionKg = Convert.ToDecimal(dr["RequisitionKg"]);
                    stockTransferReqMaster.FullPicked = Convert.ToInt32(dr["FullPicked"]);
                    stockTransferReqMaster.InternalTransferCommentId =DBNull.Value.Equals(dr["InternalTransferCommentId"]) ? 0 : Convert.ToInt32(dr["InternalTransferCommentId"]);
                    stockTransferReqMaster.Comment= dr["Comment"].ToString();

                    stockTransferReqMasterList.Add(stockTransferReqMaster);
                }

                return stockTransferReqMasterList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public StockTransferReqMaster GetStockTransferMaster(int id)
        {
            try
            {
                List<SqlParameter> aParameters = new List<SqlParameter>();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                aParameters.Add(new SqlParameter("@StockTransferReqMasterId", id));
                DataTable dt = _accessManager.GetDataTable("sp_GetStockTransferMasterById", aParameters);
                List<StockTransferReqMaster> aList = Helper.ToListof<StockTransferReqMaster>(dt);
                return aList[0];
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<StockTransferReqDetails> GetStockTransferDetailsByMaster(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@StockTransferReqMasterId", id));
                DataTable dt = _accessManager.GetDataTable("sp_GetStockTransferDetailById", aParameters);
                List<StockTransferReqDetails> aList = Helper.ToListof<StockTransferReqDetails>(dt);
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<StockTransferReqMaster> StockTransferListForIssue(DateTime? issueDate, int companyId, int fromLocation, int fromWarhouse, int toLocation, int toWarehouse, int vehicleId)
        {
            try
            {
                if (issueDate == null)
                {
                    issueDate = System.DateTime.Now;

                }
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@DeliveryDate", String.Format("{0:MM/dd/yyyy}", issueDate)));
                aSqlParameterList.Add(new SqlParameter("@CompanyId", companyId));
                aSqlParameterList.Add(new SqlParameter("@FromLocation", fromLocation));
                aSqlParameterList.Add(new SqlParameter("@ToLocation", toLocation));
                aSqlParameterList.Add(new SqlParameter("@FromWarehouseId", fromWarhouse));
                aSqlParameterList.Add(new SqlParameter("@ToWarehouseId", toWarehouse));
                aSqlParameterList.Add(new SqlParameter("@VehicleId", vehicleId));
                
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetStockListForIssue", aSqlParameterList);
                List<StockTransferReqMaster> stockTransferReqMasterList = new List<StockTransferReqMaster>();
                while (dr.Read())
                {
                    StockTransferReqMaster stockTransferReqMaster = new StockTransferReqMaster();
                    stockTransferReqMaster.StockTransferReqMasterId = Convert.ToInt32(dr["StockTransferReqMasterId"]);
                    stockTransferReqMaster.RequisitionNo = dr["RequisitionNo"].ToString();
                    stockTransferReqMaster.RequisitionDate = Convert.ToDateTime(dr["RequisitionDate"]);
                    stockTransferReqMaster.ExpectedDeliveryDate = Convert.ToDateTime(dr["ExpectedDeliveryDate"]);
                    stockTransferReqMaster.RequisitionBy = dr["RequisitionBy"].ToString();
                    stockTransferReqMaster.CompanyName = dr["CompanyName"].ToString();
                    stockTransferReqMaster.FromLocation = dr["fLocation"].ToString();
                    stockTransferReqMaster.fromWarehouse = dr["fWarehouse"].ToString();
                    stockTransferReqMaster.ToLocation = dr["toLocation"].ToString();
                    stockTransferReqMaster.ToWarehouse = dr["ToWarehouse"].ToString();
                    stockTransferReqMaster.TotalRequisitionQuantity = Convert.ToDecimal(dr["RequisitionQuantity"]);
                    stockTransferReqMaster.TotalRequisitionKg = Convert.ToDecimal(dr["RequisitionKg"]);

                    stockTransferReqMaster.TotalIssueQuantity = Convert.ToDecimal(dr["IssueQty"]);
                    stockTransferReqMaster.TotalIssueKg = Convert.ToDecimal(dr["IssueKg"]);


                    stockTransferReqMasterList.Add(stockTransferReqMaster);
                }

                return stockTransferReqMasterList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

        public List<StockTransferIssueMaster> StockTransferChallanListForPrint(DateTime fromDate, DateTime toDate, int fromWarehouse = 0, int fromLocation = 0, int toWarehouse = 0, int toLocation = 0)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@fromDate", fromDate));
                aSqlParameterList.Add(new SqlParameter("@toDate", toDate));
                aSqlParameterList.Add(new SqlParameter("@fromWarehouse", fromWarehouse));
                aSqlParameterList.Add(new SqlParameter("@fromLocation", fromLocation));
                aSqlParameterList.Add(new SqlParameter("@toWarehouse", toWarehouse));
                aSqlParameterList.Add(new SqlParameter("@toLocation", toLocation));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_StockTransferChallanListForPrint", aSqlParameterList);
                List<StockTransferIssueMaster> stockTransferReqMasterList = new List<StockTransferIssueMaster>();
                while (dr.Read())
                {
                    StockTransferIssueMaster stockTransferReqMaster = new StockTransferIssueMaster();
                    stockTransferReqMaster.StockTransferIssueMasterId = Convert.ToInt32(dr["StockTransferIssueMasterId"]); 
                    stockTransferReqMaster.fromWarehouse = dr["FromWarehouse"].ToString();
                    stockTransferReqMaster.ToWarehouse = dr["ToWarehouse"].ToString();
                    stockTransferReqMaster.FromLocation = dr["FromLocation"].ToString();
                    stockTransferReqMaster.ToLocation = dr["ToLocation"].ToString(); 
                    stockTransferReqMaster.VehicleRegNo = dr["VehicleRegNo"].ToString();
                    stockTransferReqMaster.DriverName = dr["DriverName"].ToString();
                    stockTransferReqMaster.DriverMobile = dr["DriverMobile"].ToString();
                    stockTransferReqMaster.DeliveryDate = Convert.ToDateTime(dr["DeliveryDate"]);
                    stockTransferReqMaster.IssueNo = dr["IssueNo"].ToString(); 

                    stockTransferReqMasterList.Add(stockTransferReqMaster);
                }

                return stockTransferReqMasterList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }


        public DataTable GetStockTransferIssueChallanForPrint(string id) 
        {
            try
            {
                DataTable dt = new DataTable();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                dt = _accessManager.GetDataTable("sp_GetStockTransferIssueChallanForPrint", aParameters); 

                return dt;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public List<VehicleInfo> VehicleInfo(DateTime deliveryDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@DeliveryDate", deliveryDate));
                
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetVehicleByDeliveryDate", aSqlParameterList);
                List<VehicleInfo> VehicleInfoList = new List<VehicleInfo>();
                while (dr.Read())
                {
                    VehicleInfo vehicle = new VehicleInfo();
                    vehicle.VehicleInfoId = Convert.ToInt32(dr["VehicleInfoId"]);
                    vehicle.VehicleRegNo = dr["VehicleRegNo"].ToString();
                    VehicleInfoList.Add(vehicle);
                }
                return VehicleInfoList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public List<ViewProductByGroupAndWareHouse> GetStockedProductById(int stockTransferReqMasterId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@StockTransferReqMasterId", stockTransferReqMasterId));
                
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetProductDetailForIssue", aSqlParameterList);
                List<ViewProductByGroupAndWareHouse> ViewProductByGroupAndWareHouseList = new List<ViewProductByGroupAndWareHouse>();
                while (dr.Read())
                {
                    ViewProductByGroupAndWareHouse viewProductByGroupAndWareHouse = new ViewProductByGroupAndWareHouse();
                    viewProductByGroupAndWareHouse.ProductId = Convert.ToInt32(dr["ProductID"]);
                    viewProductByGroupAndWareHouse.ProductName = dr["ProductName"].ToString();
                    viewProductByGroupAndWareHouse.TransferQty = DBNull.Value == dr["RequisitionQuantity"] ? 0 : Convert.ToDecimal(dr["RequisitionQuantity"]);
                    viewProductByGroupAndWareHouse.TransferKg = DBNull.Value == dr["RequisitionKg"] ? 0 : Convert.ToDecimal(dr["RequisitionKg"]);

                    viewProductByGroupAndWareHouse.AIssueQty = DBNull.Value == dr["AIssueQty"] ? 0 : Convert.ToDecimal(dr["AIssueQty"]);
                    viewProductByGroupAndWareHouse.AIssueKg = DBNull.Value == dr["AIssueKg"] ? 0 : Convert.ToDecimal(dr["AIssueKg"]);

                    viewProductByGroupAndWareHouse.IssueQty = DBNull.Value == dr["IssueQty"] ? 0 : Convert.ToDecimal(dr["IssueQty"]);
                    viewProductByGroupAndWareHouse.IssueKg = DBNull.Value == dr["IssueKg"] ? 0 : Convert.ToDecimal(dr["IssueKg"]);

                    viewProductByGroupAndWareHouse.StockQuantity = DBNull.Value == dr["StockInQty"] ? 0 : Convert.ToDecimal(dr["StockInQty"]);
                    viewProductByGroupAndWareHouse.StockKg = DBNull.Value == dr["StockInKG"] ? 0 : Convert.ToDecimal(dr["StockInKG"]);

                    ViewProductByGroupAndWareHouseList.Add(viewProductByGroupAndWareHouse);
                }

                return ViewProductByGroupAndWareHouseList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

        public List<StockTransferReqDetails> GetStockTransferDetailsByMasterForIssue(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@StockTransferReqMasterId", id));
                DataTable dt = _accessManager.GetDataTable("sp_GetProductDetailForIssue", aParameters);
                List<StockTransferReqDetails> aList = Helper.ToListof<StockTransferReqDetails>(dt);
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public ResultResponse SaveStockTransferIssueMaster(StockTransferIssueMaster aMaster)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                ResultResponse aResponse = new ResultResponse();

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@StockTransferIssueMasterId", aMaster.StockTransferIssueMasterId));
                aSqlParameterList.Add(new SqlParameter("@reqMasterId", aMaster.StockTransferReqMasterId));
                aSqlParameterList.Add(new SqlParameter("@CompanyId", aMaster.CompanyId));
                aSqlParameterList.Add(new SqlParameter("@FromWarehouseId", aMaster.FromWarehouseId));
                aSqlParameterList.Add(new SqlParameter("@ToWarehouseId", aMaster.ToWarehouseId));
                aSqlParameterList.Add(new SqlParameter("@FromLocationId", aMaster.FromLocationId));
                aSqlParameterList.Add(new SqlParameter("@ToLocationId", aMaster.ToLocationId));
                aSqlParameterList.Add(new SqlParameter("@IssueBy", aMaster.IssueBy));
                aSqlParameterList.Add(new SqlParameter("@DeliveryDate", aMaster.DeliveryDate));
                
                aSqlParameterList.Add(new SqlParameter("@EmpId", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@Remarks", aMaster.Remarks));


                aSqlParameterList.Add(new SqlParameter("@VehicleId", aMaster.VehicleId));
                aSqlParameterList.Add(new SqlParameter("@DriverName", aMaster.DriverName));
                aSqlParameterList.Add(new SqlParameter("@DriverMobile", aMaster.DriverMobile));
                aSqlParameterList.Add(new SqlParameter("@Groupid", aMaster.GroupId));

                

                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_SaveStockTransferIssueMaster", aSqlParameterList);
                if (aResponse.pk > 0)
                {
                    aResponse.isSuccess = SaveStockTransferIssueDetails(aMaster.StockTransferReqDetails, aResponse.pk, aMaster.FromWarehouseId);

                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                    //else
                    //{
                    //    aResponse.isSuccess = StockTransferLogEntry(aMaster);

                    //}

                }
                return aResponse;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public bool SaveStockTransferIssueDetails(List<StockTransferIssueDetails> aList, int pk,int fromWareHouseId)
        {
            try
            {
                bool result = false;
                foreach (var item in aList)
                {
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@StockTransferIssueDetailsId", item.StockTransferIssueDetailsId));
                    aParameters.Add(new SqlParameter("@StockTransferIssueMasterId", pk));
                    aParameters.Add(new SqlParameter("@fromWareHouse", fromWareHouseId));
                    aParameters.Add(new SqlParameter("@reqDetailsId", item.StockTransferReqDetailsId));
                    aParameters.Add(new SqlParameter("@reqMasterId", item.StockTransferReqMasterId));
                    aParameters.Add(new SqlParameter("@productId", item.ProductId));
                    aParameters.Add(new SqlParameter("@requisitionQuantity", item.RequisitionQuantity));
                    aParameters.Add(new SqlParameter("@requisitionKg", item.RequisitionKg));
                    aParameters.Add(new SqlParameter("@IssueQty", item.IssueQty));
                    aParameters.Add(new SqlParameter("@IssueKg", item.IssueKg));
                    aParameters.Add(new SqlParameter("@SalesPriceTransfer", item.SalesPriceTransfer));
                    aParameters.Add(new SqlParameter("@IssueAmount", item.IssueAmount));
                    aParameters.Add(new SqlParameter("@Comission", item.Comission));
                    aParameters.Add(new SqlParameter("@ComissionAmount", item.ComissionAmount));
                    aParameters.Add(new SqlParameter("@NetAmount", item.NetAmount));

                    aParameters.Add(new SqlParameter("@remarks", item.Remarks));


                    result = _accessManager.SaveData("sp_SaveStockTransferIssueDetails", aParameters);

                    if (result == false)
                    {
                        break;
                    }

                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public DataTable GetTransferWiseInvoice(DateTime FromDate, DateTime ToDate, int VehicleId=0,int FromLocationId=0,int FromWarehouseId=0)
        {

            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@FromDate", FromDate));
                parameters.Add(new SqlParameter("@ToDate", ToDate));
                parameters.Add(new SqlParameter("@VehicleId", VehicleId));
                parameters.Add(new SqlParameter("@FromLocationId", FromLocationId));
                parameters.Add(new SqlParameter("@FromWarehouseId", FromWarehouseId));
                DataTable dr = _accessManager.GetDataTable("sp_GetTransferWiseInvoice", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetTransferWiseInvoiceWithValue(DateTime FromDate, DateTime ToDate, int VehicleId = 0, int FromLocationId = 0, int FromWarehouseId = 0)
        {

            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@FromDate", FromDate));
                parameters.Add(new SqlParameter("@ToDate", ToDate));
                parameters.Add(new SqlParameter("@VehicleId", VehicleId));
                parameters.Add(new SqlParameter("@FromLocationId", FromLocationId));
                parameters.Add(new SqlParameter("@FromWarehouseId", FromWarehouseId));
                DataTable dr = _accessManager.GetDataTable("sp_GetTransferWiseInvoiceWithValue", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public List<ViewProductByGroupAndWareHouse> GetStockedProductByWarehouseAndGroup5_6(int warehouseId, int groupId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@WareHouseId", warehouseId));
                aSqlParameterList.Add(new SqlParameter("@GroupId", groupId));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetStockedProductByWarehouseAndGroup5_6", aSqlParameterList);
                List<ViewProductByGroupAndWareHouse> ViewProductByGroupAndWareHouseList = new List<ViewProductByGroupAndWareHouse>();
                while (dr.Read())
                {
                    ViewProductByGroupAndWareHouse viewProductByGroupAndWareHouse = new ViewProductByGroupAndWareHouse();
                    viewProductByGroupAndWareHouse.ProductId = Convert.ToInt32(dr["ProductID"]);
                    viewProductByGroupAndWareHouse.GroupId = Convert.ToInt32(dr["GroupId"]);
                    viewProductByGroupAndWareHouse.ProductName = dr["ProductName"].ToString();
                    viewProductByGroupAndWareHouse.StockQuantity = DBNull.Value == dr["StockInQty"] ? 0 : Convert.ToDecimal(dr["StockInQty"]);
                    viewProductByGroupAndWareHouse.StockKg = DBNull.Value == dr["StockInKG"] ? 0 : Convert.ToDecimal(dr["StockInKG"]);
                    viewProductByGroupAndWareHouse.PerKgStdQty = DBNull.Value == dr["PerKgStdQty"] ? 0 : Convert.ToDecimal(dr["PerKgStdQty"]);

                    ViewProductByGroupAndWareHouseList.Add(viewProductByGroupAndWareHouse);
                }

                return ViewProductByGroupAndWareHouseList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }


        public ViewProductByGroupAndWareHouse GetGroupByProduct(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));

                SqlDataReader aReader = _accessManager.GetSqlDataReader("GetGroupByProductId", aParameters);

                ViewProductByGroupAndWareHouse aListView = new ViewProductByGroupAndWareHouse();
                while (aReader.Read())
                {
                
                    aListView.GroupId = Convert.ToInt32(aReader["GroupId"]);
                   
                }
                return aListView;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

    }
}