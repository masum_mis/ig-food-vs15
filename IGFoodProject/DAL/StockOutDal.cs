﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using IGFoodProject.DataManager;
using IGFoodProject.Models;
using IGFoodProject.Models.ReportModel;
using IGFoodProject.ViewModel;

namespace IGFoodProject.DAL
{
    public class StockOutDal
    {
        DataAccessManager _accessManager = new DataAccessManager();
        public OtherStockIssueDetails GetItemDetails(int itemId, int warehouseId)
        {
            try
            {
                OtherStockIssueDetails aDetails = new OtherStockIssueDetails();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@WareHouseId", warehouseId));
                aParameters.Add(new SqlParameter("@fromDate", DateTime.Now.Date));
                aParameters.Add(new SqlParameter("@GroupId", 0));
                aParameters.Add(new SqlParameter("@ProductId", itemId));

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SqlDataReader dr = _accessManager.GetSqlDataReader("ItemStockInfo", aParameters);
                while (dr.Read())
                {
                    aDetails.ItemId = Convert.ToInt32(dr["ItemId"]);
                    aDetails.ItemCode = dr["ItemCode"].ToString();
                    aDetails.ItemName = dr["ItemName"].ToString();
                    aDetails.UOMId = Convert.ToInt32(dr["UOMId"]);
                    aDetails.StockQty = Convert.ToDecimal(dr["CurrentStockQty"]);
                    aDetails.StockKg = Convert.ToDecimal(dr["CurrentStockKG"]);
                }
                return aDetails;
            }
            catch (Exception ex)
            {

                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public ResultResponse SaveStockIssue(OtherStockIssueMaster aMaster)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                ResultResponse aResponse = new ResultResponse();

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@MasterId", aMaster.StockIssueMasterId));
                aSqlParameterList.Add(new SqlParameter("@RequisitionMasterId", aMaster.RequisitionMasterId));
                aSqlParameterList.Add(new SqlParameter("@CompanyId", 7));
                aSqlParameterList.Add(new SqlParameter("@WarehouseId", aMaster.WarehouseId));

                aSqlParameterList.Add(new SqlParameter("@IssueBy", aMaster.IssueBy));
                aSqlParameterList.Add(new SqlParameter("@IssueDate", aMaster.IssueDate));
                aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@Remarks", aMaster.Remarks));
                aSqlParameterList.Add(new SqlParameter("@ProjectId", aMaster.ProjectId));
                aSqlParameterList.Add(new SqlParameter("@DepartmentId", aMaster.DepartmentId));




                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("SaveOtherStockIssue", aSqlParameterList);
                if (aResponse.pk > 0)
                {
                    // aResponse.isSuccess = SaveStockIssueDetails(aMaster.StockIssueDetails, aResponse.pk);
                    aResponse.isSuccess = SaveOtherStockOut(aResponse.pk, aMaster.StockIssueDetails);
                    aResponse.isSuccess = ItemIssueSystemJournal(aResponse.pk);
                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                    //else
                    //{
                    //    aResponse.isSuccess = StockTransferLogEntry(aMaster);

                    //}

                }
                return aResponse;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        private bool SaveOtherStockOut(int aResponsePk, List<OtherStockIssueDetails> stockIssueDetails)
        {
            try
            {
                bool result = false;
                int stockmasterid = 0;
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                ResultResponse aResponse = new ResultResponse();

                //aSqlParameterList.Add(new SqlParameter("@IssueMasterId", aResponsePk));


                //stockmasterid = _accessManager.SaveDataReturnPrimaryKey("SaveIssueInotherStockIn", aSqlParameterList);

                foreach (var item in stockIssueDetails)
                {

                    //   if (stockmasterid > 0)
                    //  {



                    List<SqlParameter> aSqlParameterList1 = new List<SqlParameter>();


                    aSqlParameterList1.Add(new SqlParameter("@IssueMasterId", aResponsePk));
                    //    aSqlParameterList1.Add(new SqlParameter("@StockInMasterId", stockmasterid));
                    aSqlParameterList1.Add(new SqlParameter("@itemId", item.ItemId));
                    aSqlParameterList1.Add(new SqlParameter("@IssueQty", item.IssueQty));
                    aSqlParameterList1.Add(new SqlParameter("@IsFull", item.IsFull));
                    aSqlParameterList1.Add(new SqlParameter("@PurposeID", item.PurposeID));



                    result = _accessManager.SaveData("SaveOtherIssueStockInDetails", aSqlParameterList1);




                    // }


                }
                return result = true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private bool SaveStockIssueDetails(List<OtherStockIssueDetails> stockIssueDetails, int pk)
        {
            try
            {
                bool result = false;
                foreach (var item in stockIssueDetails)
                {


                    if (item.IssueQty > 0)
                    {
                        List<SqlParameter> aParameters = new List<SqlParameter>();

                        aParameters.Add(new SqlParameter("@IssueMasterId", pk));
                        aParameters.Add(new SqlParameter("@ItemId", item.ItemId));
                        aParameters.Add(new SqlParameter("@IssueQty", item.IssueQty));
                        aParameters.Add(new SqlParameter("@IsFull", item.IsFull));
                        //aParameters.Add(new SqlParameter("@IssueKg", item.IssueKg));


                        result = _accessManager.SaveData("sp_SaveOtherStockIssueDetails", aParameters);
                    }


                    if (result == false)
                    {
                        break;
                    }

                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public bool ItemIssueSystemJournal(int masterId)
        {

            try
            {

                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@IssueMasterId", masterId));


                result = _accessManager.SaveData("SaveIssueJournal", aParameters);

                return result;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public ResultResponse SaveIssueRequisition(IssueRequisition aMaster)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                ResultResponse aResponse = new ResultResponse();

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@MasterId", aMaster.RequisitionMasterId));
                aSqlParameterList.Add(new SqlParameter("@CompanyId", 7));
                aSqlParameterList.Add(new SqlParameter("@WarehouseId", aMaster.WarehouseId));

                aSqlParameterList.Add(new SqlParameter("@RequisitionBy", aMaster.RequisitionBy));
                aSqlParameterList.Add(new SqlParameter("@RequisitionDate", aMaster.RequisitionDate));
                aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@Remarks", aMaster.Remarks));
                aSqlParameterList.Add(new SqlParameter("@DepartmentId", aMaster.DepartmentId));
                aSqlParameterList.Add(new SqlParameter("@eflag", aMaster.eflag));
                aSqlParameterList.Add(new SqlParameter("@RequiredDate", aMaster.RequiredDate));
                aSqlParameterList.Add(new SqlParameter("@RequiredTime", aMaster.RequiredTime));
                if (aMaster.eflag == "ed")
                {
                    aMaster.IsApprove = false;
                }
                else
                {
                    aMaster.IsApprove = false;
                }
                if (aMaster.eflag == "ap")
                {
                    aSqlParameterList.Add(new SqlParameter("@IsApprove", true));
                    aSqlParameterList.Add(new SqlParameter("@ApproveBy", aMaster.CreateBy));
                }
                else
                {
                    aSqlParameterList.Add(new SqlParameter("@IsApprove", aMaster.IsApprove));
                    aSqlParameterList.Add(new SqlParameter("@ApproveBy", null));
                }


                aSqlParameterList.Add(new SqlParameter("@ProjectId", aMaster.ProjectId));
                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("SaveIssueRequisitionMaster", aSqlParameterList);
                if (aResponse.pk > 0)
                {
                    aResponse.isSuccess = SaveIssueRequisitionDetails(aMaster.IssueRequisitionDetailses, aResponse.pk);

                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                    //else
                    //{
                    //    aResponse.isSuccess = StockTransferLogEntry(aMaster);

                    //}

                }
                return aResponse;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        private bool SaveIssueRequisitionDetails(List<IssueRequisitionDetails> stockIssueDetails, int pk)
        {
            try
            {
                bool result = false;
                foreach (var item in stockIssueDetails)
                {
                    List<SqlParameter> aParameters = new List<SqlParameter>();

                    aParameters.Add(new SqlParameter("@ReqMasterId", pk));
                    aParameters.Add(new SqlParameter("@detailsId", item.RequisitionDetailsId));
                    aParameters.Add(new SqlParameter("@ItemId", item.ItemId));
                    aParameters.Add(new SqlParameter("@RequisitionQty", item.RequisitionQty));
                    aParameters.Add(new SqlParameter("@ApprovedQty", item.ApprovedQty));
                    aParameters.Add(new SqlParameter("@PurposeID", item.PurposeID));
                    //aParameters.Add(new SqlParameter("@RequisitionKg", item.RequisitionKg));
                    aParameters.Add(new SqlParameter("@Remarks", item.Remarks));
                    aParameters.Add(new SqlParameter("@IsDelete", item.IsDelete));
                    aParameters.Add(new SqlParameter("@eflag", item.eflag));

                    result = _accessManager.SaveData("SaveIssueRequisitionDetails", aParameters);

                    if (result == false)
                    {
                        break;
                    }

                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public List<IssueRequisition> GetPendingIssueList(DateTime? fromdate, DateTime? todate, int warehouse = 0, string reqBy = null)
        {
            try
            {
                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-2);

                }

                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<IssueRequisition> aList = new List<IssueRequisition>();
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@WareHouseId", warehouse));
                aParameters.Add(new SqlParameter("@RequisitionBy", reqBy));

                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("PendingRequisitionListForIssue", aParameters);
                while (dr.Read())
                {
                    IssueRequisition aMaster = new IssueRequisition();
                    aMaster.RequisitionMasterId = Convert.ToInt32(dr["RequisitionMasterId"]);
                    aMaster.Remarks = dr["Remarks"].ToString();
                    aMaster.RequisitionNo = dr["RequisitionNo"].ToString();

                    aMaster.RequisitionDate = Convert.ToDateTime(dr["RequisitionDate"]);
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.DepartmentName = dr["DepartmentName"].ToString();
                    aMaster.RequisitionBy = dr["RequisitionByName"].ToString();
                    aMaster.IsApprove = Convert.ToBoolean(dr["IsApprove"]);
                    aMaster.TotalRequisitionQuantity = Convert.ToDecimal(dr["TotalRequisitionQty"]);



                    aList.Add(aMaster);
                }

                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<IssueRequisition> IssueRequisitionList(DateTime? fromdate, DateTime? todate, int warehouse = 0, string reqBy = null)
        {
            try
            {
                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-2);

                }

                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<IssueRequisition> aList = new List<IssueRequisition>();
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@WareHouseId", warehouse));
                aParameters.Add(new SqlParameter("@RequisitionBy", reqBy));

                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetIssueRequisitionList", aParameters);
                while (dr.Read())
                {
                    IssueRequisition aMaster = new IssueRequisition();
                    aMaster.RequisitionMasterId = Convert.ToInt32(dr["RequisitionMasterId"]);
                    aMaster.RequisitionNo = dr["RequisitionNo"].ToString();
                    aMaster.Remarks = dr["Remarks"].ToString();


                    aMaster.RequisitionDate = Convert.ToDateTime(dr["RequisitionDate"]);
                    aMaster.RequiredDate = Convert.ToDateTime(dr["RequiredDate"]);
                    aMaster.RequiredTime = dr["RequiredTime"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.DepartmentName = dr["DepartmentName"].ToString();
                    aMaster.RequisitionBy = dr["RequisitionByName"].ToString();
                    aMaster.IsApprove = Convert.ToBoolean(dr["IsApprove"]);
                    aMaster.TotalRequisitionKg = Convert.ToDecimal(dr["RequisitionKg"]);
                    aMaster.TotalRequisitionQuantity = Convert.ToDecimal(dr["RequisitionQty"]);



                    aList.Add(aMaster);
                }

                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<IssueRequisition> IssueRequisitionPrintList(DateTime? fromdate, DateTime? todate, int warehouse = 0, string reqBy = null)
        {
            try
            {
                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-2);

                }

                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<IssueRequisition> aList = new List<IssueRequisition>();
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@WareHouseId", warehouse));
                aParameters.Add(new SqlParameter("@RequisitionBy", reqBy));

                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetIssueRequisitionPrintList", aParameters);
                while (dr.Read())
                {
                    IssueRequisition aMaster = new IssueRequisition();
                    aMaster.RequisitionMasterId = Convert.ToInt32(dr["RequisitionMasterId"]);
                    aMaster.RequisitionNo = dr["RequisitionNo"].ToString();
                    aMaster.Remarks = dr["Remarks"].ToString();


                    aMaster.RequisitionDate = Convert.ToDateTime(dr["RequisitionDate"]);
                    aMaster.RequiredDate = Convert.ToDateTime(dr["RequiredDate"]);
                    aMaster.RequiredTime = dr["RequiredTime"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.DepartmentName = dr["DepartmentName"].ToString();
                    aMaster.RequisitionBy = dr["RequisitionByName"].ToString();
                    aMaster.IsApprove = Convert.ToBoolean(dr["IsApprove"]);
                    aMaster.TotalRequisitionKg = Convert.ToDecimal(dr["RequisitionKg"]);
                    aMaster.TotalRequisitionQuantity = Convert.ToDecimal(dr["RequisitionQty"]);



                    aList.Add(aMaster);
                }

                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<OtherStockIssueMaster> IssueList(DateTime? fromdate, DateTime? todate, int warehouse = 0, string reqBy = null)
        {
            try
            {
                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-7);

                }

                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<OtherStockIssueMaster> aList = new List<OtherStockIssueMaster>();
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@WareHouseId", warehouse));
                aParameters.Add(new SqlParameter("@IssueBy", reqBy));

                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetIssueList", aParameters);
                while (dr.Read())
                {
                    OtherStockIssueMaster aMaster = new OtherStockIssueMaster();
                    aMaster.StockIssueMasterId = Convert.ToInt32(dr["StockIssueMasterId"]);

                    //aMaster.TotalIssueQuantity = Convert.ToInt32(dr["TotalIssueQuantity"]);
                    aMaster.TotalIssueQuantity = DBNull.Value.Equals(dr["TotalIssueQuantity"]) ? 0 : Convert.ToDecimal(dr["TotalIssueQuantity"]);
                    aMaster.IssueNo = dr["IssueNo"].ToString();

                    aMaster.IssueDate = Convert.ToDateTime(dr["IssueDate"]);
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.DepartmentName = dr["DepartmentName"].ToString();
                    aMaster.IssueBy = dr["IssueByName"].ToString();




                    aList.Add(aMaster);
                }

                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<IssueRequisition> IssueRequisitionApproveList(DateTime? fromdate, DateTime? todate, int warehouse = 0, string reqBy = null)
        {
            try
            {
                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-2);

                }

                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<IssueRequisition> aList = new List<IssueRequisition>();
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@WareHouseId", warehouse));
                aParameters.Add(new SqlParameter("@RequisitionBy", reqBy));

                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetIssueRequisitionApproveList", aParameters);
                while (dr.Read())
                {
                    IssueRequisition aMaster = new IssueRequisition();
                    aMaster.RequisitionMasterId = Convert.ToInt32(dr["RequisitionMasterId"]);
                    aMaster.RequisitionNo = dr["RequisitionNo"].ToString();
                    aMaster.Remarks = dr["Remarks"].ToString();
                    aMaster.RequiredDate = Convert.ToDateTime(dr["RequiredDate"]);
                    aMaster.RequiredTime = dr["RequiredTime"].ToString();
                    aMaster.RequisitionDate = Convert.ToDateTime(dr["RequisitionDate"]);
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.DepartmentName = dr["DepartmentName"].ToString();
                    aMaster.RequisitionBy = dr["RequisitionByName"].ToString();



                    aList.Add(aMaster);
                }

                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<IssueRequisitionDetails> RequisitionDetailsByIdForIssue(int id)
        {
            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<IssueRequisitionDetails> aList = new List<IssueRequisitionDetails>();
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@Id", id));

                SqlDataReader dr = _accessManager.GetSqlDataReader("GetIssueRequisitionByIdforIssue", aParameters);
                while (dr.Read())
                {
                    IssueRequisitionDetails aMaster = new IssueRequisitionDetails();




                    aMaster.ItemCode = dr["ItemCode"].ToString();
                    aMaster.ItemName = dr["ItemName"].ToString();
                    aMaster.ItemDescription = dr["ItemDescription"].ToString();
                    aMaster.Remarks = dr["Remarks"].ToString();
                    aMaster.UOMName = dr["UOMName"].ToString();
                    aMaster.StockInQty = (dr["StockInQty"] == DBNull.Value) ? 0 : ((decimal)dr["StockInQty"]);

                    aMaster.RequisitionQty = (dr["RequisitionQty"] == DBNull.Value) ? 0 : ((decimal)dr["RequisitionQty"]);
                    aMaster.ApprovedQty = (dr["ApprovedQty"] == DBNull.Value) ? 0 : ((decimal)dr["ApprovedQty"]);
                    aMaster.RequisitionKg = (dr["RequisitionKg"] == DBNull.Value) ? 0 : ((decimal)dr["RequisitionKg"]);
                    aMaster.RequisitionKg = (dr["RequisitionKg"] == DBNull.Value) ? 0 : ((decimal)dr["RequisitionKg"]);
                    aMaster.RemainingForIssueQty = (dr["RemainingForIssueQty"] == DBNull.Value) ? 0 : ((decimal)dr["RemainingForIssueQty"]);
                    aMaster.RemainingForIssueKg = (dr["RemainingForIssueKg"] == DBNull.Value) ? 0 : ((decimal)dr["RemainingForIssueKg"]);
                    aMaster.ItemId = Convert.ToInt32(dr["ItemId"]);
                    aMaster.PurposeID = Convert.ToInt32(dr["PurposeID"]);
                    aMaster.PurposeName =dr["PurposeName"].ToString();
                    aMaster.UOMId = Convert.ToInt32(dr["UOMId"]);
                    aMaster.RequisitionDetailsId = Convert.ToInt32(dr["RequisitionDetailsId"]);
                    aMaster.RequisitionMasterId = Convert.ToInt32(dr["RequisitionMasterId"]);
                    aMaster.IsFull = (dr["IsFull"] == DBNull.Value) ? false : ((bool)dr["IsFull"]);
                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<IssueRequisitionDetails> RequisitionDetailsById(int id)
        {
            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<IssueRequisitionDetails> aList = new List<IssueRequisitionDetails>();
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@Id", id));

                SqlDataReader dr = _accessManager.GetSqlDataReader("GetIssueRequisitionById", aParameters);
                while (dr.Read())
                {
                    IssueRequisitionDetails aMaster = new IssueRequisitionDetails();




                    aMaster.ItemCode = dr["ItemCode"].ToString();
                    aMaster.ItemName = dr["ItemName"].ToString();
                    aMaster.ItemDescription = dr["ItemDescription"].ToString();
                    aMaster.Remarks = dr["Remarks"].ToString();
                    aMaster.UOMName = dr["UOMName"].ToString();
                    aMaster.StockInQty = (dr["StockInQty"] == DBNull.Value) ? 0 : ((decimal)dr["StockInQty"]);
                    aMaster.RequisitionQty = (dr["RequisitionQty"] == DBNull.Value) ? 0 : ((decimal)dr["RequisitionQty"]);
                    aMaster.ApprovedQty = (dr["ApprovedQty"] == DBNull.Value) ? 0 : ((decimal)dr["ApprovedQty"]);
                    aMaster.RequisitionKg = (dr["RequisitionKg"] == DBNull.Value) ? 0 : ((decimal)dr["RequisitionKg"]);
                    aMaster.RequisitionKg = (dr["RequisitionKg"] == DBNull.Value) ? 0 : ((decimal)dr["RequisitionKg"]);
                    aMaster.RemainingForIssueQty = (dr["RemainingForIssueQty"] == DBNull.Value) ? 0 : ((decimal)dr["RemainingForIssueQty"]);
                    aMaster.RemainingForIssueKg = (dr["RemainingForIssueKg"] == DBNull.Value) ? 0 : ((decimal)dr["RemainingForIssueKg"]);
                    aMaster.ItemId = Convert.ToInt32(dr["ItemId"]);
                    aMaster.PurposeID = Convert.ToInt32(dr["PurposeID"]);
                    aMaster.UOMId = Convert.ToInt32(dr["UOMId"]);
                    aMaster.RequisitionDetailsId = Convert.ToInt32(dr["RequisitionDetailsId"]);
                    aMaster.RequisitionMasterId = Convert.ToInt32(dr["RequisitionMasterId"]);
                    aMaster.IsFull = Convert.ToBoolean(dr["IsFull"]);
                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public IssueRequisition GetIssueRequisitionMasterById(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                IssueRequisition aMaster = new IssueRequisition();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@Id", id));

                SqlDataReader dr = _accessManager.GetSqlDataReader("GetIssueRequisitionMasterById", aParameters);
                while (dr.Read())
                {
                    aMaster.RequisitionMasterId = Convert.ToInt32(dr["RequisitionMasterId"]);
                    aMaster.CompanyId = Convert.ToInt32(dr["CompanyId"]);
                    aMaster.WarehouseId = Convert.ToInt32(dr["WarehouseId"]);
                    aMaster.LocationId = Convert.ToInt32(dr["LocationId"]);
                    aMaster.DepartmentId = Convert.ToInt32(dr["DepartmentId"]);
                    aMaster.ProjectId = (dr["ProjectId"] == DBNull.Value) ? 0 : ((int)dr["ProjectId"]);
                    aMaster.RequisitionDate = Convert.ToDateTime(dr["RequisitionDate"]);
                    aMaster.RequiredDate = Convert.ToDateTime(dr["RequiredDate"]);
                    aMaster.RequiredTime = dr["RequiredTime"].ToString();
                    aMaster.RequisitionNo = dr["RequisitionNo"].ToString();
                    aMaster.RequisitionBy = dr["RequisitionBy"].ToString();
                    aMaster.Remarks = dr["Remarks"].ToString();
                    aMaster.CreateBy = dr["CreateBy"].ToString();
                    aMaster.CreateDate = Convert.ToDateTime(dr["CreateDate"]);
                    aMaster.IsApprove = Convert.ToBoolean(dr["IsApprove"]);
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.DepartmentName = dr["DepartmentName"].ToString();
                    aMaster.RequisitionByName = dr["RequisitionByName"].ToString();
                    aMaster.ProjectName = dr["ProjectName"].ToString();
                }
                return aMaster;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<OtherStockIssueDetails> IssueDetailsById(int id)
        {
            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<OtherStockIssueDetails> aList = new List<OtherStockIssueDetails>();
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@Id", id));

                SqlDataReader dr = _accessManager.GetSqlDataReader("GetIssueDetailsById", aParameters);
                while (dr.Read())
                {
                    OtherStockIssueDetails aMaster = new OtherStockIssueDetails();




                    aMaster.ItemCode = dr["ItemCode"].ToString();
                    aMaster.ItemName = dr["ItemName"].ToString();
                    aMaster.Remarks = dr["Remarks"].ToString();
                    aMaster.UOMName = dr["UOMName"].ToString();

                    aMaster.IssueQty = (dr["IssueQty"] == DBNull.Value) ? 0 : ((decimal)dr["IssueQty"]);
                    aMaster.IssueKg = (dr["IssueKg"] == DBNull.Value) ? 0 : ((decimal)dr["IssueKg"]);

                    aMaster.ItemId = Convert.ToInt32(dr["ItemId"]);
                    aMaster.UOMId = Convert.ToInt32(dr["UOMId"]);
                    aMaster.StockIssueDetailsId = Convert.ToInt32(dr["StockIssueDetailsId"]);
                    aMaster.StockIssueMasterId = Convert.ToInt32(dr["StockIssueMasterId"]);

                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetRequisitionReport(int location, int warehouse, DateTime fromdate, DateTime todate, int rstatus)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();

                parameters.Add(new SqlParameter("@LocationId", location));
                parameters.Add(new SqlParameter("@WarehouseId", warehouse));
                parameters.Add(new SqlParameter("@FromDate", fromdate));
                parameters.Add(new SqlParameter("@ToDate", todate));
                parameters.Add(new SqlParameter("@Status", rstatus));

                DataTable dr = _accessManager.GetDataTable("Rpt_IssueRequisitionListRpt", parameters);
                return dr;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public DataTable GetRequisitionInvoiceData(string ids)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();

                parameters.Add(new SqlParameter("@ids", ids));
                DataTable dt = _accessManager.GetDataTable("rpt_IssueRequisitionPrint", parameters);
                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public DataTable IssuePrintData(string ids)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();

                parameters.Add(new SqlParameter("@ids", ids));
                DataTable dt = _accessManager.GetDataTable("rpt_IssuePrint", parameters);
                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse Reject(int id)
        {
            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                ResultResponse aResponse = new ResultResponse();
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@MasterId", id));
                aResponse.isSuccess = _accessManager.SaveData("RejectIssueRequisition", aSqlParameterList);

                if (aResponse.isSuccess == false)
                {
                    _accessManager.SqlConnectionClose(true);
                }
                return aResponse;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public List<RequisitionPurpose> GetRequisitionPurpose()
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();

                SqlDataReader dr = _accessManager.GetSqlDataReader("GetAllRequisitionPurpose");
                List<RequisitionPurpose> _divList = new List<RequisitionPurpose>();
                while (dr.Read())
                {
                    RequisitionPurpose aDivision = new RequisitionPurpose();
                    aDivision.PurposeID = Convert.ToInt32(dr["PurposeID"]);
                    aDivision.PurposeName = dr["PurposeName"].ToString();
                    _divList.Add(aDivision);
                }
                return _divList;


            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }



        public OtherStockIssueMaster GetIssueInfoByMasterById(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                OtherStockIssueMaster aMaster = new OtherStockIssueMaster();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@Id", id));
                SqlDataReader dr = _accessManager.GetSqlDataReader("GetIssueMasterById", aParameters);
                while (dr.Read())
                {
                    aMaster.StockIssueMasterId = Convert.ToInt32(dr["StockIssueMasterId"]);

                    aMaster.LocationId = Convert.ToInt32(dr["LocationId"]);
                    aMaster.DepartmentId = Convert.ToInt32(dr["DepartmentId"]);
                    aMaster.WarehouseId = Convert.ToInt32(dr["WarehouseId"]);
                    aMaster.IssueNo = dr["IssueNo"].ToString();
                    aMaster.IssueDate = Convert.ToDateTime(dr["IssueDate"]);


                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.DepartmentName = dr["DepartmentName"].ToString();
                    aMaster.IssueBy = dr["EmpName"].ToString();
                }
                return aMaster;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<OtherStockIssueDetails> IssueDetailsForReturnById(int id)
        {
            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<OtherStockIssueDetails> aList = new List<OtherStockIssueDetails>();
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@Id", id));

                SqlDataReader dr = _accessManager.GetSqlDataReader("getIssueDetailsforReturnById", aParameters);
                while (dr.Read())
                {
                    OtherStockIssueDetails aMaster = new OtherStockIssueDetails();

                    aMaster.ItemCode = dr["ItemCode"].ToString();
                    aMaster.ItemName = dr["ItemName"].ToString();
                    aMaster.Remarks = dr["Remarks"].ToString();
                    aMaster.UOMName = dr["UOMName"].ToString();

                    aMaster.IssueQty = (dr["IssueQty"] == DBNull.Value) ? 0 : ((decimal)dr["IssueQty"]);
                    aMaster.IssueKg = (dr["IssueKg"] == DBNull.Value) ? 0 : ((decimal)dr["IssueKg"]);

                    aMaster.ItemId = Convert.ToInt32(dr["ItemId"]);
                    aMaster.UOMId = Convert.ToInt32(dr["UOMId"]);
                    aMaster.StockIssueDetailsId = Convert.ToInt32(dr["StockIssueDetailsId"]);
                    aMaster.StockIssueMasterId = Convert.ToInt32(dr["StockIssueMasterId"]);
                    aMaster.ReturnQty = (dr["ReturnQty"] == DBNull.Value) ? 0 : ((decimal)dr["ReturnQty"]);
                    aMaster.RemainingQuanity = (dr["RemainingQuanity"] == DBNull.Value) ? 0 : ((decimal)dr["RemainingQuanity"]);
                    aMaster.StockInDetailsId = (dr["StockInDetailsId"] == DBNull.Value) ? 0 : ( Convert.ToInt32(dr["StockInDetailsId"]));

                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse SaveIssueReturn(OtherStockIssueMaster aMaster, string user)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@IssueReturnID", aMaster.IssueReturnID));
                aParameters.Add(new SqlParameter("@StockIssueMasterId", aMaster.StockIssueMasterId));
                aParameters.Add(new SqlParameter("@WarehouseId", aMaster.WarehouseId));
                aParameters.Add(new SqlParameter("@ReturnDate", DateTime.Now.Date));
                aParameters.Add(new SqlParameter("@LocationId", aMaster.LocationId));
                aParameters.Add(new SqlParameter("@Remarks", aMaster.Remarks));
                aParameters.Add(new SqlParameter("@TotalReturnQty", aMaster.TotalReturnQty));
                aParameters.Add(new SqlParameter("@CreateBy", user));
                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_SaveIssueReturn", aParameters);
                if (aResponse.pk > 0)
                {
                    if (aMaster.StockIssueDetails.Count > 0)
                    {
                        aResponse.isSuccess = SaveIssueReturnDetails(aMaster.StockIssueDetails, aResponse.pk);
                    }
                    //aResponse.isSuccess = MrrReturnSystemJournal(aResponse.pk);
                    //aResponse.isSuccess = SaveOtherStockOut(aResponse.pk, aMaster.ReturnDetails);
                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                }
                else
                {
                    _accessManager.SqlConnectionClose(true);
                }
                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        private bool SaveIssueReturnDetails(List<OtherStockIssueDetails> aMaster, int aResponsePk)
        {
            try
            {
                bool result = false;
                foreach (var item in aMaster)
                {
                    if (item.ReturnQty > 0)
                    {
                        List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                        ResultResponse aResponse = new ResultResponse();
                        aSqlParameterList.Add(new SqlParameter("@IssueReturnID", aResponsePk));
                        aSqlParameterList.Add(new SqlParameter("@ItemId", item.ItemId));
                        aSqlParameterList.Add(new SqlParameter("@IssueQty", item.IssueQty));
                        aSqlParameterList.Add(new SqlParameter("@ReturnQty", item.ReturnQty));
                        aSqlParameterList.Add(new SqlParameter("@StockInDetailsId", item.StockInDetailsId));

                        aResponse.pk =
                            _accessManager.SaveDataReturnPrimaryKey("sp_SaveIssueReturnDetails", aSqlParameterList);
                    }
                    //else
                    //{
                    //    break;
                    //}
                }
                return result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool MrrReturnSystemJournal(int returnMasterId)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ReturnMasterId", returnMasterId));
                result = _accessManager.SaveData("SavReturnMrrJournal", aParameters);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<OtherStockIssueMaster> ReturnedIssueList(DateTime? fromdate, DateTime? todate, int wareHouseId = 0, int locationId = 0)
        {
            try
            {
                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-2);
                }
                if (todate == null)
                {
                    todate = System.DateTime.Now;
                }
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<OtherStockIssueMaster> aList = new List<OtherStockIssueMaster>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@wareHouseId", wareHouseId));
                aParameters.Add(new SqlParameter("@locationId", locationId));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_ReturnedIssueList", aParameters);
                while (dr.Read())
                {
                    OtherStockIssueMaster aMaster = new OtherStockIssueMaster();
                    aMaster.IssueReturnID = Convert.ToInt32(dr["IssueReturnID"]);
                    aMaster.ReturnNo = dr["ReturnNo"].ToString();
                    aMaster.IssueNo = dr["IssueNo"].ToString();
                    aMaster.ReturnDate = Convert.ToDateTime(dr["ReturnDate"]);
                    aMaster.Remarks = dr["Remarks"].ToString();
                    aMaster.CreateBy = dr["CreateBy"].ToString();
                    
                    aMaster.LocationName = dr["LocationName"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();                    
                    aMaster.ReturnQty = Convert.ToDecimal(dr["ReturnQty"]);

                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public List<OtherStockIssueMaster> GetLocationByWarehouseId(int warehouseId)  
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<OtherStockIssueMaster> _List = new List<OtherStockIssueMaster>();
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@warehouseId", warehouseId));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetLocationDetailsByWarehouseId", aSqlParameterList);
                while (dr.Read())
                {
                    OtherStockIssueMaster aInfo = new OtherStockIssueMaster();
                    aInfo.LocationId = Convert.ToInt32(dr["LocationId"]);
                    aInfo.LocationName = dr["LocationName"].ToString();

                    _List.Add(aInfo);
                }
                return _List;

            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public List<OtherStockIssueDetails> GetReturnedDetailsById(int id) 
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<OtherStockIssueDetails> aList = new List<OtherStockIssueDetails>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@Id", id));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetIssueReturnedDetailsById", aParameters);
                while (dr.Read())
                {
                    OtherStockIssueDetails aMaster = new OtherStockIssueDetails(); 
                    aMaster.ItemName = dr["ItemName"].ToString();
                    aMaster.UOMName = dr["UOMName"].ToString();
                    aMaster.ReturnQty = Convert.ToDecimal(dr["ReturnQty"]);
                    aMaster.IssueQty = Convert.ToDecimal(dr["IssueQty"]);
                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<OtherStockIssueDetails> loadMaterialIssueSummaryReport(string stockMonth, int departmentId, int locationId, int wareHouseId, int groupId)

        {
            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<OtherStockIssueDetails> aList = new List<OtherStockIssueDetails>();
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@stockMonth", stockMonth));
                aParameters.Add(new SqlParameter("@departmentId", departmentId));
                aParameters.Add(new SqlParameter("@locationId", locationId));
                aParameters.Add(new SqlParameter("@wareHouseId", wareHouseId));
                aParameters.Add(new SqlParameter("@groupId", groupId));
            
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_RPTGetMaterialIssueSummaryReport", aParameters);
                while (dr.Read())
                {
                    OtherStockIssueDetails aMaster = new OtherStockIssueDetails();
                    aMaster.ItemId = Convert.ToInt32(dr["ItemId"]);
                    aMaster.ItemName = dr["ItemName"].ToString();
                    aMaster.Unit = dr["Unit"].ToString();
                    aMaster.IssueQty = Convert.ToDecimal(dr["IssueQty"]);
                    aMaster.avgRate = Convert.ToDecimal(dr["avgRate"]);
                    aMaster.TotalAmount = Convert.ToDecimal(dr["TotalAmount"]);
                    aMaster.ItemGroupName = dr["ItemGroupName"].ToString();

                    aList.Add(aMaster);
                }

                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<OtherStockIssueDetails> LoadMaterialIssueDetails(string stockMonth, int departmentId, int locationId, int wareHouseId, int groupId, int ItemId)

        {
            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<OtherStockIssueDetails> aList = new List<OtherStockIssueDetails>();
                List<SqlParameter> aParameters = new List<SqlParameter>();

                aParameters.Add(new SqlParameter("@stockMonth", stockMonth));
                aParameters.Add(new SqlParameter("@departmentId", departmentId));
                aParameters.Add(new SqlParameter("@locationId", locationId));
                aParameters.Add(new SqlParameter("@wareHouseId", wareHouseId));
                aParameters.Add(new SqlParameter("@groupId", groupId));
                aParameters.Add(new SqlParameter("@ItemId", ItemId));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetMaterialIssueDetailsReport", aParameters);
                while (dr.Read())
                {
                    OtherStockIssueDetails aMaster = new OtherStockIssueDetails();
                    aMaster.IssueNo = dr["IssueNo"].ToString();
                    aMaster.IssueDate = dr["IssueDate"].ToString();
                    aMaster.ItemName = dr["ItemName"].ToString();
                    aMaster.Unit = dr["Unit"].ToString();
                    aMaster.IssueQty = Convert.ToDecimal(dr["IssueQty"]);
                    aMaster.DepartmentName = dr["DepartmentName"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();

                    aList.Add(aMaster);
                }

                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

    }
}