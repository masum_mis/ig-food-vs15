﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using IGFoodProject.DataManager;
using IGFoodProject.Models;
using IGFoodProject.ViewModel;

namespace IGFoodProject.DAL
{
    public class TradeSalesChallanDAL
    {
        private  DataAccessManager _accessManager = new DataAccessManager();
        public List<SalesOrderMasterTrade> GetSalesOrderProcessedForChallan()
        {
            try

            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SalesOrderMasterTrade> aList = new List<SalesOrderMasterTrade>();
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_TradeSalesOrderProcesedListForClallan");
                while (dr.Read())
                {
                    SalesOrderMasterTrade aMaster = new SalesOrderMasterTrade();

                    aMaster.TradeSalesOrderMasterId = Convert.ToInt32(dr["TradeSalesOrderMasterId"]);
                    aMaster.TotatalOrderAmtKg = Convert.ToDecimal(dr["TotatalOrderAmtKg"]);
                    aMaster.TotatalOrderAmtQty = Convert.ToDecimal(dr["TotatalOrderAmtQty"]);
                    aMaster.TotalDue = Convert.ToDecimal(dr["TotalDue"]);
                    aMaster.TotalDiscount = Convert.ToDecimal(dr["TotalDiscount"]);
                    aMaster.SalesPerson = dr["SalesPerson"].ToString();
                    aMaster.SalesOrderNo = dr["SalesOrderNo"].ToString();
                    aMaster.CustomerName = dr["CustomerName"].ToString();
                    aMaster.DeliveryDestination = dr["DeliveryDestination"].ToString();
                    aMaster.SalesPersonContact = dr["SalesPersonContact"].ToString();
                    aMaster.CreateBy = dr["CreateBy"].ToString();
                    aMaster.ApproveBy = dr["ApproveBy"].ToString();
                    aMaster.ApproveDate = dr["ApproveDate"] as DateTime?;
                    aMaster.CreateDate = dr["CreateDate"] as DateTime?;
                    aMaster.OrderDate = dr["OrderDate"] as DateTime?;
                    aMaster.DeliveryDate = dr["DeliveryDate"] as DateTime?;
                    if (DBNull.Value == dr["IsApprove"])
                    {
                        aMaster.IsApprove = 0;
                    }
                    else
                    {
                        aMaster.IsApprove = Convert.ToInt32(dr["IsApprove"]);
                    }

                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public ViewProductDetailsSales GetProductDetailsForChallan(int id, int wareHouseId,DateTime challanDate)
        {
            try
            {
                ViewProductDetailsSales aDetails = new ViewProductDetailsSales();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                aParameters.Add(new SqlParameter("@wareHouseId", wareHouseId));
                aParameters.Add(new SqlParameter("@challanDate", challanDate));
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetProductStockDetailsForTradeChallan", aParameters);
                while (dr.Read())
                {
                    aDetails.ProductID = Convert.ToInt32(dr["ProductID"]);
                    aDetails.StockQty = DBNull.Value.Equals(dr["StockInQty"]) ? 0 : Convert.ToDecimal(dr["StockInQty"]);
                    aDetails.StockKg = DBNull.Value.Equals(dr["StockInKG"]) ? 0 : Convert.ToDecimal(dr["StockInKG"]);
                    aDetails.groupid = Convert.ToInt32(dr["GroupId"]);

                }
                return aDetails;
            }
            catch (Exception ex)
            {

                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse SaveSalesOrderProcessedChallan(TradeSalesChallanMaster aMaster, string user)
        {
            try
            {

                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CompanyId", aMaster.CompanyId));
                aParameters.Add(new SqlParameter("@TradeSalesOrderMasterId", aMaster.TradeSalesOrderMasterId));
                aParameters.Add(new SqlParameter("@DriverName", aMaster.DriverName));
                aParameters.Add(new SqlParameter("@DeliveryBy", aMaster.DeliveryBy));
                aParameters.Add(new SqlParameter("@VehicleId", aMaster.VehicleId));
                aParameters.Add(new SqlParameter("@WarehouseId", aMaster.WarehouseId));
                aParameters.Add(new SqlParameter("@PaymentType", aMaster.PaymentType));
                aParameters.Add(new SqlParameter("@TotalChallanAmount", aMaster.TotalChallanAmount));
                aParameters.Add(new SqlParameter("@TotalChallanKg", aMaster.TotalChallanKg));
                aParameters.Add(new SqlParameter("@TotalChallanQty", aMaster.TotalChallanQty));
                aParameters.Add(new SqlParameter("@ChallanDate", aMaster.ChallanDate));
                aParameters.Add(new SqlParameter("@CreateDate", System.DateTime.Now));
                aParameters.Add(new SqlParameter("@DeliveryDate", aMaster.DeliveryDate));
                aParameters.Add(new SqlParameter("@CreateBy", user));
                aParameters.Add(new SqlParameter("@Remarks", aMaster.Remarks));

                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_SaveTradeChalllanMaster", aParameters);


                if (aResponse.pk > 0)
                {


                    foreach (var item in aMaster.ATradeChallanDetails)
                    {
                        List<SqlParameter> aParametersd = new List<SqlParameter>();
                        aParametersd.Add(new SqlParameter("@TradeSalesOrderDetailsId", item.TradeSalesOrderDetailsId));
                        aParametersd.Add(new SqlParameter("@TradeChallanMasterId", aResponse.pk));
                        aParametersd.Add(new SqlParameter("@IssueKg", item.IssueKg));
                        aParametersd.Add(new SqlParameter("@IssuePrice", item.IssuePrice));
                        aParametersd.Add(new SqlParameter("@IssueQty", item.IssueQty));
                        aParametersd.Add(new SqlParameter("@ProductId", item.ProductId));
                        aParametersd.Add(new SqlParameter("@WarehouseId", aMaster.WarehouseId));

                        aResponse.isSuccess = _accessManager.SaveData("sp_SaveTradeChallanDetails", aParametersd);
                    }

                    if (aResponse.isSuccess)
                    {
                        List<SqlParameter> alSqlParameters = new List<SqlParameter>();
                        alSqlParameters.Add(new SqlParameter("@IsChallanGen", 1));
                        alSqlParameters.Add(new SqlParameter("@SalesOrderId", aMaster.TradeSalesOrderMasterId));
                        aResponse.isSuccess = _accessManager.SaveData("sp_UpdateTradeOrderForChallan", alSqlParameters);
                    }
                 aResponse.isSuccess = SaveTradeChallanVoucher(aResponse.pk);
                }

                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<ViewGeneratedChallan> GetGeneratedChallanPrintList(DateTime fromDate, DateTime toDate, int? customerType, int? customer, int? location, int? warehouse)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@challanDateFrom", fromDate));
                aParameters.Add(new SqlParameter("@challanDateTo", toDate));
                aParameters.Add(new SqlParameter("@customerType", customerType));
                aParameters.Add(new SqlParameter("@customer", customer));
                aParameters.Add(new SqlParameter("@location", location));
                aParameters.Add(new SqlParameter("@warehouse", warehouse));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_GeneratedTradeChallan_filter", aParameters);

                List<ViewGeneratedChallan> aList = new List<ViewGeneratedChallan>();
                while (dr.Read())
                {
                    ViewGeneratedChallan aChallan = new ViewGeneratedChallan();
                    aChallan.SalesOrderChallanMasterId = Convert.ToInt32(dr["SalesOrderChallanMasterId"]);
                    aChallan.SalesOrderChallanNo = dr["SalesOrderChallanNo"].ToString();
                    aChallan.DeliveryDestination = dr["DeliveryDestination"].ToString();
                    aChallan.SalesOrderNo = dr["SalesOrderNo"].ToString();
                    aChallan.OrderBy = dr["OrderBy"].ToString();
                    aChallan.CreateBy = dr["CreateBy"].ToString();
                    aChallan.ChallanAmount = Convert.ToDecimal(dr["ChallanAmount"].ToString());
                    aChallan.ChallanDate = dr["ChallanDate"] as DateTime?;
                    aChallan.OrderDate = dr["OrderDate"] as DateTime?;

                    aChallan.CustomerName = dr["CustomerName"].ToString();
                    aList.Add(aChallan);
                }
                return aList;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<ChallanListForDelivery> GetChallanListForDeliveryConfirm()
        {
            try

            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@groupid", 5));
                List<ChallanListForDelivery> aList = new List<ChallanListForDelivery>();
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_TradeChallanProcesedListForDelivery", aParameters);
                while (dr.Read())
                {
                    ChallanListForDelivery aMaster = new ChallanListForDelivery();

                    aMaster.SalesOrderChallanMasterId = Convert.ToInt32(dr["SalesOrderChallanMasterId"]);
                    aMaster.SalesOrderChallanNo = dr["SalesOrderChallanNo"].ToString();
                    aMaster.SalesOrderNo = dr["SalesOrderNo"].ToString();
                    if (dr["TotalChallanKG"] != DBNull.Value)
                    {
                        aMaster.TotalChallanKG = Convert.ToDecimal(dr["TotalChallanKG"]);
                    }

                    if (dr["TotalChallanQty"] != DBNull.Value)
                    {
                        aMaster.TotalChallanQty = Convert.ToDecimal(dr["TotalChallanQty"]);
                    }
                    if (dr["ChallanAmount"] != DBNull.Value)
                    {
                        aMaster.ChallanAmount = Convert.ToDecimal(dr["ChallanAmount"]);
                    }



                    aMaster.CustomerName = dr["CustomerName"].ToString();
                    aMaster.ChallanDate = dr["ChallanDate"].ToString();
                    aMaster.OrderDate = dr["OrderDate"].ToString();



                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public ViewChallanDetailForDelivery GetTradeChallanProcessedMaster(int id)
        {
            try
            {
                List<SqlParameter> aParameters = new List<SqlParameter>();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                aParameters.Add(new SqlParameter("@id", id));
                DataTable dt = _accessManager.GetDataTable("sp_GetTradeChallanProcessedMasterById", aParameters);
                List<ViewChallanDetailForDelivery> aList = Helper.ToListof<ViewChallanDetailForDelivery>(dt);
                aList[0].IsEmployee = true;
                return aList[0];
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<SalesOrderChallanDetails> GetTradeChallanDetailsByMaster(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                DataTable dt = _accessManager.GetDataTable("sp_Get_TradeChallanPrecessedDetailsByMaster", aParameters);
                List<SalesOrderChallanDetails> aList = Helper.ToListof<SalesOrderChallanDetails>(dt);
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public ResultResponse SaveTradeChallanDelivery(SalesOrderChallanMaster aMaster, string user)
        {
            try
            {
                bool result = false;
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@SalesOrderChallanMasterId", aMaster.SalesOrderChallanMasterId));

                aParameters.Add(new SqlParameter("@DeliveryAmount", aMaster.DeliveryAmount));
                aParameters.Add(new SqlParameter("@TotalDeliveryQty", aMaster.TotalDeliveryQty));
                aParameters.Add(new SqlParameter("@TotalDeliveryKG", aMaster.TotalDeliveryKG));
                aParameters.Add(new SqlParameter("@DeliveryDate", DateTime.Now));
                aParameters.Add(new SqlParameter("@Remarks", aMaster.Remarks));
                aParameters.Add(new SqlParameter("@DeliveryBy", aMaster.DeliveryBy));

                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_Save_TradeDeliveryConfirm", aParameters);


                if (aResponse.pk > 0)
                {
                    aResponse.isSuccess = SaveDeliveryDetailsProcessed(aMaster.ChallanDetails, aResponse.pk);

                    result = SaveAdvancedAdjust(aResponse.pk);
                  bool t = SaveReturnedTradeChallanVoucher(aResponse.pk);
                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                }

                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public bool SaveAdvancedAdjust(int pk)
        {
            try
            {
                bool result = false;
                //For Processed

                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@TradeChallanMasterId", pk));

                result = _accessManager.SaveData("sp_SaveAdvancedAdjustAmount_trade", aParameters);



                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {

            }
        }

        public bool SaveDeliveryDetailsProcessed(List<SalesOrderChallanDetails> challanList, int pk)
        {
            try
            {
                bool result = false;
                //For Processed
                foreach (var item in challanList)
                {
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@SalesOrderChallanDetailsId", item.SalesOrderChallanDetailsId));

                    aParameters.Add(new SqlParameter("@DeliveryKG", item.DeliveryKG));
                    aParameters.Add(new SqlParameter("@DeliveryAmount", item.DeliveryAmount));
                    aParameters.Add(new SqlParameter("@DeliveryQty", item.DeliveryQty));
                    aParameters.Add(new SqlParameter("@DeliveryRemarks", item.DeliveryRemarks));
                    aParameters.Add(new SqlParameter("@SalePriceDelivery", item.SaleRate));

                    aParameters.Add(new SqlParameter("@ReturnQty", item.ReturnQty));
                    aParameters.Add(new SqlParameter("@ReturnKG", item.ReturnKG));
                    aParameters.Add(new SqlParameter("@WeightLossQty", item.WeightLossQty));
                    aParameters.Add(new SqlParameter("@WeightLossKG", item.WeightLossKG));

                    result = _accessManager.SaveData("sp_SaveTradeDeliveryDetails", aParameters);

                    if (result == false)
                    {
                        break;
                    }

                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {

            }
        }

        public DataTable TradeChallanReportReport(int locationId,int wareHouseId,string salesperson,DateTime? orderFromDate,DateTime? orderToDate, DateTime? challanFromDate,DateTime? challanToDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@LocationId", locationId));
                aParameters.Add(new SqlParameter("@WarehouseId", wareHouseId));
                aParameters.Add(new SqlParameter("@SalesPerson", salesperson));
                aParameters.Add(new SqlParameter("@OrderFromDate", orderFromDate));
                aParameters.Add(new SqlParameter("@OrderToDate", orderToDate));
                aParameters.Add(new SqlParameter("@ChallanFromDate", challanFromDate));
                aParameters.Add(new SqlParameter("@challanToDate", challanToDate));
                
                DataTable dt = _accessManager.GetDataTable("sp_GetChallanListFReport", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
                _accessManager.SqlConnectionClose();
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public DataTable TradeChallanReturn( DateTime orderFromDate, DateTime orderToDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@FromDate", orderFromDate));
                aParameters.Add(new SqlParameter("@ToDate", orderToDate));

                DataTable dt = _accessManager.GetDataTable("sp_GetAllTradeChallanFReturn", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
                _accessManager.SqlConnectionClose();
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public DataTable TradeChallanReturnFChallanId( int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ChallanId", id));

                DataTable dt = _accessManager.GetDataTable("sp_GetChallanDataByChallanId", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
                _accessManager.SqlConnectionClose();
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }


        public ResultResponse SaveTradeReturn(TradeReturnLog aMaster, string user)
        {
            try
            {
                bool result = false;
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@TradeChallanMasterId", aMaster.TradeChallanMasterId));
                aParameters.Add(new SqlParameter("@TradeChallanDetailsId", aMaster.TradeChallanDetailsId));
                aParameters.Add(new SqlParameter("@ProductId", aMaster.ProductId));
                aParameters.Add(new SqlParameter("@ReturnQty", aMaster.ReturnQty));
                aParameters.Add(new SqlParameter("@ReturnKg", aMaster.ReturnKg));
                aParameters.Add(new SqlParameter("@WeightLossQty", aMaster.WeightLossQty));
                aParameters.Add(new SqlParameter("@WeightLossKg", aMaster.WeightLossKg));
                aParameters.Add(new SqlParameter("@ReturnDate", aMaster.ReturnDate));
                aParameters.Add(new SqlParameter("@CreateBy", user));
                aParameters.Add(new SqlParameter("@CreateDate", DateTime.Now));
                aParameters.Add(new SqlParameter("@RequestedWarehouse", aMaster.RequestedWarehouse));
                aResponse.isSuccess = _accessManager.SaveData("sp_SaveTradeReturn", aParameters);

                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<SalesOrderChallanMaster> GetChallanList(DateTime challanDate)
        {

            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SalesOrderChallanMaster> _List = new List<SalesOrderChallanMaster>();
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@ChallanDate", challanDate));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetTradeChallanNoFQc", aSqlParameterList);
                while (dr.Read())
                {
                    SalesOrderChallanMaster aInfo = new SalesOrderChallanMaster();
                    aInfo.SalesOrderChallanMasterId = Convert.ToInt32(dr["TradeChallanMasterId"]);
                    aInfo.SalesOrderChallanNo = dr["SalesOrderChallanNo"].ToString();

                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetRemainingQcRequestedProductList(int warehouseId, int challanId, DateTime challlanDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@WarehouseId", warehouseId));
                aParameters.Add(new SqlParameter("@ChallanDate", challlanDate));
                aParameters.Add(new SqlParameter("@ChallanId", challanId));
                DataTable dt = _accessManager.GetDataTable("sp_GetQcRequestedproduct", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable GetRemainingtradeQcRequestedProductList(int challanId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ChallanId", challanId));
                DataTable dt = _accessManager.GetDataTable("sp_GetTradeQcRequestedproduct", aParameters);
                _accessManager.SqlConnectionClose();
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse SaveTradeQcOperation(QcOperationMaster aMaster)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@QcDate", aMaster.QcDate));
                aSqlParameterList.Add(new SqlParameter("@QcComments", aMaster.QcComments));
                aSqlParameterList.Add(new SqlParameter("@ForwordedWarehouseId", aMaster.ForwordedWarehouseId));
                aSqlParameterList.Add(new SqlParameter("@SalesOrderChallanMasterId", aMaster.SalesOrderChallanMasterId));
                aSqlParameterList.Add(new SqlParameter("@ChallanDate", aMaster.ChallanDate));
                aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@CreateDate", aMaster.CreateDate));
                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_SaveTradeQcOperationMaster", aSqlParameterList);

                List<SqlParameter> sSqlParameterList = new List<SqlParameter>();
                sSqlParameterList.Add(new SqlParameter("@StockInDate", aMaster.QcDate));
                sSqlParameterList.Add(new SqlParameter("@StockInBy", aMaster.CreateBy));
                sSqlParameterList.Add(new SqlParameter("@WareHouseId", aMaster.ForwordedWarehouseId));
                sSqlParameterList.Add(new SqlParameter("@Comments", aMaster.QcComments));
                sSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                sSqlParameterList.Add(new SqlParameter("@CreateDate", aMaster.CreateDate));
                sSqlParameterList.Add(new SqlParameter("@IsOpening", 8));
                var stockId = _accessManager.SaveDataReturnPrimaryKey("sp_SaveTradeStockInMasterForQc", sSqlParameterList);


                if (aResponse.pk > 0)
                {
                    foreach (var item in aMaster.QcOperationDetailses)
                    {
                        List<SqlParameter> sqlParameterList = new List<SqlParameter>();
                        sqlParameterList.Add(new SqlParameter("@QcOperationMasterId", aResponse.pk));
                        sqlParameterList.Add(new SqlParameter("@StockInId", stockId));
                        sqlParameterList.Add(new SqlParameter("@ProductId", item.ProductId));
                        sqlParameterList.Add(new SqlParameter("@ProductQty", item.ProductQty));
                        sqlParameterList.Add(new SqlParameter("@ProductKg", item.ProductKg));
                        sqlParameterList.Add(new SqlParameter("@StockInDetailID", item.StockInDetailID));
                        sqlParameterList.Add(new SqlParameter("@WeightlossQty", item.WeightlossQty));
                        sqlParameterList.Add(new SqlParameter("@WeightlossKg", item.WeightlossKg));
                        sqlParameterList.Add(new SqlParameter("@StockInDate", aMaster.QcDate));
                        sqlParameterList.Add(new SqlParameter("@IsOpening", 8));
                        sqlParameterList.Add(new SqlParameter("@CreateDate", aMaster.CreateDate));
                        sqlParameterList.Add(new SqlParameter("@unitprice", item.UnitCost));
                        sqlParameterList.Add(new SqlParameter("@SalesOrderChallanDetailsId", item.SalesOrderChallanDetailsId));
                        aResponse.isSuccess = _accessManager.SaveData("sp_SaveTradeQcOperationDetails", sqlParameterList);
                    }
                }

                return aResponse;
            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public DataTable GetTradeQcProductHistoryOfQty(int challanId, int productId,int challanDetails)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ChallanId", challanId));
                aParameters.Add(new SqlParameter("@ProductId", productId));
                aParameters.Add(new SqlParameter("@ChallanDetailsId", challanDetails));
                DataTable dt = _accessManager.GetDataTable("sp_GetTradeQcOperationHistoryOfQty", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public DataTable GetTradeQcProductHistoryOfKg(int challanId, int productId, int challanDetails)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ChallanId", challanId));
                aParameters.Add(new SqlParameter("@ProductId", productId));
                aParameters.Add(new SqlParameter("@challanDetails", challanDetails));
                DataTable dt = _accessManager.GetDataTable("sp_GetTradeQcOperationHistoryOfKg", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public double GetProdutCostPrice(int productId,DateTime challanDate)
        {
            try
            {
                var price = 0.0;
                DataTable dt = new DataTable();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@productid", productId));
                aParameters.Add(new SqlParameter("@challanDate", challanDate));
              
                    dt = _accessManager.GetDataTable("sp_GetTradeMatricPrice", aParameters);
               
                if (dt.Rows.Count > 0)
                {
                    price = Convert.ToDouble(DBNull.Value == dt.Rows[0].ItemArray[0] ? 0 : dt.Rows[0].ItemArray[0]);
                }
                else
                {
                    price = 0;
                }

                return price;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public dynamic GetProdutPreviousReturn(int detailsId)
        {
            try
            {
                DataTable dt = new DataTable();
                TradeReturnLog aLog= new TradeReturnLog();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@detailsId", detailsId));
                dt = _accessManager.GetDataTable("sp_GetPreviousReturn", aParameters);

                if (dt.Rows.Count > 0)
                {
                    aLog.ReturnQty = Convert.ToDecimal(dt.Rows[0].ItemArray[0]);
                    aLog.ReturnKg = Convert.ToDecimal(dt.Rows[0].ItemArray[1]);
                    aLog.WeightLossQty = Convert.ToDecimal(dt.Rows[0].ItemArray[2]);
                    aLog.WeightLossKg = Convert.ToDecimal(dt.Rows[0].ItemArray[3]);
                }

                return aLog;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public DataTable TradeConfirmedChallan(DateTime? fromDate, DateTime? toDate)
        {
            try
            {
                if(fromDate == null)
                    fromDate=DateTime.Now;

                if(toDate == null)
                    toDate=DateTime.Now;

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@fromDate", fromDate));
                aParameters.Add(new SqlParameter("@toDate", toDate));

                DataTable dt = _accessManager.GetDataTable("sp_GetTradeConfirmedChallan", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
                _accessManager.SqlConnectionClose();
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }

        private bool SaveTradeChallanVoucher(int aResponsePk)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ChallanMasterId", aResponsePk));
                result = _accessManager.SaveData("saveTradeChallanJournal", aParameters);

                return result;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private bool SaveReturnedTradeChallanVoucher(int aResponsePk)        {            try            {                bool result = false;                List<SqlParameter> aParameters = new List<SqlParameter>();                aParameters.Add(new SqlParameter("@ChallanMasterId", aResponsePk));                result = _accessManager.SaveData("saveTradeChallanReturnJournal", aParameters);                return result;            }            catch (Exception ex)            {                throw ex;            }        }
    }
}