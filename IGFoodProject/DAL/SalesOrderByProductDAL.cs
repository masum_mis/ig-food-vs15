﻿using IGFoodProject.DataManager;
using IGFoodProject.Models;
using IGFoodProject.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IGFoodProject.DAL
{
    public class SalesOrderByProductDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();


        public string GetSalesPersonPhone(int empNo)
        {
            try
            {
                string PhoneNo = "";
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@empNo", empNo));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_SalesPersonPhoneNo", aParameters);
                while (dr.Read())
                {
                    PhoneNo = dr["PhoneNo"].ToString();
                }
                return PhoneNo;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public ViewProductDetailsSales GetProductDetails(int id)
        {
            try
            {
                ViewProductDetailsSales aDetails = new ViewProductDetailsSales();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                //aParameters.Add(new SqlParameter("@warehiuseid", wareHouseId));
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_ProductStockDetailsByProduct", aParameters);
                while (dr.Read())
                {
                    aDetails.ProductID = Convert.ToInt32(dr["ProductID"]);
                    aDetails.StockQty = Convert.ToDecimal(dr["StockQty"]);
                    aDetails.StockPrice = Convert.ToDecimal(dr["StockPrice"]);
                    aDetails.StockKg = Convert.ToDecimal(dr["StockKg"]);
                    aDetails.CategoryName = dr["CategoryName"].ToString();
                    aDetails.MRP = Convert.ToDecimal(dr["MRP"]);
                    aDetails.TPPrice = Convert.ToDecimal(dr["TPPrice"]);
                    aDetails.DPPrice = Convert.ToDecimal(dr["DPPrice"]);
                }
                return aDetails;
            }
            catch (Exception ex)
            {

                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public ViewProductDetailsSales GetProductDetails_Edit(int id, int groupid, int masterid)
        {
            try
            {
                ViewProductDetailsSales aDetails = new ViewProductDetailsSales();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                aParameters.Add(new SqlParameter("@groupid", groupid));
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_ProductStockDetailsFurtherProcessed", aParameters);
                while (dr.Read())
                {
                    aDetails.ProductID = Convert.ToInt32(dr["ProductID"]);
                    aDetails.StockQty = Convert.ToDecimal(dr["StockQty"]);
                    aDetails.StockPrice = Convert.ToDecimal(dr["StockPrice"]);
                    aDetails.StockKg = Convert.ToDecimal(dr["StockKg"]);
                    aDetails.CategoryName = dr["CategoryName"].ToString();
                    aDetails.MRP = Convert.ToDecimal(dr["MRP"]);
                    aDetails.TPPrice = Convert.ToDecimal(dr["TPPrice"]);
                    aDetails.DPPrice = Convert.ToDecimal(dr["DPPrice"]);
                }
                return aDetails;
            }
            catch (Exception ex)
            {

                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse SaveSalesOrderFurtherProcessed(SalesOrderProcessedMaster aMaster, string user)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                var addIn = aMaster.SalesDetails.Find(a => a.OperationIdAdd == 3); // Add In Challan if add in challan Is Approve =1
                if (addIn != null)
                {
                    aMaster.IsApprove = 1;
                }
                var deletIn = aMaster.SalesDetails.Find(a => a.OperationIdDelete == 3); // Delete from  Challan if delete in challan Is Approve =1
                if (deletIn != null)
                {
                    aMaster.IsApprove = 1;
                }
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CustomerId", aMaster.CustomerId));
                aParameters.Add(new SqlParameter("@OrderType", aMaster.OrderType));
                aParameters.Add(new SqlParameter("@SalesOrderProcessedMasterId", aMaster.SalesOrderProcessedMasterId));
                if (aMaster.IsEmployee == true)
                {
                    aParameters.Add(new SqlParameter("@SalesPerson", aMaster.SalesPerson.Split(':')[0].Trim()));
                }
                else
                {
                    aParameters.Add(new SqlParameter("@SalesPerson", aMaster.SalesPerson));
                }

                aParameters.Add(new SqlParameter("@OrderDate", aMaster.OrderDate));
                aParameters.Add(new SqlParameter("@DeliveryDestination", aMaster.DeliveryDestination));
                aParameters.Add(new SqlParameter("@CreateBy", user));
                aParameters.Add(new SqlParameter("@OrderBy", user));
                aParameters.Add(new SqlParameter("@CreateDate", System.DateTime.Now));
               
                aParameters.Add(new SqlParameter("@GroupId", aMaster.GroupId));
                //aParameters.Add(new SqlParameter("@PaymentType", aMaster.PaymentType));
                //aParameters.Add(new SqlParameter("@BankId", aMaster.BankId));
                //aParameters.Add(new SqlParameter("@BranchId", aMaster.BranchId));
                //aParameters.Add(new SqlParameter("@ChequeNo", aMaster.ChequeNo));
                //aParameters.Add(new SqlParameter("@ChequeDate", aMaster.ChequeDate));


                aParameters.Add(new SqlParameter("@CompanyId", aMaster.CompanyId));
                aParameters.Add(new SqlParameter("@SalesPersonContact", aMaster.SalesPersonContact));
                aParameters.Add(new SqlParameter("@DeliveryDate", aMaster.DeliveryDate));
                aParameters.Add(new SqlParameter("@TotatalOrderAmtKg", aMaster.TotatalOrderAmtKg));
                aParameters.Add(new SqlParameter("@TotalIncTaxVat", aMaster.TotalIncTaxVat));

                aParameters.Add(new SqlParameter("@TotalExTaxVat", aMaster.TotalExTaxVat));
                aParameters.Add(new SqlParameter("@TaxVat", aMaster.TaxVat));
               


                aParameters.Add(new SqlParameter("@collectiontypeid", aMaster.CollectionType));
                aParameters.Add(new SqlParameter("@collectionamount", aMaster.CollectionAmount));
                aParameters.Add(new SqlParameter("@IsEmployee", aMaster.IsEmployee));
                aParameters.Add(new SqlParameter("@CustomerTypeID", aMaster.CustomerTypeId));
                aParameters.Add(new SqlParameter("@IsApprove", aMaster.IsApprove));

                aParameters.Add(new SqlParameter("@ColBankId", aMaster.ColBankId));
                aParameters.Add(new SqlParameter("@ColBranchId", aMaster.ColBranchId));
                aParameters.Add(new SqlParameter("@RefNo", aMaster.RefNo));
                aParameters.Add(new SqlParameter("@RefDate", aMaster.RefDate));

                aResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_Save_SalesOrderProcessed", aParameters);
                if (aResponse.pk > 0)
                {
                    aResponse.isSuccess = SaveSalesOrderFurtherProcessedDetails(aMaster.SalesDetails, aResponse.pk, user,aMaster.IsApprove);

                    if (aResponse.isSuccess == false)
                    {
                        _accessManager.SqlConnectionClose(true);
                    }
                }
                return aResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public bool SaveSalesOrderFurtherProcessedDetails(List<SalesOrderProcessedDetails> aList, int pk, string user, int isApprove)
        {
            try
            {
                bool result = false;
                foreach (var item in aList)
                {
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    

                    aParameters.Add(new SqlParameter("@SalesOrderProcessedMasterId",pk ));
                    aParameters.Add(new SqlParameter("@SalesOrderProcessDetailsId", item.SalesOrderProcessDetailsId));
                    aParameters.Add(new SqlParameter("@ProductId", item.ProductId));
                    aParameters.Add(new SqlParameter("@ProductQty", item.ProductQty));
                    aParameters.Add(new SqlParameter("@ProductKg", item.ProductKg));
                    aParameters.Add(new SqlParameter("@StockRate", item.StockRate));
                    aParameters.Add(new SqlParameter("@SaleRate", item.SaleRate));
                    aParameters.Add(new SqlParameter("@DiscountAmount", item.DiscountAmount));
                    aParameters.Add(new SqlParameter("@DiscountPercent", item.DiscountPercent));
                    aParameters.Add(new SqlParameter("@CreateBy", user));
                    aParameters.Add(new SqlParameter("@CreateDate", DateTime.Now));
                    aParameters.Add(new SqlParameter("@TotalPrice", item.TotalPrice));
                    aParameters.Add(new SqlParameter("@IsDelete", item.IsDelete));
                    aParameters.Add(new SqlParameter("@isApprove", isApprove));
                    aParameters.Add(new SqlParameter("@OperationIdAdd", item.OperationIdAdd));
                    aParameters.Add(new SqlParameter("@OperationIdDelete", item.OperationIdDelete));
                    result = _accessManager.SaveData("sp_Save_SavelOrderProcessedDetails", aParameters);
                    if (result == false)
                    {
                        break;
                    }

                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<ViewSalesOrderProcessedMaster> GetSalesOrderByProductMaster()
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<ViewSalesOrderProcessedMaster> aList = new List<ViewSalesOrderProcessedMaster>();

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_SalesOrderByProductList");
                while (dr.Read())
                {
                    ViewSalesOrderProcessedMaster aMaster = new ViewSalesOrderProcessedMaster();

                    aMaster.SalesOrderProcessedMasterId = Convert.ToInt32(dr["SalesOrderProcessedMasterId"]);
                    aMaster.TotatalOrderAmtKg = Convert.ToDecimal(dr["TotatalOrderAmtKg"]);
                    aMaster.TotalIncTaxVat = Convert.ToDecimal(dr["TotalIncTaxVat"]);
                    aMaster.TotalExTaxVat = Convert.ToDecimal(dr["TotalExTaxVat"]);

                    aMaster.SalesPerson = dr["SalesPerson"].ToString();
                    aMaster.SalesOrderNo = dr["SalesOrderNo"].ToString();
                    aMaster.CustomerName = dr["CustomerName"].ToString();
                    aMaster.DeliveryDestination = dr["DeliveryDestination"].ToString();
                    aMaster.SalesPersonContact = dr["SalesPersonContact"].ToString();
                    aMaster.SalesPerson = dr["SalesPerson"].ToString();
                    aMaster.CreateBy = dr["CreateBy"].ToString();
                    aMaster.ApproveBy = dr["ApproveBy"].ToString();
                    aMaster.ApproveDate = dr["ApproveDate"] as DateTime?;
                    aMaster.CreateDate = dr["CreateDate"] as DateTime?;
                    aMaster.OrderDate = dr["OrderDate"] as DateTime?;
                    aMaster.DeliveryDate = dr["DeliveryDate"] as DateTime?;
                    if (DBNull.Value == dr["IsApprove"])
                    {
                        aMaster.IsApprove = 0;
                    }
                    else
                    {
                        aMaster.IsApprove =Convert.ToInt32(dr["IsApprove"]);
                    }
                    if (DBNull.Value == dr["IsEmployee"])
                    {
                        aMaster.IsEmployee = null;
                    }
                    else
                    {
                        aMaster.IsEmployee = (bool)dr["IsEmployee"];
                    }
                    aMaster.CustomerType = dr["CoustomerTypeName"].ToString();
                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public SalesOrderProcessedMaster GetSalesOrderProcessedMaster(int id)
        {
            try
            {
                List<SqlParameter> aParameters = new List<SqlParameter>();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                aParameters.Add(new SqlParameter("@id", id));
                DataTable dt = _accessManager.GetDataTable("sp_Get_SalesOrderProcessedMasrerById", aParameters);
                List<SalesOrderProcessedMaster> aList = Helper.ToListof<SalesOrderProcessedMaster>(dt);
                return aList[0];
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public List<SalesOrderProcessedDetails> GetSalesOderProcessedDetailsByMaster(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                DataTable dt = _accessManager.GetDataTable("sp_Get_SalesOrderPrecessedDetailsByMaster", aParameters);
                List<SalesOrderProcessedDetails> aList = Helper.ToListof<SalesOrderProcessedDetails>(dt);
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public int GetLocationIdByWareHouse(int wareHouse)
        {
            try
            {
                int locationId = 0;
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@warehouseId", wareHouse));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_LocationIdByWarehouse", aParameters);
                while (dr.Read())
                {
                    locationId = Convert.ToInt32(dr["LocationId"]);
                }
                return locationId;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


        public ResultResponse ApproveSalesOrder(int id, bool status, string user)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                aParameters.Add(new SqlParameter("@status", status));
                aParameters.Add(new SqlParameter("@user", user));

                aResponse.isSuccess = _accessManager.SaveData("sp_Approve_SalesOrderProcessed", aParameters);
                return aResponse;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
    }
}