﻿using IGFoodProject.DataManager;
using IGFoodProject.Models.Barcode;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IGFoodProject.DAL.Barcode
{
    public class BarcodeDAL
    {
        DataAccessManager accessManager = new DataAccessManager();
        public List<BarcodeProduct> GetProductList()
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<BarcodeProduct> _List = new List<BarcodeProduct>();

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetActiveProductList");
                while (dr.Read())
                {
                    BarcodeProduct aInfo = new BarcodeProduct();
                    aInfo.ProductId = (int)dr["ProductId"];
                    aInfo.GroupId = (int)dr["GroupId"];
                    aInfo.TypeId = (int)dr["TypeId"];
                    aInfo.CategoryId = (int)dr["CategoryId"];
                    aInfo.PackSizeId = (int)dr["PackSizeId"];
                    aInfo.UOMId = (int)dr["UOMId"];


                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }


        public bool SaveBarcode(BarcodeProduct item)
        {
            bool result = false;
            try

            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);


                List<SqlParameter> gSqlParameterList = new List<SqlParameter>();

                gSqlParameterList.Add(new SqlParameter("@ProductId", item.ProductId));
                gSqlParameterList.Add(new SqlParameter("@Barcode", item.Barcode));
                gSqlParameterList.Add(new SqlParameter("@BarcodeImage", item.BarcodeImage));

                result = accessManager.SaveData("SP_SaveBarcode", gSqlParameterList);


            }
            catch (Exception exception)
            {
                accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
            return result;
        }

        public List<BarcodeProduct> GetProductListWithBarcode()
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<BarcodeProduct> _List = new List<BarcodeProduct>();
                

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetActiveProductList");
                while (dr.Read())
                {
                    BarcodeProduct aInfo = new BarcodeProduct();

                    aInfo.Name = dr["ProductName"].ToString();
                    aInfo.Barcode = dr["ProductNo"].ToString();
                    aInfo.ImageUrl = dr["BarCodeImage"] != null ? "data:image/jpg;base64," +
                        Convert.ToBase64String((byte[])dr["BarCodeImage"]) : "";


                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }



        public DataTable GetProductWithBarcode() 
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);                
                DataTable dt = accessManager.GetDataTable("sp_GetActiveProductList");
                return dt;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }




    }
}