﻿using IGFoodProject.DataManager;
using IGFoodProject.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IGFoodProject.DAL
{
    public class ProductInOutReportDAL
    {
        DataAccessManager accessManager = new DataAccessManager();
        public List<Product> GetProductList(int groupid, int typeid, int categoryid)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<Product> _List = new List<Product>();

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@GroupId", groupid));
                aSqlParameterList.Add(new SqlParameter("@TypeId", typeid));
                aSqlParameterList.Add(new SqlParameter("@CatogoryId", categoryid));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetProductForInOut", aSqlParameterList);
                while (dr.Read())
                {
                    Product aInfo = new Product();
                    aInfo.ProductId = (int)dr["ProductId"];

                    aInfo.ProductNo = dr["ProductNo"].ToString();
                    aInfo.ProductName = dr["ProductName"].ToString();



                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public DataTable ProductInOutReport(DateTime fromDate, DateTime toDate, int Company = 0, int Location = 0, int Warehouse = 0, int Group = 0, int Type = 0, int Category = 0, int ProductId = 0)
        {

            accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
            List<SqlParameter> aParameters = new List<SqlParameter>();
            aParameters.Add(new SqlParameter("@fromDate", fromDate));
            aParameters.Add(new SqlParameter("@toDate", toDate));
            aParameters.Add(new SqlParameter("@Company", Company));
            aParameters.Add(new SqlParameter("@Location", Location));
            aParameters.Add(new SqlParameter("@Warehouse", Warehouse));
            aParameters.Add(new SqlParameter("@Group", Group));
            aParameters.Add(new SqlParameter("@Type", Type));
            aParameters.Add(new SqlParameter("@Category", Category));
            aParameters.Add(new SqlParameter("@ProductId", ProductId));
          

            DataTable dt = new DataTable();
            
            dt = accessManager.GetDataTable("sp_GetProductDetailInOut", aParameters);
            


            return dt;
        }
    }
}