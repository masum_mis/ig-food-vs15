﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using IGFoodProject.Models;
using IGFoodProject.DataManager;
using System.Data.SqlClient;

namespace IGFoodProject.DAL
{
    public class MarketDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();

        public List<Market> GetMarketByTerritoryId(int territoryId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@territoryId", @territoryId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_Market", aParameters);
                List<Market> _marketList = new List<Market>();
                while (dr.Read())
                {
                    Market aMarket = new Market();
                    Territory aTerritory = new Territory();
                    aMarket.MarketId = Convert.ToInt32(dr["MarketId"]);
                    aMarket.MarketName = dr["MarketName"].ToString().Trim();
                    aTerritory.TerritoryName= dr["TerritoryName"].ToString().Trim();
                    aMarket.aTerritory = aTerritory;
                    _marketList.Add(aMarket);
                }
                return _marketList;


            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public Market GetMarketByMarketId(int marketId=0)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@marketId", marketId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_MarketByMarketId", aParameters);
                Market aMarket = new Market();
                while (dr.Read())
                {
                    aMarket.MarketId = Convert.ToInt32(dr["MarketId"]);
                    aMarket.MarketName = dr["MarketName"].ToString().Trim();
                    aMarket.TerritoryId = Convert.ToInt32(dr["TerritoryId"]); ;
                  
                }
                return aMarket;


            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public bool SaveMarketInfo(Market aMarket)
        {
          
            try
            {
                bool result=false;
               
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                string empReceived = string.Empty;
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@marketId", aMarket.MarketId));
                aSqlParameterList.Add(new SqlParameter("@territoryId", aMarket.TerritoryId));
                aSqlParameterList.Add(new SqlParameter("@marketName", aMarket.MarketName));
                aSqlParameterList.Add(new SqlParameter("@createBy", aMarket.CreateBy));
                 
                    result=_accessManager.SaveData("sp_SaveOrUpdateMarket", aSqlParameterList);
                    return result;
            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }

            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }


    }
}