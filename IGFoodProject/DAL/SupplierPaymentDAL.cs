﻿using IGFoodProject.DataManager;
using IGFoodProject.Models;
using IGFoodProject.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IGFoodProject.DAL
{
    public class SupplierPaymentDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();
        public List<ViewPurchaseOrderMaster> PurchaseOrderListForPaymentCollection(int supplierId)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();

                List<ViewPurchaseOrderMaster> aList = new List<ViewPurchaseOrderMaster>();

                aParameters.Add(new SqlParameter("@supplierId", supplierId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetPurchaseOrderForPayment1", aParameters);
                while (dr.Read())
                {
                    ViewPurchaseOrderMaster aMaster = new ViewPurchaseOrderMaster();

                    aMaster.PurchaseOrderID = Convert.ToInt32(dr["PurchaseOrderID"]);
                    aMaster.PurchaseOrderNo = dr["PurchaseOrderNo"].ToString();
                    aMaster.OrderDate = Convert.ToDateTime(dr["OrderDate"]);
                    aMaster.TotalDue = Convert.ToDecimal(dr["TotalDue"]);
                    aMaster.TotalPaid = Convert.ToDecimal(dr["TotalPaid"]);
                    aMaster.TotalTax = Convert.ToDecimal(dr["TotalTax"]);
                    aMaster.GrandTotal = Convert.ToDecimal(dr["GrandTotal"]);
                    aMaster.PurchaseType= dr["PurchaseType"].ToString();
                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<SupplierPaymentDetail> GetPreviousCollectionDetails(int DODetailsID, string ptype)
        {

            try
            {

                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SupplierPaymentDetail> aList = new List<SupplierPaymentDetail>();

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@PurchaseOrderID", DODetailsID));
                aSqlParameterList.Add(new SqlParameter("@PurchaseType", ptype));

                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetPreviousPaymentDetails", aSqlParameterList);
                while (dr.Read())
                {
                    SupplierPaymentDetail aDetail = new SupplierPaymentDetail();
                    if (dr["PaymentDate"] != DBNull.Value)
                    {
                        aDetail.PaymentDate = Convert.ToDateTime(dr["PaymentDate"]);
                    }
                    aDetail.CreateBy = dr["CreateBy"].ToString();
                    if (dr["CreateDate"] != DBNull.Value)
                    {
                        aDetail.CreateDate = Convert.ToDateTime(dr["CreateDate"]);
                    }
                    aDetail.BankName = dr["BankName"].ToString();
                    aDetail.BranchName = dr["BranchName"].ToString();
                    aDetail.PaymentTypeName = dr["PaymentType"].ToString();
                    aDetail.PaymentAmount = Convert.ToDecimal(dr["PaymentAmount"]);
                    if (dr["RefDate"] != DBNull.Value)
                    {
                        aDetail.RefDate = Convert.ToDateTime(dr["RefDate"]);
                    }

                    aDetail.RefNo = dr["RefNo"].ToString();
                    aDetail.SerialNo = Convert.ToInt32(dr["SerialNo"]);
                    aDetail.TaxAmount= Convert.ToDecimal(dr["TaxAmount"]);
                    aDetail.GrandTotalD = Convert.ToDecimal(dr["GrandTotalD"]);
                    aList.Add(aDetail);
                }
                return aList;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public bool SaveOldSupplierPaymentMaster(SupplierPaymentMaster aMaster)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                string empReceived = string.Empty;
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@SupplierId", aMaster.SupplierId));
                aSqlParameterList.Add(new SqlParameter("@PaymentAmount", aMaster.PaymentAmount));
                aSqlParameterList.Add(new SqlParameter("@PaymentDate", aMaster.PaymentDate));
                aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@CreateDate", aMaster.CreateDate));
                aSqlParameterList.Add(new SqlParameter("@RefNo", aMaster.RefNo));
                aSqlParameterList.Add(new SqlParameter("@RefDate", aMaster.RefDate));
                aSqlParameterList.Add(new SqlParameter("@InvoiceDate", aMaster.InvoiceDate));
                aSqlParameterList.Add(new SqlParameter("@BankId", aMaster.BankId));
                aSqlParameterList.Add(new SqlParameter("@BranchId", aMaster.BranchId));
                aSqlParameterList.Add(new SqlParameter("@PaymentType", aMaster.PaymentType));
                var data = _accessManager.SaveData("sp_SaveSupplierOldPaymnet", aSqlParameterList);
                return data;
            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public int SaveSalesCollectionMaster(SupplierPaymentMaster aMaster)
        {
            try
            {
                string empReceived = string.Empty;
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@SupplierId", aMaster.SupplierId));
                aSqlParameterList.Add(new SqlParameter("@PaymentAmount", aMaster.PaymentAmount));
                aSqlParameterList.Add(new SqlParameter("@PaymentDate", aMaster.PaymentDate));

                aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@CreateDate", System.DateTime.Now));
                aSqlParameterList.Add(new SqlParameter("@Remark", aMaster.Remark));
                aSqlParameterList.Add(new SqlParameter("@TaxAmount", aMaster.TaxAmount));

                aSqlParameterList.Add(new SqlParameter("@TaxPercent", aMaster.TaxPercent));

                aSqlParameterList.Add(new SqlParameter("@GrandTotal", aMaster.GrandTotal));

                return _accessManager.SaveDataReturnPrimaryKey("sp_SaveSupplierPaymentMaster", aSqlParameterList);
            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }
        }

        public bool SaveSupplierPaymentDetails(SupplierPaymentMaster aMaster)
        {
            bool result = false;
            try

            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                int pkId = SaveSalesCollectionMaster(aMaster);


                foreach (SupplierPaymentDetail item in aMaster.aDetail)
                {
                    List<SqlParameter> gSqlParameterList = new List<SqlParameter>();

                    gSqlParameterList.Add(new SqlParameter("@SupplierPaymentMasterId", pkId));
                    gSqlParameterList.Add(new SqlParameter("@paymentType", item.PaymentType));
                    gSqlParameterList.Add(new SqlParameter("@restPaymentAmmount", item.PaymentAmount));
                    gSqlParameterList.Add(new SqlParameter("@PurchaseOrderID", item.PurchaseOrderID));
                    gSqlParameterList.Add(new SqlParameter("@refNo", item.RefNo));
                    gSqlParameterList.Add(new SqlParameter("@refDate", item.RefDate));
                    gSqlParameterList.Add(new SqlParameter("@BankId", item.BankId));
                    gSqlParameterList.Add(new SqlParameter("@BranchId", item.BranchId));
                    gSqlParameterList.Add(new SqlParameter("@Remark", item.Remark));
                    gSqlParameterList.Add(new SqlParameter("@TaxAmount", item.TaxAmount));
                    gSqlParameterList.Add(new SqlParameter("@GrandTotalD", item.GrandTotalD));
                    gSqlParameterList.Add(new SqlParameter("@PurchaseType", item.PurchaseType));
                    result = _accessManager.SaveData("sp_SaveSupplierPaymentDetails", gSqlParameterList);
                    //if (result == true)
                    //{
                    //    result = UpdateVocherMaster(pkId, aMaster.CreateBy);
                    //}
                }

                //if (result == true)
                //{
                //    result = UpdateVocherMaster(pkId, aMaster.CreateBy);
                //}

            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
            return result;
        }

        public bool UpdateVocherMaster(int SupplierPaymentId, string entryby)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@SupplierPaymentMasterId", SupplierPaymentId));
                aParameters.Add(new SqlParameter("@EntryBy", entryby));
                aParameters.Add(new SqlParameter("@EntryDate", DateTime.Now));
                result = _accessManager.SaveData("sp_Save_DebitVoucherMaster_SupllierPayment", aParameters);

                return result;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public DataTable SupplierLedger(int supplierId, DateTime fromDate,DateTime toDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@SupplierId", supplierId));
                aParameters.Add(new SqlParameter("@FromDate", fromDate));
                aParameters.Add(new SqlParameter("@ToDate", toDate));
                DataTable dt = _accessManager.GetDataTable("sp_SupplierLedgerNew", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public DataTable AllSupplierLedger(DateTime fromDate, DateTime toDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@fromDate", fromDate));
                aParameters.Add(new SqlParameter("@toDate", toDate));
                DataTable dt = _accessManager.GetDataTable("sp_AllSupplierLedger", aParameters);
                return dt;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        //public bool UpdateVocherMaster(int SupplierPaymentId, string entryby)
        //{
        //    try
        //    {
        //        bool result = false;
        //        List<SqlParameter> aParameters = new List<SqlParameter>();
        //        aParameters.Add(new SqlParameter("@SupplierPaymentMasterId", SupplierPaymentId));
        //        aParameters.Add(new SqlParameter("@EntryBy", entryby));
        //        aParameters.Add(new SqlParameter("@EntryDate", DateTime.Now));
        //        result = _accessManager.SaveData("sp_Save_DebitVoucherMaster_SupllierPayment", aParameters);

        //        return result;


        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }
        //}
    }
}