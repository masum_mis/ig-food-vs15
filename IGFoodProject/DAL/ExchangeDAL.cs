﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using IGFoodProject.DataManager;
using IGFoodProject.Models;

namespace IGFoodProject.DAL
{
    public class ExchangeDAL
    {
        DataAccessManager accessManager = new DataAccessManager();

        public List<ItemExchange> LoadItemGroup() 
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_LoadItemGroup");
                List<ItemExchange> itemList = new List<ItemExchange>();  
                while (dr.Read())
                {
                    ItemExchange item = new ItemExchange();
                    item.ItemGroupId = Convert.ToInt32(dr["ItemGroupId"]);
                    item.ItemGroupName = dr["ItemGroupName"].ToString();
                    itemList.Add(item);
                }
                return itemList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

        }

        public List<ItemExchange> LoadItem(int groupId)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("@groupId", groupId));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_LoadItem", param);
                List<ItemExchange> itemList = new List<ItemExchange>();
                while (dr.Read())
                {
                    ItemExchange item = new ItemExchange();
                    item.ItemId = Convert.ToInt32(dr["ItemId"]);
                    item.ItemName = dr["ItemName"].ToString(); 
                    itemList.Add(item);
                }
                return itemList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

        }

        public List<ItemExchange> LoadProductroup() 
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_LoadProductroup"); 
                List<ItemExchange> itemList = new List<ItemExchange>();
                while (dr.Read())
                {
                    ItemExchange item = new ItemExchange();
                    item.GroupId = Convert.ToInt32(dr["GroupId"]);
                    item.GroupName = dr["GroupName"].ToString(); 
                    itemList.Add(item);
                }
                return itemList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

        }

        public List<ItemExchange> LoadProduct(int groupId) 
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("@groupId", groupId));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_LoadProduct", param);
                List<ItemExchange> itemList = new List<ItemExchange>();
                while (dr.Read())
                {
                    ItemExchange item = new ItemExchange();
                    item.ProductId = Convert.ToInt32(dr["ProductId"]); 
                    item.ProductName = dr["ProductName"].ToString();
                    itemList.Add(item);
                }
                return itemList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

        }
        
        public ItemExchange LoadStockQty(int warehouseId, int groupId, int itemId)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("@warehouseId", warehouseId));
                param.Add(new SqlParameter("@groupId", groupId));
                param.Add(new SqlParameter("@itemId", itemId));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_LoadStockQty", param);
                ItemExchange exchange = new ItemExchange();
                while (dr.Read())
                {

                    exchange.StockQty = Convert.ToDecimal(dr["StockInQty"]);
                    exchange.Unit = dr["UOMName"].ToString();
                }
                return exchange;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

        }
        
        public ResultResponse SaveExchangeRequisition(ItemExchange aMaster)  
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                ResultResponse aResponse = new ResultResponse();

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@RmLocationId", aMaster.RmLocationId));
                aSqlParameterList.Add(new SqlParameter("@RmWarehouseId", aMaster.RmWarehouseId));
                aSqlParameterList.Add(new SqlParameter("@RmItemGroupId", aMaster.RmItemGroupId));
                aSqlParameterList.Add(new SqlParameter("@RmItemId", aMaster.RmItemId));
                aSqlParameterList.Add(new SqlParameter("@FgLocationId", aMaster.FgLocationId));
                aSqlParameterList.Add(new SqlParameter("@FgWarehouseId", aMaster.FgWarehouseId));
                aSqlParameterList.Add(new SqlParameter("@FgProductGroupId", aMaster.FgProductGroupId));
                aSqlParameterList.Add(new SqlParameter("@FgProductId", aMaster.FgProductId));

                aSqlParameterList.Add(new SqlParameter("@ExchangeQty", aMaster.ExchangeQty));
                aSqlParameterList.Add(new SqlParameter("@ExchangeDate", aMaster.ExchangeDate));
                aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@Remarks", aMaster.Remarks));

                aResponse.isSuccess = accessManager.SaveData("sp_SaveExchangeRequisition", aSqlParameterList);
          
                return aResponse;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<ItemExchange> ExchangeList(string empId) 
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("@empId", empId));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_ExchangeList", param);
                List<ItemExchange> itemList = new List<ItemExchange>();
                while (dr.Read())
                {
                    ItemExchange item = new ItemExchange();
                    item.ExchangeId = Convert.ToInt32(dr["ExchangeId"]);
                    item.RmLocationName = dr["RmLocationName"].ToString();
                    item.RmWarehouseName = dr["RmWarehouseName"].ToString(); 
                    item.RmItemGroupName = dr["RmItemGroupName"].ToString();
                    item.RmItemName = dr["RmItemName"].ToString();
                    item.FgLocationName = dr["FgLocationName"].ToString();
                    item.FgWarehouseName = dr["FgWarehouseName"].ToString();
                    item.FgProductGroupName = dr["FgProductGroupName"].ToString();
                    item.FgProductName = dr["FgProductName"].ToString();

                    item.ExchangeQty = DBNull.Value == dr["ExchangeQty"] ? 0 : Convert.ToDecimal(dr["ExchangeQty"]);
                    item.ApprovedQty = DBNull.Value == dr["ApprovedQty"] ? 0 : Convert.ToDecimal(dr["ApprovedQty"]);

                    item.ExeDate = dr["ExeDate"].ToString();
                    item.Remarks = dr["Remarks"].ToString();
                    item.CreateBy = dr["CreateBy"].ToString();
                    item.CreateDate = dr["CreateDate"].ToString();
                    item.ApprovedBy = dr["ApprovedBy"].ToString();
                    item.ApprovedDate = dr["ApprovedDate"].ToString();
                    item.ApprovedStatus = dr["ApprovedStatus"].ToString(); 


                    itemList.Add(item);
                }
                return itemList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

        }

        public List<ItemExchange> ExchangeApproveList()
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_ExchangeApproveList");
                List<ItemExchange> itemList = new List<ItemExchange>();
                while (dr.Read())
                {
                    ItemExchange item = new ItemExchange();
                    item.ExchangeId = Convert.ToInt32(dr["ExchangeId"]);
                    item.RmLocationName = dr["RmLocationName"].ToString();
                    item.RmWarehouseName = dr["RmWarehouseName"].ToString();
                    item.RmItemGroupName = dr["RmItemGroupName"].ToString();
                    item.RmItemName = dr["RmItemName"].ToString();
                    item.FgLocationName = dr["FgLocationName"].ToString();
                    item.FgWarehouseName = dr["FgWarehouseName"].ToString();
                    item.FgProductGroupName = dr["FgProductGroupName"].ToString();
                    item.FgProductName = dr["FgProductName"].ToString();

                    item.ExchangeQty = DBNull.Value == dr["ExchangeQty"] ? 0 : Convert.ToDecimal(dr["ExchangeQty"]);
                    item.ApprovedQty = DBNull.Value == dr["ApprovedQty"] ? 0 : Convert.ToDecimal(dr["ApprovedQty"]);

                    item.ExeDate = dr["ExeDate"].ToString();
                    item.Remarks = dr["Remarks"].ToString();
                    item.CreateBy = dr["CreateBy"].ToString();
                    item.CreateDate = dr["CreateDate"].ToString();
                    item.ApprovedBy = dr["ApprovedBy"].ToString();
                    item.ApprovedDate = dr["ApprovedDate"].ToString();
                    item.ApprovedStatus = dr["ApprovedStatus"].ToString();
                    item.StockQty = DBNull.Value == dr["StockQty"] ? 0 : Convert.ToDecimal(dr["StockQty"]);

                    itemList.Add(item);
                }
                return itemList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }

        }

        public ResultResponse SaveAccept(decimal appQty, int exchangeId, string empId)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                ResultResponse aResponse = new ResultResponse();

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                

                aSqlParameterList.Add(new SqlParameter("@appQty", appQty));
                aSqlParameterList.Add(new SqlParameter("@exchangeId", exchangeId));
                aSqlParameterList.Add(new SqlParameter("@empId", empId));

                aResponse.isSuccess = accessManager.SaveData("sp_SaveExchangeAccept", aSqlParameterList);

                if (aResponse.isSuccess == true)
                {
                    
                    aResponse.isSuccess = ItemExchangeSystemJournal(exchangeId);
                    if (aResponse.isSuccess == false)
                    {
                        accessManager.SqlConnectionClose(true);
                    }
                    

                }

                return aResponse;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public bool ItemExchangeSystemJournal(int masterId)
        {

            try
            {

                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ExchangeId", masterId));


                result = accessManager.SaveData("SaveExchangeJournal", aParameters);

                return result;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }
}