﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using IGFoodProject.DataManager;

namespace IGFoodProject.DAL
{
    public class HomeDAL
    {

        private DataAccessManager accessManager = new DataAccessManager();


        public DataTable CheckLoginDAL(string userName, string pass)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.ControlPanel);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@LoginUserName", userName));
                aSqlParameterList.Add(new SqlParameter("@ModuleId", 2));
                aSqlParameterList.Add(new SqlParameter("@LoginPassword", EncryptDecrypt.EncryptText(pass)));
                return accessManager.GetDataTable("sp_Get_LoginInAccessCheck", aSqlParameterList);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
    }
}