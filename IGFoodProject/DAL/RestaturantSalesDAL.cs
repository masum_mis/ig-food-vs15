﻿using IGFoodProject.DataManager;
using IGFoodProject.Models;
using IGFoodProject.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IGFoodProject.DAL
{
    public class RestaturantSalesDAL
    {
        DataAccessManager accessManager = new DataAccessManager();
        public List<CustomerType> GetCustomerTypeList()
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<CustomerType> _List = new List<CustomerType>();

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetCustomerTypeForRestaturant");
                while (dr.Read())
                {
                    CustomerType aInfo = new CustomerType();
                    aInfo.CustomerTypeID = Convert.ToInt32(dr["CustomerTypeID"]);
                    aInfo.CoustomerTypeName = dr["CoustomerTypeName"].ToString();
                    aInfo.IsActive = Convert.ToBoolean(dr["IsActive"]);
                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public List<ViewCustomerInfo> GetCustomerList(int typeid, int loginUserId)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ViewCustomerInfo> _List = new List<ViewCustomerInfo>();
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@customertypeid", typeid));
                aSqlParameterList.Add(new SqlParameter("@loginUserId", loginUserId));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetCustomerListForRestaturant", aSqlParameterList);
                while (dr.Read())
                {
                    ViewCustomerInfo aInfo = new ViewCustomerInfo();
                    aInfo.CustomerID = Convert.ToInt32(dr["CustomerID"]);
                    aInfo.CustomerName = dr["CustomerName"].ToString();
                    aInfo.CustomerCode = dr["CustomerCode"].ToString();
                    aInfo.CoustomerTypeName = dr["CoustomerTypeName"].ToString();
                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public List<WareHouse> GetWareHousesByType(int warehouseTypeId, int loginUserId, int customerId)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<WareHouse> _List = new List<WareHouse>();
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@warehouseTypeId", warehouseTypeId));
                aSqlParameterList.Add(new SqlParameter("@loginUserId", loginUserId));
                aSqlParameterList.Add(new SqlParameter("@customerId", customerId));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetWarehouseByUser", aSqlParameterList);
                while (dr.Read())
                {
                    WareHouse aInfo = new WareHouse();
                    aInfo.WareHouseId = Convert.ToInt32(dr["WareHouseId"]);
                    aInfo.WareHouseName = dr["WareHouseName"].ToString();
                    aInfo.WarehouseTypeId = DBNull.Value.Equals(dr["WarehouseTypeId"]) ? 0 : Convert.ToInt32(dr["WarehouseTypeId"]);
                    aInfo.LocationId = DBNull.Value.Equals(dr["LocationId"]) ? 0 : Convert.ToInt32(dr["LocationId"]);

                    _List.Add(aInfo);
                }
                return _List;

            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public tbl_RetailCustomerInfo GetRetailCustomerDetails(string phoneNo)
        {
            try
            {
                tbl_RetailCustomerInfo aDetails = new tbl_RetailCustomerInfo();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@phoneNo", phoneNo));
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetRestaturantCustomerByPhoneNo", aParameters);
                while (dr.Read())
                {
                    aDetails.RetailCustomerID = Convert.ToInt32(dr["RetailCustomerID"]);
                    aDetails.RetailCode = dr["RetailCode"].ToString();
                    aDetails.RetailName = dr["RetailName"].ToString();
                    aDetails.PersonalAddress = dr["PersonalAddress"].ToString();

                }
                return aDetails;
            }
            catch (Exception ex)
            {

                throw ex;
            }

            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public ViewProductDetailsSales GetProductDetails(int id, DateTime? orderdate, int customertype, int wareHouseId = 0)
        {
            try
            {
                ViewProductDetailsSales aDetails = new ViewProductDetailsSales();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                aParameters.Add(new SqlParameter("@wareHouseId", wareHouseId));
                aParameters.Add(new SqlParameter("@orderdate", orderdate));
                aParameters.Add(new SqlParameter("@customertypeid", customertype));
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_ProductStockDetailsProcessedForRestaturant", aParameters);
                while (dr.Read())
                {
                    aDetails.ProductID = Convert.ToInt32(dr["ProductID"]);
                    aDetails.StockQty = DBNull.Value.Equals(dr["StockQty"]) ? 0 : Convert.ToDecimal(dr["StockQty"]);
                    aDetails.StockPrice = DBNull.Value.Equals(dr["StockPrice"]) ? 0 : Convert.ToDecimal(dr["StockPrice"]);
                    aDetails.StockKg = DBNull.Value.Equals(dr["StockKg"]) ? 0 : Convert.ToDecimal(dr["StockKg"]);
                    aDetails.groupid = Convert.ToInt32(dr["GroupId"]);

                    aDetails.BasePrice = DBNull.Value.Equals(dr["BasePrice"]) ? 0 : Convert.ToDecimal(dr["BasePrice"]);
                    aDetails.IsBulk = Convert.ToBoolean(dr["IsBulk"]);
                    aDetails.MTRetention = DBNull.Value.Equals(dr["MTRetention"]) ? 0 : Convert.ToDecimal(dr["MTRetention"]);
                    aDetails.PerKgStdQty = DBNull.Value.Equals(dr["PerKgStdQty"]) ? 0 : Convert.ToDecimal(dr["PerKgStdQty"]);
                    aDetails.Incentive = DBNull.Value.Equals(dr["Incentive"]) ? 0 : Convert.ToDecimal(dr["Incentive"]);

                }
                return aDetails;
            }
            catch (Exception ex)
            {

                throw ex;
            }

            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse SaveSalesOrderProcessed(SalesOrderProcessedMaster aMaster, string user)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);


                if (aMaster.eflag == "ed")
                {
                    var addIn = aMaster.SalesDetails.Find(a => a.OperationIdAdd == 3); // Add In Challan if add in challan Is Approve =1
                    if (addIn != null)
                    {
                        aMaster.IsApprove = 1;
                    }
                    var deletIn = aMaster.SalesDetails.Find(a => a.OperationIdDelete == 3); // Delete from  Challan if delete in challan Is Approve =1
                    if (deletIn != null)
                    {
                        aMaster.IsApprove = 1;
                    }
                }
                else
                {
                    aMaster.IsApprove = 1;
                }




                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CustomerId", aMaster.CustomerId));
                aParameters.Add(new SqlParameter("@SalesOrderProcessedMasterId", aMaster.SalesOrderProcessedMasterId));
                aParameters.Add(new SqlParameter("@OrderType", aMaster.OrderType));
                if (aMaster.IsEmployee == true)
                {
                    aParameters.Add(new SqlParameter("@SalesPerson", aMaster.SalesPerson.Split(':')[0].Trim()));
                }
                else
                {
                    aParameters.Add(new SqlParameter("@SalesPerson", aMaster.SalesPerson));
                }
                aParameters.Add(new SqlParameter("@SrId", aMaster.SrId));
                aParameters.Add(new SqlParameter("@OrderDate", aMaster.OrderDate));
                aParameters.Add(new SqlParameter("@DeliveryDestination", aMaster.DeliveryDestination));
                aParameters.Add(new SqlParameter("@CreateBy", user));
                aParameters.Add(new SqlParameter("@OrderBy", user));
                aParameters.Add(new SqlParameter("@CreateDate", System.DateTime.Now));


                //aParameters.Add(new SqlParameter("@GroupId", aMaster.GroupId));
                //aParameters.Add(new SqlParameter("@PaymentType", aMaster.PaymentType));

                // aParameters.Add(new SqlParameter("@BankId", aMaster.BankId));
                // aParameters.Add(new SqlParameter("@BranchId", aMaster.BranchId));
                // aParameters.Add(new SqlParameter("@ChequeNo", aMaster.ChequeNo));
                //  aParameters.Add(new SqlParameter("@ChequeDate", aMaster.ChequeDate));



                aParameters.Add(new SqlParameter("@CompanyId", aMaster.CompanyId));
                aParameters.Add(new SqlParameter("@SalesPersonContact", aMaster.SalesPersonContact));
                aParameters.Add(new SqlParameter("@DeliveryDate", aMaster.DeliveryDate));
                aParameters.Add(new SqlParameter("@TotalOrderQty", aMaster.TotalOrderQty));
                aParameters.Add(new SqlParameter("@TotatalOrderAmtKg", aMaster.TotatalOrderAmtKg));
                aParameters.Add(new SqlParameter("@TotalIncTaxVat", aMaster.TotalIncTaxVat));

                aParameters.Add(new SqlParameter("@TotalExTaxVat", aMaster.TotalExTaxVat));
                aParameters.Add(new SqlParameter("@TaxVat", aMaster.TaxVat));

                aParameters.Add(new SqlParameter("@collectiontypeid", aMaster.CollectionType));
                aParameters.Add(new SqlParameter("@collectionamount", aMaster.CollectionAmount));
                aParameters.Add(new SqlParameter("@IsEmployee", aMaster.IsEmployee));
                aParameters.Add(new SqlParameter("@CustomerTypeID", aMaster.CustomerTypeId));

                aParameters.Add(new SqlParameter("@ColBankId", aMaster.ColBankId));
                aParameters.Add(new SqlParameter("@ColBranchId", aMaster.ColBranchId));

                aParameters.Add(new SqlParameter("@RefNo", aMaster.RefNo));
                aParameters.Add(new SqlParameter("@RefDate", aMaster.RefDate));

                aParameters.Add(new SqlParameter("@RetailCustomerID", aMaster.RetailCustomerID));
                aParameters.Add(new SqlParameter("@RetailName", aMaster.RetailName));
                aParameters.Add(new SqlParameter("@PersonalAddress", aMaster.PersonalAddress));
                aParameters.Add(new SqlParameter("@Phone", aMaster.Phone));



                aParameters.Add(new SqlParameter("@Remarks", aMaster.Remarks));
                if (aMaster.eflag == "ap")
                {
                    aParameters.Add(new SqlParameter("@IsApprove", 1));
                }
                else
                {
                    aParameters.Add(new SqlParameter("@IsApprove", aMaster.IsApprove));
                }

                aParameters.Add(new SqlParameter("@eflag", aMaster.eflag));
                aParameters.Add(new SqlParameter("@DeliveryWarehouseId", aMaster.DeliveryWarehouseId));


                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Save_SalesProcessedForRestaturant", aParameters);
                int salesOrderMasterId = 0;
                int pickingMasterId = 0;
                int challanMasterId = 0;
                int collectionMasterId = 0;

                while (dr.Read())
                {
                    salesOrderMasterId = Convert.ToInt32(dr["masteridTab"]);
                    pickingMasterId = Convert.ToInt32(dr["PickingMasterIdTab"]);
                    challanMasterId = Convert.ToInt32(dr["ChallanMasterIdTab"]);
                    collectionMasterId = Convert.ToInt32(dr["CollectionMasterIdTab"]);

                }
                dr.Close();

                if (salesOrderMasterId > 0 && pickingMasterId > 0 && challanMasterId > 0 && collectionMasterId > 0)
                {
                    aResponse.pk = salesOrderMasterId;
                    aResponse.isSuccess = SaveSalesOrderProcessedDetails(aMaster.SalesDetails, user, salesOrderMasterId, pickingMasterId, challanMasterId);
                    if (aResponse.isSuccess == true)
                    {
                        aResponse.isSuccess = SaveChallanJournal(challanMasterId);
                        aResponse.isSuccess = SalesCollectionVoucher(collectionMasterId);
                    }
                    if (aResponse.isSuccess == false)
                    {
                        accessManager.SqlConnectionClose(true);
                    }
                }
                else
                {
                    accessManager.SqlConnectionClose(true);
                }
                return aResponse;
            }
            catch (Exception ex)
            {
                accessManager.SqlConnectionClose(true);
                throw ex;
            }

            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public bool SaveSalesOrderProcessedDetails(List<SalesOrderProcessedDetails> aList, string user, int salesOrderMasterId, int pickingMasterId, int challanMasterId)
        {
            try
            {
                bool result = false;
                foreach (var item in aList)
                {
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@SalesOrderProcessedMasterId", salesOrderMasterId));
                    aParameters.Add(new SqlParameter("@PickingMasterId", pickingMasterId));
                    aParameters.Add(new SqlParameter("@ChallanMasterId", challanMasterId));
                    aParameters.Add(new SqlParameter("@ProductId", item.ProductId));
                    aParameters.Add(new SqlParameter("@ProductQty", item.ProductQty));
                    aParameters.Add(new SqlParameter("@ProductKg", item.ProductKg));
                    aParameters.Add(new SqlParameter("@StockRate", item.StockRate));
                    aParameters.Add(new SqlParameter("@SaleRate", item.SaleRate));
                    aParameters.Add(new SqlParameter("@BaseRate", item.BaseRate));
                    aParameters.Add(new SqlParameter("@DiscountAmount", item.DiscountAmount));
                    aParameters.Add(new SqlParameter("@DiscountPercent", item.DiscountPercent));
                    aParameters.Add(new SqlParameter("@TotalPrice", item.TotalPrice));
                    aParameters.Add(new SqlParameter("@BTotalPrice", item.BTotalPrice));
                    aParameters.Add(new SqlParameter("@ProductTax", item.ProductTax));
                    aParameters.Add(new SqlParameter("@CreateBy", user));
                    aParameters.Add(new SqlParameter("@IsDelete", item.IsDelete));
                    aParameters.Add(new SqlParameter("@isApprove", 1));
                    aParameters.Add(new SqlParameter("@OperationIdAdd", item.OperationIdAdd));
                    aParameters.Add(new SqlParameter("@OperationIdDelete", item.OperationIdDelete));
                    aParameters.Add(new SqlParameter("@CreateDate", DateTime.Now));
                    aParameters.Add(new SqlParameter("@groupid", item.GroupId));
                    aParameters.Add(new SqlParameter("@haschallan", item.haschallan));
                    aParameters.Add(new SqlParameter("@eflag", item.eflag));
                    aParameters.Add(new SqlParameter("@alreadypicked", item.alreadypicked));
                    aParameters.Add(new SqlParameter("@isbulk", item.isbulk));
                    aParameters.Add(new SqlParameter("@MTRetentionFront", item.MTRetention));
                    aParameters.Add(new SqlParameter("@Incentive", item.Incentive));
                    result = accessManager.SaveData("sp_Save_SalesProcessedDetailsForRestaturant", aParameters);
                    if (result == false)
                    {
                        break;
                    }

                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<ViewSalesOrderProcessedMaster> GetSalesOrderProcessedMasterSearch(DateTime? fromdate, DateTime? todate, string salesperson, string loginUserEmpId, int retailCustomerid = 0, int deliveryWarehouseId = 0)
        {
            try
            {

                if (fromdate == null)
                {
                    //fromdate = System.DateTime.Now.AddDays(-1);
                    fromdate = System.DateTime.Now;

                }
                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }
                if (salesperson != null)
                {
                    salesperson = salesperson.Split(':')[0].Trim();
                }
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<ViewSalesOrderProcessedMaster> aList = new List<ViewSalesOrderProcessedMaster>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@retailCustomerid", retailCustomerid));
                aParameters.Add(new SqlParameter("@SalesPerson", salesperson));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                aParameters.Add(new SqlParameter("@deliveryWarehouseId", deliveryWarehouseId));
                aParameters.Add(new SqlParameter("@loginUserEmpId", loginUserEmpId));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_SalesOrderProcessedListForRestaturant", aParameters);
                while (dr.Read())
                {
                    ViewSalesOrderProcessedMaster aMaster = new ViewSalesOrderProcessedMaster();

                    aMaster.SalesOrderProcessedMasterId = Convert.ToInt32(dr["SalesOrderProcessedMasterId"]);
                    aMaster.TotatalOrderAmtKg = Convert.ToDecimal(dr["TotatalOrderAmtKg"]);
                    aMaster.TotalIncTaxVat = Convert.ToDecimal(dr["TotalIncTaxVat"]);
                    aMaster.TotalExTaxVat = Convert.ToDecimal(dr["TotalExTaxVat"]);
                    aMaster.TotalDue = Convert.ToDecimal(dr["TotalDue"]);
                    aMaster.TotalPaid = Convert.ToDecimal(dr["TotalPaid"]);

                    aMaster.SalesPerson = dr["SalesPerson"].ToString();
                    aMaster.SalesOrderNo = dr["SalesOrderNo"].ToString();
                    aMaster.CustomerName = dr["CustomerName"].ToString();
                    aMaster.DeliveryDestination = dr["DeliveryDestination"].ToString();
                    aMaster.SalesPersonContact = dr["SalesPersonContact"].ToString();

                    aMaster.CreateBy = dr["CreateBy"].ToString();
                    aMaster.ApproveBy = dr["ApproveBy"].ToString();
                    aMaster.ApproveDate = dr["ApproveDate"] as DateTime?;
                    aMaster.CreateDate = dr["CreateDate"] as DateTime?;
                    aMaster.OrderDate = dr["OrderDate"] as DateTime?;
                    aMaster.DeliveryDate = dr["DeliveryDate"] as DateTime?;
                    if (DBNull.Value == dr["IsApprove"])
                    {
                        aMaster.IsApprove = 0;
                    }
                    else
                    {
                        aMaster.IsApprove = Convert.ToInt32(dr["IsApprove"]);
                    }
                    if (DBNull.Value == dr["IsEmployee"])
                    {
                        aMaster.IsEmployee = null;
                    }
                    else
                    {
                        aMaster.IsEmployee = (bool)dr["IsEmployee"];
                    }
                    aMaster.CustomerType = dr["CoustomerTypeName"].ToString();
                    aMaster.DeliveryWarehouse = dr["WareHouseName"].ToString();
                    aMaster.RetailName = dr["ResCustomerName"].ToString();
                    aMaster.RetailCode = dr["ResCustomerCode"].ToString();
                    aMaster.RetailPhoneNo = dr["ResPhoneNo"].ToString();
                    aMaster.RetailPersonalAddress = dr["ResPersonalAddress"].ToString();
                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public List<ViewSalesOrderProcessedMaster> GetSalesOrderProcessedMasterSearchVoid(DateTime? fromdate, DateTime? todate, string salesperson, int retailCustomerid = 0, int deliveryWarehouseId = 0)
        {
            try
            {

                if (fromdate == null)
                {
                    //fromdate = System.DateTime.Now.AddDays(-1);
                    fromdate = System.DateTime.Now;

                }
                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }
                if (salesperson != null)
                {
                    salesperson = salesperson.Split(':')[0].Trim();
                }
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<ViewSalesOrderProcessedMaster> aList = new List<ViewSalesOrderProcessedMaster>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@retailCustomerid", retailCustomerid));
                aParameters.Add(new SqlParameter("@SalesPerson", salesperson));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                aParameters.Add(new SqlParameter("@deliveryWarehouseId", deliveryWarehouseId));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_SalesOrderProcessedListVoidForRestaturant", aParameters);
                while (dr.Read())
                {
                    ViewSalesOrderProcessedMaster aMaster = new ViewSalesOrderProcessedMaster();

                    aMaster.SalesOrderProcessedMasterId = Convert.ToInt32(dr["SalesOrderProcessedMasterId"]);
                    aMaster.TotatalOrderAmtKg = Convert.ToDecimal(dr["TotatalOrderAmtKg"]);
                    aMaster.TotalIncTaxVat = Convert.ToDecimal(dr["TotalIncTaxVat"]);
                    aMaster.TotalExTaxVat = Convert.ToDecimal(dr["TotalExTaxVat"]);
                    aMaster.TotalDue = Convert.ToDecimal(dr["TotalDue"]);
                    aMaster.TotalPaid = Convert.ToDecimal(dr["TotalPaid"]);

                    aMaster.SalesPerson = dr["SalesPerson"].ToString();
                    aMaster.SalesOrderNo = dr["SalesOrderNo"].ToString();
                    aMaster.CustomerName = dr["CustomerName"].ToString();
                    aMaster.DeliveryDestination = dr["DeliveryDestination"].ToString();
                    aMaster.SalesPersonContact = dr["SalesPersonContact"].ToString();

                    aMaster.CreateBy = dr["CreateBy"].ToString();
                    aMaster.ApproveBy = dr["ApproveBy"].ToString();
                    aMaster.ApproveDate = dr["ApproveDate"] as DateTime?;
                    aMaster.CreateDate = dr["CreateDate"] as DateTime?;
                    aMaster.OrderDate = dr["OrderDate"] as DateTime?;
                    aMaster.DeliveryDate = dr["DeliveryDate"] as DateTime?;
                    if (DBNull.Value == dr["IsApprove"])
                    {
                        aMaster.IsApprove = 0;
                    }
                    else
                    {
                        aMaster.IsApprove = Convert.ToInt32(dr["IsApprove"]);
                    }
                    if (DBNull.Value == dr["IsEmployee"])
                    {
                        aMaster.IsEmployee = null;
                    }
                    else
                    {
                        aMaster.IsEmployee = (bool)dr["IsEmployee"];
                    }
                    aMaster.CustomerType = dr["CoustomerTypeName"].ToString();
                    aMaster.DeliveryWarehouse = dr["WareHouseName"].ToString();
                    aMaster.RetailName = dr["ResCustomerName"].ToString();
                    aMaster.RetailCode = dr["ResCustomerCode"].ToString();
                    aMaster.RetailPhoneNo = dr["ResPhoneNo"].ToString();
                    aMaster.RetailPersonalAddress = dr["ResPersonalAddress"].ToString();
                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse SODelete(int id)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                aResponse.isSuccess = accessManager.SaveData("sp_SODeleteForRestaturant", aParameters);
                return aResponse;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public ResultResponse SOVoid(int id)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                aResponse.isSuccess = accessManager.SaveData("sp_SOVoidForRetailer", aParameters);
                return aResponse;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public ResultResponse SoCancelVoid(int id)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                aResponse.isSuccess = accessManager.SaveData("sp_SoCancelVoidForRetailer", aParameters);
                return aResponse;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public DataTable GetRestaturantOrderForPrint(int soId)
        {
            try
            {
                DataTable dt = new DataTable();
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@soId", soId));
                dt = accessManager.GetDataTable("sp_GetRestaturantOrderForPrint", aParameters);
                return dt;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public List<tbl_RestaturantCustomerInfo> GetRestaturantCustomerList()
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<tbl_RestaturantCustomerInfo> _List = new List<tbl_RestaturantCustomerInfo>();
                //List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetRestaturantCustomer");
                while (dr.Read())
                {
                    tbl_RestaturantCustomerInfo aInfo = new tbl_RestaturantCustomerInfo();
                    aInfo.RestaturantCustomerID = Convert.ToInt32(dr["RestaturantCustomerID"]);
                    aInfo.ResCustomerName = dr["ResCustomerName"].ToString();
                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public SalesOrderProcessedMaster GetSalesOrderProcessedMaster(int id)
        {
            try
            {
                List<SqlParameter> aParameters = new List<SqlParameter>();
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                aParameters.Add(new SqlParameter("@id", id));
                DataTable dt = accessManager.GetDataTable("sp_Get_SalesOrderProcessedMasrerByIdForRestaturant", aParameters);
                List<SalesOrderProcessedMaster> aList = Helper.ToListof<SalesOrderProcessedMaster>(dt);
                return aList[0];
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public List<SalesOrderProcessedDetails> GetSalesOderProcessedDetailsByMaster(int id)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                DataTable dt = accessManager.GetDataTable("sp_Get_SalesOrderPrecessedDetailsByMasterForRestaturant", aParameters);
                List<SalesOrderProcessedDetails> aList = Helper.ToListof<SalesOrderProcessedDetails>(dt);
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public DataTable SalesOrderWiseSalesPersonSalesReport(DateTime fromDate, DateTime toDate, int summary, string salesPerson = "", string customerTypeId = "", int divisionId = 0, int areaId = 0, int territoryId = 0, int marketId = 0, int warehouseId = 0)
        {
            DataTable dt = new DataTable();
            accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
            List<SqlParameter> aParameters = new List<SqlParameter>();
            aParameters.Add(new SqlParameter("@FormDate", fromDate));
            aParameters.Add(new SqlParameter("@ToDate", toDate));
            aParameters.Add(new SqlParameter("@SalesPerson", salesPerson));
            aParameters.Add(new SqlParameter("@CustomerTypeId", customerTypeId));
            aParameters.Add(new SqlParameter("@DivisionId", divisionId));
            aParameters.Add(new SqlParameter("@AreaId", areaId));
            aParameters.Add(new SqlParameter("@TerritoryId", territoryId));
            aParameters.Add(new SqlParameter("@MarketId", marketId));
            aParameters.Add(new SqlParameter("@WarehouseId", warehouseId));
            if (summary == 1)
            {
                //  dt = accessManager.GetDataTable("sp_GetSalesOrderSummerySalesPersonSalesAmount", aParameters);
            }
            else
            {
                dt = accessManager.GetDataTable("spGetSalesOrderReportSalesPersonSalesRestaturant", aParameters);
            }


            return dt;
        }

        public DataTable ChallanWiseSalesPersonSalesReport(DateTime fromDate, DateTime toDate, int summary, string salesPerson = "", string customerTypeId = "", int divisionId = 0, int areaId = 0, int territoryId = 0, int marketId = 0, int warehouseId = 0)
        {
            DataTable dt = new DataTable();
            accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
            List<SqlParameter> aParameters = new List<SqlParameter>();
            aParameters.Add(new SqlParameter("@FormDate", fromDate));
            aParameters.Add(new SqlParameter("@ToDate", toDate));
            aParameters.Add(new SqlParameter("@SalesPerson", salesPerson));
            aParameters.Add(new SqlParameter("@CustomerTypeId", customerTypeId));
            aParameters.Add(new SqlParameter("@DivisionId", divisionId));
            aParameters.Add(new SqlParameter("@AreaId", areaId));
            aParameters.Add(new SqlParameter("@TerritoryId", territoryId));
            aParameters.Add(new SqlParameter("@MarketId", marketId));
            aParameters.Add(new SqlParameter("@WarehouseId", warehouseId));
            if (summary == 1)
            {
                // dt = accessManager.GetDataTable("sp_GetSalesChallanSummerySalesPersonSalesAmount", aParameters);
            }
            else
            {
                dt = accessManager.GetDataTable("spGetSalesChallanReportSalesPersonSalesRestaturant", aParameters);
            }


            return dt;
        }

        public DataTable SalesOrderWiseSalesPersonSalesDetailsReport(DateTime fromDate, DateTime toDate, int summary, string salesPerson = "", string customerTypeId = "", int divisionId = 0, int areaId = 0, int territoryId = 0, int marketId = 0, int warehouseId = 0)
        {
            DataTable dt = new DataTable();
            accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
            List<SqlParameter> aParameters = new List<SqlParameter>();
            aParameters.Add(new SqlParameter("@FormDate", fromDate));
            aParameters.Add(new SqlParameter("@ToDate", toDate));
            aParameters.Add(new SqlParameter("@SalesPerson", salesPerson));
            aParameters.Add(new SqlParameter("@CustomerTypeId", customerTypeId));
            aParameters.Add(new SqlParameter("@DivisionId", divisionId));
            aParameters.Add(new SqlParameter("@AreaId", areaId));
            aParameters.Add(new SqlParameter("@TerritoryId", territoryId));
            aParameters.Add(new SqlParameter("@MarketId", marketId));
            aParameters.Add(new SqlParameter("@WarehouseId", warehouseId));
            if (summary == 1)
            {
                // dt = _accessManager.GetDataTable("sp_GetSalesOrderSummerySalesPersonSalesAmount", aParameters);
            }
            else
            {
                dt = accessManager.GetDataTable("spGetSalesOrderDetailsReportSalesPersonSalesRestaturant", aParameters);
            }


            return dt;
        }

        public DataTable ChallanWiseSalesPersonSalesDetailsReport(DateTime fromDate, DateTime toDate, int summary, string salesPerson = "", string customerTypeId = "", int divisionId = 0, int areaId = 0, int territoryId = 0, int marketId = 0, int warehouseId = 0)
        {
            DataTable dt = new DataTable();
            accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
            List<SqlParameter> aParameters = new List<SqlParameter>();
            aParameters.Add(new SqlParameter("@FormDate", fromDate));
            aParameters.Add(new SqlParameter("@ToDate", toDate));
            aParameters.Add(new SqlParameter("@SalesPerson", salesPerson));
            aParameters.Add(new SqlParameter("@CustomerTypeId", customerTypeId));
            aParameters.Add(new SqlParameter("@DivisionId", divisionId));
            aParameters.Add(new SqlParameter("@AreaId", areaId));
            aParameters.Add(new SqlParameter("@TerritoryId", territoryId));
            aParameters.Add(new SqlParameter("@MarketId", marketId));
            aParameters.Add(new SqlParameter("@WarehouseId", warehouseId));
            if (summary == 1)
            {
                // dt = _accessManager.GetDataTable("sp_GetSalesChallanSummerySalesPersonSalesAmount", aParameters);
            }
            else
            {
                dt = accessManager.GetDataTable("spGetSalesChallanDetailsReportSalesPersonSalesRestaturant", aParameters);
            }


            return dt;
        }

        public bool SaveChallanJournal(int ChallanId)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ChallanMasterId", ChallanId));
                result = accessManager.SaveData("saveChallanJournal", aParameters);

                return result;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public bool SalesCollectionVoucher(int CollectionId)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CollectionMasterId", CollectionId));

                result = accessManager.SaveData("SaveSalesCollectionVoucher", aParameters);

                return result;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}