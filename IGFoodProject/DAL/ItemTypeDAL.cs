﻿using IGFoodProject.DataManager;
using IGFoodProject.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IGFoodProject.DAL
{
    public class ItemTypeDAL
    {
        DataAccessManager accessManager = new DataAccessManager();
        public List<ItemType> GetItemGroupList() 
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ItemType> _List = new List<ItemType>();                

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetItemGroupForDD");
                while (dr.Read())
                {
                    ItemType aInfo = new ItemType();
                    aInfo.ItemGroupId = (int)dr["ItemGroupId"];
                    aInfo.ItemGroupName = dr["ItemGroupName"].ToString();                

                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public int CheckItemType(string ptype) 
        {
            int p_type = 0;
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@type", ptype));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_CheckItemType", aSqlParameterList);

                while (dr.Read())
                {
                    p_type = Convert.ToInt32(dr["p_type"]);
                }
            }
            catch (Exception exception)
            {
                //accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
            return p_type;
        }

        public bool SaveItemType(ItemType aMaster) 
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@ItemTypeId", aMaster.ItemTypeId));
                aSqlParameterList.Add(new SqlParameter("@ItemGroupId", aMaster.ItemGroupId));
                aSqlParameterList.Add(new SqlParameter("@ItemTypeName", aMaster.ItemTypeName));
                aSqlParameterList.Add(new SqlParameter("@CreateDate", System.DateTime.Now));
                aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@UpdateDate", System.DateTime.Now));
                aSqlParameterList.Add(new SqlParameter("@UpdateBy", aMaster.UpdateBy));
                aSqlParameterList.Add(new SqlParameter("@IsActive", aMaster.IsActive));
                return accessManager.SaveData("sp_SaveItemType", aSqlParameterList); 
            }
            catch (Exception exception)
            {
                accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public bool DeleteItemType(int id) 
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@ItemTypeId", id));

                return accessManager.SaveData("sp_DeleteItemType", aSqlParameterList);
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<ItemType> GetItemTypeList()
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ItemType> _List = new List<ItemType>();              

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetItemTypeList");
                while (dr.Read())
                {
                    ItemType aInfo = new ItemType();
                    aInfo.ItemTypeId = (int)dr["ItemTypeId"];
                    aInfo.ItemGroupId = (int)dr["ItemGroupId"];
                    aInfo.ItemTypeName = dr["ItemTypeName"].ToString();
                    aInfo.ItemGroupName = dr["ItemGroupName"].ToString();
                    if (dr["CreateDate"] != DBNull.Value)
                    {
                        aInfo.CreateDate = Convert.ToDateTime(dr["CreateDate"]);
                    }
                    aInfo.CreateBy = dr["CreateBy"].ToString();

                    aInfo.UpdateBy = dr["UpdateBy"].ToString();
                    if (dr["UpdateDate"] != DBNull.Value)
                    {
                        aInfo.UpdateDate = Convert.ToDateTime(dr["UpdateDate"]);
                    }
                    if (dr["IsActive"] != DBNull.Value)
                    {
                        aInfo.IsActive = Convert.ToBoolean(dr["IsActive"]);
                    }

                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public ItemType GetItemTypeByID(int id) 
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ItemTypeId", id));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetItemTypeByID", aParameters);
                ItemType aDetailsView = new ItemType(); 
                while (dr.Read())
                {

                    aDetailsView.ItemTypeId = (int)dr["ItemTypeId"];
                    aDetailsView.ItemGroupId = (int)dr["ItemGroupId"];
                    aDetailsView.ItemTypeName = dr["ItemTypeName"].ToString();
                    aDetailsView.IsActive = (bool)dr["IsActive"];


                }
                return aDetailsView;

            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }


    }
}