﻿using IGFoodProject.DataManager;
using IGFoodProject.Models;
using IGFoodProject.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Configuration;
using System.Web;

namespace IGFoodProject.DAL
{
    public class TradeSalesOrderDAL
    {

        DataAccessManager accessManager = new DataAccessManager();
        public List<Product> GetProductForTrade()
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_ProductsFTrade");

                List<Product> aList = new List<Product>();
                while (dr.Read())
                {
                    Product aProduct = new Product();
                    aProduct.ProductNo = dr["ProductNo"].ToString();
                    aProduct.TypeId = Convert.ToInt32(dr["TypeId"]);
                    aProduct.ProductId = Convert.ToInt32(dr["ProductId"]);
                    aProduct.ProductDescription = dr["ProductDescription"].ToString();
                    aProduct.ProductName = dr["ProductName"].ToString();
                    aList.Add(aProduct);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public ViewProductDetailsSales GetProductDetailsFTrade(int id, DateTime orderdate)
        {
            try
            {
                ViewProductDetailsSales aDetails = new ViewProductDetailsSales();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                aParameters.Add(new SqlParameter("@orderdate", orderdate));
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_ProductStockDetailsFTrade", aParameters);
                while (dr.Read())
                {
                    aDetails.ProductID = Convert.ToInt32(dr["ProductID"]);
                    aDetails.StockQty = DBNull.Value.Equals(dr["StockQty"]) ? 0 : Convert.ToDecimal(dr["StockQty"]);
                    aDetails.StockKg = DBNull.Value.Equals(dr["StockKg"]) ? 0 : Convert.ToDecimal(dr["StockKg"]);
                    aDetails.groupid = Convert.ToInt32(dr["GroupId"]);
                    aDetails.UnitPrice = DBNull.Value.Equals(dr["SalePrice"]) ? 0 : Convert.ToDecimal(dr["SalePrice"]);
                }
                return aDetails;
            }
            catch (Exception ex)
            {

                throw ex;
            }

            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public ResultResponse SaveSalesOrderProcessed(SalesOrderMasterTrade aMaster, string user)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@TradeSalesOrderMasterId", aMaster.TradeSalesOrderMasterId));
                aParameters.Add(new SqlParameter("@CustomerTypeId", aMaster.CustomerTypeId));
                aParameters.Add(new SqlParameter("@CustomerId", aMaster.CustomerId));
                //aParameters.Add(new SqlParameter("@SalesPerson", aMaster.SalesPerson));
                if (aMaster.IsEmployee == true)
                {
                    aParameters.Add(new SqlParameter("@SalesPerson", aMaster.SalesPerson.Split(':')[0].Trim()));
                }
                else
                {
                    aParameters.Add(new SqlParameter("@SalesPerson", aMaster.SalesPerson));
                }
                aParameters.Add(new SqlParameter("@OrderDate", aMaster.OrderDate));
                aParameters.Add(new SqlParameter("@DeliveryDestination", aMaster.DeliveryDestination));
                aParameters.Add(new SqlParameter("@DeliveryRcvPerson", aMaster.DeliveryRcvPerson));
                aParameters.Add(new SqlParameter("@DeliveryRcvPersonNumber", aMaster.DeliveryRcvPersonNumber));
                aParameters.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aParameters.Add(new SqlParameter("@CreateDate", aMaster.CreateDate));
                aParameters.Add(new SqlParameter("@CompanyId", aMaster.CompanyId));
                aParameters.Add(new SqlParameter("@SalesPersonContact", aMaster.SalesPersonContact));
                aParameters.Add(new SqlParameter("@CollectionType", aMaster.CollectionType));
                aParameters.Add(new SqlParameter("@DeliveryDate", aMaster.DeliveryDate));
                aParameters.Add(new SqlParameter("@TotatalOrderAmtKg", aMaster.TotatalOrderAmtKg));
                aParameters.Add(new SqlParameter("@TotatalOrderAmtQty", aMaster.TotatalOrderAmtQty));
                aParameters.Add(new SqlParameter("@TotalAmount", aMaster.TotalAmount));
                aParameters.Add(new SqlParameter("@TotalDiscount", aMaster.TotalDiscount));
                aParameters.Add(new SqlParameter("@Remarks", aMaster.Remarks));
                aParameters.Add(new SqlParameter("@TotalDue", aMaster.TotalDue));
                aParameters.Add(new SqlParameter("@TotalPaid", aMaster.TotalPaid));
                aParameters.Add(new SqlParameter("@BankId", aMaster.BankId));
                aParameters.Add(new SqlParameter("@BranchId", aMaster.BranchId));
                aParameters.Add(new SqlParameter("@ChequeNo", aMaster.ChequeNo));
                aParameters.Add(new SqlParameter("@ChequeDate", aMaster.ChequeDate));

                aResponse.pk = accessManager.SaveDataReturnPrimaryKey("sp_SaveTradeSalesOrder", aParameters);
                if (aResponse.pk > 0)
                {
                    if (aMaster.IsApprove == 1 || aMaster.IsApprove == 0) // if Approve or new Entry
                    {
                        aResponse.isSuccess = SaveSalesOrderProcessedDetails(aMaster.SalesDetailsTrade, aResponse.pk, user, aMaster.IsApprove);
                    }
                    if (aMaster.IsApprove == 2) // if Reject
                    {
                        aResponse.isSuccess = true;
                    }

                    if (aResponse.isSuccess == false)
                    {
                        accessManager.SqlConnectionClose(true);
                    }
                }
                else
                {
                    accessManager.SqlConnectionClose(true);
                }
                return aResponse;
            }
            catch (Exception ex)
            {
                accessManager.SqlConnectionClose(true);
                throw ex;
            }

            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public bool SaveSalesOrderProcessedDetails(List<SalesOrderDetailsTrade> aList, int pk, string user, int isApprove)
        {
            try
            {
                bool result = false;
                foreach (var item in aList)
                {
                    List<SqlParameter> aParameters = new List<SqlParameter>();
                    aParameters.Add(new SqlParameter("@TradeSalesOrderMasterId", pk));
                    aParameters.Add(new SqlParameter("@TradeSalesOrderDetailsId", item.TradeSalesOrderDetailsId));
                    aParameters.Add(new SqlParameter("@ProductId", item.ProductId));
                    aParameters.Add(new SqlParameter("@ProductQty", item.ProductQty));
                    aParameters.Add(new SqlParameter("@ProductKg", item.ProductKg));
                    aParameters.Add(new SqlParameter("@SaleRate", item.SaleRate));
                    aParameters.Add(new SqlParameter("@BasePrice", item.BasePrice));
                    aParameters.Add(new SqlParameter("@DiscountAmount", item.DiscountAmount));
                    aParameters.Add(new SqlParameter("@DiscountPercent", item.DiscountPercent));
                    aParameters.Add(new SqlParameter("@TotalPrice", item.TotalPrice));
                    aParameters.Add(new SqlParameter("@CreateBy",user ));
                    aParameters.Add(new SqlParameter("@CreateDate", DateTime.Now));
                    aParameters.Add(new SqlParameter("@GroupId", item.GroupId));
                    aParameters.Add(new SqlParameter("@IsDelete", item.IsDelete));
                    aParameters.Add(new SqlParameter("@OperationIdAdd", item.OperationIdAdd));
                    aParameters.Add(new SqlParameter("@OperationIdDelete", item.OperationIdDelete));
                    aParameters.Add(new SqlParameter("@DetailsRemarks", item.DetailsRemarks));
                    result = accessManager.SaveData("sp_SaveTradeSalesDetails", aParameters);
                    if (result == false)
                    {
                        break;
                    }

                }
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public List<SalesOrderMasterTrade> GetSalesOrderProcessedMaster(DateTime? fromdate, DateTime? todate, string salesperson, int customerid = 0)
        {
            try
            {

                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-1);

                }
                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }
                if (salesperson != null)
                {
                    salesperson = salesperson.Split(':')[0].Trim();
                }
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<SalesOrderMasterTrade> aList = new List<SalesOrderMasterTrade>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@customerid", customerid));
                aParameters.Add(new SqlParameter("@SalesPerson", salesperson));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_TradeSalesOrderProcessedList", aParameters);
                while (dr.Read())
                {
                    SalesOrderMasterTrade aMaster = new SalesOrderMasterTrade();

                    aMaster.TradeSalesOrderMasterId = Convert.ToInt32(dr["TradeSalesOrderMasterId"]);
                    aMaster.TotatalOrderAmtKg = Convert.ToDecimal(dr["TotatalOrderAmtKg"]);
                    aMaster.TotatalOrderAmtQty = Convert.ToDecimal(dr["TotatalOrderAmtQty"]);
                    aMaster.TotalAmount = Convert.ToDecimal(dr["TotalAmount"]);
                    aMaster.TotalDiscount = Convert.ToDecimal(dr["TotalDiscount"]);
                    aMaster.TotalDue = Convert.ToDecimal(dr["TotalDue"]);
                    aMaster.TotalPaid = Convert.ToDecimal(dr["TotalPaid"]);

                    aMaster.SalesPerson = dr["SalesPerson"].ToString();
                    aMaster.SalesOrderNo = dr["SalesOrderNo"].ToString();
                    aMaster.CustomerName = dr["CustomerName"].ToString();
                    aMaster.DeliveryDestination = dr["DeliveryDestination"].ToString();
                    aMaster.SalesPersonContact = dr["SalesPersonContact"].ToString();

                    aMaster.CreateBy = dr["CreateBy"].ToString();
                    aMaster.ApproveBy = dr["ApproveBy"].ToString();
                    aMaster.ApproveDate = dr["ApproveDate"] as DateTime?;
                    aMaster.CreateDate = dr["CreateDate"] as DateTime?;
                    aMaster.OrderDate = dr["OrderDate"] as DateTime?;
                    aMaster.DeliveryDate = dr["DeliveryDate"] as DateTime?;
                    if (DBNull.Value == dr["IsApprove"])
                    {
                        aMaster.IsApprove = 0;
                    }
                    else
                    {
                        aMaster.IsApprove = Convert.ToInt32(dr["IsApprove"]);
                    }
                    aMaster.CustomerType = dr["CoustomerTypeName"].ToString();
                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public SalesOrderMasterTrade GetSalesOrderProcessedMaster(int id)
        {
            try
            {
                List<SqlParameter> aParameters = new List<SqlParameter>();
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                aParameters.Add(new SqlParameter("@id", id));
                DataTable dt = accessManager.GetDataTable("sp_GETTradeSalesOrderProcessedMasrerById", aParameters);
                List<SalesOrderMasterTrade> aList = Helper.ToListof<SalesOrderMasterTrade>(dt);
                return aList[0];
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public List<SalesOrderDetailsTrade> GetSalesOderProcessedDetailsByMaster(int id)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                DataTable dt = accessManager.GetDataTable("sp_GetTradeSalesOrderPrecessedDetailsByMaster", aParameters);
                List<SalesOrderDetailsTrade> aList = Helper.ToListof<SalesOrderDetailsTrade>(dt);
                return aList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
        public ResultResponse ApproveSalesOrder(int id, bool status, string user)
        {
            try
            {
                ResultResponse aResponse = new ResultResponse();
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@id", id));
                aParameters.Add(new SqlParameter("@status", status));
                aParameters.Add(new SqlParameter("@user", user));

                aResponse.isSuccess = accessManager.SaveData("spApproveTradeSales", aParameters);
                return aResponse;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<ViewSalesOrderProcessedMaster> GetSalesOrderProcessedMasterForInvoice(DateTime? fromdate, DateTime? todate, string salesperson, int customerid)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<ViewSalesOrderProcessedMaster> aList = new List<ViewSalesOrderProcessedMaster>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@customerid", customerid));
                aParameters.Add(new SqlParameter("@SalesPerson", salesperson?.Split(':')[0].Trim() ?? ""));
                aParameters.Add(new SqlParameter("@FromDate", fromdate));
                aParameters.Add(new SqlParameter("@ToDate", todate));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_TradeSalesListForInvoice", aParameters);
                while (dr.Read())
                {
                    ViewSalesOrderProcessedMaster aMaster = new ViewSalesOrderProcessedMaster();

                    aMaster.SalesOrderProcessedMasterId = Convert.ToInt32(dr["SalesOrderProcessedMasterId"]);
                    aMaster.TotatalOrderAmtKg = Convert.ToDecimal(dr["TotatalOrderAmtKg"]);
                    aMaster.TotalIncTaxVat = Convert.ToDecimal(dr["TotalIncTaxVat"]);
                    aMaster.TotalExTaxVat = Convert.ToDecimal(dr["TotalExTaxVat"]);
                    aMaster.TotalDue = Convert.ToDecimal(dr["TotalDue"]);
                    aMaster.TotalPaid = Convert.ToDecimal(dr["TotalPaid"]);

                    aMaster.SalesPerson = dr["SalesPerson"].ToString();
                    aMaster.SalesOrderNo = dr["SalesOrderNo"].ToString();
                    aMaster.CustomerName = dr["CustomerName"].ToString();
                    aMaster.DeliveryDestination = dr["DeliveryDestination"].ToString();
                    aMaster.SalesPersonContact = dr["SalesPersonContact"].ToString();

                    aMaster.CreateBy = dr["CreateBy"].ToString();
                    aMaster.ApproveBy = dr["ApproveBy"].ToString();
                    aMaster.ApproveDate = dr["ApproveDate"] as DateTime?;
                    aMaster.CreateDate = dr["CreateDate"] as DateTime?;
                    aMaster.OrderDate = dr["OrderDate"] as DateTime?;
                    aMaster.DeliveryDate = dr["DeliveryDate"] as DateTime?;
                    if (DBNull.Value == dr["IsApprove"])
                    {
                        aMaster.IsApprove = 0;
                    }
                    else
                    {
                        aMaster.IsApprove = Convert.ToInt32(dr["IsApprove"]);
                    }
                    
                    aMaster.CustomerType = dr["CoustomerTypeName"].ToString();
                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<ViewSalesOrderProcessedMaster> GetSalesOrderProcessedMasterForApprove(DateTime? fromdate, DateTime? todate, string salesperson, int customerid = 0)
        {
            try
            {

                if (fromdate == null)
                {
                    fromdate = System.DateTime.Now.AddDays(-1);

                }
                if (todate == null)
                {
                    todate = System.DateTime.Now;

                }
                if (salesperson != null)
                {
                    salesperson = salesperson.Split(':')[0].Trim();
                }
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<ViewSalesOrderProcessedMaster> aList = new List<ViewSalesOrderProcessedMaster>();
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@customerid", customerid));
                aParameters.Add(new SqlParameter("@SalesPerson", salesperson));
                aParameters.Add(new SqlParameter("@FromDate", String.Format("{0:MM/dd/yyyy}", fromdate)));
                aParameters.Add(new SqlParameter("@ToDate", String.Format("{0:MM/dd/yyyy}", todate)));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_Get_TradeSalesOrderProcessedApproveList", aParameters);
                while (dr.Read())
                {
                    ViewSalesOrderProcessedMaster aMaster = new ViewSalesOrderProcessedMaster();

                    aMaster.SalesOrderProcessedMasterId = Convert.ToInt32(dr["SalesOrderProcessedMasterId"]);
                    aMaster.TotatalOrderAmtKg = Convert.ToDecimal(dr["TotatalOrderAmtKg"]);
                    aMaster.TotalIncTaxVat = Convert.ToDecimal(dr["TotalIncTaxVat"]);
                    aMaster.TotalExTaxVat = Convert.ToDecimal(dr["TotalExTaxVat"]);
                    aMaster.TotalDue = Convert.ToDecimal(dr["TotalDue"]);
                    aMaster.TotalPaid = Convert.ToDecimal(dr["TotalPaid"]);

                    aMaster.SalesPerson = dr["SalesPerson"].ToString();
                    aMaster.SalesOrderNo = dr["SalesOrderNo"].ToString();
                    aMaster.CustomerName = dr["CustomerName"].ToString();
                    aMaster.DeliveryDestination = dr["DeliveryDestination"].ToString();
                    aMaster.SalesPersonContact = dr["SalesPersonContact"].ToString();

                    aMaster.CreateBy = dr["CreateBy"].ToString();
                    aMaster.ApproveBy = dr["ApproveBy"].ToString();
                    aMaster.ApproveDate = dr["ApproveDate"] as DateTime?;
                    aMaster.CreateDate = dr["CreateDate"] as DateTime?;
                    aMaster.OrderDate = dr["OrderDate"] as DateTime?;
                    aMaster.DeliveryDate = dr["DeliveryDate"] as DateTime?;
                    if (DBNull.Value == dr["IsApprove"])
                    {
                        aMaster.IsApprove = 0;
                    }
                    else
                    {
                        aMaster.IsApprove = Convert.ToInt32(dr["IsApprove"]);
                    }

                    aMaster.CustomerType = dr["CoustomerTypeName"].ToString();
                    aList.Add(aMaster);
                }
                return aList;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }
    }
}