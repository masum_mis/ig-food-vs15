﻿using IGFoodProject.DataManager;
using IGFoodProject.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IGFoodProject.DAL
{
    public class ItemGroupDAL
    {
        DataAccessManager accessManager = new DataAccessManager();
        public int CheckItemGroup(string group) 
        {
            int pgroup = 0;
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@group", group));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_CheckItemGroup", aSqlParameterList);

                while (dr.Read())
                {
                    pgroup = Convert.ToInt32(dr["pgroup"]);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
            return pgroup;
        }

        public bool SaveItemGroup(ItemGroup aMaster)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@ItemGroupId", aMaster.ItemGroupId));
                aSqlParameterList.Add(new SqlParameter("@ItemGroupName", aMaster.ItemGroupName));
                aSqlParameterList.Add(new SqlParameter("@CreateDate", System.DateTime.Now));
                aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@UpdateDate", System.DateTime.Now));
                aSqlParameterList.Add(new SqlParameter("@UpdateBy", aMaster.UpdateBy));
                aSqlParameterList.Add(new SqlParameter("@IsActive", aMaster.IsActive));

                return accessManager.SaveData("sp_SaveItemGroup", aSqlParameterList);
            }
            catch (Exception exception)
            {
                accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public bool DeleteItemGroup(int id) 
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@ItemGroupId", id));

                return accessManager.SaveData("sp_DeleteItemGroup", aSqlParameterList);
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public List<ItemGroup> GetItemGroupList() 
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ItemGroup> _List = new List<ItemGroup>();                

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetItemGroupList");
                while (dr.Read())
                {
                    ItemGroup aInfo = new ItemGroup();
                    aInfo.ItemGroupId = (int)dr["ItemGroupId"];
                    aInfo.ItemGroupName = dr["ItemGroupName"].ToString();
                    if (dr["CreateDate"] != DBNull.Value)
                    {
                        aInfo.CreateDate = Convert.ToDateTime(dr["CreateDate"]);
                    }
                    aInfo.CreateBy = dr["CreateBy"].ToString();

                    aInfo.UpdateBy = dr["UpdateBy"].ToString();
                    if (dr["UpdateDate"] != DBNull.Value)
                    {
                        aInfo.UpdateDate = Convert.ToDateTime(dr["UpdateDate"]);
                    }
                    if (dr["IsActive"] != DBNull.Value)
                    {
                        aInfo.IsActive = Convert.ToBoolean(dr["IsActive"]);
                    }


                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public ItemGroup GetItemGroupByID(int id) 
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@ItemGroupId", id));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetItemGroupByID", aParameters);
                ItemGroup aDetailsView = new ItemGroup(); 
                while (dr.Read())
                {


                    aDetailsView.ItemGroupId = (int)dr["ItemGroupId"];
                    aDetailsView.ItemGroupName = dr["ItemGroupName"].ToString();
                    aDetailsView.IsActive = (bool)dr["IsActive"];


                }
                return aDetailsView;

            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

    }
}