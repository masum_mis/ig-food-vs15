﻿using IGFoodProject.DataManager;
using IGFoodProject.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web.DynamicData.ModelProviders;
using IGFoodProject.Utility;
using IGFoodProject.ViewModel;
namespace IGFoodProject.DAL
{
    public class MrRDAL
    {
        DataAccessManager _accessManager = new DataAccessManager();
        public List<ViewPurchaseOrderDDL> GetRemainingPurchaseOrder(int locationId,int warehouseId)
        {
            try
            {
                List<ViewPurchaseOrderDDL> aList = new List<ViewPurchaseOrderDDL>();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                //parameters.Add(new SqlParameter("@locationId", locationId));
                parameters.Add(new SqlParameter("@wareHouseId", warehouseId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_RemainingPurchaseOrders", parameters);
                while (dr.Read())
                {
                    ViewPurchaseOrderDDL orderMaster = new ViewPurchaseOrderDDL();
                    orderMaster.PurchaseOrderID = Convert.ToInt32(dr["PurchaseOrderID"]);
                    orderMaster.PurchaseOrderNo = dr["PurchaseOrderNo"].ToString();
                    orderMaster.SupplierName = dr["SupplierName"].ToString();
                    orderMaster.SupplierCode = dr["SupplierCode"].ToString();
                    aList.Add(orderMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public ViewPurchaseOrderMaster GetPurchaseOrderMasterView(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@PurchaseOrderID", id));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_PurchaseOrderByIdForMrr" , parameters);
                ViewPurchaseOrderMaster aMaster = new ViewPurchaseOrderMaster();
                while (dr.Read()) {
                    aMaster.PurchaseOrderID = Convert.ToInt32(dr["PurchaseOrderID"]);
                    aMaster.PurchaseOrderNo = dr["PurchaseOrderNo"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.SupplierName = dr["SupplierName"].ToString();
                    aMaster.OrderBY = dr["OrderBY"].ToString();
                    aMaster.OrderDate = dr["OrderDate"]as DateTime?;
                }
                return aMaster;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<ViewPurchaseOrderDetails> GetPurchaseOrderDetailsView(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@PurchaseOrderID", id));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_Get_PurchaseOrderDetailsByIdForMrr", parameters);
                List<ViewPurchaseOrderDetails> aList = new List<ViewPurchaseOrderDetails>();
                while (dr.Read())
                {
                    ViewPurchaseOrderDetails aDetails = new ViewPurchaseOrderDetails();
                    aDetails.PurchaseOrderID = Convert.ToInt32(dr["PurchaseOrderID"]);
                    aDetails.PurchaseOrderDetailID = Convert.ToInt32(dr["PurchaseOrderDetailID"]);
                    aDetails.ItemId = Convert.ToInt32(dr["ItemId"]);
                    aDetails.ItemDescription = dr["ItemDescription"].ToString();
                    aDetails.Remarks = dr["Remarks"].ToString();
                    aDetails.PurchaseQty = Convert.ToDecimal(dr["PurchaseQty"].ToString());
                    aDetails.PurchaseKG = Convert.ToDecimal(dr["PurchaseKG"].ToString());
                    aDetails.UnitPrice = Convert.ToDecimal(dr["UnitPrice"].ToString());
                    aDetails.TotalPrice = Convert.ToDecimal(dr["TotalPrice"].ToString());
                    aDetails.MRRRemainingKG = Convert.ToDecimal(dr["MRRRemainingKG"].ToString());
                    aDetails.MRRRemainingQty = Convert.ToDecimal(dr["MRRRemainingQty"].ToString());
                    aDetails.toleranceqty = Convert.ToDecimal(dr["toleranceqty"].ToString());
                    aDetails.preqty = Convert.ToDecimal(dr["preqty"].ToString());
                    aList.Add(aDetails);
                }
                return aList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public ResultResponse SaveMrrMaster(MrRMaster mrRMaster , string user)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                ResultResponse resultResponse = new ResultResponse();
                parameters.Add(new SqlParameter("@PurchaseOrderID", mrRMaster.PurchaseOrderID));
                parameters.Add(new SqlParameter("@MRRMasterId", mrRMaster.MRRMasterId));
                parameters.Add(new SqlParameter("@WareHouseId", mrRMaster.WareHouseId));
                parameters.Add(new SqlParameter("@ReceiveTime", mrRMaster.ReceiveTime));
                parameters.Add(new SqlParameter("@TotalReceiveQty", mrRMaster.TotalReceiveQty));
                parameters.Add(new SqlParameter("@TotalReceiveKg", mrRMaster.TotalReceiveKg));
                parameters.Add(new SqlParameter("@ReceiveBy", mrRMaster.ReceiveBy.Split(':')[0].Trim()));
                parameters.Add(new SqlParameter("@MRRDate", Convert.ToDateTime(mrRMaster.StrMRRDate)));  
                parameters.Add(new SqlParameter("@CreateBy",user));
                parameters.Add(new SqlParameter("@Remarks", mrRMaster.Remarks));
                parameters.Add(new SqlParameter("@MRRStatus", mrRMaster.MRRStatus));
                parameters.Add(new SqlParameter("@CreateDate", System.DateTime.Now));

                parameters.Add(new SqlParameter("@LaborCost", mrRMaster.LaborCost));
                parameters.Add(new SqlParameter("@TransportCost", mrRMaster.TransportCost));

                resultResponse.pk = _accessManager.SaveDataReturnPrimaryKey("sp_Save_MrrMaster" , parameters);
                resultResponse.isSuccess = SaveMrrDetails(mrRMaster.Details, resultResponse.pk);
                resultResponse.isSuccess = SaveMrrTotalDue(resultResponse.pk);
             resultResponse.isSuccess = CreateBirdPurchaseMrrJournal(resultResponse.pk);
             
                if (resultResponse.isSuccess == false)
                {
                    _accessManager.SqlConnectionClose(true);
                }
                return resultResponse;
            }
            catch (Exception ex)
            {
                _accessManager.SqlConnectionClose(true);
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        private bool CreateBirdPurchaseMrrJournal(int resultResponsePk)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@MRRMasterId", resultResponsePk));
                result = _accessManager.SaveData("BirdPurchaseMrrJournal", aParameters);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool CreateSystemInvoiceJournal(int MrrMasterId)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@Id", MrrMasterId));
                result = _accessManager.SaveData("SaveBirdPurchaseInvoiceJournal", aParameters);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool SaveMrrDetails(List<MrRDetails> aList , int pk)
        {
            try
            {
                bool insert = false;
                foreach (var item in aList)
                {
                    List<SqlParameter> parameters = new List<SqlParameter>();
                    parameters.Add(new SqlParameter("@MRRMasterId", pk));
                    parameters.Add(new SqlParameter("@MRRDetailID", item.MRRDetailID));
                    parameters.Add(new SqlParameter("@ItemID", item.ItemID));
                    parameters.Add(new SqlParameter("@ItemQty", item.ItemQty));
                    parameters.Add(new SqlParameter("@ItemKG", item.ItemKG));
                    parameters.Add(new SqlParameter("@Remarks", item.Remarks));
                    parameters.Add(new SqlParameter("@ReceiveStatus", item.ReceiveStatus));
                    parameters.Add(new SqlParameter("@PurchaseOrderDetailID", item.PurchaseOrderDetailID));
                    insert = _accessManager.SaveData("sp_Save_MrrDetais", parameters);
                    if (insert == false)
                    {
                        break;
                    }
                }
                return insert;
            }
            catch (Exception ex)
            {
                string err = ex.ToString();
                throw ex;
            }
        }
        public bool CreateSystemJournal(int MrrMasterId,  decimal? quantity)
        {
            try
            {
                bool result = false;
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@MRRMasterId", MrrMasterId));
                aParameters.Add(new SqlParameter("@MRRQty", quantity));
                result = _accessManager.SaveData("SaveJournalFromMRR", aParameters);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool SaveMrrTotalDue( int pk)
        {
            try
            {
                bool insert = false;
                    List<SqlParameter> parameters = new List<SqlParameter>();
                    parameters.Add(new SqlParameter("@MRRMasterID", pk));
                    insert = _accessManager.SaveData("sp_UpdateTotalDueInMRR", parameters);
                return insert;
            }
            catch (Exception ex)
            {
                string err = ex.ToString();
                throw ex;
            }
        }
        public MrRMaster GetMrRMaster(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@MRRMasterId", id));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetMrrMasterById",parameters);
                MrRMaster aMaster = new MrRMaster();
                while (dr.Read())
                {
                    aMaster.MRRMasterId = Convert.ToInt32(dr["MRRMasterId"]);
                    aMaster.PurchaseOrderID = Convert.ToInt32(dr["PurchaseOrderID"]);
                    aMaster.ReceiveBy = dr["ReceiveBy"].ToString();
                    aMaster.MRRNo = dr["MRRNo"].ToString();
                    aMaster.Remarks = dr["Remarks"].ToString();
                    aMaster.StrMRRDate = Convert.ToDateTime(dr["MRRDate"]).ToString("dd-MMM-yyyy");
                    aMaster.MRRStatus = Convert.ToInt32(dr["MRRStatus"]);
                    aMaster.WareHouseId = Convert.ToInt32(dr["WareHouseId"]);
                    //aMaster.LocationId = Convert.ToInt32(dr["LocationId"]);
                    //aMaster.CompanyId = Convert.ToInt32(dr["CompanyID"]);
                }
                return aMaster;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public ViewMrrMaster GetMrRMasterView(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@MRRMasterId", id));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetMrrMasterViewById", parameters);
                ViewMrrMaster aMaster = new ViewMrrMaster();
                while (dr.Read())
                {
                    aMaster.MRRMasterId = Convert.ToInt32(dr["MRRMasterId"]);
                    aMaster.PurchaseOrderID = Convert.ToInt32(dr["PurchaseOrderID"]);
                    aMaster.ReceiveBy = dr["ReceiveBy"].ToString();
                    aMaster.PurchaseOrderNo = dr["PurchaseOrderNo"].ToString();
                    aMaster.MRRNo = dr["MRRNo"].ToString();
                    aMaster.Remarks = dr["Remarks"].ToString();
                    aMaster.MRRDate = Convert.ToDateTime(dr["MRRDate"]);
                    aMaster.ReceiveTime =  dr["ReceiveTime"].ToString() ;
                   // var ist = asd.TimeOfDay;
                    aMaster.WareHouseId = Convert.ToInt32(dr["WareHouseId"]);
                    aMaster.WareHouseName= Convert.ToString(dr["WareHouseName"]);
                    aMaster.LocationId = Convert.ToInt32(dr["LocationId"]);
                    aMaster.LocationName = Convert.ToString(dr["LocationName"]);
                    aMaster.CompanyId = Convert.ToInt32(dr["CompanyID"]);
                    aMaster.CompanyName = Convert.ToString(dr["CompanyName"]);
                }
                return aMaster;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<ViewMrrDetails> GetMrrDetailsForEdit(int id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@MRRMasterId", id));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetMRRDetailsForEdit" , parameters);
                List<ViewMrrDetails> aList = new List<ViewMrrDetails>();
                while (dr.Read())
                {
                    ViewMrrDetails mrrDetails = new ViewMrrDetails();
                    mrrDetails.ItemDescription = dr["ItemDescription"].ToString();
                    mrrDetails.ItemId = Convert.ToInt32(dr["ItemId"]);
                    mrrDetails.MRRDetailID = Convert.ToInt32(dr["MRRDetailID"]);
                    mrrDetails.PurchaseOrderDetailID = Convert.ToInt32(dr["PurchaseOrderDetailID"]);
                    mrrDetails.PurchaseQty = Convert.ToDecimal(dr["PurchaseQty"]);
                    mrrDetails.MRRRemainingQty = Convert.ToDecimal(dr["MRRRemainingQty"]);
                    mrrDetails.MRRRemainingKG = Convert.ToDecimal(dr["MRRRemainingKG"]);
                    mrrDetails.ItemQty = Convert.ToDecimal(dr["ItemQty"]);
                    mrrDetails.ItemKG = Convert.ToDecimal(dr["ItemKG"]);
                    mrrDetails.ReceiveStatus = dr["ReceiveStatus"].ToString();
                    mrrDetails.pReceiveStatus = Convert.ToInt32(dr["pReceiveStatus"]);
                    mrrDetails.toleranceqty = Convert.ToDecimal(dr["toleranceqty"]);
                    mrrDetails.preqty = Convert.ToDecimal(dr["preqty"]);
                    mrrDetails.othqty = Convert.ToDecimal(dr["othqty"]);
                    aList.Add(mrrDetails);
                }
                return aList;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<ViewMrrMaster> GetMrrList()
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<ViewMrrMaster> aList = new List<ViewMrrMaster>();
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetMrrList");
                while (dr.Read())
                {
                    ViewMrrMaster aMaster = new ViewMrrMaster();
                    aMaster.MRRMasterId = Convert.ToInt32(dr["MRRMasterId"]);
                    aMaster.PurchaseOrderID = Convert.ToInt32(dr["PurchaseOrderID"]);
                    aMaster.MRRNo = dr["MRRNo"].ToString();
                    aMaster.MRRDate = Convert.ToDateTime(dr["MRRDate"]);
                    aMaster.CreateDate = dr["CreateDate"] as DateTime?;
                    aMaster.ReceiveBy = dr["ReceiveBy"].ToString();
                    aMaster.StatusDes = dr["StatusDes"].ToString();
                    aMaster.PurchaseOrderNo = dr["PurchaseOrderNo"].ToString();
                    aMaster.EditStatus = dr["EditStatus"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.TotalReceiveQty = Convert.ToDecimal(dr["TotalReceiveQty"]);
                    aMaster.TotalReceiveKg = Convert.ToDecimal(dr["TotalReceiveKg"]);
                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<ViewMrrMaster> GetMrrListByDate(DateTime? fromdate, DateTime? todate, string supplierName = "", string PONo = "")
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@FromDate", fromdate));
                parameters.Add(new SqlParameter("@ToDate", todate));
                parameters.Add(new SqlParameter("@supplierName", supplierName));
                parameters.Add(new SqlParameter("@PONo", PONo));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetMRRListByDate", parameters);
                List<ViewMrrMaster> aList = new List<ViewMrrMaster>();
                while (dr.Read())
                {
                    ViewMrrMaster aMaster = new ViewMrrMaster();
                    aMaster.MRRMasterId = Convert.ToInt32(dr["MRRMasterId"]);
                    aMaster.PurchaseOrderID = Convert.ToInt32(dr["PurchaseOrderID"]);
                    aMaster.MRRNo = dr["MRRNo"].ToString();
                    aMaster.MRRDateStr = dr["MRRDate"].ToString();
                    aMaster.CreateDateStr = dr["CreateDate"].ToString();
                    aMaster.ReceiveBy = dr["ReceiveBy"].ToString();
                    aMaster.StatusDes = dr["StatusDes"].ToString();
                    aMaster.PurchaseOrderNo = dr["PurchaseOrderNo"].ToString();
                    aMaster.EditStatus = dr["EditStatus"].ToString();

                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.TotalReceiveQty = Convert.ToDecimal(dr["TotalReceiveQty"]);
                    aMaster.TotalReceiveKg = Convert.ToDecimal(dr["TotalReceiveKg"]);
                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }

        public List<ViewMrrMaster> GetMrrCloseListByDate(DateTime? fromdate, DateTime? todate, string supplierName = "", string PONo = "")
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@FromDate", fromdate));
                parameters.Add(new SqlParameter("@ToDate", todate));
                parameters.Add(new SqlParameter("@supplierName", supplierName));
                parameters.Add(new SqlParameter("@PONo", PONo));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetMRRCloseListByDate", parameters);
                List<ViewMrrMaster> aList = new List<ViewMrrMaster>();
                while (dr.Read())
                {
                    ViewMrrMaster aMaster = new ViewMrrMaster();
                    aMaster.MRRMasterId = Convert.ToInt32(dr["MRRMasterId"]);
                    aMaster.PurchaseOrderID = Convert.ToInt32(dr["PurchaseOrderID"]);
                    aMaster.MRRNo = dr["MRRNo"].ToString();
                    aMaster.MRRDateStr = dr["MRRDate"].ToString();
                    aMaster.CreateDateStr = dr["CreateDate"].ToString();
                    aMaster.ReceiveBy = dr["ReceiveBy"].ToString();
                    aMaster.StatusDes = dr["StatusDes"].ToString();
                    aMaster.PurchaseOrderNo = dr["PurchaseOrderNo"].ToString();
                    aMaster.EditStatus = dr["EditStatus"].ToString();

                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.TotalReceiveQty = Convert.ToDecimal(dr["TotalReceiveQty"]);
                    aMaster.TotalReceiveKg = Convert.ToDecimal(dr["TotalReceiveKg"]);
                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public bool CloseMRR(ViewMrrMaster aMaster)
        {
            bool result = false;
            try

            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);

                List<SqlParameter> gSqlParameterList = new List<SqlParameter>();

                gSqlParameterList.Add(new SqlParameter("@MRRMasterId", aMaster.MRRMasterId));
                gSqlParameterList.Add(new SqlParameter("@ClosedBy", aMaster.ClosedBy));
                gSqlParameterList.Add(new SqlParameter("@ClosedDate", aMaster.ClosedDate));
                gSqlParameterList.Add(new SqlParameter("@IsClosed", 1));
                result = _accessManager.SaveData("sp_CloseMRR", gSqlParameterList);

                return result;
            }
            catch (Exception exception)
            {
                _accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public List<ViewMrrMaster> GetMrrListForPrintInvoice(int warehouseId, DateTime fromDate,DateTime toDate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@WareHouseId", warehouseId));
                parameters.Add(new SqlParameter("@FromDate", fromDate));
                parameters.Add(new SqlParameter("@ToDate", toDate));
                List<ViewMrrMaster> aList = new List<ViewMrrMaster>();
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetMrrList", parameters);
                while (dr.Read())
                {
                    ViewMrrMaster aMaster = new ViewMrrMaster();
                    aMaster.MRRMasterId = Convert.ToInt32(dr["MRRMasterId"]);
                    aMaster.PurchaseOrderID = Convert.ToInt32(dr["PurchaseOrderID"]);
                    aMaster.MRRNo = dr["MRRNo"].ToString();
                    aMaster.MRRDate = Convert.ToDateTime(dr["MRRDate"]);
                    aMaster.CreateDate = dr["CreateDate"] as DateTime?;
                    aMaster.ReceiveBy = dr["ReceiveBy"].ToString();
                    aMaster.StatusDes = dr["StatusDes"].ToString();
                    aMaster.PurchaseOrderNo = dr["PurchaseOrderNo"].ToString();
                    aMaster.EditStatus = dr["EditStatus"].ToString();
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.TotalReceiveQty = Convert.ToDecimal(dr["TotalReceiveQty"]);
                    aMaster.TotalReceiveKg = Convert.ToDecimal(dr["TotalReceiveKg"]);
                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public DataTable GetMrrDetailsForPrint(string id)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@MrrId", id));
                DataTable dt = _accessManager.GetDataTable("sp_RptGetMrrInvoice", aParameters);
                return dt;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
        public List<tbl_SupplierInfo> SupplierInformation(int PONo)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@PONo", PONo));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetSupplierInfoByPO", parameters);
                List<tbl_SupplierInfo> aList = new List<tbl_SupplierInfo>();
                while (dr.Read())
                {
                    tbl_SupplierInfo supplier = new tbl_SupplierInfo();
                    supplier.SupplierID = Convert.ToInt32(dr["SupplierID"]);
                    supplier.SupplierName = dr["SupplierName"].ToString();
                    aList.Add(supplier);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public List<ViewMrrMaster> LoadPO(string fromdate, string todate)
        {
            try
            {
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@FromDate", fromdate));
                parameters.Add(new SqlParameter("@ToDate", todate));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetPO", parameters);
                List<ViewMrrMaster> aList = new List<ViewMrrMaster>();
                while (dr.Read())
                {
                    ViewMrrMaster PurchaseOrderList = new ViewMrrMaster();
                    PurchaseOrderList.PurchaseOrderID = Convert.ToInt32(dr["PurchaseOrderID"]);
                    PurchaseOrderList.PurchaseOrderNo = dr["PurchaseOrderNo"].ToString();
                    aList.Add(PurchaseOrderList);
                }
                return aList;
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }

        }
        public List<ViewMrrMaster> GetMrrListForReport(DateTime fromDate,DateTime toDate, int warehouseId)
        {
            try
            {
                List<ViewMrrMaster> aList = new List<ViewMrrMaster>();
                _accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@FromDate", fromDate));
                parameters.Add(new SqlParameter("@ToDate", toDate));
                parameters.Add(new SqlParameter("@WarehouseId", warehouseId));
                SqlDataReader dr = _accessManager.GetSqlDataReader("sp_GetMrrInformationForReport", parameters);
                while (dr.Read())
                {
                    ViewMrrMaster aMaster = new ViewMrrMaster();
                    aMaster.MRRMasterId = Convert.ToInt32(dr["MRRMasterId"]);
                    aMaster.MRRNo = dr["MRRNo"].ToString();
                    aMaster.MRRDate = Convert.ToDateTime(dr["MRRDate"]);
                    aMaster.ReceiveBy = dr["ReceiveBy"].ToString();
                    aMaster.DressedComplete =DBNull.Value.Equals(dr["DressedComplete"]) ?0: Convert.ToInt32(dr["DressedComplete"]);
                    aMaster.Remarks = dr["Remarks"].ToString();
                    aMaster.MRRStatus = Convert.ToInt32(dr["MRRStatus"]);
                    aMaster.WareHouseName = dr["WareHouseName"].ToString();
                    aMaster.TotalReceiveQty = Convert.ToDecimal(dr["TotalReceiveQty"]);
                    aMaster.TotalReceiveKg = Convert.ToDecimal(dr["TotalReceiveKg"]);
                    aMaster.ReceiveTime = dr["ReceiveTime"].ToString();
                    aMaster.PurchaseOrderNo = dr["PurchaseOrderNo"].ToString();
                    aMaster.OrderDate = Convert.ToDateTime(dr["OrderDate"]);
                    aMaster.SupplierName = dr["SupplierName"].ToString();
                    aList.Add(aMaster);
                }
                return aList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _accessManager.SqlConnectionClose();
            }
        }
    }
}
