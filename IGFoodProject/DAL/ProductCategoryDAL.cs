﻿using IGFoodProject.DataManager;
using IGFoodProject.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.DAL
{
    public class ProductCategoryDAL
    {
        DataAccessManager accessManager = new DataAccessManager();

        public List<tbl_ProductCategory> GetCategoryList()
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<tbl_ProductCategory> _List = new List<tbl_ProductCategory>();

                //List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetProductCategory");
                while (dr.Read())
                {
                    tbl_ProductCategory aInfo = new tbl_ProductCategory();
                    aInfo.TypeId = (int)dr["TypeId"];
                    aInfo.CategoryId = (int)dr["CategoryId"];
                    aInfo.GroupId = (int)dr["GroupId"];
                    aInfo.GroupName = dr["GroupName"].ToString();
                    aInfo.TypeName = dr["TypeName"].ToString();
                    aInfo.CategoryCode = dr["CategoryCode"].ToString();
                    aInfo.CategoryName = dr["CategoryName"].ToString();
                    if (dr["CreateDate"] != DBNull.Value)
                    {
                        aInfo.CreateDate = Convert.ToDateTime(dr["CreateDate"]);
                    }
                    aInfo.CreateBy = dr["CreateBy"].ToString();

                    aInfo.UpdateBy = dr["UpdateBy"].ToString();
                    if (dr["UpdateDate"] != DBNull.Value)
                    {
                        aInfo.UpdateDate = Convert.ToDateTime(dr["UpdateDate"]);
                    }
                    if (dr["IsActive"] != DBNull.Value)
                    {
                        aInfo.IsActive = Convert.ToBoolean(dr["IsActive"]);
                    }


                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public int CheckCategory(string ptype)
        {
            int p_type = 0;
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@type", ptype));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_CheckCategory", aSqlParameterList);

                while (dr.Read())
                {
                    p_type = Convert.ToInt32(dr["p_type"]);
                }
            }
            catch (Exception exception)
            {
                //accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
            return p_type;
        }

        public int CheckCategoryCode(string ptype)
        {
            int p_type = 0;
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@type", ptype));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_CheckCategoryCode", aSqlParameterList);

                while (dr.Read())
                {
                    p_type = Convert.ToInt32(dr["p_type"]);
                }
            }
            catch (Exception exception)
            {
                //accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
            return p_type;
        }
        public bool SaveCategory(tbl_ProductCategory aMaster)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@CategoryId", aMaster.CategoryId));
                aSqlParameterList.Add(new SqlParameter("@GroupId", aMaster.GroupId));
                aSqlParameterList.Add(new SqlParameter("@TypeId", aMaster.TypeId));
                aSqlParameterList.Add(new SqlParameter("@CategoryName", aMaster.CategoryName));
                aSqlParameterList.Add(new SqlParameter("@CategoryCode", aMaster.CategoryCode));
                aSqlParameterList.Add(new SqlParameter("@CreateDate", System.DateTime.Now));
                aSqlParameterList.Add(new SqlParameter("@CreateBy", aMaster.CreateBy));
                aSqlParameterList.Add(new SqlParameter("@UpdateDate", System.DateTime.Now));
                aSqlParameterList.Add(new SqlParameter("@UpdateBy", aMaster.UpdateBy));
                aSqlParameterList.Add(new SqlParameter("@IsActive", aMaster.IsActive));



                return accessManager.SaveData("sp_SaveCategory", aSqlParameterList);
            }
            catch (Exception exception)
            {
                accessManager.SqlConnectionClose(true);
                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public tbl_ProductCategory GetCategoryForEdit(int id)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aParameters = new List<SqlParameter>();
                aParameters.Add(new SqlParameter("@CategoryId", id));

                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetCategoryByID", aParameters);
                tbl_ProductCategory aDetailsView = new tbl_ProductCategory();
                while (dr.Read())
                {

                    aDetailsView.TypeId = (int)dr["TypeId"];
                    aDetailsView.GroupId = (int)dr["GroupId"];
                    aDetailsView.CategoryId = (int)dr["CategoryId"];
                    aDetailsView.CategoryName = dr["CategoryName"].ToString();
                    aDetailsView.CategoryCode = dr["CategoryCode"].ToString();
                    aDetailsView.IsActive = (bool)dr["IsActive"];


                }
                return aDetailsView;

            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

        public bool DeleteCategory(int id)
        {
            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();

                aSqlParameterList.Add(new SqlParameter("@CategoryID", id));

                return accessManager.SaveData("sp_DeleteCategory", aSqlParameterList);
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }


        public List<tbl_ProductType> GetTypeList(int groupid)
        {

            try
            {
                accessManager.SqlConnectionOpen(DataBase.IGFoodDB);
                List<tbl_ProductType> _List = new List<tbl_ProductType>();

                List<SqlParameter> aSqlParameterList = new List<SqlParameter>();
                aSqlParameterList.Add(new SqlParameter("@GroupId", groupid));
                SqlDataReader dr = accessManager.GetSqlDataReader("sp_GetProductTypeByGroup", aSqlParameterList);
                while (dr.Read())
                {
                    tbl_ProductType aInfo = new tbl_ProductType();
                    aInfo.TypeId = (int)dr["TypeId"];
                   
                    aInfo.TypeName = dr["TypeName"].ToString();
                    


                    _List.Add(aInfo);
                }
                return _List;
            }
            catch (Exception exception)
            {

                throw exception;
            }
            finally
            {
                accessManager.SqlConnectionClose();
            }
        }

    }
}
