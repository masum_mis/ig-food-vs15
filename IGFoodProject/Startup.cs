﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute("IGFoodProject", typeof(IGFoodProject.Startup))]
namespace IGFoodProject
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
