﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class WarehousePermission
    {
        public int WarehousePermissionId { get; set; }
        public int LoginUserId { get; set; }
        public int WarehouseId { get; set; }
        public bool IsActive { get; set; }
    }
}