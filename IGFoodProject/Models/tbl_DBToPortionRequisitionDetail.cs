﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.Models
{
    public class tbl_DBToPortionRequisitionDetail
    {
        public int DBToPortionRequisitionDetailID { get; set; }
        public int DBToPortionReqID { get; set; }
        public int ProductID { get; set; }
        public  string ProductName { get; set; }
        public int StockDetailID { get; set; }
        public decimal ProductQty { get; set; }
        public decimal ProductQtyInKG { get; set; }
        public decimal UnitPrice { get; set; }
        public string Remarks { get; set; }
        public decimal PerKgStdQty { get; set; }
        public int GroupId { get; set; }
       
    }
}
