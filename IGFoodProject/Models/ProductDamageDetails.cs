﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class ProductDamageDetails
    {
        public int DamageDetailsId { get; set; }

        public int DamageMasterId { get; set; }

        public int ProductId { get; set; }

        public decimal StockOutQty { get; set; }

        public decimal StockOutKg { get; set; }

        public string Remarks { get; set; }
    }
}