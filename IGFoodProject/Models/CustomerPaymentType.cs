﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class CustomerPaymentType
    {
        public int PaymentTypeId { get; set; }

        public string PaymentType { get; set; }
    }
}