﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class PurchaseOrderReportParameter
    {

        public string ReportValue { get; set; }
        public string PurchaseOrderID { get; set; }
        public int CompanyId { get; set; }
        public int LocationId { get; set; }
        public int WarehouseId { get; set; }
        public int SupplierId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int RStatus { get; set; }
    }
}