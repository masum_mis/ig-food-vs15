﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class SupplierInfo
    {
        public int SupplierId { get; set; }

        public string SupplierName { get; set; }

        public string SupplierCode { get; set; }

        public string ProprietorName { get; set; }

        public string MailingAddress { get; set; }

        public string PhysicalAddress { get; set; }

        public string SupplierPhone { get; set; }

        public string SupplierSecondPhone { get; set; }

        public string SupplierEmail { get; set; }

        public bool? IsActive { get; set; }

        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public string UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }

        public string BankAccount { get; set; }

        public string ContactPerson { get; set; }

        public string GSTNo { get; set; }

        public decimal? CreditLimit { get; set; }

        public string Remarks { get; set; }
        public string SuplierType { get; set; }
        public int SuplierTypeId { get; set; }
    }
}