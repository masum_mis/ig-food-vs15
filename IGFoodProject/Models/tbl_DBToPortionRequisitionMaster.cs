﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.Models
{
    public class tbl_DBToPortionRequisitionMaster
    {
        public int DBToPortionReqID { get; set; }
        public int CompanyID { get; set; }
        public int LocationID { get; set; }
        public int WareHouseID { get; set; }
        public int ToWarehouseId { get; set; }
        public string RequisitionDate { get; set; }
        public string RequisitionNo { get; set; }
        public string ProductionDate { get; set; }
        public string Comments { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }

        public DateTime UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime ApproveDate { get; set; }
        public string ApproveBy { get; set; }
        public bool IsApprove { get; set; }
        public int IssueStatus { get; set; }
        public string RequisitionBy { get; set; }
        /// <summary>
        /// From Date & To Date For Report
        /// </summary>
        public  DateTime FromDate { get; set; }
        public  DateTime ToDate { get; set; }
        
        public List<tbl_DBToPortionRequisitionDetail> adetail { get; set; }
    }
}
