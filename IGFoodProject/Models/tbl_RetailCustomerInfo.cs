﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class tbl_RetailCustomerInfo
    {
        public int RetailCustomerID { get; set; }
        public string RetailCode { get; set; }
        public string RetailName { get; set; }
        public string PersonalAddress { get; set; }
        public string Phone { get; set; }
        public string Phone2 { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }
    }
}