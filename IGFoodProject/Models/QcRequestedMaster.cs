﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    [Table("tbl_QcRequestedMaster")]
    public class QcRequestedMaster
    {
        [Key]
        public int QcRequestMasterId { get; set; }

        public int WarehouseId { get; set; }

        public DateTime ChallanDate { get; set; }
        public DateTime? ReturnDate { get; set; }

        public string CreateBy { get; set; }

        public DateTime CreateDate { get; set; }

        public string UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }

        public  List<QcRequestedProduct> AQcRequestedProducts { get; set; } 
    }
}