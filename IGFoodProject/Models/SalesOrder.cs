﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class SalesOrder
    {
        public int SalesOrderProcessedMasterId { get; set; }

        public string SalesOrderNo { get; set; }

        public string OrderBy { get; set; }

        public int? CustomerId { get; set; }

        public int? OrderType { get; set; }

        public string SalesPerson { get; set; }

        public DateTime? OrderDate { get; set; }

        public string DeliveryDestination { get; set; }

        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public string UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }

        public bool? IsApprove { get; set; }

        public string ApproveBy { get; set; }

        public DateTime? ApproveDate { get; set; }

        public int? WarehouseId { get; set; }

        public int? GroupId { get; set; }

        public decimal? DiscountPercent { get; set; }

        public decimal? DiscountNumber { get; set; }

        public decimal? TaxAndVat { get; set; }

        public int? PaymentType { get; set; }
    }
}