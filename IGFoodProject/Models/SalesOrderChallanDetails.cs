﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class SalesOrderChallanDetails
    {
        public int SalesOrderChallanDetailsId { get; set; }

        public int? SalesOrderChallanMasterId { get; set; }
        public int? ChallanSecondDetailsId { get; set; }

        public int? StockInId { get; set; }
        public int? WarehouseId { get; set; }
        public int? SalesOrderDetailsId { get; set; }
        public int? ProductId { get; set; }
        public int? GroupId { get; set; }

        public decimal? IssueQty { get; set; }

        public decimal? IssueKg { get; set; }
        public decimal? IssuePrice { get; set; }

        public bool ConfirmFullPick { get; set; }

        public decimal? DeliveryQty { get; set; }

        public decimal? DeliveryKG { get; set; }
        public decimal? DeliveryAmount { get; set; }
        public string DeliveryRemarks { get; set; }
        public decimal SaleRate { get; set; }
        public string aFlag { get; set; }


        public decimal? ReturnQty { get; set; }

        public decimal? ReturnKG { get; set; }



        public decimal? WeightLossQty { get; set; }

        public decimal? WeightLossKG { get; set; }
        public decimal? WeightGainKG { get; set; }

        public decimal DiscountAmt { get; set; }

    }
}