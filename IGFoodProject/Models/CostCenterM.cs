﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class CostCenterM
    {
        public int CostCenterId { get; set; }

        public string CostCenterName { get; set; }
    }
}