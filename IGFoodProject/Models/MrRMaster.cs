﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IGFoodProject.ViewModel;

namespace IGFoodProject.Models
{
    public class MrRMaster
    {

        public int MRRMasterId { get; set; }

        public string MRRNo { get; set; }

        public DateTime? MRRDate { get; set; }
        public string StrMRRDate { get; set; }

        public int PurchaseOrderID { get; set; }

        public string ReceiveBy { get; set; }

        public string CreateBy { get; set; }
        public int MRRStatus { get; set; }

        public DateTime? CreateDate { get; set; }

        public string UpdateBy { get; set; }
        public string Remarks { get; set; }

        public DateTime? UpdateDate { get; set; }

        public int DressedComplete { get; set; }
       

        public int WareHouseId { get; set; }
        public int LocationId { get; set; }
        public int CompanyId { get; set; }
        public decimal TotalReceiveQty { get; set; }
        public decimal TotalReceiveKg { get; set; }
        public string ReceiveTime { get; set; }

        public string ReceiveDateStr { get; set; }
        public  List<ViewMrrMaster> ViewMrrMasters { get; set; }
        public List<MrRDetails> Details { get; set; }


        public decimal TransportCost { get; set; }
        public decimal LaborCost { get; set; }
    }
}
