﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class tbl_Reason
    {
        [Key]
        public int ReasonId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int ReasonFor { get; set; }
        public bool isActive { get; set; }
        public  string CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}