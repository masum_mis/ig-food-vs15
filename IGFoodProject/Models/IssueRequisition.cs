﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class IssueRequisition
    {
        public int RequisitionMasterId { get; set; }

        public string RequisitionNo { get; set; }
        public int CompanyId { get; set; }

        public int WarehouseId { get; set; }
        public int DepartmentId { get; set; }
        public string eflag { get; set; }


        public int LocationId { get; set; }
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }




        public string RequisitionBy { get; set; }
        public string WareHouseName { get; set; }
        public string LocationName { get; set; }
        public string DepartmentName { get; set; }
        public string RequisitionByName { get; set; }



        public DateTime RequisitionDate { get; set; }

        public string Remarks { get; set; }

        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public string UpdateBy { get; set; }
        public bool IsApprove { get; set; }
        public  string ApproveBy { get; set; }
        public DateTime? ApproveDate { get; set; }
        public string RejectBy { get; set; }
        public DateTime? RejectDate { get; set; }

        public DateTime? UpdateDate { get; set; }

        public List<IssueRequisitionDetails> IssueRequisitionDetailses { get; set; }
        public int? VehicleId { get; set; }
        public string DriverName { get; set; }
        public string DriverMobile { get; set; }

        public string CompanyName { get; set; }






        public decimal TotalRequisitionQuantity { get; set; }
        public decimal TotalRequisitionKg { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime RequiredDate { get; set; }
        public string RequiredTime { get; set; }
        public int RStatus { get; set; }
        public int PurposeID { get; set; }
    }
}