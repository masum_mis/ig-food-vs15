﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class StockTransferReqDetails
    {
        public int StockTransferReqDetailsId { get; set; }

        public int? StockTransferReqMasterId { get; set; }

        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal? RequisitionQuantity { get; set; }
        public decimal? RequisitionKg { get; set; }
        public string Remarks { get; set; }

        public StockTransferReqMaster StockTransferReqMaster { get; set; }

        public decimal? AIssueQty { get; set; }
        public decimal? AIssueKg { get; set; }


        public decimal? IssueQty { get; set; }
        public decimal? IssueKg { get; set; }

        public decimal? ComPercent { get; set; }
        public decimal? MRP { get; set; }
        public int GroupId { get; set; }

    }
}