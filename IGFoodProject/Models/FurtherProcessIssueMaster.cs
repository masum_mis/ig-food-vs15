﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class FurtherProcessIssueMaster
    {
        public int FurtherProcessIssueMasterId { get; set; }

        public string FurtherProcessIssueNo { get; set; }

        public int PortionToFurtherProcessReqId { get; set; }

        public int CompanyId { get; set; }

        public int LocationId { get; set; }

        public int WareHouseId { get; set; }

        public int ToWareHouseId { get; set; }

        public DateTime IssueDate { get; set; }

        public DateTime ProductionDate { get; set; }
        public DateTime RequisitionDate { get; set; }

        public string Comments { get; set; }

        public string CreateBy { get; set; }
        public string RequisitionBy { get; set; }

        public DateTime CreateDate { get; set; }

        public int StockInStatus_Portion { get; set; }

        public int StockInStatus_ByProduct { get; set; }

        public string IssueBy { get; set; }

        public List<FurtherProcessIssueDetail> FurtherProcessIssueDetail { get; set; }
    }
}