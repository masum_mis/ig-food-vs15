﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class SalesOrderDetailsTrade
    {
        public int TradeSalesOrderDetailsId { get; set; }

        public int? TradeSalesOrderMasterId { get; set; }

        public int? ProductId { get; set; }

        public decimal? ProductQty { get; set; }

        public decimal? ProductKg { get; set; }

        public decimal? SaleRate { get; set; }

        public decimal? DiscountAmount { get; set; }

        public decimal? DiscountPercent { get; set; }

        public int? PackSize { get; set; }

        public decimal? TotalPrice { get; set; }

        public decimal? ApprovedQty { get; set; }

        public decimal? ApprovedKg { get; set; }

        public bool? IsDelete { get; set; }

        public string DeleteBy { get; set; }

        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public string UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }

        public int? OperationIdAdd { get; set; }

        public int OperationIdDelete { get; set; }

        public decimal ProductTax { get; set; }

        public decimal? DiscountRetention { get; set; }

        public decimal? DiscountDeliveryCost { get; set; }

        public decimal? DamageDiscount { get; set; }

        public int? GroupId { get; set; }

        public int? FullPicked { get; set; }

        public decimal? BasePrice { get; set; }

        public decimal? BTotalPrice { get; set; }

        public string ApprovalUpdateBy { get; set; }

        public DateTime? ApprovalUpdateDate { get; set; }

        public decimal? ModifiedQty { get; set; }

        public decimal? ModifiedKg { get; set; }

        public decimal? WeightLossValue { get; set; }
        public string DetailsRemarks { get; set; }
    }
}