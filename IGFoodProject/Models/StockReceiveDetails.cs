﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class StockReceiveDetails
    {

        public int StockReceiveDetailsId { get; set; }

        public int? StockReceiveMasterId { get; set; }

        public int? StockTransferIssueDetailsId { get; set; }

        public int? ProductId { get; set; }

        public decimal? ReceiveQty { get; set; }

        public decimal? ReceiveKg { get; set; }

        public decimal? IssueQty { get; set; }

        public decimal? IssueKG { get; set; }
        public decimal? WeightLossQty { get; set; }

        public decimal? WeightLossKg { get; set; }

        public decimal? ReturnQty { get; set; }

        public decimal? ReturnKG { get; set; }
        public decimal? LostQty { get; set; }

        public decimal? LostKG { get; set; }

        public string Remarks { get; set; }
        public DateTime ExpireDate { get; set; }
    }
}