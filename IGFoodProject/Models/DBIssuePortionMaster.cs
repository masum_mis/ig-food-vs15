﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class DBIssuePortionMaster
    {
        public int DBIssueMasterID { get; set; }

        public string IssueNo { get; set; }

        public int? DBToPortionReqID { get; set; }

        public int? CompanyID { get; set; }

        public int? LocationID { get; set; }

        public int? WareHouseID { get; set; }

        public DateTime? IssueDate { get; set; }

        public DateTime? ProductionDate { get; set; }

        public string Comments { get; set; }

        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public int? StockInStatus_Portion { get; set; }

        public int? StockInStatus_ByProduct { get; set; }

        public string IssueBy { get; set; }

        public string CompanyName { get; set; }
        public string LocationName { get; set; }
        public string WareHouseName { get; set; }
        public  string RequisitionNo { get; set; }

        public List<DBIssuePortionDetails> DbIssuePortionDetails { get; set; }
        public List<DBIssuePortionDetails2> DbIssuePortionDetails2 { get; set; }

        public int FromWareHouseId { get; set; }
        public int ToWarehouseId { get; set; }
    }
}