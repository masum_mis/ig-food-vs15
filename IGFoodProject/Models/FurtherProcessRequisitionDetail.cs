﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class FurtherProcessRequisitionDetail
    {
        public int PortionToFurtherProcessReqDetailId { get; set; }

        public int PortionToFurtherProcessReqId { get; set; }

        public int ProductId { get; set; }
        public int GroupId { get; set; }

        public decimal ProductQty { get; set; }

        public decimal ProductKg { get; set; }

        public string Remarks { get; set; }

        public int StockInDetailId { get; set; }

        public decimal StockQty { get; set; }

        public decimal StockInKg { get; set; }

        public decimal UnitPrice { get; set; }
        public  string ProductName { get; set; }
    }
}