﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    [Table("tbl_WareHouseType")]
    public class WarehouseTypes
    {
        [Key]
        public int WarehouseTypeId { get; set; }
        public string WarehouseType { get; set; }
    }
}