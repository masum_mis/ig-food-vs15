﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class ChallanListForDelivery
    {
        public int SalesOrderChallanMasterId { get; set; }
        public string ChallanDate { get; set; }
        public string SalesOrderChallanNo { get; set; }
        public decimal TotalChallanQty { get; set; }
        public decimal TotalChallanKG { get; set; }
        public decimal ChallanAmount { get; set; }
        public string CustomerName { get; set; }
        public string OrderDate { get; set; }
        public string SalesOrderNo { get; set; }
    }
}