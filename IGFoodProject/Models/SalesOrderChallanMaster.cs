﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class SalesOrderChallanMaster
    {
        public int SalesOrderChallanMasterId { get; set; }

        public int SalesOrderId { get; set; }

        public string SalesOrderChallanNo { get; set; }

        public int? CompanyId { get; set; }
        public int? VehicleId { get; set; }
        public int? PickingMasterId { get; set; }

       
        
        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public int? WarehouseId { get; set; }
        public decimal? ChallanAmount { get; set; }
        public DateTime? ChallanDate { get; set; }

        public bool IsDeliveryConfirm { get; set; }
        
        public decimal? TotalDeliveryQty { get; set; }
        public decimal? TotalDeliveryKG { get; set; }
        public decimal DeliveryAmount { get; set; }
        public string DeliveryBy { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public decimal TotalPaid { get; set; }

        public decimal TotalTax { get; set; }
        public decimal GrandTotal { get; set; }


        public string Remarks { get; set; }
        public string DriverName { get; set; }
        public string DriverContact { get; set; }

        public List<SalesOrderChallanDetails> ChallanDetails { get; set; }

        public string SalesOrderNo { get; set; }
        public string SaleType { get; set; }

        public decimal restamount { get; set; }
        public decimal? TotalDiscountAmount { get; set; }
        public decimal? TotalDiscountAmount_delivery { get; set; }
    }
}