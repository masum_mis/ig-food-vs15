﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class TradeReturnLog
    {
        public int TradeReturnId { get; set; }

        public int TradeChallanMasterId { get; set; }

        public int TradeChallanDetailsId { get; set; }

        public int ProductId { get; set; }

        public decimal? ReturnQty { get; set; }

        public decimal? ReturnKg { get; set; }

        public decimal? WeightLossQty { get; set; }

        public decimal? WeightLossKg { get; set; }

        public DateTime? ReturnDate { get; set; }

        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public int? RequestedWarehouse { get; set; }

        public bool? CompleteQc { get; set; }

        public int? StoreWarehouse { get; set; }

        public int? StockInDetailsId { get; set; }
    }
}