﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class ItemGroup
    {
        public int ItemGroupId { get; set; } 
        public string ItemGroupName { get; set; } 
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public bool? IsActive { get; set; }
    }
}