﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class Item
    {
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public string ItemName1 { get; set; } 

        public string ItemDescription { get; set; }
        public string Remarks { get; set; } 
        public int ItemGroupId { get; set; }
        public string ItemGroupName { get; set; }
        public int ItemTypeId { get; set; }
        public string ItemTypeName { get; set; }
        public int ItemCategoryId { get; set; }
        public string ItemCategoryName { get; set; }
        public int ItemColorId { get; set; }
        public string ItemColorName { get; set; }
        public int PackSizeId { get; set; }
        public string PackSizeName { get; set; }
        public int UOMId { get; set; }
        public string UOMName { get; set; }        
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public bool? IsActive { get; set; }
    }
}