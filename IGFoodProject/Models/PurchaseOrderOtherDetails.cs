﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class PurchaseOrderOtherDetails
    {
        public decimal approvedRequisitionQty { get; set; }
        public int PurchaseOrderDetailID { get; set; }

        public int? PurchaseOrderID { get; set; }

        public int? ItemId { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }

        public decimal? PurchaseQty { get; set; }
        public int? UOMId { get; set; }

        public decimal? UnitPrice { get; set; }
        public decimal? DiscountRate { get; set; }

        public decimal? TotalPrice { get; set; }

        public string Remarks { get; set; }

        public decimal? MRRRemainingQty { get; set; }
        public decimal? RequisitionQty { get; set; }

        public int? ReceiveStatus { get; set; }
        public string eflag { get; set; }
        public bool? IsDelete { get; set; }
        public decimal RemainingPoQty { get; set; }
    }

}