﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class StockTransferIssueMaster
    {
        public int StockTransferIssueMasterId { get; set; }
        public int StockTransferReqMasterId { get; set; }
        public string IssueNo { get; set; }
        public int CompanyId { get; set; }

        public int FromWarehouseId { get; set; }

        public int ToWarehouseId { get; set; }

        public int FromLocationId { get; set; }

        public int ToLocationId { get; set; }

        public string IssueBy { get; set; }

      

        public DateTime? DeliveryDate { get; set; }

        public string Remarks { get; set; }

        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public string UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }

        public List<StockTransferIssueDetails> StockTransferReqDetails { get; set; }
        public int? VehicleId { get; set; }
        public string DriverName { get; set; }
        public string DriverMobile { get; set; }

        public string CompanyName { get; set; }
        public string FromLocation { get; set; }
        public string fromWarehouse { get; set; }
        public string ToLocation { get; set; }
        public string ToWarehouse { get; set; }
        public decimal TotalRequisitionQuantity { get; set; }
        public decimal TotalRequisitionKg { get; set; }

        public int GroupId { get; set; }

        public string VehicleRegNo { get; set; }  

        public decimal TotalIssueQuantity { get; set; }
        public decimal TotalIssueKg { get; set; }
    }
}