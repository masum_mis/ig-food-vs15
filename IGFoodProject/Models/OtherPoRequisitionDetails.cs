﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class OtherPoRequisitionDetails
    {
        public string UOMName;
        public int RequisitionDetailsId { get; set; }
        public int RequisitionMasterId { get; set; }


        public int ItemId { get; set; }
        public string ItemCode { get; set; }
        public int UOMId { get; set; }
        public decimal StockQty { get; set; }
        public decimal StockKg { get; set; }


        public string Remarks { get; set; }
        public string ItemName { get; set; }
        public string Unit { get; set; }

        public string eflag { get; set; }
        public bool? IsDelete { get; set; }


        public decimal RequisitionQty { get; set; }
        public decimal ApprovedQty { get; set; }
        public int  PurposeID { get; set; }
        public decimal RequisitionKg { get; set; }
        public decimal StockInQty { get; set; }
        public decimal RemainingPoQty { get; set; }
        public decimal RemainingPoKg { get; set; }
        public string ItemDescription { get; set; }
    }
}