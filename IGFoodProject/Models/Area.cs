﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class Area
    {
        public int AreaId { get; set; }

        public int? DivisionId { get; set; }

        public string AreaName { get; set; }

        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

    }
}