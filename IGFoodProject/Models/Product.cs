﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    [Table("tbl_Product")]
    public class Product
    {
        [Key]
        public int ProductId { get; set; }
        public string ProductNo { get; set; }
        public int GroupId { get; set; }
        public int TypeId { get; set; }
        public int CategoryId { get; set; }
        public int PackSizeId { get; set; }
        public int BrandId { get; set; }
        public int UOMId { get; set; }
        public string ProductName { get; set; }
        [NotMapped]
        public string ProductName1 { get; set; }
        public string ProductDescription { get; set; }
        [NotMapped]
        public string CategoryName { get; set; }
        [NotMapped]
        public string GroupName { get; set; }
        [NotMapped]
        public string TypeName { get; set; }
        [NotMapped]
        public string PackSizeName { get; set; }
        [NotMapped]
        public string BrandName { get; set; }
        [NotMapped]
        public string UOMName { get; set; }
        [NotMapped]
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public bool IsActive { get; set; }
        public bool? IsBulk { get; set; }
        public decimal? PerKgStdQty { get; set; }
        public int? ProductSerialId { get; set; }
        public bool? IsNonsaleable { get; set; }
        public bool? IsNonReguler { get; set; }
        public tbl_ProductPackSize PackSize { get; set; }
    }
}