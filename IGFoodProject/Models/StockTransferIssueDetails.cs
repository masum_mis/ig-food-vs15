﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class StockTransferIssueDetails
    {
        public int StockTransferIssueDetailsId { get; set; }
        public int StockTransferIssueMasterId { get; set; }
        public int StockTransferReqDetailsId { get; set; }

        public int? StockTransferReqMasterId { get; set; }

        public int? ProductId { get; set; }

        public decimal? RequisitionQuantity { get; set; }
        public decimal? RequisitionKg { get; set; }
        public string Remarks { get; set; }

       

        public decimal? IssueQty { get; set; }
        public decimal? IssueKg { get; set; }

        public decimal? SalesPriceTransfer { get; set; }
        public decimal? IssueAmount { get; set; }
        public decimal? Comission { get; set; }
        public decimal? ComissionAmount { get; set; }
        public decimal? NetAmount { get; set; }
    }
}