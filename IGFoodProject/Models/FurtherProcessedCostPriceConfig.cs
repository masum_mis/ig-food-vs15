﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    [Table("tbl_FurtherProcessedCostPriceConfig")]
    public class FurtherProcessedCostPriceConfig
    {
        [Key]
        public int FurtherProcessedCostPriceId { get; set; }
        [Required(ErrorMessage = "Product Required")]
        [Display(Name = "Product")]
        public int ProductId { get; set; }
        [Required(ErrorMessage = "Cost Price Required")]
        [Display(Name = "Cost Price")]
        public decimal CostPrice { get; set; }
        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }
        [Required(ErrorMessage = "From Date Required")]
        [Display(Name = "From Date")]
        public DateTime FromDate { get; set; }
        [Required(ErrorMessage = "To Date Required")]
        [Display(Name = "To Date")]
        public DateTime ToDate { get; set; }

        public Product Product { get; set; }
    }
}