﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class StockAdjustmentDetails
    {
        public int AdjustmentDetailsId { get; set; }

        public int AdjustmentMasterId { get; set; }

        public int StockInDetailsId { get; set; }
        public int RefStockInDetailsId { get; set; }

        public decimal StockOutQty { get; set; }

        public decimal StockOutKg { get; set; }
        public StockAdjustmentMaster AdjustmentMaster { get; set; }
    }
}