﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class DBIssuePortionDetails
    {
        public int DBIssueDetailID { get; set; }

        public int? DBIssueMasterID { get; set; }

        public int? DBToPortionRequisitionDetailID { get; set; }

        public int? ProductID { get; set; }

        public int? StockInDetailID { get; set; }

        public decimal? IssueQty { get; set; }

        public decimal? IssueQtyInKG { get; set; }
        public  decimal UnitPrice { get; set; }

        public string Remarks { get; set; }
    }
}