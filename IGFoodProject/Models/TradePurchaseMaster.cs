﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class TradePurchaseMaster
    {
        public int TradePurchaseMasterId { get; set; }
        public string TradeNo { get; set; }
        public int CompanyId { get; set; }
        public string IsDeliveryCompanyOther { get; set; }
        public int DelLocationId { get; set; }
        public int DelWareHouseID { get; set; }
        public string DeliveryOther { get; set; }
        public DateTime PurchaseDate { get; set; }
        public string LocalOrSupplier { get; set; }
        public int SupplierId { get; set; }
        public string LocalSupplier { get; set; }
        public string CommonOrFixCustomer { get; set; }
        public int CustomerId { get; set; }
        public decimal GrandTotalAmount { get; set; }
        public decimal Discount { get; set; }


        public decimal NetPayable { get; set; }

        public decimal ApprovalGrandTotalAmount { get; set; }
        public decimal ApprovalDiscount { get; set; }


        public decimal ApprovalNetPayable { get; set; }
        public string CreateBy { get; set; }

        public DateTime CreateDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime UpdateDate { get; set; }

        public int IsApprove { get; set; }
        public string ApproveBy { get; set; }
        public DateTime ApproveDate { get; set; }

        public List<TradePurchaseDetail> adetail { get; set; }

       


    }
}