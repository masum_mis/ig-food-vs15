﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class LiveBirdMortalityMaster
    {
        public int MortalityMasterId { get; set; }

        public int? MrrMasterId { get; set; }

        public DateTime? EntryDate { get; set; }

        public string EntryBy { get; set; }

        public List<LiveBirdMortalityDetails> BirdMortalityDetails { get; set; }
    }
}