﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class SalesCollectionDetail
    {
        public int SalesCollectionDetailId { get; set; }

        public int? SalesCollectionMasterId { get; set; }
        public int? BankId { get; set; }
        public int? BranchId { get; set; }

        public int? PaymentType { get; set; }

        public string RefNo { get; set; }

        public DateTime? RefDate { get; set; }

        public decimal? PaymentAmount { get; set; }

        public int? SalesOrderChallanMasterId { get; set; }

        public SalesCollectionMaster SalesCollectionMaster { get; set; }

        public string PaymentTypeName { get; set; }
        public int SerialNo { get; set; }

        public string Remark { get; set; }


        public string BankName { get; set; }
        public string BranchName { get; set; }

        public string EntryBy { get; set; }

        public DateTime EntryDate { get; set; }
        public DateTime Collectiondate { get; set; }
        public string SaleType { get; set; }


        public decimal? TaxAmountD { get; set; }
        public decimal? GrandTotalD { get; set; }


    }

}