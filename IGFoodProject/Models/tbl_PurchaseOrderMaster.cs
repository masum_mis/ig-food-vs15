﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.Models
{
    public class tbl_PurchaseOrderMaster
    {
        public int PurchaseOrderID { get; set; }
        public string PurchaseOrderNo { get; set; }
        public int ItemRequisitionMasterId { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime ExpectedDeliveryDate { get; set; }

        public string OrderDatestr { get; set; }
        public string ExpectedDeliveryDatestr { get; set; }
        public string CreateDateStr { get; set; }

        public int SupplierID { get; set; }
        public string Comments { get; set; }
        public string OrderBY { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public int DelLocationId { get; set; }
        public int DelWareHouseID { get; set; }
        public int CompanyId { get; set; }
        public bool IsApprove { get; set; }
        public string ApproveBy { get; set; }
        public DateTime ApproveDatetime { get; set; }
        public int MRRStatus { get; set; }
        public List<tbl_PurchaseOrderDetail> aDetail { get; set; } 
        public string SupplierName { get; set; } 
        public string flag { get; set; } 
        public decimal remaingqty { get; set; }
        public decimal remaingkg { get; set; }
        public int IsDelete { get; set; }
        public int IsClosed { get; set; }
        public  string DeleteBy { get; set; }
        public  string ClosedBy { get; set; }
        public DateTime DeleteDate { get; set; }
        public DateTime ClosedDate { get; set; }
        public decimal TotalDue { get; set; }
        public  decimal TotalPaid { get; set; }
        public int TotalQty { get; set; }

        public int TolerancePercent { get; set; }


    }
}
