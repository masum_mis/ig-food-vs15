﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class PortionStockInDetail_Log
    {
        public int PortionStockInDetailLogID { get; set; }

        public int? PortionStockInMasterLogID { get; set; }

        public int? ProductID { get; set; }

        public decimal? StockInQty { get; set; }

        public decimal? StockInKG { get; set; }

        public decimal? ApproveInQty { get; set; }

        public decimal? ApproveInKG { get; set; }

        public int? DBIssueDetailID { get; set; }

        public DateTime? ExpireDate { get; set; }

        public string Remark { get; set; }

        public decimal? UnitPrice { get; set; }

        public decimal? PUnitPrice { get; set; }

    }
}