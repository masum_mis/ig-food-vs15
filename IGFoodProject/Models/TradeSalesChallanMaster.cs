﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class TradeSalesChallanMaster
    {
        public int TradeChallanMasterId { get; set; }

        public int TradeSalesOrderMasterId { get; set; }

        public string TradeChallanNo { get; set; }

        public int CompanyId { get; set; }

        public string CreateBy { get; set; }

        public DateTime CreateDate { get; set; }

        public decimal TotalChallanQty { get; set; }

        public decimal TotalChallanKg { get; set; }
        public decimal TotatalOrderAmtKg { get; set; }
        public decimal TotatalOrderAmtQty  { get; set; }

        public decimal TotalChallanAmount { get; set; }

        public int WarehouseId { get; set; }
        public int PaymentType { get; set; }

        public bool IsDeliveryConfirm { get; set; }

        public decimal? TotalDeliveryQty { get; set; }

        public decimal? TotalDeliveryKg { get; set; }

        public decimal? TotalDeliveryAmount { get; set; }

        public string DeliveryBy { get; set; }

        public DateTime? DeliveryDate { get; set; }
        public DateTime? ChallanDate { get; set; }

        public decimal? TotalPaid { get; set; }

        public string Remarks { get; set; }

        public int? VehicleId { get; set; }

        public string DriverContact { get; set; }

        public string DriverName { get; set; }
        public string DeliveryEntryBy { get; set; }
        public string DeliveryEntryDate { get; set; }

        public  List<TradeSalesChallanDetails> ATradeChallanDetails { get; set; } 

    }
}