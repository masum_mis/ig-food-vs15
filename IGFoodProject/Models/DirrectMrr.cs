﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class DirrectMrr
    {
        public int MRRrOtherID { get; set; } 
        public string Ids { get; set; }
        public int MRRID { get; set; } 
        public int WareHouseId { get; set; }
        public string WareHouseName { get; set; }
        public int SupplierId { get; set; }
        public string SupplierName { get; set; }
        public int ItemId { get; set; }
        public string ItemCode { get; set; }
        public string ItemDescription { get; set; }
        public int UOMId { get; set; }
        public string UOMName { get; set; }
        public int PackSizeId { get; set; }
        public string PackSizeName { get; set; }

        public int OrderType { get; set; }
        public int ReceiveWarehouse { get; set; }
        public string OrderComments { get; set; }

        public string ChallanNo { get; set; } 
        public DateTime OrderDate  { get; set; } 
        public string DeliverTo { get; set; } 
        public string LabResult { get; set; } 
        public string ContactInfo { get; set; } 
        public string SupplierCredit { get; set; } 
        public string SupplierReference { get; set; } 
        public string Adjustment { get; set; } 
        public string VehicleNo { get; set; } 
        public string VehicleInWeight { get; set; } 
        public string VehicleOutWeight { get; set; } 
        public string QC { get; set; } 
        public int CALayerSevenId { get; set; }  
        public string CALayerSevenName { get; set; } 
        public decimal OrderQty { get; set; }  
        public DateTime MfgDate { get; set; }
        public DateTime ExpireDate { get; set; }
        public decimal UnitPrice { get; set; } 
        public decimal TotalPrice { get; set; } 
        public decimal TotalAmount { get; set; }
        public decimal TotalQty { get; set; } 
        public decimal TransportCost { get; set; } 
        public decimal LabourCost { get; set; }

        public string DeliveryTerms { get; set; } 
        public string PackagingTerms { get; set; } 
        public string PaymentsTerms { get; set; } 
        public string BillingTerms { get; set; } 
        public string OtherTerms { get; set; } 
        public string SpecialInstruction { get; set; }
        public string ReportValue { get; set; }
        public List<DirrectMrr> DirrectMrrList { get; set; } 
    }
}