﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.Models
{
    public class MenuPermission
    {
        public int DisSL { get; set; }
        public int LoginUserId { get; set; }
        public int SL { get; set; }
        public bool Status { get; set; }
        public int ModuleId { get; set; }
        public DateTime PermissionDate { get; set; }
        public string PermissionGivenBy { get; set; }
    }
}
