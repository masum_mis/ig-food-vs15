﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class TargetSetupMaster
    {
        public int TargetSetupMasterId { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }
        public string TargetSetupNo { get; set; }
        public string CustomerTargetSetupNo { get; set; }

        public string MonthName { get; set; }
        public int MonthId { get; set; }

        public string YearName { get; set; }
        public int YearId { get; set; }
        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public string UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }
        public string Title { get; set; }
        public string Remarks { get; set; }
        public bool IsEdit { get; set; } 
        public int RowCountNo { get; set; }
        public DateTime? NextDate { get; set; }

        public string CustomerName { get; set; }

        public List<TargetSetupDetails> TargetSetupDetails { get; set; }
    }

}