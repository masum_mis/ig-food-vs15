﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.Models
{
    public class DressedBirdDetail
    {
        public int DressedBirdDetailID { get; set; }

        public int? DressedBirdMasterID { get; set; }

        public int? ItemID { get; set; }

        public decimal? DressedQty { get; set; }

        public decimal? DressedInKG { get; set; }

        public decimal? RemainigDressedQty { get; set; }

        public decimal? RemainingDressedInKG { get; set; }

        public int? LiveBirdStocInID { get; set; }

        public string Remark { get; set; }

        public string ItemName { get; set; }

       public decimal StockQty { get; set; }
        public  decimal StockKg { get; set; }
        public decimal ApprovedQty { get; set; }
        public decimal ApprovedKG { get; set; }

    }
}
