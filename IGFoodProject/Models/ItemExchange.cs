﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class ItemExchange
    {
        public int ItemGroupId { get; set; }
        public string ItemGroupName { get; set; }

        public int ItemId { get; set; }
        public string ItemName { get; set; }

        public int GroupId { get; set; }
        public string GroupName { get; set; }

        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string Unit { get; set; }
        public decimal StockQty { get; set; }


        public int ExchangeId { get; set; }

        public int RmLocationId { get; set; }
        public string RmLocationName { get; set; } 

        public int RmWarehouseId { get; set; }
        public string RmWarehouseName { get; set; } 

        public int RmItemGroupId { get; set; }
        public string RmItemGroupName { get; set; } 

        public int RmItemId { get; set; }
        public string RmItemName { get; set; } 

        public int FgLocationId { get; set; }
        public string FgLocationName { get; set; } 

        public int FgWarehouseId { get; set; }
        public string FgWarehouseName { get; set; } 

        public int FgProductGroupId { get; set; }
        public string FgProductGroupName { get; set; } 

        public int FgProductId { get; set; }
        public string FgProductName { get; set; } 


        public decimal ExchangeQty { get; set; }
        public decimal ApprovedQty { get; set; } 
        public DateTime ExchangeDate { get; set; }
        public string ExeDate { get; set; } 

        public string Remarks { get; set; }
        public string CreateBy { get; set; }
        public string CreateDate { get; set; } 

        public string ApprovedBy { get; set; }
        public string ApprovedDate { get; set; } 

        public string ApprovedStatus { get; set; }   


    }
}



