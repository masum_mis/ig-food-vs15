﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.Models
{
    public class tbl_CustomerContactInfo
    {
        public int CustomerContactID { get; set; }
        public string FullName { get; set; }
        public string PhoneNo { get; set; }
        public string EmailNo { get; set; }
        public int CustomerID { get; set; }
        public string CustomerCode { get; set; }
    }
}
