﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class Territory
    {

        public int TerritoryId { get; set; }

        public int? AreaId { get; set; }

        public string TerritoryName { get; set; }

        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }
        public string AreaName { get; set; }
    }
}