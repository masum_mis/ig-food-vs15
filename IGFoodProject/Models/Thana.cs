﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class Thana
    {
        public int ThanaId { get; set; }

        public string ThanaName { get; set; }

        public int? DistrictId { get; set; }

        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

    }
}