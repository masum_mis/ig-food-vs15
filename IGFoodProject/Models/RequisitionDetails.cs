﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.Models
{
    public class RequisitionDetails
    {
        public int ItemRequisitionDetailsId { get; set; }

        public int? ItemRequisitionMasterId { get; set; }

        public int? ItemId { get; set; }

        public decimal? ProposedReqisitionQty { get; set; }

        public decimal? ProposedRequisitionKG { get; set; }

        public decimal? ProposedTentativePrice { get; set; }

        public decimal? ApprovedReqisitionQty { get; set; }

        public decimal? ApprovedReqisitionKG { get; set; }

        public decimal? ApprovedTentativePrice { get; set; }

        public decimal? RemainingQty { get; set; }
        public decimal? RemainingKG { get; set; }

        public string Remarks { get; set; }
        public string ItemName { get; set; }

    }
}
