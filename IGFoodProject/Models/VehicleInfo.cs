﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class VehicleInfo
    {
        [Key]
        public int VehicleInfoId { get; set; }

        public string VehicleRegNo { get; set; }

        public string VehicleDescription { get; set; }

        public decimal? VehicleCapacity { get; set; }

        public bool IsActive { get; set; }

        public string Remarks { get; set; }

    }
}