﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class SalesOrderProcessedDetails_FalseInvoice
    {
        public int FalseSalesOrderProcessDetailsId { get; set; }
        public int FalseSalesOrderProcessedMasterId { get; set; }
        public int SalesOrderProcessDetailsId { get; set; }

        public int? SalesOrderProcessedMasterId { get; set; }

        public int? ProductId { get; set; }

        public decimal? ProductQty { get; set; }

        public decimal? ProductKg { get; set; }

        public decimal? StockRate { get; set; }

        public decimal? SaleRate { get; set; }

        public decimal? DiscountAmount { get; set; }

        public decimal? DiscountPercent { get; set; }
        public decimal? ProductTax { get; set; }
        public decimal? ApprovedQty { get; set; }
        public decimal? ApprovedKg { get; set; }


        public int? PackSize { get; set; }

        public decimal? TotalPrice { get; set; }
        public string CreateBy { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool? IsDelete { get; set; }
        public int OperationIdAdd { get; set; }
        public int OperationIdDelete { get; set; }

        public decimal? ProductTotal { get; set; }
        public int GroupId { get; set; }
    }
}