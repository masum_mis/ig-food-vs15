﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    [Table("tbl_QCRequestedProduct")]
    public class QcRequestedProduct
    {
        [Key]
        public int QcRequestId { get; set; }

        public int? SalesOrderChallanDetailsId { get; set; }
        public int? SalesOrderChallanMasterId { get; set; }

        public int? ProductId { get; set; }

        public decimal? ReturnQty { get; set; }

        public decimal? ReturnKg { get; set; }

        public int StockInDetailsId { get; set; }
        public int QcRequestMasterId { get; set; }

        public bool IsTrade { get; set; }

        public DateTime? ExpireDate { get; set; }

    }
}