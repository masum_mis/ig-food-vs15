﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection.Emit;
using System.Web;
using System.Web.DynamicData;

namespace IGFoodProject.Models
{
    //[TableName("")]
    [Table("tbl_FurtherProcessPriceConfig")]
    public  class FurtherProcessPriceConfig
    {
        [Key]
        public int FurtherProcessPriceID { get; set; }

        [Required(ErrorMessage = "Product Required")]
        [Display(Name = "Product")]

        public int ProductId { get; set; }
        
        public int? PackSizeId { get; set; }
        [Required(ErrorMessage = "Mrp Required")]
        [Display(Name = "Mrp")]
        public decimal? MRP { get; set; }
        [Required(ErrorMessage = "TpPrice Required")]
        [Display(Name = "Trade Price")]
        public decimal? TPPrice { get; set; }
        [Required(ErrorMessage = "Delar Price Required")]
        [Display(Name = "Delar Price")]
        public decimal? DPPrice { get; set; }
        [Required(ErrorMessage = "Select IsActive Required")]
        [Display(Name = "Is Active")]
        public bool? IsActive { get; set; }
        [Required(ErrorMessage = "From Date Required")]
        [Display(Name = "From Date")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy }", ApplyFormatInEditMode = true)]
        public DateTime? FromDate { get; set; }
        [Required(ErrorMessage = "To Date Required")]
        [Display(Name = "To Date")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy }", ApplyFormatInEditMode = true)]
        public DateTime? ToDate { get; set; }
        public Product Product { get; set; }
        
        public  int CustomerTypeId { get; set; }
        public  CustomerType CustomerType { get; set; }
        public decimal? MTRetention { get; set; }
    }
}