﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class ProductDamageMaster
    {
        public int DamageMasterId { get; set; }

        public int CompanyId { get; set; }

        public int LocationId { get; set; }

        public int WarehouseId { get; set; }

        public DateTime StockOutDate { get; set; }

        public string StockOutBy { get; set; }

        public string Comment { get; set; }

        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public string UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }

        public List<ProductDamageDetails> DamageDetailses { get; set; }
    }
}