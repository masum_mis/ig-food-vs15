﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.Models
{
    public class DressedBirdMaster
    {
        public int DressedBirdMasterID { get; set; }

        public string DressedBirdNo { get; set; }

        public int? MRRMasterId { get; set; }

        public String DressingDate { get; set; }

        public string DressedBy { get; set; }

        public string CreateBy { get; set; }

        public DateTime? CreatDate { get; set; }

        public string UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }

        public string Remarks { get; set; }

        public int? StockInStatus { get; set; }

        public int? DressedStatus { get; set; }
        public int WareHouseId { get; set; }
        public string EmpName { get; set; }
        public string WareHouseName { get; set; }
        public int ToWareHouseId { get; set; }
        public List<DressedBirdDetail> Details { get; set; }

        public string ApprovedBy { get; set; }
        public DateTime ApprovedDate { get; set; }

        public  bool IsApprove { get; set; }
        public DateTime MRRDate { get; set; }
    }
}
