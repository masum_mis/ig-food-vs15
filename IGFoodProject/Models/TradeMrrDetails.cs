﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class TradeMrrDetails
    {
        public int TradeMRRDetailID { get; set; }

        public int? TradeMRRID { get; set; }

        public int? TradePurchaseOrderDetailsId { get; set; }

        public int? ProductId { get; set; }

        public decimal? MRRQty { get; set; }

        public decimal? MRRKG { get; set; }

        public decimal? UnitPrice { get; set; }

        public decimal? TotalPrice { get; set; }

        public int? PackSizeId { get; set; }

        public int? NoOfPack { get; set; }

        public DateTime? MfgDate { get; set; }

        public DateTime? ExpairyDate { get; set; }

        public string ProductNo { get; set; }
        public string ProductName { get; set; }
        public string UOMName { get; set; }

        public decimal? ApproveQty { get; set; }

        public decimal? ApproveKg { get; set; }
        public decimal? MRRRemainingQty { get; set; }

        public decimal? MRRRemainingKG { get; set; }
        public decimal? ReceivedQty { get; set; }

        public decimal? ReceivedKG { get; set; }
        public decimal? ApproveUnitPrice { get; set; }
        public decimal? ReturnQty { get; set; }

        public decimal? ReturnKG { get; set; }
        public int GroupId { get; set; }
    }

}