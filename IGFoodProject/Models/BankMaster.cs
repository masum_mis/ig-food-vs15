﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    [Table("tbl_BankMaster")]
    public class BankMaster
    {
        [Key]
        public int BankId { get; set; }
        public string BankName { get; set; }
        public bool IsActive { get; set; }
    }
}