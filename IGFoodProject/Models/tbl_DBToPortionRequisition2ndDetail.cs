﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.Models
{
    public class tbl_DBToPortionRequisition2ndDetail
    {
        public int DBToPortionRequisition2ndDetailID { get; set; }
        public int DBToPortionReqID { get; set; }
        public int ProductID { get; set; }
        public  string ProductName { get; set; }
        public decimal ProductQty { get; set; }
        public decimal ProductQtyInKG { get; set; }
        public string Remark { get; set; }

        
    }
}
