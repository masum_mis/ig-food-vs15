﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class SupplierPaymentMaster
    {
        public int SupplierPaymentMasterId { get; set; }
        public int SupplierId { get; set; }
        public decimal PaymentAmount { get; set; }
        public DateTime PaymentDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string RefNo { get; set; }
        public DateTime? RefDate { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public int BankId { get; set; }
        public int BranchId { get; set; }
        public  int PaymentType { get; set; }
        public List<SupplierPaymentDetail> aDetail { get; set; }

        public string Remark { get; set; }

        public decimal TaxAmount { get; set; }
        public decimal TaxPercent { get; set; }
        public decimal GrandTotal { get; set; }
    }
}