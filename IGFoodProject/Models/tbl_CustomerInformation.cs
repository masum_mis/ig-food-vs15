﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.Models
{
    public class tbl_CustomerInformation
    {
        public int CustomerID { get; set; }

        public string CustomerCode { get; set; }

        public string CustomerName { get; set; }

        public string CustomerShortName { get; set; }

        public string FatherName { get; set; }

        public string MotherName { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string PersonalAddress { get; set; }

        public string PersonalContact { get; set; }

        public string GSTNo { get; set; }

        public bool? IsContractor { get; set; }

        public string CustomerCurrency { get; set; }

        public int CustomerType { get; set; }

        public int CostCenterId { get; set; }

        public bool? IsRetention { get; set; }

        public bool? TSRelease { get; set; }

        public bool? IsCredit { get; set; }

        public int? GLAccountGroup { get; set; }

        public string Phone { get; set; }

        public string SecondaryPhoneNumber { get; set; }

        public string FaxNumber { get; set; }

        public string Email { get; set; }
        public string CustomerWarehouseName { get; set; }

        public decimal? DiscountPercent { get; set; }
        public string WareHouseArea { get; set; }
        public string WareHouseAddress { get; set; }

        public int PaymentType { get; set; }

        public string CreditStatus { get; set; }

        public string RetentionPriority { get; set; }

        public string DefaultInventoryLocation { get; set; }

        public int? DefaultShippingCompany { get; set; }

        public DateTime? CreateDate { get; set; }

        public string CreateBy { get; set; }

        public DateTime? UpdateDate { get; set; }

        public string UpdateBy { get; set; }

        public string NID { get; set; }

        public string TradeLicence { get; set; }

        public string TIN { get; set; }

        public int? DivisionId { get; set; }

        public int? TerritoryId { get; set; }

        public int? AreaId { get; set; }

        public int? MarketId { get; set; }

        public int? DistrictId { get; set; }

        public int? ThanaId { get; set; }
        public int? HasDealerId { get; set; }
        public string DistrictNAme { get; set; }

        public CustomerWarehouse CustomerWareHouse { get; set; }
        public string CustomerTypeName { get; set; }
        public byte IsEmployee { get; set; }
        public string EMPID { get; set; }

    }
}
