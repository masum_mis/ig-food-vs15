﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class PurchaseOrderOtherMaster
    {
        public int PurchaseOrderOtherID { get; set; }

        public int? OrderType { get; set; }
        public string OrderTypeName { get; set; }

        public string PurchaseOrderNo { get; set; }

        public DateTime OrderDate { get; set; }

        public DateTime? ExpectedDeliveryDate { get; set; }

        public int? SupplierID { get; set; }
        public string SupplierName { get; set; }

        public string SupplierRef { get; set; }

        public string ContactInfo { get; set; }

        public string Comments { get; set; }

        public string OrderBY { get; set; }

        public int? DelLocationId { get; set; }

        public int? DelWareHouseID { get; set; }
        public string WareHouseName { get; set; }
        public bool IsApprove { get; set; }

        public string ApproveBy { get; set; }

        public DateTime? ApproveDatetime { get; set; }

        public int? MRRStatus { get; set; }

        public decimal? TotalQty { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal DiscountAmt { get; set; }

        public decimal? TotalDue { get; set; }

        public decimal? TotalPaid { get; set; }

        public decimal? Advance { get; set; }

        public decimal? Adjustment { get; set; }

        public int? IOUHead { get; set; }

        public int? FromAccount { get; set; }

        public int? CurrencyId { get; set; }

        public decimal? ConversionRate { get; set; }

        public int? IsDelete { get; set; }

        public string DeleteBy { get; set; }

        public DateTime? DeleteDate { get; set; }

        public string CreateBy { get; set; }

        public DateTime CreateDate { get; set; }

        public string UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }
        public string DeliveryTo { get; set; }

        public int? BankId { get; set; }
        public int? BranchId { get; set; }
        public string ChequeNo { get; set; }
        public DateTime? ChequeDate { get; set; }
        public string RefNo { get; set; }
        public DateTime? RefDate { get; set; }
        public string eflag { get; set; }
        public int? AccountType { get; set; }
        public string SpecialInstruction { get; set; }
        public int CompanyId { get; set; }
        public int BUId { get; set; }
        public List<PurchaseOrderOtherDetails> POOtherDetails { get; set; }
        public TermsAndcondition TermsAndconditions { get; set; }
        public int RequisitionId { get; set; }
        public int Tolarance { get; set; }
        public bool IsLocal { get; set; }
        public string PurchaseType { get; set; }
        public string ReceivedStatus { get; set; }
    }

}