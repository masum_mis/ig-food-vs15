﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class TradeInvoiceDetails
    {
        public int TradeInvoiceDetailID { get; set; }

        public int TradeInvoiceId { get; set; }

        public int? TradeMRRMasterID { get; set; }

        public DateTime? TradeMRRDate { get; set; }

        public decimal? InvoicedAmount { get; set; }

        public decimal? VatPercent { get; set; }

        public decimal? InvoicedAmountWithVat { get; set; }

        public string ItemDescription { get; set; }

        public decimal? ReceivedAmount { get; set; }

        public decimal? TradePoAdvanced { get; set; }

    }

}