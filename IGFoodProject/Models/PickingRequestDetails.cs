﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class PickingRequestDetails
    {
        public int PickingDetailsId { get; set; }

        public int PickingMasterId { get; set; }

        public int SalesOrderMasterId { get; set; }

        public int SalesOrderDetailsId { get; set; }

        public int ProductId { get; set; }

        public decimal PickedQuantity { get; set; }
        public decimal RequestedQuantity { get; set; }

        public decimal? SaleQuantity { get; set; }
        public decimal PickingInputedQty { get; set; }
        public decimal PickingInputedKg { get; set; }

    }
}