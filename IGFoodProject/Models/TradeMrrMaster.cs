﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class TradeMrrMaster
    {
        public int TradeMrrMasterId { get; set; }

        public int? TradePurchaseOrderId { get; set; }

        public DateTime? MRRDate { get; set; }

        public string MrrNo { get; set; }

        public int? SupplierID { get; set; }

        public string ChallanNo { get; set; }

        public string LabResult { get; set; }

        public string VehicleNo { get; set; }

        public decimal? VehicleInWeight { get; set; }

        public decimal? VehicleOutWeight { get; set; }

        public bool? IsApprove { get; set; }

        public string ApproveBy { get; set; }

        public DateTime? ApproveDatetime { get; set; }

        public decimal? TotalQty { get; set; }

        public decimal? TotalKG { get; set; }

        public decimal? TotalAmount { get; set; }

        public int? IsDelete { get; set; }

        public string DeleteBy { get; set; }

        public DateTime? DeleteDate { get; set; }

        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public string UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }

        public string SpecialInstruction { get; set; }

        public decimal? LaborCost { get; set; }

        public decimal? TransportCost { get; set; }

        public int? OrderType { get; set; }

        public string SupplierRef { get; set; }

        public string ContactInfo { get; set; }

        public int? DelLocationId { get; set; }

        public int? DelWareHouseID { get; set; }

        public DateTime? DeliveryDate { get; set; }

        public string DeliveryTo { get; set; }

        public int? IOUHead { get; set; }

        public decimal? Adjustment { get; set; }

        public int? MrrStatus { get; set; }

        public string SupplierCredit { get; set; }

        public string OrderComments { get; set; }

        public DateTime? ChallanDate { get; set; }

        public decimal? POAdvance { get; set; }

        public string TradeNo { get; set; }
        public DateTime? PurchaseDate { get; set; }
        public string SupplierName { get; set; }
        public string LocationName { get; set; }
        public string WareHouseName { get; set; }
        public string CurrencyName { get; set; }


        public List<TradeMrrDetails> TradeMrrDetails { get; set; }

    }

}