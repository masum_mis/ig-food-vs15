﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class SupplierPaymentOtherDetail
    {
        public int SupplierPaymentOtherDetailId { get; set; }

        public int? SupplierPaymentOtherMasterId { get; set; }

        public int? InvoiceMasterID { get; set; }

        public int? PaymentType { get; set; }

        public int? FromAccount { get; set; }
        public int? IOUAccount { get; set; }
        public int? RefDetailsId { get; set; }

        public int? CurrencyId { get; set; }

        public decimal? ConversionRate { get; set; }

        public int? BankId { get; set; }

        public int? BranchId { get; set; }

        public string RefNo { get; set; }

        public DateTime? RefDate { get; set; }

        public decimal? PaymentAmount { get; set; }

        public decimal? TaxAmount { get; set; }

        public decimal? GrandTotalD { get; set; }

        public string Remark { get; set; }

        public string InvoiceType { get; set; }

    }

}