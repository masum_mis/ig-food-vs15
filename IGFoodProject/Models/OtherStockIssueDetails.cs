﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class OtherStockIssueDetails
    {
        public string UOMName { get; set; }
        public int StockIssueDetailsId { get; set; }
        public int StockIssueMasterId { get; set; }
       

        public int ItemId { get; set; }
        public int PurposeID { get; set; }
        public string ItemCode { get; set; }
        public int UOMId { get; set; }
        public decimal StockQty { get; set; }
        public decimal StockKg { get; set; }

      
        public string Remarks { get; set; }
        public string ItemName { get; set; }
        public string Unit { get; set; }

        public int StockInDetailsId { get; set; }
        public int IssueReturnID { get; set; }
        public int IssueReturnDetailID { get; set; }
         


        public string IssueNo { get; set; }
        public string IssueDate { get; set; }
        public decimal IssueQty { get; set; }
        public decimal IssueKg { get; set; }
        public bool IsFull { get; set; }
        public decimal ReturnQty { get; set; }
        public decimal RemainingQuanity { get; set; }

        public int IssueQCRequestId { get; set; }
        public int ItemQty { get; set; }
        public decimal UnitCost { get; set; } 
        public decimal avgRate { get; set; } 
        public decimal TotalAmount { get; set; } 
        public string DepartmentName { get; set; } 
        public string ItemGroupName { get; set; } 
        public string WareHouseName { get; set; } 

    }
}