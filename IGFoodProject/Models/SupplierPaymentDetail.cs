﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class SupplierPaymentDetail
    {
        public int SupplierPaymentDetailId { get; set;}
        public int SupplierPaymentMasterId { get; set; }
        public int PaymentType { get; set; }
        public string RefNo { get; set; }
        public DateTime? RefDate { get; set; }
        public decimal PaymentAmount { get; set; }
        public int PurchaseOrderID { get; set; }
        public string PaymentTypeName { get; set; }
        public int SerialNo { get; set; }
        public int BankId{ get;set;}
        public  int BranchId { get; set; }

        public DateTime PaymentDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string BankName { get; set; }
        public string BranchName { get; set; }
        public string Remark { get; set; }

        public decimal TaxAmount { get; set; }
        public decimal GrandTotalD { get; set; }
        public string PurchaseType { get; set; }
    }
}