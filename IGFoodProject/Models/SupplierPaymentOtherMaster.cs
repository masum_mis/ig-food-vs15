﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class SupplierPaymentOtherMaster
    {
        public int SupplierPaymentOtherMasterId { get; set; }

        public int? SupplierId { get; set; }

        public decimal? PaymentAmount { get; set; }

        public DateTime? PaymentDate { get; set; }

        public decimal? TaxAmount { get; set; }

        public decimal? TaxPercent { get; set; }

        public decimal? GrandTotal { get; set; }

        public decimal? Discount { get; set; } 
        public decimal? TotalAfterDiscount { get; set; } 

        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public string Remark { get; set; }
        public int PaymentTypeId { get; set; }

        public List<SupplierPaymentOtherDetail> SupplierPaymentOtherDetails { get; set; }

    }

}