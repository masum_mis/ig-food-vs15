﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    [Table("tbl_StockInDetail")]
    public class StockInDetail
    {
        [Key]
        public int StockInDetailID { get; set; }

        public int? StockInMasterID { get; set; }

        public int? ItemID { get; set; }

        public int? ProductID { get; set; }

        public DateTime? StockInDate { get; set; }

        public decimal? StockInQty { get; set; }

        public decimal? StockInKG { get; set; }

        public int? DressedBirdDetailID { get; set; }

        public int? RefStockInDetailID { get; set; }

        public DateTime? ExpireDate { get; set; }

        public string Remark { get; set; }

        public decimal? UnitPrice { get; set; }

        public decimal? TargetQty { get; set; }

        public decimal? TargetQtyInKG { get; set; }

        public int? BDToPortionIssue2ndDetail { get; set; }

        public int? IsOpening { get; set; }

        public decimal? PUnitPrice { get; set; }

        public int? DressedBirdDetailLogId { get; set; }

        public int? PortionStockInDetailLogID { get; set; }

        public int? DressBirdStockInDetailLogID { get; set; }

        public int? FurtherProcessStockInDetailLogID { get; set; }

        public decimal? CostValue { get; set; }

        public DateTime? CreateDate { get; set; }
    }
}