﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using CrystalDecisions.CrystalReports.Engine;
using IGFoodProject.ViewModel;
using Database = System.Data.Entity.Database;

namespace IGFoodProject.Models
{
    public class CodeDbSet : DbContext
    {
        public CodeDbSet()
            : base("CodeDbSet")
        {
            Database.SetInitializer<CodeDbSet>(null);
        }
   // public DbSet<CompanyInformation> Company { get; set; }


        public DbSet<FurtherProcessPriceConfig> FurtherProcessPriceConfig { get; set; }
        public DbSet<ProcessedSalePriceConfig> ProcessedSalePriceConfig { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<ViewProcessSalesPriceConfig> ViewProcessSalesPriceConfig { get; set; }
        public DbSet<BankMaster> BankMaster { get; set; }
        public DbSet<BankBranch> BankBranches { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<WarehouseTypes> WarehouseTypes { get; set; }
        public DbSet<WareHouse> WareHouses { get; set; }
        public DbSet<tbl_ProductPackSize> PackSize { get; set; }
        public DbSet<ProcessedCostPriceConfig> ProcessedCostPriceConfig { get; set; }
        public DbSet<FurtherProcessedCostPriceConfig> FurtherProcessedCostPriceConfigs { get; set; }
        public  DbSet<QcRequestedMaster> QcRequestedMasters { get; set; }
        public DbSet<QcRequestedProduct> QcRequestedProducts { get; set; }

        public  DbSet<StockInMaster> StockInMasters { get; set; }

        public  DbSet<StockInDetail> StockInDetails { get; set; }
        public  DbSet<TradeProcessedCostPriceConfig> TradeProcessedCostPriceConfigs { get; set; }
        public  DbSet<TradeProcessedSalePriceConfig> TradeProcessedSalePriceConfigs { get; set; }

        public  DbSet<StockInMasterTrade> StockInMasterTrades { get; set; }
        public  DbSet<StockInDetailTrade> StockInDetailTrades { get; set; }
        public  DbSet<CustomerType> CustomerTypes { get; set; }

    }
}