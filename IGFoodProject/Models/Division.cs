﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class Division
    {


        public int DivisionId { get; set; }

        public string DivisionName { get; set; }

        public DateTime? CreateDate { get; set; }

        public string CreateBy { get; set; }
    }
}