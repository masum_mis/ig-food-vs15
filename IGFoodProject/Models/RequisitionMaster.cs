﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.Models
{
    public class RequisitionMaster
    {
        public int ItemRequisitionMasterId { get; set; }

        public string RequisitionNo { get; set; }

        public int Company { get; set; }

        public int WarehouseId { get; set; }

        public string StrtExpectedPurchaseDate { get; set; }
        public string RequisitionDate { get; set; }
        public string ExpectedPurchaseDate { get; set; }

        public string RequisitionBy { get; set; }

        public string Remarks { get; set; }

        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public string UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }

        public int IsApprove { get; set; }

        public string ApproveBy { get; set; }

        public DateTime? ApproveDate { get; set; }
        public string CompanyName {  get; set; }
        public string WareHouseName { get; set; }
        public string LocationName { get; set; }
        public string EmpName { get; set; }
        public int LocationId { get; set; }
        public List<RequisitionDetails> Details { get; set; }


        public string RStatus { get; set; }
        public decimal TotalQty { get; set; }
        public decimal TotalKG { get; set; }
        public decimal ApprovedTentativePrice { get; set; }
    }
}
