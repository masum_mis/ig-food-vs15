﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class OtherQCOperationDetails
    {
        public int ItemId { get; set; }
        public decimal QCQty { get; set; }
        public decimal ApproveQCQty { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal CostValue { get; set; }
         public int IssueQcOperationDetailsId { get; set; }
         public int IssueQcOperationMasterId { get; set; }
    }
}