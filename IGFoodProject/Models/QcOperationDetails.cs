﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class QcOperationDetails
    {
        public int QcOperationDetailsId { get; set; }

        public int? QcOperationMasterId { get; set; }

        public int? ProductId { get; set; }
        public int? SalesOrderChallanDetailsId { get; set; }

        public decimal? ProductQty { get; set; }

        public decimal? ProductKg { get; set; }
        public decimal? WeightlossQty { get; set; }
        public decimal? WeightlossKg { get; set; }
        public int? StockInDetailID { get; set; }
        public decimal? UnitCost { get; set; }
        public int IsTrade { get; set; }

    }
}