﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class SpecialOffers
    {
        public int OfferId { get; set; }
        public string OfferName { get; set; }
        public decimal Incentive { get; set; }
    }
}