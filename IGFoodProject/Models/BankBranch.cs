﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    [Table("tbl_BankBranch")]
    public class BankBranch
    {
        [Key]
        public int BranchId { get; set; }
        public int BankId { get; set; }
        public string BranchName { get; set; }
        public string Address { get; set; }
        public string ContactPerson { get; set; }
        public string ContactNumber { get; set; }
        public BankMaster BankMaster { get; set; }
    }
}