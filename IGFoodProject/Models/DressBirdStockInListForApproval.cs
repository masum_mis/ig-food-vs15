﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class DressBirdStockInListForApproval
    {
        public int DressBirdStockInMasterLogID { get; set; }
        public string StockInDate { get; set; }
        public string StockInBy { get; set; }
        public string WareHouseName { get; set; }
        public string LocationName { get; set; }
        public decimal StockInQty { get; set; }
        public decimal StockInKG { get; set; }
       
    }
}