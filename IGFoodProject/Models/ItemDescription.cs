﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.Models
{
    public class ItemDescription
    {
        public int ItemId { get; set; }

        public string ItemName { get; set; }

        public string ItemSpecification { get; set; }

        public string Item_Description { get; set; }

        public int? UOMId { get; set; }

        public string Remarks { get; set; }

        public bool IsActive { get; set; }

        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public string UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }

        public string ItemCode { get; set; }
    }
}
