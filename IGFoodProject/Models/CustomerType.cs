﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using CrystalDecisions.CrystalReports.Engine;

namespace IGFoodProject.Models
{
    [Table("tbl_CustomerType")]
    public class CustomerType
    {
        [Key]
        public int CustomerTypeID { get; set; }

        public string CoustomerTypeName { get; set; }

        public bool? IsActive { get; set; }
    }
}