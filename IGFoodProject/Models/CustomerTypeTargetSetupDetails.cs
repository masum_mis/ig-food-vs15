﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class CustomerTypeTargetSetupDetails
    {
        public int CustomerTypeTargetSetupDetailsId { get; set; }

        public int? CustomerTypeTargetSetupMasterId { get; set; }

        public int? SalesOfficeId { get; set; }

        public int? AreaId { get; set; }

        public int? CustomerTypeId { get; set; }

        public int? TypeId { get; set; }

        public decimal? TargetKG { get; set; }

        public decimal? TargetAmount { get; set; }

    }

}