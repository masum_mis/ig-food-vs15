﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class TradeSalesChallanDetails
    {
        public int TradeChallanDetailsId { get; set; }

        public int? TradeChallanMasterId { get; set; }
        public int? TradeSalesOrderDetailsId { get; set; }
        public int? TradeStockInDetailID { get; set; }

        public int? ProductId { get; set; }

        public decimal? IssueQty { get; set; }

        public decimal? IssueKg { get; set; }

        public decimal? IssuePrice { get; set; }

        public decimal? DeliveryQty { get; set; }

        public decimal? DeliveryKg { get; set; }

        public string DeliveryAmount { get; set; }

        public string DeliveryRemarks { get; set; }

        public decimal? SalePriceDelivery { get; set; }

        public decimal? ReturnQty { get; set; }

        public decimal? ReturnKg { get; set; }

        public decimal? WeightLossQty { get; set; }

        public decimal? WeightLossKg { get; set; }
        public decimal? UndeliveredQty { get; set; }
        public decimal? UndeliveredKg { get; set; }
    }
}