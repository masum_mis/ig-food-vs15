﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models.ReportModel
{
    public class StockLedger
    {
        public string ReportValue { get; set; }
        public string FromDate { get; set; }
        public int WareHouseId { get; set; }
    }
}