﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models.ReportModel
{
    public class SalesOrderReport
    {
        public string ReportValue { get; set; }
        public  string SalesOrderProcessedMasterId { get; set; }
        public string SalesOrderChallanMasterId { get; set; }
        public  DateTime OrderDate { get; set; }
        public  DateTime FromDate { get; set; }
        public  DateTime ToDate { get; set; }
        public int  CustomerId { get; set; }
        public int LocationId { get; set; }
        public int DepartmentId { get; set; }
        public  int WareHouseId { get; set; }
        public string StockMonth { get; set; }
        public  int GroupId { get; set; }
        public  int ProductId { get; set; }
        public  string MrrMasterId { get; set; }
        public bool IsEcommerce { get; set; }
        public int DressBirdNo { get; set; }

        public string FalseSalesOrderProcessedMasterId { get; set; }
        public string StockTreansferIssueId { get; set; }
        public  bool IsTrade { get; set; }
        public int soId { get; set; }

        public int SRId { get; set; }
        public int ChallanMasterId { get; set; } 

    }
}