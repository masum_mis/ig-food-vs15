﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models.ReportModel
{
    public class ViewYearlySalesReport
    {
        public string UOMName { get; set; }
        public int RN { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal? TotalSales1 { get; set; }
        public decimal? TotalSales2 { get; set; }
        public decimal? TotalSales3 { get; set; }
        public decimal? TotalSales4 { get; set; }
        public decimal? TotalSales5 { get; set; }
        public decimal? TotalSales6 { get; set; }
        public decimal? TotalSales7 { get; set; }
        public decimal? TotalSales8 { get; set; }
        public decimal? TotalSales9 { get; set; }
        public decimal? TotalSales10 { get; set; }
        public decimal? TotalSales11 { get; set; }
        public decimal? TotalSales12 { get; set; }
        public decimal? TotalSalesT { get; set; }


        public decimal? IssueQty1 { get; set; }
        public decimal? IssueQty2 { get; set; }
        public decimal? IssueQty3 { get; set; }
        public decimal? IssueQty4 { get; set; }
        public decimal? IssueQty5 { get; set; }
        public decimal? IssueQty6 { get; set; }
        public decimal? IssueQty7 { get; set; }
        public decimal? IssueQty8 { get; set; }
        public decimal? IssueQty9 { get; set; }
        public decimal? IssueQty10 { get; set; }
        public decimal? IssueQty11 { get; set; }
        public decimal? IssueQty12 { get; set; }
        public decimal? IssueQtyT { get; set; }


        public decimal? rating_average1 { get; set; }
        public decimal? rating_average2 { get; set; }
        public decimal? rating_average3 { get; set; }
        public decimal? rating_average4 { get; set; }
        public decimal? rating_average5 { get; set; }
        public decimal? rating_average6 { get; set; }
        public decimal? rating_average7 { get; set; }
        public decimal? rating_average8 { get; set; }
        public decimal? rating_average9 { get; set; }
        public decimal? rating_average10 { get; set; }
        public decimal? rating_average11 { get; set; }
        public decimal? rating_average12 { get; set; }
        public decimal? rating_averageT { get; set; }
                        
                        
    }                   
}                       
                        
                         