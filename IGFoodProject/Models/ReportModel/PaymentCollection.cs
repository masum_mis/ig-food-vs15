﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models.ReportModel
{
    public class PaymentCollection
    {
        public DateTime OrderDate    {get;set;}
        public string CustomerName {get;set;} 
        public int CustomerID   {get;set;}
        public string SalesOrderNo {get;set;} 
        public decimal TotalDue     {get;set;}
        public decimal PaymentAmount{get;set;} 
        public string CompanyName  {get;set;}
        public string CompanyAddres{get;set;} 
        public string PhoneNumber  {get;set;}
        public string Email        {get;set;} 
        public DateTime PaymentDate { get;set; }
    }
}