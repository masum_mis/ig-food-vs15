﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class Market
    {

        public int MarketId { get; set; }

        public int? TerritoryId { get; set; }

        public string MarketName { get; set; }

        public string CreateBy { get; set; }

        public string CreateDate { get; set; }
        public Territory aTerritory { get; set; }
    }
}