﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    [Table("tbl_StockInMaster")]
    public class StockInMaster
    {
        [Key]
        public int StockInMasterID { get; set; }

        public DateTime? StockInDate { get; set; }

        public string StockInBy { get; set; }

        public int? WareHouseId { get; set; }

        public int? DressdMasterID { get; set; }

        public DateTime? CreateDate { get; set; }

        public string CreateBy { get; set; }

        public string Comments { get; set; }

        public int? DBIssueMasterID { get; set; }

        public int? IsOpening { get; set; }

        public int? CompanyID { get; set; }

        public int? LocationId { get; set; }

        public int? IsPortion { get; set; }

        public int? GroupId { get; set; }

        public int? DressdMasterLogId { get; set; }

        public int? PortionStockInMasterLogID { get; set; }

        public int? FurtherProcessStockInMasterLogId { get; set; }

        public int? ReceiveMasterId { get; set; }
    }
}