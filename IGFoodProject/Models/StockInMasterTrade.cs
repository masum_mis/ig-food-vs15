﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    [Table("tbl_StockInMaster_Trade")]
    public class StockInMasterTrade
    {
        [Key]
        public int TradeStockInMasterID { get; set; }

        public DateTime? StockInDate { get; set; }

        public string StockInBy { get; set; }

        public int? WareHouseId { get; set; }

        public int? TradePurchaseMasterId { get; set; }

        public DateTime? CreateDate { get; set; }

        public string CreateBy { get; set; }

        public string Comments { get; set; }

        public int? IsOpening { get; set; }

        public int? CompanyID { get; set; }

        public int? LocationId { get; set; }

    }
}