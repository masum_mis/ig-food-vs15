﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class DumpingMaster
    {
        public int DumpingMasterId { get; set; }

        public DateTime? DumpingDate { get; set; }

        public string DumpingBy { get; set; }

        public decimal? TotalDumpingQty { get; set; }

        public decimal? TotalDumpingKg { get; set; }

        public int? WareHouseId { get; set; }

        public int? LocationId { get; set; }

        public int? CompanyId { get; set; }

        public int? GroupId { get; set; }

        public string Remarks { get; set; }

        public DateTime? CreateDate { get; set; }

        public string CreateBy { get; set; }
        public List<DumpingDetails> DumpingDetails { get; set; }
    }
}