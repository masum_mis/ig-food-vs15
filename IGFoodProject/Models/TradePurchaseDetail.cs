﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class TradePurchaseDetail
    {
        public int TradePurchaseDetailId { get; set; }
        public int TradePurchaseMasterId { get; set; }
        public int ProductId { get; set; }
        public decimal ProductQty { get; set; }
        public decimal ProductKg { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal TotalPrice { get; set; }
        public int UOMId { get; set; }
        public bool? IsDelete { get; set; }

        public decimal ApproveQty { get; set; }
        public decimal ApproveKg { get; set; }
        public decimal ApproveUnitPrice { get; set; }
        public decimal ApproveTotalAmount { get; set; }


    }
}