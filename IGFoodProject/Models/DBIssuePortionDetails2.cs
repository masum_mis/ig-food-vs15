﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class DBIssuePortionDetails2
    {
        public int BDToPortionIssue2ndDetail { get; set; }

        public int? DBIssueDetailID { get; set; }

        public int? DBToPortionRequisition2ndDetailID { get; set; }

        public int? ProductID { get; set; }

        public decimal? ProductQty { get; set; }

        public decimal? ProductQtyInKG { get; set; }
    }
}