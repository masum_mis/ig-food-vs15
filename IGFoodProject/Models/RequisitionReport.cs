﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class RequisitionReport
    {
        public int CompanyId { get; set; }
        public int LocationId { get; set; }
        public int WarehouseId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int RStatus { get; set; }
        public string RequisitionID { get; set; }
    }
}