﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class StockReceiveMaster
    {

        public int StockReceiveMasterId { get; set; }

        public int? StockTransferIssueMasterId { get; set; }

        public int? CompanyId { get; set; }

        public int? FromWarehouseId { get; set; }

        public int? ToWarehouseId { get; set; }

        public int? FromLocationId { get; set; }

        public int? ToLocationId { get; set; }

        public DateTime? ReceiveDate { get; set; }

        public string ReceiveBy { get; set; }

        public string ReceiveTime { get; set; }

        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public List<StockReceiveDetails> Details { get; set; } 

    }
}