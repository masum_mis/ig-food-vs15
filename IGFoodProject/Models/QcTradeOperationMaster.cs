﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class QcTradeOperationMaster
    {
        public int QcTradeOperationMasterId { get; set; }

        public DateTime? QcDate { get; set; }

        public string QcComments { get; set; }

        public int? ForwordedWarehouseId { get; set; }

        public DateTime? ChallanDate { get; set; }

        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public int? TradeSalesOrderChallanMasterId { get; set; }

    }
}