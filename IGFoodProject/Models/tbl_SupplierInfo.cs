﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.Models
{
    public class tbl_SupplierInfo
    {
        public int SupplierID { get; set; }
        public string SupplierName { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierAddress { get; set; }
        public string SupplierPhone { get; set; }
        public string SupplierSecondPhone { get; set; }
        public string SupplierEmail { get; set; }
        public string ContactPerson { get; set; }
        public string ContactPersonPhone { get; set; }
       
    }
}
