﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.Models
{
    [Table("tbl_WareHouseInfo")]
    public class WareHouse
    {
        [Key]
        public int WareHouseId { get; set; }
        public int WarehouseTypeId { get; set; }

        public string WareHouseName { get; set; }

        public int LocationId { get; set; }

        public bool IsActive { get; set; }

        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public string UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }
        public WarehouseTypes WarehouseType { get; set; }
        public Location Location { get; set; }
    }
}
