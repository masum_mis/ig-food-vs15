﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class FurtherProcessIssueDetail
    {
        public int FurtherProcessIssueDetailId { get; set; }

        public int FurtherProcessIssueMasterId { get; set; }

        public int ReqDetailId { get; set; }

        public int ProductId { get; set; }

        public int StockInDetailId { get; set; }

        public decimal IssueQty { get; set; }

        public decimal IssueKg { get; set; }

        public string Remarks { get; set; }

        public decimal? UnitPrice { get; set; }
        public  string ProductName { get; set; }
    }
}