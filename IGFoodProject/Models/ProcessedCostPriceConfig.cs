﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    [Table("tbl_ProcessedCostPriceConfig")]
    public class ProcessedCostPriceConfig
    {
        [Key]
        public int ProcessedCostPriceId { get; set; }

        public int ProductId { get; set; }

        public decimal PurchasePrice { get; set; }

        public decimal CostPrice { get; set; }

        public bool IsActive { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public  Product Product { get; set; }
    }
}