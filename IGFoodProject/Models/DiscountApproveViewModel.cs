﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class DiscountApproveViewModel
    {
        public int DealerId { get; set; }
        public string DealerInfo { get; set; }

        public int MonthId { get; set; }
        public int YearId { get; set; }
        public decimal? DeliveryDiscount { get; set; }
    }
}