﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class PortionStockInMaster_Log
    {
        public int PortionStockInMasterLogID { get; set; }

        public DateTime? StockInDate { get; set; }

        public string StockInBy { get; set; }

        public int? WareHouseId { get; set; }
        public int? ToWarehouseId { get; set; }

        public int? IssueId { get; set; }

        public DateTime? CreateDate { get; set; }

        public string CreateBy { get; set; }

        public string Comments { get; set; }

        public int? CompanyID { get; set; }

        public int? LocationId { get; set; }

        public int? GroupId { get; set; }

        public bool? IsApprove { get; set; }

        public string ApproveBy { get; set; }

        public DateTime? ApproveDate { get; set; }

        public List<PortionStockInDetail_Log>  ADetails { get; set; }

    }

}