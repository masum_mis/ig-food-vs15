﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class CustomerTypeTargetSetupMaster
    {
        public int CustomerTypeTargetSetupMasterId { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public string MonthName { get; set; }

        public int? MonthId { get; set; }

        public string YearName { get; set; }

        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public string UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }

        public string Title { get; set; }

        public string Remarks { get; set; }

        public string TargetSetupNo { get; set; }
        public bool IsEdit { get; set; }
        public string AreaName { get; set; }
        public string SalesOfficer { get; set; }
        public List<CustomerTypeTargetSetupDetails> CustomerTypeTargetSetupDetails { get; set; }

    }

}