﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class TermsAndcondition
    {
        public int TermsAndConditionId { get; set; }

        public int? PurchaseOrderID { get; set; }

        public string TermsOfDelivery { get; set; }

        public string TermsOfPackaging { get; set; }

        public string TermsOfPayments { get; set; }

        public string TermsOfBilling_Doc { get; set; }

        public string OtherTerms { get; set; }
        public string SpecialInstruction { get; set; }

    }

}