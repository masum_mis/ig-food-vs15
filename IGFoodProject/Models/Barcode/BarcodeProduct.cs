﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models.Barcode
{
    public class BarcodeProduct
    {
        public int ProductId { get; set; } 
        public string Name { get; set; }
        public string Description { get; set; }
        public byte[] BarcodeImage { get; set; }
        public string Barcode { get; set; }
        public string ImageUrl { get; set; }


        public int GroupId { get; set; }
        public int TypeId { get; set; }
        public int CategoryId { get; set; }
        public int PackSizeId { get; set; }
        public int UOMId { get; set; } 

    }
}