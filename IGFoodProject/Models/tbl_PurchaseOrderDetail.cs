﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.Models
{
    public class tbl_PurchaseOrderDetail
    {
        public int PurchaseOrderDetailID { get; set; }
        public int PurchaseOrderID { get; set; }
        public int ItemId { get; set; }
        public decimal PurchaseQty { get; set; }
        public decimal PurchaseKG { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal TotalPrice { get; set; }
        public string Remarks { get; set; }
        public decimal MRRRemainingQty { get; set; }
        public int ItemRequisitionDetailsId { get; set; }


        public string ItemName { get; set; }

        public decimal RemainingQty { get; set; }
        public decimal RemainingKG { get; set; }
        public  DateTime OrderDate { get; set; }
    }
}
