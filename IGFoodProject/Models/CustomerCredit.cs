﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class CustomerCredit
    {
        public int CustomerTypeID { get; set; }

        public string CustomerTypeName { get; set; }

        public int CustomerID { get; set; }

        public string CustomerName { get; set; }

        public DateTime? CreateDate { get; set; }

        public decimal Jan { get; set; }
        public decimal Feb { get; set; }
        public decimal Mar { get; set; }
        public decimal Apr { get; set; }
        public decimal May { get; set; }
        public decimal Jun { get; set; }
        public decimal Jul { get; set; }
        public decimal Aug { get; set; }
        public decimal Sep { get; set; }
        public decimal Oct { get; set; }
        public decimal Nov { get; set; }
        public decimal Dec { get; set; }

        public decimal JanUnUse { get; set; }
        public decimal FebUnUse { get; set; }
        public decimal MarUnUse { get; set; }
        public decimal AprUnUse { get; set; }
        public decimal MayUnUse { get; set; }
        public decimal JunUnUse { get; set; }
        public decimal JulUnUse { get; set; }
        public decimal AugUnUse { get; set; }
        public decimal SepUnUse { get; set; }
        public decimal OctUnUse { get; set; }
        public decimal NovUnUse { get; set; }
        public decimal DecUnUse { get; set; }

    }
}