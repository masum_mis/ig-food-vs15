﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.Models
{
    public class tbl_ProductPackSize
    {
        [Key]
        public int PackSizeId { get; set; }
        
        public string PackSizeName { get; set; }
      
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public decimal? PackWeight { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public bool? IsActive { get; set; }
    }
}
