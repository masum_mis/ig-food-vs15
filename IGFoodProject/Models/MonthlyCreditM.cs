﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class MonthlyCreditM
    {
        public int CustomerId { get; set; }
        public int Year { get; set; }
        public string CustomerName { get; set; }
        public string MonthlyEntry { get; set; }
        public decimal TotalCredit { get; set; }
        public DateTime Createdate { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}