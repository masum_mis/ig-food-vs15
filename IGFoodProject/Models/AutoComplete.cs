﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.Models
{
    public class AutoComplete
    {
        public string label { get; set; }
        public string value { get; set; }
    }
}
