﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class FurtherProcessRequisitionMaster
    {
        public int PortionToFurtherProcessReqId { get; set; }

        public int CompanyId { get; set; }

        public int LocationId { get; set; }

        public int WareHouseId { get; set; }

        public int ToWareHouseId { get; set; }

        public int IsApprove { get; set; }

        public int IssueStatus { get; set; }

        public string RequisitionNo { get; set; }

        public DateTime RequisitionDate { get; set; }

        public DateTime ProductionDate { get; set; }

        public string Comments { get; set; }

        public string CreateBy { get; set; }

        public DateTime CreateDate { get; set; }

        public string UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }

        public string ApproveBy { get; set; }

        public DateTime? ApproveDate { get; set; }

        public string RequisitionBy { get; set; }
        public  List<FurtherProcessRequisitionDetail> FurtherProcessRequisitionDetail { get; set; }
    }
}