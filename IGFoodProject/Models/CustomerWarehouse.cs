﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class CustomerWarehouse
    {

        public int? CustomerWarehouseId { get; set; }

        public int? CustomerID { get; set; }

        public string CustomerWarehouseName { get; set; }

        public string WareHouseArea { get; set; }

        public string WareHouseAddress { get; set; }
    }
}