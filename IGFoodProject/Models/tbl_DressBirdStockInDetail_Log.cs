﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class tbl_DressBirdStockInDetail_Log
    {
        public int DressBirdStockInDetailLogID { get; set; }
        public int DressBirdStockInMasterLogID { get; set; }
        public int ItemID { get; set; }
        public int ProductID { get; set; }
        public string StockInDate { get; set; }
        public decimal StockInQty { get; set; }
        public decimal StockInKG { get; set; }
        public decimal ApproveInQty { get; set; }
        public decimal ApproveInKG { get; set; }
        public int DressedBirdDetailID { get; set; }
        public int RefStockInDetailID { get; set; }
        public string ExpireDate { get; set; }
        public decimal UnitPrice { get; set; }
        public string Remark { get; set; }

        public string DressBirdStockInDetailLogIDs { get; set; }
        public int rowid { get; set; }


        public decimal PUnitPrice { get; set; }
        public int ItemCategoryId { get; set; }
        public string itemName { get; set; }
        public string ProductName { get; set; }
        public string DressedBirdDetailIDs { get; set; }
    }
}