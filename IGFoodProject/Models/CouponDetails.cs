﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class CouponDetails
    {
        public int ShopPolicyDetailId { get; set; }

        public string PolicyTitle { get; set; }
        public decimal Percentage { get; set; }
        public string CouponCode { get; set; }
        public string ReturnText { get; set; }
       
    }
}