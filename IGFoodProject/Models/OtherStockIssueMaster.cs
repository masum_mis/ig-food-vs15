﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class OtherStockIssueMaster
    {
        public int StockIssueMasterId { get; set; }
        public int IssueReturnID { get; set; }
        public int RequisitionMasterId { get; set; }
        public string IssueNo { get; set; }
        public int CompanyId { get; set; }
        public int ProjectId { get; set; }

        public int WarehouseId { get; set; }
        public int DepartmentId { get; set; }
       

        public int LocationId { get; set; }

        public string ReturnNo { get; set; }

        public string IssueBy { get; set; }

        public DateTime ReturnDate { get; set; }

        public DateTime IssueDate { get; set; }

        public string Remarks { get; set; }

        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public string UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }

        public List<OtherStockIssueDetails> StockIssueDetails { get; set; }
        public List<OtherQCOperationDetails> IssueQcDetails { get; set; }
        public int? VehicleId { get; set; }
        public string DriverName { get; set; }
        public string DriverMobile { get; set; }

        public string CompanyName { get; set; }
        public bool IsFull { get; set; }

        public decimal TotalIssueQuantity { get; set; }
        public decimal TotalIssueKg { get; set; }
        public string WareHouseName { get; set; }
        public string LocationName { get; set; }
        public string DepartmentName { get; set; }

        public decimal ReturnQty { get; set; }
        public decimal RemainingQuanity { get; set; }

        public decimal TotalReturnQty { get; set; }


        public int IssueQcRequestedMasterId { get; set; }
        public int IssueQCRequestId { get; set; }

        // for QC
        public DateTime QcDate { get; set; }
        public string QcComments { get; set; }
        public int ForwordedWarehouseId { get; set; }
        public int IssueQcOperationMasterId { get; set; }
        //public int ItemId { get; set; }
        //public decimal QCQty { get; set; }
        //public decimal UnitPrice { get; set; } 
        //public decimal CostValue { get; set; }
        //public int IssueQcOperationDetailsId { get; set; } 
        

    }
}