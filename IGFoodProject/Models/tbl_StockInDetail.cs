﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.Models
{
    public class tbl_StockInDetail
    {
        [Key]
        public int StockInDetailID { get; set; }
        public int StockInMasterID { get; set; }
        public int ItemID { get; set; }
        public int ProductID { get; set; }
        public string StockInDate { get; set; }
        public decimal StockInQty { get; set; }
        public decimal StockInKG { get; set; }
        public int DressedBirdDetailID { get; set; }
        public int RefStockInDetailID { get; set; }
        public string ExpireDate { get; set; }
        public decimal UnitPrice { get; set; }
        public string Remark { get; set; }

        public decimal TargetQty { get; set; }
        public decimal TargetQtyInKG { get; set; }
        public int BDToPortionIssue2ndDetail { get; set; }
        public int IsOpening { get; set; }
        public decimal PUnitPrice { get; set; }

        public int DressedBirdDetailLogId { get; set; }
        public int FurtherProcessStockInDetailLogID { get; set; }
        public DateTime CreateDate { get; set; }

        public string DressedBirdDetailLogIds { get; set; }
        public decimal PerKgStdQty { get; set; }
        public int GroupId { get; set; }
    }
}
