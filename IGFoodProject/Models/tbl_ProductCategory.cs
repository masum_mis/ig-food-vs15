﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.Models
{
    public class tbl_ProductCategory
    {
        public int CategoryId { get; set; }
        public int GroupId { get; set; }
        public int TypeId { get; set; }
        public string GroupName { get; set; }
        public string TypeName { get; set; }
        public string CategoryCode { get; set; }
        public string CategoryName { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public bool? IsActive { get; set; }
    }
}
