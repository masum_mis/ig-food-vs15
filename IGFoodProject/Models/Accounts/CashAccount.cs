﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IGFoodProject.Models.Accounts
{
    [Table("tbl_CashAccount")]
    public class CashAccount
    {
        [Key]
        public int CashAccountId { get; set; }

        public string AccountName { get; set; }

        public string Description { get; set; }

        public int? SubsidiryLId { get; set; }

        public int CompanyId { get; set; }

        public int CALayerSevenId { get; set; }
        [NotMapped]
        public int CALayerSixId { get; set; }
        public  ChartOfAccLayerSeven ChartOfAccLayerSeven { get; set; }
        [NotMapped]
        public  string LayerSevenCode { get; set; }

        [NotMapped]
        public  int LayerFourId { get; set; }
        [NotMapped]

        public  int LayerFiveId { get; set; }
        [NotMapped]

        public  string CompanyName { get; set; }
    }
}