﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models.Accounts
{
    public class Modules
    {
        public int ModuleId { get; set; }
        public string ModuleName { get; set; }
    }
}