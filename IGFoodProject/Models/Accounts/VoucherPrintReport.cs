﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models.Accounts
{
    public class VoucherPrintReport
    {
        public string ReportValue { get; set; }
        public int VoucherMasterId { get; set; }

        public string VoucherType { get; set; }

        public string VoucherCode { get; set; }

        public DateTime VoucherDate { get; set; }

        public int CompanyId { get; set; }
        public string Company { get; set; }
        public int BUId { get; set; }
        public string BusinessUnit { get; set; }
        public int FinancialYearId { get; set; }
        public int FinancialYear { get; set; }
        public int CurrencyId { get; set; }
        public string Currency { get; set; }
        public decimal CurrencyConRate { get; set; }

        public string Narration { get; set; }

        public decimal TotalAmount { get; set; }

        public decimal TotalAmountBDT { get; set; }

        public int? PaymentTypeId { get; set; }
        public string PaymentType { get; set; }
        public string ChequeNo { get; set; }

        public DateTime? ChequeDate { get; set; }

        public string RefNo { get; set; }

        public DateTime? RefDate { get; set; }

        public string Remarks { get; set; }

        public string EntryBy { get; set; }

        public DateTime EntryDate { get; set; }

        public string UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }
        public int? BankId { get; set; }
        public string Bank { get; set; }
        public int? BranchId { get; set; }
        public string Branch { get; set; }
        public bool IsApprove { get; set; }
        public int SalesOrderChallanMasterId { get; set; }
        public string Category { get; set; }  
    }
}