﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models.Accounts
{
    public class COAHeadConfigureViewModel
    {
        public int COAHeadConfigureId { get; set; }

        public int? OperationMasterId { get; set; }

        public int? OperationDetailsId { get; set; }

        public int? CALayerSevenId { get; set; }

        public string CrOrDr { get; set; }

        public bool? IsCrIncrease { get; set; }

        public string InputTypeName { get; set; }

        public string InputTypeValue { get; set; }
    }
}