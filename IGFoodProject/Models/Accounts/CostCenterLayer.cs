﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IGFoodProject.Models.Accounts
{
    [Table("tbl_CostCenterLayer")]
    public class CostCenterLayer
    {
        [Key]
        public int CostCenterLayerId { get; set; }

        public string CostCenterLayerName { get; set; }
    }
}