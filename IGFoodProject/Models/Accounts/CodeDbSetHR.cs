﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using IGFoodProject.Models;

namespace IGAccounts.Models
{
    public class CodeDbSetHR:DbContext
    {
        public CodeDbSetHR()
            : base("CodeDbSetHR")
        {
            Database.SetInitializer<CodeDbSet>(null);
        }

        public DbSet< CompanyInformation> Company { get; set; }
    }
}