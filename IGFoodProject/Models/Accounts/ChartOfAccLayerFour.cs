﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IGFoodProject.Models.Accounts
{
    [Table("tbl_ChartOfAccLayerFour")]
    public class ChartOfAccLayerFour
    {
        [Key]
        public int CALayerFourId { get; set; }

        public int CALayerThreeId { get; set; }

        public string CALayerFourCode { get; set; }
        public string CALayerFourName { get; set; }
        [NotMapped]
        public string Code { get; set; }

        public string Description { get; set; }

        public bool IsActive { get; set; }

        public DateTime ActiveDate { get; set; }

        public DateTime? InactiveDate { get; set; }

        public string EntryBy { get; set; }

        public DateTime EntryDate { get; set; }

        public string UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }
        public  ChartOfAccLayerThree ChartOfAccLayerThree { get; set; }
        public  IList<ChartOfAccLayerFive> ChartOfAccLayerFiveList { get; set; } 
    }
}