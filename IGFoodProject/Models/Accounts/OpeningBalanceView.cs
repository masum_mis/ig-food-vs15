﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models.Accounts
{
    public class OpeningBalanceView
    {
        public DateTime DueDate { get; set; }
        public int? OpeningBalanceId { get; set; }
        public int CALayerSevenId { get; set; }
        public string CALayerSevenCode { get; set; }
        public string CALayerSevenName { get; set; }
        public int? ReferanceTypeId { get; set; }
        public int? RefDetailsId { get; set; }
        public string DrCr { get; set; }
        public string RefName { get; set; }
        public decimal? OpeningBalance { get; set; }
    }
}