﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models.Accounts
{
    public class FinancialYear
    {
      public int  FinancialYearId { get; set; }
      public string FYear { get; set; }
      public DateTime? FromDate { get; set; }
      public DateTime? ToDate { get; set; }
      public bool? IsActive { get; set; }
    }
}