﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models.Accounts
{
    public class GLInquiry
    {
        public int SLNo { get; set; }
        public int VoucherMasterId { get; set; }
        public string VoucherType { get; set; }
        public string VoucherRefNo { get; set; }
        public DateTime? VoucherDate { get; set; }
        public string Particular { get; set; }
        public string Memo { get; set; }
        public string PersonOrItem { get; set; }
        public decimal? DebitAmount { get; set; }
        public decimal? CreditAmount { get; set; }
        public decimal? Balance { get; set; }
        public string VoucherTime { get; set; }
        public  string LedgerGroupName { get; set; }
        public DateTime? OpeningDate { get; set; }
    }
}