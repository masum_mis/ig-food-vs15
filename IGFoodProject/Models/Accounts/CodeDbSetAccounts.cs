﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models.Accounts
{
    public class CodeDbSetAccounts : DbContext
    {
        public CodeDbSetAccounts()
            : base("CodeDbSetAccounts")
        {
            Database.SetInitializer<CodeDbSetAccounts>(null);
        }

        public DbSet<LedgerGroup> LedgerGroup { get; set; }
        public DbSet<ChartOfAccLayerTwo> ChartOfAccLayerTwo { get; set; }
        public DbSet<ChartOfAccLayerThree> ChartOfAccLayerThree { get; set; }
        public DbSet<ChartOfAccLayerFour> ChartOfAccLayerFour { get; set; }
        public DbSet<ChartOfAccLayerFive> ChartOfAccLayerFive { get; set; }
        public DbSet<ChartOfAccLayerSix> ChartOfAccLayerSix { get; set; }
        public DbSet<ChartOfAccLayerSeven> ChartOfAccLayerSeven { get; set; }
       public DbSet<BankAccount> BankAccount { get; set; }
        public DbSet<Currency> Currency { get; set; }
       public DbSet<CashAccount> CashAccount { get; set; }

        public DbSet<COAHeadConfigure> COAHeadConfigures { get; set; }
        public DbSet<AnOperationMaster> AnOperationMasters { get; set; }
        public DbSet<AnOperationDetails> AnOperationDetails { get; set; }

        public System.Data.Entity.DbSet<CostCenterType> CostCenterTypes { get; set; }

      // public System.Data.Entity.DbSet<CompanyInformation> Companies { get; set; }

        public System.Data.Entity.DbSet<CostCenter> CostCenters { get; set; }

        public System.Data.Entity.DbSet<CostCenterLayer> CostCenterLayers { get; set; }

        public DbSet<ReferanceType> ReferanceTypes { get; set; }
        public DbSet<BusinessUnit> BusinessUnits { get; set; }
    }
}