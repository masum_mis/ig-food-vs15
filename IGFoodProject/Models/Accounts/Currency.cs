﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IGFoodProject.Models.Accounts
{
    [Table("tbl_Currency")]
    public class Currency
    {
        [Key]
        public int CurrencyId { get; set; }

        public string CurrencyName { get; set; }

        public string CurrencyCode { get; set; }

        public string CurrencyIcon { get; set; }

    }
}