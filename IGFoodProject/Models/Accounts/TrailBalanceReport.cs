﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models.Accounts
{
    public class TrailBalanceReport
    {
       public string ReportValue { get; set; }
       public DateTime FromDate { get; set; }
       public   DateTime ToDate { get; set; }
       public bool all { get; set; }
       public bool tran { get; set; }
    }
}