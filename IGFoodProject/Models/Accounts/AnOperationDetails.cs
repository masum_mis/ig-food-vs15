﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models.Accounts
{
    [Table("tbl_AnOperationDetails")]
    public class AnOperationDetails
    {
        [Key]
        public int OperationDetailsId { get; set; }

        public int? OperationMasterId { get; set; }

        public string InputTypeName { get; set; }

        public string InputTypeValue { get; set; }

        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public string UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }
    }
}