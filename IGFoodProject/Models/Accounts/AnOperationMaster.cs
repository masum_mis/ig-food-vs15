﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models.Accounts
{
    [Table("tbl_AnOperationMaster")]
    public class AnOperationMaster
    {
        [Key]
        public int OperationMasterId { get; set; }

        public string OperationName { get; set; }

        public int? MenuId { get; set; }

        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public string UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }
    }
}