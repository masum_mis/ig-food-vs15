﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models.Accounts
{
    public class OpeningBalance
    {
        public int OpeningBalanceId { get; set; }

        public DateTime? DueDate { get; set; }

        public int? CALayerSevenId { get; set; }

        public int? RefTypeId { get; set; }

        public int? RefDetailsId { get; set; }

        public string DrCr { get; set; }

        public decimal? BalanceLC { get; set; }

        public decimal? OBLC { get; set; }

        public decimal? BalanceFC { get; set; }

        public decimal? OBFC { get; set; }

        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public string UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }
    }
}