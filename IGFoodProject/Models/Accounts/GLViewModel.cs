﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models.Accounts
{
    public class GLViewModel
    {
        public int LedgerGroupId { get; set; }
        public int CALayerTwoId { get; set; }
        public int CALayerThreeId { get; set; }
        public int CALayerFourId { get; set; }
        public int CALayerFiveId { get; set; }
        public int CALayerSixId { get; set; }
        public int CALayerSevenId { get; set; }
        public string CALayerSevenName { get; set; }
        public string CALayerSevenCode { get; set; }
        public bool IsActive { get; set; }
        public bool IsCostCenter { get; set; }
        public bool IsReferance { get; set; }
        public string Description { get; set; }
        public int BankId { get; set; }
        public int BranchId { get; set; }
    
    }
}