﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IGFoodProject.Models.Accounts
{
    [Table("tbl_ChartOfAccLayerSix")]
    public class ChartOfAccLayerSix
    {
        [Key]
        public int CALayerSixId { get; set; }

        public int CALayerFiveId { get; set; }

        public string CALayerSixCode { get; set; }
        public string CALayerSixName { get; set; }
        [NotMapped]
        public string Code { get; set; }
        public string Description { get; set; }

        public bool IsActive { get; set; }

        public DateTime ActiveDate { get; set; }

        public DateTime? InactiveDate { get; set; }

        public string EntryBy { get; set; }

        public DateTime EntryDate { get; set; }

        public string UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }
        public  ChartOfAccLayerFive ChartOfAccLayerFive { get; set; }

        public  IList<ChartOfAccLayerSeven> ChartOfAccLayerSevenList { get; set; }
    }
}