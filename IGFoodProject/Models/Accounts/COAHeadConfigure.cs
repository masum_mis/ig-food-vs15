﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models.Accounts
{
    [Table("tbl_COAHeadConfigure")]
    public class COAHeadConfigure
    {
        [Key]
        public int COAHeadConfigureId { get; set; }

        public int? OperationMasterId { get; set; }

        public int? OperationDetailsId { get; set; }

        public int? CALayerSevenId { get; set; }

        public string CrOrDr { get; set; }

        public bool? IsCrIncrease { get; set; }

        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public string UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }
        public AnOperationMaster AnOperationMaster { get; set; }
        public AnOperationDetails AnOperationDetails { get; set; }
        public ChartOfAccLayerSeven ChartOfAccLayerSeven { get; set; }

    }
}
