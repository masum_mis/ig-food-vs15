﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class SalesOrderMasterTrade
    {
        public int TradeSalesOrderMasterId { get; set; }

        public string SalesOrderNo { get; set; }

        public string CustomerType { get; set; }

        public string CustomerName { get; set; }

        public string OrderBy { get; set; }

        public int? CustomerTypeId { get; set; }

        public int? CustomerId { get; set; }

        public string SalesPerson { get; set; }

        public DateTime? OrderDate { get; set; }

        public string DeliveryDestination { get; set; }

        public string DeliveryRcvPerson { get; set; }

        public string DeliveryRcvPersonNumber { get; set; }

        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public string UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }

        public int IsApprove { get; set; }

        public string ApproveBy { get; set; }

        public DateTime? ApproveDate { get; set; }

        public int? PaymentType { get; set; }

        public int? CompanyId { get; set; }

        public string SalesPersonContact { get; set; }

        public int? CollectionType { get; set; }

        public DateTime? DeliveryDate { get; set; }

        public decimal? TotatalOrderAmtKg { get; set; }

        public decimal? TotatalOrderAmtQty { get; set; }
        public decimal? TotalOrderQty { get; set; }

        public decimal? TotalAmount { get; set; }

        public decimal? TotalDiscount { get; set; }

        public decimal? NetPayable { get; set; }

        public string Remarks { get; set; }

        public decimal? TotalDue { get; set; }

        public decimal? TotalPaid { get; set; }

        public bool? IsChallanGenerate { get; set; }
        public bool? IsEmployee { get; set; }

        public int? IsChallanComplete { get; set; }

        public int? BankId { get; set; }

        public int? BranchId { get; set; }

        public string ChequeNo { get; set; }

        public DateTime? ChequeDate { get; set; }

        public int? ColBankId { get; set; }

        public int? ColBranchId { get; set; }

        public string ColChequeNo { get; set; }

        public DateTime? ColChequeDate { get; set; }

        public decimal? TotalDueFromDelivery { get; set; }

        public int? DivisionId { get; set; }

        public int? TerritoryId { get; set; }

        public int? AreaId { get; set; }

        public int? MarketId { get; set; }

        public int? DistrictId { get; set; }

        public int? ThanaId { get; set; }

        public int? FullPicked { get; set; }

        public string ApprovalUpdateBy { get; set; }

        public DateTime? ApprovalUpdateDate { get; set; }

        public decimal? ModifiedQty { get; set; }

        public decimal? ModifiedKg { get; set; }

        public string PreRemark { get; set; }

        public List<SalesOrderDetailsTrade> SalesDetailsTrade { get; set; }
    }
}