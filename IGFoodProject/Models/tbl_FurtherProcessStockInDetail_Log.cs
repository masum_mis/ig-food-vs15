﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class tbl_FurtherProcessStockInDetail_Log
    {
        public int FurtherProcessStockInDetailLogID { get; set; }
        public int FurtherProcessStockInMasterLogId { get; set; }
       
        public int ProductID { get; set; }
        public string StockInDate { get; set; }
        public decimal StockInQty { get; set; }
        public decimal StockInKG { get; set; }
        public decimal ApproveInQty { get; set; }
        public decimal ApproveInKG { get; set; }
        
        public string ExpireDate { get; set; }
        public decimal UnitPrice { get; set; }
        public string Remark { get; set; }


        public decimal PUnitPrice { get; set; }
        
    }
}