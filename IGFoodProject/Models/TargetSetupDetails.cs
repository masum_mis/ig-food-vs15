﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class TargetSetupDetails
    {
        public int TargetSetupDetailsId { get; set; }

        public int? TargetSetupMasterId { get; set; }

        public int? CategoryId { get; set; }

        public int? SalesOfficeId { get; set; }

        public int? AreaId { get; set; }

        public decimal? TargetKG { get; set; }
        public decimal? RestTargetKG { get; set; }
        public decimal? RestTargetAmount { get; set; }
        
        public decimal? ActualTargetKG { get; set; }
        public decimal? ActualTargetAmount { get; set; }
        public decimal? TargetAmount { get; set; }
        public int? CustomerId { get; set; }
        public string CategoryName { get; set; }
        public int CustomerTypeId { get; set; }
        public int TypeId { get; set; }
        
        public string AreaName { get; set; }
        public string SalesOfficeName { get; set; }
        public decimal OverallTargetKG { get; set; }
        public decimal AvailableTargetKG { get; set; }
        public decimal OverallTargetAmount { get; set; }
        public decimal AvailableTargetAmount { get; set; }


    }

}