﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class PolicyDetails
    {
        public int PolicyDetailId { get; set; }
        public int PolicyMasterId { get; set; }
        public int GroupId { get; set; }
        public int CustomerTypeId { get; set; }
       
        public string PolicyTitle { get; set; }
        public decimal Percentage { get; set; }
        public string Remarks { get; set; }
    }
}