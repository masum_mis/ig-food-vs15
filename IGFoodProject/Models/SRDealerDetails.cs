﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class SRDealerDetails
    {
        public int SrDealerDetailsId { get; set; }

        public int SrId { get; set; }

        public int DealerId { get; set; }

        public bool? IsActive { get; set; }
        public string ActiveStatus { get; set; }

        public DateTime? ActiveDate { get; set; }

        public DateTime? DeactiveDate { get; set; }

        public int? CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public int? UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }
        public SRInfoMaster SRInfoMaster { get; set; }
        public tbl_CustomerInformation CustomerInformation { get; set; }
    }
}