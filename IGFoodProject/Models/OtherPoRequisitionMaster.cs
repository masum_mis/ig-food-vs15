﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class OtherPoRequisitionMaster
    {
        public int RequisitionMasterId { get; set; }

        public string RequisitionNo { get; set; }
        public int CompanyId { get; set; }

        public int WarehouseId { get; set; }
        public int DepartmentId { get; set; }
        public string eflag { get; set; }


        public int LocationId { get; set; }



        public string RequisitionBy { get; set; }
        public string WareHouseName { get; set; }
        public string LocationName { get; set; }
        public string DepartmentName { get; set; }
        public string RequisitionByName { get; set; }



        public DateTime RequisitionDate { get; set; }
        public DateTime ExpectedPoDate { get; set; }
        public decimal ExpectedPrice { get; set; }

        public string Remarks { get; set; }

        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public string UpdateBy { get; set; }
        public bool IsApprove { get; set; }

        public DateTime? UpdateDate { get; set; }

        public List<OtherPoRequisitionDetails> PoRequisitionDetailses { get; set; }
        public int? VehicleId { get; set; }
        public string DriverName { get; set; }
        public string DriverMobile { get; set; }

        public string CompanyName { get; set; }
        public bool IsLocal { get; set; }






        public decimal TotalRequisitionQuantity { get; set; }
        public decimal TotalRequisitionKg { get; set; }
        public int TolerancePercent { get; set; }
        public int TotalQtyWithTolerance { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int RStatus { get; set; }
        public DateTime RequiredDate { get; set; }
        public string RequiredTime { get; set; }
        public string PurchaseStatus { get; set; }
    }
}