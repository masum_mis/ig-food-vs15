﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class tbl_FurtherProcessStockInMaster_Log
    {
        public int FurtherProcessStockInMasterLogId { get; set; }
        public string StockInDate { get; set; }
        public string StockInBy { get; set; }
        public int WareHouseId { get; set; }
       
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public string Comments { get; set; }
       
        public int FurtherProcessIssueMasterId { get; set; }
        public List<tbl_FurtherProcessStockInDetail_Log> aDetail { get; set; }

        public int CompanyId { get; set; }
        public int LocationId { get; set; }

       
        public int GroupId { get; set; }

        
       

        public bool IsApprove { get; set; }
        public string ApproveBy { get; set; }
        public DateTime ApproveDate { get; set; }
    }
}