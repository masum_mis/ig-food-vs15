﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class SRInfoMaster
    {
        public int SrId { get; set; }

        public string SrName { get; set; }

        public string SrAddress { get; set; }

        public string SrMobileNumber { get; set; }

        public string SrEmail { get; set; }
        public List<SRDealerDetails> SRDealerDetailsList { get; set; }
    }
}