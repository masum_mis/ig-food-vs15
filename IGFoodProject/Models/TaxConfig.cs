﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class TaxConfig
    {
        public int TaxInfoId { get; set; }

        public int ProductGroupId { get; set; }

        public decimal? FromAmount { get; set; }

        public decimal? ToAmount { get; set; }

        public decimal? TaxPercent { get; set; }

        public decimal? TaxDecimal { get; set; }
    }
}