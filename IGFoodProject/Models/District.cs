﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class District
    {
        public int DistrictId { get; set; }

        public int? DivisionId { get; set; }

        public string DistrictName { get; set; }

        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }
    }
}