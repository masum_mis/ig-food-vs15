﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    [Table("tbl_StockInDetail_Trade")]
    public class StockInDetailTrade
    {
        [Key]
        public int TradeStockInDetailID { get; set; }

        public int? TradeStockInMasterID { get; set; }

        public int? ProductID { get; set; }

        public DateTime? StockInDate { get; set; }

        public decimal? StockInQty { get; set; }

        public decimal? StockInKG { get; set; }

        public int? TradePurchaseDetailId { get; set; }

        public int? RefTradeStockInDetailID { get; set; }

        public DateTime? ExpireDate { get; set; }

        public string Remark { get; set; }

        public decimal? UnitPrice { get; set; }

        public int? IsOpening { get; set; }

        public DateTime? CreateDate { get; set; }
    }
}