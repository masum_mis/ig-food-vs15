﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class QcTradeOperationDetails
    {
        public int QcTradeOperationDetailsId { get; set; }

        public int? QcTradeOperationMasterId { get; set; }

        public int? ProductId { get; set; }

        public decimal? ProductQty { get; set; }

        public decimal? ProductKg { get; set; }

        public int? TradeStockInDetailID { get; set; }

        public decimal? WeightlossQty { get; set; }

        public decimal? WeightlossKg { get; set; }

        public decimal? UnitCost { get; set; }

        public int? TradeChallanDetailsId { get; set; }
    }
}