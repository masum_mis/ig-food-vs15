﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class tbl_DressBirdStockInMaster_Log
    {
        public int DressBirdStockInMasterLogID { get; set; }
        public string StockInDate { get; set; }
        public string StockInBy { get; set; }
        public int WareHouseId { get; set; }
        public int DressdMasterID { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public string Comments { get; set; }
        public int IsOpening { get; set; }
        public int DBIssueMasterID { get; set; }
        public List<tbl_DressBirdStockInDetail_Log> aDetail { get; set; }

        public int CompanyId { get; set; }
        public int LocationId { get; set; }

        public int IsPortion { get; set; }
        public bool checkrd { get; set; }
        public int GroupId { get; set; }

        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int StoreWarehouse { get; set; }

        public bool IsApprove { get; set; }
        public string ApproveBy { get; set; }
        public DateTime ApproveDate { get; set; }
    }
}