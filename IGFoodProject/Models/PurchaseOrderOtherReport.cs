﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class PurchaseOrderOtherReport
    {
        public string ReportValue { get; set; }
        public int POMasterId { get; set; }
        public string PurchaseOrderMasterId { get; set; }
    }
}