﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class DumpingDetails
    {
        public int DumpingDetailId { get; set; }

        public int? DumpingMasterId { get; set; }

        public int? ProductID { get; set; }

        public DateTime? DumpingDate { get; set; }

        public decimal? DumpingQty { get; set; }

        public decimal? DumpingKg { get; set; }

        public int? StockInDetailId { get; set; }

        public decimal? UnitPrice { get; set; }

        public string DumpingRemarks { get; set; }
        public int? StockInMasterId { get; set; }
        public  int? RefStockInDetailID { get; set; }
    }
}