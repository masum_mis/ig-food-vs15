﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class LiveBirdMortalityDetails
    {

        public int MortalityDetailsId { get; set; }

        public int? MortalityMasterId { get; set; }

        public int? MrrDetailsId { get; set; }

        public int? ItemId { get; set; }

        public decimal? MortalityQty { get; set; }

        public decimal? MortalityKg { get; set; }

        public int? LbStockinId { get; set; }


    }
}