﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class StockAdjustmentMaster
    {
        public int AdjustmentId { get; set; }

        public int WarehouseId { get; set; }

        public int ProductId { get; set; }

        public DateTime AdjustmentDate { get; set; }
        public int ReasonId { get; set; }

        public string Reason { get; set; }

        public string CreateBy { get; set; }

        public DateTime CreateDate { get; set; }
        public List<StockAdjustmentDetails> AdjustmentDetails { get; set; }
}
}