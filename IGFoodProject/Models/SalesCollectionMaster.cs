﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class SalesCollectionMaster
    {
            public int SalesCollectionMasterId { get; set; }

            public int? MarketId { get; set; }

            public int? CustomerId { get; set; }
        public DateTime? InvoiceDate { get; set; }

        public decimal? PaymentAmount { get; set; }
        public decimal? RestAmount { get; set; }

        public DateTime? PaymentDate { get; set; }

            public string CreateBy { get; set; }

            public DateTime? CreateDate { get; set; }

            public List<SalesCollectionDetail> SalesCollectionDetails { get; set; }
        public string Remark { get; set; }
        public bool? IsAdvanced { get; set; }


        public decimal? TaxAmount { get; set; }
        public decimal? TaxPercent { get; set; }
        public decimal? GrandAmount { get; set; }
        public int MasterRefIdForAutoAdvance { get; set; }

        public int IsSrSalaryAdujst { get; set; }
        public int IsRetaintionAdjust { get; set; }
        public int IsDamageAdjust { get; set; }
        public int IsPromotionAdjust { get; set; }
        public int IsFreezerRentAdjust { get; set; }

        public int FromAccount { get; set; }
        public int IsChaldalAdjust { get; set; }
        public int IsSalesReturnAdvAdjust { get; set; }
        public int IsDiscount { get; set; }
        public int IsDamageAdjustOther { get; set; }
        
    }
}