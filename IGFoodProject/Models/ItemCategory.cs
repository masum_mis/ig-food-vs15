﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class ItemCategory
    {
        public int ItemCategoryId { get; set; } 
        public string ItemCategoryName { get; set; } 
        public int ItemTypeId { get; set; }
        public string ItemTypeName { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public bool? IsActive { get; set; }
    }
}