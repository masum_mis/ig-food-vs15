﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class RequisitionPurpose
    {
        public int PurposeID { get; set; }
        public string PurposeName { get; set; }
    }
}