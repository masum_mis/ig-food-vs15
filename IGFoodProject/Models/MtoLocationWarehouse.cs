﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class MtoLocationWarehouse
    {
        public int WarehouseId { get; set; }
        public int CustomerId { get; set; }
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public string WareHouseName { get; set; }
    }
}