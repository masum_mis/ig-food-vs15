﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class StockTransferReqMaster
    {
        public int StockTransferReqMasterId { get; set; }
        public string RequisitionNo { get; set; }
        public int? CompanyId { get; set; }

        public int? FromWarehouseId { get; set; }

        public int? ToWarehouseId { get; set; }

        public int? FromLocationId { get; set; }

        public int? ToLocationId { get; set; }

        public string RequisitionBy { get; set; }

        public DateTime? RequisitionDate { get; set; }

        public DateTime? ExpectedDeliveryDate { get; set; }

        public string Remarks { get; set; }

        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public string UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }

        public List<StockTransferReqDetails> StockTransferReqDetails { get; set; }
        public int? VehicleId { get; set; }
        public string DriverName { get; set; }
        public string DriverMobile { get; set; }

        public string CompanyName { get; set; }
        public string FromLocation { get; set; }
        public string fromWarehouse { get; set; }
        public string ToLocation { get; set; }
        public string ToWarehouse { get; set; }
        public decimal TotalRequisitionQuantity { get; set; }
        public decimal TotalRequisitionKg { get; set; }

        public int GroupId { get; set; }

        public int FullPicked { get; set; }

        public decimal TotalIssueQuantity { get; set; }
        public decimal TotalIssueKg { get; set; }
        public int InternalTransferCommentId { get; set; }
        public  string Comment { get; set; }
        public bool IsInterSTO { get; set; }

        public string PickingStatus { get; set; } 
    }
}