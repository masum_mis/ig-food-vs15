﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    [Table("tbl_TradeProcessedCostPriceConfig")]
    public class TradeProcessedCostPriceConfig
    {
        [Key]
        public int TradeProcessedCostPriceId { get; set; }

        [Required(ErrorMessage = "Product Required")]
        [Display(Name = "Product")]
        public int? ProductId { get; set; }

        [Required(ErrorMessage = "Purchase Price Required")]
        [Display(Name = "Purchase Price")]
        public decimal PurchasePrice { get; set; }

        [Required(ErrorMessage = "Sale Price Required")]
        [Display(Name = "Sale Price")]
        public decimal? CostPrice { get; set; }

        public bool IsActive { get; set; }
        [Required(ErrorMessage = "From Date Required")]
        [Display(Name = "From Date")]
        public DateTime? FromDate { get; set; }

        [Required(ErrorMessage = "To Date Required")]
        [Display(Name = "To Date")]
        public DateTime? ToDate { get; set; }

        public Product Product { get; set; }
    }
}