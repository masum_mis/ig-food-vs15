﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class PickingRequestMaster
    {
        public int PickingMasterId { get; set; }

        public int? ConpamyId { get; set; }

        public int? LocationId { get; set; }

        public int? WareHouseId { get; set; }

        public int? AreaId { get; set; }

        public int? TerritoryId { get; set; }

        public int? MarketId { get; set; }

        public int? DistrictId { get; set; }

        public int? ThanaId { get; set; }

        public int? CustomerId { get; set; }

        public int? CustomerTypeId { get; set; }

        public int VehicleId { get; set; }

        public DateTime? DeliveryDate { get; set; }
        public DateTime? OrderdateFrom { get; set; }
        public DateTime? OrderDateTo { get; set; }

        public string PickingNo { get; set; }

        public string PickingTime { get; set; }

        public string CreateBy { get; set; }

        public DateTime CreateDate { get; set; }

        public string UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }

        public int? IsPicked { get; set; }

        public string Remarks { get; set; }

    }
}