﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class SrSalaryApproval
    {
        public int DealerId { get; set; }
        public string DealerInfo { get; set; }
        public string SRInfo { get; set; }
        public int SrId { get; set; }
        public int MonthId { get; set; }
        public int YearId { get; set; }
        public decimal? SalaryAmount { get; set; }
    }
}