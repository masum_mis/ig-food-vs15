﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class tbl_RestaturantCustomerInfo
    {
        public int RestaturantCustomerID { get; set; }
        public string ResCustomerCode { get; set; }
        public string ResCustomerName { get; set; }
        public string PersonalAddress { get; set; }
        public string Phone { get; set; }
        public string Phone2 { get; set; }
        public bool IsActive { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }
    }
}