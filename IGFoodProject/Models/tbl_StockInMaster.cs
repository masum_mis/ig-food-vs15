﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.Models
{
    public class tbl_StockInMaster
    {
        [Key]
        public int StockInMasterID { get; set; }
        public string StockInDate { get; set; }
        public string StockInBy { get; set; }
        public int WareHouseId { get; set; }
        public int DressdMasterID { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public string Comments { get; set; }
        public int IsOpening { get; set; }
        public int DBIssueMasterID { get; set; }
        public List<tbl_StockInDetail> aDetail { get; set; }

        public int CompanyId { get; set; }
        public int LocationId { get; set; }

        public int IsPortion { get; set; }
        public bool checkrd { get; set; }
        public int GroupId { get; set; }

        public  DateTime FromDate { get; set; }
        public  DateTime ToDate { get; set; }
        public int StoreWarehouse { get; set; }

        public int DressdMasterLogId { get; set; }

        public int FurtherProcessStockInMasterLogId { get; set; }



    }
}
