﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class ProceesPriceConfig
    {
        public int ItemCategoryId { get; set; }
        public string ItemCategoryName { get; set; }
        public decimal UnitPrice { get; set; }
        public int oldFlag { get; set; }
        public DateTime orderdate { get; set; }
        public int ProcessSaleMasterID { get; set; }
    }
}