﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.Models
{
    public class MrRDetails
    {
        public int MRRDetailID { get; set; }

        public int? MRRMasterId { get; set; }

        public int? ItemID { get; set; }

        public decimal? ItemQty { get; set; }

        public decimal? ItemKG { get; set; }

        public int? PurchaseOrderDetailID { get; set; }

        public decimal? RemainigDressedBird { get; set; }
        public string Remarks { get; set; }
        public string ReceiveStatus { get; set; }

    }
}
