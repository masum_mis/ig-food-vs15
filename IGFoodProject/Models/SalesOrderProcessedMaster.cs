﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class SalesOrderProcessedMaster
    {
        public int SalesOrderProcessedMasterId { get; set; }

        public string SalesOrderNo { get; set; }

        public string OrderBy { get; set; }

        public int? CustomerId { get; set; }
        public int? SrId { get; set; }
        public int? OrderType { get; set; }

        public string SalesPerson { get; set; }
        public string SalesPersonContact { get; set; }

        public DateTime? OrderDate { get; set; }
        public DateTime? DeliveryDate { get; set; }

        public string DeliveryDestination { get; set; }

        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public string UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }

        public int IsApprove { get; set; }

        public string ApproveBy { get; set; }

        public DateTime? ApproveDate { get; set; }

      

        public int? GroupId { get; set; }

        public int? PaymentType { get; set; }

        public int? CompanyId { get; set; }
        public decimal TotatalOrderAmtKg { get; set; }
        public decimal TotalOrderQty { get; set; }
        public decimal TotalIncTaxVat { get; set; }
        public decimal TotalExTaxVat { get; set; }
        public decimal TaxVat { get; set; }
        public string Remarks { get; set; }
        public int CollectionType { get; set; }

        public decimal CollectionAmount { get; set; }
        public decimal TotalDue { get; set; }

        public decimal TotalPaid { get; set; }

        public List<SalesOrderProcessedDetails> SalesDetails { get; set; } 

        public bool IsEmployee { get; set; }
        public int CustomerTypeId { get; set; }

       

        public int? BankId { get; set; }
        public int? BranchId { get; set; }
        public string ChequeNo { get; set; }
        public DateTime? ChequeDate { get; set; }


        public int? ColBankId { get; set; }
        public int? ColBranchId { get; set; }
        public string RefNo { get; set; }
        public DateTime? RefDate { get; set; }
        public string ColChequeNo { get; set; }
        public DateTime? ColChequeDate { get; set; }
        public string eflag { get; set; }

        public string ApprovalUpdateBy { get; set; }

        public DateTime? ApprovalUpdateDate { get; set; }
        public bool IsEcommerce { get; set; }

        public DateTime? DeliveryDateSuggested { get; set; }
        public int? DeliveryWarehouseId { get; set; }
        public int? RetailCustomerID { get; set; }
        public decimal? SalesPoint { get; set; }
        public string RetailName { get; set; }
        public string PersonalAddress { get; set; }
        public string Phone { get; set; }
        public int? LocationId { get; set; }

        public decimal CreditAmount { get; set; } 
        public decimal LedgerBalance { get; set; }
        public decimal AdditionalDiscountPer { get; set; }

        public decimal disPercentage { get; set; }
        public string CouponCode { get; set; }


        public int PolicyTypeId { get; set; }
        public string TypeName { get; set; } 
    }
}