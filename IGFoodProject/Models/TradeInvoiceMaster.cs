﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.Models
{
    public class TradeInvoiceMaster
    {
        public int TradeInvoiceMasterID { get; set; }

        public string TradeInvoiceNo { get; set; }

        public DateTime? TradeInvoiceDate { get; set; }

        public int? SupplierID { get; set; }

        public bool? IsApprove { get; set; }

        public string ApproveBy { get; set; }

        public DateTime? ApproveDatetime { get; set; }

        public decimal? TotalQty { get; set; }

        public decimal? TotalKG { get; set; }

        public decimal? TotalAmount { get; set; }

        public int? IsDelete { get; set; }

        public string DeleteBy { get; set; }

        public DateTime? DeleteDate { get; set; }

        public string CreateBy { get; set; }

        public DateTime? CreateDate { get; set; }

        public string UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }

        public DateTime? DueDate { get; set; }

        public int? TradeInvoiceStatus { get; set; }

        public decimal? TotalDue { get; set; }

        public decimal? TotalPaid { get; set; }

        public decimal? TotalTax { get; set; }

        public decimal? GrandTotal { get; set; }

        public decimal? TotalTaxPaid { get; set; }

        public DateTime? BillDate { get; set; }

        public string BillNo { get; set; }

        public decimal? VatPercent { get; set; }

    }

}