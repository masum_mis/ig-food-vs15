﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewGeneratedChallanDetails
    {
        public  string ProductName { get; set; }
        public  decimal IssueQty { get; set; }
        public  decimal IssueKg { get; set; }
        public  decimal TotalPrice { get; set; }
        public  decimal ChallanPrice { get; set; }
        public  decimal SaleQty { get; set; }
        public  decimal SaleKg { get; set; }

        //////
        public decimal DeliveryQty { get; set; }
        public decimal DeliveryKG { get; set; }
        public decimal DeliveryPrice { get; set; }

        public decimal DeliveryAmount { get; set; }
        public int SalesReturnId { get; set; }
        public int SalesReturnMasterId { get; set; }
        public int SalesOrderChallanMasterId { get; set; }
        public int SalesOrderChallanSecondDetailsId { get; set; }
        public int ProductId { get; set; }
        public DateTime ReturnDate { get; set; }
        public decimal ReturnQty { get; set; }
        public decimal ReturnKG { get; set; }
        public decimal ReturnAmount { get; set; }
        public int WarehouseId { get; set; }
        public string UploadFileName { get; set; }
        public string FileFolderLocation { get; set; }
        public string Remarks { get; set; }
        public string Note { get; set; }
    }
}