﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewGeneratedChallan
    {

        public int SalesOrderChallanMasterId { get; set; }
        public string SalesOrderChallanNo { get; set; }
        public string DeliveryDestination { get; set; }
        public string SalesOrderNo { get; set; }
        public string OrderBy { get; set; }
        public string CreateBy { get; set; }
        public decimal ChallanAmount { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? ChallanDate { get; set; }
        public DateTime? OrderDate { get; set; }
        public List<ViewGeneratedChallanDetails> Details { get; set; }
        public int GroupId { get; set; }
        public string CustomerName { get; set; }
        public string SalesPersonContact { get; set; }
        public string GroupName { get; set; }

        ///////////////////////////////////////
        public string DeliveryBy { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public decimal DeliveryAmount { get; set; }

        public decimal ChallanQty { get; set; }
        public decimal ChallanKg { get; set; }
    }
}