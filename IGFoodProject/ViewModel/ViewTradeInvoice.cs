﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewTradeInvoice
    {
        public string ReportValue { get; set; }
        public int TradeMRRMasterID { get; set; }
        public int TradePurchaseOrderId { get; set; }
        public string TradePONo { get; set; }
        public DateTime TradeMRRDate { get; set; }
        public DateTime DueDate { get; set; }
        public int SupplierID { get; set; }

        public string MrrNo { get; set; }
        public string ProductDescription { get; set; }



        public string SupplierRef { get; set; }
        public string SupplierName { get; set; }

        public decimal TotalAmount { get; set; }
        public decimal TotalQty { get; set; }



        public decimal BillQuantity { get; set; }
        public decimal ReceivedQty { get; set; }

        public decimal TransportCost { get; set; }
        public decimal LaborCost { get; set; }

        public int InvoiceMasterID { get; set; }
        public string InvoiceNo { get; set; }
        public DateTime InvoiceDate { get; set; }
        public decimal TotalDue { get; set; }
        public decimal TotalPaid { get; set; }
        public decimal GrandTotal { get; set; }
        public decimal TotalTax { get; set; }
        public decimal TotalTaxPaid { get; set; }

        public string InvoiceType { get; set; }

        public decimal POAdvance { get; set; }
        public decimal VatAmount { get; set; }
        public decimal VatPercent { get; set; }
        public string SupplierBillNo { get; set; }
        public DateTime SupplierBillDate { get; set; }
    }
}