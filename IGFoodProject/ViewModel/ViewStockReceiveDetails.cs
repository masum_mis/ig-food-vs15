﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewStockReceiveDetails
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal IssueQty { get; set; }
        public decimal PrevIssueQty { get; set; }
        public decimal PrevIssueKg { get; set; }
        public decimal IssueKg { get; set; }
        public string ExpireDate { get; set; }
        public int GroupId { get; set; }
        public int StockTransferIssueDetailsId { get; set; }
    }
}