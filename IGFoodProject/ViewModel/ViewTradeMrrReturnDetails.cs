﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewTradeMrrReturnDetails
    {
        public int TradeMrrReturnDetailID { get; set; }
        public int TradeMrrReturnMasterID { get; set; }
        public int ProductId { get; set; }
        public decimal MRRQty { get; set; }
        public decimal MRRKG { get; set; }
        public decimal ReturnQty { get; set; }
        public decimal ReturnKG { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal TotalPrice { get; set; }
        public string ProductName { get; set; }
        public string UOMName { get; set; }
        
    }
}