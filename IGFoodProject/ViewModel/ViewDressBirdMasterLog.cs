﻿using IGFoodProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewDressBirdMasterLog
    {
        public int DressBirdStockInMasterLogID { get; set; }
        public DateTime StockInDate { get; set; }
        public string StockInBy { get; set; }
        public int WareHouseId { get; set; }
        public int DressdMasterID { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public string Comments { get; set; }
        public int CompanyID { get; set; }
        public int LocationId { get; set; }
        public int GroupId { get; set; }
        public int DressWarehouse { get; set; }
        public int DressLocation { get; set; }
        public List<tbl_DressBirdStockInDetail_Log> adetail { get; set; }
    }
}