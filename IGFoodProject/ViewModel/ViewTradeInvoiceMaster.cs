﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewTradeInvoiceMaster
    {
        public string ReportValue { get; set; }
        public int TradeMrrMasterId { get; set; }
        public int TradePurchaseOrderId { get; set; }
        public string TradeNo { get; set; }
        public DateTime MRRDate { get; set; }
        public DateTime DueDate { get; set; }
        public int SupplierID { get; set; }
        public string MrrNo { get; set; }
        public string ProductDescpriction { get; set; }
        public string SupplierRef { get; set; }
        public string SupplierName { get; set; }
        public decimal TransportCost { get; set; }
        public decimal LaborCost { get; set; }
        public decimal PreInvoiceAmount { get; set; }
        public decimal RestMrrAmount { get; set; }
        public decimal RestInvoiceAmount { get; set; }
        public int TradeInvoiceMasterID { get; set; }
        public string TradeInvoiceNo { get; set; }
        public DateTime TradeInvoiceDate { get; set; }
        public decimal TotalQty { get; set; }
        public decimal TotalKG { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal TotalDue { get; set; }
        public decimal TotalPaid { get; set; }
        public decimal GrandTotal { get; set; }
        public decimal TotalTax { get; set; }
        public decimal TotalTaxPaid { get; set; }
        public int TradeInvoiceStatus { get; set; }
        public decimal VatAmount { get; set; }
        public decimal VatPercent { get; set; }
        public string SupplierBillNo { get; set; }
        public DateTime SupplierBillDate { get; set; }
        public decimal TradePOAdvance { get; set; }
        public List<ViewTradeInvoiceDetails> InvoiceDetails { get; set; }

    }
}