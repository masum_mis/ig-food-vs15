﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.ViewModel
{
    public class ViewMrrDetails
    {
        public string ItemDescription { get; set; }
        public int PurchaseOrderDetailID { get; set; }
        public int MRRDetailID { get; set; }
        public int ItemId { get; set; }
        public decimal PurchaseQty { get; set; }
        public string Remarks { get; set; }
        public decimal MRRRemainingQty { get; set; }
        public decimal ItemQty { get; set; }
        public decimal PrevItemQty { get; set; }
        public decimal ItemKG { get; set; }
        public decimal MRRRemainingKG { get; set; }
        public string ReceiveStatus { get; set; }
        public int pReceiveStatus { get; set; }

        public decimal toleranceqty { get; set; }
        public decimal preqty { get; set; }

        public decimal othqty { get; set; }
    }
}
