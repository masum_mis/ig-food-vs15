﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewDBRequsitionDetail
    {
        public int StockInMasterID { get; set; }
        public int StockInDetailID { get; set; }
        public int ProductID { get; set; }
        public int CategoryId { get; set; }
        public string ProductName { get; set; }
        public string TypeName { get; set; }
        public string GroupName { get; set; }
        public decimal StockInQty { get; set; }
        public decimal StockInKG { get; set; }
        public string Remark { get; set; }

        //Modified by Zillur Rahman On behalf of Shahinur apu
        public  decimal UnitPrice { get; set; }
    }
}