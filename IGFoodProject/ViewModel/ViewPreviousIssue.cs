﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewPreviousIssue
    {

        public decimal PrevIssueQty { get; set; }
        public decimal PrevIssueKg { get; set; }
        public decimal ProductId { get; set; }
    }
}