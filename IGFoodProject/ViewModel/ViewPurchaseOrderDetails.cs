﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.ViewModel
{
    public class ViewPurchaseOrderDetails
    {
       public string  ItemDescription{get;set;}
       public int PurchaseOrderDetailID{get;set;}
       public int PurchaseOrderID{get;set;}
       public int ItemId{get;set;}
       public decimal PurchaseQty{get;set;}
       public decimal PurchaseKG {get;set;}
       public decimal UnitPrice {get;set;}
       public decimal TotalPrice {get;set;}
       public string Remarks{get;set;}
       public decimal MRRRemainingQty {get;set;}
       public decimal MRRRemainingKG { get;set;}
       public int ItemRequisitionDetailsId { get; set; }
        public decimal toleranceqty { get; set; }
        public decimal preqty { get; set; }

    }
}
