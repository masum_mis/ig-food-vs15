﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewDbPortionReqMaster
    {

        public int   CompanyID { get; set; }
        public int LocationID { get; set; }
        public int WareHouseID { get; set; }
        public int ToWarehouseId { get; set; }
        public string RequisitionDate { get; set; }
        public string ProductionDate { get; set; }
        //public int ToWarehouseId { get; set; }
        public string FromWarehouseName { get; set; }
        public string ToWarehouseName { get; set; }
    }
}