﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewFurtherStockInListForApproval
    {
        public int FurtherProcessStockInMasterLogId { get; set; }
        public string StockInDate { get; set; }
        public string StockInBy { get; set; }
        public string WareHouseName { get; set; }
        public string LocationName { get; set; }
        public decimal StockInQty { get; set; }
        
    }
}