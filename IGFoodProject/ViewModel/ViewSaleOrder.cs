﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewSaleOrder
    {
        public int SalesOrderProcessedMasterId { get; set; }

        public string SalesOrderNo { get; set; }
        public DateTime OrderDate { get; set; }
    }
}