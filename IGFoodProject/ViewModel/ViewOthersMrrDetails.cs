﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewOthersMrrDetails
    {
        public int MRRDetailID { get; set; }
        public int ItemId { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string UOMName { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal PurchaseQty { get; set; }
        public decimal MRRRemainingQty { get; set; }
        public decimal ReceivedQty { get; set; }
        public decimal MrrQty { get; set; }
        public decimal? ChallanQty { get; set; }
        public int PackSizeId { get; set; }
        public string PackSizeName { get; set; }
        public int NoOfPack { get; set; }
        public DateTime MFGDate { get; set; }
        public DateTime ExpireyDate { get; set; }
        public decimal RemainingQuanity { get; set; }
        public decimal ReturnQty { get; set; }

    }
}