﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewProductDetailsSales
    {
        public int ProductID { get; set; }
        public decimal StockQty { get; set; }
        public decimal StockKg { get; set; }
        public decimal StockPrice { get; set; }
        public decimal UnitPrice { get; set; }
        public string CategoryName { get; set; }
        // further process

        public decimal MRP { get; set; }
        public decimal TPPrice { get; set; }
        public decimal DPPrice { get; set; }
        public int groupid { get; set; }

        public decimal BasePrice { get; set; }

        public bool IsBulk { get; set; }
        public decimal MTRetention { get; set; }

        public  decimal PerKgStdQty { get; set; }
        public  decimal Incentive { get; set; }

        public decimal MRPPrice { get; set; }

    }
}