﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewPortionIssusDetailsLog
    {


        public int ProductID { get; set; }
        public int PortionStockInDetailLogID { get; set; }
        public decimal StockInQty { get; set; }
        public decimal StockInKG { get; set; }
        public decimal UnitPrice { get; set; }
        public string ProductName { get; set; }
        public DateTime? ExpireDate { get; set; }
    }
}