﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewDbToPortionReqReport
    {
        public int DBToPortionReqID { get; set; }
        public DateTime RequisitionDate { get; set; }
        public string RequisitionNo { get; set; }
        public DateTime ProductionDate { get; set; }
        public string Comments { get; set; }
        public string FromWareHouse { get; set; }
        public string ToWareHouse { get; set; }
        public string ProductName { get; set; }
        public decimal ProductQty { get; set; }
        public decimal ProductQtyInKG { get; set; }
        public string Remarks { get; set; }
        public decimal UnitPrice { get; set; }
    }
}