﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewOthersMrr
    {
        public string ReportValue { get; set; }
        public int MRRrOtherID { get; set; }
        public int PurchaseOrderOtherID { get; set; }
        public string PurchaseOrderNo { get; set; }
        public DateTime? OrderDate { get; set; }
        public DateTime? ExpectedDeliveryDate { get; set; }
        public int SupplierID { get; set; }
        public string SupplierName { get; set; }
        public string MrrNo { get; set; }
        public int DelLocationId { get; set; }
        public int DelWareHouseID { get; set; }
        public int CurrencyId { get; set; }
        public int MRRStatus { get; set; }
        public string LocationName { get; set; }
        public string WareHouseName { get; set; }
        public string CurrencyName { get; set; }
        public string Comments { get; set; }
        public string SupplierRef { get; set; }
        public string DeliveryTo { get; set; }
       
        public decimal OrderTotalAmt { get; set; }
        public decimal? RestAmount { get; set; }



        public DateTime MrrDate { get; set; }
        public DateTime? LastRcvDate { get; set; }
        public decimal? TotalAmountRcv { get; set; }
        public decimal MRRQty { get; set; }
        public decimal? TotalQtyRcv { get; set; }
        public string ChallanNo { get; set; }
        public string LabResult { get; set; }
        public string SpecialInstruction { get; set; }
        public string VehicleNo { get; set; }
        public string VehicleInWeight { get; set; }
        public string VehicleOutWeight { get; set; }
        public decimal TransportCost { get; set; }
        public decimal LaborCost { get; set; }
        public DateTime? ChallanDate { get; set; }

        public List<ViewOthersMrrDetails> ViewOthersMrrDetailsList { get; set; }
        public bool IsApprove { get; set; }
        public string InvoiceStatus { get; set; }
    }
}