﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewTradeMrrReturnMaster
    {
        public int TradeMRRReturnMasterID { get; set; }
        public int TradeMrrMasterId { get; set; }
        public int SupplierID { get; set; }
        public int DelLocationId { get; set; }
        public int DelWareHouseID { get; set; }
        public DateTime TradeReturnDate { get; set; }
        public string TradeReturnNo { get; set; }
        public string Remarks { get; set; }
        public decimal TotalQty { get; set; }
        public decimal TotalKG { get; set; }
        public decimal TotalAmount { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string SupplierName { get; set; }
        public string LocationName { get; set; }
        public string WareHouseName { get; set; }
        public string MrrNo { get; set; }

        public List<ViewTradeMrrReturnDetails> TradeMrrReturnDetails { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}