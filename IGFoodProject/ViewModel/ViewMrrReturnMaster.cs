﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewMrrReturnMaster
    {
        public int ReturnID { get; set; }
        public int MrrId { get; set; }
        public int SupplierID { get; set; }
        public int DelLocationId { get; set; }
        public int DelWareHouseID { get; set; }
        public DateTime ReturnDate { get; set; }
        public string ReturnNo { get; set; }
        public string Remarks { get; set; }
        public decimal TotalQty { get; set; }
      
        public decimal TotalAmount { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public List<ViewMrrReturnDetails> ReturnDetails { get; set; }
        public string SupplierName { get; set; }
        public string LocationName { get; set; }
        public string WareHouseName { get; set; }
        public string MrrNo { get; set; }
    }
}