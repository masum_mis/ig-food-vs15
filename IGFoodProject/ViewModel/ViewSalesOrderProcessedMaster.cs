﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewSalesOrderProcessedMaster
    {
        public int SalesOrderProcessedMasterId { get; set; }
        public int PickingMasterId { get; set; }
        public string SalesPerson { get; set; }
        public string SalesOrderNo { get; set; }
        public string PickingNo { get; set; }
        public string SalesPersonContact { get; set; }
        public string CustomerName { get; set; }
        public string DeliveryDestination { get; set; }
        public decimal TotatalOrderAmtKg { get; set; }
        public decimal TotatalOrderAmtQty { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal TotalDiscount { get; set; }
        public decimal TotalIncTaxVat { get; set; }
        public decimal TotalExTaxVat { get; set; }
        public decimal TotalDue { get; set; }
        public decimal TotalPaid { get; set; }
        public int  IsApprove { get; set; }
        public bool ? IsEmployee { get; set; }
        public string ApproveBy { get; set; }
        public DateTime? ApproveDate { get; set; }
        public DateTime? OrderDate { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public string CustomerType { get; set; }
        public  string DeliveryWarehouse { get; set; }
        public string RetailCode { get; set; }
        public string RetailName { get; set; }
        public string RetailPhoneNo { get; set; }
        public string RetailPersonalAddress { get; set; }

    }
}