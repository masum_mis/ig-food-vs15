﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewPortionStockTemp
    {

        public int PortionStockInMasterLogID { get; set; }
        public DateTime? StockInDate { get; set; }
        public string LocationName { get; set; }
        public string IssueNo { get; set; }
        public string WareHouseName { get; set; }
        public string StockInBy { get; set; }
    }
}