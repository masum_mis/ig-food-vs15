﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewTradePurchaseList
    {
        public int TradePurchaseMasterId { get; set; }
        public string CompanyName { get; set; }
        public string TradeNo { get; set; }
        public string IsDeliveryCompanyOther { get; set; }
        public string DeliveryLocation { get; set; }
        public string deliveryWarehouse { get; set; }
        public string DeliveryOther { get; set; }
        public string PurchaseDate { get; set; }
        public string LocalOrSupplier { get; set; }
        public string SupplierName { get; set; }
        public string LocalSupplier { get; set; }
        public string CommonOrFixCustomer { get; set; }
        public string CustomerName { get; set; }
        public string GrandTotalAmount { get; set; }
        public string Discount { get; set; }
        public string NetPayable { get; set; }

    }
}