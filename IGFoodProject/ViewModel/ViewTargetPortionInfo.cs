﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewTargetPortionInfo
    {
        public int BDToPortionIssue2ndDetail { get; set; }
        public int ProductID { get; set; }
        public DateTime StockInDate { get; set; }
        public decimal StockInQty { get; set; }
        public decimal StockInKG { get; set; }
        public DateTime ExpireDate { get; set; }
        public string Remark { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal TargetQty { get; set; }
        public decimal TargetQtyInKG { get; set; }
        public string ProductName { get; set; }


        public string CompanyShortName { get; set; }
        public string LocationName { get; set; }
        public string WareHouseName { get; set; }
        public int WareHouseID { get; set; }
        public int CompanyId { get; set; }
        public int LocationId { get; set; }
        public int serialno { get; set; }


    }
}