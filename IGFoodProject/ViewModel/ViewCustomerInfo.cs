﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewCustomerInfo
    {
        public int CustomerID { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string DivisionName { get; set; }
        public string DistrictName { get; set; }
        public string MarketName { get; set; }
        public string PaymentType { get; set; }
        public string CoustomerTypeName { get; set; }
        public string TerritoryName { get; set; }
        public string AreaName { get; set; }
        public string DefaultInventoryLocation { get; set; }
        public string PersonalAddress { get; set; }
        public DateTime? CreateDate { get; set; }
        public int CustomerTypeID { get; set; }
        public string PrimaryPhone { get; set; }
        public string SecondaryPhone { get; set; }
        public bool IsEcommerce { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string MobileNo { get; set; }
        public string NationalIDNo { get; set; }
        public bool IsCredit { get; set; }
        public string CostCenterName { get; set; }
        public string CreditType { get; set; }
        public decimal LadgerBalance { get; set; }
        public decimal CreditAmount { get; set; }
        public decimal OpeningCredit { get; set; } 

    }
}