﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewMrrReturnDetails
    {
        public int ReturnDetailID { get; set; }
        public int ReturnID { get; set; }
        public int ItemId { get; set; }
        public decimal MRRQty { get; set; }
        public decimal ReturnQty { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal TotalPrice { get; set; }
        public string ItemName { get; set; }
        public string UOMName { get; set; }
    }
}