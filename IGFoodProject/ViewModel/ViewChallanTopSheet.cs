﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewChallanTopSheet
    {
        public decimal IssueQty { get; set; }
        public decimal IssueKg { get; set; }
        public string VehicleRegNo { get; set; }
        public string SalesOrderNo { get; set; }
        public string SalesOrderChallanNo { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public string CustomerName { get; set; }
        public string WareHouseName { get; set; }
        public string DeliveryDestination { get; set; }
        public string Gateway { get; set; }
        public int MasterId { get; set; }
    }
}