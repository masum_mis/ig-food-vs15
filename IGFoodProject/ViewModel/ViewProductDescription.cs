﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.ViewModel
{
    public class ViewProductDescription
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int TypeId { get; set; }
        public decimal PerKgStdQty { get; set; }
    }
}
