﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.ViewModel
{
    public class ViewPurchaseOrderReportMaster
    {
        public int PurchaseOrderID{get;set;}
	    public string PurchaseOrderNo{get;set;}
	    public DateTime? OrderDate{get;set;}
	    public string OrderBY{get;set;}
	    public DateTime? ExpectedDeliveryDate{get;set;}
	    public string Comments{get;set;}
	    public string ApproveStatus{get;set;}
        public string WareHouseName{get;set;}
        public string LocationName{get;set;}
        public string CompanyName{get;set;}
        public string SupplierName{get;set;}
        public string ApproveBy{get;set;}
        public string StatusDes{get;set;}
        public string RequisitionNo{get;set;}
        public DateTime? RequisitionDate{get;set;}
    }
}
