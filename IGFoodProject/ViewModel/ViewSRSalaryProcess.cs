﻿using IGFoodProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewSRSalaryProcess
    {
        public List<SRDealerDetails> SRDealerDetailsList { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int EmpId { get; set; }
    }
}