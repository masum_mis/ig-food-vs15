﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace IGFoodProject.ViewModel
{
    public class ViewSalesOrderMasterForPicking
    {
        public int PickingMasterId { get; set; }
        public int SalesOrderMasterId { get; set; }
        public DateTime Orderdate { get; set; }
        public string SalesOrderNo { get; set; }
        public string CustomerName { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerType { get; set; }
        public string Market { get; set; }
        public string Territory { get; set; }
        public string Area { get; set; }
        public string Division { get; set; }
        public int CompanyId { get; set; }
        public int LocationId { get; set; }
        public int WarehouseId { get; set; }
        public int AreaId { get; set; }
        public int TerritoryId { get; set; }
        public int MarketId { get; set; }
        public int DivisionId { get; set; }
        public int ThanaId { get; set; }
        public int CustomerId { get; set; }
        public int CustomerTypeId { get; set; }
        public int VehicleId { get; set; }
        public DateTime DeliveryDate { get; set; }
        public DateTime OrderDateFrom { get; set; }
        public DateTime OrderDateTo { get; set; }
        public int FullPicked { get; set; }
        public List<ViewSalesOrderDetailsForPicking> SalesOrderDetailsForPicking { get; set; }
    }
}