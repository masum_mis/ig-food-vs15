﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewTradeInvoiceDetails
    {
        
        public int TradeInvoiceDetailID { get; set; }
        public int TradeInvoiceId { get; set; }
        public int TradeMRRMasterID { get; set; }
        public DateTime TradeMRRDate { get; set; }
        public decimal TotalInvoicePrice { get; set; }
        public decimal VatPercent { get; set; }
        public string MrrNo { get; set; }
        public decimal MrrAmount { get; set; }
        public decimal InvoiceAmount { get; set; }
        public string ProductDescription { get; set; }
        public decimal TradePoAdvanced { get; set; }
    }
}