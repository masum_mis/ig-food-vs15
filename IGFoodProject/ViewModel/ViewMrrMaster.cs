﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.ViewModel
{
    public class ViewMrrMaster
    {

           public int MRRMasterId{get;set;}
           public string MRRNo{get;set;}
           public DateTime MRRDate{get;set;}
           public string MRRDateStr{get;set;}
           public DateTime? CreateDate{get;set;}
           public string CreateDateStr{get;set;}
           public string ReceiveBy{get;set;}
           public string StatusDes{get;set;}
           public string PurchaseOrderNo{get;set;}
           public int PurchaseOrderID{get;set;}
           public int WareHouseId { get; set; }
           public int LocationId { get; set; }
           public int CompanyId { get; set; }
           public string Remarks { get; set; }
           public string WareHouseName { get; set; }
        public string LocationName { get; set; }
        public string CompanyName { get; set; }
        public string ReceiveTime { get; set; }
           public string EditStatus { get; set; }
           public decimal TotalReceiveQty { get; set; }
           public decimal TotalReceiveKg { get; set; }
           public string SupplierName { get; set; }
           public int DressedComplete { get; set; }
           public int MRRStatus { get; set; }
           public DateTime OrderDate { get; set; }
           public string OrderDatestr { get; set; }
           public int IsClosedMrr { get; set; }
           public string ClosedBy { get; set; }
           public string ClosedDate { get; set; }
        public List<ViewMrrDetails> MrrDetailsList { get; set; }

    }
}
