﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewProductByGroupAndWareHouse
    {
        public int ProductId { get; set; }
        public decimal StockQuantity { get; set; }
        public decimal StockKg { get; set; }
        public string ProductName { get; set; }


        public decimal TransferQty { get; set; }
        public decimal TransferKg { get; set; }


        public decimal AIssueQty { get; set; }
        public decimal AIssueKg { get; set; }


        public decimal IssueQty { get; set; }
        public decimal IssueKg { get; set; }

        public decimal PerKgStdQty { get; set; }
        public int GroupId { get; set; }
    }
}