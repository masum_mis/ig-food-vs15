﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewInvoiceDetails
    {
        public int InvoiceDetailID { get; set; }
        public int InvoiceId { get; set; }
        public int MRRID { get; set; }
        public DateTime MRRDate { get; set; }
        public decimal ReceivedQty { get; set; }
        public decimal InvoiceQty { get; set; }
        public decimal PriceWithoutVat { get; set; }
        public decimal VatPercent { get; set; }
        public decimal TotalInvoicePrice { get; set; }
        public string ItemDescription { get; set; }
        public string MrrNo { get; set; }
        public decimal ReceivedAmount { get; set; }
        public decimal InvoicedAmount { get; set; }
        public decimal Advanced { get; set; }
    }
}