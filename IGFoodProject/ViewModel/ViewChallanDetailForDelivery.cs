﻿using IGFoodProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewChallanDetailForDelivery
    {
        public int SalesOrderChallanMasterId { get; set; }
        public string SalesOrderChallanNo { get; set; }
        public string  ChallanDate { get; set; }
        public decimal TotalChallanQty { get; set; }
        public decimal TotalChallanKG { get; set; }
        public decimal ChallanAmount { get; set; }
        public int CompanyId { get; set; }
        public int CustomerTypeId { get; set; }
        public int CustomerId { get; set; }
        public bool IsEmployee { get; set; }
        public string SalesPerson { get; set; }
        public string OrderDate { get; set; }
        public string DeliveryDestination { get; set; }
        public string DeliveryDate { get; set; }
        public string SalesPersonContact { get; set; }

        public List<SalesOrderChallanDetails> SalesDetails { get; set; }
        public bool IsEcommerce { get; set; }

        public decimal TotalAmt { get; set; }
        public decimal TotalDiscountAmount { get; set; }
        public decimal dpercent { get; set; }

        public decimal TotalDiscountAmount_delivery { get; set; }
    }
}