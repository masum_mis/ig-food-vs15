﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewInvoiceMaster
    {
        public string ReportValue { get; set; }
        public int MRRrOtherID { get; set; }
        public int PurchaseOrderId { get; set; }
        public string PurchaseOrderNo { get; set; }
        public DateTime MRRDate { get; set; }
        public DateTime DueDate { get; set; }
        public int SupplierID { get; set; }
      
        public string MrrNo { get; set; }
        public string ItemDescription { get; set; }
       
      
       
        public string SupplierRef { get; set; }
        public string SupplierName { get; set; }
     
        public decimal TotalAmount { get; set; }
        public decimal TotalQty { get; set; }
      
     
    
        public decimal BillQuantity { get; set; }
        public decimal ReceivedQty { get; set; }
      
        public decimal TransportCost { get; set; }
        public decimal LaborCost { get; set; }

        public int InvoiceMasterID { get; set; }
        public string InvoiceNo { get; set; }
        public DateTime InvoiceDate { get; set; }
        public decimal TotalDue { get; set; }
        public decimal TotalPaid { get; set; }
        public decimal GrandTotal { get; set; }
        public decimal TotalTax { get; set; }
        public decimal TotalTaxPaid { get; set; }
        public decimal DiscountAmt { get; set; }

        public string InvoiceType { get; set; }

        public decimal POAdvance { get; set; }
        public decimal VatAmount { get; set; }
        public decimal VatPercent { get; set; }
        public string SupplierBillNo { get; set; }
        public DateTime SupplierBillDate { get; set; }
        public List<ViewInvoiceDetails> InvoiceDetails { get; set; }



    }
}