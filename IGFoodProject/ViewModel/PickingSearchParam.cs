﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class PickingSearchParam
    {
		public int PickingMasterId { get; set; }
	    public String OrderFromDate { get; set; }
        public String OrderToDate { get; set; }
		public int CustomerId { get; set; }
		public int CustomerTypeId { get; set; }
		public int AreaId { get; set; }
		public int TerritoryId { get; set; }
		public int MarketId { get; set; }
		public int DistrictId { get; set; }
		public int ThanaId { get; set; }
        public int DeliveryWarehouseId { get; set; }

	}
}