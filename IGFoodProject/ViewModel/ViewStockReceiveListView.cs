﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewStockReceiveListView
    {
        public int  StockTransferIssueMasterId { get; set; }
        public int FromWarehouseId { get; set; }
        public int ToWarehouseId { get; set; }
        public int CompanyId { get; set; }
        public int VehicleId { get; set; }
        public decimal IssueQty { get; set; }
        public decimal IssueKg { get; set; }
        public string FromWareHouseName{ get; set; }

        public string ToWareHouseName{ get; set; }

        public string  IssueBy{ get; set; }
        public string VehicleRegNo { get; set; }

        public string IssueNo{ get; set; }
        public DateTime?  IssueDate { get; set; }

        public List<ViewStockReceiveDetails> Details { get; set; } 
    }
}