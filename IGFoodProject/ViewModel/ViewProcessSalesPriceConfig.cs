﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{

    [Table("ViewProcessSalesPriceConfig")]
    public class ViewProcessSalesPriceConfig
    {
        [Key]
        public int ProcessedSalePriceConfigId { get; set; }
        public DateTime FromDate { get; set; }
        public decimal SalePrice { get; set; }
        public bool IsActive { get; set; }
        public decimal PurchasePrice { get; set; }
        public DateTime ToDate { get; set; }
        public string ProductName { get; set; }
    }
}