﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewSalesOrderDetailsForPicking
    {
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public int? ProductGroupId { get; set; }

        public decimal? OrderedApproved { get; set; }
        public decimal? OrderQty { get; set; }
        public decimal? OrderKg { get; set; }

        public decimal? PrevPicked { get; set; }
        public decimal? PrevPickedQty { get; set; }
        public decimal? PrevInputedQty { get; set; }
        public decimal? PrevInputedKg { get; set; }
        public decimal? Picked { get; set; }
        public decimal? InputedPickQty { get; set; }
        public decimal? InputedPickKg { get; set; }

        public int SalesOrderMasterId { get; set; }
        public int SalesOrderDetailsId { get; set; }
        public int PickingDetailsId { get; set; }
        public int PickingMasterId { get; set; }
        public int IsDelete { get; set; }
        public ViewSalesOrderMasterForPicking SalesOrderMasterForPicking { get; set; }

    }
}