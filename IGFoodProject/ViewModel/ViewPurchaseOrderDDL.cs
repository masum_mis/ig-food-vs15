﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewPurchaseOrderDDL
    {
        public string PurchaseOrderNo { get; set; }
        public int PurchaseOrderID { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }

    }
}