﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.ViewModel
{
    public class ViewPurchaseOrderMaster
    {

            public int PurchaseOrderID{get;set;}
            public string PurchaseOrderNo{get;set;}
            public string RequisitionNo { get;set;}
            public DateTime?     OrderDate {get;set;}
            public string WareHouseName {get;set;}
            public string SupplierName {get;set;}
            public string OrderBY {get;set;}
            public decimal TotalDue { get; set; }
            public decimal TotalQty { get; set; }
            public decimal TotalPaid { get; set; }
        public decimal TotalTax { get; set; }
        public decimal GrandTotal { get; set; }
        public string PurchaseType { get; set; }
        public List<ViewPurchaseOrderDetails> Details { get; set; }
    }
}
