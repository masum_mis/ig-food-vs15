﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class View_EmployeeInfo
    {
        public int EmId { get; set; }
        public int EmpNo { get; set; }
        public string EMPID { get; set; }
        public string EmpName { get; set; }
        public string Designation { get; set; }
        public int SalesOfficerId { get; set; }
    }
}