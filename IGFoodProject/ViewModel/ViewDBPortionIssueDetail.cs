﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewDBPortionIssueDetail
    {
       
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public decimal IssueQty { get; set; }
        public decimal IssueQtyInKG { get; set; }
        public string Remarks { get; set; }
    }
}