﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.ViewModel
{
    public class ViewDressedBirdMaster
    {
        public int DressedBirdMasterID { get; set; }
        public string DressedBirdNo { get; set; }
    }
}
