﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IGFoodProject.ViewModel
{
    public class ViewDressedDirdDetail
    {
        public int DressedBirdDetailID { get; set; }
        public int DressedBirdMasterID { get; set; }
        public int ItemId { get; set; }
        public string Remark { get; set; }
        public decimal UnitPrice { get; set; }
        public string ItemName { get; set; }
        public decimal Remainingqty { get; set; }
        public decimal RemainingKG { get; set; }
        public int ItemCategoryId { get; set; }
        public string DressedBirdDetailIDs { get; set; }
    }
}
