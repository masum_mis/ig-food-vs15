﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGFoodProject.ViewModel
{
    public class ViewUserWarehouse
    {

        public int LoginUserId { get; set; }
        public string LoginUserName { get; set; }
        public int ModuleId { get; set; }
        public string EmpName { get; set; }
        public string Designation { get; set; }
        public int WareHouseId { get; set; }
        public string WareHouseName { get; set; }
        public string LocationName { get; set; }
        public bool? IsActive { get; set; }
  
    }
}